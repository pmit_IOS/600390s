//
//  NSSafeMutableDictionary.m
//  TKCommonLib
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "NSMutableDictionaryAddition.h"

TK_FIX_CATEGORY_BUG(NSMutableDictionaryAddition)

@implementation NSMutableDictionary(TKCategory)

- (void)setSafeObject:(id)anObject forKey:(id)aKey
{
    if (anObject == nil) {
        anObject = [NSNull null];
        TKDWARNING(@"Ignore an nil object for Dictionary.");
    }
    [self setObject:anObject forKey:aKey];
}

@end


@implementation NSDictionary(TKCategory)

- (id)safeObjectForKey:(id)aKey
{
    id anObject = [self objectForKey:aKey];
    if (anObject == [NSNull null]) {
        anObject = nil;
    }
    return anObject;
}

- (id)insensitiveObjectForKey:(id)aKey 
{
    NSArray *allKeys = [self allKeys];
    NSString *theSafekey = aKey;
    for (NSString *key in allKeys) {
        
        if([key compare:aKey options:NSCaseInsensitiveSearch|  
            NSNumericSearch]==NSOrderedSame){  
            theSafekey = key;
        }  
    }
    id anObject = [self objectForKey:theSafekey];
    if (anObject == [NSNull null]) {
        anObject = nil;
    }
    return anObject;
}


@end
