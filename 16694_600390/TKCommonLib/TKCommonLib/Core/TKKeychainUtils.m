
#import "TKKeychainUtils.h"
#import <Security/Security.h>

NSString* const userKeyChainIdentifier = @"AppUseridToken";
NSString* const uservoIdentifier = @"uservoIdentifier";//用户id
NSString* const alertNewTips = @"alertNewTips";//版本提示

NSString* Userid = nil;

@interface TKKeychainUtils()
+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier;
@end
    

@implementation TKKeychainUtils

+ (BOOL)createKeychainValueForUserid:(NSString *)userid 
                           withToken:(NSString *)token 
                            withName:(NSString *)name 
                          withAccountType:(NSUInteger)accountType
                          withAvtarURL:(NSString *)avtarURL
                       forIdentifier:(NSString *)identifier
{   
    NSMutableDictionary *dictionary = [TKKeychainUtils newSearchDictionary:identifier];
//kSecAttrApplicationLabel
    NSMutableDictionary *attributes = nil;
    OSStatus result;
    // Add search attributes
    [dictionary setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    // Add search return types
    [dictionary setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnAttributes];
    if (SecItemCopyMatching((CFDictionaryRef)dictionary, (CFTypeRef *)&attributes) == noErr) {
        //set search key
        [dictionary removeObjectForKey:(id)kSecMatchLimit];
        [dictionary removeObjectForKey:(id)kSecReturnAttributes];
        [dictionary setObject:[attributes objectForKey:(id)kSecAttrAccount]  forKey:(id)kSecAttrAccount];
        NSMutableDictionary *updateDate = [[NSMutableDictionary alloc]init];
        [updateDate setObject:userid forKey:(id)kSecAttrAccount];
        //set token data
        NSData *tokenData = [token dataUsingEncoding:NSUTF8StringEncoding];
        [updateDate setObject:tokenData forKey:(id)kSecValueData];
        //set name data
        [updateDate setObject:name forKey:(id)kSecAttrLabel];
        
        //set accout type
        [updateDate setObject:@(accountType).stringValue forKey:(id)kSecAttrComment];
       
        //set avtarURL
        [updateDate setObject:avtarURL forKey:(id)kSecAttrDescription];
        
        result = SecItemUpdate((CFDictionaryRef)dictionary,
                               (CFDictionaryRef)updateDate);
        [updateDate release];
        [attributes release];
        attributes = nil;
    } else {
        [dictionary removeObjectForKey:(id)kSecMatchLimit];
        [dictionary removeObjectForKey:(id)kSecReturnAttributes];
        //set userid data
        [dictionary setObject:userid forKey:(id)kSecAttrAccount];
        //set token data
        NSData *tokenData = [token dataUsingEncoding:NSUTF8StringEncoding];
        [dictionary setObject:tokenData forKey:(id)kSecValueData];
        //set name data
        [dictionary setObject:name forKey:(id)kSecAttrLabel];
        //set accout 
        [dictionary setObject:@(accountType).stringValue forKey:(id)kSecAttrComment];
        //set mobile
        [dictionary setObject:avtarURL forKey:(id)kSecAttrDescription];
        
        result = SecItemAdd((CFDictionaryRef)dictionary, NULL);
        
    }
    [dictionary release];
    
    if (result == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (void)deleteKeychainValue:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [TKKeychainUtils newSearchDictionary:identifier];
    SecItemDelete((CFDictionaryRef)searchDictionary);
    [searchDictionary release];
}

+ (BOOL)updateUsername:(NSString *)name identifier:(NSString *)identifier {
    //setup search dict, use username as query param
    NSMutableDictionary *searchDictionary = [self newSearchDictionary:identifier];
    NSMutableDictionary *updateDictionary = [NSMutableDictionary dictionary];
    if (name) [updateDictionary setObject:name forKey:(id)kSecAttrLabel];
    OSStatus status = SecItemUpdate((CFDictionaryRef)searchDictionary,
                                    (CFDictionaryRef)updateDictionary);
    
    [searchDictionary release];
    if (status == errSecSuccess) {
        return YES;
    }
    return NO;
}

+ (NSString *)getAvtarURL:(NSString *)identifier{
    NSMutableDictionary *attributeSearch = [TKKeychainUtils newSearchDictionary:identifier];
    
    // Add search attributes
    [attributeSearch setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [attributeSearch setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnAttributes];
    
    //Get username first
    NSMutableDictionary *attributeResult = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)attributeSearch,
                                          (CFTypeRef *)&attributeResult);
    
    NSString *mobile = nil;
    if (status == errSecSuccess) {
        NSString *mobileValue = [attributeResult objectForKey:(id)kSecAttrDescription];
        if (mobileValue) {
            mobile =  [[mobileValue mutableCopy] autorelease];
            [attributeResult release];
            attributeResult = nil;
        }
    }
    [attributeResult release];
    [attributeSearch release];
    return mobile;
}

//+ (NSDictionary *)getUserAcounts:(NSString *)identifier{
//    NSString *acount = [self getUseraccount:identifier];
//    if(acount == nil){
//        return nil;
//    }
//    NSArray *acts = [acount componentsSeparatedByString:@"|"];
//    TKDPRINT(@"acts %@", acts);
//    NSMutableDictionary *dic = [NSMutableDictionary  dictionary];
//    if([acts count]>1)
//    {
//        [dic setObject:[acts objectAtIndex:1] forKey:@"sina"];
//    }
//    else
//    {
//        [dic setObject:@"未绑定" forKey:@"sina"];
//    }
//    
//    if([acts count]>2)
//    {
//        [dic setObject:[acts objectAtIndex:2 ] forKey:@"tqq"];
//    }
//    else
//    {
//        [dic setObject:@"未绑定" forKey:@"tqq"];
//    }
//    
//    if([acts count]>3)
//    {
//        [dic setObject:[acts objectAtIndex:3] forKey:@"renren"];
//    }
//    else
//    {
//        [dic setObject:@"未绑定" forKey:@"renren"];
//    }
//    
//    if([acts count]>4)
//    {
//        [dic setObject:[acts objectAtIndex:4 ] forKey:@"kaixin"];
//    }
//    else
//    {
//        [dic setObject:@"未绑定" forKey:@"kaixin"];
//    }
//    return dic;
//}


+ (NSUInteger)getAccountType:(NSString *)identifier{
    NSMutableDictionary *attributeSearch = [TKKeychainUtils newSearchDictionary:identifier];
    
    // Add search attributes
    [attributeSearch setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [attributeSearch setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnAttributes];
    
    //Get username first
    NSMutableDictionary *attributeResult = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)attributeSearch,
                                          (CFTypeRef *)&attributeResult);
    
    NSString *accout = nil;
    if (status == errSecSuccess) {
        NSString* accoutValue = [attributeResult objectForKey:(id)kSecAttrComment];
        if (accoutValue) {
            accout =  [[accoutValue mutableCopy] autorelease];
            [attributeResult release];
            attributeResult = nil;
        }
    }
    [attributeResult release];
    [attributeSearch release];
    return [accout integerValue];
}

+ (NSString *)getUsername:(NSString *)identifier{
    NSMutableDictionary *attributeSearch = [TKKeychainUtils newSearchDictionary:identifier];
    
    // Add search attributes
    [attributeSearch setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [attributeSearch setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnAttributes];
    
    //Get username first
    NSMutableDictionary *attributeResult = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)attributeSearch,
                                          (CFTypeRef *)&attributeResult);
    
    NSString *name = nil;
    if (status == errSecSuccess) {
        NSString* nameValue = [attributeResult objectForKey:(id)kSecAttrLabel];
        if (nameValue) {
            name =  [[nameValue mutableCopy] autorelease];
            [attributeResult release];
            attributeResult = nil;
        }
    }
    [attributeResult release];
    [attributeSearch release];
    return name;
}

+ (NSString *)getToken:(NSString *)identifier {
    //Get token next
    NSMutableDictionary *tokenSearch = [TKKeychainUtils newSearchDictionary:identifier];
    
    // Add search attributes
    [tokenSearch setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [tokenSearch setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnData];
    
    NSData *tokenData = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)tokenSearch,
                                          (CFTypeRef *)&tokenData);
    [tokenSearch release];
    
    NSString *token = nil;
    if (status == errSecSuccess) {
        if (tokenData) {
            token = [[[NSString alloc] initWithData:tokenData encoding:NSUTF8StringEncoding] autorelease];
            [tokenData release];
            tokenData = nil;
            //UALOG(@"Loaded token: %@",token);
        }
    }
    
    return token;
}

+ (NSString *)getUserid:(NSString *)identifier {
    NSMutableDictionary *attributeSearch = [TKKeychainUtils newSearchDictionary:identifier];
    
    // Add search attributes
    [attributeSearch setObject:(id)kSecMatchLimitOne forKey:(id)kSecMatchLimit];
    
    // Add search return types
    [attributeSearch setObject:(id)kCFBooleanTrue forKey:(id)kSecReturnAttributes];
    
    //Get username first
    NSMutableDictionary *attributeResult = nil;
    OSStatus status = SecItemCopyMatching((CFDictionaryRef)attributeSearch,
                                          (CFTypeRef *)&attributeResult);
    
    NSString *userid = nil;
    if (status == errSecSuccess) {
        NSString* accountValue = [attributeResult objectForKey:(id)kSecAttrAccount];
        if (accountValue) {
            //TKDPRINT(@"accountValue %@",accountValue);
            // 注意数据类型，　此处userid只能使用　NSString
            //userid =  [NSString  stringWithFormat:@"%@",accountValue];
            //userid =[[[NSString alloc] initWithFormat:@"%@",accountValue ] autorelease];
           userid =  
            [[accountValue mutableCopy] autorelease];
            //UALOG(@"Loaded Username: %@",username);
        }
    }
    [attributeResult release];
    [attributeSearch release];
    
    return userid;
}


+ (NSMutableDictionary *)newSearchDictionary:(NSString *)identifier {
    NSMutableDictionary *searchDictionary = [[NSMutableDictionary alloc] init];  
    
    [searchDictionary setObject:(id)kSecClassGenericPassword forKey:(id)kSecClass];
    
    //use identifier param and the bundle ID as keys
    NSData *encodedIdentifier = [identifier dataUsingEncoding:NSUTF8StringEncoding];
    [searchDictionary setObject:encodedIdentifier forKey:(id)kSecAttrGeneric];
    
    NSString *bundleId = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
    [searchDictionary setObject:bundleId forKey:(id)kSecAttrService];
    return searchDictionary; 
}


+ (void) setUserid:(NSString *)identifier {
    if (Userid) {
        [Userid release];
    }
    Userid = [identifier retain];
}
+ (NSString *)getUserid {
    return Userid;
}

@end
