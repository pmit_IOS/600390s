
// From Three20
#define TKLOGLEVEL_INFO     5
#define TKLOGLEVEL_WARNING  3
#define TKLOGLEVEL_ERROR    1

#ifndef TKMAXLOGLEVEL
  #define TKMAXLOGLEVEL TKLOGLEVEL_INFO
#endif


// ##################################  Debug  ##################################
// The general purpose logger. This ignores logging levels.
#ifdef DEBUG
    #define TKDPRINT(xx, ...)  NSLog(@" %s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#else
    //#define TKDPRINT(xx, ...)  ((void)0)
    #define TKDPRINT(xx, ...)  NSLog(@" %s(%d): " xx, __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#endif // #ifdef DEBUG

// Prints the current method's name.
#define TKDPRINTMETHODNAME() TKDPRINT(@"%s", __PRETTY_FUNCTION__)

// Debug-only assertions.
#ifdef DEBUG

#import <TargetConditionals.h>

#if TARGET_IPHONE_SIMULATOR

  int TKIsInDebugger();
  // We leave the __asm__ in this macro so that when a break occurs, we don't have to step out of
  // a "breakInDebugger" function.
  #define TKDASSERT(xx) { if(!(xx)) { TKDPRINT(@"TKDASSERT failed: %s", #xx); \
                                      if(TKIsInDebugger()) { __asm__("int $3\n" : : ); }; } \
                        } ((void)0)
#else
  #define TKDASSERT(xx) { if(!(xx)) { TKDPRINT(@"TKDASSERT failed: %s", #xx); } } ((void)0)
#endif // #if TARGET_IPHONE_SIMULATOR

#else
  #define TKDASSERT(xx) ((void)0)
#endif // #ifdef DEBUG

// Log-level based logging macros.
#if TKLOGLEVEL_ERROR <= TKMAXLOGLEVEL
  #define TKDERROR(xx, ...)  TKDPRINT(xx, ##__VA_ARGS__)
#else
  #define TKDERROR(xx, ...)  ((void)0)
#endif // #if TKLOGLEVEL_ERROR <= TKMAXLOGLEVEL

#if TKLOGLEVEL_WARNING <= TKMAXLOGLEVEL
  #define TKDWARNING(xx, ...)  TKDPRINT(xx, ##__VA_ARGS__)
#else
  #define TKDWARNING(xx, ...)  ((void)0)
#endif // #if TKLOGLEVEL_WARNING <= TKMAXLOGLEVEL

#if TKLOGLEVEL_INFO <= TKMAXLOGLEVEL
  #define TKDINFO(xx, ...)  TKDPRINT(xx, ##__VA_ARGS__)
#else
  #define TKDINFO(xx, ...)  ((void)0)
#endif // #if TKLOGLEVEL_INFO <= TKMAXLOGLEVEL

#ifdef DEBUG
  #define TKDCONDITIONLOG(condition, xx, ...) { if ((condition)) { \
                                                  TKDPRINT(xx, ##__VA_ARGS__); \
                                                } \
                                              } ((void)0)
#else
  #define TKDCONDITIONLOG(condition, xx, ...) ((void)0)
#endif // #ifdef DEBUG
