//
//  TKSafetyMutableArray.m
//  TKCommonLib
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKSafeMutableArray.h"


@implementation TKSafeMutableArray

- (void)addObject:(id)anObject
{
    if (anObject == nil) {
        anObject = [NSNull null];
        TKDPRINT(@"Ignore an nil object for array.");
    }
    [super addObject:anObject];
}

- (void)insertObject:(id)anObject atIndex:(NSUInteger)index
{
    if (anObject == nil) {
        anObject = [NSNull null];
        TKDPRINT(@"Ignore an nil object for array.");
    }
    [super insertObject:anObject atIndex:index];
}

- (void)replaceObjectAtIndex:(NSUInteger)index withObject:(id)anObject
{
    if (anObject == nil) {
        anObject = [NSNull null];
        TKDPRINT(@"Ignore an nil object for array.");
    }
    [super replaceObjectAtIndex:index withObject:anObject];
}

@end
