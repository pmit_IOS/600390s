//
//  NSSafeMutableArray.h
//  TKCommonLib
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 *	@brief	安全的可变数组，当添加项为nil，自动转换为［NSNULL null］对象，防止程序崩溃
 */
@interface TKSafeMutableArray : NSMutableArray {
    
}

@end
