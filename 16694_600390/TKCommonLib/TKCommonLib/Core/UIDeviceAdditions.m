/*
 Erica Sadun, http://ericasadun.com
 iPhone Developer's Cookbook, 3.0 Edition
 BSD License for anything not specifically marked as developed by a third party.
 Apple's code excluded.
 Use at your own risk
 */

#import <SystemConfiguration/SystemConfiguration.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <net/if.h>
#include <ifaddrs.h>
#include <sys/types.h>
#include <sys/sysctl.h>
#include <net/if_dl.h>
#import "UIDeviceAdditions.h"
#import "NSStringAdditions.h"

TK_FIX_CATEGORY_BUG(UIDeviceAdditions)

@implementation UIDevice (Reachability)
SCNetworkConnectionFlags connectionFlags;

// Matt Brown's get WiFi IP addy solution
// http://mattbsoftware.blogspot.com/2009/04/how-to-get-ip-address-of-iphone-os-v221.html
+ (NSString *) localWiFiIPAddress
{
	BOOL success;
	struct ifaddrs * addrs;
	const struct ifaddrs * cursor;
	
	success = getifaddrs(&addrs) == 0;
	if (success) {
		cursor = addrs;
		while (cursor != NULL) {
			// the second test keeps from picking up the loopback address
			if (cursor->ifa_addr->sa_family == AF_INET && (cursor->ifa_flags & IFF_LOOPBACK) == 0) 
			{
				NSString *name = [NSString stringWithUTF8String:cursor->ifa_name];
				if ([name isEqualToString:@"en0"])  // Wi-Fi adapter
					return [NSString stringWithUTF8String:inet_ntoa(((struct sockaddr_in *)cursor->ifa_addr)->sin_addr)];
			}
			cursor = cursor->ifa_next;
		}
		freeifaddrs(addrs);
	}
	return nil;
}

#pragma mark Checking Connections

+ (void) pingReachabilityInternal
{
	BOOL ignoresAdHocWiFi = NO;
	struct sockaddr_in ipAddress;
	bzero(&ipAddress, sizeof(ipAddress));
	ipAddress.sin_len = sizeof(ipAddress);
	ipAddress.sin_family = AF_INET;
	ipAddress.sin_addr.s_addr = htonl(ignoresAdHocWiFi ? INADDR_ANY : IN_LINKLOCALNETNUM);
    
    // Recover reachability flags
    SCNetworkReachabilityRef defaultRouteReachability = SCNetworkReachabilityCreateWithAddress(kCFAllocatorDefault, (struct sockaddr *)&ipAddress);    
    BOOL didRetrieveFlags = SCNetworkReachabilityGetFlags(defaultRouteReachability, &connectionFlags);
    CFRelease(defaultRouteReachability);
	if (!didRetrieveFlags) 
        printf("Error. Could not recover network reachability flags\n");
}

+ (BOOL) networkAvailable
{
	[self pingReachabilityInternal];
	BOOL isReachable = ((connectionFlags & kSCNetworkFlagsReachable) != 0);
    BOOL needsConnection = ((connectionFlags & kSCNetworkFlagsConnectionRequired) != 0);
    return (isReachable && !needsConnection) ? YES : NO;
}

+ (BOOL) activeWWAN
{
	if (![self networkAvailable]) return NO;
	return ((connectionFlags & kSCNetworkReachabilityFlagsIsWWAN) != 0);
}

+ (BOOL) activeWLAN
{
	return ([UIDevice localWiFiIPAddress] != nil);
}

@end

@implementation UIDevice (Platform)

+(NSString *)machine
{
    size_t size;
    
    sysctlbyname("hw.machine", NULL, &size, NULL, 0);
    
    char *machine = malloc(size);
    
    sysctlbyname("hw.machine", machine, &size, NULL, 0);
    
    NSString *hardwareType = [NSString stringWithCString:machine encoding:NSUTF8StringEncoding];
    
    free(machine);
    return hardwareType;
}

+ (NSString *)macaddress {
    int                 mib[6];
    size_t              len;
    char                *buf;
    unsigned char       *ptr;
    struct if_msghdr    *ifm;
    struct sockaddr_dl  *sdl;
    
    mib[0] = CTL_NET; 
    mib[1] = AF_ROUTE;
    mib[2] = 0;
    mib[3] = AF_LINK;
    mib[4] = NET_RT_IFLIST;
    
    if ((mib[5] = if_nametoindex("en0")) == 0) {
        printf("Error: if_nametoindex error/n");
        return NULL;
    }
    if (sysctl(mib, 6, NULL, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 1/n");
        return NULL;
    }
    if ((buf = malloc(len)) == NULL) {
        printf("Could not allocate memory. error!/n");
        return NULL;
    }
    if (sysctl(mib, 6, buf, &len, NULL, 0) < 0) {
        printf("Error: sysctl, take 2");
        return NULL;
    }
    
    ifm = (struct if_msghdr *)buf;
    sdl = (struct sockaddr_dl *)(ifm + 1);
    ptr = (unsigned char *)LLADDR(sdl);
    // NSString *outstring = [NSString stringWithFormat:@"%02x:%02x:%02x:%02x:%02x:%02x", *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    NSString *outstring = [NSString stringWithFormat:@"%02x%02x%02x%02x%02x%02x", *ptr, *(ptr+1), *(ptr+2), *(ptr+3), *(ptr+4), *(ptr+5)];
    free(buf);
    return [outstring uppercaseString];
}

+(BOOL)isIPod
{
    return [[UIDevice machine] hasPrefix:@"iPod"];
}

+(BOOL)isIPhone
{
    return [[UIDevice machine] hasPrefix:@"iPhone"];
}

+(BOOL)isIPad
{
    return [[UIDevice machine] hasPrefix:@"iPad"];
}

+(NSString *) platform{
    
    NSString *platform = [UIDevice machine];
    
    if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
    
    if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
    
    if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
    
    if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4";
    
    if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
    
    if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
    
    if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
    
    if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
    
    if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
    
    if ([platform isEqualToString:@"i386"])         return @"Simulator";
    
//    if ([platform isEqual:@"i386"])      return @"Simulator";       //iPhone Simulator
//    if ([platform isEqual:@"iPhone1,1"]) return @"iPhone 1G";        //iPhone 1G
//    if ([platform isEqual:@"iPhone1,2"]) return @"iPhone 3G";        //iPhone 3G
//    if ([platform isEqual:@"iPhone2,1"]) return @"iPhone 3GS";       //iPhone 3GS
//    if ([platform isEqual:@"iPhone3,1"]) return @"iPhone 4";         //iPhone 4 - AT&T
//    if ([platform isEqual:@"iPhone3,2"]) return @"iPhone 4";         //iPhone 4 - Other carrier
//    if ([platform isEqual:@"iPhone3,3"]) return @"iPhone 4";         //iPhone 4 - Other carrier
//    if ([platform isEqual:@"iPhone4,1"]) return @"iPhone 4S";        //iPhone 4S
//    if ([platform isEqual:@"iPod1,1"])   return @"iPod Touch 1G";   //iPod Touch 1G
//    if ([platform isEqual:@"iPod2,1"])   return @"iPod Touch 2G";   //iPod Touch 2G
//    if ([platform isEqual:@"iPod3,1"])   return @"iPod Touch 3G";   //iPod Touch 3G
//    if ([platform isEqual:@"iPod4,1"])   return @"iPod Touch 4G";   //iPod Touch 4G
//    if ([platform isEqual:@"iPad1,1"])   return @"iPad Wifi";       //iPad Wifi
//    if ([platform isEqual:@"iPad1,2"])   return @"iPad 3G";         //iPad 3G
//    if ([platform isEqual:@"iPad2,1"])   return @"iPad 2 (WiFi)";   //iPad 2 (WiFi)
//    if ([platform isEqual:@"iPad2,2"])   return @"iPad 2 (GSM)";    //iPad 2 (GSM)
//    if ([platform isEqual:@"iPad2,3"])   return @"iPad 2 (CDMA)";   //iPad 2 (CDMA)
    
    return platform;
    
}

@end

@implementation UIDevice (Identifier)

+ (NSString *) uniqueDeviceIdentifier{
    NSString *macaddress = [UIDevice macaddress];
    NSString *bundleIdentifier = [[NSBundle mainBundle] bundleIdentifier];
    
    NSString *stringToHash = [NSString stringWithFormat:@"%@%@",macaddress,bundleIdentifier];
    NSString *uniqueIdentifier = [stringToHash md5];
    
    return uniqueIdentifier;
}

+ (NSString *) uniqueGlobalDeviceIdentifier{
    NSString *macaddress = [UIDevice macaddress];
    NSString *uniqueIdentifier = [macaddress md5];
    
    return uniqueIdentifier;
}

@end


