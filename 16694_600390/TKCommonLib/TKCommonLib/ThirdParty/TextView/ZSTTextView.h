//
//  ZSTTextView.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-24.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTTextView : UITextView

@property (copy, nonatomic) NSString *placeholder;
@property (retain, nonatomic) UIColor *placeholderTextColor UI_APPEARANCE_SELECTOR;

@end
