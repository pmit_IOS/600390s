//
//  UIViewController+BCTabBar.h
//  VoiceChina
//
//  Created by huijun xu on 13-2-18.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BCTabBarController.h"

@interface UIViewController (BCTabBar)

@property (nonatomic, readonly, assign) BCTabBarController *bcTabbarController;

@end
