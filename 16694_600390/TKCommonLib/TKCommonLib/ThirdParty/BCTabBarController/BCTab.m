#import "BCTab.h"
#import "ZSTUtils.h"

#define kDefaultTitleColor      [UIColor colorWithRed:211.0f/255.0f green:204.0f/255.0f blue:196.0f/255.0f alpha:1.0f]
#define kHighlitedTitleColor    [UIColor whiteColor]
#define kTitleLabelHeight       12

@implementation UIButton (UIButtonImageWithLable)

- (void) setImage:(UIImage *)image withTitle:(NSString *)title forState:(UIControlState)stateType {
    
    //UIEdgeInsetsMake(CGFloat top, CGFloat left, CGFloat bottom, CGFloat right)
    
    // get the size of the elements here for readability
    CGSize imageSize = image.size;
    CGSize titleSize = [title sizeWithFont:[UIFont boldSystemFontOfSize:10.f]];
    
    // get the height they will take up as a unit
    CGFloat totalHeight = (imageSize.height + titleSize.height );
    
    // raise the image and push it right to center it
    
    // lower the text and push it left to center it
    UIImageView *newsIV = [[UIImageView alloc] init];
    newsIV.frame = CGRectMake(16, self.imageView.frame.origin.y, imageSize.width, imageSize.height);
    UIImageView *newsHighIV = [[UIImageView alloc] init];
    newsHighIV.frame = CGRectMake(16, self.imageView.frame.origin.y, imageSize.width, imageSize.height);
    
    //    [self.imageView setContentMode:UIViewContentModeCenter];
    
    [self setImageEdgeInsets:UIEdgeInsetsMake(- floor(totalHeight - imageSize.height), 0.0f , -2, - floor(titleSize.width))];
    newsHighIV.hidden = YES;
    [newsIV setContentMode:UIViewContentModeScaleAspectFit];
    [newsHighIV setContentMode:UIViewContentModeScaleAspectFit];
    [self addSubview:newsIV];
    [self addSubview:newsHighIV];
    
    //    [self setImage:image forState:stateType];
    if (stateType == UIControlStateNormal)
    {
        [newsIV setImage:image];
        newsIV.hidden = NO;
        newsHighIV.hidden = YES;
    }
    else
    {
        [newsHighIV setImage:image];
        newsHighIV.hidden = NO;
        newsIV.hidden = YES;
    }
    
    [self.titleLabel setContentMode:UIViewContentModeCenter];
    
    [self.titleLabel setBackgroundColor:[UIColor clearColor]];
    
    [self.titleLabel setFont:[UIFont boldSystemFontOfSize:10.f]];
    
    [self.titleLabel setTextColor:kDefaultTitleColor];
    
    [self setTitleEdgeInsets:UIEdgeInsetsMake(2.0, -1, - floor(totalHeight - titleSize.height) - 2, 0.0)];
    
    [self setTitle:title forState:stateType];
    
}

@end

@interface BCTab ()
@property (nonatomic, retain) UIImage *rightBorder;
@property (nonatomic, retain) UIImage *background;
@end

@implementation BCTab
@synthesize rightBorder, background;
@synthesize badgeView,tabTitleLabel;

- (id)initWithIconImageName:(NSString *)imageName {
	if (self = [super init]) {
		self.adjustsImageWhenHighlighted = NO;
		self.background = [UIImage imageNamed:@"BCTabBarController.bundle/tab-background.png"];
		self.rightBorder = [UIImage imageNamed:@"BCTabBarController.bundle/tab-right-border.png"];
		self.backgroundColor = [UIColor clearColor];
		
        NSString *selectedStr = [[imageName stringByDeletingPathExtension] stringByReplacingOccurrencesOfString:@"_n" withString:@"_p"];
		NSString *selectedName = [NSString stringWithFormat:@"%@.%@",
                                  selectedStr,
                                  [imageName pathExtension]];

//        [self setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
//        [self setBackgroundImage:[UIImage imageNamed:selectedName]  forState:UIControlStateSelected];
        
        [self setImage:[UIImage imageNamed:imageName] withTitle:@"视频" forState:UIControlStateNormal];
        [self setImage:[UIImage imageNamed:selectedName] withTitle:@"视频" forState:UIControlStateSelected];
        
        self.badgeView =  [[ZSTSignaView alloc] initWithImage:ZSTModuleImage(@"Module.bundle/module_shellc_left_messageCount.png")];
        self.badgeView.backgroundColor = [UIColor clearColor];
        self.badgeView.frame = CGRectMake(38, 5, 23, 20);
        [self setBadgeNumber:0];
        [self addSubview:self.badgeView];
        
	}
	return self;
}

-(void)setBadgeNumber:(NSInteger)count
{
    if (0 == count)
    {
        self.badgeView.alpha = 0;
        self.badgeView.text = @"0";
    }
    else
    {
        self.badgeView.alpha = 1;
        self.badgeView.text = @(count).stringValue;
    }
}

- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title
{
//    self = [self initWithIconImageName:imageName];
    if (self = [super init]) {
//        self.tabTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.frame.size.height - kTitleLabelHeight, self.frame.size.width, 10)];
//        self.tabTitleLabel.textAlignment = NSTextAlignmentCenter;
//        self.tabTitleLabel.font = [UIFont boldSystemFontOfSize:10.f];
//        self.tabTitleLabel.textColor = kDefaultTitleColor;
//        self.tabTitleLabel.text = title;
//        self.tabTitleLabel.backgroundColor = [UIColor clearColor];
//        [self addSubview:self.tabTitleLabel];
        
        self.adjustsImageWhenHighlighted = NO;
		self.background = [UIImage imageNamed:@"BCTabBarController.bundle/tab-background.png"];
		self.rightBorder = [UIImage imageNamed:@"BCTabBarController.bundle/tab-right-border.png"];
		self.backgroundColor = [UIColor clearColor];
		
        NSString *selectedStr = [[imageName stringByDeletingPathExtension] stringByReplacingOccurrencesOfString:@"_n" withString:@"_p"];
		NSString *selectedName = [NSString stringWithFormat:@"%@.%@",
                                  selectedStr,
                                  [imageName pathExtension]];
        
        if (title) {
            [self setImage:[UIImage imageNamed:imageName] withTitle:title forState:UIControlStateNormal];
            [self setImage:[UIImage imageNamed:selectedName] withTitle:title forState:UIControlStateSelected];
            [self setTitleColor:kHighlitedTitleColor forState:UIControlStateSelected];
        } else {
            
            [self setBackgroundImage:[UIImage imageNamed:imageName] forState:UIControlStateNormal];
            [self setBackgroundImage:[UIImage imageNamed:selectedName]  forState:UIControlStateSelected];
            
        }
        
        self.badgeView =  [[ZSTSignaView alloc] initWithImage:ZSTModuleImage(@"Module.bundle/module_shellc_left_messageCount.png")];
        self.badgeView.backgroundColor = [UIColor clearColor];
        self.badgeView.frame = CGRectMake(38, 5, 23, 20);
        [self setBadgeNumber:0];
        [self addSubview:self.badgeView];

    }
    return self;
}

- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title titleColor:(NSString *)color
{
    self = [self initWithIconImageName:imageName title:title];
    
    UIColor *co = [ZSTUtils colorFromHexColor:color];
    if (!co) {
        
        co = kDefaultTitleColor;
    }
    [self setTitleColor:co forState:UIControlStateNormal];
    return self;
}

- (void)setHighlighted:(BOOL)aBool {
	// no highlight state
}

//- (void)drawRect:(CGRect)rect {
//	if (self.selected) {
//		[background drawAtPoint:CGPointMake(0, 2)];
//		[rightBorder drawAtPoint:CGPointMake(self.bounds.size.width - rightBorder.size.width, 2)];
//		CGContextRef c = UIGraphicsGetCurrentContext();
//		[[UIColor colorWithRed:24/255.0 green:24/255.0 blue:24/255.0 alpha:1] set];
//		CGContextFillRect(c, CGRectMake(0, self.bounds.size.height / 2, self.bounds.size.width, self.bounds.size.height / 2));
//		[[UIColor colorWithRed:14/255.0 green:14/255.0 blue:14/255.0 alpha:1] set];
//		CGContextFillRect(c, CGRectMake(0, self.bounds.size.height / 2, 0.5, self.bounds.size.height / 2));
//		CGContextFillRect(c, CGRectMake(self.bounds.size.width - 0.5, self.bounds.size.height / 2, 0.5, self.bounds.size.height / 2));
//	}
//}

- (void)setFrame:(CGRect)aFrame {
	[super setFrame:aFrame];
	[self setNeedsDisplay];
}

- (void)layoutSubviews {
	[super layoutSubviews];
	
//    UIEdgeInsets imageInsets = UIEdgeInsetsMake(floor((self.bounds.size.height / 2) -
//                                                (self.imageView.image.size.height / 2)),
//    												floor((self.bounds.size.width / 2) -
//    												(self.imageView.image.size.width / 2)),
//    												floor((self.bounds.size.height / 2) -
//    												(self.imageView.image.size.height / 2)),
//                                                    floor((self.bounds.size.width / 2) -
//    											 	(self.imageView.image.size.width / 2)));
//    self.imageEdgeInsets = imageInsets;
//    self.tabTitleLabel.frame = CGRectMake(0, self.frame.size.height - kTitleLabelHeight, self.frame.size.width, 10);
}

- (void)setSelected:(BOOL)selected
{
//    if (self.tabTitleLabel) {
//        if (selected) {
//            self.tabTitleLabel.textColor = kHighlitedTitleColor;
//        }
//        else {
//            self.tabTitleLabel.textColor = kDefaultTitleColor;
//        }
//    }
    [super setSelected:selected];
}


@end
