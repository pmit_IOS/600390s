
#import "ZSTSignaView.h"

@interface UIButton (UIButtonImageWithLable)

- (void) setImage:(UIImage *)image withTitle:(NSString *)title forState:(UIControlState)stateType;

@end

@interface BCTab : UIButton {
	UIImage *background;
	UIImage *rightBorder;
}

@property (retain, nonatomic) UILabel* tabTitleLabel;
@property (retain, nonatomic) ZSTSignaView *badgeView;

@property (retain, nonatomic) NSString *string;

- (id)initWithIconImageName:(NSString *)imageName;
- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title;
- (id)initWithIconImageName:(NSString *)imageName title:(NSString*)title titleColor:(NSString *)color;
-(void)setBadgeNumber:(NSInteger)count;


@end
