//
//  TKWebViewController.m
//  
//
//  Created by luo bin on 12-3-19.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TKWebViewController.h"
#import "UIViewAdditions.h"
#import "UIToolbarAdditions.h"
#import "TKGlobalUICommon.h"
#import "TKUIUtil.h"

@implementation TKWebViewController

@synthesize delegate    = _delegate;
@synthesize headerView  = _headerView;


///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.hidesBottomBarWhenPushed = YES;
    }
    
    return self;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (id)init {
	self = [self initWithNibName:nil bundle:nil];
    if (self) {
    }
    
    return self;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dealloc {
    TKRELEASE(_loadingURL);
    TKRELEASE(_headerView);
    TKRELEASE(_actionSheet);
    
    _delegate = nil;
    _webView.delegate = nil;
    
    TKRELEASE(_webView);
    TKRELEASE(_toolbar);
    TKRELEASE(_backButton);
    TKRELEASE(_forwardButton);
    TKRELEASE(_refreshButton);
    TKRELEASE(_stopButton);
    TKRELEASE(_actionButton);
    TKRELEASE(_spinner);
    
    [super dealloc];
}

- (void)dismiss
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIView *)titleViewWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(62, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.font = [UIFont boldSystemFontOfSize:20];
    label.lineBreakMode = UILineBreakModeMiddleTruncation;
    return[label autorelease];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark Private


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)backAction {
    [_webView goBack];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)forwardAction {
    [_webView goForward];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)refreshAction {
    [_webView reload];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)stopAction {
    [_webView stopLoading];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)shareAction {
    if (nil != _actionSheet && [_actionSheet isVisible]) {
        //should only happen on the iPad
        assert(TKIsPad());
        [_actionSheet dismissWithClickedButtonIndex:-1 animated:YES];
        return;
    }
    
    if (nil == _actionSheet) {
        _actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                   delegate:self
                                          cancelButtonTitle:NSLocalizedString(@"Cancel", @"")
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:NSLocalizedString(@"Open in Safari", @""),
                        nil];
        if (TKIsPad()) {
            [_actionSheet showFromBarButtonItem:_actionButton animated:YES];
            
        }  else {
            [_actionSheet showInView: self.view];
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)updateToolbarWithOrientation:(UIInterfaceOrientation)interfaceOrientation {
    UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
    CGRect frame = [UIScreen mainScreen].applicationFrame;
    CGFloat height = (UIInterfaceOrientationIsPortrait(interfaceOrientation)?frame.size.height:frame.size.width) - TKToolbarHeightForOrientation(orient) - self.navigationController.navigationBar.height;
    
    _toolbar.height = TKToolbarHeightForOrientation(orient);
    _webView.height = height;
    _toolbar.top = _webView.height;
    _spinner.frame = CGRectMake(_webView.frame.size.width/2 - 10, _webView.frame.size.height/2 - 10, 20.0f, 20.0f);
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIViewController


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)loadView {
    [super loadView];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector(dismiss)];
    
    CGRect frame = [UIScreen mainScreen].applicationFrame;
    
    UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
    self.view.frame = CGRectMake(0, 0, frame.size.width, frame.size.height - TKToolbarHeightForOrientation(orient));
    self.view.backgroundColor = [UIColor redColor];
    
    frame = CGRectMake(0, 0, frame.size.width, frame.size.height - TKToolbarHeightForOrientation(orient)*2);
    
    _webView = [[UIWebView alloc] initWithFrame:frame];
    _webView.delegate = self;
    _webView.autoresizingMask = UIViewAutoresizingFlexibleWidth
    | UIViewAutoresizingFlexibleHeight;
    _webView.scalesPageToFit = YES;
    [self.view addSubview:_webView];
    
    _spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    _spinner.frame = CGRectMake(_webView.frame.size.width/2 - 10, _webView.frame.size.height/2 - 10, 20.0f, 20.0f);
    [_webView addSubview:_spinner];
    
    _backButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/WebView/WebView_Backward.png"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(backAction)];
    _backButton.tag = 2;
    _backButton.enabled = NO;
    
    _forwardButton =
    [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/WebView/WebView_Forward.png"]
                                     style:UIBarButtonItemStylePlain
                                    target:self
                                    action:@selector(forwardAction)];
    _forwardButton.tag = 1;
    _forwardButton.enabled = NO;
    
    _refreshButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/WebView/WebView_Refresh.png"]
                                                    style:UIBarButtonItemStylePlain
                                                     target:self 
                                                     action:@selector(refreshAction)];
    _refreshButton.tag = 3;
    
    _stopButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/WebView/WebView_Stop.png"]
                                                   style:UIBarButtonItemStylePlain
                                                  target:self 
                                                  action:@selector(stopAction)];
    _stopButton.tag = 3;
//    _actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:
//                     UIBarButtonSystemItemAction target:self action:@selector(shareAction)];
    
    UIBarItem* space = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:
                         UIBarButtonSystemItemFlexibleSpace target:nil action:nil] autorelease];
    
    _toolbar = [[UIToolbar alloc] initWithFrame:
                CGRectMake(0, self.view.height - TKToolbarHeightForOrientation(orient),
                           self.view.width, TKToolbarHeightForOrientation(orient))];
    _toolbar.autoresizingMask =
    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    _toolbar.tintColor = [UIColor blackColor];
    _toolbar.items = [NSArray arrayWithObjects:
                      space,
                      _backButton,
                      space,
                      _forwardButton,
                      space,
                      _refreshButton,
                      space,
                      nil];
    [self.view addSubview:_toolbar];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewDidUnload {
    [super viewDidUnload];
    
    _delegate = nil;
    _webView.delegate = nil;
    
    TKRELEASE(_webView);
    TKRELEASE(_toolbar);
    TKRELEASE(_backButton);
    TKRELEASE(_forwardButton);
    TKRELEASE(_refreshButton);
    TKRELEASE(_stopButton);
    TKRELEASE(_actionButton);
    TKRELEASE(_spinner);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
//    [self updateToolbarWithOrientation:self.interfaceOrientation];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)viewWillDisappear:(BOOL)animated {
    // If the browser launched the media player, it steals the key window and never gives it
    // back, so this is a way to try and fix that
    [self.view.window makeKeyWindow];
    
    [super viewWillDisappear:animated];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//    return TTIsSupportedOrientation(interfaceOrientation);
    return NO;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
                                         duration:(NSTimeInterval)duration {
    [super willAnimateRotationToInterfaceOrientation:toInterfaceOrientation duration:duration];
    [self updateToolbarWithOrientation:toInterfaceOrientation];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView*)rotatingFooterView {
    return _toolbar;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UTViewController (TKCategory)


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)persistView:(NSMutableDictionary*)state {
    NSString* URL = self.URL.absoluteString;
    if (URL.length && ![URL isEqualToString:@"about:blank"]) {
        [state setObject:URL forKey:@"URL"];
        return YES;
        
    } else {
        return NO;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)restoreView:(NSDictionary*)state {
    NSString* URL = [state objectForKey:@"URL"];
    if (URL.length && ![URL isEqualToString:@"about:blank"]) {
        [self openURL:[NSURL URLWithString:URL]];
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIWebViewDelegate


///////////////////////////////////////////////////////////////////////////////////////////////////
- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request
 navigationType:(UIWebViewNavigationType)navigationType {
    if ([_delegate respondsToSelector:
         @selector(webController:webView:shouldStartLoadWithRequest:navigationType:)] &&
        ![_delegate webController:self webView:webView
       shouldStartLoadWithRequest:request navigationType:navigationType]) {
            return NO;
        }
    
    [_loadingURL release];
    _loadingURL = [request.URL retain];
    _backButton.enabled = [_webView canGoBack];
    _forwardButton.enabled = [_webView canGoForward];
    return YES;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidStartLoad:(UIWebView*)webView {
    if ([_delegate respondsToSelector:@selector(webController:webViewDidStartLoad:)]) {
        [_delegate webController:self webViewDidStartLoad:webView];
    }
    
    self.navigationItem.titleView = [self titleViewWithTitle:NSLocalizedString(@"加载中...", @"")];
    [_spinner startAnimating];
    [_toolbar replaceItemWithTag:3 withItem:_stopButton];
    _backButton.enabled = [_webView canGoBack];
    _forwardButton.enabled = [_webView canGoForward];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webViewDidFinishLoad:(UIWebView*)webView {
    if ([_delegate respondsToSelector:@selector(webController:webViewDidFinishLoad:)]) {
        [_delegate webController:self webViewDidFinishLoad:webView];
    }
    
    TKRELEASE(_loadingURL);
    NSString *title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.titleView = [self titleViewWithTitle:title];
    [_spinner stopAnimating];
    [_toolbar replaceItemWithTag:3 withItem:_refreshButton];
    
    _backButton.enabled = [_webView canGoBack];
    _forwardButton.enabled = [_webView canGoForward];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    if ([_delegate respondsToSelector:@selector(webController:webView:didFailLoadWithError:)]) {
        [_delegate webController:self webView:webView didFailLoadWithError:error];
    }
    
    TKRELEASE(_loadingURL);
    [self webViewDidFinishLoad:webView];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIActionSheetDelegate


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)actionSheet:(UIActionSheet*)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        [[UIApplication sharedApplication] openURL:self.URL];
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    TKRELEASE(_actionSheet);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSURL*)URL {
    return _loadingURL ? _loadingURL : _webView.request.URL;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setHeaderView:(UIView*)headerView {
    if (headerView != _headerView) {
        BOOL addingHeader = !_headerView && headerView;
        BOOL removingHeader = _headerView && !headerView;
        
        [_headerView removeFromSuperview];
        [_headerView release];
        _headerView = [headerView retain];
        _headerView.frame = CGRectMake(0, 0, _webView.width, _headerView.height);
        
        [self view];
        UIView* scroller = [_webView descendantOrSelfWithClass:NSClassFromString(@"UIScroller")];
        UIView* docView = [scroller descendantOrSelfWithClass:NSClassFromString(@"UIWebDocumentView")];
        [scroller addSubview:_headerView];
        
        if (addingHeader) {
            docView.top += headerView.height;
            docView.height -= headerView.height;
            
        } else if (removingHeader) {
            docView.top -= headerView.height;
            docView.height += headerView.height;
        }
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)openURL:(NSURL*)URL {
    NSMutableURLRequest* request = [NSMutableURLRequest requestWithURL:URL];
    [self openRequest:request];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)openRequest:(NSURLRequest*)request {
    [self view];
    [_webView loadRequest:request];
}

@end
