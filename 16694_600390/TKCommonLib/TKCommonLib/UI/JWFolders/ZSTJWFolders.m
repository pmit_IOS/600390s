/*
 Copyright (c) 2011, Jonathan Willing
 All rights reserved.
 Licensed under the BSD License.
 http://www.opensource.org/licenses/bsd-license
 Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met: Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import "ZSTJWFolders.h"
#import <QuartzCore/QuartzCore.h>
#import "UIViewAdditions.h"
#import "UIScreenAdditions.h"
#import <QuartzCore/CALayer.h>

@interface ZSTJWFolders ()
- (JWFolderSplitView *)buttonForRect:(CGRect)aRect andScreen:(UIImage *)screen top:(BOOL)isTop position:(CGPoint)position;
- (void)openFolderWithContentView:(UIView *)view
                         position:(CGPoint)position
                    containerView:(UIView *)containerView
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock;
@property (nonatomic, retain) JWFolderSplitView *top;
@property (nonatomic, retain) JWFolderSplitView *bottom;
@property (nonatomic, assign) CGPoint topPoint;
@property (nonatomic, assign) CGPoint bottomPoint;
@property (nonatomic, retain) UIView *contentView;
@property (nonatomic, copy) JWFoldersCompletionBlock completionBlock;
@property (nonatomic, copy) JWFoldersCloseBlock closeBlock;
@property (nonatomic, copy) JWFoldersOpenBlock openBlock;
@property (nonatomic, retain) UIImageView *notch;
@property (nonatomic, retain) UIImageView *convex;
@property (nonatomic, retain) UIActivityIndicatorView *activity;

@end


@implementation ZSTJWFolders

@synthesize top = _top;
@synthesize bottom = _bottom;
@synthesize topPoint = _folderPoint;
@synthesize bottomPoint = _bottomPoint;
@synthesize contentView = _contentView;
@synthesize completionBlock = _completionBlock;
@synthesize closeBlock = _closeBlock;
@synthesize openBlock = _openBlock;
@synthesize notch = _notch;
@synthesize convex = _convex;
@synthesize activity = _activity;


- (void)openFolderWithContentViewController:(UIViewController *)viewController
                                   position:(CGPoint)position
                              containerView:(UIView *)containerView {
    [self openFolderWithContentView:viewController.view
                           position:position
                      containerView:containerView
                          openBlock:nil
                         closeBlock:nil
                    completionBlock:nil];
}



- (id)init
{
    self = [super init];
    if (self) {
        self.convex = [[[UIImageView alloc] init] autorelease];
        self.notch = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/JWFolders/JWFolders_notch.png"]];
    }
    return self;
}


- (void)openFolderWithContentView:(UIView *)contentView
                         position:(CGPoint)position
                    containerView:(UIView *)containerView
                        openBlock:(JWFoldersOpenBlock)openBlock
{
    
    [self retain];
    
    self.contentView = contentView;
    self.openBlock = openBlock;
    
    UIImage *screenshot = [containerView screenshot];
    CGFloat width = containerView.frame.size.width;
    CGFloat height = containerView.frame.size.height;
    
    CGFloat offset = 0.f;
    if ([containerView isKindOfClass:[UIScrollView class]]) {
        offset = ((UIScrollView *)containerView).contentOffset.y;
    }
    
    CGRect upperRect = CGRectMake(0, 0, width, position.y>0?position.y:0);
    CGRect lowerRect = CGRectMake(0, position.y, width, (height - position.y) > 0?height - position.y:0);
    
    self.top = [self buttonForRect:upperRect andScreen:screenshot top:YES position:position];
    self.top.frame = CGRectMake(upperRect.origin.x, upperRect.origin.y + offset, upperRect.size.width, upperRect.size.height);
    self.bottom = [self buttonForRect:lowerRect andScreen:screenshot top:NO position:position];
    self.bottom.frame = CGRectMake(lowerRect.origin.x, lowerRect.origin.y + offset, lowerRect.size.width, lowerRect.size.height);
    
    //    self.convex.alpha = 0;
    //    self.convex.image = [self convexForPosition:position andScreen:screenshot];
    //    self.convex.frame = CGRectMake(position.x - 7.5, - 8, 15, 8);
    //    [self.bottom addSubview:self.convex];
    
    CGRect viewFrame = self.contentView.frame;
    if (viewFrame.size.height > height) {
        viewFrame.size.height = height;
    }
    CGFloat p = position.y + viewFrame.size.height;
    if (p > height) {
        p = height;
        if(p < position.y && position.y < height)
        {
            p = position.y;
        }
    }
    
    self.contentView.frame = CGRectMake(0, p + offset - viewFrame.size.height - viewFrame.size.height/2, width, viewFrame.size.height);
    self.activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    self.activity.frame = CGRectMake((self.contentView.frame.size.width-15)/2, (self.contentView.frame.size.height-15)/2, 15, 15);
    [self.contentView addSubview:self.activity];
    
    [containerView addSubview:self.contentView];
    [containerView addSubview:self.top];
    [containerView addSubview:self.bottom];
    [self.activity startAnimating];
    
    CFTimeInterval duration = 0.4f;
    CAMediaTimingFunction *timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    self.topPoint = self.top.layer.position;
    CGPoint toPoint = CGPointMake(self.topPoint.x, self.topPoint.y - (position.y - self.contentView.frame.origin.y) - offset);
    CABasicAnimation *moveUp = [CABasicAnimation animationWithKeyPath:@"position"];
    [moveUp setTimingFunction:timingFunction];
    moveUp.fromValue = [NSValue valueWithCGPoint:self.topPoint];
    moveUp.toValue = [NSValue valueWithCGPoint:toPoint];
    moveUp.duration = duration;
    
    [self.top.layer addAnimation:moveUp forKey:nil];
    self.top.layer.position = toPoint;
    
    self.bottomPoint = self.bottom.layer.position;
    toPoint = CGPointMake(self.bottomPoint.x, self.bottom.layer.position.y + (self.contentView.frame.origin.y + self.contentView.frame.size.height - position.y) - offset);
    CABasicAnimation *moveDown = [CABasicAnimation animationWithKeyPath:@"position"];
    [moveDown setValue:@"moveUP" forKey:@"animationType"];
    
    moveDown.delegate = self;
    [moveDown setTimingFunction:timingFunction];
    moveDown.fromValue = [NSValue valueWithCGPoint:self.bottomPoint];
    moveDown.toValue = [NSValue valueWithCGPoint:toPoint];
    moveDown.duration = duration;
    
    [self.bottom.layer addAnimation:moveDown forKey:nil];
    self.bottom.layer.position = toPoint;
}

- (CGPathRef)newPathWithConvexPath:(CGRect)rect {
	//
	// Create the boundary path
	//
	CGMutablePathRef path = CGPathCreateMutable();
	CGPathMoveToPoint(path, NULL,
					  CGRectGetMidX(rect),
					  CGRectGetMinY(rect));
    
    CGPathAddLineToPoint(path, NULL, CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGPathAddLineToPoint(path, NULL, CGRectGetMidX(rect), CGRectGetMinY(rect));
	
	// Close the path
	CGPathCloseSubpath(path);
	
	return path;
}

- (UIImage *)convexForPosition:(CGPoint)position andScreen:(UIImage *)screen {
    CGFloat scale = [UIScreen screenScale];
    CGRect rect = CGRectMake((position.x - 7.5)*scale, (position.y - 8)*scale, 15*scale, 8*scale);
    CGImageRef imageRef = CGImageCreateWithImageInRect(screen.CGImage, rect);
    UIImage *img = [UIImage imageWithCGImage:imageRef scale:scale orientation:UIImageOrientationUp];
    CGImageRelease(imageRef);
    
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(rect.size, NO, scale);
    } else {
        UIGraphicsBeginImageContext(rect.size);
    }
    CGContextRef context = UIGraphicsGetCurrentContext();
	CGPathRef convexPath = [self newPathWithConvexPath:CGRectMake(0, 0, rect.size.width, rect.size.height)];
	CGContextAddPath(context, convexPath);
	CGContextClip(context);
    CGPathRelease(convexPath);
    
	[img drawInRect:CGRectMake(0, 0, rect.size.width, rect.size.height)];
	UIImage * resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}


- (void)openFolderWithContentView:(UIView *)contentView
                         position:(CGPoint)position
                    containerView:(UIView *)containerView
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock {
    
    [self retain];
    
    self.contentView = contentView;
    self.openBlock = openBlock;
    self.closeBlock = closeBlock;
    self.completionBlock = completionBlock;
    
    UIImage *screenshot = [containerView screenshot];
    CGFloat width = containerView.frame.size.width;
    CGFloat height = containerView.frame.size.height;
    
    CGFloat offset = 0.f;
    if ([containerView isKindOfClass:[UIScrollView class]]) {
        offset = ((UIScrollView *)containerView).contentOffset.y;
    }
    
    CGRect upperRect = CGRectMake(0, 0, width, position.y>0?position.y:0);
    CGRect lowerRect = CGRectMake(0, position.y, width, (height - position.y) > 0?height - position.y:0);
    
    self.top = [self buttonForRect:upperRect andScreen:screenshot top:YES position:position];
    self.top.frame = CGRectMake(upperRect.origin.x, upperRect.origin.y + offset, upperRect.size.width, upperRect.size.height);
    self.bottom = [self buttonForRect:lowerRect andScreen:screenshot top:NO position:position];
    self.bottom.frame = CGRectMake(lowerRect.origin.x, lowerRect.origin.y + offset, lowerRect.size.width, lowerRect.size.height);
    
    [self.top addTarget:self action:@selector(folderWillClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottom addTarget:self action:@selector(folderWillClose:) forControlEvents:UIControlEventTouchUpInside];
    
    //Todo: Create a "notch", similar to SpringBoard's folders
    //UIImageView *notch = nil;
    //notch.center = CGPointMake(position.x, position.y + 7.0);
    self.notch.frame = CGRectMake(position.x - 7.5, position.y - 8, 15, 8);  //顶部黑色箭头
    [self.top addSubview:self.notch];
    
    self.convex.alpha = 0;
    self.convex.image = [self convexForPosition:position andScreen:screenshot];
    self.convex.frame = CGRectMake(position.x - 7.5, - 8, 15, 8);
    [self.bottom addSubview:self.convex];                                    //底部白箭头
    
    CGRect viewFrame = self.contentView.frame;
    if (viewFrame.size.height > height) {
        viewFrame.size.height = height;
    }
    CGFloat p = position.y + viewFrame.size.height;
    if (p > height) {
        p = height;
        if(p < position.y && position.y < height)
        {
            p = position.y;
        }
    }
    self.contentView.frame = CGRectMake(0, p + offset - viewFrame.size.height, width, viewFrame.size.height);
    
    [containerView addSubview:self.contentView];
    [containerView addSubview:self.top];
    [containerView addSubview:self.bottom];
    
    CFTimeInterval duration = 0.4f;
    CAMediaTimingFunction *timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    self.topPoint = self.top.layer.position;
    CGPoint toPoint = CGPointMake(self.topPoint.x, self.topPoint.y - (position.y - self.contentView.frame.origin.y) - offset);
    CABasicAnimation *moveUp = [CABasicAnimation animationWithKeyPath:@"position"];
    [moveUp setTimingFunction:timingFunction];
    moveUp.fromValue = [NSValue valueWithCGPoint:self.topPoint];
    moveUp.toValue = [NSValue valueWithCGPoint:toPoint];
    moveUp.duration = duration;
    
    [self.top.layer addAnimation:moveUp forKey:nil];
    self.top.layer.position = toPoint;
    
    self.bottomPoint = self.bottom.layer.position;
    toPoint = CGPointMake(self.bottomPoint.x, self.bottom.layer.position.y + (self.contentView.frame.origin.y + self.contentView.frame.size.height - position.y) - offset);
    CABasicAnimation *moveDown = [CABasicAnimation animationWithKeyPath:@"position"];
    [moveDown setTimingFunction:timingFunction];
    moveDown.fromValue = [NSValue valueWithCGPoint:self.bottomPoint];
    moveDown.toValue = [NSValue valueWithCGPoint:toPoint];
    moveDown.duration = duration;
    
    [self.bottom.layer addAnimation:moveDown forKey:nil];
    self.bottom.layer.position = toPoint;
    
    if (openBlock) openBlock(self.contentView, duration, timingFunction);
    
}

- (void)openFollowWithContentView:(UIView *)contentView
                         position:(CGPoint)position
                    containerView:(UIView *)containerView
                        openBlock:(JWFoldersOpenBlock)openBlock
                       closeBlock:(JWFoldersCloseBlock)closeBlock
                  completionBlock:(JWFoldersCompletionBlock)completionBlock {
    
    [self retain];
    
    self.contentView = contentView;
    self.openBlock = openBlock;
    self.closeBlock = closeBlock;
    self.completionBlock = completionBlock;
    
    UIImage *screenshot = [containerView screenshot];
    CGFloat width = containerView.frame.size.width;
    CGFloat height = containerView.frame.size.height;
    
    CGFloat offset = 0.f;
    if ([containerView isKindOfClass:[UIScrollView class]]) {
        offset = ((UIScrollView *)containerView).contentOffset.y;
    }
    
    CGRect upperRect = CGRectMake(0, 0, width, position.y>0?position.y:0);
    CGRect lowerRect = CGRectMake(0, position.y, width, (height - position.y) > 0?height - position.y:0);
    
    self.top = [self buttonForRect:upperRect andScreen:screenshot top:YES position:position];
    self.top.frame = CGRectMake(upperRect.origin.x, upperRect.origin.y + offset, upperRect.size.width, upperRect.size.height);
    self.bottom = [self buttonForRect:lowerRect andScreen:screenshot top:NO position:position];
    self.bottom.frame = CGRectMake(lowerRect.origin.x, lowerRect.origin.y + offset, lowerRect.size.width, lowerRect.size.height);
    
    [self.top addTarget:self action:@selector(folderWillClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.bottom addTarget:self action:@selector(folderWillClose:) forControlEvents:UIControlEventTouchUpInside];
    
    //Todo: Create a "notch", similar to SpringBoard's folders
    //UIImageView *notch = nil;
    //notch.center = CGPointMake(position.x, position.y + 7.0);
    self.notch.frame = CGRectMake(position.x - 7.5, position.y - 8, 15, 8);  //顶部黑色箭头
    //    [self.top addSubview:self.notch];
    
    self.convex.alpha = 0;
    self.convex.image = [self convexForPosition:position andScreen:screenshot];
    self.convex.frame = CGRectMake(position.x - 7.5, - 8, 15, 8);
    //    [self.bottom addSubview:self.convex];                                    //底部白箭头
    
    CGRect viewFrame = self.contentView.frame;
    if (viewFrame.size.height > height) {
        viewFrame.size.height = height;
    }
    CGFloat p = position.y + viewFrame.size.height;
    if (p > height) {
        p = height;
        if(p < position.y && position.y < height)
        {
            p = position.y;
        }
    }
    self.contentView.frame = CGRectMake(0, p + offset - viewFrame.size.height, width, viewFrame.size.height);
    
    [containerView addSubview:self.contentView];
    [containerView addSubview:self.top];
    [containerView addSubview:self.bottom];
    
    CFTimeInterval duration = 0.4f;
    CAMediaTimingFunction *timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    self.topPoint = self.top.layer.position;
    CGPoint toPoint = CGPointMake(self.topPoint.x, self.topPoint.y - (position.y - self.contentView.frame.origin.y) - offset);
    CABasicAnimation *moveUp = [CABasicAnimation animationWithKeyPath:@"position"];
    [moveUp setTimingFunction:timingFunction];
    moveUp.fromValue = [NSValue valueWithCGPoint:self.topPoint];
    moveUp.toValue = [NSValue valueWithCGPoint:toPoint];
    moveUp.duration = duration;
    
    [self.top.layer addAnimation:moveUp forKey:nil];
    self.top.layer.position = toPoint;
    
    self.bottomPoint = self.bottom.layer.position;
    toPoint = CGPointMake(self.bottomPoint.x, self.bottom.layer.position.y + (self.contentView.frame.origin.y + self.contentView.frame.size.height - position.y) - offset);
    CABasicAnimation *moveDown = [CABasicAnimation animationWithKeyPath:@"position"];
    [moveDown setTimingFunction:timingFunction];
    moveDown.fromValue = [NSValue valueWithCGPoint:self.bottomPoint];
    moveDown.toValue = [NSValue valueWithCGPoint:toPoint];
    moveDown.duration = duration;
    
    [self.bottom.layer addAnimation:moveDown forKey:nil];
    self.bottom.layer.position = toPoint;
    
    if (openBlock) openBlock(self.contentView, duration, timingFunction);
    
}


- (void)folderWillClose:(id)sender {
    CFTimeInterval duration = 0.4f;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:duration];
    self.convex.alpha = 1;
    [UIView commitAnimations];
    
    CAMediaTimingFunction *timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
    CABasicAnimation *upClose = [CABasicAnimation animationWithKeyPath:@"position"];
    [upClose setValue:@"moveDown" forKey:@"animationType"];
    [upClose setDelegate:self];
    [upClose setTimingFunction:timingFunction];
    upClose.fromValue = [NSValue valueWithCGPoint:[((JWFolderSplitView *)[self.top.layer presentationLayer]) position]];
    upClose.toValue = [NSValue valueWithCGPoint:self.topPoint];
    upClose.duration = 0.4f;
    [self.top.layer addAnimation:upClose forKey:nil];
    self.top.layer.position = self.topPoint;
    
    CABasicAnimation *downClose = [CABasicAnimation animationWithKeyPath:@"position"];
    [downClose setTimingFunction:timingFunction];
    downClose.fromValue = [NSValue valueWithCGPoint:[((JWFolderSplitView *)[self.bottom.layer presentationLayer]) position]];
    downClose.toValue = [NSValue valueWithCGPoint:self.bottomPoint];
    downClose.duration = 0.4f;
    [self.bottom.layer addAnimation:downClose forKey:nil];
    self.bottom.layer.position = self.bottomPoint;
    
    if (self.closeBlock) self.closeBlock(self.contentView, duration, timingFunction);
    
    [self release];
}

// Using delegate callbacks instead of blocks in this case to avoid something terrible
- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag {
    
    if ([[anim valueForKey:@"animationType"] isEqualToString:@"moveDown"]) {
        [self.top removeFromSuperview];
        [self.bottom removeFromSuperview];
        [self.contentView removeFromSuperview];
        self.convex = nil;
        self.top = nil;
        self.bottom = nil;
        self.contentView = nil;
        
        if (self.completionBlock) self.completionBlock();
    }else if([[anim valueForKey:@"animationType"] isEqualToString:@"moveUP"])
    {
        if (self.openBlock){
            self.openBlock(nil, 0.4f, nil);
        }
    }
}

- (void)dealloc
{
    self.convex = nil;
    self.notch = nil;
    self.top = nil;
    self.bottom = nil;
    self.contentView = nil;
    [super dealloc];
}

- (JWFolderSplitView *)buttonForRect:(CGRect)aRect andScreen:(UIImage *)screen top:(BOOL)isTop position:(CGPoint)position {
    CGFloat scale = [UIScreen screenScale];
    CGFloat width = aRect.size.width;
    CGFloat height = aRect.size.height;
    CGPoint origin = aRect.origin;
    
    CGRect r1 = CGRectMake(origin.x*scale, origin.y*scale, width*scale, height*scale);
    CGRect u1 = CGRectMake(origin.x, origin.y, width, height);
    CGImageRef ref1 = CGImageCreateWithImageInRect([screen CGImage], r1);
    UIImage *img = [UIImage imageWithCGImage:ref1 scale: scale orientation: UIImageOrientationUp];
    CGImageRelease(ref1);
    
    JWFolderSplitView *b1 = [[JWFolderSplitView alloc] initWithFrame:u1];
    b1.isTop = isTop;
    b1.position = position;
    [b1 setBackgroundColor:[UIColor colorWithPatternImage:img]];
    return [b1 autorelease];
}



- (void)closeCurrentFolder {
    [self folderWillClose:nil];
    if (self.activity != nil) {
        [self.activity stopAnimating];
    }
}

@end

@implementation JWFolderSplitView
@synthesize isTop, position;

- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextSetRGBFillColor(ctx, 1, 1, 1, 0.2); //light color
    if (self.isTop)
        CGContextFillRect(ctx, CGRectMake(0, rect.size.height-1, rect.size.width, 1));
    else
        CGContextFillRect(ctx, CGRectMake(0, 0, rect.size.width, 1));
    [super drawRect:rect];
}

@end
