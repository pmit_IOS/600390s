//
//  HHCellBackgroundView.h
//  petmii
//
//  Created by luobin on 5/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum  {
    CustomCellBackgroundViewPositionNone,
    CustomCellBackgroundViewPositionTopNone,
    CustomCellBackgroundViewPositionBottomNone,
    CustomCellBackgroundViewPositionTop, 
    CustomCellBackgroundViewPositionMiddle, 
    CustomCellBackgroundViewPositionBottom,
    CustomCellBackgroundViewPositionSingle
} CustomCellBackgroundViewPosition;

@interface TKCellBackgroundView : UIView {
    UIColor *borderColor;
    UIColor *fillColor;
    CustomCellBackgroundViewPosition position;
}

@property(nonatomic, retain) UIColor *borderColor, *fillColor, *shadowColor;
@property(nonatomic) CustomCellBackgroundViewPosition position;
@end
