//
//  TKLoadMoreView.m
//  TKCommonLib
//
//  Created by luo bin on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "TKLoadMoreView.h"

@implementation TKLoadMoreView

@synthesize delegate = _delegate;
@synthesize activityView = _activityView;

#pragma mark -
#pragma mark Setters

- (void)setState:(TKLoadMoreViewState)aState{
    
    switch (aState) {
        case TKLoadMoreViewStateNormal:
            
            _statusLabel.text = NSLocalizedString(@"上拉加载更多...", @"上拉加载更多...");
            [_activityView stopAnimating];
            
            break;
        case TKLoadMoreViewStateLoading:
            
            _statusLabel.text = NSLocalizedString(@"加载中...", @"加载中...");
            [_activityView startAnimating];
            
            break;
            
        case TKLoadMoreViewStateFinish:
            _statusLabel.text = @"没有数据了...";
            [_activityView stopAnimating];
        default:
            break;
    }
    
    _state = aState;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        self.backgroundColor = [UIColor whiteColor];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 18, self.frame.size.width, 20.0f)];
        label.autoresizingMask = UIViewAutoresizingFlexibleWidth;
        label.font = [UIFont boldSystemFontOfSize:13.0f];
        label.textColor = [UIColor colorWithRed:124.0/255.0 green:124.0/255.0 blue:124.0/255.0 alpha:1.0];
        //label.shadowColor = [UIColor colorWithWhite:0.9f alpha:1.0f];
        label.shadowOffset = CGSizeMake(0.0f, 1.0f);
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        [self addSubview:label];
        _statusLabel=label;
        [label release];
        
        UIActivityIndicatorView *view = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        view.frame = CGRectMake(110.0f, 18.0f, 20.0f, 20.0f);
        view.activityIndicatorViewStyle = UIActivityIndicatorViewStyleGray;
        [self addSubview:view];
        _activityView = view;
        [view release];
        
        [self setState:TKLoadMoreViewStateNormal];
            
        _hasTriggerRefresh = NO;
    }
    return self;
}

#pragma mark -
#pragma mark ScrollView Methods

- (void)loadMoreScrollViewDidScroll:(UIScrollView *)scrollView {    
    
    if (!self.hidden && _state != TKLoadMoreViewStateLoading) {
        
        BOOL _loading = NO;
        if ([_delegate respondsToSelector:@selector(loadMoreDataSourceIsLoading:)]) {
            _loading = [_delegate loadMoreDataSourceIsLoading:self];
        }
        
        if (!_loading && !_hasTriggerRefresh && _state == TKLoadMoreViewStateNormal 
            && scrollView.contentOffset.y + (scrollView.frame.size.height) > scrollView.contentSize.height - self.bounds.size.height - 20) {
            
            _hasTriggerRefresh = YES;
            [self setState:TKLoadMoreViewStateLoading];
            
            if ([_delegate respondsToSelector:@selector(loadMoreDidTriggerRefresh:)]) {
                [_delegate performSelector:@selector(loadMoreDidTriggerRefresh:) withObject:self afterDelay:0];
            }
        }
        
        if (scrollView.contentInset.top != 0) {
//            scrollView.contentInset = UIEdgeInsetsZero;
        }
        
        if (!_loading && _hasTriggerRefresh && _state == TKLoadMoreViewStateNormal 
            && scrollView.contentOffset.y + (scrollView.frame.size.height) < scrollView.contentSize.height - self.bounds.size.height - 20) {
            
            _hasTriggerRefresh = NO;
        }
    }
}

- (void)loadMoreScrollViewDidEndDragging:(UIScrollView *)scrollView {
    
    BOOL _loading = NO;
    if ([_delegate respondsToSelector:@selector(loadMoreDataSourceIsLoading:)]) {
        _loading = [_delegate loadMoreDataSourceIsLoading:self];
    }
    
    if (scrollView.contentOffset.y + (scrollView.frame.size.height) > scrollView.contentSize.height + 1 && !_loading) {
        [self setState:TKLoadMoreViewStateLoading];
        if ([_delegate respondsToSelector:@selector(loadMoreDidTriggerRefresh:)]) {
            [_delegate performSelector:@selector(loadMoreDidTriggerRefresh:) withObject:self afterDelay:0];
        }
    }
}

- (void)loadMoreTriggerRefresh
{
    if (!self.hidden && _state != TKLoadMoreViewStateLoading) {
        
        BOOL _loading = NO;
        if ([_delegate respondsToSelector:@selector(loadMoreDataSourceIsLoading:)]) {
            _loading = [_delegate loadMoreDataSourceIsLoading:self];
        }
        
        _hasTriggerRefresh = YES;
        [self setState:TKLoadMoreViewStateLoading];
        
        if ([_delegate respondsToSelector:@selector(loadMoreDidTriggerRefresh:)]) {
            
            [_delegate performSelector:@selector(loadMoreDidTriggerRefresh:) withObject:self afterDelay:0];
        }
        
        _hasTriggerRefresh = NO;
    }
}
- (void)loadMoreScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView{
    
    if (!self.hidden) {
        [self setState:TKLoadMoreViewStateNormal];
    }
}

- (void)loadMoreScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView state:(TKLoadMoreViewState)state{
    
    if (!self.hidden) {
        [self setState:state];
    }
}

- (void)dealloc
{
    _delegate=nil;
    _activityView = nil;
    _statusLabel = nil;
    [super dealloc];
}
@end
