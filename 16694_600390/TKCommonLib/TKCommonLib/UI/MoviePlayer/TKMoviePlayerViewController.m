//
//  TKMoviePlayerViewController.m
//  VideoTest
//
//  Created by luobin on 12-9-20.
//  Copyright (c) 2012年 luobin. All rights reserved.
//

#import "TKMoviePlayerViewController.h"
#import "TKTransportControls.h"

#define kAutoDismissOverlayInterval 10

@interface TKMoviePlayerViewController ()

@property(nonatomic, retain ,readwrite) NSURL *url;
@property(nonatomic, retain ,readwrite) UIImageView *imgPreviewImage;
@property(nonatomic, retain ,readwrite) TKTransportControls *transportControls;
@property(retain ,readwrite) MPMoviePlayerController *moviePlayer;

@property (nonatomic, retain) NSTimer *timer;
@property(assign) BOOL isSlideOuted;

-(void)setMoviePlayerUserSettings;
- (void)resetTimer;
- (void)toggleOverlay;

@end

@implementation TKMoviePlayerViewController

@synthesize url;

@synthesize moviePlayer;
@synthesize imgPreviewImage;
@synthesize transportControls;

@synthesize timer;
@synthesize isSlideOuted;

- (void)dealloc
{
    [self.timer invalidate];
    self.timer = nil;
    
    self.url = nil;
    self.imgPreviewImage = nil;
    self.transportControls.moviePlayer = nil;
    self.transportControls = nil;
    self.moviePlayer = nil;
    [super dealloc];
}

-(id)initWithContentURL:(NSURL *)theUrl;
{
	self = [super init];
    if (self) {
        self.wantsFullScreenLayout = YES;
        self.url = theUrl;
    }
	return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [self resetTimer];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [self.timer invalidate];
    self.timer = nil;
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleDefault];
    
    [TKUIUtil hiddenHUD];
}

- (void)resetTimer
{
    if (self.moviePlayer.isPreparedToPlay) {
        [self.timer invalidate];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:kAutoDismissOverlayInterval target:self selector:@selector(toggleOverlay) userInfo:nil repeats:NO];
    }
}

- (void)eraseTimer
{
    if (self.moviePlayer.isPreparedToPlay) {
        
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)toggleOverlay
{
    //直播中才能切换Overlay
//    if (!_isRecording) {
//        return;
//    }
    if (self.isSlideOuted) {
        self.isSlideOuted = NO;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.transportControls.alpha = 1;
        [UIView commitAnimations];
        
        [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        
        [self resetTimer];
    } else {
        [self.timer invalidate];
        self.timer = nil;
        self.isSlideOuted = YES;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];

        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationBeginsFromCurrentState:YES];
        self.transportControls.alpha = 0;
        [UIView commitAnimations];
        
        
        [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationFade];
        [self.navigationController setNavigationBarHidden:YES animated:YES];
        
        [UIView commitAnimations];
    }
}

#pragma mark - View lifecycle
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.autoresizesSubviews = YES;
    self.view.backgroundColor = [UIColor clearColor];
    
    //add video to subview
    MPMoviePlayerController *mp = [[MPMoviePlayerController alloc] initWithContentURL:self.url];
	if (mp)
	{
		// save the movie player object
		self.moviePlayer = mp;
		[mp release];
		
        //get movie preview image so there will be no blink when the movie starts
//        [self performSelector:@selector(setTotalVideoTimeDuration) withObject:nil afterDelay:0.1];
//        imgPreviewImage.image = [self.moviePlayer thumbnailImageAtTime:0.0 timeOption:MPMovieTimeOptionNearestKeyFrame];
        imgPreviewImage.hidden = YES;
        
		// Apply the user specified settings to the movie player object
		[self setMoviePlayerUserSettings];
        self.moviePlayer.view.hidden = NO;
        self.moviePlayer.shouldAutoplay = NO;
        moviePlayer.view.frame = self.view.bounds;
        moviePlayer.view.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
//		self.moviePlayer.view.backgroundColor = [UIColor whiteColor];
        
        [self.view addSubview:moviePlayer.view];
    }
    
    self.transportControls = [[[TKTransportControls alloc] initWithFrame:CGRectMake(0, self.view.bounds.size.height - 42, self.view.bounds.size.width, 42)] autorelease];
    self.transportControls.moviePlayer = mp;
    self.transportControls.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    [self.view addSubview:self.transportControls];
    
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackIsPreparedToPlayDidChangeNotification:) name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:nil];
    [self.transportControls playMovie];
    
    UIView *gestureRecognizerView = [[UIView alloc] initWithFrame:mp.view.bounds];
    gestureRecognizerView.backgroundColor = [UIColor clearColor];
    gestureRecognizerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleOverlay)];
    [gestureRecognizerView addGestureRecognizer:tapGestureRecognizer];
    [tapGestureRecognizer release];
    [self.moviePlayer.view addSubview:gestureRecognizerView];
    
    [self resetTimer];
}

- (void)moviePlaybackIsPreparedToPlayDidChangeNotification:(NSNotification *)notification
{
    if (notification.object == self.moviePlayer && self.moviePlayer.isPreparedToPlay) {
        [TKUIUtil hiddenHUD];
    }
}

- (void)stop
{
    [self.moviePlayer stop];
}


-(void)setMoviePlayerUserSettings
{    
    /* 
     Movie scaling mode can be one of: MPMovieScalingModeNone, MPMovieScalingModeAspectFit,
     MPMovieScalingModeAspectFill, MPMovieScalingModeFill.
     */
    self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    
    //Dont show movie controls
    self.moviePlayer.controlStyle = MPMovieControlStyleNone;
    /*
     The color of the background area behind the movie can be any UIColor value.
     */
//	self.moviePlayer.backgroundView.backgroundColor = [UIColor whiteColor];
}

/* Sent to the view controller after the user interface rotates. */
- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	/* Size movie view to fit parent view. */
	[[[self moviePlayer] view] setFrame:self.view.bounds];
//
    /* Size the overlay view for the current orientation. */
	self.transportControls.frame = CGRectMake(0, self.view.bounds.size.height - 42, self.view.bounds.size.width, 42);
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIDeviceOrientationIsLandscape(interfaceOrientation);
}

- (BOOL)shouldAutorotate {
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations {
    return ((1 << UIInterfaceOrientationPortrait) | (1 << UIInterfaceOrientationLandscapeLeft) | (1 << UIInterfaceOrientationLandscapeRight) | (1 << UIInterfaceOrientationPortraitUpsideDown));  //UIInterfaceOrientationMaskAll
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationLandscapeRight;
}

@end