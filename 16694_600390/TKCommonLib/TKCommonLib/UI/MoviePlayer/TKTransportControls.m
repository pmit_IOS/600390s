//
//  ZSTTransportControls.m
//  VideoTest
//
//  Created by luobin on 12-9-20.
//  Copyright (c) 2012年 luobin. All rights reserved.
//

#import <MessageUI/MessageUI.h>

#import "TKTransportControls.h"
#import "TKPlaybackTimeSlider.h"

#define kPauseTag 1
#define kPlayTag 2

@interface TKTransportControls ()<MFMessageComposeViewControllerDelegate>

@property(nonatomic, retain) TKPlaybackTimeSlider *sliderTimeline;
@property(nonatomic, retain) UIButton *btnPlay;
@property(nonatomic, retain) UILabel *timeLabel;
@property(nonatomic, retain) UIButton *btnShare;
@property(nonatomic, retain) UIButton *btnFullScreen;
@property(nonatomic, assign) NSTimeInterval totalVideoTime;
@property(nonatomic, retain) NSString *totalTimeStr;
@property(nonatomic, assign) BOOL isFullscreen;
@property(nonatomic, retain) NSTimer *timer;

- (NSString *)formateTime:(NSTimeInterval)time;
-(void)onTimeSliderChange:(UISlider*)sender;
-(void)touchThumbImageDown;
-(void)touchThumbImageUp;
-(void)fastforward_touchdown;
-(void)fastforward_touchup;
-(void)rewind_touchdown;
-(void)rewind_touchup;
-(void)monitorPlaybackTime;
-(void)playerPlaybackDidFinish:(NSNotification*)notification;
-(void)resetSlider;
-(void)setTotalVideoTimeDuration:(NSNotification*)notification;
- (void)updateTimeLabel;

@end

@implementation TKTransportControls
@synthesize sliderTimeline;
@synthesize btnPlay;
@synthesize timeLabel;
@synthesize btnShare;
@synthesize btnFullScreen;
@synthesize moviePlayer;
@synthesize totalVideoTime;
@synthesize totalTimeStr;
@synthesize isFullscreen;
@synthesize timer;

- (void)setUpUI
{
    self.totalVideoTime = 0;
    self.isFullscreen = NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playerPlaybackDidFinish:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setTotalVideoTimeDuration:) name:MPMovieDurationAvailableNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(moviePlaybackIsPreparedToPlayDidChangeNotification:) name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:nil];
    
    UIImageView *imageView = [[[UIImageView alloc] initWithFrame:self.bounds] autorelease];
    imageView.backgroundColor = [UIColor clearColor];
    imageView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    //    imageView.image = [[UIImage imageNamed:@"transportControls_bg.png"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
    imageView.image = [[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/liveBg.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
    [self addSubview:imageView];
    
    //播放按钮
    self.btnPlay = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnPlay.enabled = NO;
    self.btnPlay.tag = kPauseTag;
    //CGRectMake(11, 11, 11, 20);
    self.btnPlay.imageEdgeInsets = UIEdgeInsetsMake(11, 10, 11, 10);
    self.btnPlay.frame = CGRectMake(5, 0, 40, 42);
    self.btnPlay.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_play_n.png"] forState:UIControlStateNormal];
    [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_play_p.png"] forState:UIControlStateHighlighted];
    [self.btnPlay addTarget:self action:@selector(playMovie) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.btnPlay];
    
    //进度条
    self.sliderTimeline = [[[TKPlaybackTimeSlider alloc] initWithFrame:CGRectMake(46, 10, 120, 23)] autorelease];
    self.sliderTimeline.enabled = NO;
    self.sliderTimeline.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.sliderTimeline addTarget:self action:@selector(touchThumbImageDown) forControlEvents:UIControlEventTouchDown];
    [self.sliderTimeline addTarget:self action:@selector(touchThumbImageUp) forControlEvents:UIControlEventTouchUpInside];
    [self.sliderTimeline addTarget:self action:@selector(onTimeSliderChange:) forControlEvents:UIControlEventValueChanged];
    [self addSubview:self.sliderTimeline];
    
    //时间
    self.timeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(168, 10, 90, 20)] autorelease];
    self.timeLabel.textAlignment = NSTextAlignmentLeft;
    //    self.timeLabel.text = @"11:20:19/11:20:19";
    self.timeLabel.autoresizingMask =  UIViewAutoresizingFlexibleRightMargin ;
    self.timeLabel.backgroundColor = [UIColor clearColor];
    self.timeLabel.textColor = [UIColor whiteColor];
    self.timeLabel.font = [UIFont systemFontOfSize:12];
    [self addSubview:self.timeLabel];
    
    //分享
    self.btnShare = [UIButton buttonWithType:UIButtonTypeCustom];
    self.btnShare.enabled = NO;
    self.btnShare.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    [self.btnShare addTarget:self action:@selector(share) forControlEvents:UIControlEventTouchUpInside];
    //    self.btnShare.frame = CGRectMake(260, 12, 18, 18);
    self.btnShare.imageEdgeInsets = UIEdgeInsetsMake(12, 10, 12, 10);
    self.btnShare.frame = CGRectMake(250, 0, 38, 42);
    [self.btnShare setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_share_n.png"] forState:UIControlStateNormal];
    [self.btnShare setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_share_p.png"] forState:UIControlStateHighlighted];
    [self addSubview:self.btnShare];
    
    //全屏
    //    self.btnFullScreen = [UIButton buttonWithType:UIButtonTypeCustom];
    //    [self.btnFullScreen addTarget:self action:@selector(fullscreen) forControlEvents:UIControlEventTouchUpInside];
    //    self.btnFullScreen.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin;
    ////    self.btnFullScreen.frame = CGRectMake(287, 12, 18, 18);
    //    self.btnFullScreen.imageEdgeInsets = UIEdgeInsetsMake(12, 10, 12, 10);
    //    self.btnFullScreen.frame = CGRectMake(277, 0, 38, 42);
    //    [self.btnFullScreen setImage:[UIImage imageNamed:@"moviePlayer_fullScreen_n.png"] forState:UIControlStateNormal];
    //    [self.btnFullScreen setImage:[UIImage imageNamed:@"moviePlayer_fullScreen_p.png"] forState:UIControlStateHighlighted];
    //    [self addSubview:self.btnFullScreen];
    
}

- (void)resetTimer
{
    if ([self.viewController respondsToSelector:@selector(resetTimer)]) {
        [self.viewController performSelector:@selector(resetTimer)];
    }
}

#pragma mark - Actions

-(void)onTimeSliderChange:(UISlider*)sender
{
    [self resetTimer];
    self.moviePlayer.currentPlaybackTime = self.sliderTimeline.value;
    [self updateTimeLabel];
}

-(void)touchThumbImageDown
{
    [self resetTimer];
    self.moviePlayer.currentPlaybackTime = self.sliderTimeline.value;
    [self updateTimeLabel];
}

-(void)touchThumbImageUp
{
    [self resetTimer];
    self.sliderTimeline.IsTouch = NO;
}

-(void)playMovie
{
    [self resetTimer];
    self.moviePlayer.view.hidden = NO;
    if (self.btnPlay.tag == kPauseTag)
    {
        if (self.totalVideoTime != 0 && self.moviePlayer.currentPlaybackTime >= totalVideoTime)
        {
            [self resetSlider];
        }
        
        [self.moviePlayer play];
        self.timer = [NSTimer scheduledTimerWithTimeInterval:0.1 target:self selector:@selector(monitorPlaybackTime) userInfo:nil repeats:YES];
        
        [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_pause_n.png"] forState:UIControlStateNormal];
        [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_pause_p.png"] forState:UIControlStateHighlighted];
        self.btnPlay.tag = kPlayTag;
    }
    else if(self.btnPlay.tag == kPlayTag)
    {
        [self.moviePlayer pause];
        //        [self.btnPlay setImage:[UIImage imageNamed:@"UIButtonBarPlayGray.png"] forState:UIControlStateNormal];
        //
        [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_play_n.png"] forState:UIControlStateNormal];
        [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_play_p.png"] forState:UIControlStateHighlighted];
        self.btnPlay.tag = kPauseTag;
    }
}

-(void)share
{
    if ([MFMessageComposeViewController canSendText]) {
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        picker.body = picker.body = [NSString stringWithFormat:NSLocalizedString(@"GP_SMSRecommendMessage", nil),appDisplayName,sharebaseurl];
        picker.messageComposeDelegate = self;
        [self.viewController presentModalViewController:picker animated:NO];
        [picker release];
    } else {
        TKAlertNoTitle(NSLocalizedString(@"无法发送短信！" , nil));
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self.viewController dismissModalViewController];
    // 直接检测服务器是否绑定成功
    if (result == MessageComposeResultSent) {
        TKAlertNoTitle(NSLocalizedString(@"分享短信发送成功！" , nil));
    }
}

-(void)fullscreen
{
    if (self.isFullscreen) {
        self.isFullscreen = NO;
        self.moviePlayer.scalingMode = MPMovieScalingModeAspectFit;
    } else {
        self.isFullscreen = YES;
        self.moviePlayer.scalingMode = MPMovieScalingModeFill;
    }
}

-(void)fastforward_touchdown
{
    self.moviePlayer.currentPlaybackRate = 5;
}

-(void)fastforward_touchup
{
    self.moviePlayer.currentPlaybackRate = 1;
}

-(void)rewind_touchdown
{
    self.moviePlayer.currentPlaybackRate = -5;
}

-(void)rewind_touchup
{
    self.moviePlayer.currentPlaybackRate = 1;
}

-(void)monitorPlaybackTime
{
    if (self.moviePlayer.playbackState != MPMoviePlaybackStatePlaying
        && self.moviePlayer.playbackState != MPMoviePlaybackStateSeekingForward
        && self.moviePlayer.playbackState != MPMoviePlaybackStateSeekingBackward) {
        return;
    }
    if (self.sliderTimeline.IsTouch) {
        return;
    }
    [self.sliderTimeline setValue:self.moviePlayer.currentPlaybackTime animated:NO];
//    self.sliderTimeline.value = self.moviePlayer.currentPlaybackTime;
    [self updateTimeLabel];
}

-(void)playerPlaybackDidFinish:(NSNotification*)notification
{
    if (notification.object == self.moviePlayer) {
        
        [self.timer invalidate];
        self.timer = nil;
        
        [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_play_n.png"] forState:UIControlStateNormal];
        [self.btnPlay setImage:[UIImage imageNamed:@"TKCommonLib.bundle/MoviePlayer/moviePlayer_play_n.png"] forState:UIControlStateHighlighted];
        self.btnPlay.tag = kPauseTag;
    }
}

-(void)moviePlaybackIsPreparedToPlayDidChangeNotification:(NSNotification*)notification
{
    if (notification.object == self.moviePlayer) {
        self.btnPlay.enabled = YES;
        self.sliderTimeline.enabled = YES;
        self.btnShare.enabled = YES;
    }
}

-(void)resetSlider
{
    [self.sliderTimeline setValue:0 animated:NO];
    self.moviePlayer.currentPlaybackTime = 0;
}

-(void)setTotalVideoTimeDuration:(NSNotification*)notification
{
    if (notification.object == self.moviePlayer) {
        self.totalVideoTime = self.moviePlayer.duration;
        self.sliderTimeline.totalVideoTime = self.moviePlayer.duration;
        self.sliderTimeline.minimumValue = 0.0;
        self.sliderTimeline.maximumValue = self.moviePlayer.duration;
        //    self.moviePlayer.currentPlaybackTime = 0.1;
        [self updateTimeLabel];
        NSLog(@"self.moviePlayer.currentPlaybackTime == %f", self.moviePlayer.currentPlaybackTime);
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMovieDurationAvailableNotification object:self.moviePlayer];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMediaPlaybackIsPreparedToPlayDidChangeNotification object:self.moviePlayer];
    
    self.moviePlayer = nil;
    self.totalTimeStr = nil;
    self.sliderTimeline = nil;
    self.btnPlay = nil;
    self.timeLabel = nil;
    self.btnShare = nil;
    self.btnFullScreen = nil;
    [self.timer invalidate];
    self.timer = nil;
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 42)];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        [self setUpUI];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor clearColor];
        [self setUpUI];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 42)];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.timeLabel.frame = CGRectMake(CGRectGetMaxX(self.sliderTimeline.frame) + 10, 10, 90, 20);
}

- (void)setTotalVideoTime:(NSTimeInterval)theTotalVideoTime
{
    totalVideoTime = round(theTotalVideoTime);
    if (totalVideoTime <= 0) {
        self.totalTimeStr = @"--";
    } else {
        self.totalTimeStr = [self formateTime:self.totalVideoTime];
    }
}

- (void)updateTimeLabel
{
    NSTimeInterval currentPlaybackTime = self.moviePlayer.currentPlaybackTime;
    if (self.totalVideoTime != 0 && currentPlaybackTime > totalVideoTime) {
        currentPlaybackTime = self.totalVideoTime;
    }
    self.timeLabel.text = [NSString stringWithFormat:@"%@/%@", [self formateTime:currentPlaybackTime], totalTimeStr];
}

- (NSString *)formateTime:(NSTimeInterval)time
{
    int hour = time/3600;
    int minute = (time - hour*3600)/60;
    int second = time - hour*3600 - minute*60;
    if (hour > 0) {
        return [NSString stringWithFormat:@"%d:%02d:%02d", hour, minute, second];
    } else {
        return [NSString stringWithFormat:@"%02d:%02d", minute, second];
    }
}

@end
