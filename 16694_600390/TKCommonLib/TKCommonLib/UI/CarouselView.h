//
//  CarouselView.h
//  TKCommonLib
//
//  Created by xuhuijun on 13-7-24.
//
//

#import <Foundation/Foundation.h>
#import "TKAsynImageView.h"

@class CarouselView;

@protocol CarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(CarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(CarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol CarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(CarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface CarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>
{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    UIPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<CarouselViewDataSource> _carouselDataSource;
    id<CarouselViewDelegate>  _carouselDelegate;
}

@property(nonatomic, assign) id<CarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<CarouselViewDelegate> carouselDelegate;

- (void)reloadData;
//- (void)selectTabAtIndex:(int)index;
- (void)carouselViewStopAnimation;

@end
