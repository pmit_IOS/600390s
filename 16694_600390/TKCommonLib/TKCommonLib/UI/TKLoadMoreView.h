//
//  TKLoadMoreView.h
//  TKCommonLib
//
//  Created by luo bin on 12-3-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum 
{
    TKLoadMoreViewStateNormal = 0,
    TKLoadMoreViewStateLoading,
    TKLoadMoreViewStateFinish
} TKLoadMoreViewState;

@protocol TKLoadMoreViewDelegate;

@interface TKLoadMoreView : UIView {
@private
    
    id _delegate;
    TKLoadMoreViewState _state;
    
    UILabel *_statusLabel;
    UIActivityIndicatorView *_activityView;
    
    BOOL _hasTriggerRefresh;
}

@property(nonatomic,assign) id <TKLoadMoreViewDelegate> delegate;
@property(nonatomic,retain) UIActivityIndicatorView *activityView;

- (void)loadMoreScrollViewDidEndDragging:(UIScrollView *)scrollView;
- (void)loadMoreTriggerRefresh;
- (void)loadMoreScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView;
- (void)loadMoreScrollViewDataSourceDidFinishedLoading:(UIScrollView *)scrollView state:(TKLoadMoreViewState) state;
- (void)loadMoreScrollViewDidScroll:(UIScrollView *)scrollView;

@end
@protocol TKLoadMoreViewDelegate
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view;
- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view;
@end
