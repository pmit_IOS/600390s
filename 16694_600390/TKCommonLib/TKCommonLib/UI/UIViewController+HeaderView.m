//
//  UIViewController+HeaderView.m
//  TKCommonLib
//
//  Created by admin on 12-9-29.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "UIViewController+HeaderView.h"
#import <objc/runtime.h>

static char headerViewKey;

@implementation UIViewController (HeaderView)

- (UIView *)headerView
{
    UIView *view = objc_getAssociatedObject(self, &headerViewKey);
    if (view == nil) {
        view = [[[UIView alloc] init] autorelease];

    }
    return view;
}

- (void)setHeaderView:(UIView *)headerView
{
    objc_setAssociatedObject(self, &headerViewKey, headerView, OBJC_ASSOCIATION_RETAIN);

}

@end
