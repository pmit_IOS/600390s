//
//  ZSTTransportControls.h
//  VideoTest
//
//  Created by luobin on 12-9-20.
//  Copyright (c) 2012年 luobin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>

@interface TKTransportControls : UIView

@property(nonatomic, assign) MPMoviePlayerController *moviePlayer;

-(void)playMovie;

@end
