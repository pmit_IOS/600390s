//
//  TKActionSheet.h
//  TKCommonLib
//
//  Created by luobin on 5/2/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TKActionSheet2 : UIActionSheet {
    UIToolbar* toolBar;
    UIView* view;
}

@property(nonatomic,retain) UIView* view;
@property(nonatomic,retain) UIToolbar* toolBar;

-(id)initWithHeight:(float)height WithSheetTitle:(NSString*)title;

@end

