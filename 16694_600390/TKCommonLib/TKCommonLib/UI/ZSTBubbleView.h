//
//  ZSTBubbleView.h
//  TKCommonLib
//
//  Created by xuhuijun on 12-12-30.
//  Copyright (c) 2012年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTBubbleView : UIView

+ (UIView *)bubbleView:(NSString *)text from:(BOOL)fromSelf;
//+ (void)bubleViewStopAnimating;
@end
