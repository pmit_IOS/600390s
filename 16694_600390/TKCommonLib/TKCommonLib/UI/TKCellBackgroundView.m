//
//  HHCellBackgroundView.m
//  petmii
//
//  Created by luobin on 5/10/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "TKCellBackgroundView.h"

@implementation TKCellBackgroundView

@synthesize borderColor, fillColor, shadowColor, position;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.shadowColor = [UIColor colorWithWhite:1 alpha:1];
        self.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
        self.borderColor = [UIColor colorWithWhite:0.93 alpha:1];
    }
    return self;
}

- (BOOL) isOpaque {
    return NO;
}

- (void)drawRect:(CGRect)rect {
    
    CGContextRef c = UIGraphicsGetCurrentContext();
	[fillColor set];
	CGContextFillRect(c, CGRectInset(self.bounds, 0, 1));
    
    CGFloat height = 1.f;
    if (TKIsRetina()) {
        height = 0.5f;
    }
    
    if (position == CustomCellBackgroundViewPositionNone) {
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        return;
        
    }else if (position == CustomCellBackgroundViewPositionTopNone) {
        
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        return;
        
    }else if (position == CustomCellBackgroundViewPositionTop) {
        
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, height, self.bounds.size.width, height));
        
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        
        return;
    }else if (position == CustomCellBackgroundViewPositionBottomNone) {
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        return;
    }
    else if (position == CustomCellBackgroundViewPositionBottom) {
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height*2, self.bounds.size.width, height));     
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        
        return;
    }
    else if (position == CustomCellBackgroundViewPositionMiddle) {
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        
        
        return;
    }
    else if (position == CustomCellBackgroundViewPositionSingle)
    {
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, height, self.bounds.size.width, height));
        
        [self.borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - 2*height, self.bounds.size.width, height));     
        
        [self.shadowColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));          
        return;         
    }
    
    
}

- (void)dealloc {
    [shadowColor release];
    [borderColor release];
    [fillColor release];
    [super dealloc];
}

@end