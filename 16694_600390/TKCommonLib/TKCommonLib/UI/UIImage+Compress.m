//
//  UIImage+Compress.m
//  F3_UI
//
//  Created by 9588 9588 on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#define MAX_IMAGEPIX 1024

#import "UIImage+Compress.h"

TK_FIX_CATEGORY_BUG(UIImage_Compress)

@implementation UIImage (UIImage_Compress)

- (UIImage *)compressedImageScaleFactor:(CGFloat)imagePix
{
    CGSize imageSize = self.size;
    
    CGFloat width = imageSize.width;
    
    CGFloat height = imageSize.height;
    
    if (width <= MAX_IMAGEPIX) {
        
        // no need to compress.
        
        return self;
    }
    
    if (width == 0 || height == 0) {
        
        // void zero exception
        
        return self;
    }
    
    UIImage *newImage = nil;
    
    CGFloat scaleFactor = imagePix / width;
    
    CGFloat scaledWidth  = width * scaleFactor;
    
    CGFloat scaledHeight = height * scaleFactor;
    
    CGSize targetSize = CGSizeMake(scaledWidth, scaledHeight);
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    
    thumbnailRect.size.width  = scaledWidth;
    
    thumbnailRect.size.height = scaledHeight;
    
    [self drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //pop the context to get back to the default
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (UIImage *)compressedImage {
    
    CGSize imageSize = self.size;
    
    CGFloat width = imageSize.width;
    
    CGFloat height = imageSize.height;
    
    if (width <= MAX_IMAGEPIX) {
        
        // no need to compress.
        
        return self;
    }
    
    if (width == 0 || height == 0) {
        
        // void zero exception
        
        return self;
    }
    
    UIImage *newImage = nil;
    
    CGFloat scaleFactor = MAX_IMAGEPIX / width;
    
    CGFloat scaledWidth  = width * scaleFactor;
    
    CGFloat scaledHeight = height * scaleFactor;
    
    CGSize targetSize = CGSizeMake(scaledWidth, scaledHeight);
    
    UIGraphicsBeginImageContext(targetSize); // this will crop
    
    CGRect thumbnailRect = CGRectZero;
    
    thumbnailRect.size.width  = scaledWidth;
    
    thumbnailRect.size.height = scaledHeight;
    
    [self drawInRect:thumbnailRect];
    
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    //pop the context to get back to the default
    
    UIGraphicsEndImageContext();
    
    return newImage;
}

- (NSData *)compressedData:(CGFloat)compressionQuality {
    
    assert(compressionQuality<=1.0 && compressionQuality >=0);
    
    return UIImageJPEGRepresentation(self, compressionQuality);
}

- (CGFloat)compressionQuality {
    
    NSData *data = UIImageJPEGRepresentation(self, 1.0);
    
    NSUInteger dataLength = [data length];
    
    if(dataLength>50000.0) {
        
        // 5K
        
        return 1.0-50000.0/dataLength;
        
    } else {
        
        return 1.0;
    }
}

- (NSData *)compressedData {
    
    CGFloat quality = [self compressionQuality];
    
    return [self compressedData:quality];
}

@end
