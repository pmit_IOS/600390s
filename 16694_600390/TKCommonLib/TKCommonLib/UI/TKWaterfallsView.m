//
//  TKWaterFlowView.m
//  
//
//  Created by luobin on 12-38.
//  Copyright (c) __MyCompanyName__. All rights reserved.
//  update by xuhuijun


#import <QuartzCore/QuartzCore.h>
#import "TKWaterfallsView.h"

#define kTKCellDefaultHeight 100

@implementation TKWaterfallsViewCell

    

@end

@implementation TKWaterfallsView
@synthesize containerView;
@synthesize dataSource;
@synthesize numberOfCells;
@synthesize delegate;
@synthesize headerView;
@synthesize footerView;
@synthesize separaterWidth;
@synthesize cellWidth = _cellWidth;

- (void)initData
{
    self.alwaysBounceVertical = YES;
    self.separaterWidth = 0.0f;
    
    if (reusableCells == nil) {
        reusableCells = [[NSMutableSet alloc] init];
    }
    if (heights == nil) {
        heights = [[NSMutableArray alloc] init];
    }
    
    if (visibleCellIndexes == nil) {
        visibleCellIndexes  = [[NSMutableSet alloc] init];
    }
    
    if (containerView == nil) {
        containerView = [[UIView alloc] initWithFrame:CGRectZero];
        [containerView setBackgroundColor:[UIColor clearColor]];
        [self addSubview:containerView];
    }
}

- (id)initWithCoder:(NSCoder *)decoder
{
    self = [super initWithCoder:decoder];
    if (self) {
        [self initData];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self initData];
    }
    return self;
}

- (void)dealloc {
    self.headerView = nil;
    self.footerView = nil;
    [heights release];
    [reusableCells release];
    [containerView release];
    [visibleCellIndexes release];
    [super dealloc];
}

- (void)setDelegate:(id<TKWaterfallsViewDelegate>)theDelegate
{
    [super setDelegate:theDelegate];
    delegate = theDelegate;
}

- (TKWaterfallsViewCell *)dequeueReusableCell {
    TKWaterfallsViewCell *cell = [reusableCells anyObject];
    if (cell) {
        [[cell retain] autorelease];
        [reusableCells removeObject:cell];
    }
    return cell;
}

- (void)setHeaderView:(UIView *)theHeaderView
{
    [self addSubview:theHeaderView];
    headerView = theHeaderView;
}

- (void)setFooterView:(UIView *)theFooterView
{
    [self addSubview:theFooterView];
    footerView = theFooterView;
}

- (void)willMoveToSuperview:(UIView *)newSuperview
{
    if (newSuperview) {
        [self reloadData];
    }
}

- (void)reloadData {
    // 重用所有cell
    
    self.cellWidth = (self.frame.size.width - 3*separaterWidth)/[self.dataSource numberOfColumsInWaterfallsView:self];
    
    for (TKWaterfallsViewCell *view in self.visibleCells) {
        [reusableCells addObject:view];
        [view removeFromSuperview];
    }
    [visibleCellIndexes removeAllObjects];
    [heights removeAllObjects];
    numberOfCells = [self.dataSource numberOfRowsInWaterfallsView:self];
    
    if ([self.delegate respondsToSelector:@selector(waterfallsView:heightForCellAtIndex:)]) {
        for (int i = 0; i < numberOfCells; i++) {
            NSUInteger height = [self.delegate waterfallsView:self heightForCellAtIndex:i];
            [heights addObject:[NSNumber numberWithDouble:height]];
        }
    } else {
        for (int i = 0; i < numberOfCells; i++) {
            [heights addObject:[NSNumber numberWithDouble:kTKCellDefaultHeight]];
        }
    }
    
    [self setNeedsLayout];
}

- (NSArray *)visibleCells
{
    return [containerView subviews];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGRect visibleBounds = CGRectMake(0, self.contentOffset.y, self.bounds.size.width, self.bounds.size.height);
    
    // 重用不可见的cell
    for (TKWaterfallsViewCell *cell in self.visibleCells) {
        
        if (! CGRectIntersectsRect(cell.frame, visibleBounds)) {
            [reusableCells addObject:cell];
            [cell removeFromSuperview];
        }
    }
    self.cellWidth = (self.frame.size.width - 3*separaterWidth)/[self.dataSource numberOfColumsInWaterfallsView:self]; //每列的宽度

    CGFloat leftHeight = 0, middleHeight = 0;
    
    NSMutableSet *theVisibleCellIndexes = [NSMutableSet set];
    
    //循环cell，计算出新增的cell
    for (NSUInteger row = 0; row < numberOfCells; row++) {
        
        int col = 0;
        double height = [[heights objectAtIndex:row] doubleValue];
        double mininum = MIN(leftHeight,middleHeight);
        if (leftHeight == mininum) {
            col = 0;
            leftHeight += (self.separaterWidth + height);
            
        } else if (middleHeight == mininum) {
            col = 1;
            middleHeight += (self.separaterWidth + height);
            
        }
        
        CGRect rect = CGRectMake(self.separaterWidth*(col + 1) + col*self.cellWidth, mininum + self.separaterWidth, self.cellWidth, height);
        
        if (CGRectIntersectsRect(visibleBounds, rect)) {
            
            [theVisibleCellIndexes addObject:[NSNumber numberWithUnsignedInteger:row]];

            BOOL cellIsMissing = ![visibleCellIndexes containsObject:[NSNumber numberWithUnsignedInteger:row]];
            
            if (cellIsMissing) {
                TKWaterfallsViewCell *cell = [dataSource waterfallsView:self cellAtIndex:row];
                UITapGestureRecognizer *gr = [[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didSelectView:)] autorelease];
                [cell addGestureRecognizer:gr];
                cell.userInteractionEnabled = YES;
                
                // set the cell's frame so we insert it at the correct position
                [cell setFrame:rect];
                [containerView addSubview:cell];
            }
        }
    }
    [visibleCellIndexes release];
    visibleCellIndexes = [theVisibleCellIndexes retain];
    
    CGFloat contentHeight = MAX(leftHeight, middleHeight);
    
    CGRect contentRect = CGRectMake(0, 0, self.frame.size.width, contentHeight + self.separaterWidth);
    [containerView setFrame:contentRect];
    
    CGFloat heightForHeader = 0;
    if ([delegate respondsToSelector:@selector(heightForHeaderInWaterfallsView:)]) {
        heightForHeader = [delegate heightForHeaderInWaterfallsView:self];
    }
    
    CGFloat heightForFooter = 0;
    if ([delegate respondsToSelector:@selector(heightForFooterInWaterfallsView:)]) {
        heightForFooter = [delegate heightForFooterInWaterfallsView:self];
    }
    self.footerView.frame = CGRectMake(0, heightForHeader + contentHeight, self.frame.size.width, heightForFooter);
    
    self.contentSize = CGSizeMake(self.frame.size.width, heightForHeader + contentHeight + self.separaterWidth + heightForFooter);
    
}

- (void)didSelectView:(UITapGestureRecognizer *)gestureRecognizer {
    
    TKWaterfallsViewCell *cell = (TKWaterfallsViewCell *)gestureRecognizer.view;
    if ([self.delegate respondsToSelector:@selector(waterFlowView:didSelectRowAtIndex:)]) {
        [self.delegate waterFlowView:self didSelectRowAtIndex:cell.tag];
    }
}
@end
