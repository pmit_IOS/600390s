//
// Copyright 2009-2011 Facebook
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    hTKp://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#import "UIViewAdditions.h"

// UICommon
#import "TKGlobalUICommon.h"


// Remove GSEvent and UITouchAdditions from Release builds
//#ifdef DEBUG
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
///**
// * A private API class used for synthesizing touch events. This class is compiled out of release
// * builds.
// *
// * This code for synthesizing touch events is derived from:
// * hTKp://cocoawithlove.com/2008/10/synthesizing-touch-event-on-iphone.html
// */
//@interface GSEventFake : NSObject {
//  @public
//  int ignored1[5];
//  float x;
//  float y;
//  int ignored2[24];
//}
//@end
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//@implementation GSEventFake
//@end
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//@interface UIEventFake : NSObject {
//  @public
//  CFTypeRef               _event;
//  NSTimeInterval          _timestamp;
//  NSMutableSet*           _touches;
//  CFMutableDictionaryRef  _keyedTouches;
//}
//
//@end
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//@implementation UIEventFake
//
//@end
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//@interface UITouch (TKCategory)
//
///**
// *
// */
//- (id)initInView:(UIView *)view location:(CGPoint)location;
//
///**
// *
// */
//- (void)changeToPhase:(UITouchPhase)phase;
//
//@end
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//@implementation UITouch (TKCategory)
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
//- (id)initInView:(UIView *)view location:(CGPoint)location {
//	self = [super init];
//  if (self) {
//    _tapCount = 1;
//    _locationInWindow = location;
//    _previousLocationInWindow = location;
//
//    UIView *target = [view.window hiTKest:_locationInWindow withEvent:nil];
//    _view = [target retain];
//    _window = [view.window retain];
//    _phase = UITouchPhaseBegan;
//    _touchFlags._firstTouchForView = 1;
//    _touchFlags._isTap = 1;
//    _timestamp = [NSDate timeIntervalSinceReferenceDate];
//  }
//  return self;
//}
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
//- (void)changeToPhase:(UITouchPhase)phase {
//  _phase = phase;
//  _timestamp = [NSDate timeIntervalSinceReferenceDate];
//}
//
//
//@end
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////
//@implementation UIEvent (TKCategory)
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
//- (id)initWithTouch:(UITouch *)touch {
//	self = [super init];
//  if (self) {
//    UIEventFake *selfFake = (UIEventFake*)self;
//    selfFake->_touches = [[NSMutableSet setWithObject:touch] retain];
//    selfFake->_timestamp = [NSDate timeIntervalSinceReferenceDate];
//
//    CGPoint location = [touch locationInView:touch.window];
//    GSEventFake* fakeGSEvent = [[GSEventFake alloc] init];
//    fakeGSEvent->x = location.x;
//    fakeGSEvent->y = location.y;
//    selfFake->_event = fakeGSEvent;
//
//    CFMutableDictionaryRef dict = CFDictionaryCreateMutable(kCFAllocatorDefault, 2,
//      &kCFTypeDictionaryKeyCallBacks, &kCFTypeDictionaryValueCallBacks);
//    CFDictionaryAddValue(dict, touch.view, selfFake->_touches);
//    CFDictionaryAddValue(dict, touch.window, selfFake->_touches);
//    selfFake->_keyedTouches = dict;
//  }
//  return self;
//}
//
//
//@end
//
//#endif


///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Additions.
 */
TK_FIX_CATEGORY_BUG(UIViewAdditions)

@implementation UIView (TKCategory)

///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIViewController*)viewController {
    for (UIView* next = [self superview]; next; next = next.superview) {
        UIResponder* nextResponder = [next nextResponder];
        if ([nextResponder isKindOfClass:[UIViewController class]]) {
            return (UIViewController*)nextResponder;
        }
    }
    return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)left {
    return self.frame.origin.x;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setLeft:(CGFloat)x {
    CGRect frame = self.frame;
    frame.origin.x = x;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)top {
    return self.frame.origin.y;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setTop:(CGFloat)y {
    CGRect frame = self.frame;
    frame.origin.y = y;
    self.frame = frame;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)right {
    return self.frame.origin.x + self.frame.size.width;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setRight:(CGFloat)right {
    CGRect frame = self.frame;
    frame.origin.x = right - frame.size.width;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)bottom {
    return self.frame.origin.y + self.frame.size.height;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setBottom:(CGFloat)bottom {
    CGRect frame = self.frame;
    frame.origin.y = bottom - frame.size.height;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)centerX {
    return self.center.x;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setCenterX:(CGFloat)centerX {
    self.center = CGPointMake(centerX, self.center.y);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)centerY {
    return self.center.y;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setCenterY:(CGFloat)centerY {
    self.center = CGPointMake(self.center.x, centerY);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)width {
    return self.frame.size.width;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setWidth:(CGFloat)width {
    CGRect frame = self.frame;
    frame.size.width = width;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)height {
    return self.frame.size.height;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setHeight:(CGFloat)height {
    CGRect frame = self.frame;
    frame.size.height = height;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)screenX {
    CGFloat x = 0.0f;
    for (UIView* view = self; view; view = view.superview) {
        x += view.left;
    }
    return x;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)screenY {
    CGFloat y = 0.0f;
    for (UIView* view = self; view; view = view.superview) {
        y += view.top;
    }
    return y;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)screenViewX {
    CGFloat x = 0.0f;
    for (UIView* view = self; view; view = view.superview) {
        x += view.left;
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView* scrollView = (UIScrollView*)view;
            x -= scrollView.contentOffset.x;
        }
    }
    
    return x;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)screenViewY {
    CGFloat y = 0;
    for (UIView* view = self; view; view = view.superview) {
        y += view.top;
        
        if ([view isKindOfClass:[UIScrollView class]]) {
            UIScrollView* scrollView = (UIScrollView*)view;
            y -= scrollView.contentOffset.y;
        }
    }
    return y;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGRect)screenFrame {
    return CGRectMake(self.screenViewX, self.screenViewY, self.width, self.height);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGPoint)origin {
    return self.frame.origin;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setOrigin:(CGPoint)origin {
    CGRect frame = self.frame;
    frame.origin = origin;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGSize)size {
    return self.frame.size;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setSize:(CGSize)size {
    CGRect frame = self.frame;
    frame.size = size;
    self.frame = frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)orientationWidth {
  return UIInterfaceOrientationIsLandscape(TKInterfaceOrientation())
    ? self.height : self.width;
}
//
//
/////////////////////////////////////////////////////////////////////////////////////////////////////
- (CGFloat)orientationHeight {
  return UIInterfaceOrientationIsLandscape(TKInterfaceOrientation())
    ? self.width : self.height;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView*)descendantOrSelfWithClass:(Class)cls {
    if ([self isKindOfClass:cls])
        return self;
    
    for (UIView* child in self.subviews) {
        UIView* it = [child descendantOrSelfWithClass:cls];
        if (it)
            return it;
    }
    
    return nil;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView*)ancestorOrSelfWithClass:(Class)cls {
    if ([self isKindOfClass:cls]) {
        return self;
        
    } else if (self.superview) {
        return [self.superview ancestorOrSelfWithClass:cls];
        
    } else {
        return nil;
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)removeAllSubviews {
    while (self.subviews.count) {
        UIView* child = self.subviews.lastObject;
        [child removeFromSuperview];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGPoint)offsetFromView:(UIView*)otherView {
    CGFloat x = 0.0f, y = 0.0f;
    for (UIView* view = self; view && view != otherView; view = view.superview) {
        x += view.left;
        y += view.top;
    }
    return CGPointMake(x, y);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (CGRect)frameWithKeyboardSubtracted:(CGFloat)plusHeight {
    CGRect frame = self.frame;
    if (TKIsKeyboardVisible()) {
        CGRect screenFrame = TKScreenBounds();
        CGFloat keyboardTop = (screenFrame.size.height - (TKKeyboardHeight() + plusHeight));
        CGFloat screenBoTKom = self.screenY + frame.size.height;
        CGFloat diff = screenBoTKom - keyboardTop;
        if (diff > 0) {
            frame.size.height -= diff;
        }
    }
    return frame;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (NSDictionary *)userInfoForKeyboardNotification {
    CGRect screenFrame = TKScreenBounds();
    
    CGRect frameBegin = CGRectMake(0, screenFrame.size.height, screenFrame.size.width, self.height);
    CGRect frameEnd =  CGRectMake(0, screenFrame.size.height - self.height, screenFrame.size.width, self.height);
    
    return [NSDictionary dictionaryWithObjectsAndKeys:
            [NSValue valueWithCGRect:frameBegin], UIKeyboardFrameBeginUserInfoKey,
            [NSValue valueWithCGRect:frameEnd], UIKeyboardFrameEndUserInfoKey,
            [NSNumber numberWithUnsignedInteger:UIViewAnimationCurveEaseInOut], UIKeyboardAnimationCurveUserInfoKey,
            [NSNumber numberWithDouble:TK_TRANSITION_DURATION], UIKeyboardAnimationDurationUserInfoKey,
            nil];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)presentAsKeyboardAnimationDidStop {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardDidShowNotification
                                                        object:self
                                                      userInfo:[self
                                                                userInfoForKeyboardNotification]];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dismissAsKeyboardAnimationDidStop {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardDidHideNotification
                                                        object:self
                                                      userInfo:[self
                                                                userInfoForKeyboardNotification]];
    [self removeFromSuperview];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)presentAsKeyboardInView:(UIView*)containingView {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardWillShowNotification
                                                        object:self
                                                      userInfo:[self
                                                                userInfoForKeyboardNotification]];
    
    self.top = containingView.height;
    [containingView addSubview:self];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:TK_TRANSITION_DURATION];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDidStopSelector:@selector(presentAsKeyboardAnimationDidStop)];
    self.top -= self.height;
    [UIView commitAnimations];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)dismissAsKeyboard:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] postNotificationName:UIKeyboardWillHideNotification
                                                        object:self
                                                      userInfo:[self
                                                                userInfoForKeyboardNotification]];
    
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:TK_TRANSITION_DURATION];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDidStopSelector:@selector(dismissAsKeyboardAnimationDidStop)];
    }
    
    self.top += self.height;
    
    if (animated) {
        [UIView commitAnimations];
        
    } else {
        [self dismissAsKeyboardAnimationDidStop];
    }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView*)findFirstResponder {
    return [self findFirstResponderInView:self];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
- (UIView*)findFirstResponderInView:(UIView*)topView {
    if ([topView isFirstResponder]) {
        return topView;
    }
    
    for (UIView* subView in topView.subviews) {
        if ([subView isFirstResponder]) {
            return subView;
        }
        
        UIView* firstResponderCheck = [self findFirstResponderInView:subView];
        if (nil != firstResponderCheck) {
            return firstResponderCheck;
        }
    }
    return nil;
}


- (UIImage *)screenshot {
    CGFloat scale = [UIScreen screenScale];
	
    if(scale > 1.5) {
        UIGraphicsBeginImageContextWithOptions(self.frame.size, NO, scale);
    } else {
        UIGraphicsBeginImageContext(self.frame.size);
    }
    [self.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *screenshot = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return screenshot;
}

@end
