//
//  DDPageControl.m
//  DDPageControl
//
//  Created by Damien DeVille on 1/14/11.
//  Copyright 2011 Snappy Code. All rights reserved.
//

#import "TKPageControl.h"


#define kDotSize	4.0f
#define kDotSpace		12.0f


@implementation TKPageControl

@synthesize numberOfPages;
@synthesize currentPage;
@synthesize hidesForSinglePage;
@synthesize defersCurrentPageDisplay;
@synthesize wrap;

@synthesize type;
@synthesize onColor;
@synthesize offColor;
@synthesize onImage;
@synthesize offImage;
@synthesize dotSize;
@synthesize dotSpacing;

#pragma mark -
#pragma mark Initializers - dealloc

- (id)initWithType:(TKPageControlType)theType
{
	self = [self initWithFrame: CGRectZero] ;
	[self setType: theType] ;
	return self ;
}

- (id)init
{
	self = [self initWithFrame: CGRectZero] ;
	return self ;
}

- (id)initWithFrame:(CGRect)frame
{
	if ((self = [super initWithFrame: CGRectMake(frame.origin.x, frame.origin.y, 0, 0)]))
	{
		self.backgroundColor = [UIColor clearColor] ;
	}
	return self ;
}

- (void)dealloc 
{
	[onColor release], onColor = nil ;
	[offColor release], offColor = nil ;
    [onImage release]; onImage = nil;
    [offImage release]; offImage = nil;
	
	[super dealloc] ;
}


#pragma mark -
#pragma mark drawRect

- (void)drawRect:(CGRect)rect 
{
	// get the current context
	CGContextRef context = UIGraphicsGetCurrentContext() ;
	
	// save the context
	CGContextSaveGState(context) ;
	
	// allow antialiasing
	CGContextSetAllowsAntialiasing(context, TRUE) ;
	
	// get the caller's diameter if it has been set or use the default one 
	CGFloat diameter = (dotSize > 0) ? dotSize : kDotSize ;
	CGFloat space = (dotSpacing > 0) ? dotSpacing : kDotSpace ;
	
	// geometry
	CGRect currentBounds = self.bounds ;
	CGFloat dotsWidth = self.numberOfPages * diameter + MAX(0, self.numberOfPages - 1) * space ;
	CGFloat x = CGRectGetMidX(currentBounds) - dotsWidth / 2 ;
	CGFloat y = CGRectGetMidY(currentBounds) - diameter / 2 ;
	
	// get the caller's colors it they have been set or use the defaults
	UIColor *drawOnColor = onColor ? onColor : [UIColor colorWithWhite: 1.0f alpha: 1.0f];
	UIColor *drawOffColor = offColor ? offColor : [UIColor colorWithWhite: 0.7f alpha: 0.5f];
    
    UIImage *drawOnImage = onImage ? onImage : [UIImage imageNamed:@"tabbar_badge_bg.png"];
	UIImage *drawOffImage = offImage ? offImage : [UIImage imageNamed:@"ipad_attachment_deleteImage.png"];
	
	// actually draw the dots
	for (int i = 0 ; i < numberOfPages ; i++)
	{
		CGRect dotRect = CGRectMake(x, y, diameter, diameter) ;
		
		if (i == currentPage)
		{
			if (type == TKPageControlTypeOnFullOffFull || type == TKPageControlTypeOnFullOffEmpty)
			{
				CGContextSetFillColorWithColor(context, drawOnColor.CGColor) ;
              CGContextFillEllipseInRect(context, CGRectInset(dotRect, -0.5f, -0.5f)) ;
                
			}else if (type == TKPageControlTypeOnImageOffImage){
                
                CGContextDrawImage(context, dotRect, drawOnImage.CGImage);
                
			}else{
                
				CGContextSetStrokeColorWithColor(context, drawOnColor.CGColor) ;
				CGContextStrokeEllipseInRect(context, dotRect) ;

			}
		}
		else
		{
			if (type == TKPageControlTypeOnEmptyOffEmpty || type == TKPageControlTypeOnFullOffEmpty)
			{
				CGContextSetStrokeColorWithColor(context, drawOffColor.CGColor) ;
				CGContextStrokeEllipseInRect(context, dotRect) ;

			}else if (type == TKPageControlTypeOnImageOffImage){
                
                CGContextDrawImage(context, dotRect, drawOffImage.CGImage);
                
            }else {
                
				CGContextSetFillColorWithColor(context, drawOffColor.CGColor) ;
				CGContextFillEllipseInRect(context, CGRectInset(dotRect, -0.5f, -0.5f)) ;
			}
		}
		
		x += diameter + space ;
	}
	
	// restore the context
	CGContextRestoreGState(context) ;
}


#pragma mark -
#pragma mark Accessors

- (void)setCurrentPage:(NSInteger)pageNumber
{
	// no need to update in that case
	if (currentPage == pageNumber)
		return ;
	
	// determine if the page number is in the available range
    
    if (wrap)
    {
        currentPage = (pageNumber + numberOfPages) % numberOfPages;
    }
    else
    {
        currentPage =  MIN(MAX(0, pageNumber), numberOfPages - 1);
    }
	if (currentPage == pageNumber) {
        [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
	
	// in case we do not defer the page update, we redraw the view
	if (self.defersCurrentPageDisplay == NO)
		[self setNeedsDisplay] ;
}

- (void)setNumberOfPages:(NSInteger)numOfPages
{
	// make sure the number of pages is positive
	numberOfPages = MAX(0, numOfPages) ;
	
	// we then need to update the current page
	self.currentPage = MIN(MAX(0, currentPage), numberOfPages - 1) ;
	
	// correct the bounds accordingly
	self.bounds = self.bounds ;
	
	// we need to redraw
	[self setNeedsDisplay] ;
	
	// depending on the user preferences, we hide the page control with a single element
	if (hidesForSinglePage && (numOfPages < 2))
		[self setHidden: YES] ;
	else
		[self setHidden: NO] ;
}

- (void)setHidesForSinglePage:(BOOL)hide
{
	hidesForSinglePage = hide ;
	
	// depending on the user preferences, we hide the page control with a single element
	if (hidesForSinglePage && (numberOfPages < 2))
		[self setHidden: YES] ;
}

- (void)setDefersCurrentPageDisplay:(BOOL)defers
{
	defersCurrentPageDisplay = defers ;
}

- (void)setType:(TKPageControlType)aType
{
	type = aType ;
	
	[self setNeedsDisplay] ;
}

- (void)setOnColor:(UIColor *)aColor
{
	[aColor retain] ;
	[onColor release] ;
	onColor = aColor ;
	
	[self setNeedsDisplay] ;
}

- (void)setOffColor:(UIColor *)aColor
{
	[aColor retain] ;
	[offColor release] ;
	offColor = aColor ;
	
	[self setNeedsDisplay] ;
}

- (void)setOnImage:(UIImage *)aImage
{
	[aImage retain] ;
	[onImage release] ;
	onImage = aImage ;
	
	[self setNeedsDisplay] ;
}

- (void)setOffImage:(UIImage *)aImage
{
	[aImage retain] ;
	[offImage release] ;
	offImage = aImage ;
	
	[self setNeedsDisplay] ;
}

- (void)setDotSize:(CGFloat)aDiameter
{
	dotSize = aDiameter ;
	
	// correct the bounds accordingly
	self.bounds = self.bounds ;
	
	[self setNeedsDisplay] ;
}

- (void)setDotSpacing:(CGFloat)aSpace
{
	dotSpacing = aSpace ;
	
	// correct the bounds accordingly
	self.bounds = self.bounds ;
	
	[self setNeedsDisplay] ;
}

- (void)setFrame:(CGRect)aFrame
{
	// we do not allow the caller to modify the size struct in the frame so we compute it
	aFrame.size = [self sizeForNumberOfPages: numberOfPages] ;
	super.frame = aFrame ;
}

- (void)setBounds:(CGRect)aBounds
{
	// we do not allow the caller to modify the size struct in the bounds so we compute it
	aBounds.size = [self sizeForNumberOfPages: numberOfPages] ;
	super.bounds = aBounds ;
}



#pragma mark -
#pragma mark UIPageControl methods

- (void)updateCurrentPageDisplay
{
	// ignores this method if the value of defersPageIndicatorUpdate is NO
	if (self.defersCurrentPageDisplay == NO)
		return ;
	
	// in case it is YES, we redraw the view (that will update the page control to the correct page)
	[self setNeedsDisplay] ;
}

- (CGSize)sizeForNumberOfPages:(NSInteger)pageCount
{
	CGFloat diameter = (dotSize > 0) ? dotSize : kDotSize ;
	CGFloat space = (dotSpacing > 0) ? dotSpacing : kDotSpace ;
	
	return CGSizeMake( MAX(320, pageCount * diameter + (pageCount + 1) * space) ,  diameter + 4.0f) ;
}


#pragma mark -
#pragma mark Touches handlers

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
	// get the touch location
	UITouch *theTouch = [touches anyObject] ;
	CGPoint touchLocation = [theTouch locationInView: self] ;
	
	// check whether the touch is in the right or left hand-side of the control
	if (touchLocation.x < (self.bounds.size.width / 2))
		self.currentPage = MAX(self.currentPage - 1, 0) ;
	else
		self.currentPage = MIN(self.currentPage + 1, numberOfPages - 1) ;
	
	// send the value changed action to the target
	[self sendActionsForControlEvents: UIControlEventValueChanged] ;
}

@end