

//
//  TKLoadMoreView.h
//  TKCommonLib
//
//  Created by luo bin on 12-4-11.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//


@interface TKTabBarItem : UITabBarItem {

}


- (id)initWithTitle:(NSString *)title unselectedImage:(UIImage *)anUnselectedImage selectedImage:(UIImage *)aSelectedImage tag:(NSInteger)tag;

- (id)initWithTitle:(NSString *)title unselectedImage:(UIImage *)anUnselectedImage selectedImage:(UIImage *)aSelectedImage textColor:(NSString *)hexColor tag:(NSInteger)tag;

@end
