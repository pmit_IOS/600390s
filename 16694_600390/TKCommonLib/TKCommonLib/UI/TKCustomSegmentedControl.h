//
//  CustomSegmentedControl.h
//  WoodUINavigation
//
//  Created by Peter Boctor on 12/13/10.
//
// Copyright (c) 2011 Peter Boctor
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE
//

#define BUTTON_WIDTH 54.0
#define BUTTON_SEGMENT_WIDTH 60.0
#define CAP_WIDTH 5.0

typedef enum {
    CapLeft          = 0,
    CapMiddle        = 1,
    CapRight         = 2,
    CapLeftAndRight  = 3
} CapLocation;

@class TKCustomSegmentedControl;
@protocol TKCustomSegmentedControlDelegate<NSObject>

//- (UIButton*) buttonFor:(TKCustomSegmentedControl*)segmentedControl atIndex:(NSUInteger)segmentIndex;

@optional
- (void) segmentedControl:(TKCustomSegmentedControl*)segmentedControl touchUpInsideSegmentIndex:(NSUInteger)segmentIndex;
- (void)segmentedControl:(TKCustomSegmentedControl*)segmentedControl touchDownAtSegmentIndex:(NSUInteger)segmentIndex;
@end

@interface TKCustomSegmentedControl : UIView
{
    NSObject <TKCustomSegmentedControlDelegate> *delegate;
    NSMutableArray* buttons;
}

@property (nonatomic, readonly) NSMutableArray* buttons;
@property (nonatomic, assign) id<TKCustomSegmentedControlDelegate> delegate;
@property (nonatomic, readonly) NSUInteger selectedIndex;

//Initializes and returns a segmented control with segments having the given titles.
- (id) initWithSegmentItems:(NSArray *)items;

@end
