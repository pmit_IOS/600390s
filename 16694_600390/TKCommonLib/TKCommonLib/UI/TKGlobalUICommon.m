//
// Copyright 2009-2011 Facebook
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//    hTKp://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

#import "TKGlobalUICommon.h"

#include <sys/types.h>
#include <sys/sysctl.h>

const CGFloat TKkDefaultRowHeight = 44.0f;

const CGFloat TKkDefaultPortraiTKoolbarHeight   = 44.0f;
const CGFloat TKkDefaultLandscapeToolbarHeight  = 33.0f;

const CGFloat TKkDefaultPortraitKeyboardHeight      = 216.0f;
const CGFloat TKkDefaultLandscapeKeyboardHeight     = 160.0f;
const CGFloat TKkDefaultPadPortraitKeyboardHeight   = 264.0f;
const CGFloat TKkDefaultPadLandscapeKeyboardHeight  = 352.0f;

const CGFloat TKkGroupedTableCellInset = 9.0f;
const CGFloat TKkGroupedPadTableCellInset = 42.0f;

const CGFloat TKkDefaulTKransitionDuration      = 0.3f;
const CGFloat TKkDefaultFasTKransitionDuration  = 0.2f;
const CGFloat TKkDefaultFlipTransitionDuration  = 0.7f;

BOOL TKIsRetina()
{
    return ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
NSString *TKOSVersion() {
  return [[UIDevice currentDevice] systemVersion];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKRuntimeOSVersionIsAtLeast(NSString *version) {

    return [TKOSVersion() versionStringCompare:version] != NSOrderedAscending;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKOSVersionIsAtLeast(float version) {
  // Floating-point comparison is preTKy bad, so let's cut it some slack with an epsilon.
  static const CGFloat kEpsilon = 0.0000001f;

#ifdef __IPHONE_5_0
  return 5.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_3
  return 4.3 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_2
  return 4.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_1
  return 4.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_4_0
  return 4.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_2
  return 3.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_1
  return 3.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_3_0
  return 3.0 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_2
  return 2.2 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_1
  return 2.1 - version >= -kEpsilon;
#endif
#ifdef __IPHONE_2_0
  return 2.0 - version >= -kEpsilon;
#endif
  return NO;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKIsKeyboardVisible() {
  // Operates on the assumption that the keyboard is visible if and only if there is a first
  // responder; i.e. a control responding to key events
  UIWindow* window = [UIApplication sharedApplication].keyWindow;
  return !![window findFirstResponder];
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKIsPhoneSupported() {
  NSString* deviceType = [UIDevice currentDevice].model;
  return [deviceType isEqualToString:@"iPhone"];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKIsMultiTaskingSupported() {
    UIDevice* device = [UIDevice currentDevice];
    BOOL backgroundSupported = NO;
    if ([device respondsToSelector:@selector(isMultitaskingSupported)]){
         backgroundSupported = device.multitaskingSupported;
    }
    return backgroundSupported;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKIsPad() {
#ifdef __IPHONE_3_2
		return UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad;
#else
		return NO;
#endif
}


///////////////////////////////////////////////////////////////////////////////////////////////////
UIDeviceOrientation TKDeviceOrientation() {
  UIDeviceOrientation orient = [[UIDevice currentDevice] orientation];
  if (UIDeviceOrientationUnknown == orient) {
    return UIDeviceOrientationPortrait;

  } else {
    return orient;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKDeviceOrientationIsPortrait() {
  UIDeviceOrientation orient = TKDeviceOrientation();

  switch (orient) {
    case UIInterfaceOrientationPortrait:
    case UIInterfaceOrientationPortraitUpsideDown:
      return YES;
    default:
      return NO;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKDeviceOrientationIsLandscape() {
  UIDeviceOrientation orient = TKDeviceOrientation();

  switch (orient) {
    case UIInterfaceOrientationLandscapeLeft:
    case UIInterfaceOrientationLandscapeRight:
      return YES;
    default:
      return NO;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
NSString* TKDeviceModelName() {
  size_t size;
  sysctlbyname("hw.machine", NULL, &size, NULL, 0);
  char *machine = malloc(size);
  sysctlbyname("hw.machine", machine, &size, NULL, 0);
  NSString *platform = [NSString stringWithCString:machine encoding:NSASCIIStringEncoding];
  free(machine);

  if ([platform isEqualToString:@"iPhone1,1"])    return @"iPhone 1G";
  if ([platform isEqualToString:@"iPhone1,2"])    return @"iPhone 3G";
  if ([platform isEqualToString:@"iPhone2,1"])    return @"iPhone 3GS";
  if ([platform isEqualToString:@"iPhone3,1"])    return @"iPhone 4 (GSM)";
  if ([platform isEqualToString:@"iPhone3,2"])    return @"iPhone 4 (CDMA)";
  if ([platform isEqualToString:@"iPod1,1"])      return @"iPod Touch 1G";
  if ([platform isEqualToString:@"iPod2,1"])      return @"iPod Touch 2G";
  if ([platform isEqualToString:@"iPod3,1"])      return @"iPod Touch 3G";
  if ([platform isEqualToString:@"iPod4,1"])      return @"iPod Touch 4G";
  if ([platform isEqualToString:@"iPad1,1"])      return @"iPad";
  if ([platform isEqualToString:@"iPad2,1"])      return @"iPad 2 (WiFi)";
  if ([platform isEqualToString:@"iPad2,2"])      return @"iPad 2 (GSM)";
  if ([platform isEqualToString:@"iPad2,3"])      return @"iPad 2 (CDMA)";
  if ([platform isEqualToString:@"i386"])         return @"Simulator";

  return platform;
}


///////////////////////////////////////////////////////////////////////////////////////////////////
BOOL TKIsSupportedOrientation(UIInterfaceOrientation orientation) {
  if (TKIsPad()) {
    return YES;

  } else {
    switch (orientation) {
      case UIInterfaceOrientationPortrait:
      case UIInterfaceOrientationPortraitUpsideDown:
      case UIInterfaceOrientationLandscapeLeft:
      case UIInterfaceOrientationLandscapeRight:
        return YES;
      default:
        return NO;
    }
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGAffineTransform TKRotateTransformForOrientation(UIInterfaceOrientation orientation) {
  if (orientation == UIInterfaceOrientationLandscapeLeft) {
    return CGAffineTransformMakeRotation(M_PI*1.5);

  } else if (orientation == UIInterfaceOrientationLandscapeRight) {
    return CGAffineTransformMakeRotation(M_PI/2);

  } else if (orientation == UIInterfaceOrientationPortraitUpsideDown) {
    return CGAffineTransformMakeRotation(-M_PI);

  } else {
    return CGAffineTransformIdentity;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect TKApplicationFrame() {
  CGRect frame = [UIScreen mainScreen].applicationFrame;
  return CGRectMake(0, 0, frame.size.width, frame.size.height);
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat TKToolbarHeightForOrientation(UIInterfaceOrientation orientation) {
  if (UIInterfaceOrientationIsPortrait(orientation) || TKIsPad()) {
    return TK_ROW_HEIGHT;

  } else {
    return TK_LANDSCAPE_TOOLBAR_HEIGHT;
  }
}


///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat TKKeyboardHeightForOrientation(UIInterfaceOrientation orientation) {
  if (TKIsPad()) {
    return UIInterfaceOrientationIsPortrait(orientation) ? TK_IPAD_KEYBOARD_HEIGHT
                                                         : TK_IPAD_LANDSCAPE_KEYBOARD_HEIGHT;

  } else {
    return UIInterfaceOrientationIsPortrait(orientation) ? TK_KEYBOARD_HEIGHT
                                                         : TK_LANDSCAPE_KEYBOARD_HEIGHT;
  }
}

CGFloat TKKeyboardHeight()
{
    return TKKeyboardHeightForOrientation(TKInterfaceOrientation());
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat TKGroupedTableCellInset() {
  return TKIsPad() ? TKkGroupedPadTableCellInset : TKkGroupedTableCellInset;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void TKAlert(NSString* message) {
  UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"警告", @"")
                                             message:message delegate:nil
                                             cancelButtonTitle:NSLocalizedString(@"确定", @"")
                                             otherButtonTitles:nil] autorelease];
  [alert show];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void TKAlertNoTitle(NSString* message) {
  UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:nil
                                                   message:message
                                                  delegate:nil
                                         cancelButtonTitle:NSLocalizedString(@"确定", @"")
                                         otherButtonTitles:nil] autorelease];
  [alert show];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
void TKAlertAppNameTitle(NSString* message) {
    
    NSString *appName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    UIAlertView* alert = [[[UIAlertView alloc] initWithTitle:appName
                                                     message:message 
                                                    delegate:nil
                                           cancelButtonTitle:NSLocalizedString(@"确定", @"")
                                           otherButtonTitles:nil] autorelease];
    [alert show];
}

UIInterfaceOrientation TKInterfaceOrientation() {
    UIInterfaceOrientation orient = [UIApplication sharedApplication].statusBarOrientation;
//    if (UIDeviceOrientationUnknown == orient) {
//        return UIInterfaceOrientationPortrait;
//    } else {
        return orient;
//    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGRect TKScreenBounds() {
    CGRect bounds = [UIScreen mainScreen].bounds;
    if (UIInterfaceOrientationIsLandscape(TKInterfaceOrientation())) {
        CGFloat width = bounds.size.width;
        bounds.size.width = bounds.size.height;
        bounds.size.height = width;
    }
    return bounds;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
CGFloat TKStatusHeight() {
    UIInterfaceOrientation orientation = TKInterfaceOrientation();
    if (orientation == UIInterfaceOrientationLandscapeLeft) {
        return [UIScreen mainScreen].applicationFrame.origin.x;
        
    } else if (orientation == UIInterfaceOrientationLandscapeRight) {
        return -[UIScreen mainScreen].applicationFrame.origin.x;
        
    } else {
        return [UIScreen mainScreen].applicationFrame.origin.y;
    }
}

CGFloat TKBarsHeight() {
    CGRect frame = [UIApplication sharedApplication].statusBarFrame;
    if (UIInterfaceOrientationIsPortrait(TKInterfaceOrientation())) {
        return frame.size.height + TK_ROW_HEIGHT;
        
    } else {
        return frame.size.width + (TKIsPad() ? TK_ROW_HEIGHT : TK_LANDSCAPE_TOOLBAR_HEIGHT);
    }
}



