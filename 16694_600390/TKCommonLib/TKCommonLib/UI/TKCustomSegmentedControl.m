//
//  CustomSegmentedControl.m
//  WoodUINavigation
//
//  Created by Peter Boctor on 12/13/10.
//
// Copyright (c) 2011 Peter Boctor
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
// THE SOFTWARE
//

#import "TKCustomSegmentedControl.h"

@interface TKCustomSegmentedControl()

@property (nonatomic, retain) NSArray *items;

- (UIButton*) buttonWithTitle:(NSString *)title atIndex:(NSUInteger)segmentIndex;
-(void) dimAllButtonsExcept:(UIButton*)selectedButton;
+ (UIButton *)woodButtonWithText:(NSString*)buttonText stretch:(CapLocation)location;
@end

@implementation TKCustomSegmentedControl
@synthesize buttons;
@synthesize delegate;
@synthesize items;
@synthesize selectedIndex;

- (id) initWithSegmentItems:(NSArray *)theItems
{
    if ((self = [super init]))
    {        
        selectedIndex = 0;
        self.items = theItems;
        NSUInteger segmentCount = items.count;
        
        UIImage* dividerImage = [UIImage imageNamed:@"TKCommonLib.bundle/TKCustomSegmentedControl/seg-divider.png"];
        CGSize segmentsize = CGSizeMake(BUTTON_SEGMENT_WIDTH, dividerImage.size.height);
        
        // Adjust our width based on the number of segments & the width of each segment and the sepearator
        self.frame = CGRectMake(0, 0, (segmentsize.width * segmentCount) + (dividerImage.size.width * (segmentCount - 1)), segmentsize.height);
        
        // Initalize the array we use to store our buttons
        buttons = [[NSMutableArray alloc] initWithCapacity:segmentCount];
        
        // horizontalOffset tracks the proper x value as we add buttons as subviews
        CGFloat horizontalOffset = 0;
        
        // Iterate through each segment
        for (NSUInteger i = 0 ; i < segmentCount ; i++)
        {
            // Ask the delegate to create a button
            UIButton* button = [self buttonWithTitle:[items objectAtIndex:i] atIndex:i];
            
            // Register for touch events
            [button addTarget:self action:@selector(touchDownAction:) forControlEvents:UIControlEventTouchDown];
            [button addTarget:self action:@selector(touchUpInsideAction:) forControlEvents:UIControlEventTouchUpInside];
            [button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchUpOutside];
            [button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchDragOutside];
            [button addTarget:self action:@selector(otherTouchesAction:) forControlEvents:UIControlEventTouchDragInside];
            
            // Add the button to our buttons array
            [buttons addObject:button];
            
            // Set the button's x offset
            button.frame = CGRectMake(horizontalOffset, 0.0, button.frame.size.width, button.frame.size.height);
            
            // Add the button as our subview
            [self addSubview:button];
            
            // Add the divider unless we are at the last segment
            if (i != segmentCount - 1)
            {
                UIImageView* divider = [[[UIImageView alloc] initWithImage:dividerImage] autorelease];
                divider.frame = CGRectMake(horizontalOffset + segmentsize.width, 0.0, dividerImage.size.width, dividerImage.size.height);
                [self addSubview:divider];
            }
            
            // Advance the horizontal offset
            horizontalOffset = horizontalOffset + segmentsize.width + dividerImage.size.width;
        }
    }
    
    return self;
}

- (void)touchDownAction:(UIButton*)button
{
    [self dimAllButtonsExcept:button];
    
    if ([delegate respondsToSelector:@selector(segmentedControl:touchDownAtSegmentIndex:)])
        [delegate segmentedControl:self touchDownAtSegmentIndex:[buttons indexOfObject:button]];
}

- (void)touchUpInsideAction:(UIButton*)button
{
    [self dimAllButtonsExcept:button];
    
    if ([delegate respondsToSelector:@selector(segmentedControl:touchUpInsideSegmentIndex:)])
        [delegate segmentedControl:self touchUpInsideSegmentIndex:[buttons indexOfObject:button]];
}

- (void)otherTouchesAction:(UIButton*)button
{
    [self dimAllButtonsExcept:button];
}

- (void)dealloc
{
    self.items = nil;
    [buttons release];
    [super dealloc];
}

- (UIButton*) buttonWithTitle:(NSString *)title atIndex:(NSUInteger)segmentIndex
{
    CapLocation location;
    if (segmentIndex == 0)
        location = CapLeft;
    else if (segmentIndex == items.count - 1)
        location = CapRight;
    else
        location = CapMiddle;
    
    UIButton* button = [TKCustomSegmentedControl woodButtonWithText:title stretch:location];
    if (segmentIndex == 0)
        button.selected = YES;
    return button;
}

-(void) dimAllButtonsExcept:(UIButton*)selectedButton
{
    for (NSUInteger i = 0; i < [buttons count]; i++)
    {
        UIButton* button = [buttons objectAtIndex:i];
        if (button == selectedButton)
        {
            selectedIndex = i;
            button.selected = YES;
            button.highlighted = button.selected ? NO : YES;
        }
        else
        {
            button.selected = NO;
            button.highlighted = NO;
        }
    }
}

+ (UIImage *)image:(UIImage *)image withCap:(CapLocation)location capWidth:(NSUInteger)capWidth buttonWidth:(NSUInteger)buttonWidth {
    UIGraphicsBeginImageContextWithOptions(CGSizeMake(buttonWidth, image.size.height), NO, 0.0);
    
    if (location == CapLeft)
        // To draw the left cap and not the right, we start at 0, and increase the width of the image by the cap width to push the right cap out of view
        [image drawInRect:CGRectMake(0, 0, buttonWidth + capWidth, image.size.height)];
    else if (location == CapRight)
        // To draw the right cap and not the left, we start at negative the cap width and increase the width of the image by the cap width to push the left cap out of view
        [image drawInRect:CGRectMake(0.0-capWidth, 0, buttonWidth + capWidth, image.size.height)];
    else if (location == CapMiddle)
        // To draw neither cap, we start at negative the cap width and increase the width of the image by both cap widths to push out both caps out of view
        [image drawInRect:CGRectMake(0.0-capWidth, 0, buttonWidth + (capWidth * 2), image.size.height)];
    
    UIImage* resultImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return resultImage;
}

+ (UIButton *)woodButtonWithText:(NSString*)buttonText stretch:(CapLocation)location {
    UIImage* buttonImage = nil;
    UIImage* buttonPressedImage = nil;
    NSUInteger buttonWidth = 0;
    if (location == CapLeftAndRight) {
        buttonWidth = BUTTON_WIDTH;
        buttonImage = [[UIImage imageNamed:@"TKCommonLib.bundle/TKCustomSegmentedControl/seg-button.png"] stretchableImageWithLeftCapWidth:CAP_WIDTH topCapHeight:0.0];
        buttonPressedImage = [[UIImage imageNamed:@"TKCommonLib.bundle/TKCustomSegmentedControl/seg-button-press.png"] stretchableImageWithLeftCapWidth:CAP_WIDTH topCapHeight:0.0];
    } else {
        buttonWidth = BUTTON_SEGMENT_WIDTH;
        
        buttonImage = [TKCustomSegmentedControl image:[[UIImage imageNamed:@"TKCommonLib.bundle/TKCustomSegmentedControl/seg-button.png"] stretchableImageWithLeftCapWidth:CAP_WIDTH topCapHeight:0.0] withCap:location capWidth:CAP_WIDTH buttonWidth:buttonWidth];
        buttonPressedImage = [TKCustomSegmentedControl image:[[UIImage imageNamed:@"TKCommonLib.bundle/TKCustomSegmentedControl/seg-button-press.png"] stretchableImageWithLeftCapWidth:CAP_WIDTH topCapHeight:0.0] withCap:location capWidth:CAP_WIDTH buttonWidth:buttonWidth];
    }
    
    UIButton* button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0.0, 0.0, buttonWidth, buttonImage.size.height);
    button.titleLabel.font = [UIFont boldSystemFontOfSize:[UIFont smallSystemFontSize]];
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.shadowOffset = CGSizeMake(0,-1);
    button.titleLabel.shadowColor = [UIColor darkGrayColor];
    
    [button setTitle:buttonText forState:UIControlStateNormal];
    [button setBackgroundImage:buttonImage forState:UIControlStateNormal];
    [button setTitleColor:RGBACOLOR(250.0, 250.0, 250.0, 1.0) forState:UIControlStateNormal];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateSelected];
    [button setBackgroundImage:buttonPressedImage forState:UIControlStateSelected];
    button.adjustsImageWhenHighlighted = NO;
    return button;
}

@end
