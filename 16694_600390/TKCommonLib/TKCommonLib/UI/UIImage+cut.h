//
//  UIImage+cut.h
//  VoiceChina
//
//  Created by wangguangzhao on 13-2-24.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIImage+resize.h"

typedef enum
{
    ImageCutFromTop = 0,
    ImageCutFromCenter = 1,
    ImageCutFromBottom = 2
}ImageCutMode;

@interface UIImage (cut)

- (UIImage *)cutInFrame:(CGRect)targetFrame;
+(UIImage *)imageZoom:(UIImage *)image andLength:(CGFloat)length;
- (UIImage *)cutInFrame:(CGRect)targetFrame cutMode:(ImageCutMode)cutMode;
@end
