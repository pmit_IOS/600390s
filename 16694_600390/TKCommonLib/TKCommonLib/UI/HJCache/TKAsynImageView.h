//
//  HHAsynImageView.h
//  HHCommonLib
//
//  Created by luobin on 4/12/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "HJManagedImageV.h"

@class TKAsynImageView;

@protocol TKAsynImageViewDelegate <NSObject>

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView;

@end
@interface TKAsynImageView : HJManagedImageV {
    UIImageView *adornImageView;
    id<TKAsynImageViewDelegate> asynImageDelegate;
}

@property (nonatomic, retain) UIImage *adorn;//装饰

@property (nonatomic, assign) UIEdgeInsets imageInset;

@property (nonatomic, retain) NSURL * bmiddle_pic_url;

@property (nonatomic, retain) NSURL * original_pic_url;

@property (nonatomic, retain) id<TKAsynImageViewDelegate> asynImageDelegate;

- (void)loadImage;

@end
