//
//  HJManagedImageV.m
//  hjlib
//
//  Copyright Hunter and Johnson 2009, 2010, 2011
//  HJCache may be used freely in any iOS or Mac application free or commercial.
//  May be redistributed as source code only if all the original files are included.
//  See http://www.markj.net/hjcache-iphone-image-cache/

#import "HJManagedImageV.h"
#import "TKGlobalUICommon.h"

@implementation HJManagedImageV

@synthesize oid;
@synthesize url;
@synthesize moHandler;
@synthesize defaultImage;
@synthesize adjustsImageWhenHighlighted;
@synthesize callbackOnSetImage;
@synthesize callbackOnCancel;
@synthesize imageView;
@synthesize modification;
@synthesize showLoadingWheel;
@synthesize index;

- (void)initData
{
    isCancelled=NO;
    modification=0;
    url=nil;
    index = -1;
    self.adjustsImageWhenHighlighted = YES;
    
    self.backgroundColor = [UIColor whiteColor];
    
    self.imageView = [[[UIImageView alloc] init] autorelease];
    self.imageView.backgroundColor = [UIColor clearColor];
    imageView.contentMode = UIViewContentModeScaleToFill;
    //    imageView.autoresizingMask = ( UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight );
    imageView.frame = self.bounds;
    [self addSubview:imageView];
    
    loadingWheel = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    loadingWheel.center = imageView.center;
    loadingWheel.hidesWhenStopped = YES;
    [self addSubview:loadingWheel];
    
    _maskView = [[UIView alloc] init];
    _maskView.backgroundColor = [UIColor colorWithWhite:0.2 alpha:0.5];
    _maskView.alpha = 0;
    
    [self addSubview:_maskView];
    
    self.userInteractionEnabled = NO; //because want to treat it like a UIImageView. Just turn this back on if you want to catch taps.
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self initData];
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
		[self initData];
    }
    return self;
}

- (void)dealloc {
	[self clear];
    TKRELEASE(_maskView);
    TKRELEASE(loadingWheel);
	self.callbackOnCancel=nil;
	self.callbackOnSetImage=nil;
    self.defaultImage = nil;
    [super dealloc];
	//NSLog(@"ManagedImage dealloc");
}


-(void) clear {
	[self.moHandler removeUser:self];
	self.moHandler=nil;
	self.image = nil;
	self.imageView.image=nil;
	self.oid=nil;
	self.url=nil;
    _maskView.alpha = 0;
    if (defaultImage) {
        self.imageView.image=defaultImage;
    }
    
    [loadingWheel stopAnimating];
}

-(void) changeManagedObjStateFromLoadedToReady {
	//NSLog(@"managedStateReady %@",managedState);
	if (moHandler.moData) {
		moHandler.managedObj=[UIImage imageWithData:moHandler.moData];
        
	} else if (moHandler.moReadyDataFilename) {
		moHandler.managedObj=[UIImage imageWithContentsOfFile:moHandler.moReadyDataFilename];
        
	} else {
		//error? 
        
		NSLog(@"HJManagedImageV error in changeManagedObjStateFromLoadedToReady ?");
	}
}

-(void) managedObjFailed {
//	NSLog(@"moHandlerFailed %@",moHandler);
	[image release];
	image = nil;
}

-(void) managedObjReady {
	//NSLog(@"moHandlerReady %@",moHandler);
    
    UIImage *theImage = moHandler.managedObj;
    if (theImage == nil) {
        return;
    }
    
    //如果为高清屏，且图片足够大，自动转换成高清图片
    if (TKIsRetina()
        && theImage.scale < 2.0f
        && theImage.size.width >= self.frame.size.width * 2
        && theImage.size.height >= self.frame.size.height * 2) {
        theImage = [UIImage imageWithCGImage:theImage.CGImage scale:2.0f orientation:theImage.imageOrientation];
    }
    
    if (self.moHandler.request != nil) {
        [self setImage:theImage animated:YES];
    } else {
        //        [self performSelector:@selector(setImage:) withObject:moHandler.managedObj  afterDelay:0.1];
        [self setImage:theImage];
    }
}

-(UIImage*) image {
	return image; 
}

-(void) markCancelled {
	isCancelled = YES;
	[callbackOnCancel managedImageCancelled:self];
}

-(UIImage*) modifyImage:(UIImage*)theImage modification:(int)mod {
	return theImage;
}

-(void) setImage:(UIImage*)theImage modification:(int)mod {
	if (mod == modification) {
		[self setImage:theImage];
	} else {
		UIImage* modified = [self modifyImage:theImage modification:(int)mod];
		[self setImage:modified];
	}
}

- (void)setImageAnimated:(UIImage*)theImage
{
    [self setImage:theImage animated:YES];
}

-(void) setImage:(UIImage*)theImage animated:(BOOL)animated{
	if (theImage==image) {
		//when the same image is on the screen multiple times, an image that is alredy set might be set again with the same image.
		return; 
	}
	[theImage retain];
	[image release];
	image = theImage;
    
    self.imageView.image = theImage;
    
    if (animated && self.defaultImage == nil) {
        imageView.alpha = 0;
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
        [UIView setAnimationDuration:0.25];
        imageView.alpha = 1;
        [UIView commitAnimations];
    }
    
	[loadingWheel stopAnimating];
	self.hidden=NO;
	if (image!=nil) {
		[callbackOnSetImage managedImageSet:self];
	}
}

- (void)setDefaultImage:(UIImage *)theDefaultImage
{
    if (defaultImage != theDefaultImage) {
        [defaultImage release];
        defaultImage = [theDefaultImage retain];
        if (image == nil) {
            self.imageView.image = defaultImage;
        }
    }
}

-(void) setImage:(UIImage*)theImage
{
    [self setImage:theImage animated:NO];
}

- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents
{
    [super addTarget:target action:action forControlEvents:controlEvents];
    
    self.userInteractionEnabled = YES; //because it's NO in the initializer, but if we want to get a callback on tap, 
    //then need to get touch events.
}

- (void)layoutSubviews
{
    if (self.imageView.superview == self) {
        self.imageView.frame = self.bounds;
    }
    _maskView.frame = self.bounds;
    loadingWheel.center = imageView.center;
}

///////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark UIControl


///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setHighlighted:(BOOL)highlighted {
    [super setHighlighted:highlighted];
    [self setNeedsDisplay];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    [self setNeedsDisplay];
}

///////////////////////////////////////////////////////////////////////////////////////////////////
- (void)setEnabled:(BOOL)enabled {
    [super setEnabled:enabled];
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    _maskView.alpha = (adjustsImageWhenHighlighted && self.state == UIControlStateHighlighted);
}

@end
