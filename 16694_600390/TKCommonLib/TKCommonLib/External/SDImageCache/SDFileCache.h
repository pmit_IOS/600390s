/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import <Foundation/Foundation.h>
#import "SDFileCacheDelegate.h"

@interface SDFileCache : NSObject
{
    NSMutableDictionary *memCache;
    NSString *diskCachePath;
    NSOperationQueue *cacheInQueue, *cacheOutQueue;
}

+ (SDFileCache *)sharedFileCache;
- (void)storeFile:(NSData *)fileData forKey:(NSString *)key withFilePath:(NSString *)filePath;
- (void)storeFile:(NSData *)fileData forKey:(NSString *)key toDisk:(BOOL)toDisk withFilePath:(NSString *)filePath;
- (NSString *)cachePathForKey:(NSString *)key;

- (NSData *)fileFromKey:(NSString *)key withFilePath:(NSString *)filePath;
- (NSData *)fileFromKey:(NSString *)key fromDisk:(BOOL)fromDisk withFilePath:(NSString *)filePath;
- (void)queryDiskCacheForKey:(NSString *)key delegate:(id <SDFileCacheDelegate>)delegate userInfo:(NSDictionary *)info;

- (void)removeFileForKey:(NSString *)key withFilePath:(NSString *)filePath;
- (void)clearMemory;
- (void)clearDisk;
- (void)cleanDisk;
- (int)getSize;

@end
