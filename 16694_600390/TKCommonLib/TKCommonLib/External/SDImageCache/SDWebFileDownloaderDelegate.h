/*
 * This file is part of the SDWebImage package.
 * (c) Olivier Poitrey <rs@dailymotion.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

#import "SDWebImageCompat.h"

@class SDWebFileDownloader;

@protocol SDWebFileDownloaderDelegate <NSObject>

@optional

- (void)fileDownloaderDidFinish:(SDWebFileDownloader *)downloader;
- (void)fileDownloader:(SDWebFileDownloader *)downloader didFinishWithFile:(NSData *)data;
- (void)fileDownloader:(SDWebFileDownloader *)downloader didFailWithError:(NSError *)error;

@end
