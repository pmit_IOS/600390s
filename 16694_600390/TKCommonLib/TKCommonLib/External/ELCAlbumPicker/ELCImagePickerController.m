//
//  ELCImagePickerController.m
//  ELCImagePickerDemo
//
//  Created by Collin Ruffenach on 9/9/10.
//  Copyright 2010 ELC Technologies. All rights reserved.
//

#import "ELCImagePickerController.h"
#import "ELCAsset.h"
#import "ELCAssetCell.h"
#import "ELCAssetTablePicker.h"
#import "ELCAlbumPickerController.h"

@implementation ELCImagePickerController

@synthesize delegate;

-(void)cancelImagePicker {
    if([delegate respondsToSelector:@selector(elcImagePickerControllerDidCancel:)]) {
        [delegate performSelector:@selector(elcImagePickerControllerDidCancel:) withObject:self];
    }
}

-(void)selectedAssets:(NSArray*)_assets completion:(void (^)(void))completion {
    NSMutableArray *returnArray = [[[NSMutableArray alloc] init] autorelease];
    NSString *systemVersion=[[UIDevice currentDevice] systemVersion];
    int n = [systemVersion intValue];
    ALAssetRepresentation *rep;
    for(ALAsset *asset in _assets) {
        rep = [asset defaultRepresentation];
        NSMutableDictionary *workingDictionary = [[NSMutableDictionary alloc] init];
        [workingDictionary setObject:[asset valueForProperty:ALAssetPropertyType] forKey:UIImagePickerControllerMediaType];
        if (n < 5) {
            [workingDictionary setObject:[UIImage imageWithCGImage:[[asset defaultRepresentation] fullScreenImage] scale:1.0 orientation:[[asset valueForProperty:ALAssetPropertyOrientation] intValue]] forKey:UIImagePickerControllerOriginalImage];
        }
        else{
            
            [workingDictionary setObject:[UIImage imageWithCGImage:[rep fullScreenImage]] forKey:UIImagePickerControllerOriginalImage];
        }
        [workingDictionary setObject:[[asset valueForProperty:ALAssetPropertyURLs] valueForKey:[[[asset valueForProperty:ALAssetPropertyURLs] allKeys] objectAtIndex:0]] forKey:UIImagePickerControllerReferenceURL];
        [workingDictionary setObject:[rep metadata] forKey:UIImagePickerControllerMediaMetadata];
        
        [returnArray addObject:workingDictionary];
        [workingDictionary release];
    }
    
    if([delegate respondsToSelector:@selector(elcImagePickerController:didFinishPickingMediaWithInfo:)]) {
        [delegate performSelector:@selector(elcImagePickerController:didFinishPickingMediaWithInfo:) withObject:self withObject:[NSArray arrayWithArray:returnArray]];
    }

    if (completion) {
        completion();
    }
}

-(void)selectedAssets:(NSArray*)_assets {

    [self selectedAssets:_assets completion:nil];
}

#pragma mark -
#pragma mark Memory management

- (void)didReceiveMemoryWarning {    
    NSLog(@"ELC Image Picker received memory warning.");
    
    [super didReceiveMemoryWarning];
}

- (void)viewDidUnload {
    [super viewDidUnload];
}


- (void)dealloc {
    NSLog(@"deallocing ELCImagePickerController");
    [super dealloc];
}

@end
