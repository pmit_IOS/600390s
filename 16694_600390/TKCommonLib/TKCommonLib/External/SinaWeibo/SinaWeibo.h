//
//  SinaWeibo.h
//  sinaweibo_ios_sdk
//
//  Created by Wade Cheng on 4/19/12.
//  Copyright (c) 2012 SINA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SinaWeiboAuthorizeView.h"
#import "SinaWeiboRequest.h"

typedef enum  {
    
    ZSTWeiBoStatuses           = 0,
    ZSTWeiBoComments           = 1,   //评论
    ZSTWeiBoReposts            = 2,   //转发
    ZSTWeiBoReply              = 3,   //回复
    ZSTWeiBoFav                = 4,   //收藏
    
    
} ZSTWeiBoDataType;


@protocol SinaWeiboDelegate;

@interface SinaWeibo : NSObject <SinaWeiboAuthorizeViewDelegate, SinaWeiboRequestDelegate>
{
    NSString *userID;
    NSString *accessToken;
    NSDate *expirationDate;
    id<SinaWeiboDelegate> delegate;
    
    NSString *appKey;
    NSString *appSecret;
    NSString *appRedirectURI;
    NSString *ssoCallbackScheme;
    
    SinaWeiboRequest *request;
    NSMutableSet *requests;
    BOOL ssoLoggingIn;
}

@property (nonatomic, copy) NSString *userID;
@property (nonatomic, copy) NSString *accessToken;
@property (nonatomic, copy) NSDate *expirationDate;
@property (nonatomic, copy) NSString *refreshToken;
@property (nonatomic, copy) NSString *ssoCallbackScheme;
@property (nonatomic, assign) id<SinaWeiboDelegate> delegate;

- (id)initWithAppKey:(NSString *)appKey appSecret:(NSString *)appSecrect
      appRedirectURI:(NSString *)appRedirectURI
         andDelegate:(id<SinaWeiboDelegate>)delegate;

- (id)initWithAppKey:(NSString *)appKey appSecret:(NSString *)appSecrect
      appRedirectURI:(NSString *)appRedirectURI
   ssoCallbackScheme:(NSString *)ssoCallbackScheme
         andDelegate:(id<SinaWeiboDelegate>)delegate;

- (void)applicationDidBecomeActive;
- (BOOL)handleOpenURL:(NSURL *)url;

// Log in using OAuth Web authorization.
// If succeed, sinaweiboDidLogIn will be called.
- (void)logIn;

// Log out.
// If succeed, sinaweiboDidLogOut will be called.
- (void)logOut;

// Check if user has logged in, or the authorization is expired.
- (BOOL)isLoggedIn;
- (BOOL)isAuthorizeExpired;


// isLoggedIn && isAuthorizeExpired
- (BOOL)isAuthValid;

- (SinaWeiboRequest *)requestWithMethodName:(NSString *)methodName
                                     params:(NSMutableDictionary *)params
                                 httpMethod:(NSString *)httpMethod
                                     target:(id)target
                                   selector:(SEL)selector
                                   userInfo:(id)userInfo
                                       flag:(BOOL)flag;

- (SinaWeiboRequest*)requestWithURL:(NSString *)url
                             params:(NSMutableDictionary *)params
                         httpMethod:(NSString *)httpMethod
                           delegate:(id<SinaWeiboRequestDelegate>)delegate;

- (SinaWeiboRequest *)requestWithURL:(NSString *)url
                              params:(NSMutableDictionary *)params
                          httpMethod:(NSString *)httpMethod
                            delegate:(id<SinaWeiboRequestDelegate>)_delegate
                                flag:(BOOL)flag;


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 * @获取用户信息
 * @uid 用户id
 */
- (void)getUserInfo:(NSString *)uid;

/**
 * @ 发送微博
 * @text  文字
 * @image 图片
 */
- (void)sendWeiBoWithText:(NSString *)text image:(UIImage *)image;

/**
 * @创建关注对象
 * @uid 需要关注的用户ID。
 * @screenName 需要关注的用户昵称。@"中国好声音客户端"
 */
- (void)createFriendships:(NSString *)uid name:(NSString *)screenName;
/**
 * @判断是否已经关注
 * @sourceId 用户id。
 */

- (void)showFriendships:(NSString *)sourceId;


// Send a Weibo, to which you can attach an image.
-(void)sendCommentComment:(NSString *)message ID:(NSString *)ID;//comments/create 发表评论
- (void)sendReportMessage:(NSString *)message ID:(NSString *)ID; //statuses/repost;转发微博
- (void)sendReplyMessage:(NSString *)message ID:(NSString *)ID CID:(NSString *)cid; //comments/reply;回复微博

- (void)getPublicTimelineSinceID:(int)updateID; // statuses/public_timeline
- (void)getCommentList:(NSString *)ID sinceID:(int) updateID startingAtPage:(int)pageNum count:(int)count;//comments/show //某条微博的评论列表//默认更新每页20条
- (void)getForwardList:(NSString *)ID sinceID:(int) updateID startingAtPage:(int)pageNum count:(int)count;// statuses/repost_timeline  //某条微博的转发列表//默认更新每页20条
//- (void)getForwardList:(NSString *)ID sinceID:(int) updateID startingAtPage:(int)pageNum count:(int)count;// statuses/repost_timeline  //某条微博的转发列表//默认更新每页20条
- (void)getFavorites:(NSString *)ID;// favorites/show  //某条微博的收藏信息


- (void)markUpdate:(NSString *)updateID asFavorite:(BOOL)flag;//favorites/create 收藏微博   favorites/show

+ (NSString*)gsid;

- (void)closeAllConnections;



@end


/**
 * @description 第三方应用需实现此协议，登录时传入此类对象，用于完成登录结果的回调
 */
@protocol SinaWeiboDelegate <NSObject>

@optional

- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaweibo;
- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo;
- (void)sinaweiboLogInDidCancel:(SinaWeibo *)sinaweibo;
- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error;
- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getUserInfoSucceed:(NSDictionary *)result;
- (void)getUserInfoFailured;


- (void)sendWeiBoInfoSucceed:(NSDictionary *)result;
- (void)sendWeiBoInfoFailured;

- (void)createFriendshipsInfoSucceed:(NSDictionary *)result;
- (void)createFriendshipsInfoFailured;

- (void)showFriendshipsInfoSucceed:(NSDictionary *)result;
- (void)showFriendshipsInfoFailured;

- (void)engine:(SinaWeibo *)engine parsingSucceededForStatues:(id)result;
- (void)engine:(SinaWeibo *)engine parsingSucceededForComments:(id)result;
- (void)engine:(SinaWeibo *)engine parsingSucceededForReports:(id)result;
- (void)engine:(SinaWeibo *)engine parsingSucceededForFav:(id)result;
- (void)engine:(SinaWeibo *)engine parsingSucceededForStatue:(BOOL)result;
- (void)engine:(SinaWeibo *)engine parsingSucceededForScreenName:(NSString *)result;


@end

extern BOOL SinaWeiboIsDeviceIPad();
