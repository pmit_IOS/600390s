//
//  SinaWeibo.m
//  sinaweibo_ios_sdk
//
//  Created by Wade Cheng on 4/19/12.
//  Copyright (c) 2012 SINA. All rights reserved.
//

#import "SinaWeibo.h"
#import "SinaWeiboRequest.h"
#import "SinaWeiboAuthorizeView.h"
#import "SinaWeiboConstants.h"
#import "TKDebug.h"
#import "ZSTWeiBoInfo.h"
#import "NSString_HTML.h"

#define DEFAULT_WB_COUNT 20
#define kRequestUserInfo @"kZSTRequestUserInfo"

static NSString* _gsid;

@interface SinaWeibo ()

@property (nonatomic, copy) NSString *appKey;
@property (nonatomic, copy) NSString *appSecret;
@property (nonatomic, copy) NSString *appRedirectURI;

@end

@implementation SinaWeibo

@synthesize userID;
@synthesize accessToken;
@synthesize expirationDate;
@synthesize refreshToken;
@synthesize ssoCallbackScheme;
@synthesize delegate;
@synthesize appKey;
@synthesize appSecret;
@synthesize appRedirectURI;

#pragma mark - Memory management

/**
 * @description 初始化构造函数，返回采用默认sso回调地址构造的SinaWeibo对象
 * @param _appKey: 分配给第三方应用的appkey
 * @param _appSecrect: 分配给第三方应用的appsecrect
 * @param _appRedirectURI: 微博开放平台中授权设置的应用回调页
 * @return SinaWeibo对象
 */
- (id)initWithAppKey:(NSString *)_appKey appSecret:(NSString *)_appSecrect
      appRedirectURI:(NSString *)_appRedirectURI
         andDelegate:(id<SinaWeiboDelegate>)_delegate
{
    return [self initWithAppKey:_appKey appSecret:_appSecrect appRedirectURI:_appRedirectURI ssoCallbackScheme:nil andDelegate:_delegate];
}


/**
 * @description 初始化构造函数，返回采用默认sso回调地址构造的SinaWeibo对象
 * @param _appKey: 分配给第三方应用的appkey
 * @param _appSecrect: 分配给第三方应用的appsecrect
 * @param _ssoCallbackScheme: sso回调地址，此值应与URL Types中定义的保持一致
 *            若为nil,则初始化为默认格式 sinaweibosso.your_app_key;
 * @param _appRedirectURI: 微博开放平台中授权设置的应用回调页
 * @return SinaWeibo对象
 */
- (id)initWithAppKey:(NSString *)_appKey appSecret:(NSString *)_appSecrect
      appRedirectURI:(NSString *)_appRedirectURI
   ssoCallbackScheme:(NSString *)_ssoCallbackScheme
         andDelegate:(id<SinaWeiboDelegate>)_delegate
{
    if ((self = [super init]))
    {
        self.appKey = _appKey;
        self.appSecret = _appSecrect;
        self.appRedirectURI = _appRedirectURI;
        self.delegate = _delegate;
        
        if (!_ssoCallbackScheme)
        {
            _ssoCallbackScheme = [NSString stringWithFormat:@"sinaweibosso.%@://", self.appKey];
        }
        self.ssoCallbackScheme = _ssoCallbackScheme;
        
        requests = [[NSMutableSet alloc] init];
    }
    
    return self;
}

- (void)dealloc
{
    delegate = nil;
    
    for (SinaWeiboRequest* _request in requests)
    {
        _request.sinaweibo = nil;
    }
    
    [request disconnect];
    [request release], request = nil;
    [userID release], userID = nil;
    [accessToken release], accessToken = nil;
    [expirationDate release], expirationDate = nil;
    [appKey release], appKey = nil;
    [appSecret release], appSecret = nil;
    [appRedirectURI release], appRedirectURI = nil;
    [ssoCallbackScheme release], ssoCallbackScheme = nil;
    
    [super dealloc];
}

/**
 * @description 清空认证信息
 */
- (void)removeAuthData
{
    self.accessToken = nil;
    self.userID = nil;
    self.expirationDate = nil;
    
    NSHTTPCookieStorage* cookies = [NSHTTPCookieStorage sharedHTTPCookieStorage];
    NSArray* sinaweiboCookies = [cookies cookiesForURL:
                                [NSURL URLWithString:@"https://open.weibo.cn"]];
    
    for (NSHTTPCookie* cookie in sinaweiboCookies)
    {
        [cookies deleteCookie:cookie];
    }
}

#pragma mark - Private methods

- (void)requestAccessTokenWithAuthorizationCode:(NSString *)code
{
    NSDictionary *params = [NSDictionary dictionaryWithObjectsAndKeys:
                            self.appKey, @"client_id",
                            self.appSecret, @"client_secret",
                            @"authorization_code", @"grant_type",
                            self.appRedirectURI, @"redirect_uri",
                            code, @"code", nil];
    [request disconnect];
    [request release], request = nil;
    
    request = [[SinaWeiboRequest requestWithURL:kSinaWeiboWebAccessTokenURL
                                     httpMethod:@"POST"
                                         params:params
                                       delegate:self] retain];
    
    [request connect];
}

- (void)requestDidFinish:(SinaWeiboRequest *)_request
{
    [requests removeObject:_request];
    _request.sinaweibo = nil;
}

- (void)requestDidFailWithInvalidToken:(NSError *)error
{
    if ([delegate respondsToSelector:@selector(sinaweibo:accessTokenInvalidOrExpired:)])
    {
        [delegate sinaweibo:self accessTokenInvalidOrExpired:error];
    }
}

- (void)notifyTokenExpired:(id<SinaWeiboRequestDelegate>)requestDelegate
{
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                              @"Token expired", NSLocalizedDescriptionKey, nil];
    
    NSError *error = [NSError errorWithDomain:kSinaWeiboSDKErrorDomain
                                         code:21315
                                     userInfo:userInfo];
    
    if ([delegate respondsToSelector:@selector(sinaweibo:accessTokenInvalidOrExpired:)])
    {
        [delegate sinaweibo:self accessTokenInvalidOrExpired:error];
    }
    
    if ([requestDelegate respondsToSelector:@selector(request:didFailWithError:)]) 
	{
		[requestDelegate request:nil didFailWithError:error];
	}
}

- (void)logInDidCancel
{
    if ([delegate respondsToSelector:@selector(sinaweiboLogInDidCancel:)])
    {
        [delegate sinaweiboLogInDidCancel:self];
    }
}

- (void)logInDidFinishWithAuthInfo:(NSDictionary *)authInfo
{
    NSString *access_token = [authInfo objectForKey:@"access_token"];
    NSString *uid = [authInfo objectForKey:@"uid"];
    NSString *remind_in = [authInfo objectForKey:@"remind_in"];
    NSString *refresh_token = [authInfo objectForKey:@"refresh_token"];
    if (access_token && uid)
    {
        if (remind_in != nil)
        {
            int expVal = [remind_in intValue];
            if (expVal == 0)
            {
                self.expirationDate = [NSDate distantFuture];
            }
            else
            {
                self.expirationDate = [NSDate dateWithTimeIntervalSinceNow:expVal];
            } 
        } 
        
        self.accessToken = access_token;
        self.userID = uid;
        self.refreshToken = refresh_token;
        
        if ([delegate respondsToSelector:@selector(sinaweiboDidLogIn:)])
        {
            [delegate sinaweiboDidLogIn:self];
        }
    }
}

- (void)logInDidFailWithErrorInfo:(NSDictionary *)errorInfo
{
    NSString *error_code = [errorInfo objectForKey:@"error_code"];
    if ([error_code isEqualToString:@"21330"])
    {
        [self logInDidCancel];
    }
    else
    {
        if ([delegate respondsToSelector:@selector(sinaweibo:logInDidFailWithError:)])
        {
            NSString *error_description = [errorInfo objectForKey:@"error_description"];
            NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      errorInfo, @"error",
                                      error_description, NSLocalizedDescriptionKey, nil];
            NSError *error = [NSError errorWithDomain:kSinaWeiboSDKErrorDomain 
                                                 code:[error_code intValue]
                                             userInfo:userInfo];
            [delegate sinaweibo:self logInDidFailWithError:error];
        }
        
    }
}

#pragma mark - Validation

/**
 * @description 判断是否登录
 * @return YES为已登录；NO为未登录
 */
- (BOOL)isLoggedIn
{
    return userID && accessToken && expirationDate;
}

/**
 * @description 判断登录是否过期
 * @return YES为已过期；NO为未为期
 */
- (BOOL)isAuthorizeExpired
{
    NSDate *now = [NSDate date];
    return ([now compare:expirationDate] == NSOrderedDescending);
}


/**
 * @description 判断登录是否有效，当已登录并且登录未过期时为有效状态
 * @return YES为有效；NO为无效
 */
- (BOOL)isAuthValid
{
    return ([self isLoggedIn] && ![self isAuthorizeExpired]);
}

#pragma mark - LogIn / LogOut

/**
 * @description 登录入口，当初始化SinaWeibo对象完成后直接调用此方法完成登录
 */
- (void)logIn
{
    if ([self isAuthValid])
    {
        if ([delegate respondsToSelector:@selector(sinaweiboDidLogIn:)])
        {
            [delegate sinaweiboDidLogIn:self];
        }
    }
    else
    {
        [self removeAuthData];
        
        ssoLoggingIn = NO;
        
        // open sina weibo app
        UIDevice *device = [UIDevice currentDevice];
        if ([device respondsToSelector:@selector(isMultitaskingSupported)] &&
            [device isMultitaskingSupported])
        {
            NSDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    self.appKey, @"client_id",
                                    self.appRedirectURI, @"redirect_uri",
                                    self.ssoCallbackScheme, @"callback_uri", nil];
            
            // 先用iPad微博打开
            NSString *appAuthBaseURL = kSinaWeiboAppAuthURL_iPad;
            if (SinaWeiboIsDeviceIPad())
            {
                NSString *appAuthURL = [SinaWeiboRequest serializeURL:appAuthBaseURL
                                                               params:params httpMethod:@"GET"];
                ssoLoggingIn = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appAuthURL]];
            }
            
            // 在用iPhone微博打开
            if (!ssoLoggingIn)
            {
                appAuthBaseURL = kSinaWeiboAppAuthURL_iPhone;
                NSString *appAuthURL = [SinaWeiboRequest serializeURL:appAuthBaseURL
                                                               params:params httpMethod:@"GET"];
                ssoLoggingIn = [[UIApplication sharedApplication] openURL:[NSURL URLWithString:appAuthURL]];
            }
        }
        
        if (!ssoLoggingIn)
        {
            // open authorize view
            
            NSDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                    self.appKey, @"client_id",
                                    @"code", @"response_type",
                                    self.appRedirectURI, @"redirect_uri", 
                                    @"mobile", @"display", nil];
            
            SinaWeiboAuthorizeView *authorizeView = \
            [[SinaWeiboAuthorizeView alloc] initWithAuthParams:params
                                                      delegate:self];
            [authorizeView show];
            [authorizeView release];
        }
    }
}

/**
 * @description 退出方法，需要退出时直接调用此方法
 */
- (void)logOut
{
    [self removeAuthData];
    
    if ([delegate respondsToSelector:@selector(sinaweiboDidLogOut:)])
    {
        [delegate sinaweiboDidLogOut:self];
    }
}

#pragma mark - Send request with token

/**
 * @description 微博API的请求接口，方法中自动完成token信息的拼接
 * @param url: 请求的接口
 * @param params: 请求的参数，如发微博所带的文字内容等
 * @param httpMethod: http类型，GET或POST
 * @param _delegate: 处理请求结果的回调的对象，SinaweiboRequestDelegate类
 * @return 完成实际请求操作的SinaWeiboRequest对象
 */

- (SinaWeiboRequest *)requestWithURL:(NSString *)url
                             params:(NSMutableDictionary *)params
                         httpMethod:(NSString *)httpMethod
                           delegate:(id<SinaWeiboRequestDelegate>)_delegate
{
    if (params == nil)
    {
        params = [NSMutableDictionary dictionary];
    }
    
    if ([self isAuthValid])
    {
        [params setValue:self.accessToken forKey:@"access_token"];
        NSString *fullURL = [kSinaWeiboSDKAPIDomain stringByAppendingString:url];
        
        SinaWeiboRequest *_request = [SinaWeiboRequest requestWithURL:fullURL
                                                           httpMethod:httpMethod
                                                               params:params
                                                             delegate:_delegate];
        _request.sinaweibo = self;
        [requests addObject:_request];
        [_request connect];
        return _request;
    }
    else
    {
        //notify token expired in next runloop
        [self performSelectorOnMainThread:@selector(notifyTokenExpired:)
                               withObject:_delegate
                            waitUntilDone:NO];
        
        return nil;
    }
}


- (SinaWeiboRequest *)requestWithMethodName:(NSString *)methodName
                              params:(NSMutableDictionary *)params
                          httpMethod:(NSString *)httpMethod
                              target:(id)target
                            selector:(SEL)selector
                            userInfo:(id)userInfo
                                flag:(BOOL)flag
{
    if (params == nil)
    {
        params = [NSMutableDictionary dictionary];
    }
    
    if ([self isAuthValid])
    {
        [params setValue:self.accessToken forKey:@"access_token"];
        
        SinaWeiboRequest *_request = [SinaWeiboRequest requestWithURL:[NSString stringWithFormat:@"%@%@", kWBSDKAPIDomain, methodName]
                                                           httpMethod:httpMethod
                                                               params:params
                                                             delegate:self];
        
        ZSTSinaWeiboUserInfo *communicatorUserInfo = [[ZSTSinaWeiboUserInfo alloc] init];
        communicatorUserInfo.target = target;
        communicatorUserInfo.selector = selector;
        communicatorUserInfo.userInfo = userInfo;
        _request.requestUserInfo = [NSMutableDictionary dictionaryWithObject:communicatorUserInfo forKey:kRequestUserInfo];
        [communicatorUserInfo release];
        
        
        _request.sinaweibo = self;
        [requests addObject:_request];
        [_request connect];
        return _request;
    }
    else
    {
        //notify token expired in next runloop
        [self performSelectorOnMainThread:@selector(notifyTokenExpired:)
                               withObject:self
                            waitUntilDone:NO];
        
        return nil;
    }
    
}


- (SinaWeiboRequest *)requestWithURL:(NSString *)url
                              params:(NSMutableDictionary *)params
                          httpMethod:(NSString *)httpMethod
                            delegate:(id<SinaWeiboRequestDelegate>)_delegate
                                flag:(BOOL)flag
{
    if (params == nil)
    {
        params = [NSMutableDictionary dictionary];
    }
    
    if ([self isAuthValid])
    {        
        SinaWeiboRequest *_request = [SinaWeiboRequest requestWithURL:url
                                                           httpMethod:httpMethod
                                                               params:params
                                                             delegate:_delegate];
        
        
        _request.sinaweibo = self;
        [requests addObject:_request];
        [_request connect];
        return _request;
    }
    else
    {
        //notify token expired in next runloop
        [self performSelectorOnMainThread:@selector(notifyTokenExpired:)
                               withObject:_delegate
                            waitUntilDone:NO];
        
        return nil;
    }
}

#pragma mark - SinaWeiboAuthorizeView Delegate

- (void)authorizeView:(SinaWeiboAuthorizeView *)authView didRecieveAuthorizationCode:(NSString *)code
{
    [self requestAccessTokenWithAuthorizationCode:code];
}

- (void)authorizeView:(SinaWeiboAuthorizeView *)authView didFailWithErrorInfo:(NSDictionary *)errorInfo
{
    [self logInDidFailWithErrorInfo:errorInfo];
}

- (void)authorizeViewDidCancel:(SinaWeiboAuthorizeView *)authView
{
    [self logInDidCancel];
}

#pragma mark - SinaWeiboRequest Delegate

- (void)request:(SinaWeiboRequest *)_request didFailWithError:(NSError *)error
{
    if (_request == request)
    {
        if ([delegate respondsToSelector:@selector(sinaweibo:logInDidFailWithError:)])
        {
            [delegate sinaweibo:self logInDidFailWithError:error];
        }
        
        [request release], request = nil;
        return;
    }
        
    ZSTSinaWeiboUserInfo *userInfo = (ZSTSinaWeiboUserInfo *)[_request.requestUserInfo objectForKey:kRequestUserInfo];

    if (userInfo.target && [userInfo.target respondsToSelector:userInfo.selector]) {
        
        [userInfo.target performSelector:userInfo.selector
                              withObject:error
                              withObject:userInfo.userInfo];
    }
    
}

- (void)request:(SinaWeiboRequest *)_request didFinishLoadingWithResult:(id)result
{
    if (_request == request)
    {        
        [self logInDidFinishWithAuthInfo:result];
        [request release], request = nil;
        return;
    }
    
    ZSTSinaWeiboUserInfo *userInfo = (ZSTSinaWeiboUserInfo *)[_request.requestUserInfo objectForKey:kRequestUserInfo];
    
    if (userInfo.target && [userInfo.target respondsToSelector:userInfo.selector]) {

        [userInfo.target performSelector:userInfo.selector
                              withObject:result
                              withObject:userInfo.userInfo];
    }
    

}



#pragma mark - Application life cycle

/**
 * @description 当应用从后台唤起时，应调用此方法，需要完成退出当前登录状态的功能
 */
- (void)applicationDidBecomeActive
{
    if (ssoLoggingIn)
    {
        // user open the app manually
        // clean sso login state
        ssoLoggingIn = NO;
        
        if ([delegate respondsToSelector:@selector(sinaweiboLogInDidCancel:)])
        {
            [delegate sinaweiboLogInDidCancel:self];
        }
    }
}

/**
 * @description sso回调方法，官方客户端完成sso授权后，回调唤起应用，应用中应调用此方法完成sso登录
 * @param url: 官方客户端回调给应用时传回的参数，包含认证信息等
 * @return YES
 */
- (BOOL)handleOpenURL:(NSURL *)url
{
    NSString *urlString = [url absoluteString];
    if ([urlString hasPrefix:self.ssoCallbackScheme])
    {
        if (!ssoLoggingIn)
        {
            // sso callback after user have manually opened the app
            // ignore the request
        }
        else
        {
            ssoLoggingIn = NO;
            
            if ([SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"sso_error_user_cancelled"])
            {
                if ([delegate respondsToSelector:@selector(sinaweiboLogInDidCancel:)])
                {
                    [delegate sinaweiboLogInDidCancel:self];
                }
            }
            else if ([SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"sso_error_invalid_params"])
            {
                if ([delegate respondsToSelector:@selector(sinaweibo:logInDidFailWithError:)])
                {
                    NSString *error_description = @"Invalid sso params";
                    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                              error_description, NSLocalizedDescriptionKey, nil];
                    NSError *error = [NSError errorWithDomain:kSinaWeiboSDKErrorDomain 
                                                         code:kSinaWeiboSDKErrorCodeSSOParamsError
                                                     userInfo:userInfo];
                    [delegate sinaweibo:self logInDidFailWithError:error];
                }
            }
            else if ([SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"error_code"])
            {
                NSString *error_code = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"error_code"];
                NSString *error = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"error"];
                NSString *error_uri = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"error_uri"];
                NSString *error_description = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"error_description"];
                
                NSDictionary *errorInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                           error, @"error",
                                           error_uri, @"error_uri",
                                           error_code, @"error_code",
                                           error_description, @"error_description", nil];
                
                [self logInDidFailWithErrorInfo:errorInfo];
            }
            else
            {
                NSString *access_token = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"access_token"];
                NSString *expires_in = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"expires_in"];
                NSString *remind_in = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"remind_in"];
                NSString *uid = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"uid"];
                NSString *refresh_token = [SinaWeiboRequest getParamValueFromUrl:urlString paramName:@"refresh_token"];
                
                NSMutableDictionary *authInfo = [NSMutableDictionary dictionary];
                if (access_token) [authInfo setObject:access_token forKey:@"access_token"];
                if (expires_in) [authInfo setObject:expires_in forKey:@"expires_in"];
                if (remind_in) [authInfo setObject:remind_in forKey:@"remind_in"];
                if (refresh_token) [authInfo setObject:refresh_token forKey:@"refresh_token"];
                if (uid) [authInfo setObject:uid forKey:@"uid"];                                                                                                            
                
                [self logInDidFinishWithAuthInfo:authInfo];
            }
        }
    }
    return YES;
}




////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//获取用户信息

- (void)getUserInfo:(NSString *)uid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    [params setObject:uid forKey:@"uid"];
    
    NSString *methodName = @"users/show.json";
    
    
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"GET"
                         target:self
                       selector:@selector(getUserInfoResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
}

- (void)getUserInfoResponse:(id)response userInfo:(id)userInfo

{
    if ([response isKindOfClass:[NSError class]]) {
        if ([self.delegate respondsToSelector:@selector(getUserInfoFailured)]) {
            [self.delegate getUserInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        if ([self.delegate respondsToSelector:@selector(getUserInfoSucceed:)]) {
            [self.delegate getUserInfoSucceed:response];
        }
    }
}

- (BOOL) isValidDelegateForSelector:(id)delegate selector:(SEL)selector
{
    return ((self.delegate != nil) && [self.delegate respondsToSelector:selector]);
}

- (void)sendWeiBoWithText:(NSString *)text image:(UIImage *)image
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
    
    NSString *methodName = @"statuses/update.json";
	[params setObject:(text ? text : @"") forKey:@"status"];
    if (image) {
        [params setObject:image forKey:@"pic"];
        methodName = @"statuses/upload.json";
    }
    
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"POST"
                         target:self
                       selector:@selector(sendWeiBoInfoResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
        
}

- (void)sendWeiBoInfoResponse:(id)response userInfo:(id)userInfo

{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(sendWeiBoInfoFailured)]) {
            [self.delegate sendWeiBoInfoFailured];
        }

    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(sendWeiBoInfoSucceed:)]) {
            [self.delegate sendWeiBoInfoSucceed:response];
        }
        
    }
}


- (void)createFriendships:(NSString *)uid name:(NSString *)screenName
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
	[params setObject:@"中国好声音灿星客户端" forKey:@"screen_name"];
    NSString *methodName = @"friendships/create.json";
    
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"POST"
                         target:self
                       selector:@selector(createFriendshipsInfoResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
}

- (void)createFriendshipsInfoResponse:(id)response userInfo:(id)userInfo

{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(createFriendshipsInfoFailured)]) {
            [self.delegate createFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(createFriendshipsInfoSucceed:)]) {
            [self.delegate createFriendshipsInfoSucceed:response];
        }
    }
}


- (void)showFriendships:(NSString *)sourceId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
	[params setObject:sourceId forKey:@"source_id"];
    [params setObject:@"中国好声音灿星客户端" forKey:@"target_screen_name"];

    NSString *methodName = @"friendships/show.json";
    
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"GET"
                         target:self
                       selector:@selector(showFriendshipsInfoResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
}

- (void)showFriendshipsInfoResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
    
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


- (void)_parsingSucceededFordataType:(ZSTWeiBoDataType)type result:(id)result
{
    switch (type) {
        case ZSTWeiBoStatuses:
            if ([delegate respondsToSelector:@selector(engine:parsingSucceededForStatues:)]){
				[delegate engine:self parsingSucceededForStatues:result];
			}
            break;
        case ZSTWeiBoComments:
            if ([delegate respondsToSelector:@selector(engine:parsingSucceededForComments:)]){
				[delegate engine:self parsingSucceededForComments:result];
			}
            break;
        case ZSTWeiBoReposts:
            if ([delegate respondsToSelector:@selector(engine:parsingSucceededForReports:)]){
				[delegate engine:self parsingSucceededForReports:result];
			}
            break;
        default:
            break;
    }
}

- (void)_parseData:(NSArray *)dataArray dataType:(ZSTWeiBoDataType)type
{
    NSMutableArray *weiBolist = [NSMutableArray array];
    
    for (int i = 0; i < [dataArray count] ; i ++) {
        
        ZSTWeiBoInfo *weiBoInfo = [[ZSTWeiBoInfo alloc] init];
        NSDictionary *member = [dataArray objectAtIndex:i];
        NSDictionary *user = [member objectForKey:@"user"];
        weiBoInfo.ID = [member objectForKey:@"id"];
        weiBoInfo.avatarString = [user objectForKey:@"avatar_large"];
        weiBoInfo.name = [user objectForKey:@"name"];
        NSString *timeStr = [member objectForKey:@"created_at"];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"EEE MMM dd HH':'mm':'ss Z yyyy"];
        [formatter setLocale:[[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"] autorelease]];
        NSDate *date = [formatter dateFromString:timeStr];
        weiBoInfo.time = date;
        weiBoInfo.content = [member objectForKey:@"text"];
        weiBoInfo.MID = [member objectForKey:@"mid"];
        weiBoInfo.thumbnail_pic = [member objectForKey:@"thumbnail_pic"];
        weiBoInfo.bmiddle_pic = [member objectForKey:@"bmiddle_pic"];
        weiBoInfo.original_pic = [member objectForKey:@"original_pic"];
        
        weiBoInfo.forwardCount = [NSString stringWithFormat:@"%@",[member objectForKey:@"reposts_count"]];
        weiBoInfo.commentCount = [NSString stringWithFormat:@"%@",[member objectForKey:@"comments_count"]];
        
        weiBoInfo.Platform = [[member objectForKey:@"source"] stripTags];
        
        weiBoInfo.attachmentInfo = nil;
        NSDictionary *retweeted_status = [member objectForKey:@"retweeted_status"];
        if (retweeted_status) {
            ZSTWeiBoInfo *attachmentInfo = [[ZSTWeiBoInfo alloc] init];
            weiBoInfo.attachmentInfo = attachmentInfo;
            weiBoInfo.attachmentInfo.thumbnail_pic = [retweeted_status objectForKey:@"thumbnail_pic"];
            weiBoInfo.attachmentInfo.bmiddle_pic = [retweeted_status objectForKey:@"bmiddle_pic"];
            weiBoInfo.attachmentInfo.original_pic = [retweeted_status objectForKey:@"original_pic"];
            NSString *retweeted_content = [retweeted_status objectForKey:@"text"];
            NSString *content = retweeted_content;
            weiBoInfo.attachmentInfo.content = content;
            weiBoInfo.attachmentInfo.name = [[retweeted_status objectForKey:@"user"] objectForKey:@"name"];
            [attachmentInfo release];
        }
        
        [weiBolist addObject:weiBoInfo];
        [weiBoInfo release];
        [formatter release];
    }
    
    [self _parsingSucceededFordataType:type result:weiBolist];
    
}



-(void)sendCommentComment:(NSString *)message ID:(NSString *)ID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
    
	[params setObject:message forKey:@"comment"];
    [params setObject: [ID description] forKey:@"id"];
    
    NSString *methodName = @"comments/create.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"POST"
                         target:self
                       selector:@selector(sendCommentCommentResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
}

- (void)sendCommentCommentResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}

- (void)sendReportMessage:(NSString *)message ID:(NSString *)ID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
    
	[params setObject:message forKey:@"status"];
    [params setObject: [ID description] forKey:@"id"];
    
    
    NSString *methodName = @"statuses/repost.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"POST"
                         target:self
                       selector:@selector(sendReportMessageResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
   
}

- (void)sendReportMessageResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}

- (void)sendReplyMessage:(NSString *)message ID:(NSString *)ID CID:(NSString *)cid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:2];
    
	[params setObject:message forKey:@"comment"];
    [params setObject: [ID description] forKey:@"id"];
    [params setObject: [cid description] forKey:@"cid"];
    
    
    NSString *methodName = @"comments/reply.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"POST"
                         target:self
                       selector:@selector(sendReplyMessageResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
    
}

- (void)sendReplyMessageResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}


- (void)getPublicTimelineSinceID:(int)updateID
{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:0];
    if (updateID > 0) {
        [params setObject:[NSString stringWithFormat:@"%d", updateID] forKey:@"since_id"];
    }
    
    NSString *methodName = @"statuses/friends_timeline.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"GET"
                         target:self
                       selector:@selector(getPublicTimelineResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
}

- (void)getPublicTimelineResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}

- (void)getCommentList:(NSString *)ID sinceID:(int) updateID startingAtPage:(int)pageNum count:(int)count//comments/show //某条微博的评论列表//默认更新每页20条
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    [params setObject: [ID description] forKey:@"id"];
    
    if (updateID > 0) {
        [params setObject:[NSString stringWithFormat:@"%d", updateID] forKey:@"since_id"];
    }
    if (pageNum > 0) {
        [params setObject:[NSString stringWithFormat:@"%d", pageNum] forKey:@"page"];
    }
    int weiBoCount = DEFAULT_WB_COUNT;
	if (count > 0) {
		weiBoCount = count;
	}
	[params setObject:[NSString stringWithFormat:@"%d", weiBoCount] forKey:@"count"];
    
    
    NSString *methodName = @"comments/show.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"GET"
                         target:self
                       selector:@selector(getCommentListResponse:userInfo:)
                       userInfo:nil
                           flag:NO];

}


- (void)getCommentListResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}

- (void)getForwardList:(NSString *)ID sinceID:(int) updateID startingAtPage:(int)pageNum count:(int)count// statuses/repost_timeline  //某条微博的转发列表//默认更新每页20条
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    [params setObject: [ID description] forKey:@"id"];
    
    if (updateID > 0) {
        [params setObject:[NSString stringWithFormat:@"%d", updateID] forKey:@"since_id"];
    }
    if (pageNum > 0) {
        [params setObject:[NSString stringWithFormat:@"%d", pageNum] forKey:@"page"];
    }
    int weiBoCount = DEFAULT_WB_COUNT;
	if (count > 0) {
		weiBoCount = count;
	}
	[params setObject:[NSString stringWithFormat:@"%d", weiBoCount] forKey:@"count"];
    
    [params setObject: [ID description] forKey:@"id"];
    
    
    NSString *methodName = @"statuses/repost_timeline.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"GET"
                         target:self
                       selector:@selector(getForwardListResponse:userInfo:)
                       userInfo:nil
                           flag:NO];
    
}


- (void)getForwardListResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}

- (void)getFavorites:(NSString *)ID// favorites/show  //某条微博的收藏信息
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    [params setObject: [ID description] forKey:@"id"];
        
    NSString *methodName = @"statuses/show.json";
    [self requestWithMethodName:methodName
                         params:params
                     httpMethod:@"GET"
                         target:self
                       selector:@selector(getFavoritesResponse:userInfo:)
                       userInfo:nil
                           flag:NO];

}

- (void)getFavoritesResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}

- (void)markUpdate:(NSString *)updateID asFavorite:(BOOL)flag//favorites/create 收藏微博   favorites/show
{
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithCapacity:4];
    
    [params setObject: [updateID description] forKey:@"id"];
    if (flag) {
        
        NSString *methodName = @"favorites/create.json";
        [self requestWithMethodName:methodName
                             params:params
                         httpMethod:@"POST"
                             target:self
                           selector:@selector(markUpdateResponse:userInfo:)
                           userInfo:nil
                               flag:NO];
    }else{
        
        NSString *methodName = @"favorites/destroy.json";
        [self requestWithMethodName:methodName
                             params:params
                         httpMethod:@"POST"
                             target:self
                           selector:@selector(markUpdateResponse:userInfo:)
                           userInfo:nil
                               flag:NO];
    }
}

- (void)markUpdateResponse:(id)response userInfo:(id)userInfo
{
    if ([response isKindOfClass:[NSError class]]) {
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoFailured)]) {
            [self.delegate showFriendshipsInfoFailured];
        }
    }else{
        
        if (![response isKindOfClass:[NSDictionary class]]) {
            response = [NSDictionary dictionary];
        }
        NSDictionary *target = [response objectForKey:@"target"];
        
        
        if ([self isValidDelegateForSelector:self.delegate selector:@selector(showFriendshipsInfoSucceed:)]) {
            [self.delegate showFriendshipsInfoSucceed:target];
        }
    }
}
+ (NSString*)gsid
{
    if(!_gsid)
		_gsid = [[[NSUserDefaults standardUserDefaults] stringForKey:@"Gsid"] retain];
    return [[_gsid retain] autorelease];
}

- (void)closeAllConnections
{

}






@end

BOOL SinaWeiboIsDeviceIPad()
{
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 30200
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
    {
        return YES;
    }
#endif
    return NO;
}




