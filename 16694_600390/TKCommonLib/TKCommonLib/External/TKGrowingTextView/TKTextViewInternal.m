//
//  TKTextViewInternal.m
//


#import "TKTextViewInternal.h"
#import "TKPlaceHolderTextView.h"


@implementation TKTextViewInternal

-(void)setContentOffset:(CGPoint)s
{
    if(self.tracking || self.decelerating){
        //initiated by user...
        
        UIEdgeInsets insets = self.contentInset;
        insets.bottom = 0;
        insets.top = 0;
        self.contentInset = insets;
        
    } else {

        float bottomOffset = (self.contentSize.height - self.frame.size.height + self.contentInset.bottom);
        if(s.y < bottomOffset && self.scrollEnabled){            
            UIEdgeInsets insets = self.contentInset;
            insets.bottom = 8;
            insets.top = 0;
            self.contentInset = insets;            
        }
    }
        
    [super setContentOffset:s];
}

-(void)setContentInset:(UIEdgeInsets)s
{
    UIEdgeInsets insets = s;
    
    if(s.bottom>8) insets.bottom = 0;
    insets.top = 0;

    [super setContentInset:insets];
}

-(void)setContentSize:(CGSize)contentSize
{
    // is this an iOS5 bug? Need testing!
    if(self.contentSize.height > contentSize.height)
    {
        UIEdgeInsets insets = self.contentInset;
        insets.bottom = 0;
        insets.top = 0;
        self.contentInset = insets;
    }
    [super setContentSize:contentSize];
    
}


- (void)dealloc {
    [super dealloc];
}


@end
