//
//  ZSTEComBMyOrderViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTEComBMyOrderViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTF3EngineECBDelegate>

@property (retain,nonatomic) UITableView * tableView;
@property (retain,nonatomic) NSMutableArray * dataArr;
@end
