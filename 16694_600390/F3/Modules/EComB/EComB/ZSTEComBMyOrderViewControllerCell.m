//
//  ZSTEComBMyOrderViewControllerCell.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBMyOrderViewControllerCell.h"
#import <PMCallButton.h>

@implementation ZSTEComBMyOrderViewControllerCell
{
    NSArray * _colorArr;
    NSArray * _imageArr;
    NSString * _phone;
    UILabel * _callLabel;
    UIImageView * callImgView;
    UIControl * callContr;
    UIControl *logisticsContr;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        UILabel * l1 = [[UILabel alloc]init];
        l1.font = [UIFont systemFontOfSize:14];
        l1.textColor = RGBCOLOR(9*16+15, 9*16+15, 9*16+15);
        l1.text = @"订单编号:";
        [self.contentView addSubview:l1];
        CGSize size1 = [l1.text sizeWithFont:l1.font];
        l1.frame = CGRectMake(22, 17, size1.width, 22);
        [l1 release];
        
        UILabel * l2 = [[UILabel alloc]init];
        l2.font = [UIFont systemFontOfSize:14];
        l2.textColor = RGBCOLOR(9*16+15, 9*16+15, 9*16+15);
        l2.text = @"订单总额:";
        [self.contentView addSubview:l2];
        CGSize size2 = [l2.text sizeWithFont:l2.font];
        l2.frame = CGRectMake(CGRectGetMinX(l1.frame), CGRectGetMaxY(l1.frame), size2.width, 22);
        [l2 release];
        
        UILabel * l3 = [[UILabel alloc]init];
        l3.font = [UIFont systemFontOfSize:14];
        l3.textColor = RGBCOLOR(9*16+15, 9*16+15, 9*16+15);
        l3.text = @"下单时间:";
        [self.contentView addSubview:l3];
        CGSize size3 = [l3.text sizeWithFont:l3.font];
        l3.frame = CGRectMake(CGRectGetMinX(l1.frame), CGRectGetMaxY(l2.frame), size3.width, 22);
        [l3 release];
        
        UILabel * l4 = [[UILabel alloc]init];
        l4.font = [UIFont systemFontOfSize:14];
        l4.textColor = RGBCOLOR(9*16+15, 9*16+15, 9*16+15);
        l4.text = @"订单状态:";
        [self.contentView addSubview:l4];
        CGSize size5 = [l4.text sizeWithFont:l4.font];
        l4.frame = CGRectMake(CGRectGetMinX(l1.frame), 170 - size5.height, size5.width, 22);
        [l4 release];
        
        _callLabel = [[UILabel alloc]init];
        _callLabel.font = [UIFont systemFontOfSize:14];
        _callLabel.textColor = RGBCOLOR(2*16+11, 2*16+11, 2*16+11);
        _callLabel.text = @"联系卖家";
        [self.contentView addSubview:_callLabel];
        CGSize size7 = [_callLabel.text sizeWithFont:_callLabel.font];
        _callLabel.frame = CGRectMake(320 - size7.width - 20, CGRectGetMinY(l4.frame), size7.width, 22);
        [_callLabel release];
        CGRect rect = _callLabel.bounds;
        rect.size.height = _callLabel.frame.size.height + 10;
        
        PMCallButton *callBtn = [PMCallButton buttonWithType:UIButtonTypeCustom];
        callBtn.frame = CGRectMake(320 - size7.width - 20 - 40, CGRectGetMinY(l4.frame) - 10, size7.width + 40, 22);
        [callBtn setImage:ZSTModuleImage(@"module_ecomb_myorder_call.png") forState:UIControlStateNormal];
        [callBtn setTitle:@"联系卖家" forState:UIControlStateNormal];
        [callBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        callBtn.titleLabel.font = [UIFont systemFontOfSize:13];
        //        [self.contentView addSubview:callBtn];
        [callBtn addTarget:self action:@selector(callPhone) forControlEvents:UIControlEventTouchUpInside];

        
        callContr = [[UIControl alloc]initWithFrame:_callLabel.frame];
        [callContr addTarget:self action:@selector(call:) forControlEvents:UIControlEventTouchUpInside];
//        [self addSubview:callContr];
        callContr.backgroundColor = [UIColor clearColor];
        
        
        callImgView  = [[UIImageView alloc]init];
        callImgView.frame = CGRectMake(CGRectGetMinX(_callLabel.frame) - 20, CGRectGetMinY(_callLabel.frame) + 5, 14, 13);
        callImgView.image = ZSTModuleImage(@"module_ecomb_myorder_call.png");
        [self.contentView addSubview:callImgView];
        [callImgView release];
        
        _numberLabel = [[UILabel alloc]init];
        _numberLabel.frame = CGRectMake(CGRectGetMaxX(l1.frame) + 10, CGRectGetMinY(l1.frame), 200, 22);
        _numberLabel.font = [UIFont systemFontOfSize:14];
        _numberLabel.textColor = RGBCOLOR(2*16+11, 2*16+11, 2*16+11);
        _numberLabel.text = @"201423456789";
        [self.contentView addSubview:_numberLabel];
        
        _totalLabel = [[UILabel alloc]init];
        _totalLabel.frame = CGRectMake(CGRectGetMaxX(l2.frame) + 10, CGRectGetMaxY(_numberLabel.frame), 320, 22);
        _totalLabel.font = [UIFont systemFontOfSize:14];
        _totalLabel.textColor = RGBCOLOR(12*16+12, 0, 0);
        _totalLabel.text = @"￥660";
        [self.contentView addSubview:_totalLabel];
        
        _timeLabel = [[UILabel alloc]init];
        _timeLabel.frame = CGRectMake(CGRectGetMaxX(l3.frame) + 10, CGRectGetMaxY(_totalLabel.frame), 320, 22);
        _timeLabel.font = [UIFont systemFontOfSize:14];
        _timeLabel.textColor = RGBCOLOR(2*16+11, 2*16+11, 2*16+11);
        _timeLabel.text = @"2014-02-24 20:39:34";
        [self.contentView addSubview:_timeLabel];
        
        _countLabel = [[UILabel alloc]init];
        _countLabel.font = [UIFont systemFontOfSize:14];
        _countLabel.textColor = RGBCOLOR(9*16+15, 9*16+15, 9*16+15);
        _countLabel.text = @"共3件";
        [self.contentView addSubview:_countLabel];
        CGSize size4 = [_countLabel.text sizeWithFont:_countLabel.font];
        _countLabel.frame = CGRectMake(320 - size4.width - 20 , CGRectGetMaxY(_timeLabel.frame) + 26, size4.width, 22);
        
        
        _stateLabel = [[UILabel alloc]init];
        _stateLabel.font = [UIFont systemFontOfSize:14];
        _stateLabel.textColor = RGBCOLOR(7, 10*16+10, 20);
        _stateLabel.text = @"待发货";
        [self.contentView addSubview:_stateLabel];
        //        CGSize size6 = [_stateLabel.text sizeWithFont:_stateLabel.font];
        _stateLabel.backgroundColor = [UIColor clearColor];
        _stateLabel.frame = CGRectMake(CGRectGetMaxX(l4.frame) + 10, CGRectGetMinY(l4.frame), 150, 22);
        
        _logisticsbtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _logisticsbtn.frame = CGRectMake(150, CGRectGetMinY(l4.frame)+1, 63, 20);
        [_logisticsbtn setTitle:@"查物流" forState:UIControlStateNormal];
        [_logisticsbtn setTintColor:[UIColor whiteColor]];
        [_logisticsbtn.titleLabel setFont:[UIFont boldSystemFontOfSize:14]];
        [_logisticsbtn setBackgroundColor:RGBCOLOR(240, 67, 67)];
        _logisticsbtn.layer.cornerRadius = 3.0f;
        _logisticsbtn.clipsToBounds = YES;
        _logisticsbtn.hidden = YES;
        [_logisticsbtn addTarget:self action:@selector(logisticsInfo:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:_logisticsbtn];
        
        logisticsContr = [[UIControl alloc]initWithFrame:CGRectMake(120, CGRectGetMinY(l4.frame)-15, 95, 54)];
        [logisticsContr addTarget:self action:@selector(logisticsInfo:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:logisticsContr];
        callContr.backgroundColor = [UIColor clearColor];

        
        _careouseView  = [[ZSTEComBMyOrderImageCareouseView alloc]initWithFrame:CGRectMake(15, 86, 230, 60)];
        //        _careouseView.backgroundColor = [UIColor yellowColor];
        _careouseView.careouseDataSouce = self;
        _careouseView.careouseDelegate = self;
        [self addSubview:_careouseView];
        
        
        ZSTRoundRectView * round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(10, 10, 300, 170) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5)];
        [self.contentView addSubview:round];
        [round release];
        
        [round addSubview:callBtn];
        //[callBtn release];
        
        ZSTRoundRectView * line = [[ZSTRoundRectView alloc]initWithPoint:CGPointMake(CGRectGetMinX(round.frame), CGRectGetMaxY(l3.frame)) toPoint:CGPointMake(CGRectGetMaxX(round.frame), CGRectGetMaxY(l3.frame)) borderWidth:1.0f borderColor:RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5)];
        [self.contentView addSubview:line];
        [line release];
        
        ZSTRoundRectView * line2 = [[ZSTRoundRectView alloc]initWithPoint:CGPointMake(CGRectGetMinX(round.frame), CGRectGetMaxY(_careouseView.frame)) toPoint:CGPointMake(CGRectGetMaxX(round.frame), CGRectGetMinY(l4.frame)) borderWidth:1.0f borderColor:RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5)];
        [self.contentView addSubview:line2];
        [line2 release];
        _colorArr = [[NSArray alloc]initWithObjects:[UIColor redColor],[UIColor redColor],[UIColor redColor],[UIColor greenColor],[UIColor greenColor], nil];
    }
    return self;
}
-(UIColor *)stringToColor:(NSString *)colorStr
{
    int a[6] = {},i = 0;
    char * cchar = (char *)[colorStr cStringUsingEncoding:NSASCIIStringEncoding];
    
    char * p = cchar;
    
    while (*p != '\0') {
        if (*p == '#') {
            
        }
        else if (*p >= 'a' && *p<='z'){
            a[i++] = 10 + *p - 'a';
        }
        else if(*p >= 'A' && *p<='Z')
        {
            a[i++] = 10 + *p - 'A';
        }
        else if (*p >= '0' && *p<='9') {
            a[i++] = *p - '0';
        }
        else{
            a[i++] = 0;
        }
        p++;
    }
    
    UIColor * color = RGBCOLOR(a[0] * 16 + a[1], a[2] * 16 + a[3], a[4]*16 + a[5]);
    return color;
}
-(void)call:(NSString *)number
{
    if ([_phone isKindOfClass:[NSNull class]] || _phone.length == 0) {
        return;
    }
    UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:_phone delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"呼叫" otherButtonTitles:nil];
    
    [sheet showInView:self.contentView];
}

-(void)setCheckBlock:(checkBlocked)block
{
    logisticsBlcok = [block copy];
}

- (void)logisticsInfo:(id)sender
{
    NSString *strig = _numberLabel.text;
    logisticsBlcok(self,strig);
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        NSString * tel = [NSString stringWithFormat:@"tel://%@",_phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
    }
    else
    {
        NSLog(@"电话咨询返回");
    }
}


-(NSInteger)imageCareouse:(ZSTEComBMyOrderImageCareouseView *)careouseView numberOfTotal:(NSInteger)number
{
    return _imageArr.count;
}
-(NSString *)imageCareouse:(ZSTEComBMyOrderImageCareouseView *)careouseView imageAtIndex:(NSInteger)index
{
    return [_imageArr objectAtIndex:index];
}
-(void)imageCareouse:(ZSTEComBMyOrderImageCareouseView *)careouseView didSelectIndex:(NSInteger)index
{
    NSLog(@"选择了%ld",index);
}
-(void)initWithOrderInfo:(EcomBOrderInfo *) orderInfo
{
    callContr.enabled = YES;
    if (!_phone || [_phone isKindOfClass:[NSNull class]] || _phone.length == 0) {
        _callLabel.hidden = YES;
        _callLabel.userInteractionEnabled = NO;
        callImgView.hidden = YES;
        callImgView.userInteractionEnabled = NO;
        callContr.enabled = NO;
    }
    _numberLabel.text = orderInfo.orderOrderid;
    
    _countLabel.text = [NSString stringWithFormat:@"共%d件",(int)orderInfo.orderProductcount];
    CGSize countLabelSize = [_countLabel.text sizeWithFont:_countLabel.font constrainedToSize:CGSizeMake(235, 40) lineBreakMode:NSLineBreakByWordWrapping];
    _countLabel.frame = CGRectMake(320 - 20 - countLabelSize.width, CGRectGetMaxY(_timeLabel.frame) + 26, countLabelSize.width, countLabelSize.height);
    //    CGRectMake(320 - size4.width - 20 , CGRectGetMaxY(_timeLabel.frame) + 26, size4.width, 22);
    _stateLabel.textColor = [_colorArr objectAtIndex:(orderInfo.orderStatus % 5)];
    _stateLabel.text = [self getStateNameWithOrderStatu:(orderInfo.orderStatus % 5)];
    _timeLabel.text = [NSString stringWithFormat:@"%@",orderInfo.orderTime];
    _totalLabel.text = [NSString stringWithFormat:@"￥%.2f",orderInfo.orderAmount];
    _phone = orderInfo.orderPhone;
    _imageArr = [orderInfo orderImageArr];
    [_careouseView reloadData];
}

-(NSString *)getStateNameWithOrderStatu:(int)statu
{
    if (statu == 0) {
        _logisticsbtn.hidden = YES;
        logisticsContr.enabled = NO;
        return @"待付款";
    }
    else if(statu == 1)
    {
        _logisticsbtn.hidden = YES;
        logisticsContr.enabled = NO;
        return @"待发货";
    }
    else if(statu == 3)
    {
        _logisticsbtn.hidden = NO;
        logisticsContr.enabled = YES;
        return @"已发货";
    }
    else if(statu == 4)
    {
        _logisticsbtn.hidden = NO;
        logisticsContr.enabled = YES;
        return @"已完结";
    }
    return @"错误状态";
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)callPhone
{
    if ([_phone isKindOfClass:[NSString class]] && [_phone length] > 0)
    {
        UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:_phone delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"呼叫" otherButtonTitles:nil];
        [sheet showInView:self.contentView];
    }
    else
    {
        [TKUIUtil alertInWindow:@"该卖家没有设置电话号码哦" withImage:nil];
    }
}

//-(void)dealloc
//{
//
//    [_numberLabel release];
//    [_totalLabel release];
//    [_timeLabel release];
//    [_countLabel release];
//    [_stateLabel release];
//    [super dealloc];
//}
@end
