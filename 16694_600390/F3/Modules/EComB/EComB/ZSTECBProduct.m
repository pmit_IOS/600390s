//
//  ZSTECBProduct.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBProduct.h"

@implementation ZSTECBProduct

@synthesize productName;//商品名称
@synthesize salesPrice;//现价
@synthesize primePrice;//原价
@synthesize price;//成交价

@synthesize summary;//副标题

@synthesize iconUrl;//图标图片地址
@synthesize carouselUrl;//轮播图地址
@synthesize addTime;//上架日期
@synthesize categoryID;//分类id
@synthesize productID;//商品id
@synthesize orderNum;//排列顺序
@synthesize orderID;//订单id

@synthesize mainTitle;// 主标题

+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        
        self.mainTitle = [dic safeObjectForKey:@"title"];
        
        self.productName =  [dic safeObjectForKey:@"productname"];
        self.freightPrice = [dic safeObjectForKey:@"freight"];
        self.salesPrice = [dic safeObjectForKey:@"salesprice"];
        self.primePrice = [dic safeObjectForKey:@"primeprice"];
        //        self.price = [dic valueForKey:@"Price"];
        
        self.primeLabel = [dic safeObjectForKey:@"primelabel"];
        self.salesLabel = [dic safeObjectForKey:@"saleslabel"];
        
        self.summary = [dic safeObjectForKey:@"details"] ? [dic safeObjectForKey:@"details"]: @"";
        self.iconUrl = [dic safeObjectForKey:@"imgurl"];
        //        self.carouselUrl = [dic valueForKey:@"CarouselUrl"];
        self.addTime = [dic safeObjectForKey:@"addtime"];
        self.categoryID = [dic safeObjectForKey:@"categoryid"];
        self.productID = [dic safeObjectForKey:@"productid"];
        self.orderNum = [dic safeObjectForKey:@"ordernum"];
        self.discount = [dic safeObjectForKey:@"discount"];
        self.jifenflag = [[dic safeObjectForKey:@"jifenflag"] boolValue];
        //        self.orderID = [dic valueForKey:@"OrderID"];
        if ([[dic safeObjectForKey:@"phone"] isKindOfClass:[NSNull class]]) {
            self.phone = @"";
        }
        else self.phone = [dic safeObjectForKey:@"phone"];
    }
    
    return self;
}

- (void)dealloc
{
    self.productName = nil;
    self.salesPrice = nil;
    self.primePrice = nil;
    self.price = nil;
    self.primeLabel = nil;
    self.salesLabel = nil;
    self.summary = nil;
    self.iconUrl = nil;
    self.carouselUrl = nil;
    self.addTime = nil;
    self.categoryID = nil;
    self.productID = nil;
    self.orderNum = nil;
    self.orderID = nil;
    
    [super dealloc];
}


@end


@implementation EcomBAddress

@synthesize ecomUserName;

+ (id)addressWithDictonary:(NSDictionary *) dic
{
    return [[[self alloc] initWithDictionary:dic] autorelease];
}

- (id)initWithDictionary:(NSDictionary *) dic
{
    if( (self = [super init])) {
        self.ecomUserName = [dic safeObjectForKey:@"username"];
        self.ecomMobile = [dic safeObjectForKey:@"mobile"];
        self.ecomAddress = [dic safeObjectForKey:@"provincecity"];
        self.ecomPostNum = [dic safeObjectForKey:@"postcode"];
        self.ecomDetailAddress = [dic safeObjectForKey:@"address"];
        self.ecomCityId = [dic safeObjectForKey:@"cityid"];
    }
    
    return self;
    
}

-(void)dealloc
{
    self.ecomUserName = nil;
    self.ecomAddress = nil;
    self.ecomMobile = nil;
    self.ecomPostNum = nil;
    self.ecomUserName = nil;
    self.ecomDetailAddress = nil;
    self.ecomCityId = nil;
    [super dealloc];
}
@end



@implementation  EComBHomePageInfo

+ (id)homePageWithArray:(NSArray *) arr;
{
    
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc] initWithDictionary:dic]];
    }
    return dataArr;
}
- (id)initWithDictionary:(NSDictionary *) dic
{
    EComBHomePageInfo *  pageInfo = [[EComBHomePageInfo alloc]init];
    pageInfo.ecombHomeImage = [dic safeObjectForKey:@"imgurl"];
    pageInfo.ecombHomePrice = [[dic safeObjectForKey:@"salesprice"] stringValue];
    pageInfo.ecombHomeText = [dic safeObjectForKey:@"productname"];
    pageInfo.ecombHomeOrangePrice = [dic safeObjectForKey:@"primeprice"];
    pageInfo.ecombHomeProductid = [[dic safeObjectForKey:@"productid"] stringValue];
    pageInfo.jifenflag = [[dic safeObjectForKey:@"jifenflag"] boolValue];
    return pageInfo;
}

@end


@implementation EcomBCarouseInfo

+ (id)ecomBCarouseWithArray:(NSArray *) arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc] initWithDictionary:dic]];
    }
    return  dataArr;
    
}
- (id)initWithDictionary:(NSDictionary *) dic
{
    EcomBCarouseInfo * carouse = [[EcomBCarouseInfo alloc]init];
    carouse.ecomBCarouseInfoCarouselurl = [dic safeObjectForKey:@"carouselurl"];
    carouse.ecomBCarouseInfoLinkurl = [dic safeObjectForKey:@"linkurl"];
    carouse.ecomBCarouseInfoTitle = [dic safeObjectForKey:@"title"];
    carouse.ecomBCarouseInfoProductid = [[dic safeObjectForKey:@"productid"]intValue];
    return carouse;
}
//-(void)dealloc
//{
////    [_ecomBCarouseInfoCarouselurl release];
////    [_ecomBCarouseInfoLinkurl release];
////    [_ecomBCarouseInfoTitle release];
//    [super dealloc];
//}
@end


@implementation EcomBAskInfo

+ (id)EcomBAskWithArray:(NSArray *)arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc] initWithDictionary:dic]];
    }
    return  dataArr;
    
}
- (id)initWithDictionary:(NSDictionary *) dic
{
    EcomBAskInfo * carouse = [[EcomBAskInfo alloc]init];
    carouse.EcomBAskInfoAnswer = [dic safeObjectForKey:@"answer"];
    carouse.EcomBAskInfoMsgid = [dic safeObjectForKey:@"msgid"];
    carouse.EcomBAskInfoQuestion = [dic safeObjectForKey:@"question"];
    
    return carouse;
}

@end


@implementation EcomBProperty

+(id)EcomBPropertyWithArray:(NSArray *) arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc] initWithDictionary:dic]];
    }
    return dataArr;
}
-(id)initWithDictionary:(NSDictionary *) dic
{
    EcomBProperty * property = [[EcomBProperty alloc]init];
    property.ecomBPropertyProperty = [dic safeObjectForKey:@"propertyname"];
    property.ecomBPropertyPropertyid = [[dic safeObjectForKey:@"propertyid"] stringValue];
    property.ecomBPropertyStockcount = [[dic safeObjectForKey:@"stockcount"] intValue];
    property.spec = [dic safeObjectForKey:@"spec"];
    property.salePrice = [[dic safeObjectForKey:@"saleprice"] doubleValue];
    return property;
}

@end

@implementation EcomBPropertyInfo

+(id)ecomBPropertyInfoWithDictionary:(NSDictionary *)dic
{
    return  [[self alloc]initWithDictionary:dic];
}
-(id)initWithDictionary:(NSDictionary *)dic
{
    self = [super init];
    if (self) {
        self.ecomBPropertyInfoImageStr = [dic safeObjectForKey:@"imgurl"];
        self.ecomBPropertyInfoProductname = [dic safeObjectForKey:@"productname"];
        self.ecomBPropertyInfoSalesprice = [[dic safeObjectForKey:@"salesprice"] floatValue];
        self.ecomBCarouseInfoStockcount = [[dic safeObjectForKey:@"stockcount"] intValue];
        self.ecomBPropertyInfoProperties = [EcomBProperty EcomBPropertyWithArray:[dic safeObjectForKey:@"propertylist"]];
        self.ecomBPropertyInfoSpecNames = [dic safeObjectForKey:@"attrs"];
        
        if ([[dic objectForKey:@"attflag"] integerValue] == 1)
        {
            self.isOld = NO;
        }
        else
        {
            self.isOld = YES;
        }
    }
    return self;
}
@end

@implementation EcomBShopingCarInfo

+(id)EcomBShopingCarInfo:(NSArray *) arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc] initWithDictionary:dic]];
    }
    return dataArr;
}
-(id)initWithDictionary:(NSDictionary *)dic
{
    EcomBShopingCarInfo * shopingCar = [[EcomBShopingCarInfo alloc]init];
    
    shopingCar.shopingCarInfoCount = [[dic safeObjectForKey:@"gocarnumber"] intValue];
    shopingCar.shopingCarInfoImageStr = [dic safeObjectForKey:@"imgurl"];
    shopingCar.shopingCarInfoPrice = [[dic safeObjectForKey:@"salesprice"] floatValue];
    shopingCar.shopingCarInfoProductName = [dic safeObjectForKey:@"productname"];
    shopingCar.shopingCarInfoFreight = [[dic safeObjectForKey:@"freight"] floatValue];
    shopingCar.shopingCarInfoProductid = [[dic safeObjectForKey:@"productid"] intValue];
    shopingCar.shopingCarInfoPropertyid = [[dic safeObjectForKey:@"propertyid"] intValue];
    shopingCar.shopingCarInfoStock = [[dic safeObjectForKey:@"stock"] intValue];
    shopingCar.jifenflag = [[dic safeObjectForKey:@"jifenflag"] boolValue];
    return shopingCar;
}

@end

@implementation EcomBOrderInfo


+(id)EcomBMyOrderWithArray:(NSArray *)arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc]initWithDictionary:dic]];
    }
    return dataArr;
}
-(id)initWithDictionary:(NSDictionary *)dic
{
    EcomBOrderInfo * orderInfo = [[EcomBOrderInfo alloc]init];
    orderInfo.orderAmount = [[dic safeObjectForKey:@"amount"] floatValue];
    orderInfo.orderOrderid = [dic safeObjectForKey:@"orderid"];
    
    orderInfo.orderPhone = [[dic safeObjectForKey:@"phone"] isKindOfClass:[NSNull class]]?@"":[dic objectForKey:@"phone"];
    orderInfo.orderProductcount = [[dic safeObjectForKey:@"productcount"] intValue];
    orderInfo.orderStatus = [[dic safeObjectForKey:@"status"] intValue];
    orderInfo.orderTime = [dic safeObjectForKey:@"ordertime"];
    orderInfo.orderImageArr = [self orderImageArrWithArray:[dic safeObjectForKey:@"imglist"]];
    
    return orderInfo;
}
-(NSArray *)orderImageArrWithArray:(NSArray *)arr
{
    NSMutableArray * imageArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [imageArr addObject:[dic safeObjectForKey:@"imgurl"]];
    }
    return imageArr;
}
@end

@implementation EcombOrderGools

+(id)ecombOrderGoolsWithArray:(NSArray *)arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc]initWithDictionary:dic]];
    }
    return dataArr;
}
-(id)initWithDictionary:(NSDictionary *)dic
{
    EcombOrderGools * gools = [[EcombOrderGools alloc]init];
    gools.goolsCount = [[dic safeObjectForKey:@"count"] intValue];
    gools.goolsDscription = [dic safeObjectForKey:@"productname"];
    gools.goolsPrice = [[dic safeObjectForKey:@"price"] floatValue];
    gools.jifenflag = [[dic safeObjectForKey:@"jifenflag"] boolValue];
    
    return gools;
}


@end