//
//  ZSTEComBGoodsShowViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBGoodsShowViewController.h"
#import "ZSTEComBAskOnLineViewController.h"
#import "ZSTECBProductDetailViewController.h"
#import "ZSTECBShoppingCartViewController.h"
#import "ZSTECBShoppingCartViewController.h"
#import "ZSTWebViewController.h"
#import "ZSTF3Engine+EComB.h"
#import "EGOPhotoSource.h"
#import <SDKExport/WXApi.h>
#import "ZSTUtils.h"
#import <MessageUI/MessageUI.h>
#import "ZSTHHSinaShareController.h"
#import "BaseNavgationController.h"

@interface ZSTEComBGoodsShowViewController ()<ZSTHHSinaShareControllerDelegate,MFMessageComposeViewControllerDelegate>

@end

@implementation ZSTEComBGoodsShowViewController
{
    UILabel * xianshichuxiaoLabel;
    ZSTECBProduct * _product;
    ZSTRoundRectView * _zhixunButtonRound;
    ZSTRoundRectView * _line;
    UIScrollView * bgScrollView;
    
    NSString *shareurl;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _product = nil;
	// Do any additional setup after loading the view.
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"商品展示", nil)];    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"分享",nil) target:self selector:@selector (shareAction)];
    
    self.view.backgroundColor = [UIColor whiteColor];

    bgScrollView = [[UIScrollView alloc]init];
    bgScrollView.frame = self.view.bounds;
    bgScrollView.delegate = self;
    bgScrollView.showsHorizontalScrollIndicator = NO;
    bgScrollView.showsVerticalScrollIndicator = NO;
    bgScrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:bgScrollView];

    _carouseView = [[ZSTECBHomeCarouselView alloc]initWithFrame:CGRectMake(0, 0,320,320)];
    _carouseView.carouselDelegate = self;
    _carouseView.carouselDataSource = self;
    [_carouseView setCarouselViewBackgroundImg:@"module_ecomb_goodsshow.png"];
    [bgScrollView addSubview:_carouseView];

    _titleLabel = [[UILabel alloc]init];
    _titleLabel.font = [UIFont boldSystemFontOfSize:17];
    _titleLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
    [bgScrollView addSubview:_titleLabel];
    
    _priceLabel = [[UILabel alloc]init];
    _priceLabel.font = [UIFont boldSystemFontOfSize:18];
    _priceLabel.textColor = RGBCOLOR(202, 8, 20);
    [bgScrollView addSubview:_priceLabel];

    
    xianshichuxiaoLabel  = [[UILabel alloc]init];
    xianshichuxiaoLabel.backgroundColor = RGBCOLOR(202, 8, 20);
    xianshichuxiaoLabel.textAlignment = NSTextAlignmentCenter;
    [xianshichuxiaoLabel setTextColor:[UIColor whiteColor]];
    xianshichuxiaoLabel.font = [UIFont boldSystemFontOfSize:12];
    [bgScrollView addSubview:xianshichuxiaoLabel];
    
    
    _originalPriceLabel = [[UILabel alloc]init];
    _originalPriceLabel.font = [UIFont systemFontOfSize:12];
    _originalPriceLabel.textColor = RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5);
    [bgScrollView addSubview:_originalPriceLabel];
    
    _pointLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_originalPriceLabel.frame)+5, _originalPriceLabel.frame.origin.y, 60, 21)];
    _pointLabel.hidden = YES;
    _pointLabel.backgroundColor = RGBCOLOR(248, 121, 41);
    _pointLabel.textColor = [UIColor whiteColor];
    _pointLabel.textAlignment = NSTextAlignmentCenter;
    _pointLabel.font = [UIFont systemFontOfSize:12];
    _pointLabel.text = @"积分换购";
    [bgScrollView addSubview:_pointLabel];
    
    _freightPriceLabel = [[UILabel alloc]init];
    _freightPriceLabel.font = [UIFont systemFontOfSize:12];
    _freightPriceLabel.textColor = RGBCOLOR(56, 56, 56);
    _freightPriceLabel.text = @"运费:￥0.0";
    [bgScrollView addSubview:_freightPriceLabel];
    
    _slip = [[UILabel alloc]init];
    _slip.backgroundColor = RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5);
    [bgScrollView addSubview:_slip];
    [_slip release];
    
    
    _menuView = [[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_carouseView.frame), 320, 190 + 60)] autorelease];
    _menuView.backgroundColor = [UIColor clearColor];
    [bgScrollView addSubview:_menuView];
    
    ZSTEComBPutInCarButton * putInBtn = [ZSTEComBPutInCarButton buttonWithType:UIButtonTypeCustom];
    [putInBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_orange.png") forState:UIControlStateNormal];
    putInBtn.frame = CGRectMake(15, 5, 273.0/2, 63.0/2.0);
    [putInBtn setImage:ZSTModuleImage(@"module_ecomb_goodsshow_putincarbtn.png") forState:UIControlStateNormal];
    [putInBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    putInBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [putInBtn addTarget:self action:@selector(putInCart) forControlEvents:UIControlEventTouchUpInside];
    [_menuView addSubview:putInBtn];
    
    UIButton * buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_red.png") forState:UIControlStateNormal];
    buyBtn.frame = CGRectMake(320 - 15 - (273.0/2),  5, 273.0/2, 63.0/2.0);
    [buyBtn setTitle:@"￥  立即购买" forState:UIControlStateNormal];
    buyBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [buyBtn addTarget:self action:@selector(buyAction) forControlEvents:UIControlEventTouchUpInside];
    [_menuView addSubview:buyBtn];
    
    //商品详情
    ZSTRoundRectView * goolsButtonRound = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(putInBtn.frame) + 15 , 290, 40) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5)];
    [_menuView addSubview:goolsButtonRound];
    [goolsButtonRound release];
    //右边箭头
    [goolsButtonRound showImage:CGRectMake(goolsButtonRound.frame.size.width - 10 - 14, goolsButtonRound.frame.size.height/2 - 8, 14, 14) image:ZSTModuleImage(@"module_ecomb_right.png")];
    [goolsButtonRound showImage:CGRectMake(15, (goolsButtonRound.frame.size.height-14)/2, 14, 14) image:ZSTModuleImage(@"module_ecomb_gools_show.png")];
    [goolsButtonRound showText:CGRectMake(15 + 14 + 10, 5 , 200, 30) text:@"商品详情" font:[UIFont systemFontOfSize:14]];
    //添加事件
    UIControl * goolsControl = [[UIControl alloc]initWithFrame:goolsButtonRound.bounds];
    [goolsControl addTarget:self action:@selector(goodsInfoAction) forControlEvents:UIControlEventTouchUpInside];
    [goolsButtonRound addSubview:goolsControl];
    [goolsControl release];
    
    
    
    //电话咨询，在线咨询
    _zhixunButtonRound = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(15, CGRectGetMaxY(goolsButtonRound.frame) + 10 , 290, 80) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5)];
    [_menuView addSubview:_zhixunButtonRound];
    [_zhixunButtonRound release];
    
     _line = [[ZSTRoundRectView alloc]initWithPoint:CGPointMake(CGRectGetMinX(_zhixunButtonRound.frame), _zhixunButtonRound.centerY) toPoint:CGPointMake(CGRectGetMaxX(_zhixunButtonRound.frame), _zhixunButtonRound.centerY) borderWidth:1.0f borderColor:RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5)];
    [_menuView addSubview:_line];
    [_line release];
    
    //在线
    [_zhixunButtonRound showImage:CGRectMake(goolsButtonRound.frame.size.width - 10 - 14, goolsButtonRound.frame.size.height/2 - 8, 14, 14) image:ZSTModuleImage(@"module_ecomb_right.png")];
    [_zhixunButtonRound showImage:CGRectMake(15, (goolsButtonRound.frame.size.height-14)/2, 14, 14) image:ZSTModuleImage(@"module_ecomb_online_ask.png")];
    [_zhixunButtonRound showText:CGRectMake(15 + 14 + 10, 5 , 200, 30) text:@"咨询留言" font:[UIFont systemFontOfSize:14]];
    //添加事件
    UIControl * onlineControl = [[UIControl alloc]initWithFrame:CGRectMake(CGRectGetMinX(_zhixunButtonRound.frame), 0, _zhixunButtonRound.frame.size.width ,_zhixunButtonRound.frame.size.height/2.0)];
    [onlineControl addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    onlineControl.tag = 0;
    [_zhixunButtonRound addSubview:onlineControl];
    [onlineControl release];
    
    
    //电话
    [_zhixunButtonRound showImage:CGRectMake(goolsButtonRound.frame.size.width - 10 - 14,goolsButtonRound.frame.size.height + goolsButtonRound.frame.size.height/2 - 8, 14, 14) image:ZSTModuleImage(@"module_ecomb_right.png")];
    [_zhixunButtonRound showImage:CGRectMake(15,goolsButtonRound.frame.size.height + (goolsButtonRound.frame.size.height-14)/2, 14, 14) image:ZSTModuleImage(@"module_ecomb_myorder_call.png")];
    [_zhixunButtonRound showText:CGRectMake(15 + 14 + 10, 5 + goolsButtonRound.frame.size.height , 200, 30) text:@"电话咨询" font:[UIFont systemFontOfSize:14]];
    //添加事件
    UIControl * callControl = [[UIControl alloc]initWithFrame:CGRectMake(CGRectGetMinX(_zhixunButtonRound.frame), goolsButtonRound.frame.size.height + 5, _zhixunButtonRound.frame.size.width, _zhixunButtonRound.frame.size.height/2.0)];
    [callControl addTarget:self action:@selector(callAction:) forControlEvents:UIControlEventTouchUpInside];
    callControl.tag = 1;
    [_zhixunButtonRound addSubview:callControl];
    [callControl release];
    
    
    UIButton * shopingCarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shopingCarBtn.frame = CGRectMake(320 - 15 - 40, CGRectGetMaxY(_zhixunButtonRound.frame) + 10, 40, 40);
    [shopingCarBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_goodsshow_shopingcarbtn.png") forState:UIControlStateNormal];
    [shopingCarBtn addTarget:self action:@selector(shopingCarBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [_menuView addSubview:shopingCarBtn];

    [self.engine getProductsShowInfo:_pageInfo.ecombHomeProductid];
    [self.engine getEcomBGoodsInfoCarousel:_pageInfo.ecombHomeProductid];
    
}
-(void)shopingCarBtnAction:(UIButton *) btn
{
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    
    ZSTECBShoppingCartViewController * shoppintCarViewController = [[ZSTECBShoppingCartViewController alloc] init];
    shoppintCarViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shoppintCarViewController animated:YES];
    [shoppintCarViewController release];
}
-(void)getEcomBGoodsInfoCarouselDidFailed:(int)resultCode
{
    NSLog(@"商品展示页 轮播图失败");
}
-(void)getEcomBGoodsInfoCarouselDidSucceed:(NSArray *)carouseles
{
    _carouseData = [EcomBCarouseInfo ecomBCarouseWithArray:carouseles];
    if (_carouseData.count <= 1) {
        [_carouseView hideStateBar];
    }
    [_carouseView pageControlState:pageControlStateCENTER];
    [_carouseView reloadData];
}
#pragma mark - ZSTEComBCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTECBHomeCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomBCarouseInfo *)carouselView:(ZSTECBHomeCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate

- (void)carouselView:(ZSTECBHomeCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
//    if (self.carouseData.count == 0) {
//        
//        if (self.carouseData.count == 0) {
//            [self showLoadingViewWithMessage:@"加载中..."];
//            [self.client getUserPhotoListWithDelegate:self accountId:[Preferences shared].accountId];
//            return;
//        }
//    }
    
    NSMutableArray *photos = [NSMutableArray array];
    for (int i=0; i<self.carouseData.count; i++) {
        EcomBCarouseInfo *photoInfo = [self.carouseData objectAtIndex:i];
        if (photoInfo!=nil && photoInfo.ecomBCarouseInfoCarouselurl!=nil) {
            EGOPhoto *photo = [[EGOPhoto alloc] initWithImageURL:[NSURL URLWithString:photoInfo.ecomBCarouseInfoCarouselurl]];
            [photos addObject:photo];
        }
    }
    
    EGOPhotoSource *source = [[EGOPhotoSource alloc] initWithPhotos:photos];
    EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithPhotoSource:source];
    photoController.pageIndex = index;
    photoController.haveBackButton = YES;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];
    
    
//    photoController.delegate = self;
//    photoController.accountId = [Preferences shared].accountId;
    
//     navController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"返回" style:UIBarButtonItemStylePlain target:navController action:@selector(dismissModalViewControllerAnimated:)];
    
//    [self.navigationController pushViewController:navController animated:YES];
    [self presentViewController:navController animated:YES completion:nil];

    [photoController release];
    [navController release];
}
//-(void)dismissModalViewControllerAnimated:(BOOL) animated
//{
//    [self dismissViewControllerAnimated:YES completion:nil];
//}
- (void) setProductDetail:(ZSTECBProduct *)product
{
    shareurl = [[NSString stringWithFormat:@"http://mod.pmit.cn/EcomB/detail.html?moduletype=%ld&productid=%@&ecid=%@&module_type=%ld",(long)self.moduleType,product.productID,[ZSTF3Preferences shared].ECECCID,(long)self.moduleType] retain];
    
    _titleLabel.text = product.productName;
    _titleLabel.numberOfLines = 0;
    _titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size1 = [_titleLabel.text sizeWithFont:_titleLabel.font constrainedToSize:CGSizeMake(290, 44) lineBreakMode:NSLineBreakByWordWrapping];
    _titleLabel.frame = CGRectMake(15, CGRectGetMaxY(_carouseView.frame) + 10, size1.width, size1.height);
    
    _priceLabel.text = [NSString stringWithFormat:@"%@%@",@"￥",product.salesPrice];
    _priceLabel.numberOfLines = 0;
    _priceLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size2 = [_priceLabel.text sizeWithFont:_priceLabel.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
    _priceLabel.frame = CGRectMake(15, CGRectGetMaxY(_titleLabel.frame) + 10, size2.width, size2.height);
    
    _originalPriceLabel.text = [NSString stringWithFormat:@"%@:%@",product.primeLabel,product.primePrice];
    _originalPriceLabel.numberOfLines = 0;
    _originalPriceLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size3 = [_originalPriceLabel.text sizeWithFont:_originalPriceLabel.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
    _originalPriceLabel.frame = CGRectMake(15, CGRectGetMaxY(_priceLabel.frame) + 10 , size3.width, size3.height);
    _slip.frame = CGRectMake(CGRectGetMinX(_originalPriceLabel.frame), _originalPriceLabel.centerY, CGRectGetWidth(_originalPriceLabel.frame), 1);
    
    _pointLabel.hidden = !product.jifenflag;
    
    NSString *discountStr = product.salesLabel;
    
    if (product.discount && [product.discount floatValue] < 10 && [product.discount floatValue] > 0) {
        
        discountStr = [discountStr stringByAppendingString:[NSString stringWithFormat:@" (%@折)",product.discount]];
    }
    
    xianshichuxiaoLabel.text = discountStr;
    CGSize xianshichuxiaoLabelsize = [xianshichuxiaoLabel.text sizeWithFont:xianshichuxiaoLabel.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
    xianshichuxiaoLabel.frame = CGRectMake(CGRectGetMaxX(_priceLabel.frame) + 20, CGRectGetMinY(_priceLabel.frame), xianshichuxiaoLabelsize.width + 30, 20);
    xianshichuxiaoLabel.layer.cornerRadius = 4;
    xianshichuxiaoLabel.clipsToBounds = YES;
    
    _pointLabel.frame = CGRectMake(CGRectGetMaxX(xianshichuxiaoLabel.frame)+5, xianshichuxiaoLabel.frame.origin.y, 60, 21);
    
    int freight = [product.freightPrice intValue];
    if (freight > 0) {
        _freightPriceLabel.text = [NSString stringWithFormat:@"运费￥:%.2f", [product.freightPrice  floatValue]];
    }
    else{
        _freightPriceLabel.text = @"包邮";
    }
//    _freightPriceLabel.text = [NSString stringWithFormat:@"运费￥：%.2f", [product.freightPrice  floatValue]];
    CGSize size4 = [_freightPriceLabel.text sizeWithFont:_freightPriceLabel.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
    _freightPriceLabel.frame = CGRectMake(320 - 15 - size4.width, CGRectGetMinY(_originalPriceLabel.frame) , size4.width, size4.height);
    
    
    CGRect frame = _menuView.frame;
    frame.origin.y = CGRectGetMaxY(_originalPriceLabel.frame);
    _menuView.frame = frame;
    
    if (_product.phone.length == 0){
        _zhixunButtonRound.frame = CGRectMake( 15, 101.5, 290, 40);
        _zhixunButtonRound.clipsToBounds = YES;
        _line.hidden = YES;
        _menuView.frame = CGRectMake(0, CGRectGetMaxY(_originalPriceLabel.frame), 320, 190 + 20);
        UIButton * btn = [[_menuView subviews] objectAtIndex:5];
        btn.frame = CGRectMake(320 - 15 - 40, CGRectGetMaxY(_zhixunButtonRound.frame) + 10, 40, 40);
    }

    bgScrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(_menuView.frame) + 10);
}
- (void) putInCart
{
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    
    ZSTECBPropertyViewController * propertyViewController = [[ZSTECBPropertyViewController alloc]init];
    propertyViewController.push = 0;
//    propertyViewController.delegate = self;
    propertyViewController.productid = _pageInfo.ecombHomeProductid;
    propertyViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:propertyViewController animated:YES];
    [propertyViewController release];
        
}

- (void) buyAction
{
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }

    
    //立即购买
    ZSTECBPropertyViewController * propertyViewController = [[ZSTECBPropertyViewController alloc]init];
    propertyViewController.push = 1;
//    propertyViewController.delegate = self;
    propertyViewController.productid = _pageInfo.ecombHomeProductid;
    propertyViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:propertyViewController animated:YES];
    [propertyViewController release];
//    ZSTECBPropertyViewController *controller = [[ZSTECBPropertyViewController alloc] init];
//    [self.navigationController pushViewController:controller animated:YES];
//    [controller release];
}

-(void)callAction:(UIControl *)control
{
    if (control.tag == 0) {
        NSLog(@"在线咨询");
        ZSTEComBAskOnLineViewController * ascViewController = [[ZSTEComBAskOnLineViewController alloc]init];
        ascViewController.productid = _pageInfo.ecombHomeProductid;
        ascViewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:ascViewController animated:YES];
        [ascViewController release];
    }
    else
    {
        NSLog(@"电话咨询");
        if (_product.phone.length == 0) {
            return;
        }
        UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:_product.phone delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"呼叫" otherButtonTitles:nil];
        sheet.tag = 100;
        [sheet showInView:self.view];
    }
}
-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
   if (actionSheet.tag == 100) {
    if (buttonIndex == 0) {
        NSString * tel = [NSString stringWithFormat:@"tel://%@",_product.phone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
    }
    else
    {
        NSLog(@"电话咨询返回");
    }
    
  } else if (actionSheet.tag == 101) {
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            return;
        }
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]
            || [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈" , nil)]){
            
            if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
                
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:NSLocalizedString(@"提示" , nil)
                                      message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                      delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                      otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
                alert.tag = 108;
                [alert show];
                return;
            }
            
            WXMediaMessage *message = [WXMediaMessage message];
            [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
            message.title = _product.productName;
            message.description = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！", nil),appDisplayName];

            
            WXImageObject *ext = [WXImageObject object];
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
            ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
            message.mediaObject = ext;
            
            WXWebpageObject *webpage = [WXWebpageObject object];
            webpage.webpageUrl = [shareurl length] ? shareurl:[NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
            message.mediaObject = webpage;
            
            SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
            req.bText = NO;
            req.message = message;
            
            if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]) {
                req.scene = WXSceneSession;
            }
            else  if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈"  , nil)]){
                req.scene = WXSceneTimeline;
            }
            
            [WXApi sendReq:req];
            
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"短信" , nil)]){
            
            Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
            
            if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
                
                MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
            
                NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
                picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在“%@”，看到一个不错的商品，你肯定喜欢，点击链接：%@，手机客户端下载地址：%@/%@", nil),appDisplayName,shareurl,sharebaseurl,[ZSTF3Preferences shared].ECECCID];
               
                
                
                picker.messageComposeDelegate = self;
                
                [self presentViewController:picker animated:YES completion:nil];

                
                [picker release];
            } else {
                
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
            }
        }else{
            
            ZSTHHSinaShareController *sinaShare = [[ZSTHHSinaShareController alloc] init];
            sinaShare.delegate = self;
           
            //NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
                
            sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！链接：%@", nil),appDisplayName,shareurl];
            
            if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"新浪微博" , nil)]) {
                sinaShare.shareType = sinaWeibo_ShareType;
                sinaShare.navigationItem.title = NSLocalizedString(@"新浪微博", nil);
            }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"QQ空间" , nil)]){
                sinaShare.shareType = QQ_ShareType;
                sinaShare.navigationItem.title = NSLocalizedString(@"QQ空间", nil);
            }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"腾讯微博" , nil)]){
                sinaShare.shareType = TWeibo_ShareType;
                sinaShare.navigationItem.title = NSLocalizedString(@"腾讯微博", nil);
            }
            
            UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:sinaShare];
            [self presentViewController:n animated:YES completion:nil];

            [n release];
            [sinaShare release];
        }

    }
}
-(void)goodsInfoAction
{
    NSLog(@"跳转商品详情");
    ZSTECBProductDetailViewController *controller = [[ZSTECBProductDetailViewController alloc] init];
    controller.productID = [_product.productID intValue];
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

#pragma mark- ZSTHHSinaShareControllerDelegate

- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}
#pragma mark wxShare

- (void)wxShareSucceed
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"微信分享成功", nil) withImage:nil];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0) {
        NSString* installUrl =  [WXApi getWXAppInstallUrl];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:installUrl]];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"分享成功!" , nil)];
    }
}

- (void)shareAction
{
    UIActionSheet *shareActionSheet = nil;
    NSMutableArray *shareNames = [NSMutableArray array];
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    if (pre.SinaWeiBo) {
        [shareNames addObject:NSLocalizedString(@"新浪微博",nil)];
    }
    if (pre.TWeiBo) {
        [shareNames addObject:NSLocalizedString(@"腾讯微博",nil)];
    }
    if (pre.QQ) {
        [shareNames addObject:NSLocalizedString(@"QQ空间",nil)];
    }
    if (pre.WeiXin) {
        [shareNames addObject:NSLocalizedString(@"微信好友",nil)];
        [shareNames addObject:NSLocalizedString(@"微信朋友圈",nil)];
    }
    
    [shareNames addObject:NSLocalizedString(@"短信",nil)];
    
    shareActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"分享到", nil)
                                                   delegate:self
                                          cancelButtonTitle:nil
                                     destructiveButtonTitle:nil
                                          otherButtonTitles:nil];
    
    for (NSString * title in shareNames) {
        [shareActionSheet addButtonWithTitle:title];
    }
    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消",nil)];
    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons-1;
    shareActionSheet.tag = 101;
    [shareActionSheet showInView:self.view.window];
    [shareActionSheet release];
}


- (void)getProductsShowDidSucceed:(ZSTECBProduct *)dic
{
    if (dic) {
        _product = [dic retain];
        [self setProductDetail:dic];
    }
}

- (void)getProductsShowFaild:(int)resultCode
{
    [TKUIUtil alertInWindow:@"没有数据" withImage:nil];
    [_carouseView hideStateBar];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc
{
    _carouseView.carouselDataSource = nil;
    _carouseView.carouselDelegate = nil;
//    [_carouseView release];
    [_priceLabel release];
    [_originalPriceLabel release];
    [super dealloc];
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    
}

@end
