//
//  ZSTECBProductTableImageCellView.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBProductTableImageCellView.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTECBProductTableImageCellView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.clipsToBounds = YES;
        
//        _imageView = [[TKAsynImageView alloc]initWithFrame:CGRectMake(0, 0, 140, 140)];
//        _imageView.defaultImage = [UIImage imageNamed:@"Module.bundle/module_ecomb_homepage_default_img.png"];
//        _imageView.adorn = ZSTModuleImage(@"module_ecomb_homepage_default_img.png");
//        [_imageView clear];
        _imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 140, 140)];
        [self addSubview:_imageView];
        
        
        _textLabel = [[UILabel alloc]initWithFrame:CGRectMake(5, 140, 130, 35)];
        _textLabel.font = [UIFont systemFontOfSize:12];
        _textLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
//        _textLabel.text = @"情人节 情人节 情人节 情人节 情人节 情人节 情人节 ";
        _textLabel.numberOfLines = 0;
        _textLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:_textLabel];
//        CGSize size1 = [_textLabel.text sizeWithFont:_textLabel.font constrainedToSize:CGSizeMake(136, 36) lineBreakMode:NSLineBreakByWordWrapping];
//        _textLabel.frame = CGRectMake(2, 140, size1.width, size1.height);

        
        _priceLabel = [[UILabel alloc]initWithFrame:CGRectMake(3, 175, 130, 25)];
        _priceLabel.font = [UIFont systemFontOfSize:17];
        _priceLabel.textColor = RGBCOLOR(202, 8, 20);
//        _priceLabel.text = @"￥258.00";
        _priceLabel.numberOfLines = 0;
        _priceLabel.lineBreakMode = NSLineBreakByWordWrapping;
        [self addSubview:_priceLabel];
//        CGSize size3 = [_priceLabel.text sizeWithFont:_priceLabel.font constrainedToSize:CGSizeMake(136, 16) lineBreakMode:NSLineBreakByWordWrapping];
//        _priceLabel.frame = CGRectMake(2, CGRectGetMaxY(_textLabel.frame) , size3.width, size3.height);
        
        _originalPriceLabel = [[UILabel alloc]init];
        _originalPriceLabel.font = [UIFont systemFontOfSize:12];
        _originalPriceLabel.textColor = RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5);
        [self addSubview:_originalPriceLabel];
        
        _slip = [[UILabel alloc]init];
        _slip.backgroundColor = RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5);
        [self addSubview:_slip];
        
        self.layer.borderWidth = 1;
        self.layer.cornerRadius = 8;
        self.layer.borderColor = RGBCOLOR(225, 225, 225).CGColor;
        
        _pointview = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 50, 16)];
        _pointview.backgroundColor = [UIColor clearColor];
        _pointview.hidden = YES;
        [self addSubview:_pointview];
        UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 16)];
        image.backgroundColor = [UIColor clearColor];
        image.image = ZSTModuleImage(@"module_ecomb_jifen_bg.png");
        [_pointview addSubview:image];
        [image release];
        
        UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(2, 0, 25, 16)];
        label1.backgroundColor = [UIColor clearColor];
        label1.textColor = RGBCOLOR(179, 175, 84);
        label1.font = [UIFont systemFontOfSize:12];
        label1.text = @"积分";
        [_pointview addSubview:label1];
        [label1 release];
        
        UILabel *label2 = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label1.frame), 0, 20, 16)];
        label2.backgroundColor = [UIColor clearColor];
        label2.textColor = [UIColor whiteColor];
        label2.font = [UIFont systemFontOfSize:9];
        label2.text = @"换购";
        [_pointview addSubview:label2];
        [label2 release];
        
        [_pointview release];
    }
    return self;
}

-(void)reloadData:(EComBHomePageInfo *) info
{
    [info retain];
    _textLabel.text = info.ecombHomeText;
    _priceLabel.text = [NSString stringWithFormat:@"￥%@",info.ecombHomePrice];
    
    _originalPriceLabel.text = [NSString stringWithFormat:@"%@:%@",@"原价",info.ecombHomeOrangePrice];
    _originalPriceLabel.numberOfLines = 0;
    _originalPriceLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size3 = [_originalPriceLabel.text sizeWithFont:_originalPriceLabel.font constrainedToSize:CGSizeMake(235, 44) lineBreakMode:NSLineBreakByWordWrapping];
    _originalPriceLabel.frame = CGRectMake(CGRectGetMaxX(_priceLabel.frame) + 5, _priceLabel.frame.origin.y , size3.width, size3.height);
    _slip.frame = CGRectMake(CGRectGetMinX(_originalPriceLabel.frame), _originalPriceLabel.centerY, CGRectGetWidth(_originalPriceLabel.frame), 1);
    
//    [_imageView clear];
//    _imageView.url = [NSURL URLWithString:info.ecombHomeImage];;
//    [_imageView loadImage];
    [_imageView setImageWithURL:[NSURL URLWithString:info.ecombHomeImage] placeholderImage:[UIImage imageNamed:@"Module.bundle/module_ecomb_homepage_default_img.png"]];
    
    
    _pointview.hidden = !info.jifenflag;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}_ecombHomeImage	__NSCFString *	@"http://localhost:7788/temp/myuploadimg/cutimg_20140305161626801.jpg"	0x145c8e30
 
 TKAsynImageView *asynImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(i*self.frame.size.width, 0, self.frame.size.width, self.frame.size.height)] autorelease];
 asynImageView.defaultImage = [UIImage imageNamed:@"Module.bundle/module_articlea_scroll_default_img.png"];
 asynImageView.asynImageDelegate = self;
 asynImageView.adorn = ZSTModuleImage(@"module_articlea_scroll_default_img.png");
 [asynImageView clear];
 asynImageView.url = [NSURL URLWithString:[GetInfoFile stringByAppendingString:[NSString stringWithFormat:@"?FileID=%@",[dataDic objectForKey:@"IConFileID"]]]];;
 [asynImageView loadImage];

*/
//-(void)dealloc
//{
//    [_imageView release];
//    [_textLabel release];
//    [_priceLabel release];
//    [super dealloc];
//}
@end
