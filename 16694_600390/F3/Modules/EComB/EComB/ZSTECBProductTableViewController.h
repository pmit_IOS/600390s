//
//  ZSTECBProductTableViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComB.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTECBHomeCarouselView.h"
#import "ZSTECBProductTableImageCell.h"
#import "EGORefreshTableHeaderView.h"
#import "ZSTLoginViewController.h"
#import "ZSTLoginController.h"

@interface ZSTECBProductTableViewController : ZSTModuleBaseViewController<UITableViewDataSource,UITableViewDelegate,ZSTECBHomeCarouselViewDataSource,TKLoadMoreViewDelegate,ZSTECBHomeCarouselViewDelegate,ZSTECBProductTableImageCellDelegate,ZSTF3EngineECBDelegate,LoginDelegate,ZSTLoginControllerDelegate>
{
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    NSInteger _pagenum;
}
@property (retain,nonatomic) UIView * headView;
@property (retain,nonatomic) UITableView * tableView;
@property (retain,nonatomic) NSMutableArray * btnTextArr;
@property (retain,nonatomic) ZSTECBHomeCarouselView * carouselView;
@property (retain,nonatomic) NSArray * carouseData;
@property (retain,nonatomic) NSMutableArray * resultData;

@end