//
//  ZSTECBCategoryFolderCell.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECBCategoryFolderCell : UITableViewCell

@property (nonatomic, retain) NSMutableArray *iconViewes;

- (void)configCell:(NSArray *)categories forRowAtIndex:(NSIndexPath *)indexPath;

@end


