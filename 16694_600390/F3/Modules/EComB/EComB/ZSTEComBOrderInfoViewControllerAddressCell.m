//
//  ZSTEComBOrderInfoViewControllerAddressCell.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoViewControllerAddressCell.h"

@implementation ZSTEComBOrderInfoViewControllerAddressCell
{
    ZSTRoundRectView * _noDataBackgroundView;
    ZSTRoundRectView * _haveDatabackgroundView;
    ZSTRoundRectView * _line;
}
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
       
        
//        UIImageView * bg = [[UIImageView alloc]init];
//        bg.frame = CGRectMake(5, 30, 310, 81);
//        bg.image = ZSTModuleImage(@"module_ecomb_orderinfo_addresscell_bg.png");
//        [self.contentView addSubview:bg];
//        [bg release];
        
        UILabel * l1 = [[UILabel alloc]init];
        l1.font = [UIFont systemFontOfSize:17];
        l1.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        l1.text = @"收货信息";
        [self.contentView addSubview:l1];
        CGSize size1 = [l1.text sizeWithFont:l1.font];
        l1.frame = CGRectMake(12, 7, size1.width, 22);
        [l1 release];
        
        _noDataBackgroundView = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(5, 30, 310, 41) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
        _noDataBackgroundView.backgroundColor = [UIColor whiteColor];
        [_noDataBackgroundView showImage:CGRectMake(12, (_noDataBackgroundView.frame.size.height - 14)/2, 13, 14) image:ZSTModuleImage(@"module_ecomb_add.png")];
        [self.contentView addSubview:_noDataBackgroundView];
        [_noDataBackgroundView release];
        
        UILabel * addAddress = [[UILabel alloc]init];
        addAddress.font = [UIFont boldSystemFontOfSize:14];
        addAddress.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        addAddress.text = @"添加地址";
        addAddress.textColor = [UIColor blackColor];
        addAddress.frame = CGRectMake(30, 5, 235, 30);
        [_noDataBackgroundView addSubview:addAddress];
        addAddress.numberOfLines = 0;
        addAddress.lineBreakMode = NSLineBreakByWordWrapping;
//        addAddress.backgroundColor = [UIColor yellowColor];
        
        
        _haveDatabackgroundView = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(5, 30, 310, 81) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
        _haveDatabackgroundView.backgroundColor = [UIColor whiteColor];
        [_haveDatabackgroundView showImage:CGRectMake(310 - 8 - 13, (_haveDatabackgroundView.frame.size.height - 14)/2, 13, 14) image:ZSTModuleImage(@"module_ecomb_right.png")];
        [self.contentView addSubview:_haveDatabackgroundView];
        [_haveDatabackgroundView release];
        _haveDatabackgroundView.hidden = YES;
        _haveDatabackgroundView.userInteractionEnabled = NO;
        
//        _line = [[ZSTRoundRectView alloc]initWithPoint:CGPointMake(0, CGRectGetHeight(_backgroundView.frame)/2) toPoint:CGPointMake(_backgroundView.frame.size.width, _backgroundView.frame.size.height/2) borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
//        [_backgroundView addSubview:_line];
//        [_line release];
        
        
        
        UIImageView * addressImgView  = [[UIImageView alloc]init];
        addressImgView.frame = CGRectMake(8, (40 - 14)/2, 13, 14);
        addressImgView.image = ZSTModuleImage(@"module_ecomb_orderinfo_location.png");
        [_haveDatabackgroundView addSubview:addressImgView];
        [addressImgView release];
        
        _addressLabel = [[UILabel alloc]init];
        _addressLabel.font = [UIFont systemFontOfSize:14];
        _addressLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        _addressLabel.frame = CGRectMake(30, 5, 235, 30);
        [_haveDatabackgroundView addSubview:_addressLabel];
        _addressLabel.numberOfLines = 0;
        _addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
//        _addressLabel.backgroundColor = [UIColor yellowColor];
        
        
        
        UIImageView * callImgView  = [[UIImageView alloc]init];
        callImgView.frame = CGRectMake(8, 40 + (40 - 14)/2, 14, 13);
        callImgView.image = ZSTModuleImage(@"module_ecomb_myorder_call.png");
        [_haveDatabackgroundView addSubview:callImgView];
        [callImgView release];
        
        _tellLabel = [[UILabel alloc]init];
        _tellLabel.font = [UIFont systemFontOfSize:14];
        _tellLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        [_haveDatabackgroundView addSubview:_tellLabel];
        _tellLabel.numberOfLines = 0;
        _tellLabel.lineBreakMode = NSLineBreakByWordWrapping;
//        _tellLabel.backgroundColor = [UIColor yellowColor];
        
        
        UIControl * contro = [[UIControl alloc]initWithFrame:CGRectMake(0, 0, 320, 125)];
        [contro addTarget:self action:@selector(actionClick:) forControlEvents:UIControlEventTouchUpInside];
//        contro.backgroundColor = [UIColor darkGrayColor];
        [self.contentView addSubview:contro];
        [contro release];
    }
    return self;
}
-(void) initData:(EcomBAddress *)address
{
   
        if (address) {
            if (address.ecomAddress.length == 0) {
                NSLog(@"地址为空");
            }
            else{
                _noDataBackgroundView.hidden = YES;
                _noDataBackgroundView.userInteractionEnabled = NO;
                
                _haveDatabackgroundView.hidden = NO;
                _haveDatabackgroundView.userInteractionEnabled = YES;
                
                _addressLabel.text = address.ecomAddress;
                CGSize size2 = [_addressLabel.text sizeWithFont:_addressLabel.font constrainedToSize:CGSizeMake(235, 40) lineBreakMode:NSLineBreakByWordWrapping];
                _addressLabel.frame = CGRectMake(30, (40 - 14)/2, size2.width, size2.height);
                
                if (address.ecomMobile.length) {
                    _tellLabel.text = [NSString stringWithFormat:@"%@ (%@)",address.ecomUserName,address.ecomMobile];
                    CGSize size = [_tellLabel.text sizeWithFont:_tellLabel.font constrainedToSize:CGSizeMake(235, 40) lineBreakMode:NSLineBreakByWordWrapping];
                    _tellLabel.frame = CGRectMake(30, 40 + (40 - 22)/2, size.width, 22);
                }
            }
        }
    
}

-(void)actionClick:(UIControl *)contro
{
    [_delegate addressDidPressed];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
//-(void)dealloc
//{
//    
//    [_addressLabel release];
//    [_tellLabel release];
//    [super dealloc];
//}
@end
