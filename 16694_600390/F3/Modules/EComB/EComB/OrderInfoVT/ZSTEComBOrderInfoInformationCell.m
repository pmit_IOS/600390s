//
//  ZSTEComBOrderInfoInformationCell.m
//  EComB
//
//  Created by qiuguian on 15/7/27.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBOrderInfoInformationCell.h"

@implementation ZSTEComBOrderInfoInformationCell

-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        UIView * _bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 60)];
        _bgView.backgroundColor = RGBCOLOR(243, 243, 243);
        [self.contentView addSubview:_bgView];
        [_bgView release];
        
        _goalInfoLabel = [[UILabel alloc]init];
        _goalInfoLabel.font = [UIFont systemFontOfSize:14];
        _goalInfoLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
        [self.contentView addSubview:_goalInfoLabel];
        _goalInfoLabel.numberOfLines = 1;
        _goalInfoLabel.lineBreakMode = NSLineBreakByTruncatingTail;
        _goalInfoLabel.backgroundColor = [UIColor clearColor];
        
        _goalPriceLabel = [[UILabel alloc]init];
        _goalPriceLabel.font = [UIFont systemFontOfSize:14];
        _goalPriceLabel.textColor = RGBCOLOR(159, 159, 159);
        [self.contentView addSubview:_goalPriceLabel];
        _goalPriceLabel.numberOfLines = 0;
        _goalPriceLabel.lineBreakMode = NSLineBreakByWordWrapping;
        _goalPriceLabel.backgroundColor = [UIColor clearColor];
        
        _pointLabel = [[UILabel alloc] init];
        _pointLabel.hidden = YES;
        _pointLabel.backgroundColor = RGBCOLOR(248, 121, 41);
        _pointLabel.textColor = [UIColor whiteColor];
        _pointLabel.textAlignment = NSTextAlignmentCenter;
        _pointLabel.font = [UIFont systemFontOfSize:11];
        _pointLabel.text = @"积分换购";
        _pointLabel.frame = CGRectMake(CGRectGetMaxX(_goalPriceLabel.frame)+2, _goalPriceLabel.frame.origin.y, 50, 18);
        [self.contentView addSubview:_pointLabel];
        
        _goalCountLabel = [[UILabel alloc]init];
        _goalCountLabel.font = [UIFont systemFontOfSize:14];
        _goalCountLabel.textColor = RGBCOLOR(159,159,159);
        _goalCountLabel.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:_goalCountLabel];
        _goalCountLabel.numberOfLines = 0;
        _goalCountLabel.lineBreakMode = NSLineBreakByWordWrapping;
    }

    return self;
}


-(void)reloadData:(EcombOrderGools *)gools{
 
    _goalInfoLabel.text = gools.goolsDscription;
    CGSize infoSize = [_goalInfoLabel.text sizeWithFont:_goalInfoLabel.font constrainedToSize:CGSizeMake(290, 24) lineBreakMode:NSLineBreakByWordWrapping];
    //    _goalInfoLabel.backgroundColor = [UIColor yellowColor];
    _goalInfoLabel.frame = CGRectMake(15,10, infoSize.width, infoSize.height);
    
    _goalPriceLabel.text = [NSString stringWithFormat:@"￥%.2f",gools.goolsPrice];
    CGSize priceSize = [_goalPriceLabel.text sizeWithFont:_goalPriceLabel.font constrainedToSize:CGSizeMake(235, 40) lineBreakMode:NSLineBreakByWordWrapping];
    _goalPriceLabel.frame = CGRectMake(15, CGRectGetMaxY(_goalInfoLabel.frame) , priceSize.width, priceSize.height);
    
    _goalCountLabel.text = [NSString stringWithFormat:@"x%d",gools.goolsCount];
    CGSize _goalCountLabelSize = [_goalCountLabel.text sizeWithFont:_goalCountLabel.font constrainedToSize:CGSizeMake(235, 40) lineBreakMode:NSLineBreakByWordWrapping];
    _goalCountLabel.frame = CGRectMake(320 - 15 - _goalCountLabelSize.width, CGRectGetMaxY(_goalInfoLabel.frame) , _goalCountLabelSize.width, _goalCountLabelSize.height);
    
    _pointLabel.frame = CGRectMake(CGRectGetMaxX(_goalPriceLabel.frame)+5, CGRectGetMaxY(_goalInfoLabel.frame), 50, 18);
    _pointLabel.hidden = !gools.jifenflag;



}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

   
}

@end
