//
//  ZSTECBCategoriesViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBCategoriesViewController.h"
#import "ZSTUIView+Folder.h"
#import "TKNetworkIndicatorView.h"
#import "ZSTECBCategoryCell.h"
#import "ZSTECBCategoryFolderView.h"
#import "ZSTF3Engine+EComB.h"
#import "ZSTDao+EComB.h"
#import "ZSTUtils.h"
#import "ZSTEcomBCategoriesShowViewController.h"
#import "ZSTEComBGoodsSearchViewController.h"

@interface ZSTECBCategoriesViewController ()<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>
{
    NSArray * colorArr;
}
@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) NSArray *categories;

- (NSArray *)indexPathsForSection:(NSUInteger)section rowIndexSet:(NSIndexSet *)indexSet;

- (void)openFolderForCategories:(NSArray *)categories cell:(ZSTECBCategoryCell *)cell;


@end

@implementation ZSTECBCategoriesViewController

@synthesize tableView;
@synthesize categories;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    colorArr = [[NSArray alloc]initWithObjects:RGBCOLOR(10 * 16 + 2, 14 * 16 + 5, 16 + 3),RGBCOLOR(14* 16 + 5, 16 + 3, 16 + 3),RGBCOLOR(14 * 16 + 5, 12 * 16, 16 + 3),RGBCOLOR(16+3, 16 * 14 + 5, 13 * 16 + 4), nil];
//    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"分类", nil)];
    
    
    UIView *headView = [[UIView alloc]init];
    headView.frame = CGRectMake(0, 0, 320, 40);
    headView.backgroundColor = RGBCOLOR(244, 244, 244);
    [self.view addSubview:headView];
    
    ZSTRoundRectView * round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(CGRectGetMinX(headView.frame) + 5, 5, 310, 30) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    round.backgroundColor = [UIColor whiteColor];
    [headView addSubview:round];
    [round release];
    
    UITextField * searchField = [[UITextField alloc] initWithFrame:CGRectMake(30, (round.size.height - 20)/2, 310 - 30, 20)];
    searchField.borderStyle = UITextBorderStyleNone;
    searchField.returnKeyType = UIReturnKeySearch;
    searchField.font = [UIFont systemFontOfSize:14];
    searchField.delegate = self;
    searchField.clearButtonMode = UITextFieldViewModeAlways;
    searchField.placeholder = @"请在此输入搜索内容";
    [round addSubview:searchField];
    [searchField release];
    
    UIImageView * searchImg = [[UIImageView alloc]initWithImage:ZSTModuleImage(@"module_ecomb_goolssearch_search.png")];
    searchImg.frame = CGRectMake((30 - 14)/2, (round.size.height - 15)/2, 15, 15);
    [round addSubview:searchImg];
    [searchImg release];
    
    
    UIControl * searchControl = [[UIControl alloc]initWithFrame:headView.frame];
    [searchControl addTarget:self action:@selector(goSearchAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:searchControl];
    
    
    //、、 - (IS_IOS_7?64:44)
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headView.frame), 320, self.view.bounds.size.height - 40) style:UITableViewStylePlain] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight; 
    self.tableView.separatorStyle =  UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.tableView];

    [headView release];
    
    [self addObserver:self forKeyPath:@"categories" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        //获取分类
        if (self.categories == nil) {
            [self.engine getProductBCategory];
        }
    }];
}
-(void)goSearchAction
{
    ZSTEComBGoodsSearchViewController * searchViewController = [[ZSTEComBGoodsSearchViewController alloc]init];
    searchViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchViewController animated:NO];
    [searchViewController release];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}
- (NSArray *)indexPathsForSection:(NSUInteger)section rowIndexSet:(NSIndexSet *)indexSet
{
    NSMutableArray *    indexPaths;
    NSUInteger          currentIndex;
    
    assert(indexSet != nil);
    
    indexPaths = [NSMutableArray array];
    assert(indexPaths != nil);
    currentIndex = [indexSet firstIndex];
    while (currentIndex != NSNotFound) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:currentIndex inSection:section]];
        currentIndex = [indexSet indexGreaterThanIndex:currentIndex];
    }
    return indexPaths;
}

- (void)openFolderForCategories:(NSArray *)children cell:(ZSTECBCategoryCell *)cell{
    
    CGFloat height;
    NSInteger maxRowCount;
    CGFloat rowHeight;
    
    NSUInteger rowCount = [children count]/3 + (([children count]%3)? 1 : 0);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"showImgKey"]) {
        maxRowCount = 3;
        rowHeight = 62;
    } else {
        maxRowCount = 6;
        rowHeight = 30;
    }
    
    if (rowCount > maxRowCount) {
        rowCount = maxRowCount;
    }
    
    if (rowCount <= 1) {
        height = 10*2 + rowHeight;
    } else if (rowCount <= 2) {
        height = 10*3 + rowHeight*2;
    } else {
        height = 10*4 + rowHeight*3;
    }
    
    ZSTECBCategoryFolderView *categoryFolderView = [[ZSTECBCategoryFolderView alloc] initWithFrame:CGRectMake(0, 0, 320, height)];
    categoryFolderView.categories = children;
    [self.tableView openFollowWithContentView:categoryFolderView
                                    openBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction) {
                                                                     }
                                   closeBlock:^(UIView *contentView, CFTimeInterval duration, CAMediaTimingFunction *timingFunction) {
                                       
                                       cell.checked = !cell.checked;
                                       [cell setChecked:cell.checked];
                                   }
                              completionBlock:^ {
                                }
     ];
    [categoryFolderView release];
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    NSIndexSet *indexes = [change objectForKey:NSKeyValueChangeIndexesKey];
    if ([keyPath isEqualToString:@"categories"]) {
        switch ( [[change objectForKey:NSKeyValueChangeKindKey] intValue]) {
            default:
                assert(NO);
            case NSKeyValueChangeSetting: {
                [self.tableView reloadData];
            } break;
            case NSKeyValueChangeInsertion: {
                [self.tableView insertRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView flashScrollIndicators];
            } break;
            case NSKeyValueChangeRemoval: {
                [self.tableView deleteRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
                [self.tableView flashScrollIndicators];
            } break;
            case NSKeyValueChangeReplacement: {
                [self.tableView reloadRowsAtIndexPaths:[self indexPathsForSection:1 rowIndexSet:indexes] withRowAnimation:UITableViewRowAnimationNone];
            } break;
        }
    } else if ([keyPath isEqualToString:@"carouseles"]) {
        [self.tableView reloadData];
    }
}

#pragma mark -
#pragma mark ---Table Data Source Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if (self.categories)
    {
        return 1;
    }else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.categories count];
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString *reuseIdentifier  = @"ZSTECBCategoryCell";
        ZSTECBCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
        if (cell == nil) {
            cell = [[[ZSTECBCategoryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier] autorelease];
        }
        if(([theTableView numberOfRowsInSection:indexPath.section]-1) == 0){
            cell.position = CustomCellPositionSingle;
        }
        else if(indexPath.row == 0){
            cell.position = CustomCellPositionNone;
        }
        else if (indexPath.row == ([theTableView numberOfRowsInSection:indexPath.section]-1)){
            cell.position  = CustomCellPositionBottom;
        }
        else{
            cell.position = CustomCellPositionMiddle;
        }
        if ([self.categories count]) {
            NSDictionary *category = [self.categories objectAtIndex:indexPath.row];
            [cell configCell:category forRowAtIndex:indexPath];
        }
    
        UIView * view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 5, 40)];
        view.backgroundColor = [colorArr objectAtIndex:indexPath.row%4];
        [cell.contentView addSubview:view];
    
//        cell.backgroundColor =[UIColor yellowColor];
        cell.contentView.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // cell.selectedBackgroundView = [[UIView alloc]initWithFrame:cell.bounds];
        // cell.selectedBackgroundView.backgroundColor = [self stringToColor:@"#f3f3f3"];
        return cell;
}

-(UIColor *)stringToColor:(NSString *)colorStr
{
    int a[6] = {},i = 0;
    char * cchar = (char *)[colorStr cStringUsingEncoding:NSASCIIStringEncoding];
    
    char * p = cchar;
    
    while (*p != '\0') {
        if (*p == '#') {
            
        }
        else if (*p >= 'a' && *p<='z'){
            a[i++] = 10 + *p - 'a';
        }
        else if(*p >= 'A' && *p<='Z')
        {
            a[i++] = 10 + *p - 'A';
        }
        else if (*p >= '0' && *p<='9') {
            a[i++] = *p - '0';
        }
        else{
            a[i++] = 0;
        }
        p++;
    }
    
    UIColor * color = RGBCOLOR(a[0] * 16 + a[1], a[2] * 16 + a[3], a[4]*16 + a[5]);
    return color;
}
#pragma mark - ZSTF3EngineECADelegate
- (void)getProductCategoryDidSucceed:(NSArray *)array
{
    self.categories = array;
    [self.view removeNetworkIndicatorView];
    if ([self.categories count] == 0) {
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"提示", @"")
                              message:NSLocalizedString(@"暂无数据", nil)
                              delegate:nil
                              cancelButtonTitle:NSLocalizedString(@"确定", @"")
                              otherButtonTitles:nil,nil];
        [alert show];
        [alert release];
    }
}

- (void)getProductCategoryDidFailed:(int)resultCode
{
    [self.view refreshFailed];
}



#pragma mark ---点击某行触发的方法
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    ZSTECBCategoryCell* cell = (ZSTECBCategoryCell*)[self.tableView cellForRowAtIndexPath:indexPath];
    
    if (indexPath.section == 1 || [self.categories count]) {
        NSDictionary *category = [self.categories objectAtIndex:indexPath.row];
        NSArray *children = [self.dao getChildrenBOfCategory:[[category safeObjectForKey:@"categoryid"] integerValue]];
        if ([children count] > 0) {
            cell.checked = !cell.checked;
            [cell setChecked:cell.checked];
            [self openFolderForCategories:children cell:cell];
            
        } else {
            ZSTEcomBCategoriesShowViewController *controller = [[ZSTEcomBCategoriesShowViewController alloc] init];
            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString([[self.categories objectAtIndex:indexPath.row] safeObjectForKey:@"categoryname"], nil)];
            controller.hidesBottomBarWhenPushed = YES;
            controller.categoryid = [[self.categories objectAtIndex:indexPath.row] safeObjectForKey:@"categoryid"];
            [self.navigationController pushViewController:controller animated:YES];
            [controller release];
        }
    } else {
        
    }
}


#pragma mark ---UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"categories" context:nil];
    self.tableView = nil;
    self.categories = nil;
    [super dealloc];
}


@end
