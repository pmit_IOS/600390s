//
//  ZSTEComBOrderInfoViewController.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTEComBOrderInfoViewControllerAddressCell.h"
#import "ZSTEComBOrderInfoViewControllerPayCell.h"
#import "ZSTEComBOrderInfoViewControllerInformationCell.h"
#import "ZSTEComBOrderInfoViewControllerResponseCell.h"
#import "ZSTWebViewController.h"
#import "ZSTECBPopOver.h"

typedef enum
{
    typezhifubao = 1,
    typehuodao,
    typetotal,
    typeyinlian
}PAYTYPE;

//订单状态，0：未支付；1：支付成功；2：支付失败; 3:已发货;4:已完成
typedef enum
{
    stateweizhifu = 0,
    statezhifuchenggong,
    statezhifushibai,
    stateyifahuo,
    stateyiwancheng
}STATE;
/*
 
 -(NSString *)getStateNameWithOrderStatu:(int)statu
 {
 if (statu == 0) {
 return @"待付款";
 }
 else if(statu == 1)
 {
 return @"待发货";
 }
 else if(statu == 2)
 {
 return @"支付失败";
 }
 else if(statu == 3)
 {
 return @"已发货";
 }
 return @"错误状态";
 }

 */
@interface ZSTEComBOrderInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTEComBOrderInfoViewControllerAddressCellDelegate,ZSTF3EngineECBDelegate,ZSTEComBOrderInfoViewControllerResponseCellDelegate,ZSTEComBOrderInfoViewControllerPayCellDelegate,ZSTWebViewControllerDelegate,ECBPopOverDelegate>
{
    EcomBAddress * _address;
    NSArray * _orderGools;
    float  _totalAmount;
    int _totalFreight;
    payWay _payWay;
    BOOL _hideSubmit;
}
@property (retain,nonatomic) UITableView * tableView;
@property (retain,nonatomic) NSString * orderID;
-(void) hideSubmit:(BOOL) hide;
@end
