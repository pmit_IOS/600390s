//
//  ZSTEComBAskOnLineViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-5.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBAskOnLineViewController.h"

@interface ZSTEComBAskOnLineViewController ()

@end

@implementation ZSTEComBAskOnLineViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
//     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"在线咨询", nil)];
    
    CGRect rect = self.view.bounds;
    rect.size.height -= (IS_IOS_7)?64 + 48:48;
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];

    
    _footView = [[UIView alloc]init];
    _footView.frame = CGRectMake(0, 480 - 48 + (iPhone5?88:0) - 20 - 44 , 320, 48);
    _footView.backgroundColor = RGBCOLOR(244, 244, 244);
    [self.view addSubview:_footView];
    [_footView release];
    
    ZSTRoundRectView * round = [[ZSTRoundRectView alloc]initWithFrame:CGRectMake(CGRectGetMinX(_footView.frame) + 5,  4 + 3, 310 - 5 - 69, 34) radius:4.0 borderWidth:1.0f borderColor:RGBCOLOR(239, 239, 239)];
    round.backgroundColor = [UIColor whiteColor];
    [_footView addSubview:round];
    [round release];
    
    _searchField = [[UITextField alloc] initWithFrame:CGRectMake(1, 1 , round.frame.size.width -2, round.frame.size.height - 2)];
    _searchField.borderStyle = UITextBorderStyleNone;
    _searchField.delegate = self;
    _searchField.clearButtonMode = UITextFieldViewModeAlways;
    _searchField.font = [UIFont systemFontOfSize:14];
//    _searchField.backgroundColor = [UIColor yellowColor];
    _searchField.placeholder = @"   请在此输入您的问题...";
    [round addSubview:_searchField];
    [_searchField release];
    
    
    UIButton * buyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [buyBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_send_green.png") forState:UIControlStateNormal];
    buyBtn.frame = CGRectMake(320 - 5 - 69,  round.frame.origin.y , 69, 34);
    [buyBtn setTitle:@"发送" forState:UIControlStateNormal];
    buyBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [buyBtn addTarget:self action:@selector(sendAction:) forControlEvents:UIControlEventTouchUpInside];
    [_footView addSubview:buyBtn];

    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    [self.engine getAskOnLineData:_productid];
}
-(void)getEcomBAskDidFailed:(int)resultCode
{
    [TKUIUtil alertInWindow:@"获取聊天内容失败" withImage:nil];
}
-(void)getEcomBAskDidSucceed:(NSArray *)arr
{
    NSLog(@"获取聊天内容成功");
    if (arr.count) {
        _dataArray  = [EcomBAskInfo EcomBAskWithArray:arr];
        [_tableView reloadData];
    }
    [TKUIUtil alertInWindow:@"获取聊天内容成功" withImage:nil];
}
- (void)sendAction:(UIButton *) btn
{
    if (_searchField.text.length == 0) {
        return;
    }
    [self.engine sendAskOnLineData:_searchField.text productid:_productid];
    [_searchField resignFirstResponder];
}
-(void)sendEcomBAskDidFailed:(int)resultCode
{
    NSLog(@"发送聊天内容失败");
}
-(void)sendEcomBAskDidSucceed:(NSArray *)arr
{
    NSLog(@"发送聊天内容成功");
    if (arr.count) {
        _dataArray  = [EcomBAskInfo EcomBAskWithArray:arr];
        [_tableView reloadData];
    }
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:@"您的问题已提交成功，请等待管理员处理"
                          message:nil
                          delegate:self
                          cancelButtonTitle:@"取消"
                          otherButtonTitles:@"确认"
                          ,nil];
    alert.tag = 111;
    [alert show];
    
}
-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    _tableView.frame = CGRectMake(0, 0, 320, self.view.frame.size.height - keyboardRect.size.height);
    _footView.frame = CGRectMake(0, 480 - 48 + (iPhone5?88:0) - 20 - 44 - keyboardRect.size.height, 320, 48);
    
}


- (void)keyboardWillHide:(NSNotification *)notification {
    
    CGRect rect = self.view.bounds;
    rect.size.height -= (IS_IOS_7)?0  + 48:48;
    _tableView.frame = rect;
    _footView.frame = CGRectMake(0, 480 - 48 + (iPhone5?88:0) - 20 - 44 , 320, 48);
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
     
}
#pragma mark tableDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _dataArray.count;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    EcomBAskInfo * info = [_dataArray objectAtIndex:indexPath.row];
    CGSize  askSize = [info.EcomBAskInfoQuestion sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(320, 500) lineBreakMode:NSLineBreakByTruncatingMiddle];
    CGSize  answerSize = [info.EcomBAskInfoAnswer sizeWithFont:[UIFont systemFontOfSize:16] constrainedToSize:CGSizeMake(320, 500) lineBreakMode:NSLineBreakByTruncatingMiddle];
    return askSize.height + answerSize.height + 50;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        static NSString * cellCarouse = @"carousecell";
        ZSTEComBAskOnLineTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellCarouse];
        if (!cell) {
            cell = [[ZSTEComBAskOnLineTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellCarouse];
        }
//        if (indexPath.row % 2 == 0) {
//            cell.contentView.backgroundColor = RGBCOLOR(243, 243, 243);
//        }
//        else
//        {
//            cell.contentView.backgroundColor = [UIColor clearColor];
//        }
        [cell reloadData:[_dataArray objectAtIndex:indexPath.row]];
        return  cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 0.0f;
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
