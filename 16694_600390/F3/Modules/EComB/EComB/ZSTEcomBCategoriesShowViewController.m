//
//  ZSTEcomBCategoriesShowViewController.m
//  EComB
//
//  Created by zhangwanqiang on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEcomBCategoriesShowViewController.h"
#import "ZSTEComBGoodsShowViewController.h"
#import "ZSTGoodsShowViewController.h"

@interface ZSTEcomBCategoriesShowViewController ()

@end

@implementation ZSTEcomBCategoriesShowViewController
{
    //add
    BOOL _hasMore;
    BOOL _isLoadMore;
    TKLoadMoreView * _loadMoreView;
    NSInteger _curruntPage;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //add
    _hasMore = YES;
    _isLoadMore = NO;
    _curruntPage = 1;
    
    //self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];

    CGRect rect = self.view.bounds;
    rect.size.height -= (IS_IOS_7?64:44);
    
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = NO;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    _resultData = [[NSMutableArray alloc] init];
    
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)];
        _loadMoreView.backgroundColor = [UIColor clearColor];
        _loadMoreView.delegate = self;
    }
    [TKUIUtil showHUD:self.view withText:@"正在加载"];
    
    [self.engine getCategoriesShowData:_categoryid pageIndex:_curruntPage];
    _isLoadMore = YES;
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}
-(void)getCategoriesShowDidFailed:(int)resultCode
{
//    [TKUIUtil alertInWindow:@"没有数据了~" withImage:nil];
    _isLoadMore = NO;
    _hasMore = NO;
    [self finishLoading];
    [_tableView reloadData];
    NSLog(@"获取失败");
}
-(void)getCategoriesShowDidSucceed:(NSArray *)arr hasMore:(BOOL)hasMore
{
//    _resultData = [EComBHomePageInfo homePageWithArray:arr];
    _isLoadMore = NO;
//    [_tableView reloadData];
    [self searchEComBListDidSucceed:[EComBHomePageInfo homePageWithArray:arr] isLoadMore:NO hasMore:hasMore isFinish:YES];
    NSLog(@"分类选择页获取成功 categoryid = %@",_categoryid);
}
//add
- (void)searchEComBListDidSucceed:(NSArray *)resultList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    _hasMore = hasMore;
    [self finishLoading];
    //    [self displayStyle];
    [self appendData:resultList];
}
- (void)appendData:(NSArray*)dataArray
{
    [self.resultData addObjectsFromArray:dataArray];
    [self.tableView reloadData];
    _curruntPage ++;
}
- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:_tableView state:(_hasMore?TKLoadMoreViewStateNormal:TKLoadMoreViewStateFinish)];
    _isLoadMore = NO;
    [TKUIUtil hiddenHUD];
}
- (void)loadMoreData
{
    [TKUIUtil showHUD:self.view withText:@"正在加载"];
    [self.engine getCategoriesShowData:_categoryid pageIndex:_curruntPage];
}
-(void)loadMoreDidTriggerRefresh:(TKLoadMoreView *)view
{
    _isLoadMore = YES;
    [self.engine getCategoriesShowData:_categoryid pageIndex:_curruntPage];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKLoadMoreView *loadMoreView = (TKLoadMoreView *)[cell descendantOrSelfWithClass:[TKLoadMoreView class]];
    if (loadMoreView) {
        if (!_isLoadMore && _hasMore) {
            [_loadMoreView loadMoreTriggerRefresh];
        }
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (indexPath.section == 0) {
//        return 160.0f;
//    }
//    else
    if(indexPath.row == (_resultData.count + 1)/2)
    {
        if (_hasMore) {
            return 44.0f;
        }
        return 13.01f;
    }
    else
        return 210.0f;
    
}


-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return YES;
}
#pragma mark tableDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (_resultData.count +1)/2 + (_hasMore ? 1:1);
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 210.f;
//}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString * ECBProductTableViewCell = @"ECBProductTableViewCell";
    ZSTECBProductTableImageCell * cell = [tableView dequeueReusableCellWithIdentifier:ECBProductTableViewCell];
   

    if (!cell) {
        cell = [[ZSTECBProductTableImageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ECBProductTableViewCell];
    }
    
    if (_resultData.count) {
        
        if (indexPath.row == ([_resultData count] +1)/2 && [_resultData count] > 0) {
            static NSString *newsCellIdentifier = @"loadMoreView";
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
            
            if (cell==nil) {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] ;
                cell.accessoryType = UITableViewCellAccessoryNone;
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.userInteractionEnabled = NO;
                [cell.contentView addSubview:_loadMoreView];
            }
            return cell;
        }
        else if ((_resultData.count - indexPath.row  * 2) > 1) {
            [cell reloadDataWithLeft:[_resultData objectAtIndex:(indexPath.row * 2)]  Right:[_resultData objectAtIndex:(indexPath.row * 2 +1)]];
        }
        else
        {
            [cell reloadDataWithLeft:[_resultData objectAtIndex:(indexPath.row * 2)]  Right:nil];
        }
    }
    cell.delegate = self;
    //    cell.textLabel.text = [NSString stringWithFormat:@"第%d",indexPath.row];
    cell.leftView.tag = indexPath.row * 2;
    cell.rightView.tag = indexPath.row * 2 + 1;
    return  cell;
}
-(void)ZSTECBProductTableImageCellDidSelect:(NSInteger)row
{
    NSLog(@"搜索页 %ld ",(long)row);
//    ZSTEComBGoodsShowViewController * showViewController = [[ZSTEComBGoodsShowViewController alloc]init];
    ZSTGoodsShowViewController *showViewController = [[ZSTGoodsShowViewController alloc] init];
    showViewController.pageInfo = [_resultData objectAtIndex:row];
    showViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:showViewController animated:YES];
    [showViewController release];
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}
//-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
//{
//    return _headView;
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//-(void)dealloc
//{
//    [_headView release];
//    [_tableView release];
//    [super dealloc];
//}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView *)view
{
    return NO;
}

@end
