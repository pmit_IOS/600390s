//
//  STGlobal+EComB.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZSTGlobal+EComB.h"

@interface ZSTDao(EComB)

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)createTableIfNotExistForECBModule;


/**
 *	@brief 添加分类
 *
 *	@param 	categoryID      分类ID
 *	@param 	categoryName 	分类名称
 *	@param 	parentID        父分类ID
 *	@param 	orderNum        排序字段
 *  @param  description     描述
 *  @param  iconUrl         分类图标
 *	@return	操作是否成功
 */
- (BOOL)addecbCategory:(NSInteger)categoryID
          categoryName:(NSString *)categoryName
              parentID:(NSInteger)parentID
              orderNum:(NSInteger)orderNum
           description:(NSString *)description;

/**
 *	@brief 删除分类
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllProductBCategories;

/**
 *	@param 	parentID      父分类ID，为0时取一级分类
 *	@brief  获取子分类
 *	@return	子分类
 */
- (NSArray *)getChildrenBOfCategory:(NSInteger)parentID;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

@end
