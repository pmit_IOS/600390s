//
//  ZSTEComBMyOrderImageCareouseView.h
//  EComB
//
//  Created by zhangwanqiang on 14-3-13.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"

@protocol ZSTEComBMyOrderImageCareouseViewDataSouce;
@protocol ZSTEComBMyOrderImageCareouseViewDelegate;


@interface ZSTEComBMyOrderImageCareouseView : UIView <TKAsynImageViewDelegate,UIScrollViewDelegate>
@property (nonatomic,assign) id <ZSTEComBMyOrderImageCareouseViewDataSouce> careouseDataSouce;
@property (nonatomic,assign) id <ZSTEComBMyOrderImageCareouseViewDelegate> careouseDelegate;
@property (nonatomic,retain) UIScrollView * scrollView;
-(void)reloadData;
@end


@protocol ZSTEComBMyOrderImageCareouseViewDataSouce <NSObject>

-(NSString *)imageCareouse:(ZSTEComBMyOrderImageCareouseView *) careouseView imageAtIndex:(NSInteger) index;

-(NSInteger)imageCareouse:(ZSTEComBMyOrderImageCareouseView *) careouseView numberOfTotal:(NSInteger )number;

@end


@protocol ZSTEComBMyOrderImageCareouseViewDelegate <NSObject>

-(void)imageCareouse:(ZSTEComBMyOrderImageCareouseView *) careouseView didSelectIndex:(NSInteger) index;
@end;