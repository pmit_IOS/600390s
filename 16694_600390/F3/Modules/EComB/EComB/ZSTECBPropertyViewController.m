//
//  ZSTECBPropertyViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-3-5.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBPropertyViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ZSTECBPropertyViewController ()
{
    NSArray *colorArray;
    NSArray *sizeArray;
    
    NSInteger color_lasttag;
    NSInteger size_lasttag;
}

@end

@implementation ZSTECBPropertyViewController
{
    NSString * propertyID;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    colorArray = [[NSArray alloc] initWithObjects:@"红色",@"黄色",@"黑色",@"深灰色",@"紫色",@"白色",@"红色",@"黄色",@"黑色",@"深灰色",@"紫色",@"白色", @"白色",nil];
    sizeArray = [[NSArray alloc] initWithObjects:@"S",@"M",@"L",@"XL",@"XXL",@"XXXL", nil];
    
     self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"属性选择", nil)];
//	 self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    _scrollView = [[[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0)-20-44)] autorelease];
    _scrollView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:_scrollView];
    
    UIView *productView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 75)];
    productView.backgroundColor = [UIColor clearColor];
    [_scrollView addSubview:productView];
    
//    _productImg = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(20, 10, 55, 55)] autorelease];
//    _productImg.asynImageDelegate = self;
    
    _productImg = [[[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 55, 55)] autorelease];
    _productImg.userInteractionEnabled = NO;
    _productImg.backgroundColor = [UIColor clearColor];
    _productImg.layer.cornerRadius = 8.f;
    _productImg.clipsToBounds = YES;
    _productImg.layer.cornerRadius = 8.0f;
    _productImg.clipsToBounds = YES;
    [productView addSubview:_productImg];
    
    _productNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(85, 10, 200, 30)] autorelease];
    _productNameLabel.backgroundColor = [UIColor clearColor];
    _productNameLabel.font = [UIFont systemFontOfSize:15];
    [productView addSubview:_productNameLabel];
    
    UILabel *markLabel = [[UILabel alloc] initWithFrame:CGRectMake(85, 45, 20, 20)];
    markLabel.backgroundColor = [UIColor clearColor];
    markLabel.textColor = RGBCOLOR(202, 0, 3);
    markLabel.font = [UIFont systemFontOfSize:15];
    markLabel.text = @"￥";
    [productView addSubview:markLabel];
    [markLabel release];
    
    _priceLabel = [[[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(markLabel.frame), 43, 100, 20)] autorelease];
    _priceLabel.backgroundColor = [UIColor clearColor];
    _priceLabel.textColor = RGBCOLOR(214, 84, 88);
    _priceLabel.font = [UIFont systemFontOfSize:15];
    _priceLabel.text = @"1.00";
    [productView addSubview:_priceLabel];
    
//    _reductionbtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _reductionbtn.frame = CGRectMake(210, 40, 30, 30);
//    [_reductionbtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 10, 10)];
//    _reductionbtn.tag = 2013;
//    [_reductionbtn setImage:ZSTModuleImage(@"module_ecomb_btn_reduction.png") forState:UIControlStateNormal];
//    [_reductionbtn addTarget:self action:@selector(changeCountAction:) forControlEvents:UIControlEventTouchUpInside];
//    [productView addSubview:_reductionbtn];
//    
//    UIImageView *countbg = [[UIImageView alloc] initWithFrame:CGRectMake(237, 45, 35, 15)];
//    countbg.backgroundColor = [UIColor whiteColor];
//    countbg.layer.cornerRadius = 4.0f;
//    countbg.clipsToBounds = YES;
//    [countbg.layer setBorderWidth:1];
//    [countbg.layer setBorderColor:[[UIColor grayColor] CGColor]];
//    [productView addSubview:countbg];
//    [countbg release];
//    
//    _countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(237, 45, 35, 15)] autorelease];
//    _countLabel.backgroundColor = [UIColor clearColor];
//    _countLabel.font = [UIFont systemFontOfSize:10];
//    _countLabel.textAlignment = NSTextAlignmentCenter;
//    _countLabel.text = @"1";
//    [productView addSubview:_countLabel];
//    
//    _addbtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    _addbtn.frame = CGRectMake(274, 40, 30, 30);
//    [_addbtn setImageEdgeInsets:UIEdgeInsetsMake(5, 5, 10, 10)];
//    _addbtn.tag = 2014;
//    [_addbtn setImage:ZSTModuleImage(@"module_ecomb_btn_add.png") forState:UIControlStateNormal];
//    [_addbtn addTarget:self action:@selector(changeCountAction:) forControlEvents:UIControlEventTouchUpInside];
//    [productView addSubview:_addbtn];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 74, 320, 1)];
    line.backgroundColor = RGBCOLOR(239, 239, 239);
    [productView addSubview:line];
    [line release];
    [productView release];
    
    _colorView = [[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(productView.frame), 320, 125)] autorelease];
    _colorView.backgroundColor = RGBCOLOR(243, 243, 243);
    [_scrollView addSubview:_colorView];
    
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
    headView.backgroundColor = [UIColor whiteColor];
    [_colorView addSubview:headView];
    [headView release];
    
    _colortagLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 8, 50, 18)];
    _colortagLabel.backgroundColor = [UIColor clearColor];
    _colortagLabel.font = [UIFont systemFontOfSize:15];
    _colortagLabel.text = @"请选择:";
    [_colorView addSubview:_colortagLabel];
    [_colortagLabel release];
    
//    _colorLabel = [[[UILabel alloc] initWithFrame:CGRectMake(80, 8, 200, 18)] autorelease];
//    _colorLabel.backgroundColor = [UIColor clearColor];
//    _colorLabel.font = [UIFont systemFontOfSize:15];
//    [_colorView addSubview:_colorLabel];
    
//    _sizeView = [[[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_colorView.frame)+10, 320, 125)] autorelease];
//    _sizeView.backgroundColor = RGBCOLOR(243, 243, 243);
//   [_scrollView addSubview:_sizeView];
//    
//    UIView *sizeheadView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 35)];
//    sizeheadView.backgroundColor = [UIColor whiteColor];
//    [_sizeView addSubview:sizeheadView];
//    [sizeheadView release];
//    
//    UILabel *sizetagLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 8, 50, 18)];
//    sizetagLabel.backgroundColor = [UIColor clearColor];
//    sizetagLabel.font = [UIFont systemFontOfSize:15];
//    sizetagLabel.text = @"尺码:";
//    [_sizeView addSubview:sizetagLabel];
//    [sizetagLabel release];
//    
//    _sizeLabel = [[[UILabel alloc] initWithFrame:CGRectMake(60, 8, 200, 18)] autorelease];
//    _sizeLabel.backgroundColor = [UIColor clearColor];
//    _sizeLabel.font = [UIFont systemFontOfSize:15];
//    [_sizeView addSubview:_sizeLabel];
    
    _okbtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _okbtn.frame = CGRectMake(170, CGRectGetMaxY(_colorView.frame)+20, 140, 30);
    [_okbtn setTitle:@"选好了" forState:UIControlStateNormal];
    [_okbtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_red.png") forState:UIControlStateNormal];
    _okbtn.backgroundColor = RGBCOLOR(211, 3, 6);
    [_okbtn setTintColor:[UIColor whiteColor]];
    [_okbtn.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
    _okbtn.layer.cornerRadius = 3.0f;
    _okbtn.clipsToBounds = YES;
    [_okbtn setTintColor:[UIColor whiteColor]];
    [_okbtn addTarget:self action:@selector(okAction) forControlEvents:UIControlEventTouchUpInside];
    [_scrollView addSubview:_okbtn];
    
    //[self colorButtonElementsForView];
    //[self sizeButtonElementsForView];
    [self.engine getPropertyViewControllerData:_productid];
    self.view.backgroundColor = [UIColor whiteColor];
}

//获取属性页返回数据
-(void)getPropertyViewControllerDataDidSucceed:(NSDictionary *) dic
{
    _propertyInfo = [EcomBPropertyInfo ecomBPropertyInfoWithDictionary:dic];
    [self reloadData];
}
-(void)reloadData
{
    if (_propertyInfo.ecomBCarouseInfoStockcount > 0) {
        [self colorButtonElementsForView];
    }
    else
    {
        [TKUIUtil alertInWindow:@"库存不足" withImage:nil];
        _colortagLabel.hidden = YES;
        _colortagLabel.userInteractionEnabled = NO;
        
        _okbtn.hidden = YES;
        _okbtn.userInteractionEnabled = NO;
        _colorView.backgroundColor = [UIColor clearColor];
    }
//    _productImg.url = [NSURL URLWithString:_propertyInfo.ecomBPropertyInfoImageStr];
//    [_productImg loadImage];
    [_productImg setImageWithURL:[NSURL URLWithString:_propertyInfo.ecomBPropertyInfoImageStr]];
    _productNameLabel.text = _propertyInfo.ecomBPropertyInfoProductname;
    
    _priceLabel.text = [NSString stringWithFormat:@"%.2f",_propertyInfo.ecomBPropertyInfoSalesprice];
}
-(void)getPropertyViewControllerDataDidFailed:(int)resultCode
{
    NSLog(@"获取属性页面");
}
//加入购物车
-(void)putGoodsIntoCarDidFailed:(int)resultCode
{
    [TKUIUtil alertInWindow:@"加入失败" withImage:nil];
    NSLog(@"加入购购物车失败 ");
}
-(void)putGoodsIntoCarDidSucceed:(NSArray *)carouseles
{
//    [TKUIUtil alertInWindow:@"加入成功" withImage:nil];
    NSLog(@"加入购物车成功");
}
-(void)okAction
{
    if (propertyID.length>0) {
        [self.engine putGoodsIntoCar:_productid propertyId:propertyID];
    }
    else if(propertyID.length == 0 && _propertyInfo.ecomBPropertyInfoProperties.count == 0)
    {
        [self.engine putGoodsIntoCar:_productid propertyId:@""];
    }
    else
    {
        NSLog(@"忘记选择属性了！！！！！");
        [TKUIUtil alertInWindow:@"忘记选属性了" withImage:nil];
        return;
    }

    if (_push ==  PUSHTOBUYNOW) {
        ZSTECBShoppingCartViewController *controller = [[ZSTECBShoppingCartViewController alloc] init];
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    else if(_push == PUSHTOFRONPAGE)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
- (void)colorButtonElementsForView
{
     NSArray * _propertyArr = [_propertyInfo ecomBPropertyInfoProperties];
    if (_propertyArr.count == 0) {
        _colorView.backgroundColor = [UIColor clearColor];
        _colortagLabel.text = @"";
        return;
    }
    //NSInteger columnCount = 3;
    
    float horizontalSpacing = 14;
    float verticalSpacing = 20;
    NSInteger row = 0;
    NSInteger col = 0;
    float maxWidth = 320 - horizontalSpacing * 2;
    float minWidth = (maxWidth - 28 )/3;
    float widthPrevious = horizontalSpacing;
    
    
    for (NSInteger i = 0; i < [_propertyArr count]; i++) {
        NSString *btnName = [[_propertyArr objectAtIndex:i] ecomBPropertyProperty];
        int count = [[_propertyArr objectAtIndex:i] ecomBPropertyStockcount];
//        int row = i / columnCount;
//        int col = i % columnCount;
        float x =0;//= horizontalSpacing + (62 + horizontalSpacing)  *col;
        float y =0;//= 50 + (verticalSpacing + 25) * row;
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn setTitle:btnName forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12];
        btn.backgroundColor = [UIColor whiteColor];
        btn.tag = i + 2014;
        [btn setTitleColor:RGBCOLOR(152, 152, 152) forState:UIControlStateNormal];
        UIImage *image  = ZSTModuleImage(@"module_ecomb_property_btn_selected.png");
        CGFloat capWidth = image.size.width / 2;
        CGFloat capHeight = image.size.height / 2;
        UIImage* editImage = [image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
        [btn setBackgroundImage:editImage forState:UIControlStateDisabled];
        btn.layer.borderWidth = 1.0f;
        btn.layer.borderColor = RGBCOLOR(239, 239, 239).CGColor;
        if (count>0) {
             [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            btn.alpha = 0.4;
        }
        btn.layer.cornerRadius = 4.0f;
        btn.clipsToBounds = YES;
        [_colorView addSubview:btn];
        
        CGSize  size = [btnName sizeWithFont:btn.titleLabel.font constrainedToSize:CGSizeMake(maxWidth, 50) lineBreakMode:NSLineBreakByWordWrapping];
        if (size.width < minWidth) {
            size.width = minWidth;
        }
        else if(size.width > maxWidth){
            size.width = maxWidth;
        }
        if (row == 0 && col == 0) {
            x = widthPrevious;
            y = 50 + (verticalSpacing + 25) * row;
            col++;
            widthPrevious += size.width + horizontalSpacing;
        }
        else{
            if (widthPrevious + size.width > maxWidth + horizontalSpacing) {
                row ++;
                col = 0;
                widthPrevious = horizontalSpacing;
                x = widthPrevious;
                y = 50 + (verticalSpacing + 25) * row;
                widthPrevious = x + size.width + horizontalSpacing;
                col ++;
            }
            else
            {
                x = widthPrevious;
                y = 50 + (verticalSpacing + 25) * row;
                col ++;
                widthPrevious += size.width + horizontalSpacing;
            }
        }
       
//        float x = horizontalSpacing + (62 + horizontalSpacing)  *col;
//        float y = 50 + (verticalSpacing + 25) * row;
        
        btn.frame = CGRectMake(x, y, size.width, 25);
    }
    
//    int colorrow = (int)[_propertyArr count];
    CGRect frame = _colorView.frame;
    frame.size.height = (row +1) * 50 + 35;
    _colorView.frame = frame;
//    _colorView.backgroundColor = [UIColor redColor];
    
   // _sizeView.frame = CGRectMake(0, CGRectGetMaxY(_colorView.frame)+10, 320, 125);
    _okbtn.frame = CGRectMake(170, CGRectGetMaxY(_colorView.frame)+20, 140, 30);

    _scrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(_okbtn.frame)+10);
}

//- (void)sizeButtonElementsForView
//{
//    NSInteger columnCount = 4;
//    
//    float horizontalSpacing = 14;
//    float verticalSpacing = 20;
//    
//    for (NSInteger i = 0; i < [sizeArray count]; i++) {
//        
//        NSString *btnName = [sizeArray objectAtIndex:i];
//        
//        int row = i / columnCount;
//        int col = i % columnCount;
//        
//        float x = horizontalSpacing + (62 + horizontalSpacing)  *col;
//        float y = 50 + (verticalSpacing + 25) * row;
//        
//        UIButton *btn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
//        [btn setTitle:btnName forState:UIControlStateNormal];
//        btn.tag = i + colorArray.count + 2014;
//        [btn setTitleColor:RGBCOLOR(152, 152, 152) forState:UIControlStateNormal];
//         [btn setBackgroundImage:ZSTModuleImage(@"module_ecomb_property_btn_selected.png") forState:UIControlStateDisabled];
//        btn.backgroundColor = [UIColor whiteColor];
//        [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
//        btn.layer.cornerRadius = 6.0f;
//        btn.clipsToBounds = YES;
//        btn.frame = CGRectMake(x, y, 62, 25);
//        [_sizeView addSubview:btn];
//    }
//    
//    int colorrow = (sizeArray.count + 3) / 4;
//    CGRect frame = _sizeView.frame;
//    frame.size.height = colorrow * 50 + 35;
//    _sizeView.frame = frame;
//    
//    _okbtn.frame = CGRectMake(170, CGRectGetMaxY(_sizeView.frame)+20, 140, 30);
//    _scrollView.contentSize = CGSizeMake(320, CGRectGetMaxY(_okbtn.frame)+10);
//}

- (void) clickAction:(id)sender
{
    UIButton *btn = nil;
    UIButton *button = (UIButton *)sender;
    if ((button.tag - 2014) < colorArray.count) {
       
        btn = (UIButton *)[_colorView viewWithTag:color_lasttag];
        btn.enabled = YES;
        btn.layer.borderWidth = 1.0f;
        btn.layer.borderColor = RGBCOLOR(239, 239, 239).CGColor;
        
        //_colorLabel.text = [colorArray objectAtIndex:(button.tag-2014)];
        propertyID = [[[_propertyInfo ecomBPropertyInfoProperties] objectAtIndex:(button.tag-2014)] ecomBPropertyPropertyid];
        btn = (UIButton *)[_colorView viewWithTag:button.tag];
        btn.enabled = NO;
        
        btn.layer.borderWidth = 1.0f;
        btn.layer.borderColor = [UIColor clearColor].CGColor;
        
        color_lasttag = (int)button.tag;
       
    } else {

        btn = (UIButton *)[_sizeView viewWithTag:size_lasttag];
        btn.enabled = YES;
        _sizeLabel.text = [sizeArray objectAtIndex:(button.tag-colorArray.count-2014)];
        btn = (UIButton *)[_sizeView viewWithTag:button.tag];
        btn.enabled = NO;
        
        size_lasttag = (int)button.tag;

    }
}

- (void)changeCountAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    int count = (int)[_countLabel.text integerValue];
    if (button.tag == 2013) {
        count --;
        
    } else if (button.tag == 2014) {
        
        _reductionbtn.enabled = YES;
        count ++;
    }
    
    if (count <= 1) {
        count = 1;
        _reductionbtn.enabled = NO;
    }
    
    _countLabel.text = [NSString stringWithFormat:@"%d",count];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [super dealloc];
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    
}

@end
