//
//  ZSTECBShoppingCartCell.h
//  EComB
//
//  Created by LiZhenQu on 14-3-4.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTECBShoppingCartCell;
@protocol ZSTECBShoppingCartDelegate <NSObject>

- (void) shoppingCartCell:(ZSTECBShoppingCartCell *)tableViewCell price:(float)value;
@end

@interface ZSTECBShoppingCartCell : UITableViewCell<TKAsynImageViewDelegate>
{
    EcomBShopingCarInfo * _shopCarInfo;
@private
    //是否选中状态的图标
    PMRepairImageView *_imgSelectionMark;
    //是否选中状态的变量
    BOOL        _isSelected;
}

//@property (nonatomic, retain) TKAsynImageView *productImgView;
@property (nonatomic,retain) UIImageView *productImgView;
@property (nonatomic, retain) UILabel *productNameLabel;
@property (nonatomic, retain) UILabel *priceLabel;
@property (nonatomic, retain) PMRepairButton *reductionbtn;
@property (nonatomic, retain) PMRepairButton *addbtn;
@property (nonatomic, retain) UILabel *countLabel;
@property (nonatomic, assign) BOOL checked;
@property (nonatomic, retain) UIButton *selectBtn;

@property (retain, nonatomic) UILabel *pointLabel;

@property (nonatomic, assign) id<ZSTECBShoppingCartDelegate>delegate;

- (void)setChecked:(BOOL)checked;
- (void)loadData:(EcomBShopingCarInfo *) shopCarInfo isAdd:(BOOL) isAdd;

@end
