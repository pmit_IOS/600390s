//
//  ZSTEComBPaymentViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-6-10.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTEComBPaymentViewController : UIViewController

- (void)setURL:(NSString *)url;

@end
