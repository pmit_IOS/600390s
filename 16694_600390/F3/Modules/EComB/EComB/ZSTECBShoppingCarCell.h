//
//  ZSTECBShoppingCarCell.h
//  EComB
//
//  Created by pmit on 15/7/17.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@class ZSTECBShoppingCarCell;

@protocol ZSTECBShoppingCarDelegate <NSObject>

- (void) shoppingCartCell:(ZSTECBShoppingCarCell *)tableViewCell price:(float)value;

@end

@interface ZSTECBShoppingCarCell : UITableViewCell
{
    EcomBShopingCarInfo * _shopCarInfo;
    PMRepairImageView *_imgSelectionMark;
    BOOL _isSelected;
}

@property (retain,nonatomic) UIImageView *productImgView;
@property (retain,nonatomic) UILabel *productNameLabel;
@property (retain,nonatomic) UILabel *priceLabel;
@property (retain,nonatomic) PMRepairButton *reductionbtn;
@property (retain,nonatomic) PMRepairButton *addbtn;
@property (retain,nonatomic) UILabel *countLabel;
@property (assign,nonatomic) BOOL checked;
@property (retain,nonatomic) UIButton *selectBtn;
@property (retain,nonatomic) UILabel *pointLabel;
@property (assign,nonatomic) id<ZSTECBShoppingCarDelegate> delegate;

- (void)setChecked:(BOOL)checked;
- (void)loadData:(EcomBShopingCarInfo *) shopCarInfo isAdd:(BOOL) isAdd;
- (void)createUI;

@end
