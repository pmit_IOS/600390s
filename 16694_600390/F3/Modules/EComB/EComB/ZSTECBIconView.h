//
//  ZSTECBIconView.h
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECBIconView : UIButton<TKAsynImageViewDelegate>

@property (nonatomic, retain) UILabel *titleLabel;
@property (nonatomic, retain) TKAsynImageView *iconView;
@property (nonatomic, retain) NSNumber *category;

//设置信息简介
- (void)setTitle:(NSString *)title;

//设置分类id
- (void)setCategoryID:(NSNumber *)number;


@end
