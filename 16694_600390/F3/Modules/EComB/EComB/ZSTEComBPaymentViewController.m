//
//  ZSTEComBPaymentViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-6-10.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTEComBPaymentViewController.h"
#import "ZSTECBProductTableViewController.h"

@interface ZSTEComBPaymentViewController ()<UIWebViewDelegate>
{
    UIWebView *_webView;
}

@property (nonatomic, retain)  NSString *url;

@end

@implementation ZSTEComBPaymentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0))];
    _webView.backgroundColor = [UIColor clearColor];
    [_webView setDelegate:self];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_webView];
    
    if (_url) {
        [self setURL:_url];
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [_webView stopLoading];
    [_webView setDelegate:nil];
    [TKUIUtil hiddenHUD];
}

- (void)setURL:(NSString *)url
{
    url = [self normalizeURL:url];
    [url retain];
    [_url release];
    _url = url;
    
    if (_webView) {
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
        [_webView loadRequest:request];
    }
}

- (NSString *)normalizeURL:(NSString *)url
{
    if ([url length] != 0 && [url rangeOfString:@"://"].location == NSNotFound) {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    return url;
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUD:self.view];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.titleView = [self titleViewWithTitle:title];

    [TKUIUtil hiddenHUD];
}

- (UIView *)titleViewWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(62, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.font = [UIFont boldSystemFontOfSize:20];
    label.lineBreakMode = UILineBreakModeMiddleTruncation;
    label.textColor = [ZSTUtils getNavigationTextColor];
    return[label autorelease];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [[[request URL] absoluteString] lowercaseString];
    
    if ([urlString rangeOfString:@"http://mod.pmit.cn/ecomb/default.html?"].location != NSNotFound) {
        
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            if ([controller isKindOfClass:[ZSTECBProductTableViewController class]]) {
                
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_webView release];
    [_webView setDelegate:nil];
    [super dealloc];
}

@end
