//
//  ZSTGoodsShowViewController.m
//  EComB
//
//  Created by pmit on 15/7/14.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import "ZSTGoodsShowViewController.h"
#import "ZSTEComBAskOnLineTableViewCell.h"
#import "ZSTECBShoppingCartViewController.h"
#import "ZSTWebViewController.h"
#import "ZSTF3Engine+EComB.h"
#import <SDKExport/WXApi.h>
#import "ZSTUtils.h"
#import <MessageUI/MessageUI.h>
#import "ZSTHHSinaShareController.h"
#import "BaseNavgationController.h"
#import "JHRefreshBaseView.h"
#import "JHRefresh.h"
#import "ZSTEComBAskOnLineViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <ZSTShareView.h>
#import <ZSTNewShareView.h>

#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion] floatValue]
#define SCREEN_BOUNDS [[UIScreen mainScreen] bounds]
#define WIDTH SCREEN_BOUNDS.size.width
#define HEIGHT SCREEN_BOUNDS.size.height

#define NotificationName_WXShareSucceed @"WX_share_succeed"
#define NotificationName_WXShareFaild   @"WX_share_faild"

@interface ZSTGoodsShowViewController () <UITableViewDataSource,UITableViewDelegate,UIActionSheetDelegate,ZSTHHSinaShareControllerDelegate,MFMessageComposeViewControllerDelegate,UIWebViewDelegate, ZSTShareViewDelegate,ZSTNewShareViewDelegate>

@property (retain,nonatomic) UITableView *goodsDetailTableView;
@property (retain,nonatomic) UIView *detailView;
@property (retain,nonatomic) NSArray *sectionTitleArr;
@property (retain,nonatomic) NSDictionary *specDic;
@property (retain,nonatomic) UIView *darkView;
@property (retain,nonatomic) UILabel *specPriceLB;
@property (retain,nonatomic) UILabel *specLB;
@property (retain,nonatomic) UIImageView *specProductIV;
@property (retain,nonatomic) UILabel *specCountLB;
@property (retain,nonatomic) UIView *specView;
@property (retain,nonatomic) UIScrollView *specScroll;
@property (retain,nonatomic) UIButton *currentBtn;
@property (retain,nonatomic) NSString *propertyId;
@property (retain,nonatomic) NSMutableArray *specArr;
@property (retain,nonatomic) NSMutableArray *specValueArr;
@property (retain,nonatomic) NSMutableArray *newsSelectedArr;
@property (assign,nonatomic) EcomBProperty  *hasDic;
@property (assign,nonatomic) BOOL isBuyNow;
@property (retain,nonatomic) UILabel *specValueLB;
@property (retain,nonatomic) UILabel *specStockNumLB;
@property (retain,nonatomic) NSArray *titleIVArr;
@property (retain,nonatomic) UIActivityIndicatorView *activityIndicator;
@property (assign,nonatomic) NSInteger wxSharePoint;
@property (assign,nonatomic) NSInteger wxFriendSharePoint;
@property (assign,nonatomic) NSInteger weiboSharePoint;
@property (assign,nonatomic) NSInteger qqSharePoint;
@property (assign,nonatomic) NSInteger smsSharePoint;
@property (assign,nonatomic) BOOL isSpecial;

//@property (strong, nonatomic) ZSTShareView *shareView;
@property (strong,nonatomic) ZSTNewShareView *shareView;

@end

@implementation ZSTGoodsShowViewController
{
    ZSTECBProduct * _product;
    UIWebView *_detailWebView;
    UILabel * xianshichuxiaoLabel;
    NSString *shareurl;
}

static NSString *const titleCell = @"titleCell";
static NSString *const priceCell = @"priceCell";
static NSString *const rawPriceCell = @"rawPriceCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isSpecial = NO;
    self.sectionTitleArr = @[@"占位用",@"商品详情",@"电话咨询"];
    self.titleIVArr = @[ZSTModuleImage(@"close.png"),ZSTModuleImage(@"module_ecomb_gools_show.png"),ZSTModuleImage(@"module_ecomb_myorder_call.png")];
    self.newsSelectedArr = [NSMutableArray array];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareSucceed:) name:NotificationName_WXShareSucceed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareFaild:) name:NotificationName_WXShareFaild object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboSuccess:) name:@"sendWeiboSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendQQShareSuccess:) name:@"QQShareSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendQQShareFail:) name:@"QQShareFail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboShareFail:) name:@"sendWeiboFailure" object:nil];
    
    if ([[ZSTF3Preferences shared].ECECCID isEqualToString:@"605887"])
    {
        self.isSpecial = YES;
    }
    else
    {
        self.isSpecial = NO;
    }
    
    [self buildNavigation];
    [self buildTableView];
    [self buildHeaderView];
    [self buildWebView];
    [self buildToolBar];
    [self buildFloorBtnView];
    [self buildDarkView];
    [self buildSpecView];
    [self getDetailByNet:self.pageInfo.ecombHomeProductid];
    [self buildRefreshView];

    // 分享
//    [self createShareView];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
    }
    return self;
}

- (void)buildNavigation
{
//    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"商品展示", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"分享",nil) target:self selector:@selector (shareAction)];
    
    self.view.backgroundColor = [UIColor whiteColor];
}

- (void)buildToolBar
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 42 - 64, WIDTH, 42)];
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBACOLOR(247, 247, 247, 1).CGColor;
    line.frame = CGRectMake(0, 0, WIDTH, 1);
    [bottomView.layer addSublayer:line];
    bottomView.backgroundColor = RGBACOLOR(247, 247, 247, 1);
    if (!self.isSpecial)
    {
        [self.view addSubview:bottomView];
    }
    
    
    UIButton *buyNowBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 15 - (273.0/2),  5, 273.0/2, 63.0/2.0)];
    [buyNowBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_red.png") forState:UIControlStateNormal];
    buyNowBtn.tag = 101;
    [buyNowBtn addTarget:self action:@selector(buyNow:) forControlEvents:UIControlEventTouchUpInside];
    [buyNowBtn setTitle:@"￥  立即购买" forState:UIControlStateNormal];
    buyNowBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    if (!self.isSpecial)
    {
        [bottomView addSubview:buyNowBtn];
    }

    ZSTEComBPutInCarButton *carNowBtn = [[ZSTEComBPutInCarButton alloc] initWithFrame:CGRectMake(15, 5, 273.0/2, 63.0/2.0)];
    [carNowBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_orange.png") forState:UIControlStateNormal];
    [carNowBtn setImage:ZSTModuleImage(@"module_ecomb_goodsshow_putincarbtn.png") forState:UIControlStateNormal];
    [carNowBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    [carNowBtn addTarget:self action:@selector(addToCar:) forControlEvents:UIControlEventTouchUpInside];
    carNowBtn.tag = 1;
    carNowBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    if (!self.isSpecial)
    {
        [bottomView addSubview:carNowBtn];
    }
    
}

- (void)buildTableView
{
    if (self.isSpecial)
    {
        self.goodsDetailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    }
    else
    {
        self.goodsDetailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 42)];
    }
    
    self.goodsDetailTableView.delegate = self;
    self.goodsDetailTableView.dataSource = self;
    self.goodsDetailTableView.backgroundColor = [UIColor whiteColor];
    self.goodsDetailTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.goodsDetailTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:titleCell];
    [self.goodsDetailTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:priceCell];
    [self.goodsDetailTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:rawPriceCell];
    [self.view addSubview:self.goodsDetailTableView];
}

- (void)buildWebView
{
    if (self.isSpecial)
    {
        self.detailView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64)];
    }
    else
    {
        self.detailView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64 - 42)];
    }
    
    self.detailView.backgroundColor = [UIColor whiteColor];
    _detailWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.detailView.bounds.size.height)];
    _detailWebView.scrollView.delegate = self;
    _detailWebView.delegate = self;
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(webViewDismiss:)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionRight];
    [_detailWebView addGestureRecognizer:swipe];
    [self.detailView addSubview:_detailWebView];
    [self.view addSubview:self.detailView];
}

- (void)buildRefreshView
{
    __block ZSTGoodsShowViewController *weakSelf = self;
    [self.goodsDetailTableView addRefreshFooterViewWithAniViewClass:[JHRefreshAniBaseView class] beginRefresh:^{
        
        [UIView animateWithDuration:0.3f animations:^{
            weakSelf.goodsDetailTableView.contentOffset = CGPointMake(0, HEIGHT - 20);
            if (weakSelf.isSpecial)
            {
                weakSelf.goodsDetailTableView.frame = CGRectMake(0, - HEIGHT - 64, WIDTH, HEIGHT - 64);
            }
            else
            {
                weakSelf.goodsDetailTableView.frame = CGRectMake(0, - HEIGHT - 64 - 42, WIDTH, HEIGHT - 64 - 42);
            }
            
            weakSelf.detailView.frame = CGRectMake(0, 0, WIDTH, weakSelf.detailView.bounds.size.height);
        }];
        
        [weakSelf.goodsDetailTableView footerEndRefreshing];
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 3;
    }
    else
    {
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:titleCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - 30, 10)];
            self.titleLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:17];
            self.titleLabel.numberOfLines = 0;
            self.titleLabel.textColor = RGBCOLOR(2*16+10, 2*16+10, 2*16+10);
            [cell.contentView addSubview:self.titleLabel];
            return cell;
        }
        else if (indexPath.row == 1)
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:priceCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            self.priceLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 10, 30)];
            self.priceLabel.font = [UIFont boldSystemFontOfSize:18];
            self.priceLabel.textColor = RGBCOLOR(202, 8, 20);
            self.priceLabel.numberOfLines = 0;
            self.priceLabel.lineBreakMode = NSLineBreakByWordWrapping;
            [cell.contentView addSubview:self.priceLabel];
            
            xianshichuxiaoLabel  = [[UILabel alloc] init];
            xianshichuxiaoLabel.backgroundColor = RGBCOLOR(202, 8, 20);
            xianshichuxiaoLabel.textAlignment = NSTextAlignmentCenter;
            [xianshichuxiaoLabel setTextColor:[UIColor whiteColor]];
            xianshichuxiaoLabel.font = [UIFont boldSystemFontOfSize:12];
            [cell.contentView addSubview:xianshichuxiaoLabel];
            
            _pointLabel = [[UILabel alloc] init];
            _pointLabel.hidden = YES;
            _pointLabel.backgroundColor = RGBCOLOR(248, 121, 41);
            _pointLabel.textColor = [UIColor whiteColor];
            _pointLabel.textAlignment = NSTextAlignmentCenter;
            _pointLabel.font = [UIFont systemFontOfSize:12];
            _pointLabel.text = @"积分换购";
            [cell.contentView addSubview:_pointLabel];
            
            return cell;
        }
        else
        {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:rawPriceCell];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            _originalPriceLabel = [[UILabel alloc]init];
            _originalPriceLabel.font = [UIFont systemFontOfSize:12];
            _originalPriceLabel.textColor = RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5);
            [cell.contentView addSubview:_originalPriceLabel];
            
            _freightPriceLabel = [[UILabel alloc]init];
            _freightPriceLabel.font = [UIFont systemFontOfSize:12];
            _freightPriceLabel.textColor = RGBCOLOR(56, 56, 56);
            _freightPriceLabel.text = @"运费:￥0.0";
            [cell.contentView addSubview:_freightPriceLabel];
            
            _slip = [[UILabel alloc]init];
            _slip.backgroundColor = RGBCOLOR(13 * 16 +5, 13*16 + 5, 13*16 +5);
            [cell.contentView addSubview:_slip];

            return cell;
        }
    }
    else
    {
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cells"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    headerView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBACOLOR(247, 247, 247, 1).CGColor;
    line.frame = CGRectMake(0, 0, WIDTH, 1);
    [headerView.layer addSublayer:line];
    
    UIImageView *titleIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 14, 14)];
    titleIV.contentMode = UIViewContentModeScaleAspectFit;
    titleIV.image = self.titleIVArr[section];
    [headerView addSubview:titleIV];
    
    UILabel *sectionLB = [[UILabel alloc] initWithFrame:CGRectMake(40, 7, WIDTH - 50 - 40, 30)];
    sectionLB.text = self.sectionTitleArr[section];
    sectionLB.font = [UIFont systemFontOfSize:14.0];
    [headerView addSubview:sectionLB];
    
    UIImageView *arrowImg = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 15 - 14, 15, 14, 14)];
    arrowImg.image = ZSTModuleImage(@"module_ecomb_right.png");
    [headerView addSubview:arrowImg];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
    headerView.tag = section;
    [headerView addGestureRecognizer:tap];
    
    if (section == 1 || section == 3)
    {
        CALayer *bottomLine = [CALayer layer];
        bottomLine.backgroundColor = RGBACOLOR(247, 247, 247, 1).CGColor;
        bottomLine.frame = CGRectMake(0, 43, WIDTH, 1);
        [headerView.layer addSublayer:bottomLine];
    }
    
    return headerView;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return 44;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            NSString *titleString = _product.productName;
            CGSize titleSize = [titleString sizeWithFont:[UIFont systemFontOfSize:17.0f] constrainedToSize:CGSizeMake(WIDTH - 30, MAXFLOAT)];
            return titleSize.height + 10;
        }
        else if (indexPath.row == 1)
        {
            return 35;
        }
        else
        {
            return 20;
        }
    }
    else
    {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 2)
    {
        return 0;
    }
    else
    {
        return 10;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footView = [[UIView alloc] init];
    footView.backgroundColor = [UIColor whiteColor];
    return footView;
}

- (void)buildHeaderView
{
    _carouseView = [[ZSTECBHomeCarouselView alloc]initWithFrame:CGRectMake(0, 0, WIDTH , WIDTH)];
    _carouseView.carouselDelegate = self;
    _carouseView.carouselDataSource = self;
    [_carouseView setCarouselViewBackgroundImg:@"module_ecomb_goodsshow.png"];
    self.goodsDetailTableView.tableHeaderView = _carouseView;
}

- (void)buildFloorBtnView
{
    UIButton * shopingCarBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    shopingCarBtn.frame = CGRectMake(WIDTH - 40 - 15, HEIGHT - 40 - 64 - 52, 40, 40);
    [shopingCarBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_goodsshow_shopingcarbtn.png") forState:UIControlStateNormal];
    [shopingCarBtn addTarget:self action:@selector(shopingCarBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:shopingCarBtn];
}

- (void)buildDarkView
{
    self.darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.darkView.backgroundColor = [UIColor clearColor];
    self.darkView.hidden = YES;
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.8;
    [self.darkView addSubview:alphaView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(darkViewDismiss:)];
    [self.darkView addGestureRecognizer:tap];
    
    [self.view addSubview:self.darkView];
}

- (void)buildSpecView
{
    self.specView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, (HEIGHT - 64) * 0.7)];
    self.specView.backgroundColor = [UIColor whiteColor];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(specViewDismiss:)];
    [swipe setDirection:UISwipeGestureRecognizerDirectionDown];
    [self.specView addGestureRecognizer:swipe];
    
    self.specProductIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, - WIDTH * 0.05, WIDTH * 0.3, WIDTH * 0.3)];
    self.specProductIV.contentMode = UIViewContentModeScaleAspectFit;
    [self.specView addSubview:self.specProductIV];
    
    CALayer *lines = [CALayer layer];
    lines.backgroundColor = RGBACOLOR(247, 247, 247, 1).CGColor;
//    lines.backgroundColor = [UIColor blackColor].CGColor;
    lines.frame = CGRectMake(0, WIDTH * 0.25 + 5, WIDTH, 1);
    [self.specView.layer addSublayer:lines];
    
    self.specPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.3 + 15 + 10, 5, WIDTH - WIDTH * 0.3 - 15 - 10 - 30, 20)];
    self.specPriceLB.backgroundColor = [UIColor clearColor];
    self.specPriceLB.textColor = RGBCOLOR(214, 84, 88);
    self.specPriceLB.font = [UIFont systemFontOfSize:17];
    self.specPriceLB.text = @"0.00";
    [self.specView addSubview:self.specPriceLB];
    
    PMRepairButton *closeBtn = [[PMRepairButton alloc] init];
    closeBtn.frame = CGRectMake(WIDTH - 35, 5, 20, 20);
    [closeBtn setImage:ZSTModuleImage(@"close.png") forState:UIControlStateNormal];
    [closeBtn addTarget:self action:@selector(specViewClose:) forControlEvents:UIControlEventTouchUpInside];
    [self.specView addSubview:closeBtn];
    
    self.specValueLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.3 + 15 + 10, 30, WIDTH -  WIDTH * 0.3 - 15 - 10, 20)] ;
    self.specValueLB.backgroundColor = [UIColor whiteColor];
    self.specValueLB.textColor = [UIColor lightGrayColor];
    self.specValueLB.font = [UIFont systemFontOfSize:15.0f];
//    self.specValueLB.text = @"请选择规格";
    [self.specView addSubview:self.specValueLB];
    
    self.specStockNumLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.3 + 15 + 10, 55, WIDTH - WIDTH *0.3 - 15 -10, 20)];
    self.specStockNumLB.backgroundColor = [UIColor whiteColor];
    self.specStockNumLB.textColor = [UIColor lightGrayColor];
    self.specStockNumLB.font = [UIFont systemFontOfSize:15.0f];
    [self.specView addSubview:self.specStockNumLB];
    
    
    self.specScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(15, WIDTH * 0.25 + 10, WIDTH - 30, (HEIGHT - 64) * 0.7 - WIDTH * 0.25 - 42 - 10)];
    [self.specView addSubview:self.specScroll];
    
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, self.specView.bounds.size.height - 42, WIDTH, 42)];
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBACOLOR(247, 247, 247, 1).CGColor;
    line.frame = CGRectMake(0, 0, WIDTH, 1);
    [bottomView.layer addSublayer:line];
    bottomView.backgroundColor = RGBACOLOR(247, 247, 247, 1);
    [self.view addSubview:bottomView];
    
    UIButton *buyNowBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 15 - (273.0/2),  5, 273.0/2, 63.0/2.0)];
    [buyNowBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_red.png") forState:UIControlStateNormal];
    [buyNowBtn setTitle:@"￥  立即购买" forState:UIControlStateNormal];
    buyNowBtn.tag = 102;
    [buyNowBtn addTarget:self action:@selector(buyNow:) forControlEvents:UIControlEventTouchUpInside];
    buyNowBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [bottomView addSubview:buyNowBtn];
    
    ZSTEComBPutInCarButton *carNowBtn = [[ZSTEComBPutInCarButton alloc] initWithFrame:CGRectMake(15, 5, 273.0/2, 63.0/2.0)];
    [carNowBtn setBackgroundImage:ZSTModuleImage(@"module_ecomb_submitbutton_orange.png") forState:UIControlStateNormal];
    [carNowBtn setImage:ZSTModuleImage(@"module_ecomb_goodsshow_putincarbtn.png") forState:UIControlStateNormal];
    [carNowBtn setTitle:@"加入购物车" forState:UIControlStateNormal];
    [carNowBtn addTarget:self action:@selector(addToCar:) forControlEvents:UIControlEventTouchUpInside];
    carNowBtn.tag = 2;
    carNowBtn.titleLabel.font = [UIFont boldSystemFontOfSize:17];
    [bottomView addSubview:carNowBtn];
    [self.specView addSubview:bottomView];

    [self.view addSubview:self.specView];
    
    
}

- (void)getDetailByNet:(NSString *)productId
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getProductsShowInfo:productId];
    [self.engine getEcomBGoodsInfoCarousel:productId];
    [self.engine getPropertyViewControllerData:productId];
}

- (void)getProductsShowDidSucceed:(ZSTECBProduct *)dic
{
    if (dic) {
        [TKUIUtil hiddenHUD];
        _product = dic;
        [self setProductDetail:dic];
        [self.engine getProductsDetail:[_product.productID integerValue]];
        [self.goodsDetailTableView reloadData];
        
        if ([[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""] || [[ZSTF3Preferences shared].UserId isEqualToString:@""])
        {
            if (!self.shareView)
            {
                [self createShareView:NO AndResponse:nil AndIconUrl:_product.iconUrl];
            }
        }
        else
        {
            [self.engine getPointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
        }
    }
}

- (void)getProductsShowFaild:(int)resultCode
{
    [TKUIUtil alertInWindow:@"没有数据" withImage:nil];
    [_carouseView hideStateBar];
}

- (void)setProductDetail:(ZSTECBProduct *)product
{
    shareurl = [NSString stringWithFormat:@"http://mod.pmit.cn/EcomB/detail.html?moduletype=%ld&productid=%@&ecid=%@&module_type=%ld",(long)self.moduleType,product.productID,[ZSTF3Preferences shared].ECECCID,(long)self.moduleType];
    
    NSString *titleString = product.productName;
    CGSize titleSize = [titleString sizeWithFont:[UIFont fontWithName:@"Helvetica-Bold" size:17] constrainedToSize:CGSizeMake(WIDTH - 30, MAXFLOAT)];
    _titleLabel.frame = CGRectMake(15, 10, WIDTH - 30, titleSize.height);
    _titleLabel.text = titleString;
    
    NSString *priceString = [NSString stringWithFormat:@"%@%@",@"￥",product.salesPrice];
    CGSize priceSize = [priceString sizeWithFont:[UIFont boldSystemFontOfSize:18] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
    self.priceLabel.frame = CGRectMake(15, 5, priceSize.width, 30);
    self.priceLabel.text = priceString;
    
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} range:NSMakeRange(0, 1)];
    self.specPriceLB.attributedText = nodeString;
    
    NSString *discountStr = product.salesLabel;
    
    if (product.discount && [product.discount floatValue] < 10 && [product.discount floatValue] > 0) {
        
        discountStr = [discountStr stringByAppendingString:[NSString stringWithFormat:@" (%@折)",product.discount]];
    }
    
    xianshichuxiaoLabel.text = discountStr;
    CGSize xianshichuxiaoLabelsize = [xianshichuxiaoLabel.text sizeWithFont:xianshichuxiaoLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, 30) lineBreakMode:NSLineBreakByWordWrapping];
    xianshichuxiaoLabel.frame = CGRectMake(CGRectGetMaxX(_priceLabel.frame) + 20, CGRectGetMinY(_priceLabel.frame) + 5, xianshichuxiaoLabelsize.width + 30, 20);
    xianshichuxiaoLabel.layer.cornerRadius = 4;
    xianshichuxiaoLabel.clipsToBounds = YES;
    
    _pointLabel.frame = CGRectMake(CGRectGetMaxX(xianshichuxiaoLabel.frame)+5, xianshichuxiaoLabel.frame.origin.y, 60, 21);
    
     _pointLabel.hidden = !product.jifenflag;
    
    _originalPriceLabel.text = [NSString stringWithFormat:@"%@:%@",product.primeLabel,product.primePrice];
    _originalPriceLabel.numberOfLines = 0;
    _originalPriceLabel.lineBreakMode = NSLineBreakByWordWrapping;
    CGSize size3 = [_originalPriceLabel.text sizeWithFont:_originalPriceLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, 30) lineBreakMode:NSLineBreakByWordWrapping];
    _originalPriceLabel.frame = CGRectMake(15, 0, size3.width, 20);
    
    _slip.frame = CGRectMake(CGRectGetMinX(_originalPriceLabel.frame), _originalPriceLabel.centerY, CGRectGetWidth(_originalPriceLabel.frame), 1);
    
    int freight = [product.freightPrice intValue];
    if (freight > 0) {
        _freightPriceLabel.text = [NSString stringWithFormat:@"运费￥:%.2f", [product.freightPrice  floatValue]];
    }
    else{
        _freightPriceLabel.text = @"包邮";
    }
    
    CGSize size4 = [_freightPriceLabel.text sizeWithFont:_freightPriceLabel.font constrainedToSize:CGSizeMake(MAXFLOAT, 30) lineBreakMode:NSLineBreakByWordWrapping];
     _freightPriceLabel.frame = CGRectMake(WIDTH - 15 - size4.width, CGRectGetMinY(_originalPriceLabel.frame) , size4.width, 20);
}

- (void)getProductsContentDidSucceed:(NSString *)newsContent
{
    NSString *html = @"";
    if ([newsContent hasPrefix:@"http://"] || [newsContent hasPrefix:@"https://"])
    {
        html = newsContent;
        [_detailWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:html]]];
    }
    else
    {
        html = [NSString stringWithFormat:@"<html><head><meta charset='utf-8'><meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no'><meta http-equiv='Cache-Control' content='max-age=0'><meta http-equiv='Cache-Control' content='must-revalidate'><meta http-equiv='Cache-Control' content='no-cache'>%@</body></html>",newsContent];
        [_detailWebView loadHTMLString:html baseURL:nil];
        
    }
}


-(void)getEcomBGoodsInfoCarouselDidFailed:(int)resultCode
{
    NSLog(@"商品展示页 轮播图失败");
}
-(void)getEcomBGoodsInfoCarouselDidSucceed:(NSArray *)carouseles
{
    _carouseData = [EcomBCarouseInfo ecomBCarouseWithArray:carouseles];
    if (_carouseData.count <= 1) {
        [_carouseView hideStateBar];
    }
    [_carouseView pageControlState:pageControlStateCENTER];
    [_carouseView reloadData];
}
#pragma mark - ZSTEComBCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTECBHomeCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomBCarouseInfo *)carouselView:(ZSTECBHomeCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate

- (void)carouselView:(ZSTECBHomeCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSMutableArray *photos = [NSMutableArray array];
    for (int i=0; i<self.carouseData.count; i++) {
        EcomBCarouseInfo *photoInfo = [self.carouseData objectAtIndex:i];
        if (photoInfo!=nil && photoInfo.ecomBCarouseInfoCarouselurl!=nil) {
            EGOPhoto *photo = [[EGOPhoto alloc] initWithImageURL:[NSURL URLWithString:photoInfo.ecomBCarouseInfoCarouselurl]];
            [photos addObject:photo];
        }
    }
    
    EGOPhotoSource *source = [[EGOPhotoSource alloc] initWithPhotos:photos];
    EGOPhotoViewController *photoController = [[EGOPhotoViewController alloc] initWithPhotoSource:source];
    photoController.pageIndex = index;
    photoController.haveBackButton = YES;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:photoController];

    [self presentViewController:navController animated:YES completion:nil];
    
}

-(void)dealloc
{
    _carouseView.carouselDataSource = nil;
    _carouseView.carouselDelegate = nil;
    //    [_carouseView release];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.goodsDetailTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
    else if (scrollView == _detailWebView.scrollView)
    {
        if (scrollView.contentOffset.y < -100)
        {
            [UIView animateWithDuration:0.3f animations:^{
                
                if (self.isSpecial)
                {
                    self.detailView.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64);
                    self.goodsDetailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
                }
                else
                {
                    self.detailView.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64 - 42);
                    self.goodsDetailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 42);
                }
                
                
            } completion:^(BOOL finished) {
                
                self.goodsDetailTableView.contentOffset = CGPointMake(0, 0);
                
            }];
        }
    }
    
}

- (void)goToDetail:(UITapGestureRecognizer *)tap
{
    NSInteger section = tap.view.tag;
    if (section == 1)
    {
        [UIView animateWithDuration:0.3f animations:^{
            self.goodsDetailTableView.contentOffset = CGPointMake(0, HEIGHT - 20);
            if (self.isSpecial)
            {
                self.goodsDetailTableView.frame = CGRectMake(0, - HEIGHT - 64 - 42, WIDTH, HEIGHT - 64 - 42);
                self.detailView.frame = CGRectMake(0, 0, WIDTH, self.detailView.bounds.size.height);
            }
            else
            {
                self.goodsDetailTableView.frame = CGRectMake(0, - HEIGHT - 64 - 42, WIDTH, HEIGHT - 64);
                self.detailView.frame = CGRectMake(0, 0, WIDTH, self.detailView.bounds.size.height);
            }
            
        }];
    }
//    else if (section == 2)
//    {
//        ZSTEComBAskOnLineViewController * ascViewController = [[ZSTEComBAskOnLineViewController alloc]init];
//        ascViewController.productid = _pageInfo.ecombHomeProductid;
//        ascViewController.hidesBottomBarWhenPushed = YES;
//        [self.navigationController pushViewController:ascViewController animated:YES];
//        [ascViewController release];
//    }
    else if (section == 2)
    {
        if (_product.phone.length == 0) {
            return;
        }
        UIActionSheet * sheet = [[UIActionSheet alloc]initWithTitle:_product.phone delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"呼叫" otherButtonTitles:nil];
        sheet.tag = 100;
        [sheet showInView:self.view];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"分享成功!" , nil)];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (actionSheet.tag == 100) {
        if (buttonIndex == 0) {
            NSString * tel = [NSString stringWithFormat:@"tel://%@",_product.phone];
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
        }
        else
        {
            NSLog(@"电话咨询返回");
        }
        
    } else if (actionSheet.tag == 101) {
        if (buttonIndex == actionSheet.cancelButtonIndex) {
            return;
        }
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]
            || [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈" , nil)]){
            
            if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
                
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:NSLocalizedString(@"提示" , nil)
                                      message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                      delegate:self
                                      cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                      otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
                alert.tag = 108;
                [alert show];
                return;
            }
            
            WXMediaMessage *message = [WXMediaMessage message];
            [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
            message.title = _product.productName;
            message.description = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！", nil),appDisplayName];
            
            
            WXImageObject *ext = [WXImageObject object];
            NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
            ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
            message.mediaObject = ext;
            
            WXWebpageObject *webpage = [WXWebpageObject object];
            webpage.webpageUrl = [shareurl length] ? shareurl:[NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
            message.mediaObject = webpage;
            
            SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
            req.bText = NO;
            req.message = message;
            
            if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]) {
                req.scene = WXSceneSession;
            }
            else  if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈"  , nil)]){
                req.scene = WXSceneTimeline;
            }
            
            [WXApi sendReq:req];
            
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"短信" , nil)]){
            
            Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
            
            if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
                
                MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
                
                NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
                picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在“%@”，看到一个不错的商品，你肯定喜欢，点击链接：%@，手机客户端下载地址：%@/%@", nil),appDisplayName,shareurl,sharebaseurl,[ZSTF3Preferences shared].ECECCID];
                
                
                
                picker.messageComposeDelegate = self;
                
                [self presentViewController:picker animated:YES completion:nil];
                
            } else {
                
                [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
            }
        }else{
            
            ZSTHHSinaShareController *sinaShare = [[ZSTHHSinaShareController alloc] init];
            sinaShare.delegate = self;
            
            sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！链接：%@", nil),appDisplayName,shareurl];
            
            if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"新浪微博" , nil)]) {
                sinaShare.shareType = sinaWeibo_ShareType;
                sinaShare.navigationItem.title = NSLocalizedString(@"新浪微博", nil);
            }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"QQ空间" , nil)]){
                sinaShare.shareType = QQ_ShareType;
                sinaShare.navigationItem.title = NSLocalizedString(@"QQ空间", nil);
            }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"腾讯微博" , nil)]){
                sinaShare.shareType = TWeibo_ShareType;
                sinaShare.navigationItem.title = NSLocalizedString(@"腾讯微博", nil);
            }
            
            UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:sinaShare];
            [self presentViewController:n animated:YES completion:nil];
        }
        
    }
}

-(void)getPropertyViewControllerDataDidSucceed:(NSDictionary *) dic
{
    _propertyInfo = [EcomBPropertyInfo ecomBPropertyInfoWithDictionary:dic];
    [self reloadData];
}

- (void)reloadData
{
    [self.specProductIV setImageWithURL:[NSURL URLWithString:_propertyInfo.ecomBPropertyInfoImageStr]];
    if (_propertyInfo.isOld)
    {
        [self buildOldSpecView];
    }
    else
    {
        [self buildNewsSpecView];
    }
}

- (void)specViewDismiss:(UISwipeGestureRecognizer *)swipe
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.specView.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT * 0.7);
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        
    }];
    
}

- (void)webViewDismiss:(UISwipeGestureRecognizer *)swipe
{
    [UIView animateWithDuration:0.3f animations:^{
        
        if (self.isSpecial)
        {
            self.detailView.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64);
            self.goodsDetailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
        }
        else
        {
            self.detailView.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64 - 42);
            self.goodsDetailTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 42);
        }
        
        
    } completion:^(BOOL finished) {
        
        self.goodsDetailTableView.contentOffset = CGPointMake(0, 0);
        
    }];
}

- (void)addToCar:(ZSTEComBPutInCarButton *)sender
{
    self.isBuyNow = NO;
    
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        
        return;
    }
    
    if (sender.tag == 1)
    {
        if (_propertyInfo.ecomBPropertyInfoProperties.count == 0)
        {
            if (self.propertyId.length>0) {
                [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
            }
            else if(self.propertyId.length == 0 && _propertyInfo.ecomBPropertyInfoProperties.count == 0)
            {
                [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:@""];
            }
        }
        else
        {
            self.darkView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.specView.frame = CGRectMake(0, (HEIGHT - 64) * 0.3, WIDTH, (HEIGHT - 64) * 0.7);
                
            } completion:^(BOOL finished) {
                
                
                
            }];
        }
    }
    else
    {
        if (_propertyInfo.isOld)
        {
            if (self.propertyId.length>0) {
                [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加入购物车哦..." , nil)];
                [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
            }
            else
            {
                [TKUIUtil alertInWindow:@"忘记选属性了" withImage:nil];
                return;
            }
        }
        else
        {
            if (self.newsSelectedArr.count < self.specArr.count)
            {
                [TKUIUtil alertInWindow:@"忘记选属性了" withImage:nil];
                return;
            }
            else
            {
                if (_propertyInfo.isOld)
                {
                    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加入购物车哦..." , nil)];
                    [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
                }
                else
                {
                    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加入购物车哦..." , nil)];
                    [self.engine putNewsGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
                }
                
            }
        }
    }
}

- (void)buyNow:(UIButton *)sender
{
    self.isBuyNow = YES;
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        
        return;
    }
    
    if (sender.tag == 101)
    {
        if (_propertyInfo.ecomBPropertyInfoProperties.count == 0)
        {
            [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加入购物车哦..." , nil)];
            [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:@""];
        }
        else
        {
            self.darkView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.specView.frame = CGRectMake(0, (HEIGHT - 64) * 0.3, WIDTH, (HEIGHT - 64) * 0.7);
                
            } completion:^(BOOL finished) {
                

            }];
        }
    }
    else
    {
        if (self.newsSelectedArr.count < self.specArr.count)
        {
            [TKUIUtil alertInWindow:@"忘记选属性了" withImage:nil];
            return;
        }
        else
        {
            if (_propertyInfo.isOld)
            {
                if (self.propertyId.length>0) {
                    [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
                }
                else
                {
                    [TKUIUtil alertInWindow:@"忘记选属性了" withImage:nil];
                    return;
                }
            }
            else
            {
                if (self.newsSelectedArr.count < self.specArr.count)
                {
                    [TKUIUtil alertInWindow:@"忘记选属性了" withImage:nil];
                    return;
                }
                else
                {
                    if (_propertyInfo.isOld)
                    {
                        [self.engine putGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
                    }
                    else
                    {
                        [self.engine putNewsGoodsIntoCar:self.pageInfo.ecombHomeProductid propertyId:self.propertyId];
                    }
                    
                }
            }
        }
    }
}

- (void)buildOldSpecView
{
    NSArray *specArr = _propertyInfo.ecomBPropertyInfoProperties;
    CGFloat startX = 0;
    CGFloat startY = 5;
    CGFloat widthX = 10;
    CGFloat heightY = 10;
    CGFloat borderWidth = WIDTH - 30;
    NSInteger row = 0;
    
    self.specStockNumLB.text = [NSString stringWithFormat:@"库存:%@",@([_propertyInfo ecomBCarouseInfoStockcount])];
    self.specValueLB.text = @"请选择规格";
    for (NSInteger i = 0; i < specArr.count; i++)
    {
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        NSString *btnTitle = [specArr[i] ecomBPropertyProperty];
        CGSize btnSize = [btnTitle sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 25)];
        if (startX + btnSize.width + 10 > borderWidth)
        {
            startX = 0;
            startY = startY + 25 + heightY;
            row += 1;
        }
        
        UIImage *image  = ZSTModuleImage(@"module_ecomb_property_btn_selected.png");
        CGFloat capWidth = image.size.width / 2;
        CGFloat capHeight = image.size.height / 2;
        UIImage* editImage = [image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
        
        btn.frame = CGRectMake(startX, startY, btnSize.width + 40, 25);
        startX = startX + btnSize.width + widthX + 40;
        [btn setTitle:btnTitle forState:UIControlStateNormal];
        btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
        [btn setBackgroundImage:editImage forState:UIControlStateDisabled];
        [btn setTitleColor:RGBCOLOR(152, 152, 152) forState:UIControlStateNormal];
        btn.layer.borderWidth = 1.0f;
        btn.layer.borderColor = RGBCOLOR(239, 239, 239).CGColor;
        btn.layer.cornerRadius = 4.0f;
        btn.clipsToBounds = YES;
        btn.tag = i + 2015;
        [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.specScroll addSubview:btn];
    }
    
    self.specScroll.contentSize = CGSizeMake(0, (row + 1) * 36);
}

- (void)buildNewsSpecView
{
    NSString *specNames = _propertyInfo.ecomBPropertyInfoSpecNames;
    NSMutableArray *specArr = [NSMutableArray array];
    NSMutableArray *specValueArr = [NSMutableArray array];
    self.specArr = specArr;
    self.specValueArr = specValueArr;
    self.specStockNumLB.text = [NSString stringWithFormat:@"库存:%@",@([_propertyInfo ecomBCarouseInfoStockcount])];
    
    NSArray *specNameArr = [specNames componentsSeparatedByString:@"|"];
    for (NSString *thisSpecName in specNameArr)
    {
        NSString *specTitle = [[thisSpecName componentsSeparatedByString:@"#"] firstObject];
        [specArr addObject:specTitle];
        NSString *specValueString = [[thisSpecName componentsSeparatedByString:@"#"] lastObject];
        NSArray *specValues = [specValueString componentsSeparatedByString:@";"];
        [specValueArr addObject:specValues];
    }
    
    self.newsSelectedArr = self.specArr;
    NSString *specTitle = [self.newsSelectedArr componentsJoinedByString:@"、"];
    self.specValueLB.text = [NSString stringWithFormat:@"请选择%@",specTitle];
    
    CGFloat startX = 0;
    CGFloat startY = 5;
    CGFloat widthX = 10;
    CGFloat heightY = 10;
    CGFloat borderWidth = WIDTH - 30;
    NSInteger row = 0;
    
    for (NSInteger i = 0; i < specArr.count; i++)
    {
        UILabel *specTitle = [[UILabel alloc] initWithFrame:CGRectMake(startX, startY, WIDTH - 30, 20)];
        specTitle.text = specArr[i];
        specTitle.font = [UIFont systemFontOfSize:15.0f];
        specTitle.textColor = [UIColor lightGrayColor];
        [self.specScroll addSubview:specTitle];
        
        startY += 30;
        
        NSArray *thisSpecValueArr = specValueArr[i];
        for (NSInteger j = 0; j < thisSpecValueArr.count; j++)
        {
            UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
            NSString *btnTitle = thisSpecValueArr[j];
            CGSize btnSize = [btnTitle sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 25)];
            if (startX + btnSize.width + 10 > borderWidth)
            {
                startX = 0;
                startY = startY + 25 + heightY;
                row += 1;
            }
            
            UIImage *image  = ZSTModuleImage(@"module_ecomb_property_btn_selected.png");
            CGFloat capWidth = image.size.width / 2;
            CGFloat capHeight = image.size.height / 2;
            UIImage* editImage = [image stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
            
            btn.frame = CGRectMake(startX, startY, btnSize.width + 40, 25);
            startX = startX + btnSize.width + widthX + 40;
            [btn setTitle:btnTitle forState:UIControlStateNormal];
            btn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
            [btn setBackgroundImage:editImage forState:UIControlStateDisabled];
            [btn setTitleColor:RGBCOLOR(152, 152, 152) forState:UIControlStateNormal];
            btn.layer.borderWidth = 1.0f;
            btn.layer.borderColor = RGBCOLOR(239, 239, 239).CGColor;
            btn.layer.cornerRadius = 4.0f;
            btn.clipsToBounds = YES;
            btn.tag = (j + 1) * 100 + i * 1000;
            [btn addTarget:self action:@selector(clickAction:) forControlEvents:UIControlEventTouchUpInside];
            [self.specScroll addSubview:btn];
        }
        
    
        startX = 0;
        startY += 40;
        
    }
    
    self.specScroll.contentSize = CGSizeMake(0, (row + 1) * 36);
    
}

- (void)clickAction:(UIButton *)sender
{
    if (_propertyInfo.isOld)
    {
        self.currentBtn.layer.borderColor = RGBCOLOR(239, 239, 239).CGColor;
        self.currentBtn.enabled = YES;
        
        sender.enabled = NO;
        sender.layer.borderWidth = 1.0f;
        sender.layer.borderColor = [UIColor clearColor].CGColor;
        
        self.currentBtn = sender;
        
        NSInteger index = sender.tag - 2015;
        NSArray *specList = _propertyInfo.ecomBPropertyInfoProperties;
        self.propertyId = [[specList objectAtIndex:index] ecomBPropertyPropertyid];
        NSString *specPriceString = [NSString stringWithFormat:@"￥%.2f",_propertyInfo.ecomBPropertyInfoSalesprice];
        NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:specPriceString];
        [nodeString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} range:NSMakeRange(0, 1)];
        self.specPriceLB.attributedText = nodeString;
        self.specValueLB.text = [[specList objectAtIndex:index] ecomBPropertyProperty];
        self.specStockNumLB.text = [NSString stringWithFormat:@"库存:%@",@([[specList objectAtIndex:index] ecomBPropertyStockcount])];
    }
    else
    {
        NSInteger indexI = sender.tag / 1000;
        
//        NSString *titleString = self.specArr[indexI];
        NSArray *thisSpecValues = self.specValueArr[indexI];
        for (NSInteger i = 0; i < thisSpecValues.count; i++)
        {
            UIButton *thisBtn = (UIButton *)[self.specScroll viewWithTag:((i + 1) * 100 + indexI * 1000)];
            if (!thisBtn.enabled)
            {
                thisBtn.enabled = YES;
//                [self.newsSelectedArr removeObject:thisBtn.titleLabel.text];
                thisBtn.layer.borderColor = RGBCOLOR(239, 239, 239).CGColor;
                break;
            }
        }
        
        sender.enabled = NO;
        sender.layer.borderWidth = 1.0f;
        sender.layer.borderColor = [UIColor clearColor].CGColor;
        [self.newsSelectedArr replaceObjectAtIndex:indexI withObject:sender.titleLabel.text];
        
        NSString *specValueString = [self.newsSelectedArr componentsJoinedByString:@"、"];
        self.specValueLB.text = specValueString;

        NSArray *array = _propertyInfo.ecomBPropertyInfoProperties;
        
        BOOL hasSame = NO;
        if (self.newsSelectedArr.count == self.specArr.count)
        {
            for (EcomBProperty *propertyListDic in array)
            {
                NSString *property = [propertyListDic spec];
                for (NSInteger i = 0; i < self.newsSelectedArr.count; i++)
                {
                    NSString *selectedValue = self.newsSelectedArr[i];
                    if (![property containsString:selectedValue])
                    {
                        break;
                    }
                    else
                    {
                        if (i == self.newsSelectedArr.count - 1)
                        {
                            hasSame = YES;
                            self.hasDic = propertyListDic;
                        }
                    }
                }
            }
        }
        
        if (hasSame)
        {
            NSString *specPriceString = [NSString stringWithFormat:@"￥%.2lf",[self.hasDic salePrice]];
            NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:specPriceString];
            [nodeString setAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} range:NSMakeRange(0, 1)];
            self.specPriceLB.attributedText = nodeString;
            self.specStockNumLB.text = [NSString stringWithFormat:@"库存:%@",@([self.hasDic ecomBPropertyStockcount])];
            self.propertyId = [self.hasDic ecomBPropertyPropertyid];
        }
    }
}

-(void)putGoodsIntoCarDidSucceed:(NSArray *)carouseles
{
    NSLog(@"加入购物车成功");
    [TKUIUtil hiddenHUD];
    [UIView animateWithDuration:0.3f animations:^{
        
        self.specView.frame = CGRectMake(0, HEIGHT, WIDTH, (HEIGHT - 64) * 0.7);
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        if (self.isBuyNow)
        {
            ZSTECBShoppingCartViewController *controller = [[ZSTECBShoppingCartViewController alloc] init];
            controller.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:controller animated:YES];
        }
        else
        {
            [TKUIUtil alertInView:self.view withTitle:@"加入购物车成功" withImage:nil];
        }
        
        
    }];
}

#pragma mark - 分享按钮响应事件
- (void)shareAction
{
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    self.shareView.shareString = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！链接：%@", nil),appDisplayName,shareurl];
    self.darkView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.shareView.frame = CGRectMake(0, HEIGHT - 64 - self.shareView.frame.size.height, WIDTH, self.shareView.frame.size.height);
    }];
    
    
    
//    UIActionSheet *shareActionSheet = nil;
//    NSMutableArray *shareNames = [NSMutableArray array];
//    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
//    if (pre.SinaWeiBo) {
//        [shareNames addObject:NSLocalizedString(@"新浪微博",nil)];
//    }
//    if (pre.TWeiBo) {
//        [shareNames addObject:NSLocalizedString(@"腾讯微博",nil)];
//    }
//    if (pre.QQ) {
//        [shareNames addObject:NSLocalizedString(@"QQ空间",nil)];
//    }
//    if (pre.WeiXin) {
//        [shareNames addObject:NSLocalizedString(@"微信好友",nil)];
//        [shareNames addObject:NSLocalizedString(@"微信朋友圈",nil)];
//    }
//    
//    [shareNames addObject:NSLocalizedString(@"短信",nil)];
//    
//    shareActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"分享到", nil)
//                                                   delegate:self
//                                          cancelButtonTitle:nil
//                                     destructiveButtonTitle:nil
//                                          otherButtonTitles:nil];
//    
//    for (NSString * title in shareNames) {
//        [shareActionSheet addButtonWithTitle:title];
//    }
//    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消",nil)];
//    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons-1;
//    shareActionSheet.tag = 101;
//    [shareActionSheet showInView:self.view.window];
//    [shareActionSheet release];
}

- (void)wxShareSucceed
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"微信分享成功", nil) withImage:nil];
}

- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}

- (void)shopingCarBtnAction:(UIButton *)sender
{
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        
        return;
    }
    
    ZSTECBShoppingCartViewController * shoppintCarViewController = [[ZSTECBShoppingCartViewController alloc] init];
    shoppintCarViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:shoppintCarViewController animated:YES];
}

- (void)darkViewDismiss:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.specView.frame = CGRectMake(0, HEIGHT, WIDTH, (HEIGHT - 64) * 0.7);
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.bounds.size.height);
    }];
}

- (void)specViewClose:(PMRepairButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.specView.frame = CGRectMake(0, HEIGHT, WIDTH, (HEIGHT - 64) * 0.7);
        
    } completion:^(BOOL finished) {
        
        self.darkView.hidden = YES;
        
    }];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    UIView *noWaitView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.detailView.bounds.size.height)];
    [noWaitView setTag:108];
    noWaitView.alpha = 1.0f;
    noWaitView.backgroundColor = [UIColor clearColor];
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.detailView.bounds.size.height)];
    [view setBackgroundColor:[UIColor blackColor]];
    [view setAlpha:0.5];
    
    [noWaitView addSubview:view];
    [self.detailView addSubview:noWaitView];
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(0,0,50,50)];
    [self.activityIndicator setCenter:view.center];
    [self.activityIndicator setActivityIndicatorViewStyle:UIActivityIndicatorViewStyleWhite];
    [noWaitView addSubview:self.activityIndicator];
    
    [self.activityIndicator startAnimating];
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.activityIndicator stopAnimating];
    UIView *view = (UIView*)[self.view viewWithTag:108];
    [view removeFromSuperview];
}

#pragma mark - 创建分享 view 
- (void)createShareView:(BOOL)isLogin AndResponse:(NSDictionary *)response AndIconUrl:(NSString *)iconUrl
{
//    self.shareView = [[ZSTShareView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 150)];
//    [self.shareView createUIWithFrame:CGRectMake(0, HEIGHT, WIDTH, 200)];
//    self.shareView.delegate = self;
//    [self.view addSubview:self.shareView];
    
    NSInteger shareCount = 0;
    if (!self.shareView)
    {
        self.shareView = [[ZSTNewShareView alloc] init];
        self.shareView.backgroundColor = [UIColor whiteColor];
        
        if ([ZSTF3Preferences shared].qqKey && [[ZSTF3Preferences shared].qqKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].qqKey isEqualToString:@""] && [QQApi isQQInstalled] && [QQApi isQQSupportApi])
        {
            self.shareView.isHasQQ = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasQQ = NO;
        }
        
        if ([ZSTF3Preferences shared].wxKey && [[ZSTF3Preferences shared].wxKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].wxKey isEqualToString:@""] && [WXApi isWXAppInstalled] && [WXApi isWXAppSupportApi])
        {
            self.shareView.isHasWX = YES;
            shareCount += 2;
        }
        else
        {
            self.shareView.isHasWX = NO;
        }
        
        if ([ZSTF3Preferences shared].weiboKey && [[ZSTF3Preferences shared].weiboKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].weiboKey isEqualToString:@""])
        {
            self.shareView.isHasWeiBo = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasWeiBo = NO;
        }
    
        
        NSInteger heights = shareCount / 4 + 1;
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, heights * 100);
        [self.view addSubview: self.shareView];
    }
    
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】地址：http://ci.pmit.cn/d/%@", nil),appDisplayName,[ZSTF3Preferences shared].ECECCID];
    self.shareView.imgUrlString = iconUrl;
    self.shareView.shareUrlString = shareurl;
    
    if (isLogin)
    {
        self.shareView.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.shareView.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        self.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        self.shareView.shareString = shareString;
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.shareDelegate = self;
        
        self.shareView.isTodayWX = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXin"] integerValue] == 0 ? NO : YES;
        self.shareView.isTodayWXFriedn = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXinFriends"] integerValue] == 0 ? NO : YES;
        self.shareView.isTodayWeiBo = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiBo"] integerValue] == 0 ? NO : YES;
        self.shareView.isTodayQQ = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareQq"] integerValue] == 0 ? NO : YES;
        [self.shareView createShareUI];
        [self.shareView checkIsHasShare];
    }
    else
    {
        self.shareView.isTodayWX = YES;
        self.shareView.isTodayWXFriedn = YES;
        self.shareView.isTodayWeiBo = YES;
        self.shareView.isTodayQQ = YES;
        [self.shareView checkIsHasShare];
        
        self.shareView.shareString = shareString;
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.shareDelegate = self;
        
        [ self.shareView createShareUI];
    }
    
}

#pragma mark 取消分享弹出 view
- (void)dismissShareView
{
    self.darkView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.frame.size.height);
        
    }];
}

- (void)getPointDidSucceed:(NSDictionary *)response
{
    [self createShareView:YES AndResponse:response AndIconUrl:_product.iconUrl];
}

- (void)getPointDidFailed:(NSString *)response
{
    
}

- (void)showDefaultShare
{
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！", nil),appDisplayName];
    //    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://ci.pmit.cn/d/%@",[ZSTF3Preferences shared].ECECCID]];
    NSURL *URL = [NSURL URLWithString:shareurl];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[shareString, URL]
                                      applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToFacebook];
    [self.navigationController presentViewController:activityViewController
                                            animated:YES
                                          completion:^{
                                              // ...
                                          }];
}

- (void)showSNSShare
{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    
    if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        NSString *sharebaseurl = shareurl;
        picker.body = [NSString stringWithFormat:NSLocalizedString(@"#船票商品#，我在%@看到一个不错的商品，你肯定喜欢，赶快来看看吧！地址：%@", nil),appDisplayName,sharebaseurl];
        picker.messageComposeDelegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
    }
    [ZSTLogUtil logUserAction:NSStringFromClass([MFMessageComposeViewController class])];  
}

- (void)wxShareSucceed:(NSNotification *)notification
{
    
    if ([[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""] || [[ZSTF3Preferences shared].UserId isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"分享成功", nil) withImage:nil];
    }
    else
    {
        if ([ZSTWXShareType shareInstance].isWXShare)
        {
            [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                        userId:[ZSTF3Preferences shared].UserId
                                      pointNum:self.wxSharePoint
                                     pointType:6
                                    costAmount:@""
                                          desc:@""];
        }
        else
        {
            [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                        userId:[ZSTF3Preferences shared].UserId
                                      pointNum:self.wxFriendSharePoint
                                     pointType:7
                                    costAmount:@""
                                          desc:@""];
        }
        
    }
    
}

- (void)wxShareFaild:(NSNotification *)notification
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您已取消了分享", nil) withImage:nil];
}


- (void)sendWeiboSuccess:(NSNotification *)notification
{

    if ([[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""] || [[ZSTF3Preferences shared].UserId isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"分享成功", nil) withImage:nil];
    }
    else
    {
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.weiboSharePoint
                                 pointType:5
                                costAmount:@""
                                      desc:@""];
    }
    
}

- (void)sendQQShareSuccess:(NSNotification *)notification
{
    if ([[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""] || [[ZSTF3Preferences shared].UserId isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"分享成功", nil) withImage:nil];
    }
    else
    {
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.weiboSharePoint
                                 pointType:8
                                costAmount:@""
                                      desc:@""];
    }
    
}

- (void)updatePointDidSucceed:(NSDictionary *)response
{
    NSString *message = [[response safeObjectForKey:@"data"] safeObjectForKey:@"PointNum"];
    NSString *string = [NSString stringWithFormat:@"恭喜你获得%@积分",message];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(string, nil) withImage:nil];
}

- (void)updatePointDidFailed:(NSString *)response
{
    NSString *string = response;
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(string, nil) withImage:nil];
}

- (void)sendQQShareFail:(NSNotification *)notification
{
    
}

- (void)sendWeiBoInfoFailured
{
    
}

@end
