//
//  ZSTGoodsShowViewController.h
//  EComB
//
//  Created by pmit on 15/7/14.
//  Copyright (c) 2015年 zhangshangtong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECBHomeCarouselView.h"
#import "ZSTECBPropertyViewController.h"
#import "ZSTEComBPutInCarButton.h"
#import "EGOPhotoViewController.h"
//#import "EGOPhotoGlobal.h"
#import "ZSTLoginViewController.h"
#import <PMRepairButton.h>
#import <ZSTLoginController.h>
#import "ZSTECBProduct.h"

@interface ZSTGoodsShowViewController : UIViewController <ZSTF3EngineECBDelegate,ZSTECBHomeCarouselViewDelegate,ZSTECBHomeCarouselViewDataSource,ZSTLoginControllerDelegate>
{
    EcomBPropertyInfo * _propertyInfo;
}

@property (retain,nonatomic)EComBHomePageInfo *pageInfo;
@property (retain,nonatomic)ZSTECBHomeCarouselView *carouseView;
@property (retain,nonatomic)UILabel *titleLabel;
@property (retain,nonatomic)UILabel *priceLabel;
@property (retain,nonatomic)UILabel *originalPriceLabel;
@property (retain,nonatomic)NSArray *carouseData;
@property (retain,nonatomic)UILabel *freightPriceLabel;
@property (retain,nonatomic)UILabel * slip;
@property (retain,nonatomic) UILabel *pointLabel;

@property (strong, nonatomic) ZSTECBProduct *ecbProduct;


@end
