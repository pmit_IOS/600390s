//
//  ZSTECBProductDetailViewController.h
//  EComB
//
//  Created by LiZhenQu on 14-3-6.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"

@interface ZSTECBProductDetailViewController : UIViewController
@property (assign,nonatomic) NSInteger * productID;
@end
