//
//  ZSTECBProductTableViewController.m
//  EComB
//
//  Created by LiZhenQu on 14-3-3.
//  Copyright (c) 2014年 zhangshangtong. All rights reserved.
//

#import "ZSTECBProductTableViewController.h"
#import "ZSTECBProductTableImageButton.h"
#import "ZSTWebViewController.h"
#import "ZSTECBCategoriesViewController.h"

#import "ZSTEComBMyOrderViewController.h"
#import "ZSTEComBGoodsSearchViewController.h"
#import "ZSTECBShoppingCartViewController.h"
#import "ZSTEComBGoodsShowViewController.h"
#import "ZSTGoodsShowViewController.h"
#import "ZSTDao+EComB.h"
#import "BaseNavgationController.h"

@interface ZSTECBProductTableViewController ()

@end

@implementation ZSTECBProductTableViewController

- (void)moduleApplication:(ZSTModuleApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [application.dao createTableIfNotExistForECBModule];
}

-(void)initData
{
    
    _pagenum = 1;
    _hasMore = YES;
    _btnTextArr = [[NSMutableArray alloc]initWithObjects:@"分类",@"订单",@"购物车",@"搜索", nil];
    
    _carouselView = [[ZSTECBHomeCarouselView alloc]initWithFrame:CGRectMake(0, 0, WIDTH, WIDTH / 2)];
    _carouselView.carouselDataSource = self;
    _carouselView.carouselDelegate = self;
    _carouselView.haveTitle = YES;
    
    _carouseData = nil;
    //轮播图获取
    [self.engine getEcomBCarousel];
    
    //主页数据
    _resultData = [[NSMutableArray alloc]init];
    [self.engine getPage:6 pageIndex:_pagenum];
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initData];
    _headView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, WIDTH , HeightRate(60))];
    _headView.backgroundColor = RGBCOLOR(243, 243, 243);
    

    CGRect rect = self.view.bounds;
    
    _tableView = [[UITableView alloc]initWithFrame:rect style:UITableViewStylePlain];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.allowsSelection = NO;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:_tableView];
    
    [self initBtn];
    
    if (_loadMoreView == nil) {
        _loadMoreView = [[TKLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(50))];
        _loadMoreView.backgroundColor = [UIColor clearColor];
        _loadMoreView.delegate = self;
        
    }
    
}
- (void)getPageDidSucceed:(NSArray *)carouseles hasMore:(BOOL) hasMore
{
    _hasMore = hasMore;
    
    if ([[[UIDevice currentDevice] systemVersion] compare:@"8.2"] != NSOrderedAscending) {
        [self searchEComBListDidSucceed:[EComBHomePageInfo homePageWithArray:carouseles] isLoadMore:NO hasMore:YES isFinish:YES];
    }
    else {
        [self searchEComBListDidSucceed:[EComBHomePageInfo homePageWithArray:carouseles] isLoadMore:NO hasMore:hasMore isFinish:YES];
    }
}
- (void)getPageDidFailed:(int)resultCode
{
    NSLog(@"首页数据获取失败");
    [self searchEcomBListDidFailed:resultCode];
}
- (void)appendData:(NSArray*)dataArray
{
    [self.resultData addObjectsFromArray:dataArray];
    [self.tableView reloadData];
    _pagenum ++;
}
- (void)finishLoading
{
    [_loadMoreView loadMoreScrollViewDataSourceDidFinishedLoading:_tableView];
    _isLoadingMore = NO;
    _isRefreshing = NO;
}
- (void)loadMoreData
{
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    [self.engine getPage:6 pageIndex:_pagenum];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKLoadMoreView *loadMoreView = (TKLoadMoreView *)[cell descendantOrSelfWithClass:[TKLoadMoreView class]];
    if (loadMoreView) {
        if (!_isRefreshing) {
            [_loadMoreView loadMoreTriggerRefresh];
        }
    }
}

-(void)initBtn
{
    for (int i = 0; i < 4; i++) {
        ZSTECBProductTableImageButton * imageBtn = [ZSTECBProductTableImageButton buttonWithType:UIButtonTypeCustom];
//        imageBtn.frame = CGRectMake(i * 77 + 15, 0, 60, 50);
        imageBtn.frame = CGRectMake(i * (WIDTH - 30 ) / 4 + 15, 0, (WIDTH - 30), 50);
        imageBtn.tag = i;
        [imageBtn setImage:ZSTModuleImage(ZSTIMAGE(i+1)) forState:UIControlStateNormal];
        [imageBtn setTitle:[_btnTextArr objectAtIndex:i] forState:UIControlStateNormal];
        UIImage * image = [self imageWithColor:[UIColor lightGrayColor] size:imageBtn.frame.size];
        
        [imageBtn setBackgroundImage:image forState:UIControlStateHighlighted];
        [imageBtn addTarget:self action:@selector(btnClicked:) forControlEvents:UIControlEventTouchUpInside];
        [_headView addSubview:imageBtn];
    }
    
}
- (UIImage *)imageWithColor:(UIColor *)color size:(CGSize)size
{
    // http://stackoverflow.com/questions/1213790/how-to-get-a-color-image-in-iphone-sdk
    
    //Create a context of the appropriate size
    UIGraphicsBeginImageContext(size);
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    //Build a rect of appropriate size at origin 0,0
    CGRect fillRect = CGRectMake(0, 0, size.width, size.height);
    
    //Set the fill color
    CGContextSetFillColorWithColor(currentContext, color.CGColor);
    
    //Fill the color
    CGContextFillRect(currentContext, fillRect);
    
    //Snap the picture and close the context
    UIImage *colorImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return colorImage;
}
-(void)btnClicked:(id)btn
{
    UIButton * button = (UIButton *)btn;
    if (button.tag == 0) {
        NSLog(@"跳转：商品分类");
        
        ZSTECBCategoriesViewController *controller = [[ZSTECBCategoriesViewController alloc] init];
         controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    else if (button.tag == 1) {
        NSLog(@"跳转：我的订单");
        
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
//            ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            [navicontroller release];
            [controller release];
            
            return;
        }
        
        ZSTEComBMyOrderViewController * controller = [[ZSTEComBMyOrderViewController alloc] init];
         controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    else if (button.tag == 2) {
        NSLog(@"跳转：购物车");
        
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
            
//            ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
            
            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            [navicontroller release];
            [controller release];
            
            return;
        }
        
        ZSTECBShoppingCartViewController *controller = [[ZSTECBShoppingCartViewController alloc] init];
         controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    else if (button.tag == 3) {
        NSLog(@"跳转：商品搜索");
        ZSTEComBGoodsSearchViewController * controller = [[ZSTEComBGoodsSearchViewController alloc]init];
         controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
}

- (void)loginFinish
{
    
}

#pragma mark tableDelegate
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    else
        return (_resultData.count +1)/2 + (_hasMore ? 1:0);
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        return HeightRate(160);
    }
    else if(indexPath.row == (_resultData.count + 1)/2)
    {
        if (_hasMore) {
            return 44.0f;
        }
        return 0.1f;
    }
    else
        return HeightRate(220);
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == ([_resultData count] +1)/2 && [_resultData count] > 0) {
        static NSString *newsCellIdentifier = @"loadMoreView";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:newsCellIdentifier];
        
        if (cell==nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:newsCellIdentifier] ;
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.userInteractionEnabled = NO;
            [cell.contentView addSubview:_loadMoreView];
        }
        return cell;
    }
    else if (indexPath.section == 0) {
        static NSString * cellCarouse = @"carousecell";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:cellCarouse];
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellCarouse];
            [cell addSubview:_carouselView];
        }
        return  cell;
    }
    else{
        static NSString * ECBProductTableViewCell = @"ECBProductTableViewCell";
        ZSTECBProductTableImageCell * cell = [tableView dequeueReusableCellWithIdentifier:ECBProductTableViewCell];
        if (!cell) {
            cell = [[ZSTECBProductTableImageCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:ECBProductTableViewCell];
        }
        if (_resultData.count) {
            
            EComBHomePageInfo *left = nil;
            EComBHomePageInfo *right = nil;
            
            if (indexPath.row*2 > self.resultData.count+1) {
                
                left = [self.resultData lastObject];
            } else {
                
                left = [self.resultData objectAtIndex:indexPath.row*2];
            }
            
            if (indexPath.row*2+1 < self.resultData.count)
            {
                right = [self.resultData objectAtIndex:indexPath.row*2+1];
            }
            
            [cell reloadDataWithLeft:left Right:right];
        }
        cell.delegate = self;
        //cell.textLabel.text = [NSString stringWithFormat:@"第%d",indexPath.row];
        cell.leftView.tag = indexPath.row * 2;
        cell.rightView.tag = indexPath.row * 2 + 1;
        return  cell;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return 55.0f;
    }
    else
        return 0.0f;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 1) {
        return _headView;
    }
    else return nil;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma  mark ZSTECBProductTableViewControllerDelegate

-(void)ZSTECBProductTableImageCellDidSelect:(NSInteger)row
{
//    ZSTEComBGoodsShowViewController * showViewController = [[ZSTEComBGoodsShowViewController alloc]init];
    ZSTGoodsShowViewController *showViewController = [[ZSTGoodsShowViewController alloc] init];
    showViewController.pageInfo = [_resultData objectAtIndex:row];
    showViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:showViewController animated:YES];
    [showViewController release];
}
- (void)searchEComBListDidSucceed:(NSArray *)resultList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish
{
    [self finishLoading];
    [TKUIUtil hiddenHUD];
    _hasMore = hasMore;
    //    [self displayStyle];
    [self appendData:resultList];
}

- (void)searchEcomBListDidFailed:(int)response
{
    [TKUIUtil hiddenHUD];
    [self finishLoading];
    _hasMore = NO;
    [_tableView reloadData];
}

#pragma mark HHLoadMoreViewDelegate Methods
- (void)loadMoreDidTriggerRefresh:(TKLoadMoreView*)view
{
    if (_hasMore && !_isLoadingMore) {
        _isLoadingMore = YES;
        [self loadMoreData];
    }
}

- (BOOL)loadMoreDataSourceIsLoading:(TKLoadMoreView*)view
{
    return _isLoadingMore;
}



#pragma mark ZSTF3EngineEComBDelegate

- (void)getEcomBCarouselDidSucceed:(NSArray *)carouseles
{
    
    NSLog(@"===carouseles====>%@",carouseles);
    
    [_carouseData release];
    _carouseData = nil;
    _carouseData = [ [EcomBCarouseInfo ecomBCarouseWithArray:carouseles] retain];
    if (_carouseData.count>0) {
        [_carouselView reloadData];
    }
}
- (void)getEcomBCarouselDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
}


#pragma mark - ZSTEComBCarouselViewDataSource

- (NSInteger)numberOfViewsInCarouselView:(ZSTECBHomeCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomBCarouseInfo *)carouselView:(ZSTECBHomeCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{

    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }      
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate

- (void)carouselView:(ZSTECBHomeCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    //NSString *urlPath = [[_carouseData objectAtIndex:index] ecomBCarouseInfoLinkurl];
//    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
//        return;
//    }
    EComBHomePageInfo * pageInfo = [[EComBHomePageInfo alloc]init];
    pageInfo.ecombHomeProductid = [NSString stringWithFormat:@"%d",[[_carouseData objectAtIndex:index] ecomBCarouseInfoProductid]];
    
//    ZSTEComBGoodsShowViewController * showViewcontroller = [[ZSTEComBGoodsShowViewController alloc]init];
    ZSTGoodsShowViewController *showViewcontroller = [[ZSTGoodsShowViewController alloc] init];
    showViewcontroller.pageInfo = pageInfo;
    showViewcontroller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:showViewcontroller animated:YES];
    [showViewcontroller release];
    
//    
//    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
//    [webViewController setURL:urlPath];
//    webViewController.hidesBottomBarWhenPushed = YES;
//    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
//    
//    [self.navigationController pushViewController:webViewController animated:NO];
//    [webViewController release];
}
-(void)dealloc
{
    _carouselView.carouselDelegate = nil;
    _carouselView.carouselDataSource = nil;
    [_headView release];
    [_tableView release];
    [_btnTextArr release];
    [_carouselView release];
    [_carouseData release];
    [_resultData release];
    [super dealloc];
}
@end
