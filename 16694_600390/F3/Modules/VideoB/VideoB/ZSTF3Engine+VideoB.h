//
//  ZSTF3Engine+VideoB.h
//  VideoB
//
//  Created by lizhenqu on 13-3-27.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine.h"
#import "ZSTGlobal+VideoB.h"

@protocol ZSTF3EngineVideoBDelegate <ZSTF3EngineDelegate>

@optional

- (void)getVideoBListDidSucceed:(NSArray *)videoList hasMore:(BOOL)hasMore;
- (void)getVideoBListDidFailed:(int)resultCode;

@end

@interface ZSTF3Engine (VideoB)

/*
 *获取新闻列表
 */
- (void)getVideoListWithDelegete:(id<ZSTF3EngineVideoBDelegate>)delegate
                       videoType:(int)videoType
                        pageSize:(int)pageSize
                         sinceId:(NSString *)sinceId
                        pageIdex:(int)pageindex
                       orderType:(NSString *)orderType;

- (void)setVideoActionWithDelegete:(id<ZSTF3EngineVideoBDelegate>)delegate
                            opType:(int)opType
                           VideoId:(NSString *)VideoId;

@end
