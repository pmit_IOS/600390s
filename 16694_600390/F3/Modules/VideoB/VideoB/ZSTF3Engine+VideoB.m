//
//  ZSTF3Engine+VideoB.m
//  VideoB
//
//  Created by lizhenqu on 13-3-27.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import "ZSTF3Engine+VideoB.h"
#import "ZSTCommunicator.h"
#import "ZSTF3Preferences.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "JSON.h"
#import "ZSTVideoBVO.h"
#import "ZSTDao+VideoB.h"


@implementation ZSTF3Engine (VideoB)

- (void)getVideoListWithDelegete:(id<ZSTF3EngineVideoBDelegate>)delegate
                       videoType:(int)videoType
                        pageSize:(int)pageSize
                         sinceId:(NSString *)sinceId
                        pageIdex:(int)pageindex
                       orderType:(NSString *)orderType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:videoType] forKey:@"videotype"];
    [params setObject:[NSNumber numberWithInt:pageSize] forKey:@"size"];
    [params setObject:sinceId forKey:@"sinceid"];
    [params setObject:[NSNumber numberWithInt:pageindex] forKey:@"pageindex"];
    [params setObject:orderType forKey:@"ordertype"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetVideoList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getVideoResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
}

- (void)getVideoResponse:(ZSTResponse *)response userInfo:(id)userInfo 
{
    if (response.resultCode == ZSTResultCode_OK) {
        id videos = [ZSTVideoBVO videoListWithdic:response.jsonResponse];
        BOOL hasmore = [[response.jsonResponse objectForKey:@"hasmore"] boolValue];
        if (![videos isKindOfClass:[NSArray class]]) {
            videos = [NSArray array];
        }
        
//        [self.dao deleteAllVideos];

        //入库
        [ZSTSqlManager beginTransaction];
        for (ZSTVideoBVO *video in videos){

            if (![self.dao videoExist:video.videoid]) {
              
                [self.dao addVideoID:video.videoid
                           videoName:video.videoname
                           videoMemo:video.videomemo
                           videoType:video.videotype
                             iconUrl:video.iconurl
                             fileUrl:video.fileurl
                            duration:video.duration
                            orderNum:video.ordernum
                       favoriteCount:video.favoritecount
                        supportCount:video.supportcount
                        trampleCount:video.tramplecount
                         description:video.description
                             addTime:video.addtime];
            }
        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getVideoBListDidSucceed:hasMore:)]) {
            
            [self.delegate getVideoBListDidSucceed:videos hasMore:hasmore];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getVideoBListDidFailed:)]) {
            [self.delegate getVideoBListDidFailed:response.resultCode];
        }
    }
}

- (void)setVideoActionWithDelegete:(id<ZSTF3EngineVideoBDelegate>)delegate
                            opType:(int)opType
                           VideoId:(NSString *)VideoId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[NSNumber numberWithInt:opType] forKey:@"optype"];
    [params setObject:VideoId forKey:@"videoid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[SetVideoAction stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:nil
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
    
}

@end
