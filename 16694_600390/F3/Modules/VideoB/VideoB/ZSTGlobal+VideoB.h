//
//  ZSTDao+VideoB.h
//  VideoB
//
//  Created by lizhenqu on 13-3-27.
//  Copyright (c) 2013年 zhangshangtong. All rights reserved.
//

#import <Foundation/Foundation.h>

#define VIDEO_PAGE_SIZE 10

#define SortType_Desc @"Desc"               //向新取
#define SortType_Asc @"Asc"                 //向旧取


//////////////////////////////////////////////////////////////////////////////////////////
#define VideoBBaseURL @"http://mod.pmit.cn/VideoB"
#define GetVideoList VideoBBaseURL @"/GetVideoList"
#define SetVideoAction VideoBBaseURL @"/SetVideoAction"


