//
//  ZSTGoodDelegate.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface ZSTGoodDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *goodArr;

@end
