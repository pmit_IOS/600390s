//
//  ZSTTopicContentCell.h
//  SNSA
//
//  Created by pmit on 15/10/14.
//
//

#import <UIKit/UIKit.h>
#import "TQRichTextView.h"
#import "ZSTSNSAMessage.h"
#import "CDTDisplayView.h"

@protocol ZSTTopicContentCellDelegate <NSObject>

@optional
- (void)goToLookBigPic:(NSInteger)tapTag Message:(ZSTSNSAMessage *)message;
- (void)jumpToDetail:(ZSTSNSAMessage *)chatContentDic;

@end

@interface ZSTTopicContentCell : UITableViewCell

//@property (strong,nonatomic) TQRichTextView *chatroomContentLB;
@property (strong,nonatomic) CDTDisplayView *chatroomContentLB;
@property (strong,nonatomic) UIView *chatroomContentIV;
@property (weak,nonatomic) id<ZSTTopicContentCellDelegate> contentCellDelegate;
@property (assign,nonatomic) BOOL isFromTopicDetail;
@property (assign,nonatomic) NSInteger cellSection;
@property (strong,nonatomic) ZSTSNSAMessage *chatContentDic;


- (void)createUI;
- (void)setCellData:(ZSTSNSAMessage *)chatContentDic;

@end
