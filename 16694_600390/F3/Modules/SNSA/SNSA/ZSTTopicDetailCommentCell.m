//
//  ZSTTopicDetailCommentCell.m
//  SNSA
//
//  Created by pmit on 15/10/13.
//
//

#import "ZSTTopicDetailCommentCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "CoreTextData.h"
#import "CTFrameParser.h"
#import "CTFrameParserConfig.h"

@implementation ZSTTopicDetailCommentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.headerIV)
    {
        self.headerIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 40, 40)];
        self.headerIV.layer.masksToBounds = YES;
        self.headerIV.layer.cornerRadius = 20;
        self.headerIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
        [self.contentView addSubview:self.headerIV];
        
        self.nameLB = [[UILabel alloc] initWithFrame:CGRectMake(60, 10, WIDTH - 60 - 15, 20)];
        self.nameLB.textAlignment = NSTextAlignmentLeft;
        self.nameLB.font = [UIFont systemFontOfSize:14.0f];
        self.nameLB.textColor = RGBACOLOR(248, 145, 70, 1);
        [self.contentView addSubview:self.nameLB];
        
        self.timeLB = [[UILabel alloc] initWithFrame:CGRectMake(61, 30, WIDTH - 61 - 15, 20)];
        self.timeLB.font = [UIFont systemFontOfSize:12.0f];
        self.timeLB.textColor = RGBACOLOR(159, 159, 159, 1);
        [self.contentView addSubview:self.timeLB];
        
//        self.commentContentLB = [[UILabel alloc] initWithFrame:CGRectMake(60, 55, WIDTH - 60 - 15, 10)];
        self.commentContentLB = [[CDTDisplayView alloc] initWithFrame:CGRectMake(60, 55, WIDTH - 60 - 15, 10)];
        self.commentContentLB.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:self.commentContentLB];
        
        self.line = [CALayer layer];
        self.line.frame = CGRectMake(0, 74, WIDTH, 1);
        self.line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [self.contentView.layer addSublayer:self.line];
    }
}

- (void)setCellData:(ZSTSNSAMessageComments *)commentArr
{
    [self.headerIV setImageWithURL:[NSURL URLWithString:commentArr.commentHeaderPhoto] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    NSString *showTimeString = [self makeTimeCount:commentArr.addTime];
    self.timeLB.text = showTimeString;
    
    NSString *firstName = @"";
    NSString *secondName = @"";
    
    if ([commentArr.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue])
    {
        firstName = @"我";
    }
    else
    {
        firstName = commentArr.uName;
    }
    
    if ([commentArr.toUserId integerValue] == [[ZSTF3Preferences shared].UID integerValue])
    {
        secondName = @"我";
    }
    else
    {
        secondName = commentArr.toUsername;
    }
    
    if ([self validateMobile:commentArr.commentHeaderPhoto] || [commentArr.uName isEqualToString:@""])
    {
        self.nameLB.text = @"佚名";
    }
    else
    {
        self.nameLB.text = firstName;
    }
    
    
    NSString *commentsString = commentArr.commentContent;
//    NSMutableString *accountCommentsString = [NSMutableString string];
    NSString *accountCommentsString = @"";
    
    if (commentArr.parentId != 0)
    {
        accountCommentsString = [NSString stringWithFormat:@"回复%@:%@",secondName,commentsString];
    }
    else
    {
        accountCommentsString = commentsString;
    }
    
    CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
    config.width = self.commentContentLB.width;
    config.textColor = [UIColor blackColor];
    
    CTFrameParser *parser = [[CTFrameParser alloc] init];
    parser.isDetailComments = YES;
    CoreTextData *data = [parser parseTemplateFileContent:accountCommentsString config:config UserMsisdn:commentArr.msisdn ToUserMsisdn:commentArr.toMsisdn];
    
    self.commentContentLB.data = data;
    self.commentContentLB.height = data.height;
    self.commentContentLB.frame = CGRectMake(self.commentContentLB.frame.origin.x, self.commentContentLB.frame.origin.y, self.commentContentLB.bounds.size.width, data.height);
    [self.commentContentLB setNeedsDisplay];
    
    self.commentContentLB.frame = CGRectMake(self.commentContentLB.frame.origin.x, self.commentContentLB.frame.origin.y, WIDTH - 60 - 15, data.height);
    self.line.frame = CGRectMake(self.line.frame.origin.y, 60 + data.height + 5, self.line.bounds.size.width, self.line.bounds.size.height);
    
}

- (NSString *)makeTimeCount:(NSInteger)timeCount
{
    NSString *timeType = @"";
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSDate *now = [NSDate date];
    
    NSTimeInterval delta = [now timeIntervalSince1970] - timeCount;
    
    if (delta <= 60)
    {
        timeType = @"刚刚";
    }
    else if (delta <= 3600 && delta > 60)
    {
        timeType = [NSString stringWithFormat:@"%.0f分钟前",delta / 60];
    }
    else if (delta <= 24 * 60 * 60 && delta > 3600)
    {
        timeType = [NSString stringWithFormat:@"%.0f小时前",delta / 3600];
    }else
    {
        fmt.dateFormat = @"MM-dd HH:mm";
        NSTimeInterval time = timeCount;//因为时差问题要加8小时 == 28800 sec
        NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
        timeType = [fmt stringFromDate: detaildate];
    }
    
    return timeType;
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[2378]|7[0-9]|4[0-9])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
