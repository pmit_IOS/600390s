//
//  ZSTChatroomTopicCGCell.h
//  SNSA
//
//  Created by pmit on 15/9/19.
//
//

#import <UIKit/UIKit.h>
#import "ZSTSNSAMessage.h"
#import "ZSTSNSAMessageComments.h"
#import <TTTAttributedLabel.h>

@protocol ZSTChatroomTopicCGCellDelegate <NSObject>

- (void)sendCommentToComments:(ZSTSNSAMessage *)message Comments:(ZSTSNSAMessageComments *)comments;
- (void)moveToMenDetail:(ZSTSNSAThumShare *)thumbShare;

@end

@interface ZSTChatroomTopicCGCell : UITableViewCell

@property (strong,nonatomic) UIView *CGView;
@property (strong,nonatomic) UIImageView *loveIV;
@property (strong,nonatomic) UIView *loveView;
@property (strong,nonatomic) UIView *commentsView;
@property (strong,nonatomic) UIImageView *chatArrowIV;
@property (strong,nonatomic) ZSTSNSAMessage *thisMessage;
@property (strong,nonatomic) CALayer *line;
@property (weak,nonatomic) id<ZSTChatroomTopicCGCellDelegate> CGcellDelegate;
@property (strong,nonatomic) TTTAttributedLabel *goodMenLB;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessage *)message;

@end
