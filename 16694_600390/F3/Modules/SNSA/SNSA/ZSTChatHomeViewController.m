//
//  ZSTChatHomeViewController.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatHomeViewController.h"
#import <ZSTUtils.h>
#import <PMRepairButton.h>
#import "ZSTChatroomMyCircleCell.h"
#import "ZSTMyChatroomViewController.h"
#import "ZSTChatroomWaitCareViewController.h"
#import "ZSTChatroomMenViewController.h"
#import <ZSTF3ClientAppDelegate.h>
#import <BaseNavgationController.h>

@interface ZSTChatHomeViewController () <UITableViewDataSource,UITableViewDelegate,ZSTYouYunEngineDelegate,ZSTChatroomMyCircleCellDelegate,ZSTLoginControllerDelegate>

@property (assign,nonatomic) BOOL isFirst;
@property (strong,nonatomic) UITableView *myCircleTableView;
@property (strong,nonatomic) NSMutableArray *myCircleArr;

@end

@implementation ZSTChatHomeViewController

static NSString *const circlrCell = @"circleCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.myCircleArr = [NSMutableArray array];
    if (self.navigationController.childViewControllers.count == 1)
    {
        self.isFirst = YES;
    }
    else
    {
        self.isFirst = NO;
        self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    }
    
    PMRepairButton *addCircleBtn = [[PMRepairButton alloc] init];
    addCircleBtn.frame = CGRectMake(0, 0, 30, 30);
    [addCircleBtn setImage:[UIImage imageNamed:@"chatroom_addCircle.png"] forState:UIControlStateNormal];
    [addCircleBtn addTarget:self action:@selector(goToMoreCircle:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:addCircleBtn];
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"企业圈子", nil)];
    
    self.view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
//    [self.engine getUserByUID:[ZSTF3Preferences shared].UID];
    self.engine.moduleType = self.moduleType;
    
    [self buildMyCircleTableView];
    
    [self.engine getMyCircles:[NSString stringWithFormat:@"%@:%@",@"",@""]];
    [self.engine getUserInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (![ZSTF3Preferences shared].loginMsisdn || [[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""])
    {
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            
        }];

    }
    else
    {
        if (self.isNeedRefresh)
        {
            //        [self.engine getCircles:[NSString stringWithFormat:@"%@:%@",@"",@""]];
            [self.engine getMyCircles:[NSString stringWithFormat:@"%@:%@",@"",@""]];
            self.isNeedRefresh = NO;
        }
        
        if (((ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate).hasChange)
        {
            [self.engine getUserInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
            //        [self.engine getCircles:[NSString stringWithFormat:@"%@:%@",@"",@""]];
            [self.engine getMyCircles:[NSString stringWithFormat:@"%@:%@",@"",@""]];
            ((ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate).hasChange = NO;
        }
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildMyCircleTableView
{
    if (self.isFirst)
    {
        self.myCircleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64  - 49) style:UITableViewStylePlain];
    }
    else
    {
        self.myCircleTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    }
    
    self.myCircleTableView.delegate = self;
    self.myCircleTableView.dataSource = self;
    [self.myCircleTableView registerClass:[ZSTChatroomMyCircleCell class] forCellReuseIdentifier:circlrCell];
    self.myCircleTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.myCircleTableView setSeparatorColor:[UIColor whiteColor]];
    [self.view addSubview:self.myCircleTableView];
    
}

- (void)goToMoreCircle:(PMRepairButton *)sender
{
    ZSTChatroomWaitCareViewController *waitCareVC = [[ZSTChatroomWaitCareViewController alloc] init];
    waitCareVC.homeVC = self;
    waitCareVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:waitCareVC animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.myCircleArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTMyChatroomViewController *myChatroom = [[ZSTMyChatroomViewController alloc] init];
    NSDictionary *circleDic = [self.myCircleArr objectAtIndex:indexPath.row];
    myChatroom.chatroomTitle = [circleDic safeObjectForKey:@"CName"];
    myChatroom.circleId = [circleDic safeObjectForKey:@"CID"];
    myChatroom.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myChatroom animated:YES];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomMyCircleCell *cell = [tableView dequeueReusableCellWithIdentifier:circlrCell];
    cell.circleCellDelegate = self;
    [cell createUI];
    [cell setCellData:self.myCircleArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)goToMember:(NSDictionary *)circleDic
{
    ZSTChatroomMenViewController *menVC = [[ZSTChatroomMenViewController alloc] init];
    menVC.homeVC = self;
    menVC.circleDic = circleDic;
    menVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:menVC animated:YES];
}

- (void)getUserInfoDidSucceed:(NSDictionary *)response
{
    [ZSTF3Preferences shared].personIcon = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Icon"];
    [ZSTF3Preferences shared].personName = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Name"];
}

- (void)getUserInfoFailured
{
    
}

- (void)getMyCirclesDidSuccess:(NSDictionary *)response
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSInteger isAdmin = [[response safeObjectForKey:@"IsAdmin"] integerValue];
    [ud setObject:@(isAdmin) forKey:@"isAdmin"];
    [ud synchronize];
    
    NSArray *allCircles = [response safeObjectForKey:@"Info"];
    self.myCircleArr = [NSMutableArray array];
    for (NSDictionary *oneCirclesDic in allCircles)
    {
        if ([[oneCirclesDic safeObjectForKey:@"AuditStatus"] integerValue] == 1)
        {
            [self.myCircleArr addObject:oneCirclesDic];
        }
    }
    
    [self.myCircleTableView reloadData];
}

- (void)getMyCirclesDidFailure:(NSString *)resultString
{
    
}

#pragma mark - 不登录
- (void)loginDidCancel
{
    NSLog(@"%@",@(self.navigationController.viewControllers.count));
    if (self.navigationController.viewControllers.count == 1)
    {
        ZSTF3ClientAppDelegate *app = (ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate;
        [((UITabBarController *)app.window.rootViewController) setSelectedIndex:0];
    }
    else
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}


@end
