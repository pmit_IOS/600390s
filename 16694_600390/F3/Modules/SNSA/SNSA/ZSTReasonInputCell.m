//
//  ZSTReasonInputCell.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTReasonInputCell.h"

@implementation ZSTReasonInputCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.inputTV)
    {
        self.inputTV = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
        self.inputTV.textColor = RGBACOLOR(56, 56, 56, 1);
        self.inputTV.textAlignment = NSTextAlignmentLeft;
        self.inputTV.delegate = self;
        self.inputTV.returnKeyType = UIReturnKeyDone;
        self.inputTV.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.inputTV];
        
        self.backUpLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 4, self.inputTV.bounds.size.width - 30, 20)];
        self.backUpLB.text = @"备注请在此输入...";
        self.backUpLB.font = [UIFont systemFontOfSize:12.0f];
        self.backUpLB.textColor = RGBACOLOR(204, 204, 204, 1);
        [self.inputTV addSubview:self.backUpLB];
    }
}

-(void)textViewDidChange:(UITextView *)textView
{
    //    self.examineText =  textView.text;
    if (textView.text.length == 0) {
        
        self.backUpLB.hidden = NO;
        
    }else{
        
        self.backUpLB.hidden = YES;
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    
    if ([text isEqualToString:@"\n"]) {
        
        [textView resignFirstResponder];
        return NO;
    }
    
    return YES;
}

@end
