//
//  ZSTBigPicDelegate.h
//  SNSA
//
//  Created by pmit on 15/10/13.
//
//

#import <Foundation/Foundation.h>


@interface ZSTBigPicDelegate : NSObject <UIScrollViewDelegate>

+ (ZSTBigPicDelegate *)defaultDelegate;

@end
