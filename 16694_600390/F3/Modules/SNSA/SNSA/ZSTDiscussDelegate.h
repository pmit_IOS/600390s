//
//  ZSTDiscussDelegate.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ZSTSNSAMessageComments.h"

@protocol ZSTDiscussDelegateDelegate <NSObject>

- (void)commentToComments:(ZSTSNSAMessageComments *)comments;

@end

@interface ZSTDiscussDelegate : NSObject <UITableViewDelegate,UITableViewDataSource>

@property (strong,nonatomic) NSArray *commentArr;
@property (weak,nonatomic) id<ZSTDiscussDelegateDelegate> discussFunDelegate;

@end
