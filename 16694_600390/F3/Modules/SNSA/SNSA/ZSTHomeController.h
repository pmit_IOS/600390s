//
//  YouYunViewController.h
//  YouYun
//
//  Created by luobin on 5/28/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModuleBaseViewController.h"
#import "ZSTGroupTableViewCell.h"
#import "ZSTLoginViewController.h"
#import <ZSTLoginController.h>
@interface ZSTHomeController : ZSTModuleBaseViewController <UITableViewDelegate,UITableViewDataSource,ZSTLoginControllerDelegate>
{
    EGORefreshTableHeaderView *_refreshHeaderView;
    UITableView *_circleTableView;
    BOOL _isRefreshing;
    NSInteger _row;
    BOOL _canDel;
}

@end
