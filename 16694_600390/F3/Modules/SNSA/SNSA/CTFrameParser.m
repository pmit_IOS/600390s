//
//  CTFrameParser.m
//  CoreTextDemo
//
//  Created by pmit on 15/10/19.
//  Copyright © 2015年 com.9588.f3. All rights reserved.
//

#import "CTFrameParser.h"
#import "CoreTextLinkData.h"


@implementation CTFrameParser

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    
    return self;
}


static CGFloat ascentCallback(void *ref){
    return [(NSNumber*)[(__bridge NSDictionary*)ref objectForKey:@"height"] floatValue];
}

static CGFloat descentCallback(void *ref){
    return 0;
}

static CGFloat widthCallback(void* ref){
    return [(NSNumber*)[(__bridge NSDictionary*)ref objectForKey:@"width"] floatValue];
}

- (NSDictionary *)attributesWithConfig:(CTFrameParserConfig *)config {
    CGFloat fontSize = config.fontSize;
    CTFontRef fontRef = CTFontCreateWithName((CFStringRef)@"ArialMT", fontSize, NULL);
    CGFloat lineSpacing = config.lineSpace;
    const CFIndex kNumberOfSettings = 3;
    CTParagraphStyleSetting theSettings[kNumberOfSettings] = {
        { kCTParagraphStyleSpecifierLineSpacingAdjustment, sizeof(CGFloat), &lineSpacing },
        { kCTParagraphStyleSpecifierMaximumLineSpacing, sizeof(CGFloat), &lineSpacing },
        { kCTParagraphStyleSpecifierMinimumLineSpacing, sizeof(CGFloat), &lineSpacing }
    };
    
    CTParagraphStyleRef theParagraphRef = CTParagraphStyleCreate(theSettings, kNumberOfSettings);
    
    UIColor * textColor = config.textColor;
    
    NSMutableDictionary * dict = [NSMutableDictionary dictionary];
    dict[(id)kCTForegroundColorAttributeName] = (id)textColor.CGColor;
    dict[(id)kCTFontAttributeName] = (__bridge id)fontRef;
    dict[(id)kCTParagraphStyleAttributeName] = (__bridge id)theParagraphRef;
    
    CFRelease(theParagraphRef);
    CFRelease(fontRef);
    return dict;
}

- (CoreTextData *)parseContent:(NSString *)content config:(CTFrameParserConfig*)config {
    NSDictionary *attributes = [self attributesWithConfig:config];
    NSAttributedString *contentString =
    [[NSAttributedString alloc] initWithString:content
                                    attributes:attributes];
    
    // 创建 CTFramesetterRef 实例
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)contentString);
    
    // 获得要绘制的区域的高度
    CGSize restrictSize = CGSizeMake(config.width, CGFLOAT_MAX);
    CGSize coreTextSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0,0), nil, restrictSize, nil);
    CGFloat textHeight = coreTextSize.height;
    
    // 生成 CTFrameRef 实例
    CTFrameRef frame = [self createFrameWithFramesetter:framesetter config:config height:textHeight];
    
    // 将生成好的 CTFrameRef 实例和计算好的绘制高度保存到 CoreTextData 实例中，最后返回 CoreTextData 实例
    CoreTextData *data = [[CoreTextData alloc] init];
    data.ctFrame = frame;
    data.height = textHeight;
    
    // 释放内存
    CFRelease(frame);
    CFRelease(framesetter);
    return data;
}

- (CTFrameRef)createFrameWithFramesetter:(CTFramesetterRef)framesetter
                                  config:(CTFrameParserConfig *)config
                                  height:(CGFloat)height {
    
    CGMutablePathRef path = CGPathCreateMutable();
    CGPathAddRect(path, NULL, CGRectMake(0, 0, config.width, height));
    
    CTFrameRef frame = CTFramesetterCreateFrame(framesetter, CFRangeMake(0, 0), path, NULL);
    CFRelease(path);
    return frame;
}

// 方法一
- (CoreTextData *)parseTemplateFileContent:(NSString *)content config:(CTFrameParserConfig *)config UserMsisdn:(NSString *)userMsisdn ToUserMsisdn:(NSString *)toUserMsisdn
{
    NSMutableArray *imageArray = [NSMutableArray array];
    NSMutableArray *linkArray = [NSMutableArray array];
    NSAttributedString *nodeString = [self loadTempLateFileFace:content config:config ImgArr:imageArray UserMsisdn:userMsisdn ToUserMsisdn:toUserMsisdn LinkArr:linkArray];
    CoreTextData *data = [self parseAttributedContent:nodeString config:config];
    data.imageArray = imageArray;
    data.linkArray = linkArray;
    return data;
}

- (CoreTextData *)parseTemplateTopicContent:(NSString *)content config:(CTFrameParserConfig *)config
{
    NSMutableArray *imageArray = [NSMutableArray array];
    NSAttributedString *nodeString = [self loadTempTopicFileFace:content config:config ImgArr:imageArray];
    CoreTextData *data = [self parseAttributedContent:nodeString config:config];
    data.imageArray = imageArray;
    return data;
}

- (NSAttributedString *)loadTempTopicFileFace:(NSString *)content config:(CTFrameParserConfig*)config ImgArr:(NSMutableArray *)imageArray
{
    NSMutableAttributedString *result = [[NSMutableAttributedString alloc] init];
    NSString *leftM = @"[";
    
    NSString *rightM = @"]";
    
    BOOL isInFace = NO;
    
    NSInteger startIndex = 0;
    
    NSInteger endIndex = 0;
    
    for (NSInteger j = 0; j < content.length; j++)
    {
        NSString *oneString = [content substringWithRange:NSMakeRange(j, 1)];
        if ([oneString isEqualToString:leftM])
        {
            if (j + 1 < content.length)
            {
                NSString *fString = [content substringWithRange:NSMakeRange(j + 1, 1)];
                if ([fString isEqualToString:@"f"])
                {
                    isInFace = YES;
                    startIndex = j;
                }
                else
                {
                    isInFace = NO;
                }
            }
            else
            {
                NSAttributedString *as = [self parseAttributedContentFromString:oneString config:config Color:RGBA(56, 56, 56, 1) FontSize:14.0f];
                [result appendAttributedString:as];
            }
        }
        
        else if ([oneString isEqualToString:rightM])
        {
            if (isInFace)
            {
                endIndex = j;
                isInFace = NO;
                CoreTextImageData *imageData = [[CoreTextImageData alloc] init];
                NSString *faceString = [content substringWithRange:NSMakeRange(startIndex + 1, 7)];
                imageData.name = faceString;
                imageData.position = [result length];
                [imageArray addObject:imageData];
                // 创建空白占位符，并且设置它的 CTRunDelegate 信息
                NSDictionary* imgAttr = [NSDictionary dictionaryWithObjectsAndKeys: //2
                                         
                                         @18, @"width",
                                         
                                         @18, @"height",
                                         
                                         nil];
                
                NSAttributedString *as = [self parseImageDataFromNSDictionary:imgAttr config:config];
                [result appendAttributedString:as];
            }
            else
            {
                NSAttributedString *as = [self parseAttributedContentFromString:oneString config:config Color:RGBA(56, 56, 56, 1) FontSize:14.0f];
                [result appendAttributedString:as];
            }
        }
        else
        {
            if (isInFace)
            {
                continue;
            }
            else
            {
                NSAttributedString *as = [self parseAttributedContentFromString:oneString config:config Color:RGBA(56, 56, 56, 1) FontSize:14.0f];
                [result appendAttributedString:as];
            }
        }
    }
    return result;
}


- (NSAttributedString *)loadTempLateFileFace:(NSString *)content config:(CTFrameParserConfig*)config ImgArr:(NSMutableArray *)imageArray UserMsisdn:(NSString *)userMsisdn ToUserMsisdn:(NSString *)toUserMsisdn LinkArr:(NSMutableArray *)linkArray
{
    NSMutableAttributedString *result = [[NSMutableAttributedString alloc] init];
    NSString *leftM = @"[";
    
    NSString *rightM = @"]";
    
    BOOL isInFace = NO;
    
    NSInteger startIndex = 0;
    
    NSInteger endIndex = 0;
    
    for (NSInteger j = 0; j < content.length; j++)
    {
        NSString *oneString = [content substringWithRange:NSMakeRange(j, 1)];
        if ([oneString isEqualToString:leftM])
        {
            if (j + 1 < content.length)
            {
                NSString *fString = [content substringWithRange:NSMakeRange(j + 1, 1)];
                if ([fString isEqualToString:@"f"])
                {
                    isInFace = YES;
                    startIndex = j;
                }
                else
                {
                    isInFace = NO;
                }
            }
            else
            {
                NSAttributedString *as = [self parseAttributedContentFromString:oneString config:config Color:RGBA(159, 159, 159, 1) FontSize:13.0f];
                [result appendAttributedString:as];
            }
        }
        else if ([oneString isEqualToString:rightM])
        {
            if (isInFace)
            {
                endIndex = j;
                isInFace = NO;
                CoreTextImageData *imageData = [[CoreTextImageData alloc] init];
                NSString *faceString = [content substringWithRange:NSMakeRange(startIndex + 1, 7)];
                imageData.name = faceString;
                imageData.position = [result length];
                [imageArray addObject:imageData];
                // 创建空白占位符，并且设置它的 CTRunDelegate 信息
                NSDictionary* imgAttr = [NSDictionary dictionaryWithObjectsAndKeys: //2
                                         
                                         @15, @"width",
                                         
                                         @15, @"height",
                                         
                                         nil];
                
                NSAttributedString *as = [self parseImageDataFromNSDictionary:imgAttr config:config];
                [result appendAttributedString:as];
            }
            else
            {
                NSAttributedString *as = [self parseAttributedContentFromString:oneString config:config Color:RGBA(159, 159, 159, 1) FontSize:13.0f];
                [result appendAttributedString:as];
            }
        }
        else
        {
            if (isInFace)
            {
                continue;
            }
            else
            {
                NSAttributedString *as = [self parseAttributedContentFromString:oneString config:config Color:RGBA(159, 159, 159, 1) FontSize:13.0f];
                [result appendAttributedString:as];
            }
        }
    }
    
    if (self.isDetailComments)
    {
        if ([content rangeOfString:@"回复"].length > 0)
        {
            NSInteger firstEnd = [content rangeOfString:@"回复"].location;
            [result setAttributes:@{UITextAttributeTextColor : RGBA(205, 154, 116, 1),NSFontAttributeName:[UIFont systemFontOfSize:13.0f]} range:NSMakeRange(0, firstEnd)];
        }
    }
    else
    {
        if ([content rangeOfString:@"回复"].length > 0)
        {
            NSInteger firstEnd = [content rangeOfString:@"回复"].location;
            [result setAttributes:@{UITextAttributeTextColor : RGBA(205, 154, 116, 1)} range:NSMakeRange(0, firstEnd)];
            CoreTextLinkData *linkData = [[CoreTextLinkData alloc] init];
            linkData.title = [content substringWithRange:NSMakeRange(0, firstEnd)];
            linkData.url = [NSString stringWithFormat:@"http://goToMyDetailId=%@",userMsisdn];
            linkData.range = NSMakeRange(0, firstEnd);
            [linkArray addObject:linkData];
            NSInteger sencondEnd = [content rangeOfString:@":"].location;
            [result setAttributes:@{UITextAttributeTextColor : RGBA(205, 154, 116, 1),NSFontAttributeName:[UIFont systemFontOfSize:13.0f]} range:NSMakeRange(firstEnd + 2, sencondEnd - 2 - firstEnd)];
            CoreTextLinkData *linkData2 = [[CoreTextLinkData alloc] init];
            linkData2.title = [content substringWithRange:NSMakeRange(firstEnd + 2, sencondEnd - 2 - firstEnd)];
            linkData2.url = [NSString stringWithFormat:@"http://goToMyDetailId=%@",toUserMsisdn];
            linkData2.range = NSMakeRange(firstEnd + 2, sencondEnd - 2 - firstEnd);
            [linkArray addObject:linkData2];
        }
        else
        {
            NSInteger firstEnd = [content rangeOfString:@":"].location;
            [result setAttributes:@{UITextAttributeTextColor : RGBA(205, 154, 116, 1),NSFontAttributeName:[UIFont systemFontOfSize:13.0f]} range:NSMakeRange(0, firstEnd)];
            CoreTextLinkData *linkData = [[CoreTextLinkData alloc] init];
            linkData.title = [content substringWithRange:NSMakeRange(0, firstEnd)];
            linkData.url = [NSString stringWithFormat:@"http://goToMyDetailId=%@",userMsisdn];
            linkData.range = NSMakeRange(0, firstEnd);
            [linkArray addObject:linkData];
        }
    }
    
    return result;
}



- (NSAttributedString *)parseImageDataFromNSDictionary:(NSDictionary *)dict
                                                config:(CTFrameParserConfig*)config {
    CTRunDelegateCallbacks callbacks;
    memset(&callbacks, 0, sizeof(CTRunDelegateCallbacks));
    callbacks.version = kCTRunDelegateVersion1;
    callbacks.getAscent = ascentCallback;
    callbacks.getDescent = descentCallback;
    callbacks.getWidth = widthCallback;
    
    CTRunDelegateRef delegate = CTRunDelegateCreate(&callbacks, (__bridge void *)(dict));
    
    // 使用0xFFFC作为空白的占位符
    unichar objectReplacementChar = 0xFFFC;
    NSString * content = [NSString stringWithCharacters:&objectReplacementChar length:1];
    NSDictionary * attributes = [self attributesWithConfig:config];
    NSMutableAttributedString * space = [[NSMutableAttributedString alloc] initWithString:content
                                                                               attributes:attributes];
    CFAttributedStringSetAttribute((CFMutableAttributedStringRef)space, CFRangeMake(0, 1),
                                   kCTRunDelegateAttributeName, delegate);
    CFRelease(delegate);
    return space;
}


// 方法三
- (NSAttributedString *)parseAttributedContentFromString:(NSString *)dictString
                                                  config:(CTFrameParserConfig*)config Color:(UIColor *)color FontSize:(CGFloat)fontSize
{
    NSMutableDictionary *attributes = [[self attributesWithConfig:config] mutableCopy];
    if (color)
    {
        attributes[(id)kCTForegroundColorAttributeName] = (id)color.CGColor;
    }
    
    if (fontSize > 0) {
        CTFontRef fontRef = CTFontCreateWithName((CFStringRef)@"ArialMT", fontSize, NULL);
        attributes[(id)kCTFontAttributeName] = (__bridge id)fontRef;
        CFRelease(fontRef);
    }
    
    return [[NSAttributedString alloc] initWithString:dictString attributes:attributes];
}

// 方法四
+ (UIColor *)colorFromTemplate:(NSString *)name {
    if ([name isEqualToString:@"blue"]) {
        return [UIColor blueColor];
    } else if ([name isEqualToString:@"red"]) {
        return [UIColor redColor];
    } else if ([name isEqualToString:@"black"]) {
        return [UIColor blackColor];
    } else {
        return nil;
    }
}

// 方法五
- (CoreTextData *)parseAttributedContent:(NSAttributedString *)content config:(CTFrameParserConfig*)config {
    // 创建 CTFramesetterRef 实例
    CTFramesetterRef framesetter = CTFramesetterCreateWithAttributedString((CFAttributedStringRef)content);
    
    // 获得要缓制的区域的高度
    CGSize restrictSize = CGSizeMake(config.width, CGFLOAT_MAX);
    CGSize coreTextSize = CTFramesetterSuggestFrameSizeWithConstraints(framesetter, CFRangeMake(0,0), nil, restrictSize, nil);
    CGFloat textHeight = coreTextSize.height;
    
    // 生成 CTFrameRef 实例
    CTFrameRef frame = [self createFrameWithFramesetter:framesetter config:config height:textHeight];
    
    // 将生成好的 CTFrameRef 实例和计算好的缓制高度保存到 CoreTextData 实例中，最后返回 CoreTextData 实例
    CoreTextData *data = [[CoreTextData alloc] init];
    data.ctFrame = frame;
    data.height = textHeight;
    
    // 释放内存
    CFRelease(frame);
    CFRelease(framesetter);
    return data;
}


@end
