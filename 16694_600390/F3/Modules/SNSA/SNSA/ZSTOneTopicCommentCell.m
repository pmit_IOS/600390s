//
//  ZSTOneTopicCommentCell.m
//  SNSA
//
//  Created by pmit on 15/10/22.
//
//

#import "ZSTOneTopicCommentCell.h"
#import "CTFrameParserConfig.h"
#import "CTFrameParser.h"
#import "CoreTextData.h"

@implementation ZSTOneTopicCommentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.commentsBgView)
    {
        self.commentsBgView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30, 20)];
        self.commentsBgView.backgroundColor = RGBA(243, 243, 243, 1);
        [self.contentView addSubview:self.commentsBgView];
        
        self.commentsContentView = [[CDTDisplayView alloc] initWithFrame:CGRectMake(5, 0, self.commentsBgView.bounds.size.width - 5, 20)];
        self.commentsContentView.backgroundColor = RGBA(243, 243, 243, 1);
        [self.commentsBgView addSubview:self.commentsContentView];
        
//        self.line = [CALayer layer];
//        self.line.frame = CGRectMake(10, 0, self.commentsBgView.bounds.size.width - 10, 0.5);
//        self.line.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
//        [self.commentsBgView.layer addSublayer:self.line];
    }
}

- (void)setCellData:(ZSTSNSAMessageComments *)comments
{
    CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
    config.width = WIDTH - 30;
    config.textColor = [UIColor blackColor];
    
    NSString *contentString = @"";
    if (comments.parentId != 0)
    {
        contentString = [NSString stringWithFormat:@"%@回复%@: %@",[comments.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : comments.uName,[comments.toUserId integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : comments.toUsername,comments.commentContent];
    }
    else
    {
        contentString = [NSString stringWithFormat:@"%@: %@",[comments.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : comments.uName,comments.commentContent];
    }
    
    CTFrameParser *parser = [[CTFrameParser alloc] init];
    parser.isDetailComments = NO;
    CoreTextData *data = [parser parseTemplateFileContent:contentString config:config UserMsisdn:comments.msisdn ToUserMsisdn:comments.toMsisdn];
    self.commentsContentView.data = data;
    self.commentsContentView.height = data.height;
    self.commentsContentView.frame = CGRectMake(self.commentsContentView.frame.origin.x, self.commentsContentView.frame.origin.y, self.commentsContentView.bounds.size.width, data.height);
    self.commentsBgView.frame = CGRectMake(self.commentsBgView.frame.origin.x, self.commentsBgView.frame.origin.y, self.commentsBgView.bounds.size.width, data.height);
    [self.commentsContentView setNeedsDisplay];
}

@end
