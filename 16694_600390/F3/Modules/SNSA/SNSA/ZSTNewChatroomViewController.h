//
//  ZSTNewChatroomViewController.h
//  SNSA
//
//  Created by pmit on 15/9/6.
//
//

#import <UIKit/UIKit.h>

@interface ZSTNewChatroomViewController : UIViewController <UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (copy,nonatomic) NSString *chatroomName;
@property (strong,nonatomic) UIImagePickerController *iconPickerController;
@property (copy,nonatomic) NSString *circleId;

@end
