//
//  ZSTChatroomTopicCGCell.m
//  SNSA
//
//  Created by pmit on 15/9/19.
//
//

#import "ZSTChatroomTopicCGCell.h"
#import "TQRichTextView.h"
#import "CDTDisplayView.h"
#import "CTFrameParserConfig.h"
#import "CTFrameParser.h"
#import "CoreTextData.h"

@implementation ZSTChatroomTopicCGCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.CGView)
    {
        self.chatArrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30, 10)];
        [self.chatArrowIV setImage:[UIImage imageNamed:@"chatroom_arrow.png"]];
        [self.contentView addSubview:self.chatArrowIV];
        
        self.CGView = [[UIView alloc] initWithFrame:CGRectMake(15, 5, WIDTH - 30, 80)];
        self.CGView.backgroundColor = RGBA(243, 243, 243, 1);
        [self.contentView addSubview:self.CGView];
        
        self.loveView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.CGView.bounds.size.width, 25)];
        self.loveView.backgroundColor = RGBA(243, 243, 243, 1);
        [self.CGView addSubview:self.loveView];
        
        UIImageView *loveIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 2.5, 15, 15)];
        loveIV.contentMode = UIViewContentModeScaleAspectFit;
        loveIV.image = [UIImage imageNamed:@"chatroom_noGood.png"];
        [self.loveView addSubview:loveIV];
        
        self.goodMenLB = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(loveIV.frame) + 2.5, 2.5, self.loveView.bounds.size.width - CGRectGetMaxX(loveIV.frame) - 2.5, 20)];
        self.goodMenLB.font = [UIFont systemFontOfSize:13];
        self.goodMenLB.textColor = RGBA(205, 154, 116, 1);
        self.goodMenLB.backgroundColor = RGBA(243, 243, 243, 1);
        self.goodMenLB.lineBreakMode = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        self.goodMenLB.numberOfLines = 0;
        self.goodMenLB.linkAttributes = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
        [self.loveView addSubview:self.goodMenLB];
        
        self.commentsView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.loveView.frame), self.CGView.bounds.size.width, 10)];
        self.commentsView.backgroundColor = RGBA(243, 243, 243, 1);
        [self.CGView addSubview:self.commentsView];
        
        for (NSInteger i = 0; i < 3; i++)
        {
//            UILabel *commentsLB = [[UILabel alloc] initWithFrame:CGRectMake(5, i * 20, self.commentsView.bounds.size.width - 10, 20)];
//            commentsLB.font = [UIFont systemFontOfSize:13.0f];
//            commentsLB.numberOfLines = 0;
//            commentsLB.tag = i + 1992;
//            commentsLB.backgroundColor = RGBA(243, 243, 243, 1);
//            [self.commentsView addSubview:commentsLB];
//            commentsLB.userInteractionEnabled = YES;
            CDTDisplayView *displayView = [[CDTDisplayView alloc] initWithFrame:CGRectMake(5, i * 20 + 2.5, self.commentsView.bounds.size.width - 10, 20)];
            displayView.backgroundColor = [UIColor clearColor];
            displayView.tag = i + 1992;
            [self.commentsView addSubview:displayView];
//            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapCommentsToComments:)];
//            [displayView addGestureRecognizer:tap];

            
        }
        
        self.line = [CALayer layer];
        self.line.frame = CGRectMake(10, 0, self.commentsView.bounds.size.width - 10, 0.5);
        self.line.backgroundColor = RGBA(229, 229, 229, 1).CGColor;
        [self.commentsView.layer addSublayer:self.line];
    }
}

- (void)setCellData:(ZSTSNSAMessage *)message
{
    self.thisMessage = message;
    CGFloat thumbHeight = 0;
    
    if (message.thumArr.count == 0)
    {
        self.loveView.hidden = YES;
        self.commentsView.frame = CGRectMake(0, 0, self.CGView.bounds.size.width, 10);
        thumbHeight = 0;
    }
    else
    {
        self.loveView.hidden = NO;
        self.CGView.hidden = NO;
        self.chatArrowIV.hidden = NO;
        NSMutableArray *goodNameArr = [NSMutableArray array];
        
        for (NSInteger i = 0; i < message.thumArr.count; i++)
        {
            ZSTSNSAThumShare *thumbShare = message.thumArr[i];
            NSString *oneName = [thumbShare.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : thumbShare.uname;
            [goodNameArr addObject:oneName];
        }
        
        NSString *showMenNameString = [goodNameArr componentsJoinedByString:@","];
        
        [self.goodMenLB setText:showMenNameString afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
           
            
            
            return mutableAttributedString;
        }];
        
        for (ZSTSNSAThumShare *thumbShare in message.thumArr)
        {
            NSString *oneName = [thumbShare.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : thumbShare.uname;
            NSRange range = [showMenNameString rangeOfString:oneName];
            [self.goodMenLB addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goToMenDetailById=%@",thumbShare.msisdn]] withRange:range];
        }
        
        CGSize menNameSize = [showMenNameString boundingRectWithSize:CGSizeMake(self.goodMenLB.bounds.size.width, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0f]} context:nil].size;
        self.loveView.frame = CGRectMake(self.loveView.frame.origin.x, self.loveView.frame.origin.y, self.loveView.bounds.size.width, menNameSize.height + 5);
        self.goodMenLB.frame = CGRectMake(self.goodMenLB.frame.origin.x, self.goodMenLB.frame.origin.y, self.goodMenLB.bounds.size.width, menNameSize.height);
        self.CGView.frame = CGRectMake(self.CGView.frame.origin.x, self.CGView.frame.origin.y, self.CGView.bounds.size.width, menNameSize.height + 10 );
        thumbHeight = menNameSize.height;
    }
    
    CGFloat commentHeight = 2.5;
    
    if (message.cgArr.count == 0 && message.thumArr.count == 0)
    {
        self.CGView.hidden = YES;
        self.chatArrowIV.hidden = YES;
    }
    else if (message.cgArr.count == 0)
    {
        self.commentsView.hidden = YES;
        self.CGView.frame = CGRectMake(self.CGView.frame.origin.x, self.CGView.frame.origin.y, self.CGView.bounds.size.width, thumbHeight);
    }
    else
    {
        self.CGView.hidden = NO;
        self.chatArrowIV.hidden = NO;
        self.commentsView.hidden = NO;
        
        NSArray *cgArr = message.cgArr;
        
        for (NSInteger i = 0; i < cgArr.count; i++)
        {
            CDTDisplayView *displayView = (CDTDisplayView *)[self.commentsView viewWithTag:i + 1992];
            displayView.hidden = NO;
            CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
            config.width = displayView.width;
            config.textColor = [UIColor blackColor];
            
            ZSTSNSAMessageComments *comments = cgArr[i];
            NSString *contentString = @"";
            if (comments.parentId != 0)
            {
                contentString = [NSString stringWithFormat:@"%@回复%@: %@",[comments.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : comments.uName,[comments.toUserId integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : comments.toUsername,comments.commentContent];
            }
            else
            {
                contentString = [NSString stringWithFormat:@"%@: %@",[comments.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : comments.uName,comments.commentContent];
            }
        
            CTFrameParser *parser = [[CTFrameParser alloc] init];
            parser.isDetailComments = NO;
            CoreTextData *data = [parser parseTemplateFileContent:contentString config:config UserMsisdn:comments.msisdn ToUserMsisdn:comments.toMsisdn];
            displayView.data = data;
            displayView.height = data.height;
            displayView.frame = CGRectMake(displayView.frame.origin.x, commentHeight, displayView.bounds.size.width, data.height);
            commentHeight = displayView.height + commentHeight + 1.5;
            [displayView setNeedsDisplay];
        }
        
        for (NSInteger j = cgArr.count; j < 3; j++)
        {
            CDTDisplayView *displayView = (CDTDisplayView *)[self.commentsView viewWithTag:j + 1992];
            displayView.hidden = YES;
        }
        
        self.CGView.frame = CGRectMake(self.CGView.frame.origin.x, self.CGView.frame.origin.y, self.CGView.frame.size.width, commentHeight + thumbHeight + 2.5);
        if (self.loveView.isHidden)
        {
            self.line.hidden = YES;
            self.commentsView.frame = CGRectMake(0, 0, self.commentsView.bounds.size.width, commentHeight);
        }
        else
        {
            self.line.hidden = NO;
            self.commentsView.frame = CGRectMake(0, CGRectGetMaxY(self.loveView.frame), self.commentsView.bounds.size.width, commentHeight);
        }
    }
}

- (void)tapCommentsToComments:(UITapGestureRecognizer *)tap
{
    NSInteger tapInteger = tap.view.tag - 1992;
    ZSTSNSAMessageComments *comments = self.thisMessage.cgArr[tapInteger];
    if ([self.CGcellDelegate respondsToSelector:@selector(sendCommentToComments:Comments:)])
    {
        [self.CGcellDelegate sendCommentToComments:self.thisMessage Comments:comments];
    }
}


@end
