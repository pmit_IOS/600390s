//
//  ZSTChatroomTopicDetailViewController.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <UIKit/UIKit.h>
#import "ZSTSNSAMessage.h"

@protocol ZSTChatroomTopicDetailViewControllerDelegate <NSObject>

- (void)upDateTopic:(ZSTSNSAMessage *)message newsComments:(ZSTSNSAMessageComments *)newsComments;
- (void)makeTopTopic:(ZSTSNSAMessage *)message;
- (void)removeTopic:(ZSTSNSAMessage *)message;

@end

@interface ZSTChatroomTopicDetailViewController : UIViewController

@property (copy,nonatomic) NSString *circleTitle;
@property (copy,nonatomic) NSString *topicId;
@property (strong,nonatomic) ZSTSNSAMessage *messageDic;
@property (copy,nonatomic) NSString *circleId;
@property (weak,nonatomic) id<ZSTChatroomTopicDetailViewControllerDelegate> detailDelegate;
@property (assign,nonatomic) BOOL isFromNews;
@property (assign,nonatomic) BOOL isDiscuss;
@property (strong,nonatomic) NSString *enterType;

@end
