//
//  ZSTReportViewController.m
//  SNSA
//
//  Created by LiZhenQu on 14-5-20.
//
//

#import "ZSTReportViewController.h"
#import "ZSTUtils.h"

@interface ZSTReportViewController ()<UIWebViewDelegate>
//{
//     UIWebView *webView;
//}

@property (nonatomic, retain) UIWebView *webView;

@end

@implementation ZSTReportViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"举报", nil)];
    //self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    _webView = [[[UIWebView alloc] initWithFrame:CGRectMake(0, 0, 320, 480+(iPhone5?88:0)-20)] autorelease];
    _webView.backgroundColor = [UIColor clearColor];
    [_webView setUserInteractionEnabled:YES];  //是否支持交互
    [_webView setDelegate:self];
//    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
     [self.view addSubview:_webView];
    
    NSString *msisdn = [ZSTF3Preferences shared].loginMsisdn;
    NSString *ecid = [ZSTF3Preferences shared].ECECCID;

    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?msisdn=%@&ecid=%@&circleid=%@&userid=%@&touserid=%@",ZSTSNSA_REPORT,msisdn,ecid,self.circleId,self.userId,self.toUserId]];//创建URL
    
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_webView setDelegate:nil];
    [super dealloc];
}


@end
