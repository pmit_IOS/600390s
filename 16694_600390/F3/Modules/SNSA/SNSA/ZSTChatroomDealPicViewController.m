//
//  ZSTChatroomDealPicViewController.m
//  SNSA
//
//  Created by pmit on 15/10/8.
//
//

#import "ZSTChatroomDealPicViewController.h"
#import <TKUIUtil.h>
#import <PMRepairButton.h>
#import "ZSTNewsTopicViewController.h"
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAssetRepresentation.h>

@interface ZSTChatroomDealPicViewController () <UIScrollViewDelegate>

@property (strong,nonatomic) UIScrollView *imgScroll;
@property (assign,nonatomic) BOOL isBarShow;
@property (assign,nonatomic) NSInteger nowShowIndex;

@end

@implementation ZSTChatroomDealPicViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isBarShow = YES;
    self.nowShowIndex = 0;
    [self buildNavi];
    [self buildScroll];
    [self buildIV:self.dealImgArr];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildNavi
{
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(backToLastView)];
    PMRepairButton *rubbishBtn = [[PMRepairButton alloc] init];
    rubbishBtn.frame = CGRectMake(0, 0, 30, 30);
    [rubbishBtn setImage:[UIImage imageNamed:@"chatroom_rubbish.png"] forState:UIControlStateNormal];
    [rubbishBtn addTarget:self action:@selector(deleteImg:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rubbishBtn];
}

- (void)buildScroll
{
    self.imgScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.imgScroll.delegate = self;
    self.imgScroll.backgroundColor = [UIColor blackColor];
    self.imgScroll.pagingEnabled = YES;
    self.imgScroll.contentSize = CGSizeMake(self.dealImgArr.count * WIDTH, 0);
    [self.view addSubview:self.imgScroll];
}

- (void)buildIV:(NSArray *)showArr
{
    for (UIView *sSubView in self.imgScroll.subviews)
    {
        if ([sSubView isKindOfClass:[UIImageView class]])
        {
            [sSubView removeFromSuperview];
        }
    }
    
    for (NSInteger i = 0; i < showArr.count; i++)
    {
        id asset = showArr[i];
        UIImage *tempImg;
        if ([asset isKindOfClass:[ALAsset class]])
        {
            tempImg = [UIImage imageWithCGImage:((ALAsset *)asset).defaultRepresentation.fullScreenImage];
        }
        else
        {
            tempImg = (UIImage *)asset;
        }
        
        
        UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(i * WIDTH, 0, WIDTH, self.imgScroll.bounds.size.height)];
        iv.contentMode = UIViewContentModeScaleAspectFit;
        iv.image = tempImg;
        iv.tag = 100 + i;
        iv.userInteractionEnabled = YES;
//        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showOrDismissBar:)];
//        [iv addGestureRecognizer:tap];
        [self.imgScroll addSubview:iv];
    }
    
    self.nowShowIndex = self.showPage;
    [self.imgScroll setContentOffset:CGPointMake(self.showPage * WIDTH, 0)];
}

- (void)deleteImg:(PMRepairButton *)sender
{
    BOOL isLast = NO;
    if (self.nowShowIndex == self.dealImgArr.count - 1)
    {
        isLast = YES;
    }
    
    
    NSMutableArray *dealArr = [NSMutableArray arrayWithArray:self.dealImgArr];
    UIImageView *iv = (UIImageView *)[self.imgScroll viewWithTag:(100 + self.nowShowIndex)];
    [iv removeFromSuperview];
    [dealArr removeObjectAtIndex:self.nowShowIndex];
    self.dealImgArr = dealArr;
    [self buildIV:dealArr];
    self.imgScroll.contentSize = CGSizeMake(dealArr.count * WIDTH, 0);
    if (isLast)
    {
        [self.imgScroll setContentOffset:CGPointMake((dealArr.count - 1 )* WIDTH, 0)];
    }
    else
    {
        [self.imgScroll setContentOffset:CGPointMake(self.nowShowIndex * WIDTH, 0)];
    }
    self.weakNewsTopic.isFromDeal = YES;
    self.nowShowIndex = self.imgScroll.contentOffset.x / WIDTH;
}

#pragma mark - 滚动时隐藏navigationBar
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    [UIView animateWithDuration:0.3f animations:^{
//        
//        self.imgScroll.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
//        self.navigationController.navigationBarHidden = YES;
//        self.isBarShow = NO;
//        
//    }];
    
    self.nowShowIndex = scrollView.contentOffset.x / WIDTH;
}

- (void)showOrDismissBar:(UITapGestureRecognizer *)tap
{
    if (self.isBarShow)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.imgScroll.frame = CGRectMake(0, 0, WIDTH, HEIGHT);
            self.navigationController.navigationBarHidden = YES;
            
        }];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
//            self.imgScroll.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
            self.navigationController.navigationBarHidden = NO;
            
        }];
    }
    
    self.isBarShow = !self.isBarShow;
}

- (void)backToLastView
{
    self.weakNewsTopic.isFromDeal = YES;
    [self.weakNewsTopic chatUploadImage:self.dealImgArr];
    [self popViewController];
}

@end
