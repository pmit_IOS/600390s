//
//  ZSTProfileFieldCell.m
//  YouYun
//
//  Created by luobin on 6/27/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTProfileFieldCell.h"

@implementation ZSTProfileFieldCell

@synthesize position;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        self.contentMode = UIViewContentModeRedraw;
        
        _switchOne = [[CustomSwitch alloc] initWithFrame:CGRectMake(250, (self.frame.size.height - 21)/2+2, 42, 21)];
        _switchOne.arrange = CustomSwitchArrangeONLeftOFFRight;
        _switchOne.onImage = ZSTModuleImage(@"module_snsa_switch_on.png");
        _switchOne.offImage = ZSTModuleImage(@"module_snsa_switch_off.png");
        _switchOne.status = CustomSwitchStatusOff;
        _switchOne.delegate = self;
        _switchOne.hidden = YES;
        [self.contentView addSubview:_switchOne];
    }
    return self;
}


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.contentMode = UIViewContentModeRedraw;
    }
    return self;
}

- (void)setInfoStatesBlock:(infoStatesBlock)block
{
    statesBlock = [block copy];
}

#pragma mark - customSwitch delegate
-(void)customSwitchSetStatus:(CustomSwitchStatus)status
{
    //    switch (status) {
    //        case CustomSwitchStatusOn:
    //            //todo
    //            break;
    //        case CustomSwitchStatusOff:
    //            //todo
    //            break;
    //        default:
    //            break;
    //    }
    
    if (!statesBlock) {
        
        return;
    }
    
    statesBlock(self,status);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGContextRef c = UIGraphicsGetCurrentContext();
    
    CGFloat height = 1.f;
    if (TKIsRetina()) {
        height = 0.5f;
    }
    
    UIColor *borderColor = [UIColor colorWithWhite:0.93 alpha:1];
    UIColor *shadowColor = [UIColor whiteColor];
    
    if (position == CustomCellBackgroundViewPositionTop) {
        
        [borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        
        return;
    }
    else if (position == CustomCellBackgroundViewPositionBottom) {
        
        [shadowColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height*2, self.bounds.size.width, height));     
        
        [shadowColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        
        return;
    }
    else if (position == CustomCellBackgroundViewPositionMiddle) {
        
        [shadowColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));
        
        return;
    }
    else if (position == CustomCellBackgroundViewPositionSingle)
    {
        [borderColor set];
        CGContextFillRect(c, CGRectMake(0, 0, self.bounds.size.width, height));
        
        [shadowColor set];
        CGContextFillRect(c, CGRectMake(0, height, self.bounds.size.width, height));
        
        [borderColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - 2*height, self.bounds.size.width, height));     
        
        [shadowColor set];
        CGContextFillRect(c, CGRectMake(0, self.bounds.size.height - height, self.bounds.size.width, height));          
        return;         
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

@end
