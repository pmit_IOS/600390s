
#import "ZSTDao+SNSA.h"
#import "ZSTSqlManager.h"
#import "TKUtil.h"
#import "ZSTSNSAMessageComments.h"

#define CREATE_MYFAVORITEINFO_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_MESSAGE_%@] (\
                                                                            id INTEGER PRIMARY KEY AUTOINCREMENT,\
                                                                            MID  VARCHAR(50) unique,\
                                                                            CID VARCHAR(50),\
                                                                            UID VARCHAR(100) ,\
                                                                            MContent VARCHAR,\
                                                                            ImgUrl VARCHAR(100),\
                                                                            AddTime double \
                                                                            );", @(moduleType)]
#define CREATE_TABLE_TOPIC_CMD(moduleType)   [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_TOPIC_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            TopicID int unique, \
                                                                            AddTime double DEFAULT (getdate()), \
                                                                            ImgUrl VARCHAR(100), \
                                                                            TopicContent VARCHAR,\
                                                                            CircleId INTEGER,\
                                                                            UserId INTEGER ,\
                                                                            UserAvatar  VARCHAR(100),\
                                                                            UserName VARCHAR(50) \
                                                                            );", @(moduleType)]
#define CREATE_TABLE_COMMENTS_CMD(moduleType)  [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [Z_COMMENTS_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            CommentId  int unique,\
                                                                            TopicID int , \
                                                                            AddTime double DEFAULT (getdate()), \
                                                                            ImgUrl VARCHAR(100), \
                                                                            CommentContent VARCHAR,\
                                                                            CircleId INTEGER,\
                                                                            UserId INTEGER ,\
                                                                            UserAvatar  VARCHAR(100),\
                                                                            UserName VARCHAR(50) \
                                                                            );", @(moduleType)]
#define kTableName [NSString stringWithFormat:@"Chat%@",[ZSTF3Preferences shared].ECECCID]
#define kCommentTableName [NSString stringWithFormat:@"Comments%@",[ZSTF3Preferences shared].ECECCID]
#define kThumTableName [NSString stringWithFormat:@"Thumb%@",[ZSTF3Preferences shared].ECECCID]


@implementation ZSTDao(SNSA)

- (void)createTableIfNotExistForSnsbModule {
    [ZSTSqlManager executeUpdate:CREATE_MYFAVORITEINFO_CMD(self.moduleType)];
    [ZSTSqlManager executeUpdate:CREATE_TABLE_TOPIC_CMD(self.moduleType)];
    [ZSTSqlManager executeUpdate:CREATE_TABLE_COMMENTS_CMD(self.moduleType)];
//    [self addcolumnTableWithName:@"VideoUrl"];
    [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"alter table Z_TOPIC_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),@"VideoUrl"]];
}

//- (BOOL) addcolumnTableWithName:(NSString *)name
//{
//    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"alter table Z_TOPIC_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),name]];
//    return success;
//}

- (BOOL)addTopic:(NSInteger)topicId
         addTime:(NSDate *)time
          imgUrl:(NSString *)imgUrl
        videoUrl:(NSString *)videoUrl
         content:(NSString *)topicContent
        circleId:(NSInteger)circleId
          userId:(NSInteger)userId
      userAvatar:(NSString *)userAvatar
        userName:(NSString *)userName
{
    NSString *querySql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_TOPIC_%@ (TopicID, AddTime, ImgUrl, VideoUrl, TopicContent, CircleId, UserId, UserAvatar,                                UserName) VALUES (?,?,?,?,?,?,?,?,?);", @(self.moduleType)];
    BOOL value = [ZSTSqlManager executeUpdate:querySql,
                  [NSNumber numberWithInteger:topicId], 
                  [NSNumber numberWithDouble:[time timeIntervalSince1970]],
                  [TKUtil wrapNilObject:imgUrl],
                  [TKUtil wrapNilObject:videoUrl],
                  [TKUtil wrapNilObject:topicContent], 
                  [NSNumber numberWithInteger:circleId], 
                  [NSNumber numberWithInteger:userId], 
                  [TKUtil wrapNilObject:userAvatar], 
                  [TKUtil wrapNilObject:userName]
                  ];
    
    return value;
}

- (BOOL)addComment:(NSInteger)commentId
           topicId:(NSInteger)topicId
           addTime:(NSDate *)time
            imgUrl:(NSString *)imgUrl
    commentContent:(NSString *)commentContent
          circleId:(NSInteger)circleId
            userId:(NSInteger)userId
        userAvatar:(NSString *)userAvatar
          userName:(NSString *)userName
{
    NSString *querySql = [NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_COMMENTS_%@ (CommentId, TopicID, AddTime, ImgUrl, CommentContent, CircleId, UserId, UserAvatar, UserName) VALUES (?,?,?,?,?,?,?,?,?);", @(self.moduleType)];
    BOOL value = [ZSTSqlManager executeUpdate:querySql,
                  [NSNumber numberWithInteger:commentId], 
                  [NSNumber numberWithInteger:topicId], 
                  [NSNumber numberWithDouble:[time timeIntervalSince1970]],
                  [TKUtil wrapNilObject:imgUrl], 
                  [TKUtil wrapNilObject:commentContent], 
                  [NSNumber numberWithInteger:circleId], 
                  [NSNumber numberWithInteger:userId], 
                  [TKUtil wrapNilObject:userAvatar], 
                  [TKUtil wrapNilObject:userName]
                  ];
    
    return value;
}

- (BOOL)addMessage:(NSString *)mID
          circleID:(NSString *)circleID
            userID:(NSString *)userID
           content:(NSString *)content
            imgUrl:(NSString *)imgUrl
           addTime:(NSDate *)time
{
    BOOL value = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO Z_MESSAGE_%@ (MID, CID, UID, MContent, ImgUrl, AddTime) VALUES (?,?,?,?,?,?);", @(self.moduleType)],
                  [TKUtil wrapNilObject:mID], [TKUtil wrapNilObject:circleID], [TKUtil wrapNilObject:userID], [TKUtil wrapNilObject:content], [TKUtil wrapNilObject:imgUrl], [NSNumber numberWithDouble:[time timeIntervalSince1970]]];
    
    if (!value) NSLog(@"insert favorite error..");
    return value;
}

- (BOOL)deleteMessage:(NSString *)mID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_MESSAGE_%@ WHERE mID = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:mID], nil]]) {
        NSLog(@"delete favorite error.. ID is: %@", mID);
        return NO;
    }
    return YES;
}

- (BOOL)messageExist:(NSString *)mID;
{
    NSUInteger count = 0;
    NSString *querySql = [NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM Z_MESSAGE_%@ where mID = ?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:mID], nil]];
    for (id value in results) {
        count = [[value safeObjectForKey:@"cnt"] unsignedIntegerValue];
    }
    return count;
}

- (NSDictionary *)messagesOfCircle:(NSString *)circleID atIndex:(int)index
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MID, CID, UID, MContent, ImgUrl, AddTime FROM Z_MESSAGE_%@ where CID = ? ORDER BY AddTime desc limit ?,?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [TKUtil wrapNilObject:circleID], 
                                                            [NSNumber numberWithInt:index], 
                                                            [NSNumber numberWithInt:1], 
                                                            nil]];
    if ([results count]) {
        return [results objectAtIndex:0];
    }
    return nil;
}

- (NSArray *)messagesOfCircle:(NSString *)circleID  atPage:(int)pageNum pageCount:(int)pageCount
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, MID, CID, UID, MContent, ImgUrl, AddTime FROM Z_MESSAGE_%@ where CID = ? ORDER BY AddTime desc limit ?,?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                            [TKUtil wrapNilObject:circleID], 
                                                            [NSNumber numberWithInt:(pageNum-1)*pageCount], 
                                                            [NSNumber numberWithInt:pageNum], 
                                                            nil]];
    return results;
}

- (NSUInteger)messagesCountOfCircle:(NSString *)circleID
{
    NSUInteger count = 0;
    NSArray *results = [ZSTSqlManager executeQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM Z_MESSAGE_%@ where CID = ?", @(self.moduleType)] arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:circleID], nil]];
    for (id value in results) {
        count = [[value safeObjectForKey:@"cnt"] unsignedIntegerValue];
    }
    return count;
}

- (NSString *) getMaxTopicID:(NSString *)circleID
{
	//不能用max(mblogid)，因为mblogid ascii排序有问题
    NSArray *results = [ZSTSqlManager executeQuery:[NSString stringWithFormat:@"select TopicID from Z_TOPIC_%@ where CircleId = ? ORDER BY TopicID desc limit 1", @(self.moduleType)] arguments:[NSArray arrayWithObjects:[TKUtil wrapNilObject:circleID], nil]];
    if ([results count] > 0) {
        return [[results lastObject] safeObjectForKey:@"TopicID"];
    }
    return nil;
}

- (NSArray *)topicAndCommentOfCircle:(NSString *)circleId 
                             startId:(NSInteger)startTopic
                          topicCount:(NSInteger)topicCount
                        commentCount:(NSInteger)commentCount
{
    NSArray *topics = nil;
    if (startTopic == 0) {
        NSString *querySql = [NSString stringWithFormat:@"SELECT TopicID, AddTime, ImgUrl, TopicContent, CircleId, UserId, UserAvatar, UserName FROM Z_TOPIC_%@ WHERE CircleId=?  ORDER BY TopicID desc limit ?", @(self.moduleType)];
        topics = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                      [NSNumber numberWithInteger:[circleId integerValue]],
                                                      [NSNumber numberWithInteger:topicCount],
                                                      nil
                                                      ]];
    } else {
        NSString *querySql = [NSString stringWithFormat:@"SELECT TopicID, AddTime, ImgUrl, TopicContent, CircleId, UserId, UserAvatar, UserName FROM Z_TOPIC_%@ WHERE TopicID<?   AND CircleId=? ORDER BY TopicID desc limit ?", @(self.moduleType)];
        topics = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                      [NSNumber numberWithInteger:startTopic],
                                                      [NSNumber numberWithInteger:[circleId integerValue]],
                                                      [NSNumber numberWithInteger:topicCount],
                                                      nil
                                                      ]];
    }
    NSMutableArray *topicArray = [NSMutableArray array];
    for (NSDictionary *topic in topics) {
        //读取主题
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setSafeObject:[topic safeObjectForKey:@"TopicID"] forKey:@"MID"];
        [params setSafeObject:[topic safeObjectForKey:@"AddTime"] forKey:@"AddTime"];
        [params setSafeObject:[topic safeObjectForKey:@"ImgUrl"] forKey:@"ImgUrl"];
        [params setSafeObject:[topic safeObjectForKey:@"TopicContent"] forKey:@"MContent"];
        [params setSafeObject:[topic safeObjectForKey:@"CircleId"] forKey:@"CircleId"];
        [params setSafeObject:[topic safeObjectForKey:@"UserId"] forKey:@"UID"];
        [params setSafeObject:[topic safeObjectForKey:@"UserAvatar"] forKey:@"UAvatar"];
        [params setSafeObject:[topic safeObjectForKey:@"UserName"] forKey:@"UName"];
        
        //读取评论
        NSString *commentQuery = [NSString stringWithFormat:@"SELECT CommentId, TopicID, AddTime, ImgUrl, CommentContent, CircleId, UserId, UserAvatar, UserName FROM Z_COMMENTS_%@ WHERE TopicID=? ORDER BY CommentId desc limit ?", @(self.moduleType)];
        NSArray *comments = [ZSTSqlManager executeQuery:commentQuery arguments:[NSArray arrayWithObjects:
                                                                     [topic objectForKey:@"TopicID"],
                                                                     [NSNumber numberWithInteger:commentCount],
                                                                     nil
                                                                     ]];
        NSMutableArray *commentsArray = [NSMutableArray array];
        for (NSDictionary *comment in comments) {
            NSMutableDictionary *commentParams = [NSMutableDictionary dictionary];
            [commentParams setSafeObject:[comment safeObjectForKey:@"CommentId"] forKey:@"MID"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"TopicID"] forKey:@"Parentid"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"AddTime"] forKey:@"AddTime"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"ImgUrl"] forKey:@"ImgUrl"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"CommentContent"] forKey:@"MContent"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"CircleId"] forKey:@"CircleId"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"UserId"] forKey:@"UID"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"UserAvatar"] forKey:@"UAvatar"];
            [commentParams setSafeObject:[comment safeObjectForKey:@"UserName"] forKey:@"UName"];
            
            [commentsArray insertObject:commentParams atIndex: 0];
        }
        NSInteger commentsCount = [commentsArray count];
        [params setSafeObject:@(commentsCount).stringValue forKey:@"CmtCount"];
        [params setSafeObject:commentsArray forKey:@"Cmts"];
        
        [topicArray addObject:params];
    }
    
    return topicArray;
}

- (BOOL)deleteTopicOfCircle:(NSString *)circleId
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_TOPIC_%@ WHERE CircleId=?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInteger:[circleId integerValue]]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)deleteCommentOfCircle: (NSString *)circleId
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM Z_COMMENTS_%@ WHERE CircleId=?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithInteger:[circleId integerValue]]]) {
        return NO;
    }
    
    return YES;
}

- (BOOL)saveComments:(NSArray *)comments circle:(NSString *)circleId
{
    if (comments==nil || circleId==nil) {
        return NO;
    }

    [ZSTSqlManager beginTransaction];
    for (int i=0; i<[comments count]; i++) {
        NSDictionary *comment = [comments objectAtIndex:i];
        NSString *addTime = [comment safeObjectForKey:@"AddTime"];
        [self addComment:[[comment objectForKey:@"MID"] integerValue]
                                  topicId:[[comment safeObjectForKey:@"Parentid"] integerValue]
                                  addTime:[NSDate dateWithTimeIntervalSince1970:[addTime doubleValue]]
                                   imgUrl:[comment safeObjectForKey:@"ImgUrl"]
                           commentContent:[comment safeObjectForKey:@"MContent"]
                                 circleId:[circleId integerValue]
                                   userId:[[comment safeObjectForKey:@"UID"] integerValue]
                               userAvatar:[comment safeObjectForKey:@"UAvatar"]
                                 userName:[comment safeObjectForKey:@"UName"]
         ];
    }
    [ZSTSqlManager commit];
    
    return YES;
}

- (void)createChatHistoryTable:(NSString *)circleID
{
    NSString *createSqlString = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID INTEGER PRIMARY KEY AUTOINCREMENT, circleID TEXT,addTime INTEGER, chatContent TEXT, chatMID INTEGER, chatMsisdn TEXT, chatNewsAvatar TEXT, chatNewsUName TEXT, chatParentId TEXT, chatUID TEXT, chatUName TEXT, commentCount INTEGER,userPhoto TEXT,shareCount INTEGER,goodCount INTEGER,istop INTEGER,isthumbup INTEGER,commentsArr BLOB,thumbArr BLOB,oneImgHeight DOUBLE,oneImgWidth DOUBLE)",kTableName];
    [ZSTSqlManager executeUpdate:createSqlString];
}

- (void)createCommentTable:(NSString *)circleID
{
    NSString *createSqlString = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID INTEGER PRIMARY KEY AUTOINCREMENT,circleID TEXT,addtime INTEGER,headphoto TEXT,mcid INTEGER,msisdn TEXT,parentid INTEGER,uid TEXT,tomsisdn TEXT,touserid TEXT,tousername TEXT,touserphoto TEXT,uname TEXT,comments TEXT,mid INTEGER)",kCommentTableName];
    [ZSTSqlManager executeUpdate:createSqlString];
}

- (NSArray *)selectedCircleMessage:(NSString *)circleID
{
    NSString *selectSql = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE circleID = ? ORDER BY addTime DESC",kTableName];
    NSArray *resultArr = [ZSTSqlManager executeQuery:selectSql arguments:@[[NSString stringWithFormat:@"%@",@([circleID integerValue])]]];
    return resultArr;
}

- (void)insertCircleMessage:(ZSTSNSAMessage *)messageDic CircleId:(NSString *)circleId MessageArrString:(NSString *)messageArrString
{
    NSArray *messageThreeComments = messageDic.cgArr;
    NSArray *messageThumbs = messageDic.thumArr;
    
    NSData *dataCommentsArrData = [NSKeyedArchiver archivedDataWithRootObject:messageThreeComments];
    NSData *dataThumbArrData = [NSKeyedArchiver archivedDataWithRootObject:messageThumbs];
    
    
    NSString *inserSql = [NSString stringWithFormat:@"INSERT INTO %@ (circleID,addTime,chatContent,chatMID,chatMsisdn,chatNewsAvatar,chatNewsUName,chatParentId,chatUID,chatUName,commentCount,userPhoto,shareCount,goodCount,istop,isthumbup,commentsArr,thumbArr,oneImgHeight,oneImgWidth) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)",kTableName];
    [ZSTSqlManager executeUpdate:inserSql arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(messageDic.addTime),messageDic.chatContent,@(messageDic.msgId),messageDic.chatMsisdn,[messageDic.chatNewsAvatar isKindOfClass:[NSString class]] && ![messageDic.chatNewsAvatar isEqualToString:@""] ? messageDic.chatNewsAvatar : @"" ,[messageDic.chatNewsUserName isKindOfClass:[NSString class]] && ![messageDic.chatNewsUserName isEqualToString:@""] ? messageDic.chatNewsUserName : @"佚名",@"0",messageDic.chatUID,[messageDic.chatNewsUserName isKindOfClass:[NSString class]] && ![messageDic.chatNewsUserName isEqualToString:@""] ? messageDic.chatNewsUserName : @"",@(messageDic.commentCount),messageDic.imageArrString,@(0),@(messageDic.thumbupCount),@(messageDic.isTop),@(messageDic.isThumb),dataCommentsArrData,dataThumbArrData,@(messageDic.oneImgHeight),@(messageDic.oneImgWidth)]];
    
    
}

- (NSInteger)selectMaxMID:(NSString *)circleId
{
    NSString *selectSql = [NSString stringWithFormat:@"SELECT MAX(chatMID) FROM %@ WHERE circleID = ?",kTableName];
    NSArray *resultArr = [ZSTSqlManager executeQuery:selectSql arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])]]];
    return  [[[resultArr firstObject] safeObjectForKey:@"MAX(chatMID)"] integerValue];
}

- (void)removeCircleMessage:(NSString *)circleId
{
    NSString *deleteSql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE circleID = ?",kTableName];
    [ZSTSqlManager executeUpdate:deleteSql arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])]]];
}

- (void)removeTopic:(NSString *)circleId MID:(NSString *)MID
{
    NSString *sql = [NSString stringWithFormat:@"DELETE FROM %@ WHERE circleID=? AND chatMID=?",kTableName];
    [ZSTSqlManager executeUpdate:sql arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],MID]];
}

- (void)insertTopicComments:(ZSTSNSAMessageComments *)newsComments CircleId:(NSString *)circleId
{
    NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO %@ (circleID,addtime,headphoto,mcid,msisdn,parentid,uid,tomsisdn,touserid,tousername,uname,comments,mid) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?)",kCommentTableName];
    [ZSTSqlManager executeUpdate:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(newsComments.addTime),newsComments.commentHeaderPhoto,@(newsComments.mcid),newsComments.msisdn,@(newsComments.parentId),newsComments.uid,newsComments.toMsisdn,[newsComments.toUserId isKindOfClass:[NSString class]] ? newsComments.toUserId : [ZSTF3Preferences shared].UserId,[newsComments.toUsername isKindOfClass:[NSString class]] ? newsComments.toUsername :[ZSTF3Preferences shared].personName,newsComments.uName,newsComments.commentContent,@(newsComments.mid)]];
}

- (NSArray *)selectedCommentsWith:(NSString *)circleId MID:(NSString *)MID
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE circleID=? AND mid=? ORDER BY addtime DESC",kCommentTableName];
    NSArray *resultArr =[ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],MID]];
    return resultArr;
}

- (void)removeComments:(NSString *)circleId MID:(NSString *)MID
{
    NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE circleID=? AND mid=?",kCommentTableName];
    [ZSTSqlManager executeUpdate:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],MID]];
}

- (void)updateTopic:(NSString *)circleId MID:(NSString *)MID NewsMessage:(ZSTSNSAMessage *)messageDic
{
    NSString *sqlString = [NSString stringWithFormat:@"UPDATE %@ SET circleID=?,addTime=?,chatContent=?,chatMID=?,chatMsisdn=?,chatNewsAvatar=?,chatNewsUName=?,chatParentId=?,chatUID=?,chatUName=?,commentCount=?,userPhoto=?,shareCount=?,goodCount=?,istop=?,isthumbup=?,commentsArr=?,thumbArr=?,oneImgHeight=?,oneImgWidth=? WHERE circleId=? AND chatMID=?",kTableName];
    
    NSArray *messageThreeComments = messageDic.cgArr;
    NSArray *messageThumbs = messageDic.thumArr;
    
    NSData *dataCommentsArrData = [NSKeyedArchiver archivedDataWithRootObject:messageThreeComments];
    NSData *dataThumbArrData = [NSKeyedArchiver archivedDataWithRootObject:messageThumbs];
    
    [ZSTSqlManager executeUpdate:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(messageDic.addTime),messageDic.chatContent,@(messageDic.msgId),messageDic.chatMsisdn,messageDic.chatNewsAvatar,messageDic.chatNewsUserName,@"",messageDic.chatUID,messageDic.chatNewsUserName,@(messageDic.commentCount),messageDic.imageArrString,@(0),@(messageDic.thumbupCount),@(messageDic.isTop),@(messageDic.isThumb),dataCommentsArrData,dataThumbArrData,circleId,MID,@(messageDic.oneImgHeight),@(messageDic.oneImgWidth)]];
    
}

- (NSArray *)selectTopThreeComments:(NSString *)circleId MID:(NSString *)MID
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE circleID=? AND mid=? ORDER BY addtime DESC LIMIT 3",kCommentTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],MID]];
}

- (NSArray *)selectTopMessageDic:(NSString *)circleId IsTop:(BOOL)isTop
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE circleID= ? AND istop = ? ORDER BY addTime DESC",kTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],isTop ? @(1) : @(0)]];
}

- (void)createThumMan
{
    NSString *sqlString = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID INTEGER PRIMARY KEY AUTOINCREMENT,circleId  TEXT,addtime INTEGER,mcid INTEGER,mid INTEGER,msisdn TEXT,uid TEXT,uname TEXT,headphoto TEXT,isShare INTEGER)",kThumTableName];
    NSLog(@"%@",@([ZSTSqlManager executeUpdate:sqlString]));
}

- (NSArray *)selectThumeShare:(NSString *)circleId MID:(NSString *)mid IsShare:(BOOL)isShare
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE circleId=? AND mid=? AND isshare=?",kThumTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],mid,isShare ? @(1) : @(0)]];
}

- (void)insertThumMan:(ZSTSNSAThumShare *)thumShare CircleId:(NSString *)circleId MID:(NSString *)mid
{
    NSString *sqlString = [NSString stringWithFormat:@"INSERT INTO %@ (circleId,addtime,mcid,mid,msisdn,uid,uname,headphoto,isshare) VALUES (?,?,?,?,?,?,?,?,?)",kThumTableName];
    [ZSTSqlManager executeUpdate:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(thumShare.addtime),@(thumShare.mcid),@(thumShare.mid),thumShare.msisdn,thumShare.uid,thumShare.uname,@"http://wx.qlogo.cn/mmopen/Q3auHgzwzM40Zwiaw4icMzGMDatIJVSnwScLxqDJtRg1HP6wDITOp9VPQ9f1K0RALtWpr4hrKu4J37LD5O4d2QXucR0X4A4FD9JUc34ia2lCv8/0",@(thumShare.isShare)]];
}

- (void)removeThumb:(NSString *)circleId MID:(NSString *)mid UID:(NSString *)uid
{
    NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE circleId=? AND mid=? AND uid=?",kThumTableName];
    [ZSTSqlManager executeUpdate:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],mid,uid]];
}

- (NSArray *)selectOneTopic:(NSString *)MID
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE chatMID=?",kTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[MID]];
}

- (NSArray *)selectMaxMsgId:(NSString *)circleId
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT MAX(chatMID) AS maxId FROM %@ WHERE circleID=?",kTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])]]];
}

- (NSArray *)selectMinMsgId:(NSString *)circleId
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT MIN(chatMID) AS minId FROM %@ WHERE circleID=?",kTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])]]];
}

- (NSArray *)selectMaxCommentsId:(NSString *)circleId
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT MAX(mcid) AS maxMcId FROM %@ WHERE circleID=?",kCommentTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])]]];
}

- (NSArray *)selectMaxThumbId:(NSString *)circleId
{
    NSString *sqlString = [NSString stringWithFormat:@"SELECT MAX(mcid) AS maxMcId FROM %@ WHERE circleID=?",kThumTableName];
    return [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])]]];
}

- (void)removeTopicAllThumb:(NSString *)circleId MID:(NSString *)MID
{
    NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE circleID=? AND mid=?",kThumTableName];
    [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],MID]];
}

- (void)removeTopicByMID:(NSString *)circleId MaxMid:(NSInteger)maxMid
{
//    NSString *sqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE chatMID IN (SELECT chatMID FROM %@ WHERE chatMID < ? AND circleID = ? ORDER BY chatMID DESC LIMIT 0,10)",kTableName,kTableName];
//    NSString *commentsString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE mid IN (SELECT chatMID FROM %@ WHERE chatMID<? AND circleID=? ORDER BY chatMID DESC LIMIT 0,10)",kCommentTableName,kTableName];
//    [ZSTSqlManager executeUpdate:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(maxMid)]];
//    [ZSTSqlManager executeUpdate:commentsString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(maxMid)]];
    NSString *sqlString = [NSString stringWithFormat:@"SELECT * FROM %@ WHERE circleID = ? AND chatMID < ? LIMIT 11",kTableName];
    NSArray *resultArr = [ZSTSqlManager executeQuery:sqlString arguments:@[[NSString stringWithFormat:@"%@",@([circleId integerValue])],@(maxMid)]];
    for (NSDictionary *dic in resultArr)
    {
        NSString *deleteSqlString = [NSString stringWithFormat:@"DELETE FROM %@ WHERE chatMID = ?",kTableName];
        [ZSTSqlManager executeUpdate:deleteSqlString arguments:@[[dic safeObjectForKey:@"chatMID"]]];
    }
}


@end
