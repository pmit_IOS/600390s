//
//  ZSTMyChatroomHeaderCell.h
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>
#import "ZSTSNSAMessage.h"

@protocol ZSTMyChatroomHeaderCellDelegate <NSObject>

- (void)showMoreFun:(ZSTSNSAMessage *)messageDic;
- (void)goToUserDetail:(NSString *)loginMsisdn;
- (void)goTopicDetail:(ZSTSNSAMessage *)messageDic;

@end

@interface ZSTMyChatroomHeaderCell : UITableViewCell

@property (strong,nonatomic) UIImageView *headerIV;
@property (strong,nonatomic) UILabel *headerNameLB;
@property (strong,nonatomic) UILabel *headerTimeLB;
@property (strong,nonatomic) UIButton *headerBtn;
@property (strong,nonatomic) PMRepairButton *moreBtn;
@property (weak,nonatomic) id<ZSTMyChatroomHeaderCellDelegate> headerCellDelegate;
@property (strong,nonatomic) ZSTSNSAMessage *topicMessage;
@property (strong,nonatomic) UIButton *topBtn;
@property (assign,nonatomic) BOOL isDetailCell;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessage *)messageDic;

@end
