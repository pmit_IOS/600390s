//
//  ZSTYouYunEngine.h
//  YouYun
//
//  Created by luobin on 6/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZSTSNSCommunicator.h"
#import "ZSTGlobal+F3.h"

@protocol ZSTYouYunEngineDelegate <NSObject>

@optional

- (void)requestDidFail:(NSError *)error method:(NSString *)method;

- (void)addCircleResponse:(NSString *)circleID;

- (void)getCirclesResponse:(NSArray *)circles;

- (void)getUsersByCIDResponse:(NSArray *)users;

- (void)quitCircleResponse;

- (void)quitCircleFailed;

- (void)sendMessageResponse;

- (void)getMessagesResponse:(NSArray *)messages dataFromLocal:(BOOL)dataFromLocal hasMore:(BOOL)hasMore;

- (void)getUserByUIDResponse:(NSDictionary *)userInfo;

- (void)updateUserByUIDResponse;

- (void)registerResponse:(NSUInteger)UID;

- (void)sendVerifyCodeResponse;

- (void)getCommentsByMIDResponse:(NSArray *)msgs;

- (void)upLoadFileResponse:(NSString *)fileKey;

- (void)upLoadVideoFileResponse:(NSString *)fileKey;

- (void)jionCircleSuccess;

- (void)jionCircleFailed;

- (void)getMyMessageSuccess:(NSDictionary *)messageArrDic;
- (void)getMyMessageFailure:(NSString *)resultString;

- (void)getUserInfoDidSucceed:(NSDictionary *)response;
- (void)getUserInfoDidFailed:(NSString *)response;

- (void)sendNewsTopicDidSuccess:(NSDictionary *)response;
- (void)sendNewsTopicDidFailure:(NSString *)response;

- (void)sendThumbopDidSuccess:(NSDictionary *)response;
- (void)sendThumbopDidFailure:(NSString *)response;

- (void)getAllCommentsDidSuccess:(NSDictionary *)response;
- (void)getAllCommentsDidFailure:(NSString *)response;

- (void)getAllThumbopDidSuccess:(NSDictionary *)response;
- (void)getAllThumbopDidFailure:(NSString *)response;

- (void)makeTopicTopDidSuccess:(NSDictionary *)response;
- (void)makeTopicTopDidFailure:(NSInteger)resultCode;

- (void)removeTopicDidSuccess:(NSDictionary *)response;
- (void)removeTopicDidFailure:(NSString *)resultString;

- (void)sendCommentsDidSuccess:(NSDictionary *)response;
- (void)sendCommentsDidFailure:(NSString *)resultString;

- (void)sendTopicReportDidSuccess:(NSDictionary *)response;
- (void)sendTopicReportDidFailiure:(NSString *)resultString;

- (void)getAboutMeDidSuccess:(NSDictionary *)response;
- (void)getAboutMeDidFailure:(NSString *)resultString;

- (void)getOneTopicDidSuccess:(NSDictionary *)response;
- (void)getOneTopicDidFailure:(NSString *)resultString;

- (void)getMessageHasReadDidSuccess:(NSDictionary *)response;
- (void)getMessageHasReadDidFailure:(NSString *)resultString;

- (void)getNewsMessageCountDidSuccess:(NSDictionary *)response;
- (void)getNewsMessageCountDidFailure:(NSString *)resultString;

- (void)getCircleNewsCommentsDidSuccess:(NSDictionary *)response;
- (void)getCircleNewsCommentsDidFailure:(NSString *)resultString;

- (void)getAboutMeCountDidSuccess:(NSDictionary *)response;
- (void)getAboutMeCountDidFailure:(NSString *)resultString;

- (void)getMyCirclesDidSuccess:(NSDictionary *)response;
- (void)getMyCirclesDidFailure:(NSString *)resultString;

@end

@interface ZSTYouYunEngine : NSObject<ZSTSNSCommunicatorDelegate>

@property (nonatomic, retain) ZSTDao *dao;
@property (nonatomic, assign) NSInteger moduleType;
@property (nonatomic, retain) id<ZSTYouYunEngineDelegate> delegate;

- (id)initWithDelegate:(id<ZSTYouYunEngineDelegate>)delegate ;

+ (id)engineWithDelegate:(id<ZSTYouYunEngineDelegate>)delegate;

- (BOOL) isValidDelegateForSelector:(SEL)selector;

//////////////////////////////////////////////////////////////// local ////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////加入圈子  JoinCircles
/**
*CID：圈子ID
*UID：用户ID
*ECID：企业编号
*Msisdn：手机号
**/
-(void)joinCircle:(NSString *) cid flag:(int)flag;

/**
 *	@brief	添加圈子
 *
 *	@param 	circleName      圈子名称
 *	@param 	description 	圈子描述
 */
- (void)addCircle:(NSString *)circleName desc:(NSString *)description;


/**
 *	@brief	获取当前登录用户的圈子列表
 */
- (void)getCircles:(NSString *)cInfo;


- (void)getMyCircles:(NSString *)cInfo;


/**
 *	@brief	根据圈子ID获得成员名单
 *
 *	@param 	CID 	圈子ID
 */
- (void)getUsersByCID:(NSString *)CID;


/**
 *	@brief	退出圈子
 *
 *	@param 	CID 	圈子ID
 */
- (void)quitCircle:(NSString *)CID;


/**
 *	@brief	发表消息
 *
 *	@param 	CID         要发表到的圈子ID
 *	@param 	parentID 	评论信息ID,如果为0则为原创,非零为评论ID为ParentID的信息
 *	@param 	content 	消息内容
 */
- (void)sendMessage:(NSString *)CID parentID:(NSString *)parentID content:(NSString *)content fileKey:(NSString *)fileKey;

/**
 *	@brief	发表语音消息
 *
 *	@param 	CID         要发表到的圈子ID
 *	@param 	parentID 	评论信息ID,如果为0则为原创,非零为评论ID为ParentID的信息
 *	@param 	content 	消息内容
 */
- (void)sendvideoMessage:(NSString *)CID parentID:(NSString *)parentID content:(NSString *)content fileKey:(NSString *)fileKey videoTimeLength:(NSInteger)duration;

/**
 *	@brief	根据圈子获取消息列表
 *
 *	@param 	CID         圈子ID
 *	@param 	fetchCount 	消息数
 */
- (void)getMessages:(NSString *)CID messageID:(NSString *)MID sortType:(NSString *)sortType fetchCount:(NSUInteger)fetchCount;


/**
 *	@brief	根据用户ID获取个人信息
 *
 *	@param 	UID 	用户ID
 */
- (void)getUserByUID:(NSString *)UID;


/**
 *	@brief	修改个人名片信息
 *
 *	@param 	UID         用户ID
 *	@param 	msisdn      手机号
 *	@param 	userName 	用户名
 *  @param 	gender      性别
 *	@param 	city        城市
 *	@param 	company 	公司
 *	@param 	address 	地址
 *	@param 	qq          qq
 *	@param 	email       邮箱地址
 *	@param 	imgUrl      头像
 *	@param 	ispublic    是否公开电话
 */
- (void)updateUserByUID:(NSString *)UID
                 msisdn:(NSString *)msisdn
               userName:(NSString *)userName
                 gender:(int)gender
                   city:(NSString *)city
                company:(NSString *)company
                address:(NSString *)address
                     qq:(NSString *)qq
                  email:(NSString *)email
                 imgUrl:(NSString *)imgUrl
               ispublic:(int)ispublic;

/**
 *	@brief	注册
 *
 *	@param 	msisdn 	手机号码
 *	@param 	verifyCode 	验证码
 */
- (void)registerMsisdn:(NSString *)msisdn verifyCode:(NSString *)verifyCode;


/**
 *	@brief	发送验证码
 *
 *	@param 	msisdn 	手机号码
 */
- (void)sendVerifyCode:(NSString *)msisdn;


/**
 *	@brief	获取主题评论列表
 *
 *	@param 	msgID 	主题ID
 */
- (void)getCommentsByMID:(NSString *)msgID;


/**
 *	@brief	上传文件
 *
 *	@param 	data        图片数据
 *	@param 	progress 	进度代理，实现 - (void)setProgress:(float)newProgress;
 *	@param 	suffix      图片后缀
 */
- (void)upLoadFile:(NSData *)data progressDelegate:(id)progress suffix:(NSString *)suffix;


/**
 *	@brief	上传音频文件
 *
 *	@param 	data        图片数据
 *	@param 	progress 	进度代理，实现 - (void)setProgress:(float)newProgress;
 *	@param 	suffix      图片后缀
 */
- (void)upLoadVideoFile:(NSData *)data progressDelegate:(id)progress suffix:(NSString *)suffix;



/**
 *	@brief	获取指定圈子下pageNum页的消息
 *
 *	@param 	circleID 	指定圈子
 *	@param 	pageNum 	页数
 */
- (void)getMessages:(NSString *)circleID atPage:(int)pageNum;

- (void)uploadFile:(NSArray *)imgDataArr Content:(NSString *)contentString;

- (void)getMyMessages:(NSString *)CID messageID:(NSString *)MID sortType:(NSString *)sortType fetchCount:(NSUInteger)fetchCount;

- (void)getUserInfoWithMsisdn:(NSString *)msisdn userId:(NSString *)userid;

- (void)getMyCircleTopic:(NSString *)circleID messageID:(NSString *)MID Flag:(NSString *)topicFlag sortType:(NSString *)sortType PageSize:(NSInteger)pageSize;

- (void)sendNewsTopic:(NSString *)circleId TopicContent:(NSString *)content ImgArr:(NSArray *)imageArr;

- (void)sendThumbop:(NSString *)circleId messageID:(NSString *)MID ThumType:(NSString *)thumType;

- (void)sendComments:(NSString *)circleId MID:(NSString *)MID Comments:(NSString *)comments ParentId:(NSString *)parentId TouristId:(NSString *)touruserId;

- (void)getAllComments:(NSString *)circleId MID:(NSString *)MID PageSize:(NSInteger)pageSize currentMsgId:(NSString *)currentMsgId;

- (void)getThumBop:(NSString *)circleId MID:(NSString *)MID PageSize:(NSInteger)pageSize currentMsgId:(NSString *)currentMsgId;

- (void)makeTopicTop:(NSString *)circleId MID:(NSString *)MID IsTop:(BOOL)isTop;

- (void)removeTopic:(NSString *)MID CircleId:(NSString *)circleId;

- (void)sendTopicReport:(NSString *)circleId MID:(NSString *)MID Reason:(NSString *)reason Remark:(NSString *)remark;

- (void)getAboutMe:(NSString *)circleId;

- (void)getOneTopic:(NSString *)mid;

- (void)getMessageHasRead:(NSString *)circleId;

- (void)getNewsMessageCount:(NSString *)circleId MID:(NSString *)MID;

- (void)getCircleNewsComments:(NSString *)circleId MaxCommentsId:(NSString *)mcid MaxThumbId:(NSString *)maxThumbId;

- (void)getAboutMeCount:(NSString *)circleId;

@end
