//
//  ZSTGlobal.h
//  YouYun
//
//  Created by luobin on 6/6/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#ifndef YouYun_ZSTGlobal_h
#define YouYun_ZSTGlobal_h

#define TOPIC_PAGE_SIZE 30

#define SortType_Desc @"desc"               //向新取
#define SortType_Asc @"asc"                 //向旧取

#define TK_DB_NAME @"YouYun.db"

typedef enum
{
    UserType_Common = 1,
    UserType_Admin = 2,
    UserType_Creator = 3
} UserType;


NSDictionary* nameAttributes(void);

NSDictionary* smallNameAttributes(void);

NSDictionary* contentAttributes(void);

NSDictionary* smallContentAttributes(void);

NSDictionary* timeAttributes(void);

NSDictionary* smallTimeAttributes(void);

#endif