//
//  MyClass.m
//  YouYun
//
//  Created by luobin on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTGroupAddressBookTableviewCell.h"

@implementation ZSTGroupAddressBookTableviewCell

@synthesize avtarImageView;
@synthesize nameLabel;
@synthesize adminImageView;
@synthesize stateImageView;
@synthesize phoneNumberLabel;
@synthesize userData;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code

        UIImageView *avtarbg = [[[UIImageView alloc] initWithFrame:CGRectMake((58-(101/2.0))/2.0, (58-(101/2.0))/2.0, (101/2.0), (101/2.0))] autorelease];
        avtarbg.image = [ZSTModuleImage(@"module_snsa_icon_frame.png") stretchableImageWithLeftCapWidth:2 topCapHeight:2];
        avtarbg.backgroundColor = [UIColor clearColor];
        [self.contentView addSubview:avtarbg];

        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(((101/2.0)-45)/2.0,((101/2.0)-45)/2.0 , 45, 45)] autorelease];
        self.avtarImageView.backgroundColor = [UIColor clearColor];
        self.avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");

        [avtarbg addSubview:self.avtarImageView];
        
        self.nameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(65, 11, 200, 14)] autorelease];
        self.nameLabel.backgroundColor = [UIColor clearColor];
        self.nameLabel.font = [UIFont systemFontOfSize:14];
        self.nameLabel.textColor = [UIColor colorWithRed:0.17 green:0.25 blue:0.32 alpha:1];
        self.nameLabel.highlightedTextColor = [UIColor whiteColor];
        self.nameLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.nameLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self.contentView addSubview:self.nameLabel];
        
        self.adminImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(100, 12, 12.5, 12.5)] autorelease];
        self.adminImageView.image = ZSTModuleImage(@"module_snsa_master_head_icon.png");
//        [self.contentView addSubview:self.adminImageView];
        
        self.stateImageView = [[[UIImageView alloc] initWithFrame:CGRectMake(120, 12, 12.5, 12.5)] autorelease];
        self.stateImageView.image = ZSTModuleImage(@"module_snsa_install_state.png");
//        [self.contentView addSubview:self.stateImageView];
        
        self.phoneNumberLabel = [[[UILabel alloc] initWithFrame:CGRectMake(65, 32, 250, 15)] autorelease];
        self.phoneNumberLabel.backgroundColor = [UIColor clearColor];
        self.phoneNumberLabel.font = [UIFont systemFontOfSize:12];
        self.phoneNumberLabel.textColor = [UIColor colorWithRed:0.38 green:0.49 blue:0.57 alpha:1];
        self.phoneNumberLabel.highlightedTextColor = [UIColor whiteColor];
        self.phoneNumberLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.phoneNumberLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self.contentView addSubview:self.phoneNumberLabel];
        
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    return self;
}

- (void)dealloc
{
    self.avtarImageView = nil;
    self.nameLabel = nil;
    self.adminImageView = nil;
    self.stateImageView = nil;
    self.phoneNumberLabel = nil;
    self.userData = nil;
    [super dealloc];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setUserData:(NSDictionary *)theUserData
{
    if (userData != theUserData) {
        [userData release];
        userData = [theUserData retain];
        
        [self.avtarImageView clear];
        NSString *fileKey = [self.userData safeObjectForKey:@"AvatarImgUrl"];
        if (fileKey == nil) {
            fileKey = @"";
        }
        
         NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", fileKey]];
        [self.avtarImageView setUrl:url];
        [self.avtarImageView loadImage];
        
        NSString *names = [userData safeObjectForKey:@"New_UserName"];
        if ([self isPureInt:names] && names.length >= 11)
        {
            names = [names stringByReplacingCharactersInRange:NSMakeRange(5, 4) withString:@"****"];
        }
        self.nameLabel.text = names;
        self.phoneNumberLabel.text = [userData safeObjectForKey:@"Msisdn"];
        
        UserType userType = [[userData safeObjectForKey:@"UType"] intValue];
        self.adminImageView.hidden = !(userType == UserType_Admin || userType == UserType_Creator);
        self.stateImageView.hidden = ([[userData safeObjectForKey:@"IsClientUser"] intValue] != 1);
    }
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    CGSize size = [self.nameLabel.text sizeWithFont:self.nameLabel.font constrainedToSize:CGSizeMake(200, 14) lineBreakMode:NSLineBreakByCharWrapping];
    self.nameLabel.width = size.width;
    
    self.adminImageView.left = CGRectGetMaxX(self.nameLabel.frame) + 20;
    self.stateImageView.left = CGRectGetMaxX(self.nameLabel.frame) + 5;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [[ZSTModuleImage(@"module_snsa_addressBook_list_cell_bg.png") stretchableImageWithLeftCapWidth:0 topCapHeight:29] drawInRect:rect];
}

- (BOOL)isPureInt:(NSString *)string{
    NSScanner* scan = [NSScanner scannerWithString:string];
    NSInteger val;
    return [scan scanInteger:&val] && [scan isAtEnd];
}

@end
