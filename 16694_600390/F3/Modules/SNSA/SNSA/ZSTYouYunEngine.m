//
//  ZSTYouYunEngine.m
//  YouYun
//
//  Created by luobin on 6/5/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTYouYunEngine.h"
#import "ZSTSNSCommunicator.h"
#import "ZSTDao+SNSA.h"
#import "ZSTYouYunModel.h"
#import "ZSTSqlManager.h"
#import "CommonFunc.h"

#define KCommentLimit  3
#define SendNineImgUrl  [NSString stringWithFormat:@"%@%@",URL_SERVER_BASE_PATH,@"UpLoadNineImg"]

@implementation ZSTYouYunEngine

@synthesize moduleType;
@synthesize delegate;
@synthesize dao;

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
    }
    
    return self;
}

- (ZSTDao *)dao
{
    if (dao == nil) {
        dao = [[ZSTDao daoWithModuleType:self.moduleType] retain];
    }
    return dao;
}

- (id)initWithDelegate:(id<ZSTYouYunEngineDelegate>)theDelegate
{
    self = [super init];
    if (self) {
        self.delegate = theDelegate;
    }
    return self;
}

+ (id)engineWithDelegate:(id<ZSTYouYunEngineDelegate>)delegate
{
    return [[[ZSTYouYunEngine alloc] initWithDelegate:delegate] autorelease];
}

- (void)dealloc
{
    self.dao = nil;
    self.delegate = nil;
    [[ZSTSNSCommunicator shared] cancelUploadByDelegate:self];
    [[ZSTSNSCommunicator shared] cancelByDelegate:self];
    [super dealloc];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (BOOL) isValidDelegateForSelector:(SEL)selector
{
	return ((delegate != nil) && [delegate respondsToSelector:selector]);
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getLocalMessagesAtPage:(int)pageNum
{
    
}


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)requestDidFail:(NSError *)error userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(requestDidFail:method:)]) {
        [self.delegate requestDidFail:error method:userInfo];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)addCircle:(NSString *)circleName desc:(NSString *)description
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:circleName forKey:@"CName"];
    [params setSafeObject:description forKey:@"CDesc"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_AddCircle
                                       postParams:params 
                                     needUserAuth:YES 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(addCircleResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_AddCircle];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)addCircleResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(addCircleResponse:)]) {
        [self.delegate addCircleResponse:[results safeObjectForKey:@"CID"]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getCircles:(NSString *)cInfo
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSString stringWithFormat:@"%@",cInfo] forKey:@"CInfo"];
    [params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
    [params setSafeObject:@(self.moduleType).stringValue forKey:@"ModuleType"];
    [params setSafeObject:[ZSTF3Preferences shared].UID forKey:@"UID"];
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:[ZSTHttpMethod_GetCircles stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                       postParams:params 
                                     needUserAuth:NO 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(getCirclesResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_GetCircles];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getCirclesResponse:(id)results userInfo:(id)userInfo
{
    NSLog(@"snsaRes --> %@",results);
    if ([results safeObjectForKey:@"UID"]) {
        int uid = [[results safeObjectForKey:@"UID"]intValue];
        [[ZSTF3Preferences shared] setUID:[NSString stringWithFormat:@"%d",uid]];
    }
//    NSString *userID = [TKKeychainUtils getUserid:userKeyChainIdentifier];
    if ([self isValidDelegateForSelector:@selector(getCirclesResponse:)]) {
        [self.delegate getCirclesResponse:[results safeObjectForKey:@"Info"]];
        
//        NSLog(@"!!!!!!!!!!!!!@@@@@@@@@@ %@",[results objectForKey:@"UID"]);
        [TKKeychainUtils createKeychainValueForUserid:[[results safeObjectForKey:@"UID"] stringValue] withToken:@"" withName:@"" withAccountType:0 withAvtarURL:@"" forIdentifier:userKeyChainIdentifier];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getUsersByCID:(NSString *)CID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:CID forKey:@"CID"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:[ZSTHttpMethod_GetUsersByCID stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                       postParams:params 
                                     needUserAuth:YES 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(getUsersByCIDResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_GetUsersByCID];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getUsersByCIDResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(getUsersByCIDResponse:)]) {
        [self.delegate getUsersByCIDResponse:[results safeObjectForKey:@"Info"]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)quitCircle:(NSString *)CID
{
    NSDictionary *userDic = [[CommonFunc sharedCommon] getCurrentUVO];
    NSString *userId;
    if (!userDic || [userDic isKindOfClass:[NSNull class]]) {
        userId = @"";
    } else {
        userId = [userDic safeObjectForKey:@"UID"];
    }

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:CID forKey:@"CID"];
    [params setSafeObject:userId forKey:@"UID"];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_QuitCircle
                                       postParams:params 
                                     needUserAuth:YES 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail)
                                  succeedSelector:@selector(quitCircleResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_QuitCircle];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)quitCircleResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(quitCircleResponse)]) {
        [self.delegate quitCircleResponse];
    }
}

-(void)requestDidFail
{
    if ([self isValidDelegateForSelector:@selector(quitCircleFailed)]) {
        [self.delegate quitCircleFailed];
    }

}
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)sendMessage:(NSString *)CID parentID:(NSString *)parentID content:(NSString *)content fileKey:(NSString *)fileKey
{
    if (parentID == nil || [parentID isEmptyOrWhitespace]) {
        parentID = @"0";
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:CID forKey:@"CID"];
    [params setSafeObject:parentID forKey:@"ParentID"];
    [params setSafeObject:content forKey:@"MContent"];
    if (fileKey) {
        [params setSafeObject:fileKey forKey:@"ImgUrl"];
    }
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_SendMessage
                                       postParams:params 
                                     needUserAuth:YES 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(sendMessageResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_SendMessage];
}

- (void)sendvideoMessage:(NSString *)CID parentID:(NSString *)parentID content:(NSString *)content fileKey:(NSString *)fileKey videoTimeLength:(NSInteger)duration
{
    if (parentID == nil || [parentID isEmptyOrWhitespace]) {
        parentID = @"0";
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:CID forKey:@"CID"];
    [params setSafeObject:parentID forKey:@"ParentID"];
    [params setSafeObject:content forKey:@"MContent"];
    if (fileKey) {
        [params setSafeObject:fileKey forKey:@"VideoUrl"];
    }
    [params setSafeObject:[NSNumber numberWithInteger:duration] forKey:@"VideoTimeLength"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_SendMessage
                                          postParams:params
                                        needUserAuth:YES
                                            delegate:self
                                        failSelector:@selector(requestDidFail:userInfo:)
                                     succeedSelector:@selector(sendMessageResponse:userInfo:)
                                            userInfo:ZSTHttpMethod_SendMessage];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)sendMessageResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(sendMessageResponse)]) {
        [self.delegate sendMessageResponse];
    }
}

- (void)getMyMessages:(NSString *)CID messageID:(NSString *)MID sortType:(NSString *)sortType fetchCount:(NSUInteger)fetchCount
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:CID forKey:@"CID"];
    [params setSafeObject:MID forKey:@"MID"];
    [params setSafeObject:sortType forKey:@"SortType"];
    [params setSafeObject:@(fetchCount).stringValue forKey:@"PageNum"];
    [params setSafeObject:[ZSTF3Preferences shared].UID forKey:@"UID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_GetMessages
                                          postParams:params
                                        needUserAuth:YES
                                            delegate:self
                                        failSelector:@selector(requestDidFail:userInfo:)
                                     succeedSelector:@selector(getMyMessagesResponse:userInfo:)
                                            userInfo:[NSDictionary dictionaryWithObjectsAndKeys:ZSTHttpMethod_GetMessages, @"Method", params, @"Params", nil]];
}

- (void)getMyMessagesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"response --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(getMyMessageSuccess:)])
        {
            [self.delegate getMyMessageSuccess:response.jsonResponse];
        }
    }
    else
    {
        
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getMessages:(NSString *)CID messageID:(NSString *)MID sortType:(NSString *)sortType fetchCount:(NSUInteger)fetchCount
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:CID forKey:@"CID"];
    [params setSafeObject:MID forKey:@"MID"];
    [params setSafeObject:sortType forKey:@"SortType"];
    [params setSafeObject:@(fetchCount).stringValue forKey:@"PageNum"];
    [params setSafeObject:[ZSTF3Preferences shared].UID forKey:@"UID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    if (MID != nil && [MID integerValue]>0) {
        NSArray *data = [self.dao topicAndCommentOfCircle:CID
                                                  startId:[MID integerValue]
                                               topicCount:fetchCount
                                             commentCount:KCommentLimit];
        
        if (data!=nil && [data count]>0) {
            if ([self isValidDelegateForSelector:@selector(getMessagesResponse:dataFromLocal:hasMore:)]) {
                [self.delegate getMessagesResponse:data dataFromLocal:YES hasMore:YES];
                return;
            }
        }
    }
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_GetMessages
                                       postParams:params 
                                     needUserAuth:YES 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(getMessagesResponse:userInfo:) 
                                         userInfo:[NSDictionary dictionaryWithObjectsAndKeys:ZSTHttpMethod_GetMessages, @"Method", params, @"Params", nil]];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getMessagesResponse:(id)results userInfo:(id)userInfo
{
    NSArray *messages = [results safeObjectForKey:@"Info"];
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
//    NSString *sortType = [params safeObjectForKey:@"SortType"];
    NSString *circleID = [params safeObjectForKey:@"CID"];
//    NSString *mid = [params safeObjectForKey:@"MID"];
//    int pageSize = [[params objectForKey:@"PageNum"] integerValue];
    
//    //一段,如果首页获取到TOPIC_PAGE_SIZE条(count)，则删除原数据(因为不连续)
//    if ([sortType isEqualToString:SortType_Desc]
//        && (((NSNull *)mid) == [NSNull null] || [mid intValue]==0)) {
//        [ZSTSqlManager beginTransaction];
//        [self.dao deleteTopicOfCircle:circleID];
//        [self.dao deleteCommentOfCircle:circleID];
//        [ZSTSqlManager commit];
//    }
//    
//    //入库
//    [ZSTSqlManager beginTransaction]; 
//    for (NSMutableDictionary *topic in messages){
//        //保存主题
//        NSString *addTime = [topic objectForKey:@"AddTime"];
//        [self.dao addTopic:[[topic objectForKey:@"MID"] integerValue]
//                                addTime:[NSDate dateWithTimeIntervalSince1970:[addTime doubleValue]]
//                                 imgUrl:[topic objectForKey:@"ImgUrl"]
//                               videoUrl:[topic objectForKey:@"VideoUrl"]
//                                content:[topic objectForKey:@"MContent"]
//                               circleId:[circleID integerValue]
//                                 userId:[[topic objectForKey:@"UID"] integerValue]
//                             userAvatar:[topic objectForKey:@"UAvatar"]
//                               userName:[topic objectForKey:@"UName"]
//         ];
//        //保存评论
//        NSArray *comments = [topic objectForKey:@"Cmts"];
//        for (int i=0; i<[comments count]; i++) {
//            NSDictionary *comment = [comments objectAtIndex:i];
//            NSString *addTime = [comment objectForKey:@"AddTime"];
//            [self.dao addComment:[[comment objectForKey:@"MID"] integerValue]
//                                      topicId:[[comment objectForKey:@"Parentid"] integerValue]
//                                      addTime:[NSDate dateWithTimeIntervalSince1970:[addTime doubleValue]]
//                                       imgUrl:[comment objectForKey:@"ImgUrl"]
//                               commentContent:[comment objectForKey:@"MContent"]
//                                     circleId:[circleID integerValue]
//                                       userId:[[comment objectForKey:@"UID"] integerValue]
//                                   userAvatar:[comment objectForKey:@"UAvatar"]
//                                     userName:[comment objectForKey:@"UName"]
//             ];
//        }
//    }
//    [ZSTSqlManager commit];
    
//    if ([sortType isEqualToString:SortType_Desc]) {
//    
//        if (messages.count > 0) {
//            
//             NSString *maxMsgID = [[NSUserDefaults standardUserDefaults] valueForKey:[NSString stringWithFormat:@"SNSA_TOPIC_MAXMSGID_%@",circleID]];
//            NSString *msgId = [[messages objectAtIndex:0] objectForKey:@"MID"];
//            
//            if (!maxMsgID || maxMsgID < msgId) {
//                
//                [[NSUserDefaults standardUserDefaults] setValue:msgId forKey:[NSString stringWithFormat:@"SNSA_TOPIC_MAXMSGID_%@",circleID]];
//                
//                [[NSUserDefaults standardUserDefaults] synchronize];
//            }
//        }
//    }

    NSString *maxMsgID = [results safeObjectForKey:@"MaxMessageId"];
    if (!maxMsgID) {
        
        maxMsgID = @"0";
    }
     
    [[NSUserDefaults standardUserDefaults] setValue:maxMsgID forKey:[NSString stringWithFormat:@"SNSA_TOPIC_MAXMSGID_%@",circleID]];
    [[NSUserDefaults standardUserDefaults] synchronize];

    
    if ([self isValidDelegateForSelector:@selector(getMessagesResponse:dataFromLocal:hasMore:)]) {
        [self.delegate getMessagesResponse:messages dataFromLocal:NO hasMore:[[results safeObjectForKey:@"HasMore"] boolValue]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getUserByUID:(NSString *)UID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:UID forKey:@"UID"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_GetUserByUID
                                       postParams:params 
                                     needUserAuth:NO 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(getUserByUIDResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_GetUserByUID];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getUserByUIDResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(getUserByUIDResponse:)]) {
        [self.delegate getUserByUIDResponse:results];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)updateUserByUID:(NSString *)UID
                 msisdn:(NSString *)msisdn
               userName:(NSString *)userName
                 gender:(int)gender
                   city:(NSString *)city
                company:(NSString *)company
                address:(NSString *)address
                     qq:(NSString *)qq
                  email:(NSString *)email
                 imgUrl:(NSString *)imgUrl
               ispublic:(int)ispublic
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:UID forKey:@"UID"];
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userName forKey:@"UserName"];
    [params setSafeObject:city forKey:@"City"];
    [params setSafeObject:company forKey:@"Company"];
    [params setSafeObject:address forKey:@"Address"];
    [params setSafeObject:imgUrl forKey:@"ImgUrl"];
    [params setSafeObject:[NSNumber numberWithInt:gender] forKey:@"Gender"];
    [params setSafeObject:qq forKey:@"QQ"];
    [params setSafeObject:email forKey:@"Email"];
    [params setSafeObject:[NSNumber numberWithInt:ispublic] forKey:@"ispublic"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_UpdateUserByUID
                                       postParams:params 
                                     needUserAuth:NO 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(updateUserByUIDResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_UpdateUserByUID];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)updateUserByUIDResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(updateUserByUIDResponse)]) {
        [self.delegate updateUserByUIDResponse];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)registerMsisdn:(NSString *)msisdn verifyCode:(NSString *)verifyCode
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:TKOSVersion() forKey:@"Version"];
    [params setSafeObject:[UIDevice machine] forKey:@"UA"];
    [params setSafeObject:verifyCode forKey:@"VerifyCode"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_UserRegClient
                                       postParams:params 
                                     needUserAuth:NO 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(registerResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_UserRegClient];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)registerResponse:(id)results userInfo:(id)userInfo
{
    NSNumber *UID = [results objectForKey:@"UID"];
    if (UID != nil) {
        if ([self isValidDelegateForSelector:@selector(registerResponse:)]) {
            
            [self.delegate registerResponse:[UID unsignedIntegerValue]];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(requestDidFail:method:)]) {
            
            NSError *error = [NSError errorWithDomain:ZSTRequestErrorDomain code:ZSTResultCode_DataIllegal userInfo:[NSDictionary dictionaryWithObjectsAndKeys: @"UID must not be empty.", NSLocalizedDescriptionKey,nil]];
            [self.delegate requestDidFail:error method:(NSString *)userInfo];
        }
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)sendVerifyCode:(NSString *)msisdn
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_SendVerifyCode
                                       postParams:params 
                                     needUserAuth:NO 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(sendVerifyCodeResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_SendVerifyCode];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)sendVerifyCodeResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(sendVerifyCodeResponse)]) {
        [self.delegate sendVerifyCodeResponse];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getCommentsByMID:(NSString *)msgID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:msgID forKey:@"MID"];
    
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:ZSTHttpMethod_GetCommentsByMID
                                       postParams:params 
                                     needUserAuth:YES 
                                         delegate:self 
                                     failSelector:@selector(requestDidFail:userInfo:) 
                                  succeedSelector:@selector(getCommentsByMIDResponse:userInfo:) 
                                         userInfo:ZSTHttpMethod_GetCommentsByMID];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getCommentsByMIDResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(getCommentsByMIDResponse:)]) {
        [self.delegate getCommentsByMIDResponse:[results safeObjectForKey:@"Info"]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)upLoadFile:(NSData *)data progressDelegate:(id)progress suffix:(NSString *)suffix
{    
    [[ZSTSNSCommunicator shared] uploadFileData:data 
                                      suffix:suffix 
                                    delegate:self 
                                failSelector:@selector(requestDidFail:userInfo:) 
                             succeedSelector:@selector(upLoadFileResponse:userInfo:)  
                            progressDelegate:progress
                                    userInfo:nil];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)upLoadVideoFile:(NSData *)data progressDelegate:(id)progress suffix:(NSString *)suffix
{
    [[ZSTSNSCommunicator shared] uploadVideoData:data
                                         suffix:suffix
                                       delegate:self
                                   failSelector:@selector(requestDidFail:userInfo:)
                                succeedSelector:@selector(upLoadvideoFileResponse:userInfo:)
                               progressDelegate:progress
                                       userInfo:nil];

}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)upLoadvideoFileResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(upLoadVideoFileResponse:)]) {
        [self.delegate upLoadVideoFileResponse:[results safeObjectForKey:@"FileKey"]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)upLoadFileResponse:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(upLoadFileResponse:)]) {
        [self.delegate upLoadFileResponse:[results safeObjectForKey:@"FileKey"]];
    }
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)getMessages:(NSString *)circleID atPage:(int)pageNum
{
    
    NSString *dbMaxId = [self.dao getMaxTopicID:circleID];
	
	//如果是loadMore (pageNum>1),则从本地数据获取
	if (pageNum>1){
        
//        NSArray *tempLocalWeibos = [[ZSTYouYunDao share] getLocalWeibosBetween:nil max:newWeibosMinId];
        NSArray *tempLocalWeibos = [self.dao messagesOfCircle:circleID atPage:pageNum pageCount:TOPIC_PAGE_SIZE];
        if ([tempLocalWeibos count] < TOPIC_PAGE_SIZE) {
            
            NSString *newWeibosMinId = nil;
            if ([tempLocalWeibos count]>0) {
                newWeibosMinId = [[tempLocalWeibos objectAtIndex:[tempLocalWeibos count]-1] safeObjectForKey:@"mblogid"];
            } else {
                newWeibosMinId = [[self.dao messagesOfCircle:circleID atIndex:(pageNum - 1) * TOPIC_PAGE_SIZE] safeObjectForKey:@"mblogid"];
            }
            
            [self getMessages:circleID messageID:newWeibosMinId sortType:SortType_Asc fetchCount:TOPIC_PAGE_SIZE - [tempLocalWeibos count]];
        } else {
            if ([self isValidDelegateForSelector:@selector(getMessagesResponse:dataFromLocal:hasMore:)]) {
                [self.delegate getMessagesResponse:tempLocalWeibos dataFromLocal:YES hasMore:YES];
            }
        }
        
	} else {
		
        [self getMessages:circleID messageID:dbMaxId sortType:SortType_Asc fetchCount:TOPIC_PAGE_SIZE];
	}
}
-(void)joinCircle:(NSString *) cid flag:(int)flag
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSString stringWithFormat:@"%@",cid] forKey:@"CID"];
    [params setSafeObject:[ZSTF3Preferences shared].UID forKey:@"UID"];
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].phoneNumber forKey:@"Msisdn"];
    [params setSafeObject:[NSNumber numberWithInt:flag] forKey:@"knowflag"];
//    [params setSafeObject:nil forKey:@"UID"];
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:[ZSTHttpMethod_JionCircles stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                          postParams:params
                                        needUserAuth:NO
                                            delegate:self
                                        failSelector:@selector(jionCirclesDidFail:userInfo:)
                                     succeedSelector:@selector(jionCirclesResponse:userInfo:)
                                            userInfo:ZSTHttpMethod_GetCircles];
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void)jionCirclesDidFail:(id)results userInfo:(id)userInfo
{
    if ([self isValidDelegateForSelector:@selector(jionCircleFailed)]) {
        [self.delegate jionCircleFailed];
    }
}
- (void)jionCirclesResponse:(id)results userInfo:(id)userInfo
{
    
    if ([self isValidDelegateForSelector:@selector(jionCircleSuccess)]) {
        [self.delegate jionCircleSuccess];
    }
}

- (void)uploadFile:(NSArray *)imgDataArr Content:(NSString *)contentString
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    for (NSInteger i = 0; i < imgDataArr.count; i++)
    {
        NSString *keyName = [NSString stringWithFormat:@"image%@",@(i)];
        NSData *keyData = imgDataArr[i];
        NSString *keyBaseString = [keyData base64Encoding];
        [params setSafeObject:keyBaseString forKey:keyName];
    }
    
    [params setSafeObject:contentString forKey:@"content"];
    [params setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[SendNineImgUrl stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[ZSTF3Preferences shared].UID ,@"UID",[ZSTF3Preferences shared].ECECCID,@"ECID",nil]]
                                         params:params
                                         target:self
                                       selector:@selector(snsaUploadFile:UserInfo:)
                                       userInfo:nil
                                      matchCase:NO];
}

- (void)snsaUploadFile:(ZSTResponse *)response UserInfo:(id)userInfo
{
    NSLog(@"upResponse --> %@",response.jsonResponse);
}

- (void)getUserInfoWithMsisdn:(NSString *)msisdn userId:(NSString *)userid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:msisdn forKey:@"Msisdn"];
    [params setSafeObject:userid forKey:@"UserId"];
    
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetUserInfo
                                         params:params
                                         target:self
                                       selector:@selector(getUserInfoResponse:userInfo:)
                                       userInfo:nil];
}


- (void)getUserInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"code"] intValue] == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getUserInfoDidSucceed:)]) {
            [self.delegate getUserInfoDidSucceed:response.jsonResponse];
        }
    } else {
        
        NSString *message = [response.jsonResponse safeObjectForKey:@"notice"];
        
        if ([self isValidDelegateForSelector:@selector(getUserInfoDidFailed:)]) {
            [self.delegate getUserInfoDidFailed:message];
        }
    }
}

- (void)getMyCircleTopic:(NSString *)circleID messageID:(NSString *)MID Flag:(NSString *)topicFlag sortType:(NSString *)sortType PageSize:(NSInteger)pageSize
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleID forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:MID forKey:@"currmessageid"];
    [param setSafeObject:topicFlag forKey:@"getflag"];
    [param setSafeObject:sortType forKey:@"sorttype"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [param setSafeObject:@(pageSize) forKey:@"pagesize"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_MESSAGESINFO params:param target:self selector:@selector(getMyMessagesResponse:userInfo:) userInfo:nil];
}

- (void)sendNewsTopic:(NSString *)circleId TopicContent:(NSString *)content ImgArr:(NSArray *)imageArr
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:content forKey:@"content"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    for (NSInteger i = 0; i < imageArr.count; i++)
    {
        NSString *keyName = [NSString stringWithFormat:@"image%@",@(i)];
        NSData *keyData = imageArr[i];
        NSString *keyBaseString = [keyData base64Encoding];
        [param setSafeObject:keyBaseString forKey:keyName];
    }
    
    if (imageArr.count == 1)
    {
        NSData *keyData = imageArr[0];
        UIImage *oneImg = [UIImage imageWithData:keyData];
        [param setValue:@(oneImg.size.width) forKey:@"oneImgWidth"];
        [param setValue:@(oneImg.size.height) forKey:@"oneImgHeight"];
    }
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_SENDNEWSTOPIC params:param target:self selector:@selector(sendNewsTopicResponse:userInfo:) userInfo:nil];
}

- (void)sendNewsTopicResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"sendresponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(sendNewsTopicDidSuccess:)])
        {
            [self.delegate sendNewsTopicDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(sendNewsTopicDidFailure:)])
        {
            [self.delegate sendNewsTopicDidFailure:[response.jsonResponse safeObjectForKey:@"Notice"]];
        }
    }
}

- (void)sendThumbop:(NSString *)circleId messageID:(NSString *)MID ThumType:(NSString *)thumType
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:thumType forKey:@"state"];
    [param setSafeObject:@"1" forKey:@"type"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_THUMBBUP params:param target:self selector:@selector(sendThumbupResponse:userInfo:) userInfo:nil];
}

- (void)sendThumbupResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(sendThumbopDidSuccess:)])
        {
            [self.delegate sendThumbopDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(sendThumbopDidFailure:)])
        {
           
        }
    }
}

- (void)sendComments:(NSString *)circleId MID:(NSString *)MID Comments:(NSString *)comments ParentId:(NSString *)parentId TouristId:(NSString *)touruserId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:comments forKey:@"comments"];
    [param setSafeObject:parentId forKey:@"parentid"];
    [param setSafeObject:touruserId forKey:@"touserid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_MESSAGECOMMENTS params:param target:self selector:@selector(sendCommentResponse:userInfo:) userInfo:nil];
}

- (void)sendCommentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"commentResponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(sendCommentsDidSuccess:)])
        {
            [self.delegate sendCommentsDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(sendCommentsDidFailure:)])
        {
            [self.delegate sendCommentsDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

- (void)getAllComments:(NSString *)circleId MID:(NSString *)MID PageSize:(NSInteger)pageSize currentMsgId:(NSString *)currentMsgId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:[NSString stringWithFormat:@"%@",@(pageSize)] forKey:@"pagesize"];
    [param setSafeObject:currentMsgId forKey:@"curmsgid"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_GETALLCOMMENTS params:param target:self selector:@selector(getAllCommentsResponse:userInfo:) userInfo:nil];
    
}

- (void)getAllCommentsResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"commentResponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(getAllCommentsDidSuccess:)])
        {
            [self.delegate getAllCommentsDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(getAllCommentsDidFailure:)])
        {
            [self.delegate getAllCommentsDidFailure:@""];
        }
    }
}

- (void)getThumBop:(NSString *)circleId MID:(NSString *)MID PageSize:(NSInteger)pageSize currentMsgId:(NSString *)currentMsgId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:[NSString stringWithFormat:@"%@",@(pageSize)] forKey:@"pagesize"];
    [param setSafeObject:currentMsgId forKey:@"curmsgid"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_GETALLTHUMBOP params:param target:self selector:@selector(getAllThumbopResponse:userInfo:) userInfo:nil];
}

- (void)getAllThumbopResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"commentResponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(getAllThumbopDidSuccess:)])
        {
            [self.delegate getAllThumbopDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(getAllThumbopDidFailure:)])
        {
            [self.delegate getAllThumbopDidFailure:@""];
        }
    }
}

- (void)makeTopicTop:(NSString *)circleId MID:(NSString *)MID IsTop:(BOOL)isTop
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:isTop?@"1":@"0" forKey:@"istop"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_MESSAGETOP params:param target:self selector:@selector(makeTopicTopResponse:userInfo:) userInfo:nil];
}

- (void)makeTopicTopResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"topicResponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(makeTopicTopDidSuccess:)])
        {
            [self.delegate makeTopicTopDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(makeTopicTopDidFailure:)])
        {
            [self.delegate makeTopicTopDidFailure:[[response.jsonResponse safeObjectForKey:@"resultCode"] integerValue]];
        }
    }
}

- (void)removeTopic:(NSString *)MID CircleId:(NSString *)circleId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_DELETETOPIC params:param target:self selector:@selector(removeTopicResponse:userInfo:) userInfo:nil];
}

- (void)removeTopicResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"removeresponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(removeTopicDidSuccess:)])
        {
            [self.delegate removeTopicDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(removeTopicDidFailure:)])
        {
            [self.delegate removeTopicDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

- (void)sendTopicReport:(NSString *)circleId MID:(NSString *)MID Reason:(NSString *)reason Remark:(NSString *)remark
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:MID forKey:@"mid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:reason forKey:@"reason"];
    [param setSafeObject:remark forKey:@"remark"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_REPORT params:param target:self selector:@selector(sendTopicReportResponse:userInfo:) userInfo:nil];
}

- (void)sendTopicReportResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"reportResponse --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(sendTopicReportDidSuccess:)])
        {
            [self.delegate sendTopicReportDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(sendTopicReportDidFailiure:)])
        {
            [self.delegate sendTopicReportDidFailiure:[response.jsonResponse safeObjectForKey:@"Notice"]];
        }
    }
}

- (void)getAboutMe:(NSString *)circleId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [param setSafeObject:circleId forKey:@"cid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_ABOUTME params:param target:self selector:@selector(getAboutMeResponse:userInfo:) userInfo:nil];
}

- (void)getAboutMeResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"response --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self isValidDelegateForSelector:@selector(getAboutMeDidSuccess:)])
        {
            [self.delegate getAboutMeDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(getAboutMeDidFailure:)])
        {
            [self.delegate getAboutMeDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

- (void)getOneTopic:(NSString *)mid
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:mid forKey:@"mid"];
    [param setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_GETONETOPIC params:param target:self selector:@selector(getOneTopicResponse:userInfo:) userInfo:nil];
}

- (void)getOneTopicResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"responseTopic --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self.delegate respondsToSelector:@selector(getOneTopicDidSuccess:)])
        {
            [self.delegate getOneTopicDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(getOneTopicDidFailure:)])
        {
            [self.delegate getOneTopicDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

- (void)getMessageHasRead:(NSString *)circleId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:circleId forKey:@"cid"];
    [param setValue:[ZSTF3Preferences shared].UID forKey:@"uid"];
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_MESSAGECOMMENTREAD params:param target:self selector:@selector(getMessageHasReadResponse:userInfo:) userInfo:nil];
}

- (void)getMessageHasReadResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"responseJson --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self.delegate respondsToSelector:@selector(getMessageHasReadDidSuccess:)])
        {
            [self.delegate getMessageHasReadDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(getMessageHasReadDidFailure:)])
        {
            [self.delegate getMessageHasReadDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

- (void)getNewsMessageCount:(NSString *)circleId MID:(NSString *)MID
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:circleId forKey:@"cid"];
    [param setValue:MID forKey:@"mid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_GETNEWSMESSAGECOUNT params:param target:self selector:@selector(getNewsMessageCountResponse:userInfo:) userInfo:nil];
}

- (void)getNewsMessageCountResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"%@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self.delegate respondsToSelector:@selector(getNewsMessageCountDidSuccess:)])
        {
            [self.delegate getNewsMessageCountDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(getNewsMessageCountDidFailure:)])
        {
            [self.delegate getNewsMessageCountDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

- (void)getCircleNewsComments:(NSString *)circleId MaxCommentsId:(NSString *)mcid MaxThumbId:(NSString *)maxThumbId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:circleId forKey:@"cid"];
    [param setValue:mcid forKey:@"mcid"];
    [param setValue:maxThumbId forKey:@"mtid"];
    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_GETCIRCLENEWSCOMMENTS params:param target:self selector:@selector(getCircleNewsCommentsResponse:userInfo:) userInfo:nil];
}

- (void)getCircleNewsCommentsResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"responseJson --> %@",response.jsonResponse);
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self.delegate respondsToSelector:@selector(getCircleNewsCommentsDidSuccess:)])
        {
            [self.delegate getCircleNewsCommentsDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(getCircleNewsCommentsDidFailure:)])
        {
            [self.delegate getCircleNewsCommentsDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
    
}

- (void)getAboutMeCount:(NSString *)circleId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:circleId forKey:@"cid"];
    [param setSafeObject:[ZSTF3Preferences shared].UID forKey:@"uid"];
    
    [[ZSTCommunicator shared] openAPISNSAPostToPath:ZSTSNSA_GETABOUTMECOUNT params:param target:self selector:@selector(getAboutMeCountResponse:userInfo:) userInfo:nil];
}

- (void)getAboutMeCountResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if ([[response.jsonResponse safeObjectForKey:@"result"] integerValue] == 1)
    {
        if ([self.delegate respondsToSelector:@selector(getAboutMeCountDidSuccess:)])
        {
            [self.delegate getAboutMeCountDidSuccess:response.jsonResponse];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(getAboutMeCountDidFailure:)])
        {
            [self.delegate getAboutMeDidFailure:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

#pragma mark - 获取圈子列表
- (void)getMyCircles:(NSString *)cInfo
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSString stringWithFormat:@"%@",cInfo] forKey:@"CInfo"];
    [params setSafeObject:[ZSTF3Preferences shared].imsi forKey:@"IMEI"];
    [params setSafeObject:@(self.moduleType).stringValue forKey:@"ModuleType"];
    [params setSafeObject:[ZSTF3Preferences shared].UID forKey:@"UID"];
    [[ZSTSNSCommunicator shared] openAPIPostToMethod:[ZSTHttpMethod_GetCircles stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                          postParams:params
                                        needUserAuth:NO
                                            delegate:self
                                        failSelector:@selector(requestDidFail:userInfo:)
                                     succeedSelector:@selector(getMyCirclesResponse:userInfo:)
                                            userInfo:ZSTHttpMethod_GetCircles];
}

- (void)getMyCirclesResponse:(id)response userInfo:(id)userInfo
{
    if ([[response safeObjectForKey:@"Result"] integerValue] == 1)
    {
        if ([response safeObjectForKey:@"UID"]) {
            int uid = [[response safeObjectForKey:@"UID"]intValue];
            [[ZSTF3Preferences shared] setUID:[NSString stringWithFormat:@"%d",uid]];
        }
        
        if ([self.delegate respondsToSelector:@selector(getMyCirclesDidSuccess:)])
        {
            [self.delegate getMyCirclesDidSuccess:response];
        }
    }
    else
    {
        if ([self.delegate respondsToSelector:@selector(getMyCirclesDidFailure:)])
        {
            [self.delegate getMyCirclesDidFailure:[response safeObjectForKey:@"Notice"]];
        }
    }
}



@end
