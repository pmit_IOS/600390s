//
//  CTFrameParser.h
//  CoreTextDemo
//
//  Created by pmit on 15/10/19.
//  Copyright © 2015年 com.9588.f3. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreText/CoreText.h>
#import "CoreTextData.h"
#import "CTFrameParserConfig.h"
#import "CTFrameParser.h"

@interface CTFrameParser : NSObject

@property (assign,nonatomic) BOOL isDetailComments;

- (CoreTextData *)parseContent:(NSString *)content config:(CTFrameParserConfig*)config;
- (NSDictionary *)attributesWithConfig:(CTFrameParserConfig *)config ;
- (NSAttributedString *)parseAttributedContentFromNSDictionary:(NSDictionary *)dict
                                                        config:(CTFrameParserConfig*)config;
- (CoreTextData *)parseAttributedContent:(NSAttributedString *)content config:(CTFrameParserConfig*)config;
- (CoreTextData *)parseTemplateFileContent:(NSString *)content config:(CTFrameParserConfig *)config UserMsisdn:(NSString *)userMsisdn ToUserMsisdn:(NSString *)toUserMsisdn;
- (CoreTextData *)parseTemplateTopicContent:(NSString *)content config:(CTFrameParserConfig *)config;

@end
