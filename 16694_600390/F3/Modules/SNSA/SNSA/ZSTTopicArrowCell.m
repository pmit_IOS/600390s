//
//  ZSTTopicArrowCell.m
//  SNSA
//
//  Created by pmit on 15/10/22.
//
//

#import "ZSTTopicArrowCell.h"

@implementation ZSTTopicArrowCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.arrowIV)
    {
        self.arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(25, 0, 15, 5)];
        [self.arrowIV setImage:[UIImage imageNamed:@"chatroom_arrow.png"]];
        [self.contentView addSubview:self.arrowIV];
    }
}

- (void)setCellData:(ZSTSNSAMessage *)message
{
    if (message.thumArr.count == 0 && message.cgArr.count == 0)
    {
        self.arrowIV.hidden = YES;
    }
    else
    {
        self.arrowIV.hidden = NO;
    }
}

@end
