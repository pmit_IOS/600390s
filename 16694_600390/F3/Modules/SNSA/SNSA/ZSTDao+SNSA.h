
#import <Foundation/Foundation.h>
#import "ZSTSNSAMessage.h"
#import "ZSTSNSAMessageComments.h"
#import "ZSTSNSAThumShare.h"

@interface ZSTDao(SNSA)

- (void)createTableIfNotExistForSnsbModule;

- (BOOL)addMessage:(NSString *)mID
          circleID:(NSString *)circleID
            userID:(NSString *)userID
           content:(NSString *)content
            imgUrl:(NSString *)imgUrl
           addTime:(NSDate *)time;

- (BOOL)addTopic:(NSInteger)topicId
         addTime:(NSDate *)time
          imgUrl:(NSString *)imgUrl
        videoUrl:(NSString *)videoUrl
         content:(NSString *)topicContent
        circleId:(NSInteger)circleId
          userId:(NSInteger)userId
      userAvatar: (NSString *)userAvatar
        userName: (NSString *)userName;

- (BOOL)addComment:(NSInteger)commentId
           topicId:(NSInteger)topicId
           addTime:(NSDate *)time
            imgUrl:(NSString *)imgUrl
    commentContent:(NSString *)commentContent
          circleId:(NSInteger)circleId
            userId:(NSInteger)userId
        userAvatar:(NSString *)userAvatar
          userName:(NSString *)userName;

- (NSArray *)topicAndCommentOfCircle:(NSString *)circleId 
                             startId:(NSInteger)startTopic
                          topicCount:(NSInteger)topicCount
                        commentCount:(NSInteger)commentCount;


- (BOOL)deleteMessage:(NSString *)mID;

- (BOOL)messageExist:(NSString *)mID;

- (NSDictionary *)messagesOfCircle:(NSString *)circleID atIndex:(int)index;

- (NSArray *)messagesOfCircle:(NSString *)circleID  atPage:(int)pageNum pageCount:(int)pageCount;

- (NSUInteger)messagesCountOfCircle:(NSString *)circleID;

- (NSString *) getMaxTopicID:(NSString *)circleID;

- (BOOL)deleteTopicOfCircle:(NSString *)circleId;

- (BOOL)deleteCommentOfCircle: (NSString *)circleId;

- (BOOL)saveComments:(NSArray *)comments circle:(NSString *)circleId;

- (void)createChatHistoryTable:(NSString *)circleID;

- (void)insertCircleMessage:(ZSTSNSAMessage *)messageDic CircleId:(NSString *)circleId MessageArrString:(NSString *)messageArrString;

- (NSArray *)selectedCircleMessage:(NSString *)circleID;

- (void)removeTopic:(NSString *)circleId MID:(NSString *)MID;

- (void)createCommentTable:(NSString *)circleID;

- (void)insertTopicComments:(ZSTSNSAMessageComments *)newsComments CircleId:(NSString *)circleId;

- (NSArray *)selectedCommentsWith:(NSString *)circleId MID:(NSString *)MID;

- (void)removeComments:(NSString *)circleId MID:(NSString *)MID;

- (void)updateTopic:(NSString *)circleId MID:(NSString *)MID NewsMessage:(ZSTSNSAMessage *)messageDic;

- (NSArray *)selectTopThreeComments:(NSString *)circleId MID:(NSString *)MID;

- (NSArray *)selectTopMessageDic:(NSString *)circleId IsTop:(BOOL)isTop;

- (void)createThumMan;

- (void)removeThumb:(NSString *)circleId MID:(NSString *)mid UID:(NSString *)uid;

- (void)insertThumMan:(ZSTSNSAThumShare *)thumShare CircleId:(NSString *)circleId MID:(NSString *)mid;

- (NSArray *)selectThumeShare:(NSString *)circleId MID:(NSString *)mid IsShare:(BOOL)isShare;

- (NSArray *)selectOneTopic:(NSString *)MID;

- (NSArray *)selectMaxMsgId:(NSString *)circleId;

- (NSArray *)selectMinMsgId:(NSString *)circleId;

- (NSArray *)selectMaxCommentsId:(NSString *)circleId;

- (NSArray *)selectMaxThumbId:(NSString *)circleId;

- (void)removeTopicAllThumb:(NSString *)circleId MID:(NSString *)MID;

- (void)removeTopicByMID:(NSString *)circleId MaxMid:(NSInteger)maxMid;

@end
