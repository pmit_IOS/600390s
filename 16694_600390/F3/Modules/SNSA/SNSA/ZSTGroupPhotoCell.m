//
//  ZSTGroupPhotoCell.m
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import "ZSTGroupPhotoCell.h"
#import <PMRepairButton.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import "ZSTMyGroupPhotoViewController.h"

@implementation ZSTGroupPhotoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.oneIV)
    {
        self.oneIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (WIDTH - 15) / 4, (WIDTH - 15) / 4)];
        self.oneIV.tag = 1;
        self.oneIV.userInteractionEnabled = YES;
//        UIImageView *oneImg = [[UIImageView alloc] initWithFrame:CGRectMake(self.oneIV.bounds.size.width - 25, 0, 25, 25)];
//        [oneImg setImage:[UIImage imageNamed:@"checkNo.png"]];
//        oneImg.tag = 101 * 100;
//        [self.oneIV addSubview:oneImg];
//        self.oneBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(self.oneIV.bounds.size.width - 25, 0, 25, 25)];
//        self.oneBtn.tag = 101;
        
        [self.oneBtn addTarget:self action:@selector(selectPicOrNot:) forControlEvents:UIControlEventTouchUpInside];
        [self.oneIV addSubview:self.oneBtn];
        [self.contentView addSubview:self.oneIV];
        
        self.twoIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.oneIV.frame) + 5, 0, (WIDTH - 15) / 4, (WIDTH - 15) / 4)];
        self.twoIV.tag = 2;
        self.twoIV.userInteractionEnabled = YES;
        UIImageView *twoImg = [[UIImageView alloc] initWithFrame:CGRectMake(self.twoIV.bounds.size.width - 25, 0, 25, 25)];
        [twoImg setImage:[UIImage imageNamed:@"checkNo.png"]];
        twoImg.tag = 102 * 100;
        [self.twoIV addSubview:twoImg];
        self.twoBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(self.twoIV.bounds.size.width - 25, 0, 25, 25)];
        self.twoBtn.tag = 102;
        [self.twoBtn addTarget:self action:@selector(selectPicOrNot:) forControlEvents:UIControlEventTouchUpInside];
        [self.twoIV addSubview:self.twoBtn];
        [self.contentView addSubview:self.twoIV];
        
        self.threeIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.twoIV.frame) + 5, 0, (WIDTH - 15) / 4, (WIDTH - 15) / 4)];
        self.threeIV.tag = 3;
        self.threeIV.userInteractionEnabled = YES;
        UIImageView *threeImg = [[UIImageView alloc] initWithFrame:CGRectMake(self.threeIV.bounds.size.width - 25, 0, 25, 25)];
        [threeImg setImage:[UIImage imageNamed:@"checkNo.png"]];
        threeImg.tag = 103 * 100;
        [self.threeIV addSubview:threeImg];
        self.threeBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(self.threeIV.bounds.size.width - 25, 0, 25, 25)];
        self.threeBtn.tag = 103;
        [self.threeBtn addTarget:self action:@selector(selectPicOrNot:) forControlEvents:UIControlEventTouchUpInside];
        [self.threeIV addSubview:self.threeBtn];
        [self.contentView addSubview:self.threeIV];
        
        self.fourIV = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.threeIV.frame) + 5, 0, (WIDTH - 15) / 4, (WIDTH - 15) / 4)];
        self.fourIV.tag = 4;
        self.fourIV.userInteractionEnabled = YES;
        UIImageView *fourImg = [[UIImageView alloc] initWithFrame:CGRectMake(self.fourIV.bounds.size.width - 25, 0, 25, 25)];
        [fourImg setImage:[UIImage imageNamed:@"checkNo.png"]];
        fourImg.tag = 104 * 100;
        [self.fourIV addSubview:fourImg];
        self.fourBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(self.fourIV.bounds.size.width - 25, 0, 25, 25)];
        self.fourBtn.tag = 104;
        [self.fourBtn addTarget:self action:@selector(selectPicOrNot:) forControlEvents:UIControlEventTouchUpInside];
        [self.fourIV addSubview:self.fourBtn];
        [self.contentView addSubview:self.fourIV];
    }
    
    
    UIImageView *onesIV = [self.oneBtn.superview viewWithTag:self.oneBtn.tag * 100];
    UIImageView *twosIV = [self.twoBtn.superview viewWithTag:self.twoBtn.tag * 100];
    UIImageView *thresssIV = [self.threeBtn.superview viewWithTag:self.threeBtn.tag * 100];
    UIImageView *foursIV = [self.fourBtn.superview viewWithTag:self.fourBtn.tag * 100];
    
    if (self.oneBtn.selected)
    {
        [onesIV setImage:[UIImage imageNamed:@"checkYes.png"]];
    }
    else
    {
        [onesIV setImage:[UIImage imageNamed:@"checkNo.png"]];
    }
    
    if (self.twoBtn.selected)
    {
        [twosIV setImage:[UIImage imageNamed:@"checkYes.png"]];
    }
    else
    {
        [twosIV setImage:[UIImage imageNamed:@"checkNo.png"]];
    }
    
    if (self.threeBtn.selected)
    {
        [thresssIV setImage:[UIImage imageNamed:@"checkYes.png"]];
    }
    else
    {
        [thresssIV setImage:[UIImage imageNamed:@"checkNo.png"]];
    }
    
    if (self.fourBtn.selected)
    {
        [foursIV setImage:[UIImage imageNamed:@"checkYes.png"]];
    }
    else
    {
        [foursIV setImage:[UIImage imageNamed:@"checkNo.png"]];
    }
}

- (void)setCellData:(NSArray *)photoArr
{
    self.photoArr = photoArr;
    
    for (NSInteger i = photoArr.count; i < 4; i++)
    {
        UIImageView *iv = (UIImageView *)[self.contentView viewWithTag:i + 1];
        iv.hidden = YES;
    }
    
    for (NSInteger i = 0; i < photoArr.count; i++)
    {
        UIImageView *iv = (UIImageView *)[self.contentView viewWithTag:i + 1];
        iv.hidden = NO;
        ALAsset *result = photoArr[i];
        CGImageRef  ref = [result thumbnail];
        UIImage *img = [[UIImage alloc]initWithCGImage:ref];
        iv.image = img;
    }
}

- (void)selectPicOrNot:(PMRepairButton *)sender
{
    if (self.mgpVC.selectPicArr.count < self.mgpVC.maxCount || sender.isSelected)
    {
        sender.selected = !sender.isSelected;
        UIImageView *iv = [sender.superview viewWithTag:sender.tag * 100];
        if (sender.selected)
        {
            [iv setImage:[UIImage imageNamed:@"checkYes.png"]];
        }
        else
        {
            [iv setImage:[UIImage imageNamed:@"checkNo.png"]];
        }
        NSInteger index = sender.tag - 101;
        ALAsset *result = self.photoArr[index];
        if ([self.cellDelegate respondsToSelector:@selector(passImg:IsInsert:)])
        {
            [self.cellDelegate passImg:result IsInsert:sender.isSelected];
        }
    }
}

@end
