//
//  ZSTChatroomLookPicViewController.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomLookPicViewController : UIViewController

@property (strong,nonatomic) NSArray *imageUrlArr;
@property (assign,nonatomic) NSInteger selectInteger;

@end
