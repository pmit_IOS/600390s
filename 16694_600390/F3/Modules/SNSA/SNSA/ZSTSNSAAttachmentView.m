//
//  AttachmentView.m
//  F3
//
//  Created by xuhuijun on 11-12-14.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTSNSAAttachmentView.h"


@implementation ZSTSNSAAttachmentView

@synthesize attachmentBackgroundView = _attachmentBackgroundView;
@synthesize imageView = _imageView;
@synthesize imageNameLabel = _imageNameLabel;
@synthesize count = _count;
@synthesize deleteBtn = _deleteBtn;
@synthesize delegate = _delegate;
@synthesize filePath = _filePath;
- (void)dealloc
{
    [super dealloc];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _attachmentBackgroundView = [[[UIImageView alloc] initWithFrame:CGRectMake(-5, 0, 330, 41)] autorelease];
        _attachmentBackgroundView.image = [UIImage imageNamed:@"createback.png"];
        [self addSubview:_attachmentBackgroundView];
        
        _imageView = [UIButton buttonWithType:UIButtonTypeCustom];
        _imageView.frame = CGRectMake(10 , 2.5, 32, 36);
        _imageView.userInteractionEnabled = YES;
        [self addSubview:_imageView];
        
        _imageNameLabel = [[[UILabel alloc] initWithFrame:CGRectMake(55, 0, 220, 40)] autorelease];
        _imageNameLabel.backgroundColor = [UIColor clearColor];
        _imageNameLabel.userInteractionEnabled = YES;
        _imageNameLabel.adjustsFontSizeToFitWidth = YES;
        [self addSubview:_imageNameLabel];
        
        _deleteBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _deleteBtn.frame = CGRectMake(278, 0, 40, 40);
        [_deleteBtn setTitle:@"X" forState:UIControlStateNormal];
        [_deleteBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        [_deleteBtn addTarget:self action:@selector(removeAttachmentView) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_deleteBtn];
        
        self.tag = 0;    
    }
    return self;
}

- (void)removeAttachmentView
{
    if ([_delegate respondsToSelector:@selector(attachmentViewWillDisappear:)]) {
        [_delegate attachmentViewWillDisappear:self];
    }
    
    [self removeFromSuperview];
}

@end
