//
//  ZSTChatroomLoadMoreView.h
//  SNSA
//
//  Created by pmit on 15/10/15.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomLoadMoreView : UIView

@property (strong,nonatomic) UIActivityIndicatorView *activityIndicator;
@property (strong,nonatomic) UILabel *infoLB;

- (void)becomLoading;

- (void)endLoading;

- (void)finishLoading;

@end
