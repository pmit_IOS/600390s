//
//  ZSTChatroomLoadMoreView.m
//  SNSA
//
//  Created by pmit on 15/10/15.
//
//

#import "ZSTChatroomLoadMoreView.h"

@implementation ZSTChatroomLoadMoreView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        self.activityIndicator.frame = CGRectMake(frame.size.width * 0.3, 0, frame.size.width * 0.1, frame.size.width * 0.1);
        [self addSubview:self.activityIndicator];
        self.infoLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.activityIndicator.frame), 0, frame.size.width - CGRectGetMaxX(self.activityIndicator.frame), frame.size.height)];
        self.infoLB.textAlignment = NSTextAlignmentLeft;
        self.infoLB.textColor = RGBA(59, 59, 59, 1);
        self.infoLB.font = [UIFont systemFontOfSize:13.0f];
        [self addSubview:self.infoLB];
    }
    
    return self;
}


- (void)becomLoading
{
    [self.activityIndicator startAnimating];
    self.infoLB.text = @"正在努力为您加载";
}

- (void)endLoading
{
    [self.activityIndicator stopAnimating];
}

- (void)finishLoading
{
    [self.activityIndicator stopAnimating];
    self.infoLB.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    self.infoLB.text = @"已经是最后一页啦";
}

@end
