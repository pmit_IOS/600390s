//
//  ZSTContactDetailController.m
//  YouYun
//
//  Created by luobin on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <MessageUI/MessageUI.h>

#import "ZSTContactDetailController.h"
#import "FXLabel.h"
#import "ZSTContactDetailHeaderCell.h"
#import "ZSTContactDetailLabelCell.h"
#import "ZSTUtils.h"
#import "ZSTSexSwitch.h"

//#define kZSTPropertyPhoneRow      0                 //手机
//#define kZSTPropertyCityRow       1                 //城市
//#define kZSTPropertyAddressRow    2                 //地址
//#define kZSTPropertyCompanyRow    3                 //公司
//#define kZSTPropertyEmailRow      4                 //邮箱
//#define kZSTPropertyQQRow         5                 //QQ
#define kZSTPropertyPhoneRow      0                 //手机
#define kZSTPropertyAddressRow    1                 //地址
//#define kZSTPropertyEmailRow      2                 //邮箱
#define kZSTPropertyCompanyRow    2                 //公司

@interface ZSTContactDetailController()<UITableViewDelegate, UITableViewDataSource, ZSTYouYunEngineDelegate, MFMessageComposeViewControllerDelegate>

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

@property (nonatomic, retain) UIWebView *callPhoneWebView;

@property (nonatomic, retain) UIView *bottomBackground;

@end

@implementation ZSTContactDetailController

@synthesize tableView;
@synthesize userInfo;
@synthesize engine;
@synthesize userID;
@synthesize callPhoneWebView;
@synthesize leftBtn;
@synthesize rightBtn;
@synthesize saveAddressBtn;
@synthesize isAddressSave;

- (void)dealloc
{
    self.leftBtn = nil;
    self.rightBtn = nil;
    self.callPhoneWebView = nil;
    self.userID = nil;
    self.engine = nil;
    self.tableView = nil;
    self.userInfo = nil;
    [super dealloc];
}

#pragma mark - View lifecycle

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView
{
}
*/

//- (void)viewWillAppear:(BOOL)animated
//{
//}
//
//- (void)viewWillDisappear:(BOOL)animated
//{
////    UIView *topLine = [self.navigationController.navigationBar viewWithTag:1001];
////    [topLine removeFromSuperview];
//}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"联系人详情", nil)];

    self.view.backgroundColor = [UIColor colorWithWhite:0.953 alpha:1];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
    //collect addressbook data
    NSMutableDictionary *abdic = [NSMutableDictionary dictionary];
    
    ABAddressBookRef addressBook = ABAddressBookCreate();  
    NSMutableArray* personArray = (NSMutableArray *)ABAddressBookCopyArrayOfAllPeople(addressBook);
    
    for (int i=0; i<[personArray count]; i++){         
        id person = [personArray objectAtIndex:i];  
        
        NSString *firstName = (NSString *)ABRecordCopyValue(person, kABPersonFirstNameProperty);           
        NSString *lastName = (NSString *)ABRecordCopyValue(person, kABPersonLastNameProperty);      
        
        NSMutableString *fullName = [NSMutableString string];
        if (firstName!=nil && firstName.length>0) {
            [fullName appendString:firstName];
        }
        if (lastName !=nil && lastName.length>0) {
            [fullName appendString:lastName];
        }
        [firstName release];
        [lastName release];
        
        ABMultiValueRef phones = (ABMultiValueRef) ABRecordCopyValue(person, kABPersonPhoneProperty);  
        for(int i = 0 ;i < ABMultiValueGetCount(phones); i++){    
            NSString *phone = (NSString *)ABMultiValueCopyValueAtIndex(phones, i);   
            
            NSString *realNumber = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
            realNumber = [realNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
            realNumber = [realNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
            if (realNumber!=nil && realNumber.length == 11) {
                [abdic setObject:fullName forKey:realNumber];
            }
            [phone release];
        } 
        CFRelease(phones);
    }
    [personArray release];
    CFRelease(addressBook);
    
    NSString *userName = [abdic safeObjectForKey:[self.userInfo safeObjectForKey:@"Msisdn"]];
    if (userName!=nil && userName.length >0) {
        self.isAddressSave = YES;
    }
    
    BOOL isClientUser = [[self.userInfo safeObjectForKey:@"IsClientUser"] intValue];
    
    if (!isClientUser && self.userInfo) {
        
         self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"邀请", nil) target:self selector:@selector (sendAdvice)];
    } 
    
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 416 - 52) style:UITableViewStylePlain] autorelease];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.view addSubview:self.tableView];
        
    UIView *bottomBackground = [[UIView alloc] initWithFrame:CGRectMake(0, 416 - 52+(iPhone5?88:0), self.view.width, 52)];
    bottomBackground.backgroundColor = [UIColor clearColor];
    bottomBackground.userInteractionEnabled = YES;
    self.bottomBackground = bottomBackground;
    [self.view addSubview:bottomBackground];
    [bottomBackground release];
    
//    self.leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftBtn = [[PMRepairButton alloc] init];
    leftBtn.frame = CGRectMake(0, 0, (self.view.width - 2)/2, 52);
    [leftBtn setImage:ZSTModuleImage(@"module_snsa_btn_call_n.png") forState:UIControlStateNormal];
    [leftBtn setImage:ZSTModuleImage(@"module_snsa_btn_call_p.png") forState:UIControlStateHighlighted];
    [leftBtn addTarget:self action:@selector(call) forControlEvents:UIControlEventTouchUpInside];
    [bottomBackground addSubview:leftBtn];
    
    UIImageView *bottomSeparatorImageView = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(leftBtn.frame), 0, 2, 52)];
    bottomSeparatorImageView.image = ZSTModuleImage(@"module_snsa_bottom_split.png");
    [bottomBackground addSubview:bottomSeparatorImageView];
    [bottomSeparatorImageView release];
    
//    self.rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBtn = [[PMRepairButton alloc] init];
    rightBtn.frame = CGRectMake(CGRectGetMaxX(bottomSeparatorImageView.frame), 0, (self.view.width - 2)/2, 52);
    [rightBtn setImage:ZSTModuleImage(@"module_snsa_btn_sms_n.png") forState:UIControlStateNormal];
    [rightBtn setImage:ZSTModuleImage(@"module_snsa_btn_sms_p.png") forState:UIControlStateHighlighted];
    [rightBtn addTarget:self action:@selector(sendSMS) forControlEvents:UIControlEventTouchUpInside];
    [bottomBackground addSubview:rightBtn];
//    self.leftBtn.enabled = NO;
//    self.rightBtn.enabled = NO;
    if (self.userInfo == nil && self.userID) {
        self.engine = [ZSTYouYunEngine engineWithDelegate:self];
        self.engine.moduleType = self.moduleType;
        [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
            [self.engine getUserByUID:self.userID];
        }];        
    }
}

- (void)call
{
    NSString *deviceType = [UIDevice currentDevice].model;
    
    if([deviceType  isEqualToString:@"iPod touch"]||[deviceType  isEqualToString:@"iPad"]||[deviceType  isEqualToString:@"iPhone Simulator"]){
        
         TKAlertAppNameTitle(NSLocalizedString(@"您的设备不能打电话！" , nil));
        return;
    }

    
    NSString *phoneNumber = [userInfo safeObjectForKey:@"Msisdn"];
    if (phoneNumber == nil || [phoneNumber isEmptyOrWhitespace]) {
        TKAlertAppNameTitle(NSLocalizedString(@"电话号码不能空！" , nil));
        return;
    }
    
    if  ([phoneNumber hasPrefix:@"9"]) {
        
        TKAlertAppNameTitle(NSLocalizedString(@"亲，他还没有绑定手机号哦!" , nil));
        return;
    }
    
    if (callPhoneWebView == nil) {
        self.callPhoneWebView = [[[UIWebView alloc] init] autorelease];  
    }
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@", phoneNumber]]];  
    [callPhoneWebView loadRequest:request]; 
}

- (void)sendSMS
{
    NSString *phoneNumber = [userInfo safeObjectForKey:@"Msisdn"];
    if (phoneNumber == nil || [phoneNumber isEmptyOrWhitespace]) {
        TKAlertAppNameTitle(NSLocalizedString(@"电话号码不能空！" , nil));
        return;
    }
    
    if  ([phoneNumber hasPrefix:@"9"]) {
        
        TKAlertAppNameTitle(NSLocalizedString(@"亲，他还没有绑定手机号哦!" , nil));
        return;
    }
    
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController")); 
    if (messageClass != nil) {
        
        if ([MFMessageComposeViewController canSendText]) {
            MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
            [picker.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
            picker.messageComposeDelegate = self;
            picker.recipients = [NSArray arrayWithObjects:phoneNumber, nil];
            [self presentModalViewController: picker animated:YES];
            [picker release];
        } else {
            TKAlertAppNameTitle(NSLocalizedString(@"您的设备不能发短信！" , nil));
        }
        
    } else {
        
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms://%@", phoneNumber]]];  
    }
}

- (void)sendAdvice 
{
     NSString *phoneNumber = [userInfo safeObjectForKey:@"Msisdn"];
    
    if (phoneNumber == nil || [phoneNumber isEmptyOrWhitespace]) {
        TKAlertAppNameTitle(NSLocalizedString(@"电话号码不能空！" , nil));
        return;
    }    

    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController")); 
    if (messageClass != nil) {
        if ([MFMessageComposeViewController canSendText]) {
            
            NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];

            MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
            picker.messageComposeDelegate = self;
            NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
        picker.body = [NSString stringWithFormat:NSLocalizedString(@"GP_SMSRecommendMessage", nil),appDisplayName,sharebaseurl];
            picker.recipients = [NSArray arrayWithObjects:phoneNumber, nil];
            
            [self presentModalViewController: picker animated:YES];
            [picker release];
        } else {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示信息", @"") 
                                                            message:NSLocalizedString(@"该设备不支持短信功能", @"") 
                                                           delegate:self 
                                                  cancelButtonTitle:nil
                                                  otherButtonTitles:NSLocalizedString(@"确定", @""), nil];
            [alert show];
            [alert release];
        }
    } else {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"sms://%@", phoneNumber]]];          
    }
}


#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (indexPath.section || indexPath.row)? 64 : 100;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return section ? 3 : 1;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.userInfo? 2 : 0;
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        
        static NSString *identifier = @"ZSTContactDetailHeaderCell";
        ZSTContactDetailHeaderCell *cell = [theTableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTContactDetailHeaderCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.avtarImageView clear];
            NSString *fileKey = [self.userInfo safeObjectForKey:@"New_AvatarImgUrl"];
            if (fileKey == nil) {
                fileKey = @"";
            }
            NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", fileKey]];
            cell.avtarImageView.url = url;
            [cell.avtarImageView loadImage];
            cell.nameLabel.text = [self.userInfo safeObjectForKey:@"New_UserName"];
            cell.locationLabel.text = [self.userInfo safeObjectForKey:@"City"];
            cell.sexSwitch.sex = [[self.userInfo safeObjectForKey:@"New_Gender"] intValue];
            
            UserType userType = [[self.userInfo safeObjectForKey:@"UType"] intValue];
            cell.adminImageView.hidden = !(userType == UserType_Admin || userType == UserType_Creator);
            cell.stateImageView.hidden = ([[self.userInfo safeObjectForKey:@"IsClientUser"] intValue] != 1);
            
        }
        return cell;
        
    } else {
        static NSString *identifier = @"LabelCell";
        ZSTContactDetailLabelCell *cell = [theTableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil) {
            cell = [[[ZSTContactDetailLabelCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            
            cell.textLabel.font = [UIFont systemFontOfSize:13];
            cell.textLabel.textColor = [UIColor colorWithRed:0.38 green:0.49 blue:0.57 alpha:1];
            cell.textLabel.highlightedTextColor = [UIColor whiteColor];
            cell.textLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
            cell.textLabel.shadowOffset = CGSizeMake(0, 1.0f);
            cell.backgroundColor = RGBCOLOR(250, 250, 250);
            
            cell.labelWidth = 90.f;
            cell.label.textAlignment = NSTextAlignmentLeft;
            cell.label.backgroundColor = [UIColor clearColor];
            cell.label.font = [UIFont systemFontOfSize:13];
            cell.label.textColor = [UIColor colorWithRed:0.38 green:0.49 blue:0.57 alpha:1];
            cell.label.highlightedTextColor = [UIColor whiteColor];
            cell.label.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
            cell.label.shadowOffset = CGSizeMake(0, 1.0f);
        }
        if (indexPath.row == kZSTPropertyPhoneRow) {
            cell.textLabel.text = NSLocalizedString(@"   手机", nil);
            NSString *string = [self.userInfo safeObjectForKey:@"Msisdn"];
            if ([[self.userInfo safeObjectForKey:@"New_ispublic"] intValue] == 1) {
                
                self.leftBtn.enabled = YES;
                self.rightBtn.enabled = YES;
                
                self.bottomBackground.hidden = NO;
                
            } else {
                
                string = [string stringByReplacingCharactersInRange:NSMakeRange(3,4) withString:@"****"];
                self.leftBtn.enabled = NO;
                self.rightBtn.enabled = NO;
                
                self.bottomBackground.hidden = YES;
            }
            
            cell.label.text = string;
            
        } else if(indexPath.row == kZSTPropertyAddressRow) {
            cell.textLabel.text = NSLocalizedString(@"   地址", nil);
            cell.label.text = [self.userInfo safeObjectForKey:@"New_Address"];
        } else if(indexPath.row == kZSTPropertyCompanyRow) {
            cell.textLabel.text = NSLocalizedString(@"   个性签名", nil);
            cell.label.text = [self.userInfo safeObjectForKey:@"New_Signature"];
        }
        return cell;
    }
}

#pragma mark - ZSTYouYunEngineDelegate
- (void)getUserByUIDResponse:(NSDictionary *)theUserInfo
{
    self.leftBtn.enabled = YES;
    self.rightBtn.enabled = YES;
    
    [self.view removeNetworkIndicatorView];
    self.userInfo = [[theUserInfo mutableCopy] autorelease];
    
    [self.tableView insertSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, 2)] withRowAnimation:UITableViewRowAnimationTop];
}

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    [self.view refreshFailed];
}

#pragma mark - MFMessageComposeViewControllerDelegate
- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result == MessageComposeResultSent) {
        TKAlertAppNameTitle(NSLocalizedString(@"推荐好友短信发送成功", nil));
    } else if (result == MessageComposeResultFailed) {
        TKAlertAppNameTitle(NSLocalizedString(@"推荐好友短信发送失败", nil));
    }
}

-(void)saveToAddressBook {
    
    ABNewPersonViewController *newPerson = [[ABNewPersonViewController alloc] init];
    [newPerson.navigationController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    ABRecordRef personRecord = ABPersonCreate();
    CFErrorRef error = NULL;
    ABRecordSetValue(personRecord, kABPersonFirstNameProperty, [self.userInfo safeObjectForKey:@"New_UserName"] , &error);
    //这里有个问题就是姓名没法分开
    ABRecordSetValue(personRecord, kABPersonOrganizationProperty, [self.userInfo safeObjectForKey:@"New_Company"], &error);
    
    //phone number
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
     ABMultiValueAddValueAndLabel(multiPhone, [self.userInfo safeObjectForKey:@"Msisdn"], kABPersonPhoneMobileLabel, NULL);
    ABRecordSetValue(personRecord, kABPersonPhoneProperty, multiPhone,&error);
    CFRelease(multiPhone);
    
    //email
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiEmail, [self.userInfo safeObjectForKey:@"Email"], kABHomeLabel, NULL);
    
    ABRecordSetValue(personRecord, kABPersonEmailProperty, multiEmail, &error);
    CFRelease(multiEmail);
    
    //address
    ABMutableMultiValueRef multiAddress = ABMultiValueCreateMutable(kABMultiDictionaryPropertyType);        
    NSMutableDictionary *addressDictionary = [NSMutableDictionary dictionary];
    [addressDictionary setObject:[self.userInfo safeObjectForKey:@"New_Address"] forKey:(NSString *) kABPersonAddressStreetKey];
    [addressDictionary setObject:[self.userInfo safeObjectForKey:@"City"] forKey:(NSString *)kABPersonAddressCityKey];

    [addressDictionary setObject:@"China" forKey:(NSString *)kABPersonAddressCountryKey];
    
    ABMultiValueAddValueAndLabel(multiAddress, addressDictionary, kABWorkLabel, NULL);
    ABRecordSetValue(personRecord, kABPersonAddressProperty, multiAddress,&error);
    CFRelease(multiAddress);        
    
    newPerson.displayedPerson = personRecord;
    newPerson.newPersonViewDelegate = self;
    
    [self presentModalViewController:[[[UINavigationController alloc] initWithRootViewController:newPerson] autorelease] animated:YES];
    [newPerson release];
    CFRelease(personRecord);
}

#pragma mark -----------ABNewPersonViewController delegate------------
- (void)newPersonViewController:(ABNewPersonViewController *)newPersonView didCompleteWithNewPerson:(ABRecordRef)person {
    //    判断person里面有没有数据，如果有则保存，并提示添加成功，如果没有直接返回上一页
    if (person == NULL) {
        [self dismissModalViewController];
        return;
    }
    BOOL hasRecord;
    ABMultiValueRef phones = (ABMultiValueRef) ABRecordCopyValue(person, kABPersonPhoneProperty);   
    for(int i = 0 ;i < ABMultiValueGetCount(phones); i++){    
        NSString *phone = (NSString *)ABMultiValueCopyValueAtIndex(phones, i);   
        
        NSString *realNumber = [phone stringByReplacingOccurrencesOfString:@"-" withString:@""];
        realNumber = [realNumber stringByReplacingOccurrencesOfString:@")" withString:@""];
        realNumber = [realNumber stringByReplacingOccurrencesOfString:@"(" withString:@""];
        if (realNumber!=nil && realNumber.length == 11) {
            hasRecord = YES;
        }
        [phone release];
    }
    CFRelease(phones);
    if (!hasRecord) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", @"") message:NSLocalizedString(@"手机号不合法！", nil) delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        [alert release];
        [self dismissModalViewController];
        return;
    } 
    
    ABAddressBookRef iPhoneAddressBook = ABAddressBookCreate();    
    CFErrorRef errorRef = NULL;
    ABAddressBookAddRecord(iPhoneAddressBook, person, &errorRef);
    CFRelease(iPhoneAddressBook);
    if (errorRef == NULL) 
    {                
        self.isAddressSave = YES;
        saveAddressBtn.enabled = NO;
        [saveAddressBtn setTitle:NSLocalizedString(@"已存通讯录", @"") forState:UIControlStateDisabled];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", @"")
                                                        message:NSLocalizedString(@"添加至通讯录成功！", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", @"")
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    } else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"提示", @"")
                                                        message:NSLocalizedString(@"添加失败，请重新操作！", @"")
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", @"")
                                              otherButtonTitles: nil];
        [alert show];
        [alert release];
    }
    [self dismissModalViewController];
}


@end
