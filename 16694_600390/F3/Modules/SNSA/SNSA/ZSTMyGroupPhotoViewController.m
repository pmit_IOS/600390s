//
//  ZSTMyGroupPhotoViewController.m
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import "ZSTMyGroupPhotoViewController.h"
#import <AssetsLibrary/ALAsset.h>
#import "ZSTGroupPhotoCell.h"
#import <AssetsLibrary/ALAssetRepresentation.h>

@interface ZSTMyGroupPhotoViewController () <UITableViewDataSource,UITableViewDelegate,ZSTGroupPhotoCellDelegate>

@property (strong,nonatomic) UITableView *groupPhotoTableView;
@property (strong,nonatomic) NSMutableArray *groupPhotoArr;
@property (strong,nonatomic) UIButton *finishBtn;
@property (strong,nonatomic) UILabel *countLB;

@end

@implementation ZSTMyGroupPhotoViewController

static NSString *groupPhotoCell = @"groupPhotoCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.title = self.groupName;
    self.selectPicArr = [NSMutableArray array];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    self.groupPhotoArr = [NSMutableArray array];
    [self buildTableView];
    [self buildGroup];
    [self buildFootFunView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildGroup
{
    [self.selectGroup enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {//从group里面
        if (result)
        {
            NSString* assetType = [result valueForProperty:ALAssetPropertyType];
            if ([assetType isEqualToString:ALAssetTypePhoto])
            {
                [self.groupPhotoArr addObject:result];
            }
        }
        else if (self.groupPhotoArr.count > 0)
        {
            [self.groupPhotoTableView reloadData];
        }
//        NSDictionary *assetUrls = [result valueForProperty:ALAssetPropertyURLs];
//        NSUInteger assetCounter = 0;
//        for (NSString *assetURLKey in assetUrls) {
//            NSLog(@"Asset URL %lu = %@",(unsigned long)assetCounter,[assetUrls objectForKey:assetURLKey]);
//        }
    }];
}

- (void)buildTableView
{
    self.groupPhotoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 40) style:UITableViewStylePlain];
    self.groupPhotoTableView.dataSource = self;
    self.groupPhotoTableView.delegate = self;
    [self.groupPhotoTableView registerClass:[ZSTGroupPhotoCell class] forCellReuseIdentifier:groupPhotoCell];
    self.groupPhotoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.groupPhotoTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger rowNum = self.groupPhotoArr.count / 4 + (self.groupPhotoArr.count % 4 == 0 ? 0 : 1);
    return rowNum;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger startIndex = indexPath.row * 4;
    NSInteger lenght = 4;
    if (startIndex + lenght > self.groupPhotoArr.count)
    {
        lenght = self.groupPhotoArr.count - startIndex;
    }
    
    NSMutableArray *array = [NSMutableArray array];
    for (NSInteger i = startIndex; i < startIndex + lenght; i++)
    {
        ALAsset *result = self.groupPhotoArr[i];
        [array addObject:result];
    }
    
    ZSTGroupPhotoCell *cell = [tableView dequeueReusableCellWithIdentifier:groupPhotoCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.mgpVC = self;
    [cell createUI];
    cell.cellDelegate = self;
    [cell setCellData:array];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return (WIDTH - 15) / 4 + 5;
}

- (void)buildFootFunView
{
    UIView *footFunView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 40, WIDTH, 40)];
    footFunView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.view addSubview:footFunView];
    
    self.countLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 60, 30)];
    self.countLB.font = [UIFont systemFontOfSize:14.0f];
    self.countLB.textColor = [UIColor blackColor];
    self.countLB.textAlignment = NSTextAlignmentLeft;
    self.countLB.text = [NSString stringWithFormat:@"0 / %@",@(self.maxCount)];
    [footFunView addSubview:self.countLB];
    
    self.finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    UIImage *normalImg = [self createImageWithColor:RGBA(31, 185, 34, 1)];
    [self.finishBtn setBackgroundImage:normalImg forState:UIControlStateNormal];
    self.finishBtn.frame = CGRectMake(WIDTH - 70, 5, 60, 30);
    [self.finishBtn setTitle:@"完成" forState:UIControlStateNormal];
    self.finishBtn.layer.cornerRadius = 2.0f;
    self.finishBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.finishBtn addTarget:self action:@selector(finishPickPhoto:) forControlEvents:UIControlEventTouchUpInside];
    [footFunView addSubview:self.finishBtn];
    
    if (self.selectPicArr.count == 0)
    {
        self.finishBtn.enabled = NO;
    }
    else
    {
        self.finishBtn.enabled = YES;
    }
}

- (void)passImg:(ALAsset *)dealImg IsInsert:(BOOL)isInsert
{
    if (isInsert)
    {
        [self.selectPicArr addObject:dealImg];
    }
    else
    {
        [self.selectPicArr removeObject:dealImg];
    }
    
    self.countLB.text = [NSString stringWithFormat:@"%@ / %@",@(self.selectPicArr.count),@(self.maxCount)];
    if (self.selectPicArr.count == 0)
    {
        self.finishBtn.enabled = NO;
    }
    else
    {
        self.finishBtn.enabled = YES;
    }
}

- (void)dismiss
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)finishPickPhoto:(UIButton *)sender
{
    NSMutableArray *imgArr = [NSMutableArray array];
    for (ALAsset *oneAssert in self.selectPicArr)
    {
        CGImageRef ref = [[oneAssert  defaultRepresentation] fullScreenImage];
        UIImage *img = [[UIImage alloc]initWithCGImage:ref];
        [imgArr addObject:img];
    }
    
    if ([self.groupPhotoDelegate respondsToSelector:@selector(finishPickerImage:)])
    {
        [self.groupPhotoDelegate finishPickerImage:imgArr];
    }
}

- (UIImage*) createImageWithColor: (UIColor*) color
{
    CGRect rect=CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *theImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return theImage;
}

@end
