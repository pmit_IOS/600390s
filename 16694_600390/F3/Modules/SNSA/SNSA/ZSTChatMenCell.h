//
//  ZSTChatMenCell.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import <UIKit/UIKit.h>

@protocol ZSTChatMenCellDelegate <NSObject>

- (void)callMenPhone:(NSString *)menPhoneString;

@end

@interface ZSTChatMenCell : UITableViewCell

@property (strong,nonatomic) UIImageView *chatMenHeaderIV;
@property (strong,nonatomic) UILabel *chatMenNameLB;
@property (strong,nonatomic) UILabel *chatMenPhoneLB;
@property (strong,nonatomic) UIImageView *chatMenPhoneCallIV;
@property (strong,nonatomic) UIButton *chatMenPhoneCallBtn;
@property (strong,nonatomic) NSDictionary *chatMenDic;
@property (weak,nonatomic) id<ZSTChatMenCellDelegate> menCellDelegate;

- (void)createUI;
- (void)setCellData:(NSDictionary *)chatMenDic;

@end
