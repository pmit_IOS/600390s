//
//  ZSTSexSwitch.h
//  YouYun
//
//  Created by luobin on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum 
{
    ZST_Sex_Male = 1,
    ZST_Sex_Female = 0
} ZST_Sex;

@interface ZSTSexSwitch : UIControl

@property (nonatomic, assign) ZST_Sex sex;

@end
