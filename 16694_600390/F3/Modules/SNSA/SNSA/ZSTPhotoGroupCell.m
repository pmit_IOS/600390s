//
//  ZSTPhotoGroupCell.m
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import "ZSTPhotoGroupCell.h"

@implementation ZSTPhotoGroupCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.firstImg)
    {
        self.firstImg = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 60, 60)];
        [self.contentView addSubview:self.firstImg];
        
        self.groupNameLB = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, WIDTH - 60 - 15, 30)];
        self.groupNameLB.textAlignment = NSTextAlignmentLeft;
        self.groupNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.groupNameLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.groupNameLB];
        
        self.groupCountLB = [[UILabel alloc] initWithFrame:CGRectMake(90, 40, WIDTH - 60 - 15, 30)];
        self.groupCountLB.textAlignment = NSTextAlignmentLeft;
        self.groupCountLB.textColor = [UIColor lightGrayColor];
        self.groupCountLB.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.groupCountLB];
    }
}

- (void)setCellData:(UIImage *)firstImg Name:(NSString *)groupName Count:(NSString *)groupCount
{
    self.firstImg.image = firstImg;
    self.groupNameLB.text = groupName;
    self.groupCountLB.text = groupCount;
}

@end
