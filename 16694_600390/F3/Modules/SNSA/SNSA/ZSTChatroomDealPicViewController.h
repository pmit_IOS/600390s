//
//  ZSTChatroomDealPicViewController.h
//  SNSA
//
//  Created by pmit on 15/10/8.
//
//

#import <UIKit/UIKit.h>

@class ZSTNewsTopicViewController;

@interface ZSTChatroomDealPicViewController : UIViewController

@property (strong,nonatomic) NSArray *dealImgArr;
@property (weak,nonatomic) ZSTNewsTopicViewController *weakNewsTopic;
@property (assign,nonatomic) NSInteger showPage;

@end
