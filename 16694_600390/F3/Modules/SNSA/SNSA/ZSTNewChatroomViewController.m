//
//  ZSTNewChatroomViewController.m
//  SNSA
//
//  Created by pmit on 15/9/6.
//
//

#import "ZSTNewChatroomViewController.h"
#import <ZSTUtils.h>
#import <PMRepairButton.h>
#import <MobileCoreServices/MobileCoreServices.h>

@interface ZSTNewChatroomViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) PMRepairButton *keyBoardBtn;
@property (strong,nonatomic) PMRepairButton *addBtn;
@property (strong,nonatomic) PMRepairButton *faceBtn;
@property (strong,nonatomic) PMRepairButton *voiceBtn;
@property (strong,nonatomic) UIButton *startTalkBtn;
@property (strong,nonatomic) UITextField *talkTF;
@property (strong,nonatomic) UIView *mediaView;
@property (strong,nonatomic) NSArray *mediaTitleArr;
@property (strong,nonatomic) UITableView *chatTableView;
@property (strong,nonatomic) NSArray *mediaImgArr;
@property (assign,nonatomic) BOOL isMediaOpen;

@end

@implementation ZSTNewChatroomViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardDismiss:) name:UIKeyboardWillHideNotification object:nil];
    
    self.mediaTitleArr = @[@"图片",@"拍照",@"视频"];
    self.mediaImgArr = @[@"sharemore_pic.png",@"sharemore_video.png",@"sharemore_videovoip.png"];
    [self buildNavigatonItem];
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    [self buildDownInputView];
    [self buildMediaView];
    [self buildChatTable];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 创建navigaiton项目
- (void)buildNavigatonItem
{
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(self.chatroomName, nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.navigationItem.rightBarButtonItem = [self buildReportSubView];
    [self.navigationController.navigationBar setBarTintColor:RGBA(231, 231, 231, 1)];
    [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - 创建聊天TableView
- (void)buildChatTable
{
    self.chatTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 45) style:UITableViewStylePlain];
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    self.chatTableView.separatorColor = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.chatTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBA(243, 243, 243, 1);
    self.chatTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"chatCell"];
    return cell;
}

#pragma mark - 举报按钮
- (UIBarButtonItem *)buildReportSubView
{
//    UIBarButtonItem *reportBtn = [[UIBarButtonItem alloc] initWithTitle:@"举报" style:UIBarButtonItemStyleDone target:self action:@selector(reportCircle:)];
//    return reportBtn;
    UIButton *reportBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    reportBtn.frame = CGRectMake(0, 0, 30, 20);
    [reportBtn setTitle:@"举报" forState:UIControlStateNormal];
    reportBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [reportBtn addTarget:self action:@selector(reportCircle:) forControlEvents:UIControlEventTouchUpInside];
    return [[UIBarButtonItem alloc] initWithCustomView:reportBtn];
    
}

#pragma mark - 底层新信息输入框
- (void)buildDownInputView
{
    UIImage *rawBackground = ZSTModuleImage(@"module_snsa_MessageEntryBackground.png");
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 45, WIDTH, 45)];
    UIImageView *bgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 45)];
    bgView.image = [rawBackground stretchableImageWithLeftCapWidth:1 topCapHeight:22];
    bgView.userInteractionEnabled = YES;
    [self.inputView addSubview:bgView];
    [self.view addSubview:self.inputView];
    
    self.voiceBtn = [[PMRepairButton alloc] init];
    self.voiceBtn.frame = CGRectMake(5, 7.5, 30, 30);
    [self.voiceBtn setImage:[UIImage imageNamed:@"voice.png"] forState:UIControlStateNormal];
//    [self.voiceBtn setImage:[UIImage imageNamed:@"voice_HL.png"] forState:UIControlStateHighlighted];
    self.voiceBtn.tag = 100;
    [self.voiceBtn addTarget:self action:@selector(changeInputWay:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.voiceBtn];
    
    self.talkTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 5, WIDTH - 65 - 30 - 15, 35)];
    self.talkTF.delegate = self;
    self.talkTF.placeholder = @" 说点什么吧";
    self.talkTF.layer.borderColor = [UIColor blackColor].CGColor;
    self.talkTF.layer.borderWidth = 0.5f;
    self.talkTF.layer.cornerRadius = 2.0f;
    self.talkTF.backgroundColor = [UIColor whiteColor];
    self.talkTF.returnKeyType = UIReturnKeyDone;
    self.talkTF.keyboardType = UIReturnKeyDefault;
    [bgView addSubview:self.talkTF];
    self.talkTF.hidden = YES;
    
    self.keyBoardBtn = [[PMRepairButton alloc] init];
    self.keyBoardBtn.frame = CGRectMake(5, 7.5, 30, 30);
    [self.keyBoardBtn setImage:[UIImage imageNamed:@"keyborad.png"] forState:UIControlStateNormal];
//    [self.keyBoardBtn setImage:[UIImage imageNamed:@"keyborad_HL.png"] forState:UIControlStateHighlighted];
    self.keyBoardBtn.tag = 101;
    [self.keyBoardBtn addTarget:self action:@selector(changeInputWay:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.keyBoardBtn];
    
    UIImage *image3 = ZSTModuleImage(@"module_snsa_btn_voice_n.png");
    UIImage *image4 = ZSTModuleImage(@"module_snsa_btn_voice_p.png");
    self.startTalkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.startTalkBtn.frame = CGRectMake(40, 5, WIDTH - 65 - 30 - 15, 35);
    [self.startTalkBtn setTitle:@"按住说话" forState:UIControlStateNormal];
    [self.startTalkBtn setBackgroundImage:[image3 stretchableImageWithLeftCapWidth:5 topCapHeight:18] forState:UIControlStateNormal];
    [self.startTalkBtn setBackgroundImage:[image4 stretchableImageWithLeftCapWidth:5 topCapHeight:18] forState:UIControlStateHighlighted];
    self.startTalkBtn.layer.cornerRadius = 1.0f;
    self.startTalkBtn.tag = 1000;
    [self.startTalkBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [bgView addSubview:self.startTalkBtn];
    
    self.addBtn = [[PMRepairButton alloc] init];
    self.addBtn.frame = CGRectMake(WIDTH - 60 - 5, 7.5, 30, 30);
    self.addBtn.tag = 1001;
    [self.addBtn setImage:[UIImage imageNamed:@"multiMedia.png"] forState:UIControlStateNormal];
    [self.addBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [bgView addSubview:self.addBtn];
    
    self.faceBtn = [[PMRepairButton alloc] init];
    self.faceBtn.frame = CGRectMake(WIDTH - 30 - 2, 7.5, 30, 30);
    [self.faceBtn setImage:[UIImage imageNamed:@"face.png"] forState:UIControlStateNormal];
    [bgView addSubview:self.faceBtn];
}

#pragma mark - 多媒体视图
- (void)buildMediaView
{
    self.mediaView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64, WIDTH, 100)];
    self.mediaView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.view addSubview:self.mediaView];
    
    for (NSInteger i = 0; i < self.mediaTitleArr.count; i++)
    {
        
        UIView *oneView = [[UIView alloc] initWithFrame:CGRectMake(15 + (WIDTH - 30) / 4 * i, 10, (WIDTH - 30) / 4, 80)];
        oneView.backgroundColor = [UIColor clearColor];
        [self.mediaView addSubview:oneView];
        
        NSString *mediaTitle = self.mediaTitleArr[i];
        NSString *imageName = self.mediaImgArr[i];
        UIImageView *mediaIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, (WIDTH - 30) / 4, 50)];
        [mediaIV setImage:[UIImage imageNamed:imageName]];
        mediaIV.contentMode = UIViewContentModeScaleAspectFit;
        [oneView addSubview:mediaIV];
        
        UILabel *mediaTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, (WIDTH - 30) / 4, 20)];
        mediaTitleLB.font = [UIFont systemFontOfSize:10.0f];
        mediaTitleLB.textColor = [UIColor blackColor];
        mediaTitleLB.textAlignment = NSTextAlignmentCenter;
        mediaTitleLB.text = mediaTitle;
        [oneView addSubview:mediaTitleLB];
        
        oneView.tag = 10000 + i;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mediaTap:)];
        [oneView addGestureRecognizer:tap];
    }
}

- (void)buildVoiceView
{
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - 更换发送的消息类型
- (void)changeInputWay:(PMRepairButton *)sender
{
    if (sender.tag == 100)
    {
        self.keyBoardBtn.hidden = NO;
        self.voiceBtn.hidden = YES;
        self.startTalkBtn.hidden = NO;
        self.talkTF.hidden = YES;
    }
    else
    {
        self.keyBoardBtn.hidden = YES;
        self.voiceBtn.hidden = NO;
        self.startTalkBtn.hidden = YES;
        self.talkTF.hidden = NO;
    }
    
}

#pragma mark - 功能按钮
- (void)btnClick:(PMRepairButton *)sender
{
    if (sender.tag == 1001)
    {
        if (!self.isMediaOpen)
        {
            for (UIView *subView in self.view.subviews)
            {
                [UIView animateWithDuration:0.3f animations:^{
                    
                    subView.frame = CGRectMake(subView.frame.origin.x, subView.frame.origin.y - 100, subView.bounds.size.width, subView.bounds.size.height);
                    
                } completion:^(BOOL finished) {
                    
                }];
                
            }
            
            self.isMediaOpen = YES;
        }
        else
        {
            for (UIView *subView in self.view.subviews)
            {
                [UIView animateWithDuration:0.3f animations:^{
                    
                    subView.frame = CGRectMake(subView.frame.origin.x, subView.frame.origin.y + 100, subView.bounds.size.width, subView.bounds.size.height);
                    
                } completion:^(BOOL finished) {
                    
                }];
                
            }
            
            self.isMediaOpen = NO;
        }
        
    }
}

- (void)reportCircle:(UIBarButtonItem *)sender
{
    
}

- (void)mediaTap:(UITapGestureRecognizer *)tap
{
    NSInteger tapTag = tap.view.tag;
    self.iconPickerController = [[UIImagePickerController alloc] init];
    [_iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    switch (tapTag)
    {
        case 10000:
        {
            self.iconPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        }
            break;
        case 10001:
        {
            _iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            _iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
            _iconPickerController.showsCameraControls = YES;
        }
            
            break;
        case 10002:
        {
            NSArray* availableMediaTypes = [UIImagePickerController availableMediaTypesForSourceType:UIImagePickerControllerSourceTypeCamera];
            BOOL canTakeVideo = NO;
            for (NSString *mediaType in availableMediaTypes) {
                if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
                    //支持摄像
                    canTakeVideo = YES;
                    break;
                }
            }
            //检查是否支持摄像
            if (!canTakeVideo) {
                NSLog(@"sorry, capturing video is not supported.!!!");
                return;
            }
            //设置图像选取控制器的来源模式为相机模式
            _iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            //设置图像选取控制器的类型为动态图像
            _iconPickerController.mediaTypes = @[(NSString *)kUTTypeMovie];
            //设置摄像图像品质
            _iconPickerController.videoQuality = UIImagePickerControllerQualityTypeHigh;
            //设置最长摄像时间
            _iconPickerController.videoMaximumDuration = 15;
        }
            
            break;
            
        default:
            break;
    }
    _iconPickerController.allowsEditing = YES;
    _iconPickerController.delegate = self;
    [self presentViewController:_iconPickerController animated:YES completion:nil];
}

#pragma mark - 键盘弹出
- (void)keyBoardShow:(NSNotification *)notification
{
    
}

#pragma mark - 键盘收回
- (void)keyBoardDismiss:(NSNotification *)notification
{
    
}


@end
