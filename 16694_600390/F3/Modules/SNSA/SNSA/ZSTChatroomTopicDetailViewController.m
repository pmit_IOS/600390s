//
//  ZSTChatroomTopicDetailViewController.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTChatroomTopicDetailViewController.h"
#import <ZSTUtils.h>
#import <PMRepairButton.h>
#import "ZSTMyChatroomHeaderCell.h"
#import "ZSTMyChatroomfooterCell.h"
#import "ZSTShareDelegate.h"
#import "ZSTDiscussDelegate.h"
#import "ZSTGoodDelegate.h"
#import "ZSTChatroomShareGoodCell.h"
#import "ZSTChatroomLookPicViewController.h"
#import "ZSTYouYunEngine.h"
#import "ZSTTopicDetailCommentCell.h"
#import "ZSTDao+SNSA.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZSTTopicReportViewController.h"
#import "ZSTChatroomNoDataCell.h"
#import "ZSTBigPicDelegate.h"
#import "ZSTTopicContentCell.h"
#import "ZSTChatroomLoadMoreView.h"
#import "ZSTChatroomMenDetailViewController.h"
#import "EmojiTextAttachment.h"
#import "NSAttributedString+EmojiExtension.h"
#import "CTFrameParser.h"
#import "CTFrameParserConfig.h"
#import "CoreTextData.h"

#define kPageSize 5

@interface ZSTChatroomTopicDetailViewController () <UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,ZSTMyChatroomHeaderCellDelegate,ZSTMyChatroomfooterCellDelegate,ZSTTopicContentCellDelegate,ZSTYouYunEngineDelegate,ZSTDiscussDelegateDelegate,UITextViewDelegate,ZSTTopicReportViewControllerDelegate>

@property (strong,nonatomic) UITableView *topicContentTableView;
@property (strong,nonatomic) UITableView *topicShareTableView;
@property (strong,nonatomic) UITableView *topicDiscussTableView;
@property (strong,nonatomic) UITableView *topicGoodTableView;
@property (strong,nonatomic) UIScrollView *downShowScroll;
@property (strong,nonatomic) UIScrollView *widthScroll;
@property (strong,nonatomic) ZSTShareDelegate *shareDelegate;
@property (strong,nonatomic) ZSTDiscussDelegate *discussDelegate;
@property (strong,nonatomic) ZSTGoodDelegate *goodDelegate;
@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) UIView *faceView;
@property (strong,nonatomic) UITextView *inputTV;
@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (strong,nonatomic) NSMutableArray *commentArr;
@property (strong,nonatomic) NSMutableArray *thumArr;
@property (strong,nonatomic) NSMutableArray *shareArr;
@property (strong,nonatomic) ZSTDao *dao;
@property (assign,nonatomic) BOOL isToComment;
@property (strong,nonatomic) ZSTSNSAMessageComments *comments;
@property (strong,nonatomic) UILabel *backUpLB;
@property (strong,nonatomic) ZSTSNSAMessageComments *toComments;
@property (strong,nonatomic) UIView *moreFunView;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) UIView *exitAlertView;
@property (strong,nonatomic) UIView *darkScrollView;
@property (strong,nonatomic) UIScrollView *darkScroll;
@property (strong,nonatomic) UIView *noShareView;
@property (strong,nonatomic) UIView *noDiscussView;
@property (strong,nonatomic) UIView *noGoodView;
@property (assign,nonatomic) BOOL isFaceOpen;
@property (strong,nonatomic) UIScrollView *faceScroll;
@property (strong,nonatomic) UIPageControl *facePageControl;
@property (strong,nonatomic) NSArray *nowShowArr;
@property (strong,nonatomic) NSString *nowShowType;
@property (strong,nonatomic) UILabel *showNumLB;
@property (strong,nonatomic) UIView *footerView;
@property (assign,nonatomic) BOOL commentsHasMore;
@property (assign,nonatomic) BOOL goodHasMore;
@property (assign,nonatomic) BOOL isLoadOld;


@end

@implementation ZSTChatroomTopicDetailViewController

static NSString *const headerCell = @"headerCell";
static NSString *const footerCell = @"footerCell";
static NSString *const contentCell = @"contentCell";
static NSString *const shareCell = @"shareCell";
static NSString *const discussCell = @"discussCell";
static NSString *const goodCell = @"goodCell";
static NSString *const noDataCell = @"noDataCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.nowShowType = @"share";
    
    self.isToComment = NO;
    self.commentsHasMore = YES;
    
    if (self.isFromNews)
    {
        self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(self.circleTitle, nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(goToBackVC:)];
    PMRepairButton *shareTopicBtn = [[PMRepairButton alloc] init];
    shareTopicBtn.frame = CGRectMake(0, 0, 30, 30);
    [shareTopicBtn setImage:[UIImage imageNamed:@"chatroom_naviShare.png"] forState:UIControlStateNormal];
    [shareTopicBtn addTarget:self action:@selector(shareTopic:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:shareTopicBtn];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.commentArr = [NSMutableArray array];
    self.thumArr = [NSMutableArray array];
    self.shareArr = [NSMutableArray array];
    self.nowShowArr = self.shareArr;
    self.shareDelegate = [[ZSTShareDelegate alloc] init];
    self.goodDelegate = [[ZSTGoodDelegate alloc] init];
    self.discussDelegate = [[ZSTDiscussDelegate alloc] init];
    self.discussDelegate.discussFunDelegate = self;
    
    [self buildAllTableView];
    [self buildInputFirstView];
    [self buildCommentsInputView];
    [self resetTextStyle];

    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    
    [self.engine getAllComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] PageSize:kPageSize currentMsgId:[NSString stringWithFormat:@"0"]];
    [self.engine getThumBop:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] PageSize:kPageSize currentMsgId:@"0"];
    
    if (self.isFromNews)
    {
        if ([self.enterType isEqualToString:@"discuss"])
        {
            self.nowShowType = @"discuss";
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    if (!self.faceView)
    {
        [self buildFaceView];
        [self buildShadowView];
        [self buildMoreFunView];
        [self buildSureAlertView];
        [self buildDarkScroll];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)buildAllTableView
{
    self.topicContentTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.topicContentTableView.backgroundColor = RGBA(243, 243, 243, 1);
    self.topicContentTableView.delegate = self;
    self.topicContentTableView.dataSource = self;
    self.topicContentTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.topicContentTableView setSeparatorColor:[UIColor whiteColor]];
    [self.topicContentTableView registerClass:[ZSTMyChatroomHeaderCell class] forCellReuseIdentifier:headerCell];
    [self.topicContentTableView registerClass:[ZSTMyChatroomfooterCell class] forCellReuseIdentifier:footerCell];
    [self.topicContentTableView registerClass:[ZSTTopicContentCell class] forCellReuseIdentifier:contentCell];
    [self.topicContentTableView registerClass:[ZSTChatroomShareGoodCell class] forCellReuseIdentifier:shareCell];
    [self.topicContentTableView registerClass:[ZSTTopicDetailCommentCell class] forCellReuseIdentifier:discussCell];
    [self.topicContentTableView registerClass:[ZSTChatroomShareGoodCell class] forCellReuseIdentifier:goodCell];
    [self.topicContentTableView registerClass:[ZSTChatroomNoDataCell class] forCellReuseIdentifier:noDataCell];
    [self.view addSubview:self.topicContentTableView];
    
}

- (void)buildDownScrollView
{
    self.downShowScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.topicContentTableView.frame) + 20, WIDTH, 180)];
    self.downShowScroll.backgroundColor = [UIColor whiteColor];
    self.downShowScroll.contentSize = CGSizeMake(WIDTH * 3, 0);
    self.downShowScroll.scrollEnabled = NO;
    [self.widthScroll addSubview:self.downShowScroll];
    self.widthScroll.contentSize = CGSizeMake(0, self.topicContentTableView.frame.size.height + self.downShowScroll.frame.size.height + 20);
    
    self.topicShareTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 180) style:UITableViewStylePlain];
    self.topicShareTableView.delegate = self.shareDelegate;
    self.topicShareTableView.dataSource = self.shareDelegate;
    self.topicShareTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.topicShareTableView setSeparatorColor:[UIColor whiteColor]];
    [self.topicShareTableView registerClass:[ZSTChatroomShareGoodCell class] forCellReuseIdentifier:shareCell];
    self.topicShareTableView.scrollEnabled = NO;
    [self.downShowScroll addSubview:self.topicShareTableView];
    
    self.topicDiscussTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH, 180) style:UITableViewStylePlain];
    self.topicDiscussTableView.delegate = self.discussDelegate;
    self.topicDiscussTableView.dataSource = self.discussDelegate;
    [self.topicDiscussTableView setSeparatorColor:[UIColor whiteColor]];
    self.topicDiscussTableView.scrollEnabled = NO;
    [self.topicDiscussTableView registerClass:[ZSTTopicDetailCommentCell class] forCellReuseIdentifier:discussCell];
    [self.downShowScroll addSubview:self.topicDiscussTableView];
    
    self.topicGoodTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH * 2, 0, WIDTH, 180) style:UITableViewStylePlain];
    self.topicGoodTableView.delegate = self.goodDelegate;
    self.topicGoodTableView.dataSource = self.goodDelegate;
    self.topicGoodTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.topicGoodTableView setSeparatorColor:[UIColor whiteColor]];
    [self.topicGoodTableView registerClass:[ZSTChatroomShareGoodCell class] forCellReuseIdentifier:goodCell];
    self.topicGoodTableView.scrollEnabled = NO;
    [self.downShowScroll addSubview:self.topicGoodTableView];
    
    self.noShareView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 180)];
    self.noShareView.backgroundColor = [UIColor whiteColor];
    self.noShareView.hidden = NO;
    [self.downShowScroll addSubview:self.noShareView];
    UILabel *noShareLB = [[UILabel alloc] init];
    noShareLB.center = self.noShareView.center;
    noShareLB.bounds = (CGRect){CGPointZero,{WIDTH,40}};
    noShareLB.text = @"暂时没有人分享哦";
    noShareLB.font = [UIFont systemFontOfSize:14.0f];
    noShareLB.textAlignment = NSTextAlignmentCenter;
    noShareLB.textColor = RGBA(243, 243, 243, 1);
    [self.noShareView addSubview:noShareLB];
    
    self.noDiscussView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH, 0, WIDTH, 180)];
    self.noDiscussView.backgroundColor = [UIColor whiteColor];
    self.noDiscussView.hidden = YES;
    [self.downShowScroll addSubview:self.noDiscussView];
    
    UILabel *noDiscussLB = [[UILabel alloc] init];
    noDiscussLB.center = self.noShareView.center;
    noDiscussLB.bounds = (CGRect){CGPointZero,{WIDTH,40}};
    noDiscussLB.text = @"暂时没有人评论哦";
    noDiscussLB.font = [UIFont systemFontOfSize:14.0f];
    noDiscussLB.textAlignment = NSTextAlignmentCenter;
    noDiscussLB.textColor = RGBA(243, 243, 243, 1);
    [self.noDiscussView addSubview:noDiscussLB];
    
    self.noGoodView = [[UIView alloc] initWithFrame:CGRectMake(WIDTH * 2, 0, WIDTH, 180)];
    self.noGoodView.backgroundColor = [UIColor whiteColor];
    self.noGoodView.hidden = YES;
    [self.downShowScroll addSubview:self.noGoodView];
    
    UILabel *noGoodLB = [[UILabel alloc] init];
    noGoodLB.center = self.noShareView.center;
    noGoodLB.bounds = (CGRect){CGPointZero,{WIDTH,40}};
    noGoodLB.text = @"暂时没有人点赞哦";
    noGoodLB.textAlignment = NSTextAlignmentCenter;
    noGoodLB.font = [UIFont systemFontOfSize:14.0f];
    noGoodLB.textColor = RGBA(243, 243, 243, 1);
    [self.noGoodView addSubview:noGoodLB];
    
}

- (void)buildInputFirstView
{
    if (self.messageDic.shareCount == 0)
    {
        self.downShowScroll.frame = CGRectMake(self.downShowScroll.frame.origin.x, self.downShowScroll.frame.origin.y, self.downShowScroll.bounds.size.width, 180);
    }
}

- (void)buildCommentsInputView
{
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
    self.inputView.backgroundColor = RGBA(238, 238, 238, 1);
    [self.view addSubview:self.inputView];
    
    PMRepairButton *faceBtn = [[PMRepairButton alloc] init];
    faceBtn.frame = CGRectMake(10, 10, 30, 30);
    [faceBtn setImage:[UIImage imageNamed:@"chatroom_face.png"] forState:UIControlStateNormal];
    [faceBtn addTarget:self action:@selector(showFaceSelectView:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:faceBtn];
    
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake(WIDTH - 10 - 50, 10, 50, 30);
    sendBtn.backgroundColor = [UIColor whiteColor];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [sendBtn setTitleColor:RGBA(159, 159, 159, 1) forState:UIControlStateNormal];
    sendBtn.layer.borderColor = RGBA(159, 159, 159, 1).CGColor;
    sendBtn.layer.borderWidth = 0.5;
    sendBtn.layer.cornerRadius = 2.0f;
    [sendBtn addTarget:self action:@selector(sendComment:) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.inputView addSubview:sendBtn];
    
    
    self.inputTV = [[UITextView alloc] initWithFrame:CGRectMake(45, 10, WIDTH - 50 - 50 - 10, 30)];
    self.inputTV.layer.borderColor = RGBA(159, 159, 159, 1).CGColor;
    self.inputTV.layer.borderWidth = 0.5;
    self.inputTV.layer.cornerRadius = 2.0f;
    self.inputTV.backgroundColor = [UIColor whiteColor];
    self.inputTV.delegate = self;
    [self.inputView addSubview:self.inputTV];
    
    self.backUpLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.inputTV.bounds.size.width - 20, 20)];
    self.backUpLB.font = [UIFont systemFontOfSize:12.0f];
    self.backUpLB.textColor = RGBACOLOR(204, 204, 204, 1);
    [self.inputTV addSubview:self.backUpLB];
    
    self.backUpLB.hidden = YES;
    
    self.inputView.hidden = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 3;
    }
    else
    {
        return self.nowShowArr.count == 0 ? 1 : self.nowShowArr.count;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            ZSTMyChatroomHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:headerCell];
            cell.isDetailCell = YES;
            [cell createUI];
            cell.headerCellDelegate = self;
            [cell setCellData:self.messageDic];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else if (indexPath.row == 1)
        {
            ZSTTopicContentCell *cell = [tableView dequeueReusableCellWithIdentifier:contentCell];
            cell.isFromTopicDetail = YES;
            cell.cellSection = 1;
            [cell createUI];
            cell.contentCellDelegate = self;
            [cell setCellData:self.messageDic];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
            
        }
        else
        {
            ZSTMyChatroomfooterCell *cell = [tableView dequeueReusableCellWithIdentifier:footerCell];
            cell.ZSTMyChatroomfooterCellDelegate = self;
            [cell createUI];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell setCellData:self.messageDic];
            return cell;
        }
    }
    else
    {
        if ([self.nowShowType isEqualToString:@"share"])
        {
            if (self.shareArr.count != 0)
            {
                ZSTChatroomShareGoodCell *cell = [tableView dequeueReusableCellWithIdentifier:shareCell];
                cell.isGood = NO;
                [cell createUI];
                return cell;
            }
            else
            {
                ZSTChatroomNoDataCell *cell = [tableView dequeueReusableCellWithIdentifier:noDataCell];
                [cell createUI];
                [cell setCellLBNameString:@"暂时还没有人来分享哦!"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
        else if ([self.nowShowType isEqualToString:@"discuss"])
        {
            if (self.commentArr.count != 0)
            {
                ZSTTopicDetailCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:discussCell];
                [cell createUI];
                ZSTSNSAMessageComments *comments = self.commentArr[indexPath.row];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                [cell setCellData:comments];
                
                return cell;
            }
            else
            {
                ZSTChatroomNoDataCell *cell = [tableView dequeueReusableCellWithIdentifier:noDataCell];
                [cell createUI];
                [cell setCellLBNameString:@"暂时还没有人评论哦!"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            
        }
        else
        {
            if (self.thumArr.count != 0)
            {
                ZSTChatroomShareGoodCell *cell = [tableView dequeueReusableCellWithIdentifier:goodCell];
                cell.isGood = YES;
                [cell createUI];
                NSDictionary *thumDic = self.thumArr[indexPath.row];
                [cell setCellData:[thumDic safeObjectForKey:@"uname"] DateString:[thumDic safeObjectForKey:@"addtime"] HeadeString:[thumDic safeObjectForKey:@"headphoto"]];
                return cell;
            }
            else
            {
                ZSTChatroomNoDataCell *cell = [tableView dequeueReusableCellWithIdentifier:noDataCell];
                [cell createUI];
                [cell setCellLBNameString:@"暂时还没有人点赞哦!"];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    else
    {
        return 20;
    }
    
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = RGBA(243, 243, 243, 1);
    
    UIImageView *arrowIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"chatroom_detailArrow.png"]];
    arrowIV.contentMode = UIViewContentModeScaleAspectFit;
    [UIView animateWithDuration:0.3f animations:^{
        
        if ([self.nowShowType isEqualToString:@"share"])
        {
            arrowIV.center = CGPointMake((WIDTH - 30) / 3 / 2 + 10, 14);
        }
        else if ([self.nowShowType isEqualToString:@"discuss"])
        {
            arrowIV.center = CGPointMake((WIDTH - 30) / 3 / 2 + (WIDTH - 30) / 3 + 10, 14);
            if (self.commentsHasMore)
            {
                self.topicContentTableView.tableFooterView = self.footerView;
            }
            else
            {
                self.topicContentTableView.tableFooterView = nil;
            }
        }
        else
        {
            arrowIV.center = CGPointMake((WIDTH - 30) / 3 / 2 + (WIDTH - 30) / 3 * 2 + 10, 14);
            if (self.goodHasMore)
            {
                self.topicContentTableView.tableFooterView = self.footerView;
            }
            else
            {
                self.topicContentTableView.tableFooterView = nil;
            }
        }
        
    }];
    
    arrowIV.bounds = (CGRect){CGPointZero,{(WIDTH - 30) / 3 , 12}};
    [headerView addSubview:arrowIV];
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            return 80;
        }
        else if (indexPath.row == 1)
        {
            NSString *messageString = self.messageDic.chatContent;
            CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
            config.width = WIDTH - 30;
            config.textColor = [UIColor blackColor];
            
            CTFrameParser *parser = [[CTFrameParser alloc] init];
            CoreTextData *data = [parser parseTemplateTopicContent:messageString config:config];
            
            NSString *imageUrl = self.messageDic.imageArrString;
            NSArray *imgArr = [imageUrl componentsSeparatedByString:@","];
            if (!imgArr || imgArr.count == 0 || [[imgArr firstObject] isEqualToString:@""])
            {
                return data.height + 15;
            }
            else if (imgArr.count == 1)
            {
                __block CGFloat dealWidth;
                __block CGFloat dealHeight;
                
                id imgObj = imgArr[0];
                if ([imgObj rangeOfString:@"http://"].length > 0)
                {
                    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imgArr[0]] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                        
                        CGFloat imgWidth = image.size.width;
                        CGFloat imgHeight = image.size.height;
                        CGFloat maxWidth = (WIDTH - 30) / 2;
                        CGFloat maxHeight = maxWidth * 1.5;
                        
                        if (imgWidth > maxWidth)
                        {
                            CGFloat imgNowHeight = imgHeight * (maxWidth / imgWidth);
                            dealWidth = maxWidth;
                            if (imgNowHeight > maxHeight)
                            {
                                dealHeight = maxHeight;
                            }
                            else
                            {
                                dealHeight = imgNowHeight;
                            }
                        }
                        else
                        {
                            dealWidth = imgWidth;
                            if (imgHeight > maxHeight)
                            {
                                dealHeight = maxHeight;
                            }
                            else
                            {
                                dealHeight = imgHeight;
                            }
                        }
                        
                    }];
                }
                else
                {
                    NSData *imgData = [[NSData alloc] initWithBase64Encoding:imgObj];
                    UIImage *img = [[UIImage alloc] initWithData:imgData];
                    CGFloat imgWidth = img.size.width;
                    CGFloat imgHeight = img.size.height;
                    CGFloat maxWidth = (WIDTH - 30) / 2;
                    CGFloat maxHeight = maxWidth * 1.5;
                    if (imgWidth > maxWidth)
                    {
                        CGFloat imgNowHeight = imgHeight * (maxWidth / imgWidth);
                        dealWidth = maxWidth;
                        if (imgNowHeight > maxHeight)
                        {
                            dealHeight = maxHeight;
                        }
                        else
                        {
                            dealHeight = imgNowHeight;
                        }
                    }
                    else
                    {
                        dealWidth = imgWidth;
                        if (imgHeight > maxHeight)
                        {
                            dealHeight = maxHeight;
                        }
                        else
                        {
                            dealHeight = imgHeight;
                        }
                    }
                }
                
                return dealHeight + data.height + 15;
            }
            else
            {
                NSInteger countNum = imgArr.count / 3;
                NSInteger lessNum = imgArr.count % 3;
                NSInteger height = 0;
                
                if (lessNum == 0)
                {
                    height = countNum;
                }
                else
                {
                    height = countNum + 1;
                }
                
                if (height > 0)
                {
                    if ([messageString isEqualToString:@""])
                    {
                        return height * (WIDTH - 40) / 3 + countNum * 5;
                    }
                    else
                    {
                        return data.height + height * (WIDTH - 40) / 3 + 15 + (height - 1) * 5;
                    }
                    
                }
                else
                {
                    return data.height + height * (WIDTH - 40) / 3 + 15;
                }
            }
        }
        else
        {
            return 40;
        }
    }
    else
    {
        if ([self.nowShowType isEqualToString:@"share"])
        {
            if (self.shareArr.count == 0)
            {
                return 180;
            }
            else
            {
                return 60;
            }
            
        }
        else if ([self.nowShowType isEqualToString:@"discuss"])
        {
            if (self.commentArr.count != 0)
            {
                ZSTSNSAMessageComments *comments = self.commentArr[indexPath.row];
                
                NSString *secondName = @"";
                
                
                if ([comments.toUserId integerValue] == [[ZSTF3Preferences shared].UID integerValue])
                {
                    secondName = @"我";
                }
                else
                {
                    secondName = comments.toUsername;
                }
    
                NSString *commentsString = comments.commentContent;
//                NSMutableString *accountCommentsString = [NSMutableString string];
                NSString *accountCommentsString = @"";
    
                if (comments.parentId != 0)
                {
                    accountCommentsString = [NSString stringWithFormat:@"回复%@:%@",secondName,commentsString];
                }
                else
                {
                    accountCommentsString = commentsString;
                }
                
                CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
                config.width = WIDTH - 60 - 15;
                config.textColor = [UIColor blackColor];
                
                CTFrameParser *parser = [[CTFrameParser alloc] init];
                parser.isDetailComments = YES;
                CoreTextData *data = [parser parseTemplateFileContent:accountCommentsString config:config UserMsisdn:comments.msisdn ToUserMsisdn:comments.msisdn];
                
                return 60 + data.height + 5;
                
            }
            else
            {
                return 180;
            }
           
        }
        else
        {
            if (self.thumArr.count == 0)
            {
                return 180;
            }
            else
            {
                return 60;
            }
        }
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1)
    {
        if ([self.nowShowType isEqualToString:@"share"])
        {
            
        }
        else if ([self.nowShowType isEqualToString:@"discuss"])
        {
            ZSTSNSAMessageComments *selectedComments = self.commentArr[indexPath.row];
            [self commentToComments:selectedComments];
        }
        else
        {
//            ZSTSNSAThumShare *selectedShare = self.thumArr[indexPath.row];
            NSDictionary *thumShareMen = self.thumArr[indexPath.row];
            ZSTChatroomMenDetailViewController *menDetailVC = [[ZSTChatroomMenDetailViewController alloc] init];
            menDetailVC.chatMenMsisdn = [thumShareMen safeObjectForKey:@"msisdn"];
            menDetailVC.chatMenOrigineMsisdn = [thumShareMen safeObjectForKey:@"msisdn"];
            [self.navigationController pushViewController:menDetailVC animated:YES];
        }
            
    }
}

- (void)shareTopic:(PMRepairButton *)sender
{
    
}

#pragma mark - 切换
- (void)topicFunClick:(NSInteger)senderTag MessageDic:(ZSTSNSAMessage *)messageDic Btn:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 101:
            
            self.isLoadOld = NO;
            self.nowShowArr = self.shareArr;
            self.nowShowType = @"share";
            self.inputView.hidden = YES;
            [self.topicContentTableView reloadData];
            self.topicContentTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
            break;
        
        case 102:
            
            self.isLoadOld = NO;
            self.nowShowArr = self.commentArr;
            self.nowShowType = @"discuss";
            [self.topicContentTableView reloadData];
            self.inputView.hidden = NO;
            self.topicContentTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50);
            
            if (self.commentsHasMore)
            {
                if (!self.topicContentTableView.tableFooterView)
                {
                    ZSTChatroomLoadMoreView *footerView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                    self.footerView = footerView;
                    self.topicContentTableView.tableFooterView = self.footerView;
                }
            }
            else
            {
                self.topicContentTableView.tableFooterView = nil;
            }
            
            break;
        
        case 103:
            
            self.isLoadOld = NO;
            self.nowShowArr = self.thumArr;
            self.nowShowType = @"good";
            self.inputView.hidden = YES;
            [self.topicContentTableView reloadData];
            self.topicContentTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
            
            if (self.goodHasMore)
            {
                if (!self.topicContentTableView.tableFooterView)
                {
                    ZSTChatroomLoadMoreView *footerView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                    self.footerView = footerView;
                    self.topicContentTableView.tableFooterView = self.footerView;
                }
            }
            else
            {
                self.topicContentTableView.tableFooterView = nil;
            }
            
            break;
            
        default:
            break;
    }
}

//- (void)goToLookBigPic:(NSInteger)tapTag
//{
//    NSString *imageArrString = self.messageDic.imageArrString;
//    NSArray *imageArr = [imageArrString componentsSeparatedByString:@","];
//    
//    [self buildDetailDarkScroll:imageArr Offset:(tapTag - 1000)];
//    self.showNumLB.text = [NSString stringWithFormat:@"%@ / %@",@(tapTag - 1000 + 1),@(imageArr.count)];
//    
//    self.darkScroll.hidden = NO;
//    self.darkScrollView.hidden = NO;
//    [UIView animateWithDuration:0.5f animations:^{
//        
//        self.navigationController.navigationBarHidden = YES;
//        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
//        self.darkScrollView.backgroundColor = [UIColor blackColor];
//        self.darkScroll.backgroundColor = [UIColor blackColor];
//        
//    } completion:^(BOOL finished) {
//        
//    }];
//    
//}

#pragma mark - 查看图片
- (void)goToLookBigPic:(NSInteger)tapTag Message:(ZSTSNSAMessage *)message
{
    NSInteger cellImgTag = tapTag - 1000;
    NSString *imageArrString = message.imageArrString;
    NSArray *imageArr = [imageArrString componentsSeparatedByString:@","];
    self.showNumLB.text = [NSString stringWithFormat:@"%@ / %@",@(cellImgTag + 1),@(imageArr.count)];
    [self buildDetailDarkScroll:imageArr Offset:(cellImgTag)];
    
    self.darkScroll.hidden = NO;
    self.darkScrollView.hidden = NO;
    [UIView animateWithDuration:0.5f animations:^{
        
        self.navigationController.navigationBarHidden = YES;
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        self.darkScrollView.backgroundColor = [UIColor blackColor];
        self.darkScroll.backgroundColor = [UIColor blackColor];
        
    } completion:^(BOOL finished) {
        
    }];
}


- (void)buildDarkScroll
{
    self.darkScrollView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.darkScrollView.backgroundColor = [UIColor whiteColor];
    self.darkScrollView.hidden = YES;
    [self.view addSubview:self.darkScrollView];
    
    self.darkScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT - 50)];
    self.darkScroll.backgroundColor = [UIColor whiteColor];
    [self.darkScrollView addSubview:self.darkScroll];
    self.darkScroll.delegate = self;
    self.darkScroll.hidden = YES;
    
    PMRepairButton *backBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(15, 20, 30, 30)];
    [backBtn setImage:[UIImage imageNamed:@"normal_back.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(hideDarkScroll:) forControlEvents:UIControlEventTouchUpInside];
    [self.darkScrollView addSubview:backBtn];
    
    self.showNumLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(backBtn.frame) + 10, 20, WIDTH - CGRectGetMaxX(backBtn.frame) * 2 - 20, 30)];
    self.showNumLB.font = [UIFont systemFontOfSize:14.0f];
    self.showNumLB.textColor = [UIColor whiteColor];
    self.showNumLB.textAlignment = NSTextAlignmentCenter;
    [self.darkScrollView addSubview:self.showNumLB];
    
    UIView *imgDarkView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 50, WIDTH, 50)];
    imgDarkView.backgroundColor = [UIColor clearColor];
    PMRepairButton *saveBtn = [[PMRepairButton alloc] init];
    saveBtn.frame = CGRectMake(WIDTH - 60, 5, 40, 40);
    [saveBtn setImage:[UIImage imageNamed:@"chatroom_menSave.png"] forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveThisPic:) forControlEvents:UIControlEventTouchUpInside];
    [imgDarkView addSubview:saveBtn];
    [self.darkScrollView addSubview:imgDarkView];
}

- (void)sendComment:(UIButton *)sender
{
     NSString * newsTopicString = [NSString stringWithFormat:@"%@", [self.inputTV.textStorage getPlainString]];
    if ([newsTopicString isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"不能发表空评论哦", nil) withImage:nil];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = NO;
        }];
        
        [self.inputTV resignFirstResponder];
       
        if (self.isToComment)
        {
            [self.engine sendComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] Comments:newsTopicString ParentId:[NSString stringWithFormat:@"%@",@(self.toComments.mcid)] TouristId:self.toComments.uid];
        }
        else
        {
            [self.engine sendComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] Comments:newsTopicString ParentId:@"0" TouristId:self.messageDic.chatUID];
        }
    }
}

- (void)getAllCommentsDidSuccess:(NSDictionary *)response
{
    NSArray *commentsArr = [[response safeObjectForKey:@"data"] safeObjectForKey:@"msgcomtsInfo"];
    self.commentsHasMore = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"hasmore"] integerValue] == 1 ? YES : NO;
    
    for (NSDictionary *dic in commentsArr)
    {
        ZSTSNSAMessageComments *comments = [[ZSTSNSAMessageComments alloc] initWithCommentDic:dic];
        [self.commentArr addObject:comments];
    }
    
    [self loadMoreCompleted];
    
    if (self.isFromNews)
    {
        if ([self.enterType isEqualToString:@"discuss"])
        {
            self.nowShowArr = self.commentArr;
        }
    }
    
    if (self.commentsHasMore)
    {
        if (!self.topicContentTableView.tableFooterView)
        {
            ZSTChatroomLoadMoreView *footerView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
            self.footerView = footerView;
            self.topicContentTableView.tableFooterView = self.footerView;
        }
    }
    else
    {
        self.topicContentTableView.tableFooterView = nil;
    }
    
    [self.topicContentTableView reloadData];
}

- (void)getAllThumbopDidSuccess:(NSDictionary *)response
{
    [self.thumArr addObjectsFromArray:[[response safeObjectForKey:@"data"] safeObjectForKey:@"msgthumbup"]];
    self.goodHasMore = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"hasmore"] integerValue] == 1 ? YES : NO;
    self.goodDelegate.goodArr = self.thumArr;
    [self.topicGoodTableView reloadData];
}

- (void)getAllThumbopDidFailure:(NSString *)response
{
    
}

- (void)keyBoardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    self.faceView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect rect = self.view.frame;
        if (IS_IOS7 && !IS_IOS8) {
            rect.origin.y = - 80;
        }else{
            rect.origin.y = - 216;
        }
        
        self.view.frame = rect;
        
        self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
        self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
        self.isFaceOpen = NO;
        
    } completion:^(BOOL finished) {
        
        self.faceView.hidden = NO;
        
    }];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
        self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.isFaceOpen = NO;
    }];
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:@"myAnimations" context:nil];
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    self.view.frame = rect;
    [UIView commitAnimations];
}

- (void)sendCommentsDidSuccess:(NSDictionary *)response
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSInteger addTime = [dat timeIntervalSince1970];
    NSDictionary *commentDic = @{@"circleID":self.circleId,@"addtime":@(addTime),@"comments":self.inputTV.text,@"msisdn":[ZSTF3Preferences shared].loginMsisdn,@"uid":[ZSTF3Preferences shared].UID,@"uname":[ZSTF3Preferences shared].personName,@"headphoto":[[ZSTF3Preferences shared].personIcon isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].personIcon isEqualToString:@""] ? [ZSTF3Preferences shared].personIcon : @"",@"mid":@(self.messageDic.msgId),@"mcid":[NSString stringWithFormat:@"%@",[[response safeObjectForKey:@"data"] safeObjectForKey:@"mcid"]],@"parentid": self.isToComment ? [NSString stringWithFormat:@"%@",@(self.toComments.mcid)] : @"0",@"touserid": self.isToComment ? self.toComments.uid : self.messageDic.chatUID,@"tousername": self.isToComment ? self.toComments.uName : self.messageDic.chatNewsUserName,@"tomsisdn": self.isToComment ? self.toComments.msisdn : self.messageDic.chatMsisdn};
    ZSTSNSAMessageComments *newsComments = [[ZSTSNSAMessageComments alloc] initWithCommentDic:commentDic];
    newsComments.commentDic = commentDic;
    
    [self.commentArr insertObject:newsComments atIndex:0];
    self.nowShowType = @"discuss";
    self.nowShowArr = self.commentArr;
    [self.topicContentTableView reloadData];
    
    [self.inputTV resignFirstResponder];
    self.inputView.hidden = YES;
    self.topicContentTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
    
    self.noDiscussView.hidden = YES;
    
    self.messageDic.commentCount += 1; 
    [self.topicContentTableView reloadData];
    

    if ([self.detailDelegate respondsToSelector:@selector(upDateTopic:newsComments:)])
    {
        [self.detailDelegate upDateTopic:self.messageDic newsComments:newsComments];
        
    }
    
    self.inputTV.text = @"";
    
    self.isToComment = NO;
    
}

- (void)sendCommentsDidFailure:(NSString *)resultString
{
    
}

- (void)addSqlite:(NSArray *)commentsDicArr
{
    for (ZSTSNSAMessageComments *commentDic in commentsDicArr)
    {
//        ZSTSNSAMessageComments *comments = [[ZSTSNSAMessageComments alloc] initWithCommentDic:commentDic];
        [self.dao insertTopicComments:commentDic CircleId:self.circleId];
    }
}

- (void)commentToComments:(ZSTSNSAMessageComments *)comments
{
    self.inputView.hidden = NO;
    self.widthScroll.frame = CGRectMake(self.widthScroll.frame.origin.x, self.widthScroll.frame.origin.y, self.widthScroll.bounds.size.width, HEIGHT - 64 - 50);
    if ([comments.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue])
    {
        self.isToComment = NO;
        self.backUpLB.hidden = YES;
    }
    else
    {
        self.backUpLB.text = [NSString stringWithFormat:@"对%@说：",comments.uName];
        self.backUpLB.hidden = NO;
        self.isToComment = YES;
        self.toComments = comments;
    }
    
    [self.inputTV becomeFirstResponder];
}

-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        
        self.backUpLB.hidden = NO;
        
    }else{
        
        self.backUpLB.hidden = YES;
    }
}

- (void)commentToTopic:(UITapGestureRecognizer *)tap
{
    self.isToComment = NO;
}


- (void)buildMoreFunView
{
    self.moreFunView = [[UIView alloc] init];
    self.moreFunView.center = CGPointMake(self.view.centerX, self.view.centerY - 64);
    self.moreFunView.bounds = (CGRect){CGPointZero,{200,151}};
    self.moreFunView.backgroundColor = [UIColor whiteColor];
    self.moreFunView.hidden = YES;
    
    [self buildLayer:CGRectMake(0, 50, self.moreFunView.bounds.size.width, 0.5) Tag:999];
    [self buildLayer:CGRectMake(0, 100.5, self.moreFunView.bounds.size.width, 0.5) Tag:9999];
    
    [self buildLB:CGRectMake(20, 0, self.moreFunView.bounds.size.width - 40, 50) Tag:101 Title:@"删 除"];
    [self buildLB:CGRectMake(20, 50.5, self.moreFunView.bounds.size.width - 40, 50) Tag:102 Title:@"置 顶"];
    [self buildLB:CGRectMake(20, 101, self.moreFunView.bounds.size.width - 40, 50) Tag:103 Title:@"举 报"];
    
    [self.view addSubview:self.moreFunView];
}

- (void)buildLayer:(CGRect)rect Tag:(NSInteger)lineTag
{
    CALayer *line = [CALayer layer];
    line.frame = rect;
    line.backgroundColor = RGBACOLOR(243, 243, 243, 1).CGColor;
    [self.moreFunView.layer addSublayer:line];
}

- (void)buildLB:(CGRect)rect Tag:(NSInteger)tag Title:(NSString *)btnTitle
{
    UILabel *titleLB = [[UILabel alloc] initWithFrame:rect];
    titleLB.textAlignment = NSTextAlignmentLeft;
    titleLB.font = [UIFont systemFontOfSize:15.0f];
    titleLB.text = btnTitle;
    titleLB.tag = tag * 100;
    titleLB.textColor = [UIColor blackColor];
    [self.moreFunView addSubview:titleLB];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = rect;
    [btn addTarget:self action:@selector(moreFunClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = tag;
    [self.moreFunView addSubview:btn];
}


- (void)updateMoreFunView:(BOOL)isMine IsManager:(BOOL)isManager
{
    CALayer *line = (CALayer *)[self.moreFunView viewWithTag:9998];
    CALayer *line2 = (CALayer *)[self.moreFunView viewWithTag:9999];
    
    UILabel *deleteLB = (UILabel *)[self.moreFunView viewWithTag:(101 * 100)];
    UILabel *topLB = (UILabel *)[self.moreFunView viewWithTag:(102 * 100)];
    UILabel *reportLB = (UILabel *)[self.moreFunView viewWithTag:(103 * 100)];
    
    UIButton *deleteBtn = (UIButton *)[self.moreFunView viewWithTag:101];
    UIButton *topBtn = (UIButton *)[self.moreFunView viewWithTag:102];
    UIButton *reportBtn = (UIButton *)[self.moreFunView viewWithTag:103];
    
    if (isMine)
    {
        if (isManager)
        {
            line.hidden = NO;
            line2.hidden= NO;
            deleteLB.hidden = NO;
            deleteBtn.hidden = NO;
            topLB.hidden = NO;
            topBtn.hidden = NO;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,151}};
            topLB.frame = CGRectMake(topLB.frame.origin.x, 50.5, topLB.frame.size.width, topLB.frame.size.height);
            reportLB.frame = CGRectMake(reportLB.frame.origin.x, 101, reportLB.frame.size.width, reportLB.frame.size.height);
            topBtn.frame = CGRectMake(topBtn.frame.origin.x, 50.5, topBtn.frame.size.width, topBtn.frame.size.height);
            reportBtn.frame = CGRectMake(reportBtn.frame.origin.x, 101, reportBtn.frame.size.width, reportBtn.frame.size.height);
        }
        else
        {
            line.hidden = NO;
            deleteLB.hidden = NO;
            deleteBtn.hidden = NO;
            
            line2.hidden = YES;
            topLB.hidden = YES;
            topBtn.hidden = YES;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,101}};
            reportLB.frame = CGRectMake(topLB.frame.origin.x, 50.5, topLB.frame.size.width, topLB.frame.size.height);
            reportBtn.frame = CGRectMake(topBtn.frame.origin.x, 50.5, topBtn.frame.size.width, topBtn.frame.size.height);
            
        }
    }
    else
    {
        if (isManager)
        {
            line.hidden = YES;
            deleteLB.hidden = YES;
            deleteBtn.hidden = YES;
            
            reportBtn.hidden = NO;
            reportLB.hidden = NO;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,100.5}};
            topLB.frame = CGRectMake(topLB.frame.origin.x, 0, topLB.frame.size.width, topLB.frame.size.height);
            topBtn.frame = CGRectMake(topBtn.frame.origin.x, 0, topBtn.frame.size.width, topBtn.frame.size.height);
            reportLB.frame = CGRectMake(reportLB.frame.origin.x, 50.5, reportLB.frame.size.width, reportLB.frame.size.height);
            reportBtn.frame = CGRectMake(reportBtn.frame.origin.x, 50.5, reportBtn.frame.size.width, reportBtn.frame.size.height);
        }
        else
        {
            line.hidden = YES;
            deleteLB.hidden = YES;
            deleteBtn.hidden = YES;
            
            line2.hidden = YES;
            topLB.hidden = YES;
            topBtn.hidden = YES;
            
            reportBtn.hidden = NO;
            reportLB.hidden = NO;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,50}};
            reportLB.frame = CGRectMake(reportLB.frame.origin.x, 0, reportLB.frame.size.width, reportLB.frame.size.height);
            reportBtn.frame = CGRectMake(reportBtn.frame.origin.x, 0, reportBtn.frame.size.width, reportBtn.frame.size.height);
        }
        
    }
}

- (void)moreFunClick:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 101:
        {
            if ([self.messageDic.chatUID integerValue] ==[[ZSTF3Preferences shared].UID integerValue])
            {
                self.shadowView.hidden = NO;
                self.moreFunView.hidden = YES;
                [UIView animateWithDuration:0.3f animations:^{
                    
                    self.exitAlertView.hidden = NO;
                    
                }];
            }
        }
            break;
        case 102:
        {
            [self.engine makeTopicTop:self.messageDic.circleId MID:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] IsTop:!self.messageDic.isTop];
        }
            break;
        case 103:
        {
            ZSTTopicReportViewController *reportVC = [[ZSTTopicReportViewController alloc] init];
            reportVC.circleId = self.circleId;
            reportVC.reportDelegate = self;
            reportVC.msgId = [NSString stringWithFormat:@"%@",@(self.messageDic.msgId)];
            [self.navigationController pushViewController:reportVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    self.shadowView.hidden = YES;
    [self.view addSubview:self.shadowView];
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.6;
    [self.shadowView addSubview:blackView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss:)];
    [blackView addGestureRecognizer:tap];
}

- (void)buildSureAlertView
{
    self.exitAlertView = [[UIView alloc] init];
    self.exitAlertView.center = CGPointMake(self.shadowView.centerX, self.shadowView.centerY - 64);
    self.exitAlertView.bounds = (CGRect){CGPointZero,{WIDTH - 60,100}};
    self.exitAlertView.backgroundColor = [UIColor whiteColor];
    [self.shadowView addSubview:self.exitAlertView];
    
    CALayer *lineOne = [CALayer layer];
    lineOne.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineOne.frame = CGRectMake(0, 59.5, self.exitAlertView.bounds.size.width, 0.5);
    [self.exitAlertView.layer addSublayer:lineOne];
    
    CALayer *lineTwo = [CALayer layer];
    lineTwo.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineTwo.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 59.5, 0.5, 40.5);
    [self.exitAlertView.layer addSublayer:lineTwo];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.exitAlertView.bounds.size.width , 60)];
    tipLb.textColor = RGBACOLOR(56, 56, 56, 1);
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.font = [UIFont systemFontOfSize:14.0f];
    tipLb.text = @"您确定要删除该话题吗?";
    [self.exitAlertView addSubview:tipLb];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(0, 60, self.exitAlertView.bounds.size.width / 2, 40);
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [cancelBtn addTarget:self action:@selector(canceDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.exitAlertView addSubview:cancelBtn];
    
    UIButton *sureExitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureExitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureExitBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    sureExitBtn.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 60, self.exitAlertView.bounds.size.width / 2, 40);
    [sureExitBtn addTarget:self action:@selector(sureDelete:) forControlEvents:UIControlEventTouchUpInside];
    sureExitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.exitAlertView addSubview:sureExitBtn];
    
    self.exitAlertView.hidden = YES;
}

- (void)showMoreFun:(ZSTSNSAMessage *)messageDic
{
    self.shadowView.hidden = NO;
    
    UILabel *makeLB = (UILabel *)[self.moreFunView viewWithTag:102 * 100];
    if (messageDic.isTop == 0)
    {
        makeLB.text = @"置 顶";
    }
    else
    {
        makeLB.text = @"取消置顶";
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isAdmin = [[ud objectForKey:@"isAdmin"] integerValue] == 0 ? NO : YES;
    
    if ([self.messageDic.chatUID integerValue] == [[ZSTF3Preferences shared].UID integerValue])
    {
        [self updateMoreFunView:YES IsManager:isAdmin];
    }
    else
    {
        [self updateMoreFunView:NO IsManager:isAdmin];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.moreFunView.hidden = NO;
    }];
}

- (void)pickerViewDismiss:(UITapGestureRecognizer *)tap
{
    self.shadowView.hidden = YES;
    self.moreFunView.hidden = YES;
}

- (void)sureDelete:(UIButton *)sender
{
    [self.engine removeTopic:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] CircleId:self.circleId];
}

- (void)canceDelete:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.exitAlertView.hidden = YES;
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)removeTopicDidSuccess:(NSDictionary *)response
{
    self.exitAlertView.hidden = YES;
    self.shadowView.hidden = YES;
    
    if ([self.detailDelegate respondsToSelector:@selector(removeTopic:)])
    {
        [self.detailDelegate removeTopic:self.messageDic];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
    
}

- (void)removeTopicDidFailure:(NSString *)resultString
{
    
}

- (void)makeTopicTopDidSuccess:(NSDictionary *)response
{
    self.moreFunView.hidden = YES;
    self.shadowView.hidden = YES;
    
    [TKUIUtil alertInView:self.view withTitle:[response safeObjectForKey:@"notice"] withImage:nil];
    
    if ([self.detailDelegate respondsToSelector:@selector(makeTopTopic:)])
    {
        [self.detailDelegate makeTopTopic:self.messageDic];
    }
    
    ZSTMyChatroomHeaderCell *cell = (ZSTMyChatroomHeaderCell *)[self.topicContentTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
    cell.headerBtn.hidden = !cell.headerBtn.isHidden;
}

- (void)makeTopicTopDidFailure:(NSInteger)resultCode
{
    
}

- (void)hideDarkScroll:(PMRepairButton *)sender
{
    for (UIView *dSubview in self.darkScroll.subviews)
    {
        dSubview.hidden = YES;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.navigationController.navigationBarHidden = NO;
        self.darkScroll.backgroundColor = [UIColor whiteColor];
        self.darkScrollView.backgroundColor = [UIColor whiteColor];
        
    } completion:^(BOOL finished) {
        
        self.darkScroll.hidden = YES;
        self.darkScrollView.hidden = YES;
        
    }];
}

- (void)buildDetailDarkScroll:(NSArray *)imgArr Offset:(NSInteger)offsetInteger
{
    if (self.darkScroll.subviews.count < 2)
    {
        
    }
    else
    {
        for (UIView *dSubview in self.darkScroll.subviews)
        {
            if ([dSubview isKindOfClass:[UIImageView class]])
            {
                [dSubview removeFromSuperview];
            }
            else
            {
                dSubview.hidden = NO;
            }
        }
    }
   
    
    for (NSInteger i = 0; i < imgArr.count; i++)
    {
        UIScrollView *s = [[UIScrollView alloc] initWithFrame:CGRectMake(i * WIDTH, 0, WIDTH, HEIGHT - 60 - 50)];
        s.minimumZoomScale = 1.0;
        s.maximumZoomScale = 3.0;
        s.delegate = [ZSTBigPicDelegate defaultDelegate];
        s.tag = 1111111 + i;
        [self.darkScroll addSubview:s];
        
        UIImageView *detailIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, s.bounds.size.width, s.bounds.size.height)];
        detailIV.contentMode = UIViewContentModeScaleAspectFit;
        detailIV.userInteractionEnabled = YES;
        detailIV.tag = 111111 + i;
        UITapGestureRecognizer *disTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(noLookBigPic:)];
        [detailIV addGestureRecognizer:disTap];
        if ([imgArr[i] rangeOfString:@"http://"].length > 0)
        {
            [detailIV setImageWithURL:[NSURL URLWithString:imgArr[i]]];
        }
        else
        {
            NSString *dataString = imgArr[i];
            NSData *imgData = [[NSData alloc] initWithBase64Encoding:dataString];
            detailIV.image = [UIImage imageWithData:imgData];
        }
        
        [s addSubview:detailIV];
    }
    
    
    self.darkScroll.contentSize = CGSizeMake(WIDTH * imgArr.count, 0);
    self.darkScroll.pagingEnabled = YES;
    self.darkScroll.contentOffset = CGPointMake(offsetInteger * WIDTH, 0);
    
    
}

- (void)showFaceSelectView:(PMRepairButton *)sender
{
    [self.inputTV resignFirstResponder];
    
    if (self.isFaceOpen)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = NO;
        }];
        
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT - 64 - self.faceView.bounds.size.height, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50 - self.faceView.bounds.size.height, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = YES;
            
        }];
        
    }
}

- (void)buildFaceView
{
    self.faceView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64, WIDTH, 165)];
    self.faceView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    self.faceScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 165)];
    self.faceScroll.delegate = self;
    self.faceScroll.showsHorizontalScrollIndicator = NO;
    self.faceScroll.showsVerticalScrollIndicator = NO;
    self.faceScroll.pagingEnabled = YES;
    self.faceScroll.delegate = self;
    [self.faceView addSubview:self.faceScroll];
    self.faceScroll.contentSize = CGSizeMake(WIDTH * 3, 0);
    
    for (NSInteger i = 0; i < 72; i++)
    {
        NSInteger page = i / 24;
        
        NSInteger pageRow = i;
        if (i >= 24)
        {
            pageRow = i - page * 24;
        }
        
        NSInteger row = pageRow / 6;
        NSInteger column = i % 6;
        
        
        
        PMRepairButton *faceOneBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(page * WIDTH + column * WIDTH / 6, row * 30 + (row + 1) * 5, WIDTH / 6, 30)];
        NSString *faceName = @"";
        if (i < 9)
        {
            faceName = [NSString stringWithFormat:@"face00%@",@(i + 1)];
        }
        else if (i < 99)
        {
            faceName = [NSString stringWithFormat:@"face0%@",@(i + 1)];
        }
        faceOneBtn.tag = i + 1001;
        [faceOneBtn addTarget:self action:@selector(selectedFace:) forControlEvents:UIControlEventTouchUpInside];
        [faceOneBtn setImage:[UIImage imageNamed:faceName] forState:UIControlStateNormal];
        [self.faceScroll addSubview:faceOneBtn];
    }
    
    self.facePageControl = [[UIPageControl alloc] init];
    self.facePageControl.center = CGPointMake(WIDTH / 2, 155);
    self.facePageControl.bounds = (CGRect){CGPointZero,{WIDTH,10}};
    self.facePageControl.numberOfPages = 3;
    [self.faceView addSubview:self.facePageControl];
    
    [self.view addSubview:self.faceView];
}

#pragma mark - 选择表情
- (void)selectedFace:(PMRepairButton *)sender
{
    NSInteger index = sender.tag - 1001;
    //    NSString *selectText = [[CommonFunc sharedCommon].faceTilteArr objectAtIndex:index];
    NSMutableArray *emojiArr = [NSMutableArray array];
    for (NSInteger i = 0; i < 72; i++)
    {
        NSString *faceName = @"";
        if (i < 9)
        {
            faceName = [NSString stringWithFormat:@"face00%@",@(i + 1)];
        }
        else if (i < 99)
        {
            faceName = [NSString stringWithFormat:@"face0%@",@(i + 1)];
        }
        
        [emojiArr addObject:faceName];
    }
    EmojiTextAttachment *emojiTextAttachment = [EmojiTextAttachment new];
    emojiTextAttachment.emojiTag = [NSString stringWithFormat:@"[%@]",emojiArr[index]];
    emojiTextAttachment.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",emojiArr[index]]];
    emojiTextAttachment.emojiSize = 16.0f;
    [self.inputTV.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:emojiTextAttachment] atIndex:self.inputTV.selectedRange.location];
    self.inputTV.selectedRange = NSMakeRange(self.inputTV.selectedRange.location + 1, self.inputTV.selectedRange.length);
    self.backUpLB.hidden = YES;
    [self resetTextStyle];
}

#pragma mark - 应该是重置textView的style
- (void)resetTextStyle {
    NSRange wholeRange = NSMakeRange(0, self.inputTV.textStorage.length);
    
    [self.inputTV.textStorage removeAttribute:NSFontAttributeName range:wholeRange];
    
    [self.inputTV.textStorage addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:wholeRange];
}

- (void)noLookBigPic:(UITapGestureRecognizer *)tap
{
    for (UIView *dSubview in self.darkScroll.subviews)
    {
        dSubview.hidden = YES;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.navigationController.navigationBarHidden = NO;
        self.darkScroll.backgroundColor = [UIColor whiteColor];
        self.darkScrollView.backgroundColor = [UIColor whiteColor];
        
    } completion:^(BOOL finished) {
        
        self.darkScroll.hidden = YES;
        self.darkScrollView.hidden = YES;
        
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.topicContentTableView)
    {
        CGFloat sectionHeaderHeight = 20;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        
//        self.inputView.hidden = YES;
//        self.topicContentTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
        
        if (!self.isLoadOld && self.commentsHasMore) {
            CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
            if (scrollPosition < [self footerLoadMoreHeight]) {
                [self loadMore];
            }
        }
    }
    else if (scrollView == self.darkScroll)
    {
        NSString *imageArrString = self.messageDic.imageArrString;
        NSArray *imageArr = [imageArrString componentsSeparatedByString:@","];
        NSInteger imageOffsetTag = scrollView.contentOffset.x / WIDTH + 1;
        self.showNumLB.text = [NSString stringWithFormat:@"%@ / %@",@(imageOffsetTag),@(imageArr.count)];
    }
        
}

- (void)saveThisPic:(PMRepairButton *)sender
{
    NSInteger i = self.darkScroll.contentOffset.x / WIDTH;
    UIScrollView *imgS = (UIScrollView *)[self.darkScroll viewWithTag:1111111 + i];
    UIImageView *imgIV = (UIImageView *)[imgS viewWithTag:111111 + i];
    UIImage *targetImg = imgIV.image;
    [self saveImageToPhotos:targetImg];
}

- (void)saveImageToPhotos:(UIImage*)savedImage
{
    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)image: (UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error)
    {
        NSLog(@"error --> %@",[error description]);
    }
    else
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"保存成功", nil) withImage:nil];
    }
}


#pragma mark - 开始加载更多
- (void)willBeginLoadingMore
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.footerView;
    [fv becomLoading];
    
    if ([self.nowShowType isEqualToString:@"discuss"])
    {
        ZSTSNSAMessageComments *comments = [self.commentArr lastObject];
        NSInteger minCommentsId = comments.mcid;
        [self.engine getAllComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.messageDic.msgId)] PageSize:kPageSize currentMsgId:[NSString stringWithFormat:@"%@",@(minCommentsId)]];
        
    }
}

#pragma mark - 结束加载更多
- (void) loadMoreCompleted
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.footerView;
    [fv endLoading];

    if (!self.commentsHasMore) {
        
        self.topicContentTableView.tableFooterView = nil;
        fv.activityIndicator.hidden = YES;
        fv.infoLB.hidden = YES;
    }
    else
    {
        self.isLoadOld = NO;
    }
}


- (BOOL) loadMore
{
    if (self.isLoadOld)
        return NO;
    
    [self willBeginLoadingMore];
    self.isLoadOld = YES;
    return YES;
}

- (CGFloat) footerLoadMoreHeight
{
    if (self.footerView)
        return self.footerView.frame.size.height;
    else
        return 10;
}

- (void)goToUserDetail:(NSString *)loginMsisdn
{
    ZSTChatroomMenDetailViewController *menDetailVC = [[ZSTChatroomMenDetailViewController alloc] init];
    menDetailVC.chatMenMsisdn = loginMsisdn;
    menDetailVC.chatMenOrigineMsisdn = loginMsisdn;
    [self.navigationController pushViewController:menDetailVC animated:YES];
}

#pragma mark - 回到上一个视图
- (void)goToBackVC:(PMRepairButton *)sender
{
    if (self.isFromNews)
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
    else
    {
        [self popViewController];
    }
}

@end
