//
//  ZSTTopicDetailCommentCell.h
//  SNSA
//
//  Created by pmit on 15/10/13.
//
//

#import <UIKit/UIKit.h>
#import "CDTDisplayView.h"

@interface ZSTTopicDetailCommentCell : UITableViewCell

@property (strong,nonatomic) UIImageView *headerIV;
@property (strong,nonatomic) UILabel *nameLB;
@property (strong,nonatomic) UILabel *timeLB;
@property (strong,nonatomic) UIView *commentCommentView;
//@property (strong,nonatomic) UILabel *commentContentLB;
@property (strong,nonatomic) CDTDisplayView *commentContentLB;
@property (strong,nonatomic) CALayer *line;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessageComments *)commentArr;

@end
