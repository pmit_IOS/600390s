//
//  CDTDisplayView.h
//  CoreTextDemo
//
//  Created by pmit on 15/10/19.
//  Copyright © 2015年 com.9588.f3. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreTextData.h"
#import "CoreTextLinkData.h"
#import "CoreTextUtils.h"

@interface CDTDisplayView : UIView

@property (strong, nonatomic) CoreTextData * data;


@end
