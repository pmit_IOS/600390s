//
//  ZSTSNSAMessage.m
//  SNSA
//
//  Created by pmit on 15/9/19.
//
//

#import "ZSTSNSAMessage.h"
#import "ZSTSNSAMessageComments.h"
#import "ZSTSNSAThumShare.h"

@implementation ZSTSNSAMessage


- (id)initWithMessageDic:(NSDictionary *)messageDic
{
    self = [super init];
    if (self)
    {
        NSMutableArray *trueImgArr = [NSMutableArray array];
        NSMutableArray *commentArr = [NSMutableArray array];
        NSMutableArray *thumbsArr = [NSMutableArray array];
        self.addTime = [[messageDic safeObjectForKey:@"addtime"] integerValue];
        self.chatNewsAvatar = [messageDic safeObjectForKey:@"headphoto"];
        id imageArr = [messageDic safeObjectForKey:@"imgurl"];
        if ([imageArr isKindOfClass:[NSArray class]])
        {
            for (id trueImgString in (NSArray *)imageArr)
            {
                if (trueImgString && [trueImgString isKindOfClass:[NSString class]])
                {
                    [trueImgArr addObject:trueImgString];
                }
                else if (trueImgString && [trueImgString isKindOfClass:[NSData class]])
                {
                    NSString *base64StringData = [trueImgString base64Encoding];
                    [trueImgArr addObject:base64StringData];
                }
                else
                {
                    break;
                }
            }
        }
        else
        {
            trueImgArr = [[((NSString *)imageArr) componentsSeparatedByString:@","] mutableCopy];
        }
        
        NSString *imageUrlString = [trueImgArr componentsJoinedByString:@","];
        self.imageArrString = imageUrlString;
        self.chatUID = [messageDic safeObjectForKey:@"uid"];
        self.chatMsisdn = [messageDic safeObjectForKey:@"msisdn"];
        self.msgId = [[messageDic safeObjectForKey:@"mid"] integerValue];
        self.chatContent = [messageDic safeObjectForKey:@"mcontent"];
        self.chatNewsUserName = [messageDic safeObjectForKey:@"uname"];
        self.thumbupCount = [[messageDic safeObjectForKey:@"thumbup"] integerValue];
        self.isThumb = [[messageDic safeObjectForKey:@"isthumbup"] integerValue];
        self.commentCount = [[messageDic safeObjectForKey:@"comscount"] integerValue];
        self.oneImgWidth = [[messageDic safeObjectForKey:@"oneImgWidth"] doubleValue];
        self.oneImgHeight = [[messageDic safeObjectForKey:@"oneImgHeight"] doubleValue];
        NSArray *getTheCGArr = [messageDic safeObjectForKey:@"mgscom"];
        if (getTheCGArr.count > 0)
        {
            for (NSDictionary *dic in getTheCGArr)
            {
                ZSTSNSAMessageComments *snsaComments = [[ZSTSNSAMessageComments alloc] initWithCommentDic:dic];
                snsaComments.commentDic = dic;
                [commentArr addObject:snsaComments];
            }
        }
        
        self.cgArr = commentArr;
        self.isTop = [[messageDic safeObjectForKey:@"istop"] integerValue];
        
        NSArray *getTheThumArr = [messageDic safeObjectForKey:@"msgthump"];
        if (getTheThumArr.count > 0)
        {
            for (NSDictionary *dic in getTheThumArr)
            {
                ZSTSNSAThumShare *thumb = [[ZSTSNSAThumShare alloc] initWithDic:dic];
                thumb.circleId = self.circleId;
                [thumbsArr addObject:thumb];
            }
        }
        
        self.thumArr = thumbsArr;
    }
    
    return self;
}

- (id)initWithMessageDicBySqlite:(NSDictionary *)messageDic
{
    self = [super init];
    if (self)
    {
        self.dao = [[ZSTDao alloc] init];
        self.addTime = [[messageDic safeObjectForKey:@"addTime"] integerValue];
        self.chatContent = [messageDic safeObjectForKey:@"chatContent"];
        self.msgId = [[messageDic safeObjectForKey:@"chatMID"] integerValue];
        self.chatMsisdn = [messageDic safeObjectForKey:@"chatMsisdn"];
        self.chatNewsAvatar = [messageDic safeObjectForKey:@"chatNewsAvatar"];
        self.chatNewsUserName = [messageDic safeObjectForKey:@"chatNewsUName"];
        self.chatUID = [messageDic safeObjectForKey:@"chatUID"];
        self.circleId = [messageDic safeObjectForKey:@"circleID"];
        self.isTop = [[messageDic safeObjectForKey:@"istop"] integerValue];
        self.thumbupCount = [[messageDic safeObjectForKey:@"goodCount"] integerValue];
        self.imageArrString = [messageDic safeObjectForKey:@"userPhoto"];
        self.commentCount = [[messageDic safeObjectForKey:@"commentCount"] integerValue];
        self.parentId = [messageDic safeObjectForKey:@"chatParentId"];
        self.shareCount = 0;
        self.isThumb = [[messageDic safeObjectForKey:@"isthumbup"] integerValue];
        NSData *commentsData = [messageDic safeObjectForKey:@"commentsArr"];
        NSData *thumbsData = [messageDic safeObjectForKey:@"thumbArr"];
        self.oneImgHeight = [[messageDic safeObjectForKey:@"oneImgHeight"] doubleValue];
        self.oneImgWidth = [[messageDic safeObjectForKey:@"oneImgWidth"] doubleValue];
        self.cgArr = [NSKeyedUnarchiver unarchiveObjectWithData:commentsData];
        self.thumArr = [NSKeyedUnarchiver unarchiveObjectWithData:thumbsData];
    }
    
    return self;
}

@end
