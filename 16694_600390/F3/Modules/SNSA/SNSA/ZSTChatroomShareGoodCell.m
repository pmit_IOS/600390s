//
//  ZSTChatroomShareGoodCell.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTChatroomShareGoodCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTChatroomShareGoodCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.menHeaderIV)
    {
        self.menHeaderIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 30, 30)];
        self.menHeaderIV.layer.masksToBounds = YES;
        self.menHeaderIV.layer.cornerRadius = 15;
        self.menHeaderIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
        [self.contentView addSubview:self.menHeaderIV];
        
        self.headerNameLB = [[UILabel alloc] initWithFrame:CGRectMake(60, 15, WIDTH - 60, 15)];
        self.headerNameLB.textAlignment = NSTextAlignmentLeft;
        self.headerNameLB.font = [UIFont systemFontOfSize:12.0f];
        self.headerNameLB.textColor = RGBACOLOR(248, 145, 70, 1);
        self.headerNameLB.text = @"小明真的碉堡了";
        [self.contentView addSubview:self.headerNameLB];
        
        self.headerTimeLB = [[UILabel alloc] initWithFrame:CGRectMake(61, 30, WIDTH - 61, 15)];
        self.headerTimeLB.text = @"09/11 18:00";
        self.headerTimeLB.font = [UIFont systemFontOfSize:12.0f];
        self.headerTimeLB.textColor = RGBACOLOR(159, 159, 159, 1);
        [self.contentView addSubview:self.headerTimeLB];
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(0, 59, WIDTH, 1);
        [self.contentView.layer addSublayer:line];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

- (void)setCellData:(NSString *)menName DateString:(NSString *)dateString HeadeString:(NSString *)headerString
{
    if (self.isGood)
    {
        self.headerTimeLB.hidden = YES;
        self.headerNameLB.frame = CGRectMake(60, 15, WIDTH - 60, 30);
    }
    else
    {
        self.headerTimeLB.hidden = NO;
        self.headerNameLB.frame = CGRectMake(60, 15, WIDTH - 60, 15);
    }
    
    [self.menHeaderIV setImageWithURL:[NSURL URLWithString:headerString] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    self.headerNameLB.text = menName;
//    self.headerTimeLB.text = dateString;
}

@end
