//
//  ZSTMyNewsMessageCell.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <UIKit/UIKit.h>
#import "TQRichTextView.h"

@interface ZSTMyNewsMessageCell : UITableViewCell

@property (strong,nonatomic) NSDictionary *myNewsMessageDic;
@property (strong,nonatomic) UIImageView *headerIV;
@property (strong,nonatomic) UIImageView *contentIV;
@property (strong,nonatomic) UILabel *nameLB;
@property (strong,nonatomic) UILabel *timeLB;
//@property (strong,nonatomic) UILabel *contentLB;
//@property (strong,nonatomic) TQRichTextView *contentLB;
@property (strong,nonatomic) UILabel *contentLB;
@property (strong,nonatomic) UILabel *topicContentLB;

- (void)createUI;
- (void)setCellData:(NSDictionary *)aboutMeMessageDic;

@end
