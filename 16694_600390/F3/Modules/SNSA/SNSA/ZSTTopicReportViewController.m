//
//  ZSTTopicReportViewController.m
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import "ZSTTopicReportViewController.h"
#import "ZSTChatroomReportCell.h"
#import "ZSTReasonInputCell.h"
#import <ZSTUtils.h>
#import "ZSTYouYunEngine.h"

@interface ZSTTopicReportViewController () <UITableViewDataSource,UITableViewDelegate,ZSTYouYunEngineDelegate>

@property (strong,nonatomic) UITableView *reportTableView;
@property (strong,nonatomic) NSArray *reportTitleArr;
@property (strong,nonatomic) NSIndexPath *selectIndexPath;
@property (strong,nonatomic) ZSTYouYunEngine *engine;

@end

@implementation ZSTTopicReportViewController

static NSString *const reportCell = @"reportCell";
static NSString *const reportInputCell = @"reportInputCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    self.reportTitleArr = @[@"暴力色情",@"反动违法",@"欺诈勒索",@"无故骚扰",@"内容侵权",@"其他原因"];
    self.selectIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"举报话题", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    clearView.backgroundColor = [UIColor clearColor];
    clearView.layer.cornerRadius = 2.0f;
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 40, 30)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.2;
    alphaView.layer.cornerRadius = 2.0f;
    [clearView addSubview:alphaView];
    
    UIButton *submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [submitBtn setTitle:@"提交" forState:UIControlStateNormal];
    submitBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    submitBtn.frame = CGRectMake(0, 0, 40, 30);
    [submitBtn addTarget:self action:@selector(sendToReport:) forControlEvents:UIControlEventTouchUpInside];
    [clearView addSubview:submitBtn];

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:clearView];
    
    self.view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    
    [self buildReportTableView];
    self.reason = self.reportTitleArr[0];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildReportTableView
{
    self.reportTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.reportTableView.delegate = self;
    self.reportTableView.dataSource = self;
    self.reportTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.reportTableView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    [self.reportTableView setSeparatorColor:[UIColor whiteColor]];
    [self.reportTableView registerClass:[ZSTChatroomReportCell class] forCellReuseIdentifier:reportCell];
    [self.reportTableView registerClass:[ZSTReasonInputCell class] forCellReuseIdentifier:reportInputCell];
    [self.view addSubview:self.reportTableView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    headerView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    UILabel *titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - 30, 25)];
    titleLB.textColor = RGBACOLOR(159, 159, 159, 1);
    titleLB.font = [UIFont systemFontOfSize:12.0f];
    titleLB.textAlignment = NSTextAlignmentLeft;
    titleLB.text = @"请选择举报该话题的原因：";
    [headerView addSubview:titleLB];
    
    self.reportTableView.tableHeaderView = headerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0)
    {
        return self.reportTitleArr.count;
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        ZSTChatroomReportCell *cell = [tableView dequeueReusableCellWithIdentifier:reportCell];
        [cell createUI];
        if (self.selectIndexPath == indexPath)
        {
            cell.singleBtn.selected = YES;
        }
        
        [cell setCellReportTitle:self.reportTitleArr[indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return cell;
    }
    else
    {
        ZSTReasonInputCell *cell = [tableView dequeueReusableCellWithIdentifier:reportInputCell];
        [cell createUI];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] init];
    headerView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomReportCell *cell = (ZSTChatroomReportCell *)[tableView cellForRowAtIndexPath:self.selectIndexPath];
    cell.singleBtn.selected = NO;
    ZSTChatroomReportCell *newsCell = (ZSTChatroomReportCell *)[tableView cellForRowAtIndexPath:indexPath];
    newsCell.singleBtn.selected = YES;
    self.selectIndexPath = indexPath;
    if (indexPath.section == 0)
    {
        self.reason = self.reportTitleArr[indexPath.row];
    }
    
}

- (void)sendToReport:(UIButton *)sender
{
    ZSTReasonInputCell *cell = (ZSTReasonInputCell *)[self.reportTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
    [self.engine sendTopicReport:self.circleId MID:self.msgId Reason:self.reason Remark:cell.inputTV.text];
}

- (void)sendTopicReportDidSuccess:(NSDictionary *)response
{
    if ([self.reportDelegate respondsToSelector:@selector(sendReportResult:)])
    {
        [self.reportDelegate sendReportResult:[response safeObjectForKey:@"notice"]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)sendTopicReportDidFailiure:(NSString *)resultString
{
    if ([self.reportDelegate respondsToSelector:@selector(sendReportResult:)])
    {
        [self.reportDelegate sendReportResult:resultString];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)keyBoardWillShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect rect = self.view.frame;
        if (IS_IOS7 && !IS_IOS8) {
            rect.origin.y = - 80;
        }else{
            rect.origin.y = - 120;
        }
        
        self.view.frame = rect;
        
    }];
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:@"myAnimations" context:nil];
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    self.view.frame = rect;
    [UIView commitAnimations];
}

@end
