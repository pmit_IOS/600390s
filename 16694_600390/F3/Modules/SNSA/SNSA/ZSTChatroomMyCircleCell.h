//
//  ZSTChatroomMyCircleCell.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import <UIKit/UIKit.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import <PMRepairButton.h>

@protocol ZSTChatroomMyCircleCellDelegate <NSObject>

@optional
- (void)goToMember:(NSDictionary *)circleDic;
- (void)goToCareThisCircle:(NSDictionary *)circleDic;

@end

@interface ZSTChatroomMyCircleCell : UITableViewCell

@property (strong,nonatomic) UIImageView *circleIV;
@property (strong,nonatomic) UILabel *circlTitleLB;
@property (strong,nonatomic) UILabel *circleIntroduceLB;
@property (strong,nonatomic) UIImageView *memberIV;
@property (strong,nonatomic) PMRepairButton *memberBtn;
@property (strong,nonatomic) UILabel *memberCountLB;
@property (strong,nonatomic) UIButton *waitCareBtn;
@property (weak,nonatomic) id<ZSTChatroomMyCircleCellDelegate> circleCellDelegate;
@property (strong,nonatomic) NSDictionary *circleDic;

- (void)createUI;
- (void)setCellData:(NSDictionary *)myCircleDic;

@end
