//
//  ZSTChatroomShareGoodCell.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomShareGoodCell : UITableViewCell

@property (strong,nonatomic) UIImageView *menHeaderIV;
@property (strong,nonatomic) UILabel *headerNameLB;
@property (strong,nonatomic) UILabel *headerTimeLB;
@property (assign,nonatomic) BOOL isGood;

- (void)createUI;
- (void)setCellData:(NSString *)menName DateString:(NSString *)dateString HeadeString:(NSString *)headerString;

@end
