//
//  ZSTChatroomLookPicViewController.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTChatroomLookPicViewController.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImageManager.h>

@interface ZSTChatroomLookPicViewController () <UIScrollViewDelegate>

@property (strong,nonatomic) UIScrollView *bigPicScroll;

@end

@implementation ZSTChatroomLookPicViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    if([[[UIDevice currentDevice]systemVersion] doubleValue]>=7.0){
        self.navigationController.navigationBar.barTintColor = [UIColor blackColor];
        [[UINavigationBar appearance]setTintColor:[UIColor whiteColor]];
    }else{
        self.navigationController.navigationBar.tintColor = [UIColor blackColor];
    }
    self.view.backgroundColor = [UIColor blackColor];
    [self buildBigScroll];
    [self buildBigImgView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildBigScroll
{
    self.bigPicScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.bigPicScroll.delegate = self;
    self.bigPicScroll.backgroundColor = [UIColor blackColor];
    self.bigPicScroll.pagingEnabled = YES;
    self.bigPicScroll.contentSize = CGSizeMake(self.imageUrlArr.count * WIDTH, 0);
    //设置最大伸缩比例
    self.bigPicScroll.maximumZoomScale=2.0;
    //设置最小伸缩比例
    self.bigPicScroll.minimumZoomScale=0.5;
    [self.view addSubview:self.bigPicScroll];
}

- (void)buildBigImgView
{
    for (NSInteger i = 0; i < self.imageUrlArr.count; i++)
    {
        UIImageView *bigIV = [[UIImageView alloc] initWithFrame:CGRectMake(i * WIDTH, 0, WIDTH, HEIGHT)];
        bigIV.contentMode = UIViewContentModeScaleAspectFit;
        [bigIV setImageWithURL:[NSURL URLWithString:self.imageUrlArr[i]]];
        [self.bigPicScroll addSubview:bigIV];
    }
    
    self.bigPicScroll.contentOffset = CGPointMake(self.selectInteger * WIDTH, 0);
}

-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    for (UIView *v in scrollView.subviews){
        return v;
    }
    return nil;
}

@end
