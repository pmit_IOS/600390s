//
//  ZSTNewsTopicViewController.m
//  SNSA
//
//  Created by pmit on 15/9/14.
//
//

#import "ZSTNewsTopicViewController.h"
#import <ZSTUtils.h>
#import <ZSTPhotoTypeView.h>
#import <PMRepairButton.h>
#import <ShowBigViewController.h>
#import "CommonFunc.h"
#import "ZSTMyChatroomViewController.h"
#import "ZSTChatroomTopicDetailViewController.h"
#import <AssetsLibrary/ALAsset.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AssetsLibrary/ALAssetsGroup.h>
#import <AssetsLibrary/ALAssetRepresentation.h>
#import "ZSTChatroomDealPicViewController.h"
#import "EmojiTextAttachment.h"
#import "NSAttributedString+EmojiExtension.h"
#import "ZSTMyPhotoGroupViewController.h"


@interface ZSTNewsTopicViewController () <UIScrollViewDelegate,UITextViewDelegate,ZSTPhotoTypeViewDelegate,UINavigationControllerDelegate,UIImagePickerControllerDelegate,ZSTYouYunEngineDelegate,UIActionSheetDelegate,ZYQAssetPickerControllerDelegate,ZSTMyPhotoGroupViewControllerDelegate>

@property (strong,nonatomic) UIScrollView *newsTopicScrollView;
@property (strong,nonatomic) UIView *exitAlertView;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) UITextView *inputTF;
@property (strong,nonatomic) UIView *faceView;
@property (strong,nonatomic) UIScrollView *faceScroll;
@property (strong,nonatomic) UIPageControl *facePageControl;
@property (assign,nonatomic) BOOL isFaceOpen;
@property (strong,nonatomic) ZSTPhotoTypeView *photoView;
@property (strong,nonatomic) PMRepairButton *addPhotoBtn;
@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) UIImagePickerController *iconPickerController;
@property (strong,nonatomic) UILabel *backUpLB;
@property (strong,nonatomic) ALAssetsLibrary *library;
@property (strong,nonatomic) NSMutableArray *groups;
@property (copy,nonatomic) NSString *showSelectString;
@property (strong,nonatomic) NSString *inputString;

@end

@implementation ZSTNewsTopicViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildNavigation];
    [self buildScroll];
    [self buildInputView];
    [self resetTextStyle];
    [self buildPicSelectView];
    [self buildFaceView];
    [self buildShadowView];
    [self buildSureAlertView];
    [self buildPhotoPickView];
    
    self.isFromDeal = NO;
    
    if (self.imageDataArr.count == 0)
    {
        self.imageDataArr = [NSMutableArray array];
    }
    
    if (self.roomArrayOK.count != 0)
    {
        self.photoMenuItems = [NSMutableArray array];
        [self.photoMenuItems addObjectsFromArray:self.roomArrayOK];
        [self chatUploadImage:self.roomArrayOK];
    }
    else if (self.photoMenuItems.count == 0)
    {
        self.photoMenuItems = [NSMutableArray array];
    }
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildNavigation
{
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"新话题", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(surePop:)];
    
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake(0, 0, 30, 20);
    [sendBtn setTitle:@"发表" forState:UIControlStateNormal];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [sendBtn addTarget:self action:@selector(sendNewsTopic:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:sendBtn];
}

- (void)buildScroll
{
    self.newsTopicScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.newsTopicScrollView.showsHorizontalScrollIndicator = NO;
    self.newsTopicScrollView.showsVerticalScrollIndicator = NO;
    self.newsTopicScrollView.backgroundColor = RGBACOLOR(240, 240, 240, 1);
    self.newsTopicScrollView.userInteractionEnabled = YES;
    [self.view addSubview:self.newsTopicScrollView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignAllResponse:)];
    [self.newsTopicScrollView addGestureRecognizer:tap];
}

- (void)buildInputView
{
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, 15, WIDTH, 125)];
    self.inputView.layer.borderColor = RGBACOLOR(159, 159, 159, 1).CGColor;
    self.inputView.backgroundColor = [UIColor whiteColor];
    self.inputView.layer.borderWidth = 0.2;
    self.inputView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    [self.newsTopicScrollView addSubview:self.inputView];
    
    self.inputTF = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 80)];
    self.inputTF.font = [UIFont systemFontOfSize:14.0f];
    self.inputTF.delegate = self;
    self.inputTF.returnKeyType = UIReturnKeyDone;
    [self.inputView addSubview:self.inputTF];
    
    
    self.backUpLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 4, self.inputTF.bounds.size.width - 20, 20)];
    self.backUpLB.text = @"想说点什么...";
    self.backUpLB.font = [UIFont systemFontOfSize:12.0f];
    self.backUpLB.textColor = RGBACOLOR(204, 204, 204, 1);
    [self.inputTF addSubview:self.backUpLB];
    
    
    UIView *faceView = [[UIView alloc] initWithFrame:CGRectMake(0, 80, WIDTH, 45)];
    faceView.backgroundColor = [UIColor whiteColor];
    [self.inputView addSubview:faceView];
    
    PMRepairButton *faceBtn = [[PMRepairButton alloc] init];
    faceBtn.frame = CGRectMake(faceView.bounds.size.width - 10 - 30, 10, 30, 30);
    [faceBtn setImage:[UIImage imageNamed:@"chatroom_face.png"] forState:UIControlStateNormal];
    [faceBtn addTarget:self action:@selector(showFaceView:) forControlEvents:UIControlEventTouchUpInside];
    [faceView addSubview:faceBtn];
}

- (void)buildPicSelectView
{
    self.addPhotoBtn = [[PMRepairButton alloc] init];
    self.addPhotoBtn.frame = CGRectMake(15, 145, (WIDTH - 60) / 4, (WIDTH - 60) / 4);
    [self.addPhotoBtn setImage:[UIImage imageNamed:@"chatroom_addpicture.png"] forState:UIControlStateNormal];
    self.addPhotoBtn.backgroundColor = [UIColor whiteColor];
    [self.addPhotoBtn addTarget:self action:@selector(addPicture:) forControlEvents:UIControlEventTouchUpInside];
    [self.newsTopicScrollView addSubview:self.addPhotoBtn];
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shadowView addSubview:alphaView];
    self.shadowView.hidden = YES;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss:)];
    [alphaView addGestureRecognizer:tap];
}

- (void)buildSureAlertView
{
    self.exitAlertView = [[UIView alloc] init];
    self.exitAlertView.center = CGPointMake(self.shadowView.centerX, self.shadowView.centerY - 64);
    self.exitAlertView.bounds = (CGRect){CGPointZero,{WIDTH - 60,100}};
    self.exitAlertView.backgroundColor = [UIColor whiteColor];
    [self.shadowView addSubview:self.exitAlertView];
    
    CALayer *lineOne = [CALayer layer];
    lineOne.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineOne.frame = CGRectMake(0, 59.5, self.exitAlertView.bounds.size.width, 0.5);
    [self.exitAlertView.layer addSublayer:lineOne];
    
    CALayer *lineTwo = [CALayer layer];
    lineTwo.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineTwo.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 59.5, 0.5, 40.5);
    [self.exitAlertView.layer addSublayer:lineTwo];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.exitAlertView.bounds.size.width , 60)];
    tipLb.textColor = RGBACOLOR(56, 56, 56, 1);
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.font = [UIFont systemFontOfSize:14.0f];
    tipLb.text = @"您确定要退出本次编辑吗?";
    [self.exitAlertView addSubview:tipLb];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(0, 60, self.exitAlertView.bounds.size.width / 2, 40);
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [cancelBtn addTarget:self action:@selector(cancelTip:) forControlEvents:UIControlEventTouchUpInside];
    [self.exitAlertView addSubview:cancelBtn];
    
    UIButton *sureExitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureExitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureExitBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    sureExitBtn.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 60, self.exitAlertView.bounds.size.width / 2, 40);
    [sureExitBtn addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
    sureExitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.exitAlertView addSubview:sureExitBtn];
    
    self.exitAlertView.hidden = YES;
}


- (void)sendNewsTopic:(UIButton *)sender
{
    [self.inputTF resignFirstResponder];
    NSString * newsTopicString = [NSString stringWithFormat:@"%@", [self.inputTF.textStorage getPlainString]];
    
    
    
    if ([self.newsTopicDelegate respondsToSelector:@selector(sendNewsTopicJump:Content:)])
    {
        [self.newsTopicDelegate sendNewsTopicJump:self.imageDataArr Content:newsTopicString];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)surePop:(PMRepairButton *)sender
{
    [self.inputTF resignFirstResponder];
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.exitAlertView.hidden = NO;
        
    }];
}

- (void)cancelTip:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.exitAlertView.hidden = YES;
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

#pragma mark - 表情视图
- (void)buildFaceView
{
    self.faceView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64, WIDTH, 165)];
    self.faceView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    self.faceScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 165)];
    self.faceScroll.delegate = self;
    self.faceScroll.showsHorizontalScrollIndicator = NO;
    self.faceScroll.showsVerticalScrollIndicator = NO;
    self.faceScroll.pagingEnabled = YES;
    self.faceScroll.delegate = self;
    [self.faceView addSubview:self.faceScroll];
    self.faceScroll.contentSize = CGSizeMake(WIDTH * 3, 0);
    
    for (NSInteger i = 0; i < 72; i++)
    {
        NSInteger page = i / 24;
        
        NSInteger pageRow = i;
        if (i >= 24)
        {
            pageRow = i - page * 24;
        }
        
        NSInteger row = pageRow / 6;
        NSInteger column = i % 6;
        
        
        
        PMRepairButton *faceOneBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(page * WIDTH + column * WIDTH / 6, row * 30 + (row + 1) * 5, WIDTH / 6, 30)];
        NSString *faceName = @"";
        if (i < 9)
        {
            faceName = [NSString stringWithFormat:@"face00%@",@(i + 1)];
        }
        else if (i < 99)
        {
            faceName = [NSString stringWithFormat:@"face0%@",@(i + 1)];
        }
        faceOneBtn.tag = i + 1001;
        [faceOneBtn addTarget:self action:@selector(selectedFace:) forControlEvents:UIControlEventTouchUpInside];
        [faceOneBtn setImage:[UIImage imageNamed:faceName] forState:UIControlStateNormal];
        [self.faceScroll addSubview:faceOneBtn];
    }
    
    self.facePageControl = [[UIPageControl alloc] init];
    self.facePageControl.center = CGPointMake(WIDTH / 2, 155);
    self.facePageControl.bounds = (CGRect){CGPointZero,{WIDTH,10}};
    self.facePageControl.numberOfPages = 3;
    [self.faceView addSubview:self.facePageControl];
    
    [self.view addSubview:self.faceView];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.faceScroll)
    {
        self.facePageControl.currentPage = scrollView.contentOffset.x / WIDTH;
    }
}

- (void)showFaceView:(PMRepairButton *)sender
{
    [self.inputTF resignFirstResponder];
    
    if (self.isFaceOpen)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = NO;
        }];
         
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT - 64 - self.faceView.bounds.size.height, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = YES;
            
        }];
  
    }
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.isFaceOpen = NO;
    }];
    
    return YES;
}

- (void)buildPhotoPickView
{
    self.photoView = [[ZSTPhotoTypeView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 180)];
    self.photoView.backgroundColor = [UIColor whiteColor];
    self.photoView.photoTypeDelegate = self;
    [self.photoView createPhotoPicker];
    [self.view addSubview:self.photoView];
}


- (void)cancelPhotoView
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.photoView.frame = CGRectMake(0, HEIGHT, self.photoView.bounds.size.width, self.photoView.bounds.size.height);
        
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)addPicture:(PMRepairButton *)sender
{
    [self.inputTF resignFirstResponder];
//    self.shadowView.hidden = NO;
//    [UIView animateWithDuration:0.3f animations:^{
//        
//        self.photoView.frame = CGRectMake(0, HEIGHT - 64 - self.photoView.bounds.size.height, self.photoView.bounds.size.width, self.photoView.bounds.size.height);
//        
//    }];
    UIActionSheet *acSheet = [[UIActionSheet alloc] initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍照" otherButtonTitles:@"从相册中选择", nil];
    [acSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.iconPickerController = [[UIImagePickerController alloc] init];
    [_iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    switch (buttonIndex) {
        case 0:
            
            _iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            _iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
            _iconPickerController.showsCameraControls = YES;
            _iconPickerController.allowsEditing = YES;
            _iconPickerController.delegate = self;
            [self presentViewController:_iconPickerController animated:YES completion:nil];
            
            break;
        case 1:
        {
//            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc]init];
//            picker.maximumNumberOfSelection = 9 - self.photoMenuItems.count;
//            picker.assetsFilter = [ALAssetsFilter allPhotos];
//            picker.showEmptyGroups = NO;
//            picker.delegate = self;
//            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject,NSDictionary *bindings){
//                if ([[(ALAsset *)evaluatedObject valueForProperty:ALAssetPropertyType]isEqual:ALAssetTypeVideo]) {
//                    NSTimeInterval duration = [[(ALAsset *)evaluatedObject valueForProperty:ALAssetPropertyDuration]doubleValue];
//                    return duration >= 5;
//                }else{
//                    return  YES;
//                }
//            }];
            ZSTMyPhotoGroupViewController *photoVC = [[ZSTMyPhotoGroupViewController alloc] init];
            photoVC.photoGroupDelegate = self;
            photoVC.maxCount = 9 - self.photoMenuItems.count;
            UINavigationController *picker = [[UINavigationController alloc] initWithRootViewController:photoVC];
            [self presentViewController:picker animated:YES completion:nil];
        }
            break;
    }
}

- (void)pickerViewDismiss:(UITapGestureRecognizer *)tap
{
    [self cancelPhotoView];
}

- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
//    self.photoMenuItems = [NSMutableArray arrayWithArray:assets];
    [self.photoMenuItems addObjectsFromArray:assets];
    
    [picker dismissViewControllerAnimated:YES completion:^{
        self.isFromDeal = NO;
        [self chatUploadImage:self.photoMenuItems];
        
    }];
}

- (void)chatUploadImage:(NSArray *)arrayOK
{
    self.roomArrayOK = arrayOK;
    [self cancelPhotoView];
    
    for (UIView *oldSubView in self.newsTopicScrollView.subviews)
    {
        if ([oldSubView isKindOfClass:[PMRepairButton class]] && oldSubView != self.addPhotoBtn)
        {
            [oldSubView removeFromSuperview];
        }
    }
    
    for (NSInteger i = 0; i < arrayOK.count; i++)
    {
        NSInteger row = i / 4;
        NSInteger column = i % 4;
    
        id asset = arrayOK[i];
        
        UIImage *showImg;
        UIImage *tempImg;
        
        if ([asset isKindOfClass:[NSData class]])
        {
            tempImg = [UIImage imageWithData:asset];
            showImg = [UIImage imageWithData:asset];
        }
        else if ([asset isKindOfClass:[ALAsset class]])
        {
            tempImg = [UIImage imageWithCGImage:((ALAsset *)asset).defaultRepresentation.fullScreenImage];
            CGFloat borderWH = tempImg.size.width > tempImg.size.height ? tempImg.size.height : tempImg.size.width;
            showImg = [self imageCompressForSize:tempImg targetSize:CGSizeMake(borderWH, borderWH)];
        }
        else
        {
            tempImg = asset;
            CGFloat borderWH = tempImg.size.width > tempImg.size.height ? tempImg.size.height : tempImg.size.width;
            showImg = [self imageCompressForSize:tempImg targetSize:CGSizeMake(borderWH, borderWH)];
        }

            
        PMRepairButton *imgBtn = [[PMRepairButton alloc] init];
        imgBtn.frame = CGRectMake(15 + column * (WIDTH - 60) / 4 + column * 10, CGRectGetMaxY(self.inputView.frame) + 10 +  row * (WIDTH - 60) / 4 + row * 10, (WIDTH - 60) / 4, (WIDTH - 60) / 4);
        [imgBtn setImage:showImg forState:UIControlStateNormal];
        imgBtn.tag = 920109 + i;
        [imgBtn addTarget:self action:@selector(goToDealPic:) forControlEvents:UIControlEventTouchUpInside];
        [self.newsTopicScrollView addSubview:imgBtn];
            
        NSData *imageData = UIImageJPEGRepresentation(tempImg, 0.2);
        
        if (self.isFromDeal)
        {
            self.imageDataArr = [NSMutableArray array];
            self.isFromDeal = NO;
        }
        
        if ([self.imageDataArr containsObject:imageData])
        {
            continue;
        }
        else
        {
            [self.imageDataArr addObject:imageData];
        }
    }
    
    if (arrayOK.count == 9)
    {
        self.addPhotoBtn.hidden = YES;
    }
    else
    {
        NSInteger row = arrayOK.count / 4;
        NSInteger column = arrayOK.count % 4;
        self.addPhotoBtn.frame = CGRectMake(15 + column * (WIDTH - 60) / 4 + column * 10, CGRectGetMaxY(self.inputView.frame) + 10 +  row * (WIDTH - 60) / 4 + row * 10, (WIDTH - 60) / 4, (WIDTH - 60) / 4);
//        [self.newsTopicScrollView addSubview:self.addPhotoBtn];
    }
}

- (UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size
{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }

    UIGraphicsBeginImageContext(size);

    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();

    if(newImage == nil){
        NSLog(@"scale image fail");
    }

    UIGraphicsEndImageContext();

    return newImage;

}

-(void)textViewDidChange:(UITextView *)textView
{
    //    self.examineText =  textView.text;
    if (textView.text.length == 0) {
        
        self.backUpLB.hidden = NO;
        
    }else{
        
        self.backUpLB.hidden = YES;
    }
}

- (void)resignAllResponse:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.isFaceOpen = NO;
    }];
    
    [self.inputTF resignFirstResponder];
}

#pragma mark - 选择表情
- (void)selectedFace:(PMRepairButton *)sender
{
    NSInteger index = sender.tag - 1001;
//    NSString *selectText = [[CommonFunc sharedCommon].faceTilteArr objectAtIndex:index];
    NSMutableArray *emojiArr = [NSMutableArray array];
    for (NSInteger i = 0; i < 72; i++)
    {
        NSString *faceName = @"";
        if (i < 9)
        {
            faceName = [NSString stringWithFormat:@"face00%@",@(i + 1)];
        }
        else if (i < 99)
        {
            faceName = [NSString stringWithFormat:@"face0%@",@(i + 1)];
        }
        
        [emojiArr addObject:faceName];
    }
    EmojiTextAttachment *emojiTextAttachment = [EmojiTextAttachment new];
    emojiTextAttachment.emojiTag = [NSString stringWithFormat:@"[%@]",emojiArr[index]];
    emojiTextAttachment.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",emojiArr[index]]];
    emojiTextAttachment.emojiSize = 16.0f;
    [self.inputTF.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:emojiTextAttachment] atIndex:self.inputTF.selectedRange.location];
    self.inputTF.selectedRange = NSMakeRange(self.inputTF.selectedRange.location + 1, self.inputTF.selectedRange.length);
    self.backUpLB.hidden = YES;
    [self resetTextStyle];
}

#pragma mark - 应该是重置textView的style
- (void)resetTextStyle {
    //After changing text selection, should reset style.
    NSRange wholeRange = NSMakeRange(0, self.inputTF.textStorage.length);
    
    [self.inputTF.textStorage removeAttribute:NSFontAttributeName range:wholeRange];
    
    [self.inputTF.textStorage addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:wholeRange];
}

- (void)sendNewsTopicDidSuccess:(NSDictionary *)response
{
    NSString *imgUrlString = [[response safeObjectForKey:@"data"] safeObjectForKey:@"imgurl"];
    NSString * newsTopicString = [NSString stringWithFormat:@"%@", [self.inputTF.textStorage getPlainString]];
    if ([self.newsTopicDelegate respondsToSelector:@selector(sendNewsTopicFinish:Content:imgString:MID:)])
    {
        [self.newsTopicDelegate sendNewsTopicFinish:self.imageDataArr Content:newsTopicString imgString:imgUrlString MID:[NSString stringWithFormat:@"%@",[[response safeObjectForKey:@"data"] safeObjectForKey:@"mid"]]];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}



- (void)sendNewsTopicDidFailure:(NSString *)response
{
    
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {
        return;
    }
    
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [self.photoMenuItems addObject:tempImage];
    [self chatUploadImage:self.photoMenuItems];
    __block ZSTNewsTopicViewController *weakSelf = self;
    
    [picker dismissViewControllerAnimated:YES completion:^{
        
        [UIView animateWithDuration:0.3f animations:^{
            
            weakSelf.photoView.frame = CGRectMake(0, HEIGHT, weakSelf.photoView.bounds.size.width, weakSelf.photoView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            weakSelf.shadowView.hidden = YES;
            
        }];

        
    }];
}

- (void)goToDealPic:(PMRepairButton *)sender
{
    ZSTChatroomDealPicViewController *dealPicVC = [[ZSTChatroomDealPicViewController alloc] init];
    dealPicVC.weakNewsTopic = self;
    dealPicVC.dealImgArr = self.roomArrayOK;
    dealPicVC.showPage = sender.tag - 920109;
    [self.navigationController pushViewController:dealPicVC animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.inputTF resignFirstResponder];
}

- (void)finishPickerImageAndShow:(NSArray *)imgArr
{
    [self.photoMenuItems addObjectsFromArray:imgArr];
    [self chatUploadImage:self.photoMenuItems];
}

@end
