//
//  ZSTMyChatroomfooterCell.h
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import <UIKit/UIKit.h>
#import "ZSTSNSAMessage.h"

@protocol ZSTMyChatroomfooterCellDelegate <NSObject>

@optional
- (void)topicFunClick:(NSInteger)senderTag MessageDic:(ZSTSNSAMessage *)messageDic Btn:(UIButton *)sender;

@end

@interface ZSTMyChatroomfooterCell : UITableViewCell

@property (strong,nonatomic) UIButton *turnRoundBtn;
@property (strong,nonatomic) UIButton *discussBtn;
@property (strong,nonatomic) UIButton *goodBtn;
@property (strong,nonatomic) UILabel *turnLB;
@property (strong,nonatomic) UILabel *discussLB;
@property (strong,nonatomic) UILabel *goodLB;
@property (strong,nonatomic) ZSTSNSAMessage *messageDic;
@property (strong,nonatomic) UIImageView *goodIV;
@property (strong,nonatomic) UIImageView *turnIV;
@property (strong,nonatomic) UIImageView *discussIV;
@property (strong,nonatomic) UILabel *turnTitleLB;
@property (strong,nonatomic) UILabel *discussTitleLB;
@property (strong,nonatomic) UILabel *goodTitleLB;

@property (weak,nonatomic) id<ZSTMyChatroomfooterCellDelegate> ZSTMyChatroomfooterCellDelegate;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessage *)messageDic;

@end
