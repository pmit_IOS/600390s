//
//  ZSTChatroomNoDataCell.h
//  SNSA
//
//  Created by pmit on 15/10/13.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomNoDataCell : UITableViewCell

@property (strong,nonatomic) UILabel *noDataLB;

- (void)createUI;

- (void)setCellLBNameString:(NSString *)tipString;

@end
