//
//  ZSTMyNewsMessageCell.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTMyNewsMessageCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTMyNewsMessageCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.headerIV)
    {
        CALayer *line = [CALayer layer];
        line.frame = CGRectMake(70, 79, WIDTH - 70, 1);
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [self.contentView.layer addSublayer:line];
        
        self.headerIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 20, 40, 40)];
        self.headerIV.layer.masksToBounds = YES;
        self.headerIV.layer.cornerRadius = 20;
        self.headerIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
        [self.contentView addSubview:self.headerIV];
        
        self.contentIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 15 - 50, 15, 50, 50)];
        self.contentIV.contentMode = UIViewContentModeScaleAspectFit;
        self.contentIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
        [self.contentView addSubview:self.contentIV];
        
        self.topicContentLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 15 - 50, 15, 50, 50)];
        self.topicContentLB.backgroundColor = [UIColor whiteColor];
        self.topicContentLB.textAlignment = NSTextAlignmentLeft;
        self.topicContentLB.font = [UIFont systemFontOfSize:12.0f];
        self.topicContentLB.textColor = RGBA(56, 56, 56, 1);
        self.topicContentLB.numberOfLines = 3;
        self.topicContentLB.hidden = YES;
        [self.contentView addSubview:self.topicContentLB];
        
        self.nameLB = [[UILabel alloc] initWithFrame:CGRectMake(70, 15, WIDTH - 70 - 50 - 15, 12.5)];
        self.nameLB.textAlignment = NSTextAlignmentLeft;
        self.nameLB.font = [UIFont systemFontOfSize:12.0f];
        self.nameLB.textColor = RGBACOLOR(248, 145, 70, 1);
        self.nameLB.text = @"小明真的碉堡了";
        [self.contentView addSubview:self.nameLB];
        
        self.timeLB = [[UILabel alloc] initWithFrame:CGRectMake(70, 30.5, WIDTH - 70 - 50 - 15, 12.5)];
        self.timeLB.text = @"今天14:00";
        self.timeLB.textAlignment = NSTextAlignmentLeft;
        self.timeLB.font = [UIFont systemFontOfSize:10.0f];
        self.timeLB.textColor = RGBACOLOR(159, 159, 159, 1);
        [self.contentView addSubview:self.timeLB];

        self.contentLB = [[UILabel alloc] initWithFrame:CGRectMake(70, CGRectGetMaxY(self.timeLB.frame) + 2, WIDTH - 70 - 50 - 15, 20)];
//        self.contentLB = [[TQRichTextView alloc] initWithFrame:CGRectMake(70, CGRectGetMaxY(self.timeLB.frame) + 5, WIDTH - 70 - 50 - 15, 20)];
        self.contentLB.backgroundColor = [UIColor clearColor];
        self.contentLB.textColor = RGBA(56, 56, 56, 1);
        self.contentLB.font = [UIFont systemFontOfSize:12.0];
        self.contentLB.numberOfLines = 1;
        [self.contentView addSubview:self.contentLB];
        
        
    }
}

- (void)setCellData:(NSDictionary *)aboutMeMessageDic
{
    NSString *menName = [aboutMeMessageDic safeObjectForKey:@"uname"];
    NSMutableString *commentsString = [NSMutableString string];
    NSString *newsMessageContent = @"";
    if ([[aboutMeMessageDic safeObjectForKey:@"type"] integerValue] == 0)
    {
        newsMessageContent = [aboutMeMessageDic safeObjectForKey:@"comments"];
    }
    else
    {
        newsMessageContent = @"点了个赞!";
    }
    
    [commentsString appendString:[NSString stringWithFormat:@"<font size=\"3\" color=#9f9f9f style='width:100%%;display:block; text-overflow: ellipsis;white-space: nowrap;overflow: hidden;'>"]];
    
    NSString *leftM = @"[";
    
    NSString *rightM = @"]";
    
    BOOL isInFace = NO;
    
    NSInteger startIndex = 0;
    
    NSInteger endIndex = 0;
    
    for (NSInteger i = 0; i < newsMessageContent.length; i++)
    {
        NSString *oneString = [newsMessageContent substringWithRange:NSMakeRange(i, 1)];
        if ([oneString isEqualToString:leftM])
        {
            if (i + 1 < newsMessageContent.length)
            {
                NSString *fString = [newsMessageContent substringWithRange:NSMakeRange(i + 1, 1)];
                if ([fString isEqualToString:@"f"])
                {
                    isInFace = YES;
                    startIndex = i;
                }
                else
                {
                    isInFace = NO;
                    [commentsString appendString:oneString];
                }
            }
            else
            {
                [commentsString appendString:oneString];
            }
        }
        else if ([oneString isEqualToString:rightM])
        {
            if (isInFace)
            {
                endIndex = i;
                isInFace = NO;
                NSString *faceString = [newsMessageContent substringWithRange:NSMakeRange(startIndex + 1, 7)];
                NSString *urlPathString = [[NSBundle mainBundle] pathForResource:faceString ofType:@"png"];
                NSURL *imgUrl = [NSURL fileURLWithPath:urlPathString];
                [commentsString appendString:[NSString stringWithFormat:@"<img src=\"%@\" width=\"1.5%%\" />",imgUrl]];
                
            }
            else
            {
                [commentsString appendString:oneString];
            }
        }
        else
        {
            if (isInFace)
            {
                continue;
            }
            else
            {
                [commentsString appendString:oneString];
            }
            
        }
        
        if (i == commentsString.length - 1)
        {
            [commentsString appendString:@"</font>"];
        }
    }
    
    NSString * htmlString = [NSString stringWithFormat:@"<html><body>%@</body></html>",commentsString];
    NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType} documentAttributes:nil error:nil];
    
    self.nameLB.text = menName;
    self.contentLB.attributedText = attrStr;
    
    if ([[aboutMeMessageDic safeObjectForKey:@"mimgurl0"] isKindOfClass:[NSString class]] && ![[aboutMeMessageDic safeObjectForKey:@"mimgurl0"] isEqualToString:@""])
    {
        self.contentIV.hidden = NO;
        self.topicContentLB.hidden = YES;
        [self.contentIV setImageWithURL:[NSURL URLWithString:[aboutMeMessageDic safeObjectForKey:@"mimgurl0"]]];
    }
    else
    {
        self.contentIV.hidden = YES;
        self.topicContentLB.hidden = NO;
        self.topicContentLB.text = [aboutMeMessageDic safeObjectForKey:@"mcontent"];
    }
    
    [self.headerIV setImageWithURL:[NSURL URLWithString:[aboutMeMessageDic safeObjectForKey:@"headphoto"]] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    NSInteger timeCount = [[aboutMeMessageDic safeObjectForKey:@"addtime"] integerValue];
    NSString *showTimeString = [self makeTimeCount:timeCount];
    self.timeLB.text = showTimeString;
    
}

- (NSString *)makeTimeCount:(NSInteger)timeCount
{
    NSString *timeType = @"";
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSDate *now = [NSDate date];
    
    NSTimeInterval delta = [now timeIntervalSince1970] - timeCount;
    
    if (delta <= 60)
    {
        timeType = @"刚刚";
    }
    else if (delta <= 3600 && delta > 60)
    {
        timeType = [NSString stringWithFormat:@"%.0f分钟前",delta / 60];
    }
    else if (delta <= 24 * 60 * 60 && delta > 3600)
    {
        timeType = [NSString stringWithFormat:@"%.0f小时前",delta / 3600];
    }else
    {
        fmt.dateFormat = @"MM-dd HH:mm";
        NSTimeInterval time = timeCount;//因为时差问题要加8小时 == 28800 sec
        NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
        timeType = [fmt stringFromDate: detaildate];
    }
    
    return timeType;
}

@end
