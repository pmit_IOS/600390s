//
//  ZSTSNSAMessageComments.m
//  SNSA
//
//  Created by pmit on 15/9/19.
//
//

#import "ZSTSNSAMessageComments.h"

@implementation ZSTSNSAMessageComments

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        self.addTime = [[aDecoder decodeObjectForKey:@"addtime"] integerValue];
        self.commentContent = [aDecoder decodeObjectForKey:@"commentContent"];
        self.msisdn = [aDecoder decodeObjectForKey:@"msisdn"];
        self.uid = [aDecoder decodeObjectForKey:@"uid"];
        self.uName = [aDecoder decodeObjectForKey:@"uname"];
        self.toUserId = [aDecoder decodeObjectForKey:@"touserid"];
        self.mid = [[aDecoder decodeObjectForKey:@"mid"] integerValue];
        self.commentHeaderPhoto = [aDecoder decodeObjectForKey:@"headphoto"];
        self.parentId = [[aDecoder decodeObjectForKey:@"parentid"] integerValue];
        self.toUsername = [aDecoder decodeObjectForKey:@"tousername"];
        self.toUserPhoto = [aDecoder decodeObjectForKey:@"touserphoto"];
        self.toMsisdn = [aDecoder decodeObjectForKey:@"tomsisdn"];
        self.mcid = [[aDecoder decodeObjectForKey:@"mcid"] integerValue];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"addtime"] forKey:@"addTime"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"comments"] forKey:@"commentContent"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"msisdn"] forKey:@"msisdn"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"uid"] forKey:@"uid"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"uname"] forKey:@"uname"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"touserid"] forKey:@"touserid"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"mid"] forKey:@"mid"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"headphoto"] forKey:@"headphoto"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"parentid"] forKey:@"parentid"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"tousername"] forKey:@"tousername"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"touserphoto"] forKey:@"touserphoto"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"tomsisdn"] forKey:@"tomsisdn"];
    [aCoder encodeObject:[self.commentDic safeObjectForKey:@"mcid"] forKey:@"mcid"];
}

- (id)initWithCommentDic:(NSDictionary *)commentDic
{
    self = [super init];
    if (self)
    {
        self.addTime = [[commentDic safeObjectForKey:@"addtime"] integerValue];
        self.commentContent = [commentDic safeObjectForKey:@"comments"];
        self.msisdn = [commentDic safeObjectForKey:@"msisdn"];
        self.uid = [commentDic safeObjectForKey:@"uid"];
        self.uName = [commentDic safeObjectForKey:@"uname"];
        self.toUserId = [commentDic safeObjectForKey:@"touserid"];
        self.mid = [[commentDic safeObjectForKey:@"mid"] integerValue];
        self.commentHeaderPhoto = [commentDic safeObjectForKey:@"headphoto"];
        self.parentId = [[commentDic safeObjectForKey:@"parentid"] integerValue];
        self.toUsername = [commentDic safeObjectForKey:@"tousername"];
        self.toUserPhoto = [commentDic safeObjectForKey:@"touserphoto"];
        self.toMsisdn = [commentDic safeObjectForKey:@"tomsisdn"];
        self.mcid = [[commentDic safeObjectForKey:@"mcid"] integerValue];
    }
    
    return self;
}

@end
