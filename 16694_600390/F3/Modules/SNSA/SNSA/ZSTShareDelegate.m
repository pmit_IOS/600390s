//
//  ZSTShareDelegate.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTShareDelegate.h"
#import "ZSTChatroomShareGoodCell.h"

@implementation ZSTShareDelegate

static NSString *const shareCell = @"shareCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1000;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomShareGoodCell *cell = [tableView dequeueReusableCellWithIdentifier:shareCell];
    cell.isGood = NO;
    [cell createUI];
//    [cell setCellData:@"" DateString:@""];
    [cell setCellData:@"1231" DateString:@"12312" HeadeString:@""];
    return cell;
}

@end
