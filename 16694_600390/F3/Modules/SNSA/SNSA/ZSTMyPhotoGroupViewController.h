//
//  ZSTMyPhotoGroupViewController.h
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import <UIKit/UIKit.h>

@protocol ZSTMyPhotoGroupViewControllerDelegate <NSObject>

- (void)finishPickerImageAndShow:(NSArray *)imgArr;

@end

@interface ZSTMyPhotoGroupViewController : UIViewController

@property (assign,nonatomic) NSInteger maxCount;
@property (weak,nonatomic) id<ZSTMyPhotoGroupViewControllerDelegate> photoGroupDelegate;


@end
