//
//  CTFrameParserConfig.h
//  CoreTextDemo
//
//  Created by pmit on 15/10/19.
//  Copyright © 2015年 com.9588.f3. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CTFrameParserConfig : NSObject

@property (nonatomic, assign) CGFloat width;
@property (nonatomic, assign) CGFloat fontSize;
@property (nonatomic, assign) CGFloat lineSpace;
@property (nonatomic, strong) UIColor *textColor;


@end
