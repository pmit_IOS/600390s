//
//  ZSTPhotoGroupCell.h
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import <UIKit/UIKit.h>

@interface ZSTPhotoGroupCell : UITableViewCell

@property (strong,nonatomic) UIImageView *firstImg;
@property (strong,nonatomic) UILabel *groupNameLB;
@property (strong,nonatomic) UILabel *groupCountLB;

- (void)createUI;
- (void)setCellData:(UIImage *)firstImg Name:(NSString *)groupName Count:(NSString *)groupCount;

@end
