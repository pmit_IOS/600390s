//
//  ZSTSNSAMessage.h
//  SNSA
//
//  Created by pmit on 15/9/19.
//
//

#import <Foundation/Foundation.h>
#import "ZSTDao+SNSA.h"

@interface ZSTSNSAMessage : NSObject

@property (copy,nonatomic) NSString *circleId;
@property (assign,nonatomic) NSInteger addTime;
@property (copy,nonatomic) NSString *chatContent;
@property (assign,nonatomic) NSInteger msgId;
@property (copy,nonatomic) NSString *chatMsisdn;
@property (copy,nonatomic) NSString *chatNewsAvatar;
@property (copy,nonatomic) NSString *chatNewsUserName;
@property (copy,nonatomic) NSString *chatUID;
@property (assign,nonatomic) NSInteger commentCount;
@property (copy,nonatomic) NSString *imageArrString;
@property (copy,nonatomic) NSString *chatUserName;
@property (strong,nonatomic) NSArray *cgArr;
@property (strong,nonatomic) NSArray *thumArr;
@property (assign,nonatomic) NSInteger isTop;
@property (assign,nonatomic) NSInteger thumbupCount;
@property (assign,nonatomic) NSInteger shareCount;
@property (assign,nonatomic) NSInteger isThumb;
@property (assign,nonatomic) NSString *parentId;
@property (assign,nonatomic) double oneImgHeight;
@property (assign,nonatomic) double oneImgWidth;
@property (strong,nonatomic) ZSTDao *dao;

- (id)initWithMessageDic:(NSDictionary *)messageDic;
- (id)initWithMessageDicBySqlite:(NSDictionary *)messageDic;


//NSString *createSqlString = [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS %@ (ID INTEGER PRIMARY KEY AUTOINCREMENT, circleID TEXT,addTime INTEGER, chatContent TEXT, chatMID INTEGER, chatMsisdn TEXT, chatNewsAvatar TEXT, chatNewsUName TEXT, chatParentId TEXT, chatUAvatar TEXT, chatUID TEXT, chatUName TEXT, commentCount INTEGER,userPhoto TEXT,shareCount INTEGER,goodCount INTEGER)",tableName];

@end
