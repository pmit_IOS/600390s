//
//  ZSTCircleEmptyView.m
//  YouYun
//
//  Created by luobin on 6/25/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTCircleEmptyTableViewCell.h"

@implementation ZSTCircleEmptyTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        NSString *introduce = NSLocalizedString(@"您还没有加入任何圈子", nil);
        UILabel *introduceLabel = [[[UILabel alloc] initWithFrame:CGRectMake(40, 140, 240, 60)] autorelease];
        introduceLabel.numberOfLines = 1;
        introduceLabel.textAlignment = NSTextAlignmentCenter;
        introduceLabel.textColor = RGBCOLOR(150, 150, 150);
        introduceLabel.backgroundColor = [UIColor clearColor];
        introduceLabel.font = [UIFont systemFontOfSize:18];
        introduceLabel.text = introduce;
        [self.contentView addSubview:introduceLabel];
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    
    CGContextRef cr = UIGraphicsGetCurrentContext();
    [RGBCOLOR(241, 241, 241) set];
    CGContextFillRect(cr, self.bounds);

}

@end
