//
//  ZSTTopicThumbCell.h
//  SNSA
//
//  Created by pmit on 15/10/22.
//
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

@interface ZSTTopicThumbCell : UITableViewCell

@property (strong,nonatomic) UIView *thunbBgView;
@property (strong,nonatomic) TTTAttributedLabel *goodMenLB;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessage *)message;

@end
