//
//  ZSTChatroomMenDetailCell.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatroomMenDetailCell.h"

@implementation ZSTChatroomMenDetailCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.titleLB)
    {
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(15, 49, WIDTH, 1);
        [self.contentView.layer addSublayer:line];
        
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, (WIDTH - 30) / 2, 20)];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textColor = RGBA(56, 56, 56, 1);
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLB];
        
        self.rightTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15 + (WIDTH - 30) / 2, 15, (WIDTH - 30) / 2, 20)];
        self.rightTitleLB.font = [UIFont systemFontOfSize:12.0f];
        self.rightTitleLB.textColor = RGBA(204, 204, 204, 1);
        self.rightTitleLB.textAlignment = NSTextAlignmentRight;
        [self.contentView addSubview:self.rightTitleLB];
    }
}

- (void)setDetailCellData:(NSString *)titleString RightTitle:(NSString *)rightTitleString
{
    self.titleLB.text = titleString;
    self.rightTitleLB.text = rightTitleString;
}

@end
