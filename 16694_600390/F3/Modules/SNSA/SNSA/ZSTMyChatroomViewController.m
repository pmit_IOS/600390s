//
//  ZSTMyChatroomViewController.m
//  SNSA
//
//  Created by pmit on 15/10/14.
//
//

#import "ZSTMyChatroomViewController.h"
#import <ZSTUtils.h>
#import <PMRepairButton.h>
#import "ZSTMyChatroomHeaderCell.h"
#import "ZSTMyChatroomfooterCell.h"
#import "ZSTChatroomTopicCGCell.h"
#import "ZSTDao+SNSA.h"
#import "ZSTYouYunEngine.h"
#import "ZSTChatroomTopicDetailViewController.h"
#import <ZSTPhotoTypeView.h>
#import <ZYQAssetPickerController.h>
#import "ZSTTopicReportViewController.h"
#import "ZSTNewsTopicViewController.h"
#import <EGORefreshTableHeaderView.h>
#import <ZSTNewShareView.h>
#import "ZSTChatroomMenDetailViewController.h"
#import "ZSTChatroomMyNewsMessageViewController.h"
#import <SDWebImage/SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>
#import "ZSTBigPicDelegate.h"
#import "ZSTTopicContentCell.h"
#import "ZSTChatroomLoadMoreView.h"
#import <TTTAttributedLabel.h>
#import "CoreTextData.h"
#import "CTFrameParser.h"
#import "CTFrameParserConfig.h"
#import "NSAttributedString+EmojiExtension.h"
#import "EmojiTextAttachment.h"
#import "ZSTOneTopicCommentCell.h"
#import "ZSTTopicArrowCell.h"
#import "ZSTTopicThumbCell.h"
#import "ZSTMyPhotoGroupViewController.h"

#define kPageSize 10

@interface ZSTMyChatroomViewController () <ZSTPhotoTypeViewDelegate,ZYQAssetPickerControllerDelegate,UINavigationControllerDelegate,UITableViewDataSource,UITableViewDelegate,ZSTMyChatroomHeaderCellDelegate,ZSTYouYunEngineDelegate,ZSTNewsTopicViewControllerDelegate,ZSTMyChatroomfooterCellDelegate,ZSTTopicContentCellDelegate,ZSTChatroomTopicDetailViewControllerDelegate,ZSTTopicReportViewControllerDelegate,UITextViewDelegate,ZSTChatroomTopicCGCellDelegate,UIImagePickerControllerDelegate,ZSTNewShareViewDelegate,EGORefreshTableHeaderDelegate,TTTAttributedLabelDelegate,ZSTF3EngineDelegate,UIActionSheetDelegate,ZSTMyPhotoGroupViewControllerDelegate>

@property (strong,nonatomic) UITableView *myChatTableView;
@property (strong,nonatomic) NSMutableArray *myMessageArr;
@property (strong,nonatomic) ZSTDao *dao;
@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (assign,nonatomic) BOOL isSycn;
@property (assign,nonatomic) NSInteger pushCount;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) ZSTPhotoTypeView *photoView;
@property (strong,nonatomic) UIView *newsMessageShadowView;
@property (strong,nonatomic) UILabel *newsMessageLB;
@property (assign,nonatomic) BOOL isLoadNew; //判断是否是加载新的
@property (assign,nonatomic) BOOL refreshWay; //判断是点击加载还是下拉加载
@property (assign,nonatomic) BOOL isLoadOld; //判断是否是加载更多旧
@property (assign,nonatomic) NSInteger updateCount;
@property (assign,nonatomic) BOOL isFaceOpen; //表情选择框的状态
@property (strong,nonatomic) UIView *inputView;
@property (strong,nonatomic) UITextView *inputTV;
@property (strong,nonatomic) UILabel *backUpLB;
@property (strong,nonatomic) UIView *faceView;
@property (strong,nonatomic) UIScrollView *faceScroll;
@property (strong,nonatomic) UIPageControl *facePageControl;
@property (strong,nonatomic) UIView *emptyView;
@property (strong,nonatomic) UIView *moreFunView;
@property (strong,nonatomic) ZSTNewShareView *shareView;
@property (assign,nonatomic) NSInteger weiboSharePoint;
@property (assign,nonatomic) NSInteger wxSharePoint;
@property (assign,nonatomic) NSInteger wxFriendSharePoint;
@property (assign,nonatomic) NSInteger qqSharePoint;
@property (strong,nonatomic) UIView *exitAlertView;
@property (strong,nonatomic) ZSTSNSAMessage *moreFunMessage;
@property (strong,nonatomic) UIView *darkScrollView;
@property (strong,nonatomic) UIScrollView *darkScroll;
@property (strong,nonatomic) UILabel *showNumLB;
@property (strong,nonatomic) EGORefreshTableHeaderView *refreshHeaderView;
@property (assign,nonatomic) BOOL isToComments;
@property (strong,nonatomic) ZSTSNSAMessage *footerMessage;
@property (strong,nonatomic) ZSTSNSAMessageComments *toComments;
@property (nonatomic, strong) NSMutableArray *photoMenuItems;
@property (strong,nonatomic) UIImagePickerController *iconPickerController;
@property (strong,nonatomic) PMRepairButton *myMessageBtn;
@property (assign,nonatomic) NSInteger aboutMeCount;
@property (strong,nonatomic) ZSTSNSAMessage *bigShowMessage;
@property (strong,nonatomic) UIView *footerView;
@property (assign,nonatomic) BOOL hasLoadMore; //是否有下一页
@property (strong,nonatomic) ALAssetsLibrary *library;
@property (strong,nonatomic) NSMutableArray *groups;
@property (strong,nonatomic) ZSTF3Engine *f3Engine;
@property (strong,nonatomic) CALayer *line;
@property (strong,nonatomic) CALayer *line2;
@property (assign,nonatomic) BOOL isFirstLoad;

@end

@implementation ZSTMyChatroomViewController

static NSString *const chatHeaderCell = @"chatHeaderCell";
static NSString *const chatFooterCell = @"chatFooterCell";
static NSString *const chatContentCell = @"chatContentCell";
static NSString *const chatOneCommentsCell = @"chatOneCommentsCell";
static NSString *const chatArrowCell = @"chatArrowCell";
static NSString *const chatThumbCell = @"chatThumbCell";

//NSString *const CTDisplayViewLinkPressedNotification = @"CTDisplayViewLinkPressedNotification";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyBoardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(commentsToMenDetail:) name:@"CTDisplayViewLinkPressedNotification" object:nil];
    
    self.isFirstLoad = YES;
    self.isFaceOpen = NO;
    self.refreshWay = NO;
    self.hasLoadMore = NO;
    self.dao = [[ZSTDao alloc] init];
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.f3Engine = [[ZSTF3Engine alloc] init];
    self.f3Engine.delegate = self;
    self.groups = [NSMutableArray array];
    [self createSqliteTable];
    [self buildNavigation];
    [self buildMyChatTableView];
    [self buildAboutMeView];
    [self getAboutMeCountByNet];
    [self buildEmtpyView];
    
    if (_refreshHeaderView == nil) {
        
        EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.myChatTableView.bounds.size.height, self.view.frame.size.width, self.myChatTableView.bounds.size.height)];
        view.delegate = self;
        [self.myChatTableView addSubview:view];
        _refreshHeaderView = view;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.pushCount = 0;
    self.navigationController.navigationBarHidden = NO;
    if (self.isFirstLoad)
    {
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            
            self.myMessageArr = [self searchSqlite];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                self.isFirstLoad = NO;
                if (self.myMessageArr.count > 0)
                {
                    self.isSycn = YES;
                    [self.myChatTableView reloadData];
                }
                else
                {
                    self.isSycn = NO;
                    [self getChatDataByNet];
                }
                
                [_refreshHeaderView refreshLastUpdatedDate];
            });
            
        });
    }

}

- (void)viewDidAppear:(BOOL)animated
{
    [self.f3Engine getPointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
    if (!self.inputView)
    {
        [self buildCommentsInputView];
        [self resetTextStyle];
        [self buildFaceView];
        [self buildEmtpyView];
        [self buildShadowView];
        [self buildPhotoPickView];
        [self buildMoreFunView];
        [self buildSureAlertView];
        [self buildDarkScroll];
        
        if (self.isSycn)
        {
            dispatch_async(dispatch_get_global_queue(0, 0), ^{
                NSInteger max = [[[[self.dao selectMaxMsgId:self.circleId] firstObject] safeObjectForKey:@"maxId"] integerValue];
                [self.engine getMyCircleTopic:self.circleId messageID:[NSString stringWithFormat:@"%@",@(max + 1)] Flag:@"all" sortType:@"desc" PageSize:kPageSize];
                [self.engine getNewsMessageCount:self.circleId MID:[NSString stringWithFormat:@"%@",@(max)]];
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    
                });
                

            });
        }
    }
}

- (void)getAboutMeCountByNet
{
    [self.engine getAboutMeCount:self.circleId];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildNavigation
{
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(self.chatroomTitle, nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    PMRepairButton *newsTopicBtn = [[PMRepairButton alloc] init];
    newsTopicBtn.frame = CGRectMake(0, 0, 40, 20);
    [newsTopicBtn setImage:[UIImage imageNamed:@"chatroom_newstopic.png"] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:newsTopicBtn];
    
    UIImageView *newsTopicIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    newsTopicIV.image = [UIImage imageNamed:@"chatroom_newstopic.png"];
    newsTopicIV.userInteractionEnabled = YES;
    UITapGestureRecognizer *oneTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(takePhotos:)];
    [newsTopicBtn addGestureRecognizer:oneTap];
    
    UILongPressGestureRecognizer *longPresses = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(sendTextTopic:)];
    longPresses.minimumPressDuration = 0.5f;
    [newsTopicBtn addGestureRecognizer:longPresses];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:newsTopicBtn];
    
    self.view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
}

#pragma mark - 拍照上传
- (void)takePhotos:(UITapGestureRecognizer *)sender
{
    UIActionSheet *acSheet = [[UIActionSheet alloc] initWithTitle:@"请选择" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍照" otherButtonTitles:@"从相册中选择", nil];
    [acSheet showInView:self.view];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    self.iconPickerController = [[UIImagePickerController alloc] init];
    [_iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    switch (buttonIndex) {
        case 0:
            
            _iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            _iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
            _iconPickerController.showsCameraControls = YES;
            _iconPickerController.allowsEditing = YES;
            _iconPickerController.delegate = self;
            [self presentViewController:_iconPickerController animated:YES completion:nil];
            
            break;
        case 1:
        {
//            ZYQAssetPickerController *picker = [[ZYQAssetPickerController alloc]init];
//            picker.maximumNumberOfSelection = 9;
//            picker.assetsFilter = [ALAssetsFilter allPhotos];
//            picker.showEmptyGroups = NO;
//            picker.delegate = self;
//            picker.selectionFilter = [NSPredicate predicateWithBlock:^BOOL(id evaluatedObject,NSDictionary *bindings){
//                if ([[(ALAsset *)evaluatedObject valueForProperty:ALAssetPropertyType]isEqual:ALAssetTypeVideo]) {
//                    NSTimeInterval duration = [[(ALAsset *)evaluatedObject valueForProperty:ALAssetPropertyDuration]doubleValue];
//                    return duration >= 5;
//                }else{
//                    return  YES;
//                }
//            }];
// 
//            [self presentViewController:picker animated:YES completion:nil];
            ZSTMyPhotoGroupViewController *photoVC = [[ZSTMyPhotoGroupViewController alloc] init];
            photoVC.photoGroupDelegate = self;
            photoVC.maxCount = 9 - self.photoMenuItems.count;
            UINavigationController *picker = [[UINavigationController alloc] initWithRootViewController:photoVC];
            [self presentViewController:picker animated:YES completion:nil];
        }
            break;
    }
}

#pragma mark - 跳转
- (void)sendTextTopic:(UILongPressGestureRecognizer *)sender
{
    if (self.pushCount == 0)
    {
        ZSTNewsTopicViewController *topicVC = [[ZSTNewsTopicViewController alloc] init];
        topicVC.circleId = self.circleId;
        topicVC.newsTopicDelegate = self;
        [self.navigationController pushViewController:topicVC animated:YES];
        self.pushCount += 1;
    }
    
}

#pragma mark - 请求话题数据
- (void)getChatDataByNet
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在为您加载话题，请稍后...", nil) withImage:nil];
    [self.engine getMyCircleTopic:self.circleId messageID:@"0" Flag:@"all" sortType:SortType_Desc PageSize:kPageSize];
}

#pragma mark - 照片选择遮罩
- (void)buildNewsMessageShadowView
{
    self.newsMessageShadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    self.newsMessageShadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.newsMessageShadowView];
    self.newsMessageShadowView.hidden = YES;
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.5;
    [self.newsMessageShadowView addSubview:alphaView];
    
    self.newsMessageLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30 - 20, 40)];
    self.newsMessageLB.font = [UIFont systemFontOfSize:12.0f];
    self.newsMessageLB.textColor = [UIColor whiteColor];
    self.newsMessageLB.textAlignment = NSTextAlignmentCenter;
    [self.newsMessageShadowView addSubview:self.newsMessageLB];
    
    UITapGestureRecognizer *loadNewRefreshTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadNewsTopic:)];
    [alphaView addGestureRecognizer:loadNewRefreshTap];
    
    PMRepairButton *cancelNewsBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(WIDTH - 15 - 20, 10, 20, 20)];
    [cancelNewsBtn setImage:[UIImage imageNamed:@"chatroom_cancelUpdate.png"] forState:UIControlStateNormal];
    [cancelNewsBtn addTarget:self action:@selector(cancelUpdateMessage:) forControlEvents:UIControlEventTouchUpInside];
    [self.newsMessageShadowView addSubview:cancelNewsBtn];
    
}


- (void)buildMyChatTableView
{
    self.myChatTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.myChatTableView.delegate = self;
    self.myChatTableView.dataSource = self;
    self.myChatTableView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    [self.myChatTableView registerClass:[ZSTMyChatroomHeaderCell class] forCellReuseIdentifier:chatHeaderCell];
    [self.myChatTableView registerClass:[ZSTMyChatroomfooterCell class] forCellReuseIdentifier:chatFooterCell];
    [self.myChatTableView registerClass:[ZSTTopicContentCell class] forCellReuseIdentifier:chatContentCell];
    [self.myChatTableView registerClass:[ZSTOneTopicCommentCell class] forCellReuseIdentifier:chatOneCommentsCell];
    [self.myChatTableView registerClass:[ZSTTopicArrowCell class] forCellReuseIdentifier:chatArrowCell];
    [self.myChatTableView registerClass:[ZSTTopicThumbCell class] forCellReuseIdentifier:chatThumbCell];
    self.myChatTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.myChatTableView setSeparatorColor:[UIColor whiteColor]];
    [self.view addSubview:self.myChatTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    self.myChatTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.myMessageArr.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    ZSTSNSAMessage *message = self.myMessageArr[section];
    return 5 + message.cgArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSNSAMessage *messageDic = self.myMessageArr[indexPath.section];
    if (indexPath.row == 0)
    {
        ZSTMyChatroomHeaderCell *cell = [tableView dequeueReusableCellWithIdentifier:chatHeaderCell];
        cell.isDetailCell = NO;
        [cell createUI];
        cell.headerCellDelegate = self;
        messageDic.circleId = self.circleId;
        [cell setCellData:messageDic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.row == 1)
    {
        ZSTTopicContentCell *cell = [tableView dequeueReusableCellWithIdentifier:chatContentCell];
        cell.isFromTopicDetail = NO;
        cell.cellSection = indexPath.section;
        [cell createUI];
        cell.contentCellDelegate = self;
        messageDic.circleId = self.circleId;
        [cell setCellData:messageDic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
        
    }
    else if (indexPath.row == 4 + messageDic.cgArr.count)
    {
        ZSTMyChatroomfooterCell *cell = [tableView dequeueReusableCellWithIdentifier:chatFooterCell];
        [cell createUI];
        cell.ZSTMyChatroomfooterCellDelegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        messageDic.circleId = self.circleId;
        [cell setCellData:messageDic];
        
        return cell;
    }
    else if (indexPath.row == 2)
    {
        ZSTTopicArrowCell *cell = [tableView dequeueReusableCellWithIdentifier:chatArrowCell];
        [cell createUI];
        [cell setCellData:messageDic];
        return cell;
    }
    else if (indexPath.row == 3)
    {
        ZSTTopicThumbCell *cell = [tableView dequeueReusableCellWithIdentifier:chatThumbCell];
        cell.goodMenLB.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell createUI];
        [cell setCellData:messageDic];
        return cell;
    }
    else
    {
        ZSTOneTopicCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:chatOneCommentsCell];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell createUI];
        ZSTSNSAMessageComments *oneComments = messageDic.cgArr[indexPath.row - 4];
        [cell setCellData:oneComments];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] init];
    view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSNSAMessage *messagedic = self.myMessageArr[indexPath.section];
    if (indexPath.row == 0)
    {
        return 80;
    }
    else if (indexPath.row == 1)
    {
        NSString *messageString = messagedic.chatContent;
        CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
        config.width = WIDTH - 30;
        config.textColor = [UIColor blackColor];
        
        CTFrameParser *parser = [[CTFrameParser alloc] init];
        CoreTextData *data = [parser parseTemplateTopicContent:messageString config:config];
        
        NSString *imageUrl = messagedic.imageArrString;
        NSArray *imgArr = [imageUrl componentsSeparatedByString:@","];
        if (!imgArr || imgArr.count == 0 || [[imgArr firstObject] isEqualToString:@""])
        {
            return data.height + 15;
        }
        else if (imgArr.count == 1)
        {
            CGFloat dealWidth = 0;
            CGFloat dealHeight = 0;
            
            CGFloat imgWidth = messagedic.oneImgWidth;
            CGFloat imgHeight = messagedic.oneImgHeight;
            CGFloat maxWidth = (WIDTH - 30) / 2;
            CGFloat maxHeight = maxWidth * 1.5;
            if (imgWidth > maxWidth)
            {
                CGFloat imgNowHeight = imgHeight * (maxWidth / imgWidth);
                dealWidth = maxWidth;
                if (imgNowHeight > maxHeight)
                {
                    dealHeight = maxHeight;
                }
                else
                {
                    dealHeight = imgNowHeight;
                }
            }
            else
            {
                dealWidth = imgWidth;
                if (imgHeight > maxHeight)
                {
                    dealHeight = maxHeight;
                }
                else
                {
                    dealHeight = imgHeight;
                }
            }
            
            return dealHeight + data.height + 15;
        }
        else
        {
            NSInteger countNum = imgArr.count / 3;
            NSInteger lessNum = imgArr.count % 3;
            NSInteger height = 0;
            
            if (lessNum == 0)
            {
                height = countNum;
            }
            else
            {
                height = countNum + 1;
            }
            
            if (height > 0)
            {
                if ([messageString isEqualToString:@""])
                {
                    return height * (WIDTH - 40) / 3 + countNum * 5 + 5;
                }
                else
                {
                    return data.height + height * (WIDTH - 40) / 3 + 5 + (height - 1) * 5;
                }
                
            }
            else
            {
                return data.height + 5;
            }
        }
    }
    else if (indexPath.row == 2)
    {
        if (messagedic.cgArr.count != 0 || messagedic.thumArr.count != 0)
        {
            return 5;
        }
        else
        {
            return 0;
        }
    }
    else if (indexPath.row == 4 + messagedic.cgArr.count)
    {
        return 40;
    }
    else if (indexPath.row == 3)
    {
        if (messagedic.thumArr.count == 0)
        {
            return 0;
        }
        else
        {
            NSMutableArray *goodNameArr = [NSMutableArray array];
            for (NSInteger i = 0; i < messagedic.thumArr.count; i++)
            {
                ZSTSNSAThumShare *thumbShare = messagedic.thumArr[i];
                NSString *oneName = [thumbShare.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : thumbShare.uname;
                [goodNameArr addObject:oneName];
            }
            
            NSString *showMenNameString = [goodNameArr componentsJoinedByString:@","];
            CGSize menNameSize = [showMenNameString boundingRectWithSize:CGSizeMake(WIDTH - 30 - 7.5, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0f]} context:nil].size;
            if (messagedic.commentCount == 0)
            {
                return menNameSize.height + 10;
            }
            else
            {
                 return menNameSize.height + 3;
            }
        }
    }
    else
    {
        ZSTSNSAMessageComments *comments = messagedic.cgArr[indexPath.row - 4];
        CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
        
        config.width = WIDTH - 30;
        config.textColor = [UIColor blackColor];
        NSString *contentString = @"";
        if (comments.parentId != 0)
        {
            contentString = [NSString stringWithFormat:@"%@回复%@:%@",comments.uName,comments.toUsername,comments.commentContent];
        }
        else
        {
            contentString = [NSString stringWithFormat:@"%@:%@",comments.uName,comments.commentContent];
        }
        
        CTFrameParser *parser = [[CTFrameParser alloc] init];
        CoreTextData *data = [parser parseTemplateFileContent:contentString config:config UserMsisdn:comments.msisdn ToUserMsisdn:comments.msisdn];
        if (indexPath.row - 3 == messagedic.cgArr.count)
        {
            return data.height + 5;
        }
        else
        {
            return data.height;
        }
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSNSAMessage *messageDic = self.myMessageArr[indexPath.section];
    if (indexPath.row == 1)
    {
        messageDic.circleId = self.circleId;
        ZSTChatroomTopicDetailViewController *topicDetailVC = [[ZSTChatroomTopicDetailViewController alloc] init];
        topicDetailVC.detailDelegate = self;
        topicDetailVC.circleId = self.circleId;
        topicDetailVC.messageDic = messageDic;
        [self.navigationController pushViewController:topicDetailVC animated:YES];
    }
    else if (indexPath.row == 0 || indexPath.row == 2 || indexPath.row == 4 + messageDic.cgArr.count || indexPath.row == 3)
    {
        
    }
    else
    {
        ZSTSNSAMessageComments *comments = messageDic.cgArr[indexPath.row - 4];
        [self sendCommentToComments:messageDic Comments:comments];
    }
    
}

#pragma mark - 查询缓存中的数据
- (NSMutableArray *)searchSqlite
{
    NSMutableArray *TNArr = [NSMutableArray array];
    NSArray *topArr = [self.dao selectTopMessageDic:self.circleId IsTop:YES];
    if (topArr && topArr.count != 0)
    {
        [TNArr addObjectsFromArray:topArr];
    }
    
    NSArray *normalArr = [self.dao selectTopMessageDic:self.circleId IsTop:NO];
    if (normalArr && normalArr.count != 0)
    {
        [TNArr addObjectsFromArray:normalArr];
    }
    
    NSMutableArray *showArr = [NSMutableArray array];
    for (NSDictionary *sqlDic in TNArr)
    {
        ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDicBySqlite:sqlDic];
        [showArr addObject:message];
    }
    
    return showArr;
}

#pragma mark - 创建缓存表
- (void)createSqliteTable
{
    [self.dao createChatHistoryTable:self.circleId];
}

#pragma mark - 请求话题响应
- (void)getMyMessageSuccess:(NSDictionary *)messageArrDic
{
    [TKUIUtil hiddenHUD];
    NSArray *messageArr = [[messageArrDic safeObjectForKey:@"data"] safeObjectForKey:@"mgsdetail"];
    BOOL hasMore = [[[messageArrDic safeObjectForKey:@"data"] safeObjectForKey:@"hasmore"] boolValue];
    self.hasLoadMore = hasMore;
    
    if (self.isLoadNew)
    {
        if (messageArr.count == 0)
        {
            if (self.refreshWay)
            {
                [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"没有更新的话题啦！", nil) withImage:nil];
            }
            
        }
        else
        {
            NSMutableIndexSet *indexes = [NSMutableIndexSet indexSet];
            for(int i = 0;i < messageArr.count;i++)
            {
                [indexes addIndex:i];
            }
            
            NSMutableArray *tempArr = [NSMutableArray array];
            for (NSDictionary *dic in messageArr)
            {
                ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDic:dic];
                message.circleId = self.circleId;
                
                [self.dao insertCircleMessage:message CircleId:self.circleId MessageArrString:@""];
                [tempArr addObject:message];
            }
            
            [self.myMessageArr insertObjects:tempArr atIndexes:indexes];
            self.isLoadNew = NO;
        }
    }
    else if (self.isLoadOld)
    {
        if (messageArr.count == 0)
        {
            self.isLoadOld = NO;
            [self loadMoreCompleted];
        }
        else
        {
            for (NSDictionary *dic in messageArr)
            {
                ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDic:dic];
                message.circleId = self.circleId;
                
                [self.dao insertCircleMessage:message CircleId:self.circleId MessageArrString:@""];
                [self.myMessageArr addObject:message];
            }
            
            self.isLoadOld = NO;
            [self loadMoreCompleted];
            [self.myChatTableView reloadData];
            
        }
    }
    else
    {
        if (self.isSycn)
        {
            NSInteger max = [[[[self.dao selectMaxMsgId:self.circleId] firstObject] safeObjectForKey:@"maxId"] integerValue];
            [self.dao removeTopicByMID:self.circleId MaxMid:(max + 1)];
            
            for (NSDictionary *dic in messageArr)
            {
                ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDic:dic];
                message.circleId = self.circleId;
                [self.myMessageArr addObject:message];
            }
            
            [self addSqlite:messageArr];
            
            self.myMessageArr = [self searchSqlite];
            if (!self.hasLoadMore)
            {
                self.myChatTableView.tableFooterView = nil;
            }
            else
            {
                if (!self.footerView)
                {
                    ZSTChatroomLoadMoreView *footerView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                    self.footerView = footerView;
                    self.myChatTableView.tableFooterView = self.footerView;
                }
                else
                {
                    self.myChatTableView.tableFooterView = self.footerView;
                }
                
            }
            [self.myChatTableView reloadData];
        }
        else
        {
            if (messageArr.count == 0)
            {
                if (self.hasLoadMore)
                {
                    if (!self.footerView)
                    {
                        ZSTChatroomLoadMoreView *footerView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                        self.footerView = footerView;
                        self.myChatTableView.tableFooterView = self.footerView;
                    }
                    else
                    {
                        self.myChatTableView.tableFooterView = self.footerView;
                    }
                }
                else
                {
                    self.myChatTableView.tableFooterView = nil;
                }
                
                self.emptyView.hidden = NO;
            }
            else
            {
                for (NSDictionary *dic in messageArr)
                {
                    ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDic:dic];
                    message.circleId = self.circleId;
                    [self.myMessageArr addObject:message];
                }
                
                [self addSqlite:messageArr];
                
                self.myMessageArr = [self searchSqlite];
                [self.myChatTableView reloadData];
                if (self.hasLoadMore)
                {
                    if (!self.footerView)
                    {
                        ZSTChatroomLoadMoreView *footerView = [[ZSTChatroomLoadMoreView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
                        self.footerView = footerView;
                        self.myChatTableView.tableFooterView = self.footerView;
                    }
                    
                }
                else
                {
                    self.myChatTableView.tableFooterView = nil;
                }
            }
            
        }
    }
}

- (void)addSqlite:(NSArray *)messageDicArr
{
    for (NSDictionary *messageDic in messageDicArr)
    {
        ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDic:messageDic];
        [self.dao insertCircleMessage:message CircleId:self.circleId MessageArrString:@""];
    }
}

- (void)getMyMessageFailure:(NSString *)resultString
{
    [TKUIUtil hiddenHUD];
}

#pragma mark - 获取新话题
- (void)loadNewsTopic:(UITapGestureRecognizer *)tap
{
    self.isLoadNew = YES;
    self.refreshWay = NO;
    NSInteger max = [[[[self.dao selectMaxMsgId:self.circleId] firstObject] safeObjectForKey:@"maxId"] integerValue];
    [self.engine getMyCircleTopic:self.circleId messageID:[NSString stringWithFormat:@"%@",@(max)] Flag:@"new" sortType:SortType_Desc PageSize:self.updateCount == 0 ? kPageSize : self.updateCount];
}

#pragma mark - 关闭提示
- (void)cancelUpdateMessage:(PMRepairButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.newsMessageShadowView.frame = CGRectMake(0, -self.newsMessageShadowView.bounds.size.height, self.newsMessageShadowView.bounds.size.width, self.newsMessageShadowView.bounds.size.height);
        self.myChatTableView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
        
    }];
}

#pragma mark - 评论输入框
- (void)buildCommentsInputView
{
    self.inputView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
    self.inputView.backgroundColor = RGBA(238, 238, 238, 1);
    [self.view addSubview:self.inputView];
    
    PMRepairButton *faceBtn = [[PMRepairButton alloc] init];
    faceBtn.frame = CGRectMake(10, 10, 30, 30);
    [faceBtn setImage:[UIImage imageNamed:@"chatroom_face.png"] forState:UIControlStateNormal];
    [faceBtn addTarget:self action:@selector(showFaceSelectView:) forControlEvents:UIControlEventTouchUpInside];
    [self.inputView addSubview:faceBtn];
    
    UIButton *sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sendBtn.frame = CGRectMake(WIDTH - 10 - 50, 10, 50, 30);
    sendBtn.backgroundColor = [UIColor whiteColor];
    sendBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [sendBtn setTitleColor:RGBA(159, 159, 159, 1) forState:UIControlStateNormal];
    sendBtn.layer.borderColor = RGBA(159, 159, 159, 1).CGColor;
    sendBtn.layer.borderWidth = 0.5;
    sendBtn.layer.cornerRadius = 2.0f;
    [sendBtn addTarget:self action:@selector(sendComment:) forControlEvents:UIControlEventTouchUpInside];
    [sendBtn setTitle:@"发送" forState:UIControlStateNormal];
    [self.inputView addSubview:sendBtn];
    
    
    self.inputTV = [[UITextView alloc] initWithFrame:CGRectMake(45, 10, WIDTH - 50 - 50 - 10, 30)];
    self.inputTV.layer.borderColor = RGBA(159, 159, 159, 1).CGColor;
    self.inputTV.layer.borderWidth = 0.5;
    self.inputTV.layer.cornerRadius = 2.0f;
    self.inputTV.backgroundColor = [UIColor whiteColor];
    self.inputTV.delegate = self;
    [self.inputView addSubview:self.inputTV];
    
    self.backUpLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, self.inputTV.bounds.size.width - 20, 20)];
    self.backUpLB.font = [UIFont systemFontOfSize:12.0f];
    self.backUpLB.textColor = RGBACOLOR(204, 204, 204, 1);
    [self.inputTV addSubview:self.backUpLB];
    
    self.backUpLB.hidden = YES;
    self.inputView.hidden = YES;
}

#pragma mark -显示表情选择器
- (void)showFaceSelectView:(PMRepairButton *)sender
{
    [self.inputTV resignFirstResponder];
    
    if (self.isFaceOpen)
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = NO;
        }];
        
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT - 64 - self.faceView.bounds.size.height, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50 - self.faceView.bounds.size.height, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = YES;
            
        }];
        
    }
}

#pragma mark - 发送评论
- (void)sendComment:(UIButton *)sender
{
    NSString * newsTopicString = [NSString stringWithFormat:@"%@", [self.inputTV.textStorage getPlainString]];
    if ([newsTopicString isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"不能发表空评论哦", nil) withImage:nil];
    }
    else
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = NO;
        }];
        
        if (self.isToComments)
        {
            [self.engine sendComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.footerMessage.msgId)] Comments:newsTopicString ParentId:[NSString stringWithFormat:@"%@",@(self.toComments.mcid)] TouristId:self.toComments.uid];
        }
        else
        {
            [self.engine sendComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.footerMessage.msgId)] Comments:newsTopicString ParentId:@"0" TouristId:self.footerMessage.chatUID];
        }
    }
}

#pragma mark - 表情选择器
- (void)buildFaceView
{
    self.faceView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64, WIDTH, 165)];
    self.faceView.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    self.faceScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 165)];
    self.faceScroll.delegate = self;
    self.faceScroll.showsHorizontalScrollIndicator = NO;
    self.faceScroll.showsVerticalScrollIndicator = NO;
    self.faceScroll.pagingEnabled = YES;
    self.faceScroll.delegate = self;
    [self.faceView addSubview:self.faceScroll];
    self.faceScroll.contentSize = CGSizeMake(WIDTH * 3, 0);
    
    for (NSInteger i = 0; i < 72; i++)
    {
        NSInteger page = i / 24;
        
        NSInteger pageRow = i;
        if (i >= 24)
        {
            pageRow = i - page * 24;
        }
        
        NSInteger row = pageRow / 6;
        NSInteger column = i % 6;
        
        
        
        PMRepairButton *faceOneBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(page * WIDTH + column * WIDTH / 6, row * 30 + (row + 1) * 5, WIDTH / 6, 30)];
        NSString *faceName = @"";
        if (i < 9)
        {
            faceName = [NSString stringWithFormat:@"face00%@",@(i + 1)];
        }
        else if (i < 99)
        {
            faceName = [NSString stringWithFormat:@"face0%@",@(i + 1)];
        }
        faceOneBtn.tag = i + 1001;
        [faceOneBtn addTarget:self action:@selector(selectedFace:) forControlEvents:UIControlEventTouchUpInside];
        [faceOneBtn setImage:[UIImage imageNamed:faceName] forState:UIControlStateNormal];
        [self.faceScroll addSubview:faceOneBtn];
    }
    
    self.facePageControl = [[UIPageControl alloc] init];
    self.facePageControl.center = CGPointMake(WIDTH / 2, 155);
    self.facePageControl.bounds = (CGRect){CGPointZero,{WIDTH,10}};
    self.facePageControl.numberOfPages = 3;
    [self.faceView addSubview:self.facePageControl];
    
    [self.view addSubview:self.faceView];
}

#pragma mark - 选择表情
- (void)selectedFace:(PMRepairButton *)sender
{
    NSInteger index = sender.tag - 1001;
    //    NSString *selectText = [[CommonFunc sharedCommon].faceTilteArr objectAtIndex:index];
    NSMutableArray *emojiArr = [NSMutableArray array];
    for (NSInteger i = 0; i < 72; i++)
    {
        NSString *faceName = @"";
        if (i < 9)
        {
            faceName = [NSString stringWithFormat:@"face00%@",@(i + 1)];
        }
        else if (i < 99)
        {
            faceName = [NSString stringWithFormat:@"face0%@",@(i + 1)];
        }
        
        [emojiArr addObject:faceName];
    }
    EmojiTextAttachment *emojiTextAttachment = [EmojiTextAttachment new];
    emojiTextAttachment.emojiTag = [NSString stringWithFormat:@"[%@]",emojiArr[index]];
    emojiTextAttachment.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@.png",emojiArr[index]]];
    emojiTextAttachment.emojiSize = 16.0f;
    [self.inputTV.textStorage insertAttributedString:[NSAttributedString attributedStringWithAttachment:emojiTextAttachment] atIndex:self.inputTV.selectedRange.location];
    self.inputTV.selectedRange = NSMakeRange(self.inputTV.selectedRange.location + 1, self.inputTV.selectedRange.length);
    self.backUpLB.hidden = YES;
    [self resetTextStyle];
}

#pragma mark - 应该是重置textView的style
- (void)resetTextStyle {
    NSRange wholeRange = NSMakeRange(0, self.inputTV.textStorage.length);
    
    [self.inputTV.textStorage removeAttribute:NSFontAttributeName range:wholeRange];
    
    [self.inputTV.textStorage addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:14.0f] range:wholeRange];
}

#pragma mark - 无数据页面
- (void)buildEmtpyView
{
    self.emptyView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.emptyView.backgroundColor = [UIColor whiteColor];
    UILabel *emptyLB = [[UILabel alloc] init];
    emptyLB.center = self.emptyView.center;
    emptyLB.bounds = (CGRect){CGPointZero,{WIDTH,40}};
    emptyLB.textAlignment = NSTextAlignmentCenter;
    emptyLB.textColor = RGBA(243, 243, 243, 1);
    emptyLB.font = [UIFont systemFontOfSize:14.0f];
    emptyLB.text = @"这个圈子还是空空如也,快去发表话题吧";
    [self.emptyView addSubview:emptyLB];
    [self.view addSubview:self.emptyView];
    self.emptyView.hidden = YES;
}

#pragma mark - 其他遮罩View
- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    self.shadowView.hidden = YES;
    [self.view addSubview:self.shadowView];
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.6;
    [self.shadowView addSubview:blackView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss:)];
    [blackView addGestureRecognizer:tap];
}

- (void)pickerViewDismiss:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.photoView.frame = CGRectMake(0, HEIGHT, self.photoView.bounds.size.width, self.photoView.bounds.size.height);
        self.shareView.frame = CGRectMake(0, HEIGHT, self.shareView.bounds.size.width, self.shareView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        self.moreFunView.hidden = YES;
        self.exitAlertView.hidden = YES;
        
    }];
}

#pragma mark - 图片选择
- (void)buildPhotoPickView
{
    self.photoView = [[ZSTPhotoTypeView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 180)];
    self.photoView.backgroundColor = [UIColor whiteColor];
    self.photoView.photoTypeDelegate = self;
    [self.photoView createPhotoPicker];
    [self.view addSubview:self.photoView];
}

#pragma mark - 更多功能
- (void)buildMoreFunView
{
    self.moreFunView = [[UIView alloc] init];
    self.moreFunView.center = CGPointMake(self.view.centerX, self.view.centerY - 64);
    self.moreFunView.bounds = (CGRect){CGPointZero,{200,151}};
    self.moreFunView.backgroundColor = [UIColor whiteColor];
    self.moreFunView.hidden = YES;
    
    [self buildLayer:CGRectMake(0, 50, self.moreFunView.bounds.size.width, 0.5) Tag:999];
    [self buildLayer:CGRectMake(0, 100.5, self.moreFunView.bounds.size.width, 0.5) Tag:9999];
    
    [self buildLB:CGRectMake(20, 0, self.moreFunView.bounds.size.width - 40, 50) Tag:101 Title:@"删 除"];
    [self buildLB:CGRectMake(20, 50.5, self.moreFunView.bounds.size.width - 40, 50) Tag:102 Title:@"置 顶"];
    [self buildLB:CGRectMake(20, 101, self.moreFunView.bounds.size.width - 40, 50) Tag:103 Title:@"举 报"];
    
    [self.view addSubview:self.moreFunView];
}

#pragma mark - 重构的方法
- (void)buildLayer:(CGRect)rect Tag:(NSInteger)lineTag
{
    CALayer *line = [CALayer layer];
    line.frame = rect;
    
    if (lineTag == 999)
    {
        self.line = line;
    }
    else
    {
        self.line2 = line;
    }
    
    line.backgroundColor = RGBACOLOR(243, 243, 243, 1).CGColor;
    [self.moreFunView.layer addSublayer:line];
}

- (void)buildLB:(CGRect)rect Tag:(NSInteger)tag Title:(NSString *)btnTitle
{
    UILabel *titleLB = [[UILabel alloc] initWithFrame:rect];
    titleLB.textAlignment = NSTextAlignmentLeft;
    titleLB.font = [UIFont systemFontOfSize:15.0f];
    titleLB.text = btnTitle;
    titleLB.tag = tag * 100;
    titleLB.textColor = [UIColor blackColor];
    [self.moreFunView addSubview:titleLB];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = rect;
    [btn addTarget:self action:@selector(moreFunClick:) forControlEvents:UIControlEventTouchUpInside];
    btn.tag = tag;
    [self.moreFunView addSubview:btn];
}

- (void)moreFunClick:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 101:
        {
            self.shadowView.hidden = NO;
            self.moreFunView.hidden = YES;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.exitAlertView.hidden = NO;
                
            }];
        }
            break;
        case 102:
        {
            [self.engine makeTopicTop:self.moreFunMessage.circleId MID:[NSString stringWithFormat:@"%@",@(self.moreFunMessage.msgId)] IsTop:!self.moreFunMessage.isTop];
        }
            break;
        case 103:
        {
            ZSTTopicReportViewController *reportVC = [[ZSTTopicReportViewController alloc] init];
            reportVC.circleId = self.circleId;
            reportVC.reportDelegate = self;
            reportVC.msgId = [NSString stringWithFormat:@"%@",@(self.moreFunMessage.msgId)];
            [self.navigationController pushViewController:reportVC animated:YES];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - 确认框
- (void)buildSureAlertView
{
    self.exitAlertView = [[UIView alloc] init];
    self.exitAlertView.center = CGPointMake(self.shadowView.centerX, self.shadowView.centerY - 64);
    self.exitAlertView.bounds = (CGRect){CGPointZero,{WIDTH - 60,100}};
    self.exitAlertView.backgroundColor = [UIColor whiteColor];
    [self.shadowView addSubview:self.exitAlertView];
    
    CALayer *lineOne = [CALayer layer];
    lineOne.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineOne.frame = CGRectMake(0, 59.5, self.exitAlertView.bounds.size.width, 0.5);
    [self.exitAlertView.layer addSublayer:lineOne];
    
    CALayer *lineTwo = [CALayer layer];
    lineTwo.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineTwo.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 59.5, 0.5, 40.5);
    [self.exitAlertView.layer addSublayer:lineTwo];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.exitAlertView.bounds.size.width , 60)];
    tipLb.textColor = RGBACOLOR(56, 56, 56, 1);
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.font = [UIFont systemFontOfSize:14.0f];
    tipLb.text = @"您确定要删除该话题吗?";
    [self.exitAlertView addSubview:tipLb];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(0, 60, self.exitAlertView.bounds.size.width / 2, 40);
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [cancelBtn addTarget:self action:@selector(canceDelete:) forControlEvents:UIControlEventTouchUpInside];
    [self.exitAlertView addSubview:cancelBtn];
    
    UIButton *sureExitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureExitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureExitBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    sureExitBtn.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 60, self.exitAlertView.bounds.size.width / 2, 40);
    [sureExitBtn addTarget:self action:@selector(sureDelete:) forControlEvents:UIControlEventTouchUpInside];
    sureExitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.exitAlertView addSubview:sureExitBtn];
    
    self.exitAlertView.hidden = YES;
}

#pragma mark - 取消
- (void)canceDelete:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.exitAlertView.hidden = YES;
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

#pragma mark - 确认
- (void)sureDelete:(UIButton *)sender
{
    [self.engine removeTopic:[NSString stringWithFormat:@"%@",@(self.moreFunMessage.msgId)] CircleId:self.circleId];
}

#pragma mark - 删除话题回调
- (void)removeTopicDidSuccess:(NSDictionary *)response
{
    self.exitAlertView.hidden = YES;
    self.shadowView.hidden = YES;
    NSInteger removeIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *snsaMessage = self.myMessageArr[i];
        if (snsaMessage.msgId == self.moreFunMessage.msgId)
        {
            removeIndex = i;
            break;
        }
    }
    
    [self.dao removeTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.moreFunMessage.msgId)]];
    [self.myMessageArr removeObjectAtIndex:removeIndex];
    [self.myChatTableView reloadData];
    
    if (self.myMessageArr.count == 0)
    {
        self.emptyView.hidden = NO;
    }
}

- (void)removeTopicDidFailure:(NSString *)resultString
{
    
}

#pragma mark - 图片查看器
- (void)buildDarkScroll
{
    self.darkScrollView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.darkScrollView.backgroundColor = [UIColor whiteColor];
    self.darkScrollView.hidden = YES;
    [self.view addSubview:self.darkScrollView];
    
    self.darkScroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT - 50)];
    self.darkScroll.backgroundColor = [UIColor whiteColor];
    [self.darkScrollView addSubview:self.darkScroll];
    self.darkScroll.hidden = YES;
    self.darkScroll.delegate = self;
    self.darkScroll.maximumZoomScale = 2.0;
    self.darkScroll.minimumZoomScale = 0.5;
    
    PMRepairButton *backBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(15, 20, 30, 30)];
    [backBtn setImage:[UIImage imageNamed:@"normal_back.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(hideDarkScroll:) forControlEvents:UIControlEventTouchUpInside];
    [self.darkScrollView addSubview:backBtn];
    
    self.showNumLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(backBtn.frame) + 10, 20, WIDTH - CGRectGetMaxX(backBtn.frame) * 2 - 20, 30)];
    self.showNumLB.font = [UIFont systemFontOfSize:14.0f];
    self.showNumLB.textColor = [UIColor whiteColor];
    self.showNumLB.textAlignment = NSTextAlignmentCenter;
    [self.darkScrollView addSubview:self.showNumLB];
    
    UIView *imgDarkView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 50, WIDTH, 50)];
    imgDarkView.backgroundColor = [UIColor clearColor];
    PMRepairButton *saveBtn = [[PMRepairButton alloc] init];
    saveBtn.frame = CGRectMake(WIDTH - 60, 5, 40, 40);
    [saveBtn setImage:[UIImage imageNamed:@"chatroom_menSave.png"] forState:UIControlStateNormal];
    [saveBtn addTarget:self action:@selector(saveThisPic:) forControlEvents:UIControlEventTouchUpInside];
    [imgDarkView addSubview:saveBtn];
    [self.darkScrollView addSubview:imgDarkView];
}

#pragma mark - 隐藏图片查看
- (void)hideDarkScroll:(PMRepairButton *)sender
{
    for (UIView *dSubview in self.darkScroll.subviews)
    {
        dSubview.hidden = YES;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.navigationController.navigationBarHidden = NO;
        self.darkScroll.backgroundColor = [UIColor whiteColor];
        self.darkScrollView.backgroundColor = [UIColor whiteColor];
        
    } completion:^(BOOL finished) {
        
        self.darkScroll.hidden = YES;
        self.darkScrollView.hidden = YES;
        
    }];
}

#pragma mark - 保存图片
- (void)saveThisPic:(PMRepairButton *)sender
{
    NSInteger i = self.darkScroll.contentOffset.x / WIDTH;
    UIScrollView *imgS = (UIScrollView *)[self.darkScroll viewWithTag:1111111 + i];
    UIImageView *imgIV = (UIImageView *)[imgS viewWithTag:111111 + i];
    UIImage *targetImg = imgIV.image;
    [self saveImageToPhotos:targetImg];
}

- (void)saveImageToPhotos:(UIImage*)savedImage
{
    UIImageWriteToSavedPhotosAlbum(savedImage, self, @selector(image:didFinishSavingWithError:contextInfo:), NULL);
}

- (void)image: (UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    if (error)
    {
        NSLog(@"error --> %@",[error description]);
    }
    else
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"保存成功", nil) withImage:nil];
    }
}

#pragma mark - 发送评论后进行更新
- (void)upDateTopic:(ZSTSNSAMessage *)message newsComments:(ZSTSNSAMessageComments *)newsComments
{
    NSInteger messageIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *thisMessage = self.myMessageArr[i];
        if (thisMessage.msgId == message.msgId)
        {
            messageIndex = i;
            break;
        }
    }
    
    NSMutableArray *threeComment = [message.cgArr mutableCopy];
    if (threeComment.count == 0)
    {
        [threeComment insertObject:newsComments atIndex:0];
    }
    else if (threeComment.count == 3)
    {
        [threeComment removeLastObject];
        [threeComment insertObject:newsComments atIndex:0];
    }
    else
    {
        [threeComment insertObject:newsComments atIndex:0];
    }
    message.cgArr = threeComment;
    message.commentCount += 1;
    
    [self.myMessageArr replaceObjectAtIndex:messageIndex withObject:message];
    [self.dao updateTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(message.msgId)] NewsMessage:message];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:messageIndex];
    [self.myChatTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
    
//    [self.myChatTableView reloadData];
}

#pragma mark - 置顶话题
- (void)makeTopTopic:(ZSTSNSAMessage *)message
{
    message.isTop = message.isTop == 1 ? 0 :1;
    [self.dao updateTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.moreFunMessage.msgId)] NewsMessage:message];
    
    NSInteger oldIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *queueMessage = self.myMessageArr[i];
        if (message.msgId == queueMessage.msgId)
        {
            oldIndex = i;
            break;
        }
    }
    
    if (message.isTop == 1)
    {
        [self.myMessageArr removeObjectAtIndex:oldIndex];
        [self.myMessageArr insertObject:message atIndex:0];
        [self.myChatTableView reloadData];
        
        [self.myChatTableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else
    {
        self.myMessageArr = [self searchSqlite];
        [self.myChatTableView reloadData];
    }
}

#pragma mark - 选择图片
- (void)assetPickerController:(ZYQAssetPickerController *)picker didFinishPickingAssets:(NSArray *)assets
{
    self.photoMenuItems = [NSMutableArray arrayWithArray:assets];
    [picker dismissViewControllerAnimated:YES completion:^{
        
        ZSTNewsTopicViewController *newsTopicVC = [[ZSTNewsTopicViewController alloc] init];
        newsTopicVC.circleId = self.circleId;
        newsTopicVC.newsTopicDelegate = self;
        newsTopicVC.isFromDeal = NO;
        [newsTopicVC chatUploadImage:self.photoMenuItems];
        [self.navigationController pushViewController:newsTopicVC animated:YES];
        
    }];
    
}

- (void)uploadImageAndJump:(NSArray *)arrayOK
{
    ZSTNewsTopicViewController *topicVC = [[ZSTNewsTopicViewController alloc] init];
    topicVC.newsTopicDelegate = self;
    topicVC.circleId = self.circleId;
    topicVC.roomArrayOK = arrayOK;
    [self.navigationController pushViewController:topicVC animated:YES];
    self.shadowView.hidden = YES;
    self.photoView.hidden = YES;
}

#pragma mark - 去个人详情
- (void)goToUserDetail:(NSString *)loginMsisdn
{
    ZSTChatroomMenDetailViewController *menDetailVC = [[ZSTChatroomMenDetailViewController alloc] init];
    menDetailVC.chatMenMsisdn = loginMsisdn;
    menDetailVC.chatMenOrigineMsisdn = loginMsisdn;
    [self.navigationController pushViewController:menDetailVC animated:YES];
}

#pragma mark - 删除话题
- (void)removeTopic:(ZSTSNSAMessage *)message
{
    NSInteger removeIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *snsaMessage = self.myMessageArr[i];
        if (snsaMessage.msgId == message.msgId)
        {
            removeIndex = i;
            break;
        }
    }
    
    [self.dao removeTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(message.msgId)]];
    [self.dao removeComments:self.circleId MID:[NSString stringWithFormat:@"%@",@(message.msgId)]];
    [self.dao removeTopicAllThumb:self.circleId MID:[NSString stringWithFormat:@"%@",@(message.msgId)]];
    [self.myMessageArr removeObjectAtIndex:removeIndex];
    [self.myChatTableView reloadData];
    
    if (self.myMessageArr.count == 0)
    {
        self.emptyView.hidden = NO;
    }
}

#pragma mark - 显示更多
- (void)showMoreFun:(ZSTSNSAMessage *)messageDic
{
    self.moreFunMessage = messageDic;
    self.shadowView.hidden = NO;
    
    UILabel *makeLB = (UILabel *)[self.moreFunView viewWithTag:102 * 100];
    if (messageDic.isTop == 0)
    {
        makeLB.text = @"置 顶";
    }
    else
    {
        makeLB.text = @"取消置顶";
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL isAdmin = [[ud objectForKey:@"isAdmin"] integerValue] == 0 ? NO : YES;
    
    if ([self.moreFunMessage.chatUID integerValue] == [[ZSTF3Preferences shared].UID integerValue])
    {
        [self updateMoreFunView:YES IsManager:isAdmin];
    }
    else
    {
        [self updateMoreFunView:NO IsManager:isAdmin];
    }
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.moreFunView.hidden = NO;
    }];
}

#pragma mark - 更新更多的显示
- (void)updateMoreFunView:(BOOL)isMine IsManager:(BOOL)isManager
{
//    CALayer *line = (CALayer *)[self.moreFunView viewWithTag:999];
//    CALayer *line2 = (CALayer *)[self.moreFunView viewWithTag:9999];
    
    UILabel *deleteLB = (UILabel *)[self.moreFunView viewWithTag:(101 * 100)];
    UILabel *topLB = (UILabel *)[self.moreFunView viewWithTag:(102 * 100)];
    UILabel *reportLB = (UILabel *)[self.moreFunView viewWithTag:(103 * 100)];
    
    UIButton *deleteBtn = (UIButton *)[self.moreFunView viewWithTag:101];
    UIButton *topBtn = (UIButton *)[self.moreFunView viewWithTag:102];
    UIButton *reportBtn = (UIButton *)[self.moreFunView viewWithTag:103];
    
    if (isMine)
    {
        if (isManager)
        {
            self.line.hidden = NO;
            self.line2.hidden= NO;
            deleteLB.hidden = NO;
            deleteBtn.hidden = NO;
            topLB.hidden = NO;
            topBtn.hidden = NO;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,151}};
            topLB.frame = CGRectMake(topLB.frame.origin.x, 50.5, topLB.frame.size.width, topLB.frame.size.height);
            reportLB.frame = CGRectMake(reportLB.frame.origin.x, 101, reportLB.frame.size.width, reportLB.frame.size.height);
            topBtn.frame = CGRectMake(topBtn.frame.origin.x, 50.5, topBtn.frame.size.width, topBtn.frame.size.height);
            reportBtn.frame = CGRectMake(reportBtn.frame.origin.x, 101, reportBtn.frame.size.width, reportBtn.frame.size.height);
        }
        else
        {
            self.line.hidden = NO;
            deleteLB.hidden = NO;
            deleteBtn.hidden = NO;
            
            self.line2.hidden = YES;
            topLB.hidden = YES;
            topBtn.hidden = YES;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,101}};
            reportLB.frame = CGRectMake(topLB.frame.origin.x, 50.5, topLB.frame.size.width, topLB.frame.size.height);
            reportBtn.frame = CGRectMake(topBtn.frame.origin.x, 50.5, topBtn.frame.size.width, topBtn.frame.size.height);
            
        }
    }
    else
    {
        if (isManager)
        {
            self.line.hidden = YES;
            deleteLB.hidden = YES;
            deleteBtn.hidden = YES;
            
            reportBtn.hidden = NO;
            reportLB.hidden = NO;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,100.5}};
            topLB.frame = CGRectMake(topLB.frame.origin.x, 0, topLB.frame.size.width, topLB.frame.size.height);
            topBtn.frame = CGRectMake(topBtn.frame.origin.x, 0, topBtn.frame.size.width, topBtn.frame.size.height);
            reportLB.frame = CGRectMake(reportLB.frame.origin.x, 50.5, reportLB.frame.size.width, reportLB.frame.size.height);
            reportBtn.frame = CGRectMake(reportBtn.frame.origin.x, 50.5, reportBtn.frame.size.width, reportBtn.frame.size.height);
        }
        else
        {
            self.line.hidden = YES;
            deleteLB.hidden = YES;
            deleteBtn.hidden = YES;
            
            self.line2.hidden = YES;
            topLB.hidden = YES;
            topBtn.hidden = YES;
            
            reportBtn.hidden = NO;
            reportLB.hidden = NO;
            
            self.moreFunView.bounds = (CGRect){CGPointZero,{200,50}};
            reportLB.frame = CGRectMake(reportLB.frame.origin.x, 0, reportLB.frame.size.width, reportLB.frame.size.height);
            reportBtn.frame = CGRectMake(reportBtn.frame.origin.x, 0, reportBtn.frame.size.width, reportBtn.frame.size.height);
        }
       
    }
}

#pragma mark - 点赞人的资料
- (void)moveToMenDetail:(ZSTSNSAThumShare *)thumbShare
{
    ZSTChatroomMenDetailViewController *menDetailVC = [[ZSTChatroomMenDetailViewController alloc] init];
    menDetailVC.chatMenMsisdn = thumbShare.msisdn;
    [self.navigationController pushViewController:menDetailVC animated:YES];
}

#pragma mark - 取消photoView
- (void)cancelPhotoView
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.photoView.frame = CGRectMake(0, HEIGHT, self.photoView.bounds.size.width, self.photoView.bounds.size.height);
        
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

#pragma mark - 隐藏分享View
- (void)dismissShareView
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}


#pragma mark EGORefreshTableHeaderDelegate Methods
//下拉到一定距离，手指放开时调用
- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view{
    
    [self reloadTableViewDataSource];
    
    //停止加载，弹回下拉
    [self performSelector:@selector(doneLoadingTableViewData) withObject:nil afterDelay:2.0];
    
    [self performSelector:@selector(updateUI) withObject:nil afterDelay:2.0];
}

- (void)updateUI
{
    self.refreshWay = YES;
    self.isLoadNew = YES;
    NSInteger max = [[[[self.dao selectMaxMsgId:self.circleId] firstObject] safeObjectForKey:@"maxId"] integerValue];
    [self.engine getMyCircleTopic:self.circleId messageID:[NSString stringWithFormat:@"%@",@(max)] Flag:@"new" sortType:SortType_Desc PageSize:self.updateCount == 0 ? kPageSize : self.updateCount];
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

- (void)reloadTableViewDataSource
{
    self.isLoadNew = YES;
    
}

- (void)doneLoadingTableViewData
{
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.myChatTableView];
}



- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    
    [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    
}

#pragma mark - 对评论进行评论
- (void)sendCommentToComments:(ZSTSNSAMessage *)message Comments:(ZSTSNSAMessageComments *)comments
{
    self.inputView.hidden = NO;
    
    if ([comments.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue])
    {
        self.isToComments = NO;
        self.backUpLB.hidden = YES;
    }
    else
    {
        self.backUpLB.text = [NSString stringWithFormat:@"对%@说：",comments.uName];
        self.backUpLB.hidden = NO;
        self.toComments = comments;
        self.isToComments = YES;
    }
    
    self.footerMessage = message;
    [self.inputTV becomeFirstResponder];
}

- (void)sendReportResult:(NSString *)result
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(result, nil) withImage:nil];
}

#pragma mark - 获取新话题数量回调
- (void)getNewsMessageCountDidSuccess:(NSDictionary *)response
{
    self.updateCount = [[response safeObjectForKey:@"data"] integerValue];
    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.updateCount > 0)
        {
            self.newsMessageLB.text = [NSString stringWithFormat:@"有%@条新话题等待更新",@(self.updateCount)];
            self.newsMessageShadowView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.newsMessageShadowView.frame = CGRectMake(0, 0, self.newsMessageShadowView.bounds.size.width, self.newsMessageShadowView.bounds.size.height);
                self.myChatTableView.frame = CGRectMake(0, self.newsMessageShadowView.bounds.size.height, WIDTH, HEIGHT - 64 - self.newsMessageShadowView.bounds.size.height);
                
            }];
        }
        
    });
   
}

- (void)getNewsMessageCountDidFailure:(NSString *)resultString
{
    
}

#pragma mark 与我相关
- (void)buildAboutMeView
{
    self.myMessageBtn = [[PMRepairButton alloc] init];
    self.myMessageBtn.frame = CGRectMake(WIDTH, 20, 100, 30);
    [self.myMessageBtn setBackgroundImage:[UIImage imageNamed:@"chatroom_newsMessageRemind.png"] forState:UIControlStateNormal];
    [self.myMessageBtn setTitle:[NSString stringWithFormat:@"您有%@条消息",@(self.aboutMeCount)] forState:UIControlStateNormal];
    [self.myMessageBtn addTarget:self action:@selector(goToMyNewsMessage:) forControlEvents:UIControlEventTouchUpInside];
    self.myMessageBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [self.view addSubview:self.myMessageBtn];
}

#pragma mark - 去与我相关界面
- (void)goToMyNewsMessage:(PMRepairButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.myMessageBtn.frame = CGRectMake(WIDTH, self.myMessageBtn.frame.origin.y, self.myMessageBtn.bounds.size.width, self.myMessageBtn.bounds.size.height);
        
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.3f animations:^{
            
            
            self.myChatTableView.tableHeaderView = nil;
            
        } completion:^(BOOL finished) {
            
            ZSTChatroomMyNewsMessageViewController *myNewsMessageVC = [[ZSTChatroomMyNewsMessageViewController alloc] init];
            myNewsMessageVC.circleId = self.circleId;
            [self.navigationController pushViewController:myNewsMessageVC animated:YES];
            
        }];
        
    }];
}

#pragma mark - 获取与我相关数量回调
- (void)getAboutMeCountDidSuccess:(NSDictionary *)response
{
    if ([[response safeObjectForKey:@"data"] integerValue] > 0)
    {
        [UIView animateWithDuration:0.3f animations:^{
            [self.myMessageBtn setTitle:[NSString stringWithFormat:@"您有%@条消息",[response safeObjectForKey:@"data"]] forState:UIControlStateNormal];
            self.myMessageBtn.frame = CGRectMake(WIDTH - 100, self.myMessageBtn.frame.origin.y, self.myMessageBtn.bounds.size.width, self.myMessageBtn.bounds.size.height);
            
        }];
    }
}

#pragma mark - 发表新话题
- (void)sendNewsTopicFinish:(NSArray *)imgDataArr Content:(NSString *)content imgString:(NSString *)imgUrlString MID:(NSString *)MID
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSInteger addTime = [dat timeIntervalSince1970];
    NSArray *imageUrlArr = [imgUrlString componentsSeparatedByString:@","];
    if (!imageUrlArr || imageUrlArr.count == 0)
    {
        imageUrlArr = @[];
    }
    NSDictionary *sqlMessageDic = @{@"addtime":@(addTime),@"uid":[ZSTF3Preferences shared].UID,@"msisdn":[ZSTF3Preferences shared].loginMsisdn,@"mid":MID,@"mcontent":content,@"imgurl":imgUrlString,@"headphoto":[ZSTF3Preferences shared].personIcon ? [ZSTF3Preferences shared].personIcon : @"",@"uname":[ZSTF3Preferences shared].personName,@"isthumbup":@(0),@"istop":@(0),@"newcount":@(0),@"thumbup":@(0),@"circleId":self.circleId,@"parentId":@(0),@"comscount":@(0),@"mgscom":@[],@"msgthump":@[]};
    ZSTSNSAMessage *saveMessage = [[ZSTSNSAMessage alloc] initWithMessageDic:sqlMessageDic];
    [self.dao insertCircleMessage:saveMessage CircleId:self.circleId MessageArrString:@""];
    
    self.myMessageArr = [self searchSqlite];
    [self.myChatTableView reloadData];
    
    self.emptyView.hidden = YES;
}

- (void)sendCommentsDidSuccess:(NSDictionary *)response
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSInteger addTime = [dat timeIntervalSince1970];
    NSDictionary *commentDic = @{@"circleID":self.circleId,@"addtime":@(addTime),@"comments":[NSString stringWithFormat:@"%@", [self.inputTV.textStorage getPlainString]],@"msisdn":[ZSTF3Preferences shared].loginMsisdn,@"uid":[ZSTF3Preferences shared].UID,@"uname":[[ZSTF3Preferences shared].personName isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].personName isEqualToString:@""] ? [ZSTF3Preferences shared].personName : @"佚名",@"headphoto":[[ZSTF3Preferences shared].personIcon isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].personIcon isEqualToString:@""] ? [ZSTF3Preferences shared].personIcon : @"",@"mid":@(self.footerMessage.msgId),@"mcid":[NSString stringWithFormat:@"%@",[[response safeObjectForKey:@"data"] safeObjectForKey:@"mcid"]],@"parentid": self.isToComments ? [NSString stringWithFormat:@"%@",@(self.toComments.mcid)] : @"0",@"touserid": self.isToComments ? self.toComments.uid : self.footerMessage.chatUID,@"tousername": self.isToComments ? self.toComments.uName : self.footerMessage.chatNewsUserName,@"tomsisdn": self.isToComments ? self.toComments.msisdn : self.footerMessage.chatMsisdn};
    
    ZSTSNSAMessageComments *newsComments = [[ZSTSNSAMessageComments alloc] initWithCommentDic:commentDic];
    newsComments.commentDic = commentDic;
    
    [self.inputTV resignFirstResponder];
    self.inputView.hidden = YES;
    
    [self upDateTopic:self.footerMessage newsComments:newsComments];
    self.inputTV.text = @"";
    
    self.isToComments = NO;
    
}

#pragma mark - 话题最后的功能
- (void)topicFunClick:(NSInteger)senderTag MessageDic:(ZSTSNSAMessage *)messageDic Btn:(UIButton *)sender
{
    self.footerMessage = messageDic;
    switch (senderTag)
    {
        case 101: //分享
        {
            self.inputView.hidden = YES;
            if (!self.shareView)
            {
                [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"正在初始化分享界面,请稍后...", nil) withImage:nil];
            }
            else
            {
                [self showShareView];
            }
        }
            
            break;
        case 102:
        {
            self.inputView.hidden = NO;
            [self.inputTV becomeFirstResponder];
            self.isToComments = NO;
        }
            break;
        case 103:
        {
            self.inputView.hidden = YES;
            NSString *mid = [NSString stringWithFormat:@"%@",@(messageDic.msgId)];
            if (sender.isSelected)
            {
                [self.engine sendThumbop:self.circleId messageID:mid ThumType:@"0"];
            }
            else
            {
                [self.engine sendThumbop:self.circleId messageID:mid ThumType:@"1"];
            }
            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - 展示shareView
- (void)showShareView
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shadowView.hidden = NO;
        self.shareView.frame = CGRectMake(0, HEIGHT - self.shareView.frame.size.height - 64, WIDTH, self.shareView.frame.size.height);
        
    } completion:^(BOOL finished) {
        
    }];
}

#pragma mark - 点赞回调
- (void)sendThumbopDidSuccess:(NSDictionary *)response
{
    NSInteger messageIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *thisMessage = self.myMessageArr[i];
        if (thisMessage.msgId == self.footerMessage.msgId)
        {
            messageIndex = i;
            break;
        }
    }
    
    ZSTSNSAMessage *oldMessage = self.myMessageArr[messageIndex];
    if ([[response safeObjectForKey:@"data"] integerValue] == 1)
    {
        NSInteger delelteIndex = 0;
        for (NSInteger i = 0; i < oldMessage.thumArr.count; i++)
        {
            ZSTSNSAThumShare *thumbShare = oldMessage.thumArr[i];
            if ([thumbShare.uid integerValue] == [[ZSTF3Preferences shared].UID  integerValue])
            {
                delelteIndex = i;
                break;
            }
        }
        
        oldMessage.thumbupCount -= 1;
        oldMessage.isThumb = 0;
        NSMutableArray *deleteMArr = [oldMessage.thumArr mutableCopy];
        [deleteMArr removeObjectAtIndex:delelteIndex];
        oldMessage.thumArr = deleteMArr;
        
    }
    else
    {
        NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
        NSInteger addTime = [dat timeIntervalSince1970];
        NSDictionary *newsThumb = @{@"addtime":@(addTime),@"uname":[ZSTF3Preferences shared].personName,@"mid":@(oldMessage.msgId),@"mcid":[NSString stringWithFormat:@"%@",@([[response safeObjectForKey:@"data"] integerValue])],@"headphoto":[ZSTF3Preferences shared].personIcon,@"msisdn":[ZSTF3Preferences shared].loginMsisdn,@"uid":[ZSTF3Preferences shared].UID,@"circleId":self.circleId};
        ZSTSNSAThumShare *insertThumb = [[ZSTSNSAThumShare alloc] initWithDic:newsThumb];
        
        oldMessage.thumbupCount += 1;
        oldMessage.isThumb = insertThumb.mcid;
        NSMutableArray *oldThumArr = [oldMessage.thumArr mutableCopy];
        [oldThumArr insertObject:insertThumb atIndex:0];
        oldMessage.thumArr = oldThumArr;
    }
    
    
    [self.myMessageArr replaceObjectAtIndex:messageIndex withObject:oldMessage];
    [self.dao updateTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(oldMessage.msgId)] NewsMessage:oldMessage];
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:messageIndex];
    [self.myChatTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
//    [self.myChatTableView reloadData];
}

-(void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        
        self.backUpLB.hidden = NO;
        
    }else{
        
        self.backUpLB.hidden = YES;
    }
}

- (void)keyBoardWillShow:(NSNotification *)notification
{
    self.faceView.hidden = YES;
    self.shareView.hidden = YES;
    self.photoView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
        self.inputView.frame = CGRectMake(0, HEIGHT - 64 - 50, self.inputView.bounds.size.width, self.inputView.bounds.size.height);
        self.shareView.frame = CGRectMake(0, HEIGHT, self.shareView.bounds.size.width, self.shareView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.isFaceOpen = NO;
    }];
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView animateWithDuration:0.3f animations:^{
        
        CGRect rect = self.view.frame;
        if (IS_IOS7 && !IS_IOS8) {
            rect.origin.y = - 80;
        }else{
            rect.origin.y = - 216;
        }
        
        self.view.frame = rect;
        
    } completion:^(BOOL finished) {
        
        self.faceView.hidden = NO;
        self.shareView.hidden = NO;
        self.photoView.hidden = NO;
    }];
    
}

- (void)keyBoardWillHide:(NSNotification *)notification
{
    [UIView beginAnimations:@"myAnimations" context:nil];
    CGRect rect = self.view.frame;
    rect.origin.y = 64;
    self.view.frame = rect;
    [UIView commitAnimations];
}

#pragma mark - 查看图片
- (void)goToLookBigPic:(NSInteger)tapTag Message:(ZSTSNSAMessage *)message
{
    NSInteger cellImgTag = tapTag - 1000;
    self.bigShowMessage = message;
    NSString *imageArrString = message.imageArrString;
    NSArray *imageArr = [imageArrString componentsSeparatedByString:@","];
    self.showNumLB.text = [NSString stringWithFormat:@"%@ / %@",@(cellImgTag + 1),@(imageArr.count)];
    [self buildDetailDarkScroll:imageArr Offset:(cellImgTag)];
    
    self.darkScroll.hidden = NO;
    self.darkScrollView.hidden = NO;
    [UIView animateWithDuration:0.5f animations:^{
        
        self.navigationController.navigationBarHidden = YES;
        [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
        self.darkScrollView.backgroundColor = [UIColor blackColor];
        self.darkScroll.backgroundColor = [UIColor blackColor];
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)buildDetailDarkScroll:(NSArray *)imgArr Offset:(NSInteger)offsetInteger
{
    if (self.darkScroll.subviews.count < 2)
    {
        
    }
    else
    {
        for (UIView *dSubview in self.darkScroll.subviews)
        {
            if ([dSubview isKindOfClass:[UIScrollView class]])
            {
                [dSubview removeFromSuperview];
            }
            else
            {
                dSubview.hidden = NO;
            }
        }
    }
    
    
    for (NSInteger i = 0; i < imgArr.count; i++)
    {
        UIScrollView *s = [[UIScrollView alloc] initWithFrame:CGRectMake(i * WIDTH, 0, WIDTH, HEIGHT - 60 - 50)];
        s.minimumZoomScale = 1.0;
        s.maximumZoomScale = 3.0;
        s.delegate = [ZSTBigPicDelegate defaultDelegate];
        s.tag = 1111111 + i;
        [self.darkScroll addSubview:s];
        
        UIImageView *detailIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, s.bounds.size.width, s.bounds.size.height)];
        detailIV.contentMode = UIViewContentModeScaleAspectFit;
        detailIV.userInteractionEnabled = YES;
        detailIV.tag = 111111 + i;
        UITapGestureRecognizer *disTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(noLookBigPic:)];
        [detailIV addGestureRecognizer:disTap];
        if ([imgArr[i] rangeOfString:@"http://"].length > 0)
        {
//            [detailIV setImageWithURL:[NSURL URLWithString:imgArr[i]]];
            [detailIV sd_setImageWithURL:[NSURL URLWithString:imgArr[i]]];
        }
        else
        {
            NSString *dataString = imgArr[i];
            NSData *imgData = [[NSData alloc] initWithBase64Encoding:dataString];
            detailIV.image = [UIImage imageWithData:imgData];
        }
        
        [s addSubview:detailIV];
    }
    
    
    self.darkScroll.contentSize = CGSizeMake(WIDTH * imgArr.count, 0);
    self.darkScroll.pagingEnabled = YES;
    self.darkScroll.contentOffset = CGPointMake(offsetInteger * WIDTH, 0);
    
    
}

- (void)noLookBigPic:(UITapGestureRecognizer *)tap
{
    for (UIView *dSubview in self.darkScroll.subviews)
    {
        dSubview.hidden = YES;
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        
        self.navigationController.navigationBarHidden = NO;
        self.darkScroll.backgroundColor = [UIColor whiteColor];
        self.darkScrollView.backgroundColor = [UIColor whiteColor];
        
    } completion:^(BOOL finished) {
        
        self.darkScroll.hidden = YES;
        self.darkScrollView.hidden = YES;
        
    }];
}

#pragma mark - 置顶成功
- (void)makeTopicTopDidSuccess:(NSDictionary *)response
{
    
    self.moreFunView.hidden = YES;
    self.shadowView.hidden = YES;
    
    [TKUIUtil alertInView:self.view withTitle:[response safeObjectForKey:@"notice"] withImage:nil];
    
    self.moreFunMessage.isTop = self.moreFunMessage.isTop == 1 ? 0 :1;
    [self.dao updateTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(self.moreFunMessage.msgId)] NewsMessage:self.moreFunMessage];
    
    NSInteger oldIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *queueMessage = self.myMessageArr[i];
        if (self.moreFunMessage.msgId == queueMessage.msgId)
        {
            oldIndex = i;
            break;
        }
    }
    
    if (self.moreFunMessage.isTop == 1)
    {
        [self.myMessageArr removeObjectAtIndex:oldIndex];
        [self.myMessageArr insertObject:self.moreFunMessage atIndex:0];
        [self.myChatTableView reloadData];
        
        [self.myChatTableView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
    else
    {
        self.myMessageArr = [self searchSqlite];
        [self.myChatTableView reloadData];
    }
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.myChatTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
        
        [self.inputTV resignFirstResponder];
        
        self.inputView.hidden = YES;
        
        [UIView animateWithDuration:0.3f animations:^{
            
            self.faceView.frame = CGRectMake(0, HEIGHT, self.faceView.bounds.size.width, self.faceView.bounds.size.height);
            
        } completion:^(BOOL finished) {
            
            self.isFaceOpen = NO;
        }];
        
        [self.refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
        
         if (!self.isLoadOld && self.hasLoadMore) {
            CGFloat scrollPosition = scrollView.contentSize.height - scrollView.frame.size.height - scrollView.contentOffset.y;
            if (scrollPosition < [self footerLoadMoreHeight]) {
                [self loadMore];
            }
        }
        
        
    }
    else if (scrollView == self.darkScroll)
    {
        NSString *imageArrString = self.bigShowMessage.imageArrString;
        NSArray *imageArr = [imageArrString componentsSeparatedByString:@","];
        NSInteger imageOffsetTag = scrollView.contentOffset.x / WIDTH + 1;
        self.showNumLB.text = [NSString stringWithFormat:@"%@ / %@",@(imageOffsetTag),@(imageArr.count)];
    }
}



#pragma mark - 开始加载更多
- (void)willBeginLoadingMore
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.footerView;
    [fv becomLoading];
    
    NSInteger min = [[[[self.dao selectMinMsgId:self.circleId] firstObject] safeObjectForKey:@"minId"] integerValue];
    [self.engine getMyCircleTopic:self.circleId messageID:[NSString stringWithFormat:@"%@",@(min)] Flag:@"all" sortType:@"desc" PageSize:kPageSize];
}

#pragma mark - 结束加载更多
- (void) loadMoreCompleted
{
    ZSTChatroomLoadMoreView *fv = (ZSTChatroomLoadMoreView *)self.footerView;
    [fv endLoading];
    
    if (!self.hasLoadMore) {
        
        self.myChatTableView.tableFooterView = nil;
        fv.activityIndicator.hidden = YES;
        fv.infoLB.hidden = YES;
    }
}


- (BOOL) loadMore
{
    if (self.isLoadOld)
        return NO;
    
    [self willBeginLoadingMore];
    self.isLoadOld = YES;
    return YES;
}

- (CGFloat) footerLoadMoreHeight
{
    if (self.footerView)
        return self.footerView.frame.size.height;
    else
        return 10;
}

- (void)jumpToDetail:(ZSTSNSAMessage *)chatContentDic
{
    ZSTChatroomTopicDetailViewController *topicDetailVC = [[ZSTChatroomTopicDetailViewController alloc] init];
    topicDetailVC.circleId = self.circleId;
    topicDetailVC.detailDelegate = self;
    topicDetailVC.messageDic = chatContentDic;
    [self.navigationController pushViewController:topicDetailVC animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.shadowView.hidden = YES;
    self.moreFunView.hidden = YES;
    self.photoView.frame = CGRectMake(0, HEIGHT, self.photoView.bounds.size.width, self.photoView.bounds.size.height);
}

- (void)goTopicDetail:(ZSTSNSAMessage *)messageDic
{
    ZSTChatroomTopicDetailViewController *topicDetailVC = [[ZSTChatroomTopicDetailViewController alloc] init];
    topicDetailVC.circleId = self.circleId;
    topicDetailVC.messageDic = messageDic;
    topicDetailVC.detailDelegate = self;
    [self.navigationController pushViewController:topicDetailVC animated:YES];
}

- (void)attributedLabel:(TTTAttributedLabel *)label didSelectLinkWithURL:(NSURL *)url
{
    NSString *urlString = [NSString stringWithFormat:@"%@",url];
    NSInteger startLocation = [urlString rangeOfString:@"Id="].location + @"Id=".length;
    NSString *msisdn = [urlString substringFromIndex:startLocation];
    ZSTChatroomMenDetailViewController *menDetailVC = [[ZSTChatroomMenDetailViewController alloc] init];
    menDetailVC.chatMenMsisdn = msisdn;
    menDetailVC.chatMenOrigineMsisdn = msisdn;
    [self.navigationController pushViewController:menDetailVC animated:YES];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {
        return;
    }
    
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerEditedImage];
    self.photoMenuItems = [NSMutableArray arrayWithObject:tempImage];
    ZSTNewsTopicViewController *newsTopicVC = [[ZSTNewsTopicViewController alloc] init];
    newsTopicVC.circleId = self.circleId;
    newsTopicVC.newsTopicDelegate = self;
    newsTopicVC.isFromDeal = NO;
    newsTopicVC.photoMenuItems = self.photoMenuItems;
    [newsTopicVC chatUploadImage:self.photoMenuItems];
    
    __block ZSTMyChatroomViewController *weakSelf = self;
    [picker dismissViewControllerAnimated:YES completion:^{
        
        [weakSelf.navigationController pushViewController:newsTopicVC animated:YES];
        [UIView animateWithDuration:0.3f animations:^{

            weakSelf.photoView.frame = CGRectMake(0, HEIGHT, weakSelf.photoView.bounds.size.width, weakSelf.photoView.bounds.size.height);
 
        } completion:^(BOOL finished) {
            
            weakSelf.shadowView.hidden = YES;
 
        }];
    }];
    
}

#pragma mark - 分数回调
- (void)getPointDidSucceed:(NSDictionary *)response
{
    NSInteger shareCount = 0;
    if (!self.shareView)
    {
        self.shareView = [[ZSTNewShareView alloc] init];
        self.shareView.backgroundColor = [UIColor whiteColor];
        
        if ([ZSTF3Preferences shared].qqKey && [[ZSTF3Preferences shared].qqKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].qqKey isEqualToString:@""] && [QQApi isQQSupportApi] && [QQApi isQQInstalled])
        {
            self.shareView.isHasQQ = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasQQ = NO;
        }
        
        if ([ZSTF3Preferences shared].wxKey && [[ZSTF3Preferences shared].wxKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].wxKey isEqualToString:@""] && [WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled])
        {
            self.shareView.isHasWX = YES;
            shareCount += 2;
        }
        else
        {
            self.shareView.isHasWX = NO;
        }
        
        if ([ZSTF3Preferences shared].weiboKey && [[ZSTF3Preferences shared].weiboKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].weiboKey isEqualToString:@""])
        {
            self.shareView.isHasWeiBo = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasWeiBo = NO;
        }
        
        self.shareView.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.shareView.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        self.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】地址：http://ci.pmit.cn/d/%@", nil),appDisplayName,[ZSTF3Preferences shared].ECECCID];
        self.shareView.shareString = shareString;
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.shareDelegate = self;
        NSInteger heights = shareCount / 4 + 1;
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, heights * 100);
        [ self.shareView createShareUI];
        [self.view addSubview: self.shareView];
    }
    
    self.shareView.isTodayWX = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXin"] integerValue] == 0 ? NO : YES;
    self.shareView.isTodayWXFriedn = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXinFriends"] integerValue] == 0 ? NO : YES;
    self.shareView.isTodayWeiBo = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiBo"] integerValue] == 0 ? NO : YES;
    self.shareView.isTodayQQ = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareQq"] integerValue] == 0 ? NO : YES;
    
    [self.shareView checkIsHasShare];
}

#pragma mark - 优化发送新话题
- (void)sendNewsTopicJump:(NSArray *)imageDataArr Content:(NSString *)content
{
    NSDate* dat = [NSDate dateWithTimeIntervalSinceNow:0];
    NSInteger addTime = [dat timeIntervalSince1970];
    
    NSMutableArray *dataStringArr = [NSMutableArray array];
    for (NSData *oneData in imageDataArr)
    {
        NSString *dateString = [oneData base64Encoding];
        [dataStringArr addObject:dateString];
    }
    
    double oneImgHeight = 0;
    double oneImgWidth = 0;
    
    if (imageDataArr.count == 1)
    {
        NSData *oneData = imageDataArr[0];
        UIImage *image = [UIImage imageWithData:oneData];
        oneImgWidth = image.size.width;
        oneImgHeight = image.size.height;
    }
    else
    {
        oneImgWidth = 0;
        oneImgHeight = 0;
    }
    
    NSDictionary *sqlMessageDic = @{@"addtime":@(addTime),@"uid":[ZSTF3Preferences shared].UID,@"msisdn":[ZSTF3Preferences shared].loginMsisdn,@"mid":[NSString stringWithFormat:@"%@",@(kTempMID)],@"mcontent":content,@"imgurl":[dataStringArr componentsJoinedByString:@","],@"headphoto":[ZSTF3Preferences shared].personIcon ? [ZSTF3Preferences shared].personIcon : @"",@"uname":[ZSTF3Preferences shared].personName,@"isthumbup":@(0),@"istop":@(0),@"newcount":@(0),@"thumbup":@(0),@"circleId":self.circleId,@"parentId":@(0),@"comscount":@(0),@"mgscom":@[],@"msgthump":@[],@"oneImgHeight":@(oneImgHeight),@"oneImgWidth":@(oneImgWidth)};
    ZSTSNSAMessage *messages = [[ZSTSNSAMessage alloc] initWithMessageDic:sqlMessageDic];
    [self.dao insertCircleMessage:messages CircleId:self.circleId MessageArrString:@""];
    if (self.myMessageArr.count != 0)
    {
        ZSTSNSAMessage *firstMessage = self.myMessageArr[0];
        if (firstMessage.isTop)
        {
            [self.myMessageArr insertObject:messages atIndex:1];
            NSIndexSet *refreshSet = [NSIndexSet indexSetWithIndex:1];
            [self.myChatTableView insertSections:refreshSet withRowAnimation:YES];
        }
        else
        {
            [self.myMessageArr insertObject:messages atIndex:0];
            NSIndexSet *refreshSet = [NSIndexSet indexSetWithIndex:0];
            [self.myChatTableView insertSections:refreshSet withRowAnimation:YES];
        }
    }
    else
    {
        [self.myMessageArr insertObject:messages atIndex:0];
        NSIndexSet *refreshSet = [NSIndexSet indexSetWithIndex:0];
        [self.myChatTableView insertSections:refreshSet withRowAnimation:YES];
    }
    
    self.emptyView.hidden = YES;

    [self.engine sendNewsTopic:self.circleId TopicContent:content ImgArr:imageDataArr];
    
}

- (void)sendNewsTopicDidSuccess:(NSDictionary *)response
{
    NSString *mid = [NSString stringWithFormat:@"%@",[[response safeObjectForKey:@"data"] objectForKey:@"mid"]];
    NSString *imgUrlString = [[response safeObjectForKey:@"data"] safeObjectForKey:@"imgurl"];
    NSInteger refreshIndex = 0;
    for (NSInteger i = 0; i < self.myMessageArr.count; i++)
    {
        ZSTSNSAMessage *oneMessage = self.myMessageArr[i];
        if (oneMessage.msgId == kTempMID)
        {
            refreshIndex = i;
            break;
        }
    }
    
    ZSTSNSAMessage *message = self.myMessageArr[refreshIndex];
    message.imageArrString = imgUrlString;
    message.msgId = [mid integerValue];
    [self.dao updateTopic:self.circleId MID:[NSString stringWithFormat:@"%@",@(kTempMID)] NewsMessage:message];
    [self.myMessageArr replaceObjectAtIndex:refreshIndex withObject:message];
}

- (void)sendNewsTopicDidFailure:(NSString *)response
{
    
}

#pragma mark - 评论点击到个人详情
- (void)commentsToMenDetail:(NSNotification *)notify
{
    NSDictionary *infoDic = notify.userInfo;
    CoreTextLinkData *linkData = [infoDic safeObjectForKey:@"linkData"];
    NSString *urlString = linkData.url;
    NSInteger startLocation = [urlString rangeOfString:@"Id="].location + @"Id=".length;
    NSString *msisdn = [urlString substringFromIndex:startLocation];
    ZSTChatroomMenDetailViewController *menDetailVC = [[ZSTChatroomMenDetailViewController alloc] init];
    menDetailVC.chatMenMsisdn = msisdn;
    menDetailVC.chatMenOrigineMsisdn = msisdn;
    [self.navigationController pushViewController:menDetailVC animated:YES];
}

- (void)finishPickerImageAndShow:(NSArray *)imgArr
{
    self.photoMenuItems = [NSMutableArray arrayWithArray:imgArr];
    ZSTNewsTopicViewController *newsTopicVC = [[ZSTNewsTopicViewController alloc] init];
    newsTopicVC.circleId = self.circleId;
    newsTopicVC.newsTopicDelegate = self;
    newsTopicVC.isFromDeal = NO;
    newsTopicVC.photoMenuItems = self.photoMenuItems;
    [newsTopicVC chatUploadImage:self.photoMenuItems];
    [self.navigationController pushViewController:newsTopicVC animated:YES];
}

@end
