//
//  MyClass.h
//  YouYun
//
//  Created by luobin on 5/31/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTGroupAddressBookTableviewCell : UITableViewCell

@property (nonatomic, retain) TKAsynImageView *avtarImageView;

@property (nonatomic, retain) UILabel *nameLabel;

@property (nonatomic, retain) UIImageView *adminImageView;

@property (nonatomic, retain) UIImageView *stateImageView;

@property (nonatomic, retain) UILabel *phoneNumberLabel;

@property (nonatomic, retain) NSDictionary *userData;

@end
