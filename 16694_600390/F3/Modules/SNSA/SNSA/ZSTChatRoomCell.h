//
//  ZSTChatRoomCell.h
//  SNSA
//
//  Created by LiZhenQu on 14-5-6.
//
//

#import <UIKit/UIKit.h>

@class ZSTChatRoomCell;

@protocol ClickViewDelegate <NSObject>

- (void)ClickViewDidClicked:(ZSTChatRoomCell *)cell;

@end


@interface ZSTChatRoomCell : UITableViewCell<ZSTYouYunEngineDelegate,TKAsynImageViewDelegate>

@property (nonatomic, retain) ZSTYouYunEngine *engine;
@property (nonatomic, retain) NSDictionary *cellData;

@property (nonatomic, retain) NSString *cid;

//@property (nonatomic, retain) TKAsynImageView *avtarImageView; //头像
@property (nonatomic,retain) UIImageView *avtarImageView;

//@property (nonatomic, retain) TKAsynImageView *pictureImageView; //消息图片
@property (retain ,nonatomic) UIImageView *pictureImageView;;

@property (nonatomic, retain) UIImageView *chatBg; //只做一个背景

@property (nonatomic, retain) NIAttributedLabel *contentLabel;

@property (nonatomic, assign) id<ClickViewDelegate>delegate;

- (void)chatBubbleTouchUpInsideAction;

@end
