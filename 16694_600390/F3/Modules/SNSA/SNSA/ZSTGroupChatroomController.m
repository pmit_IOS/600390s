//
//  ZSTGroupChatController.m
//  YouYun
//
//  Created by luobin on 5/30/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTGroupChatroomController.h"
#import "ZSTGroupChatTableViewCell.h"
#import "ZSTCreateTopicController.h"
#import "ZSTUtils.h"

#define kOneLimit 10
#define KCommentLimit  3

@implementation ZSTGroupChatroomController

@synthesize circle;
@synthesize engine;

- (void)dealloc
{
    [self removeObserver];
    self.engine = nil;
    self.circle = nil;
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)createTopic
{
    ZSTCreateTopicController *createTopicController = [[ZSTCreateTopicController alloc] init];
    createTopicController.circleID = [circle safeObjectForKey:@"CID"];
    
    ZSTCreateTopicControllerCompletionHandler completionHandler = ^(BOOL done) {
        if (done) {
            [self performSelector:@selector(autoRefresh) 
                       withObject:nil 
                       afterDelay:0.6];
        }
    };
    createTopicController.completionHandler = completionHandler;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:createTopicController];
    navController.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    [self presentViewController:navController animated:YES completion:nil];
    [navController release];
    [createTopicController release];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:[circle safeObjectForKey:@"CName"]];
    self.tableView.backgroundView = [[[UIView alloc] init] autorelease];
    
    UIImageView *generalImg = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
    generalImg.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
    generalImg.contentMode = UIViewContentModeScaleToFill;
    [self.tableView.backgroundView addSubview:generalImg];
    [generalImg release];
    
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"话题", nil) target:self selector:@selector (createTopic)];
    //self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    _hasMore = [[NSUserDefaults standardUserDefaults] boolForKey:[NSString stringWithFormat:@"%@_%@_hasMore", ZSTHttpMethod_GetMessages, [circle safeObjectForKey:@"CID"]]];
    
    /*
    self.data = [TKDataCache getCacheDataByURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages, [circle objectForKey:@"CID"]]];
    */
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
    self.tableView.backgroundColor = [UIColor whiteColor];
    
    self.data = (NSMutableArray *)[self.dao topicAndCommentOfCircle:[circle safeObjectForKey:@"CID"] startId:0 topicCount:kOneLimit commentCount:KCommentLimit];
    [self.tableView reloadData];
    
    [self registerObserver];
}

#pragma mark ---------- 检测是否有可用设备（麦克风）-----------------

- (BOOL) startAudioSession//检测是否可以访问麦克风
{
	// Prepare the audio session
	NSError *error;
	self.session = [AVAudioSession sharedInstance];
	
	if (![self.session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}//设置会话环境
    
    
	
	if (![self.session setActive:YES error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}//激活会话
	
	return self.session.inputIsAvailable;
}


// 注册观察者
-(void)registerObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playVideo:) name:[NSString stringWithFormat:@"%@", @"PlayVideo"] object:nil];
}


- (void) myMovieFinishedCallback:(NSNotification*)aNotification
{
    MPMoviePlayerController* theMoviePlayer=[aNotification object];
	CFShow([aNotification userInfo]);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:theMoviePlayer];
    [theMoviePlayer stop];
}

-(void)playVideo:(NSNotification*)notify
{
    if ([self startAudioSession]) {
        NSString *attachmentPath = [notify object];
        MPMoviePlayerViewController* theMoviePlayer=[[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:attachmentPath]];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myMovieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:[theMoviePlayer moviePlayer]];
        [self presentModalViewController:theMoviePlayer animated:YES];
        [[theMoviePlayer moviePlayer] play];
        [theMoviePlayer release];
    }
}

// 移除注册的观察者
-(void)removeObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:[NSString stringWithFormat:@"%@", @"PlayVideo"] object:nil];
}

#pragma mark - overriding  super class 

- (Class)tableView:(UITableView*)tableView cellClassForObject:(id)object
{
    return [ZSTGroupChatTableViewCell class];
}

- (void)refreshNewestData {
    [self.engine getMessages:[circle safeObjectForKey:@"CID"] messageID:@"0" sortType:SortType_Desc fetchCount:kOneLimit];
}

- (void)loadMoreData {
    [self.engine getMessages:[circle safeObjectForKey:@"CID"] messageID:[[self.data lastObject] safeObjectForKey:@"MID"] sortType:SortType_Desc fetchCount:kOneLimit];
}

#pragma mark - ZSTYouYunEngineDelegate
- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    [self fetchDataFailed];
    
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"网络连接失败!",nil) withImage:nil withCenter:CGPointMake(160, 100)];
}

- (void)getMessagesResponse:(NSArray *)messages dataFromLocal:(BOOL)dataFromLocal hasMore:(BOOL)hasMore
{    
    [[NSUserDefaults standardUserDefaults] setBool:hasMore forKey:[NSString stringWithFormat:@"%@_%@_hasMore", ZSTHttpMethod_GetMessages, [circle safeObjectForKey:@"CID"]]];
    [self fetchDataSuccess:messages hasMore:hasMore];
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithFormat:@"%@_%@", @"ZSTSNSMessageCount", @(self.moduleType)] object:nil userInfo:nil];
}

- (void)getCommentsByMIDResponse:(NSArray *)msgs
{
    if (msgs && [msgs count]) {
        NSUInteger startPos = 0;
        if ([msgs count] > 3) {
            startPos = [msgs count] - 3;
        } else {
            startPos = 0;
        }
        
        if (row < [self.data count]) {
            NSDictionary *topic = [self.data objectAtIndex:row];
            NSString *mid = [topic safeObjectForKey:@"MID"];
            NSString *newMid = [[msgs objectAtIndex: 0] safeObjectForKey:@"Parentid"];
            if ([mid intValue] != [newMid intValue]) {
                return;
            }
            NSMutableArray *mutableArray = [[NSMutableArray alloc] init];
            for (NSUInteger i=startPos; i<[msgs count]; i++) {
                id object = [[[msgs objectAtIndex:i] copy] autorelease];
                [mutableArray addObject:object];
            }
            
            NSMutableDictionary *newTopic = [NSMutableDictionary dictionaryWithDictionary:topic];
            [newTopic removeObjectForKey:@"bubbleHeight"];
            [newTopic setObject:mutableArray forKey:@"Cmts"];
            [self.data replaceObjectAtIndex:row withObject:newTopic];
            
            [self.dao saveComments:mutableArray circle:[circle safeObjectForKey:@"CID"]];
            [mutableArray release];
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:0];
            NSArray *array = [NSArray arrayWithObjects:indexPath, nil];
            [self.tableView reloadRowsAtIndexPaths:array withRowAnimation:UITableViewRowAnimationNone];
        }
    }
}

#pragma mark - ZSTCommentSendFinishDelegate
- (void)commentSendFinished:(NSDictionary *)topic row:(NSInteger)pos
{
    row = pos;
    [engine getCommentsByMID:[[topic safeObjectForKey:@"MID"] description]];
}

- (void)autoRefresh
{
    
}

@end
