//
//  ZSTChatroomWaitCareViewController.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatroomWaitCareViewController.h"
#import <ZSTUtils.h>
#import "ZSTYouYunEngine.h"
#import "ZSTChatroomMyCircleCell.h"
#import "ZSTChatHomeViewController.h"

@interface ZSTChatroomWaitCareViewController () <UITableViewDataSource,UITableViewDelegate,ZSTYouYunEngineDelegate,ZSTChatroomMyCircleCellDelegate>

@property (strong,nonatomic) UITableView *waitCareTableView;
@property (strong,nonatomic) NSMutableArray *waitCareArr;
@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (strong,nonatomic) UIView *shadowView;
@property (copy,nonatomic) NSString *circleId;


@end

@implementation ZSTChatroomWaitCareViewController

static NSString *const waitCell = @"waitCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.waitCareArr = [NSMutableArray array];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"企业圈子", nil)];
    self.view.backgroundColor = RGBACOLOR(243, 243, 243, 1);
    [self buildWaitCareTableView];
    [self buildShadowView];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    [self.engine getCircles:[NSString stringWithFormat:@"%@:%@",@"",@""]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildWaitCareTableView
{
    self.waitCareTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.waitCareTableView.delegate = self;
    self.waitCareTableView.dataSource = self;
    self.waitCareTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.waitCareTableView setSeparatorColor:[UIColor whiteColor]];
    [self.waitCareTableView registerClass:[ZSTChatroomMyCircleCell class] forCellReuseIdentifier:waitCell];
    [self.view addSubview:self.waitCareTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.waitCareArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomMyCircleCell *cell = [tableView dequeueReusableCellWithIdentifier:waitCell];
    [cell createUI];
    cell.circleCellDelegate = self;
    [cell setCellData:self.waitCareArr[indexPath.row]];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)getCirclesResponse:(NSArray *)circles
{
    for (NSDictionary *oneCircleDic in circles)
    {
        if ([[oneCircleDic safeObjectForKey:@"AuditStatus"] integerValue] != 1 )
        {
            [self.waitCareArr addObject:oneCircleDic];
        }
    }
    
    [self.waitCareTableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)goToCareThisCircle:(NSDictionary *)circleDic
{
    self.circleId = [circleDic safeObjectForKey:@"CID"];
    [self.engine joinCircle:[circleDic safeObjectForKey:@"CID"] flag:0];
    self.homeVC.isNeedRefresh = YES;
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shadowView addSubview:alphaView];
    self.shadowView.hidden = YES;
}

- (void)jionCircleSuccess
{
    NSInteger dealIndex = 0;
    for (NSInteger i = 0; i < self.waitCareArr.count; i++)
    {
        NSDictionary *oneCircleDic = self.waitCareArr[i];
        if ([[oneCircleDic safeObjectForKey:@"CID"] integerValue] == [self.circleId integerValue])
        {
            dealIndex = i;
            break;
        }
    }
    
    NSMutableDictionary *dealCircleDic = [self.waitCareArr[dealIndex] mutableCopy];
    [dealCircleDic setValue:@(2) forKey:@"AuditStatus"];
    [self.waitCareArr replaceObjectAtIndex:dealIndex withObject:dealCircleDic];
    [self.waitCareTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:dealIndex inSection:0]] withRowAnimation:UITableViewRowAnimationNone];
}

@end
