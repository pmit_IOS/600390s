//
//  ZSTChatHomeViewController.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTYouYunEngine.h"

@interface ZSTChatHomeViewController : ZSTModuleBaseViewController

@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (assign,nonatomic) BOOL isNeedRefresh;

@end
