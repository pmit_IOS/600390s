//
//  ZSTSNSAShowAllCircleViewController.h
//  SNSA
//
//  Created by zhangwanqiang on 14-4-25.
//
//

#import <UIKit/UIKit.h>
#import "PopOver.h"
#import "ZSTRegisterViewController.h"

@interface ZSTSNSAShowAllCircleViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,EGORefreshTableHeaderDelegate,ZSTYouYunEngineDelegate,UIActionSheetDelegate,PopOverDelegate,RegisterDelegate>
{
    EGORefreshTableHeaderView *_refreshHeaderView;
    UITableView *_circleTableView;
    BOOL _isRefreshing;
    NSInteger _row;

}
@property (nonatomic, retain) NSArray *circles;

@property (nonatomic, retain) ZSTYouYunEngine *engine;

//- (void)login;

- (void)autoRefresh;

- (void)refreshCirles;

@end
