//
//  ZSTGoodDelegate.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTGoodDelegate.h"
#import "ZSTChatroomShareGoodCell.h"

@implementation ZSTGoodDelegate

static NSString *const goodCell = @"goodCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.goodArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomShareGoodCell *cell = [tableView dequeueReusableCellWithIdentifier:goodCell];
    cell.isGood = YES;
    [cell createUI];
    NSDictionary *thumDic = self.goodArr[indexPath.row];
    [cell setCellData:[thumDic safeObjectForKey:@"uname"] DateString:[thumDic safeObjectForKey:@"addtime"] HeadeString:[thumDic safeObjectForKey:@"headphoto"]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

@end
