//
//  HHCommentController.m
//  HHelloCat
//
//  Created by luo bin on 12-3-3.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTCreateTopicController.h"
#import "TKUIUtil.h"
#import "ZSTCreateTopicController.h"
#import "MBProgressHUD.h"
#import "ZSTUtils.h"
#import "UIImage+Compress.h"
#import "ZSTSNSAAudioAverageView.h"
#import "ZSTYouYunEngine.h"

#define kMaxCommentLength 140

#define attachmentHeight 40

#define ROOTFILEPATH [ZSTUtils pathForTempFinderOfOutbox]
#define AUDIOPATH [ROOTFILEPATH stringByAppendingPathComponent:[ZSTUtils audioDateString]]
#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

@implementation ZSTCreateTopicController

@synthesize session;
@synthesize recorder;
@synthesize textView;
@synthesize countLabel;
@synthesize pictureBtn;
@synthesize voiceBtn;
@synthesize sendBtn;
@synthesize circleID;
@synthesize engine;
@synthesize completionHandler;
@synthesize iconPickerController;

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dealloc
{
//    [HHUIUtil hiddenHUD];
    [DSBezelActivityView removeViewAnimated:NO];
    self.engine = nil;
    self.circleID = nil;
    self.pictureBtn = nil;
    self.sendBtn = nil;
    self.countLabel = nil;
    self.textView = nil;
    self.engine = nil;
    self.completionHandler = nil;
    self.iconPickerController = nil;
    [super dealloc];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - View lifecycle
- (void)addTopLine
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.view.layer.contents = (id) [UIImage imageNamed:@"bg.png"].CGImage;

    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"取消",@"取消") target:self selector:@selector (dismiss)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"发表话题",nil)];
    
    self.navigationController.navigationBar.tintColor = [ZSTUtils getNavigationTintColor];
    
    self.textView = [[[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(10, 10, 300, 100)] autorelease];
    self.textView.font = [UIFont systemFontOfSize:13];
    [self.textView setPlaceholder:NSLocalizedString(@"在此输入话题...",nil)];
    self.textView.delegate = self;
    self.textView.backgroundImage = [ZSTModuleImage(@"module_snsa_textViewBackground.png") stretchableImageWithLeftCapWidth:50 topCapHeight:7];
    [self.view addSubview:self.textView];
    [self.textView becomeFirstResponder];
    
    countLabel = [[[UILabel alloc] initWithFrame:CGRectMake(230, 90, 80, 18)] autorelease];
    countLabel.backgroundColor = [UIColor clearColor];
    countLabel.font = [UIFont systemFontOfSize:14];
    countLabel.textAlignment = NSTextAlignmentCenter;
    countLabel.textColor = [UIColor groupTableViewBackgroundColor];
    countLabel.text = @"140";
    [self.view addSubview:countLabel];
    
    self.pictureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.pictureBtn.frame = CGRectMake(10, 118, 40, 40);
    [self.pictureBtn addTarget:self action:@selector(selectPictureAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.pictureBtn setBackgroundImage:ZSTModuleImage(@"module_snsa_create_topic_btn_photo.png") forState:UIControlStateNormal];
    [self.view addSubview:self.pictureBtn];
    
    if ([self startAudioSession]) {
        self.voiceBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.voiceBtn.frame = CGRectMake(10 + 45, 118 , 40, 40);
        [self.voiceBtn addTarget:self action:@selector(selectAudioAction) forControlEvents:UIControlEventTouchUpInside];
        [self.voiceBtn setBackgroundImage:ZSTModuleImage(@"module_snsa_create_topic_btn_photo.png") forState:UIControlStateNormal];
        [self.view addSubview:self.voiceBtn];
    }
    
    self.sendBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.sendBtn.frame = CGRectMake(197, 120, 113, 32);
    [self.sendBtn addTarget:self action:@selector(sendButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.sendBtn setBackgroundImage:ZSTModuleImage(@"module_snsa_create_topic_btn_send_n.png") forState:UIControlStateNormal];
    [self.sendBtn setBackgroundImage:ZSTModuleImage(@"module_snsa_create_topic_btn_send_p.png") forState:UIControlStateHighlighted];
    [self.sendBtn setTitle:NSLocalizedString(@"发送", @"发送") forState:UIControlStateNormal];
    [self.view addSubview:self.sendBtn];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dismiss
{
    self.completionHandler(NO);
    [self dismissViewControllerAnimated:YES completion:nil];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark ---------- 检测是否有可用设备（麦克风）-----------------

- (BOOL) startAudioSession//检测是否可以访问麦克风
{
	// Prepare the audio session
	NSError *error;
	self.session = [AVAudioSession sharedInstance];
	
	if (![self.session setCategory:AVAudioSessionCategoryPlayAndRecord error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}//设置会话环境
    
    
	
	if (![self.session setActive:YES error:&error])
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}//激活会话
	
	return self.session.inputIsAvailable;
}

#pragma mark -------------------AudioButtonAction-----------------------------------------------------

- (void) continueRecording
{
	[self.recorder record];
    recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.0f target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
    meter.pauseButton.hidden = NO;
    meter.continueButton.hidden = YES;
}

- (void) pauseRecording
{
	[self.recorder pause];
    
    NSString *recorderCurrenttime = [self formatTime:self.recorder.currentTime];
    
	meter.timelabel.text = recorderCurrenttime;
    
    [recordTimer invalidate];
    recordTimer = nil;
    meter.pauseButton.hidden = YES;
    meter.continueButton.hidden = NO;
}
- (void) stopRecording
{
    if (recordTimer != nil) {
        [recordTimer invalidate];
        recordTimer = nil;
    }
    
    [meter dismiss];
    meter = nil;
	[self.recorder stop];
    
    recorder = nil;
    [[AVAudioSession sharedInstance] setActive: NO error: nil];
}

- (BOOL) selectAudioAction
{
	NSError *error;
	
	// Recording settings//PCM脉冲编码调制
	NSMutableDictionary *settings = [[NSMutableDictionary alloc] init];
	[settings setValue: [NSNumber numberWithInt:kAudioFormatLinearPCM] forKey:AVFormatIDKey];//音频格式（此处为非常大的文件）
	[settings setValue: [NSNumber numberWithFloat:8000.0] forKey:AVSampleRateKey];//每秒采样8000次（采样率）
	[settings setValue: [NSNumber numberWithInt: 1] forKey:AVNumberOfChannelsKey]; // mono单声道
	[settings setValue: [NSNumber numberWithInt:16] forKey:AVLinearPCMBitDepthKey];//线性pcm位深度（采样位数）
	[settings setValue: [NSNumber numberWithBool:NO] forKey:AVLinearPCMIsBigEndianKey];
	[settings setValue: [NSNumber numberWithBool:NO] forKey:AVLinearPCMIsFloatKey];//采样信号是整数还是浮点数
	
    [_filePath release];
    _filePath = [AUDIOPATH retain];
    
	NSURL *url = [NSURL fileURLWithPath:_filePath];
    
    if ([[NSFileManager defaultManager] fileExistsAtPath:_filePath]) {
        [[NSFileManager defaultManager] removeItemAtPath:_filePath error:nil];
    }
    
	self.recorder = [[[AVAudioRecorder alloc] initWithURL:url settings:settings error:&error] autorelease];
    [settings release];
    //    [self.recorder recordForDuration:59];//设置录音上限为59秒
	if (!self.recorder)
	{
		NSLog(@"Error: %@", [error localizedDescription]);
		return NO;
	}
    
	self.recorder.delegate = self;
	self.recorder.meteringEnabled = YES;
    
	
	if (![self.recorder prepareToRecord])//搭建环境
	{
		NSLog(@"Error: Prepare to record failed");
		return NO;
	}
	
	if (![self.recorder record])
	{
		NSLog(@"Error: Record failed");
		return NO;
	}
    meter = [[ZSTSNSAAudioAverageView alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
    meter.center = self.view.center;
    [meter.stopButton addTarget:self action:@selector(stopRecording) forControlEvents:UIControlEventTouchUpInside];
    [meter.pauseButton addTarget:self action:@selector(pauseRecording) forControlEvents:UIControlEventTouchUpInside];
    [meter.continueButton addTarget:self action:@selector(continueRecording) forControlEvents:UIControlEventTouchUpInside];
    [meter show];
	recordTimer = [NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(updateMeters) userInfo:nil repeats:YES];
    
	return YES;
}

- (NSString *) formatTime: (int) num
{
	int secs = num % 60;
	int min = num / 60;
	if (num < 60) return [NSString stringWithFormat:@"0:%02d", num];
	return	[NSString stringWithFormat:@"%d:%02d", min, secs];
}

- (void) updateMeters
{
	[self.recorder updateMeters];
	float avg = [self.recorder averagePowerForChannel:0];
    float topImageWidth = meter.topImageView.image.size.width;
    float topImageHeight = meter.topImageOriginHeight;
    
    //这里需要适配一下，如果是高分辨率得就是两倍
    
    if (isRetina || iPhone5)
    {
        avg = 2*avg;
        topImageWidth = 2*topImageWidth;
        topImageHeight = 2*topImageHeight;
    }
    
    [meter updateTopImageViewRect:CGRectMake(0, -avg, topImageWidth, topImageHeight+avg)];
    
    NSString *recorderCurrenttime = [self formatTime:self.recorder.currentTime];
    
	meter.timelabel.text = recorderCurrenttime;
}

- (void)audioRecorderDidFinishRecording:(AVAudioRecorder *)recorder successfully:(BOOL)flag

{
    if (recordTimer != nil) {
        [recordTimer invalidate];
        recordTimer = nil;
    }
    if (meter != nil) {
        [meter dismiss];
        meter = nil;
    }
    
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    
    [self updateAttachmentView];
}

- (void)updateAttachmentView
{
    if (_filePath == nil) {
        return;
    }
    
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSInteger attachmentViewCount = [attachmentFileName count];
    CGRect attchmetViewFrame = CGRectMake(0, 120, 330, attachmentHeight);
    ZSTSNSAAttachmentView *attachmentView = [[ZSTSNSAAttachmentView alloc] initWithFrame:attchmetViewFrame];
    attachmentView.delegate = self;
    attachmentView.imageView.tag = attachmentViewCount-1;
   
    attachmentView.imageNameLabel.text = [NSString stringWithFormat:@"大小:%@KB",[NSNumber numberWithInteger:[ZSTUtils fileSize:_filePath]]];
    
    if ([[_filePath lastPathComponent] hasSuffix:@"wav"]) {
        
        [attachmentView.imageView setBackgroundImage:[UIImage imageNamed:@"voice_attachmet_icon.png"] forState:UIControlStateNormal];
        [attachmentView.imageView addTarget:self action:@selector(playVideo:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [self.view addSubview:attachmentView];
    [attachmentView release];
    
     self.pictureBtn.frame = CGRectMake(10, 118 + 50, 40, 40);
     self.voiceBtn.frame = CGRectMake(10 + 45, 118 + 50 , 40, 40);
    self.sendBtn.frame = CGRectMake(197, 120 + 50, 113, 32);
}

#pragma mark - ButtonAction

- (void)sendButtonAction:(id)sender
{
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSInteger attachmentViewCount = [attachmentFileName count];
    NSString *attachmentName = [attachmentFileName objectAtIndex:(attachmentViewCount-1)];
    NSString *attachmentPath = [[ZSTUtils pathForTempFinderOfOutbox]  stringByAppendingPathComponent:attachmentName];
    NSData *fileData = [NSData dataWithContentsOfFile:attachmentPath];
    
    UIImage *attachmentImage = [self.pictureBtn currentImage];
    
    if (self.textView.text != nil && [self.textView.text isEmptyOrWhitespace] && attachmentImage == nil && fileData == nil) {
        TKAlertAppNameTitle(NSLocalizedString(@"内容不能为空!",nil));
        return;
    } else if (self.textView.text != nil && [self.textView.text isEmptyOrWhitespace] && attachmentImage != nil){
        self.textView.text = NSLocalizedString(@"分享图片",nil);
    } else if (self.textView.text != nil && [self.textView.text isEmptyOrWhitespace] && fileData != nil){
        self.textView.text = NSLocalizedString(@"分享声音",nil);
    }

    
    if (attachmentImage) {
        
        [self.engine upLoadFile:UIImagePNGRepresentation(attachmentImage) progressDelegate:nil suffix:@"png"];
    }
    
    if (fileData) {
        
         [self.engine upLoadVideoFile:fileData progressDelegate:nil suffix:@"wav"];
    }
    
    if (!attachmentImage && !fileData) {
        
        [self.engine sendMessage:self.circleID parentID:nil content:self.textView.text fileKey:nil];
    }
    
    [self.textView resignFirstResponder];
    
    [TKUIUtil showHUDInView:self.view 
                   withText:NSLocalizedString(@"正在发送", nil)
                  withImage:nil 
                 withCenter:CGPointMake(160, 100)];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)selectPictureAction:(id)sender
{
    UIActionSheet *actionSheet = nil;
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"获取图片",nil)
                                                                 delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                                   destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"拍照",nil), NSLocalizedString(@"本地图片",nil), nil];
    } else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"获取图片",nil)
                                                  delegate:self
                                         cancelButtonTitle:NSLocalizedString(@"取消",@"取消")
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:NSLocalizedString(@"本地图片",nil), nil];
    }
    
    [actionSheet showInView:self.view];
    [actionSheet release];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

-(void)myMovieFinishedCallback:(NSNotification*)aNotification
{
    MPMoviePlayerController* theMoviePlayer=[aNotification object];
	CFShow([aNotification userInfo]);
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MPMoviePlayerPlaybackDidFinishNotification object:theMoviePlayer];
    [theMoviePlayer stop];
}

-(void)playVideo:(UIButton *)attachmentButton
{
    NSArray *attachmentFileName = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
    NSString *attachmentName = [attachmentFileName objectAtIndex:attachmentButton.tag];
    NSString *attachmentPath = [[ZSTUtils pathForTempFinderOfOutbox]  stringByAppendingPathComponent:attachmentName];
    
   	MPMoviePlayerViewController* theMoviePlayer=[[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL fileURLWithPath:attachmentPath]];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(myMovieFinishedCallback:) name:MPMoviePlayerPlaybackDidFinishNotification object:[theMoviePlayer moviePlayer]];
    [self presentModalViewController:theMoviePlayer animated:YES];
    [[theMoviePlayer moviePlayer] play];
    [theMoviePlayer release];
    
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    self.iconPickerController = [[[UIImagePickerController alloc] init] autorelease];
   // [iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    if ( buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // 拍照
        
        iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        iconPickerController.showsCameraControls = YES;
        
    } else if(buttonIndex != actionSheet.cancelButtonIndex) { // 本地图片
        
        iconPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    }
    iconPickerController.allowsEditing = NO;
    iconPickerController.delegate = self;
    [self presentModalViewController:iconPickerController animated:YES];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {  
        return;
    }
    
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerOriginalImage];   
    [self compressImage:tempImage];
}

- (void)compressImage:(UIImage *)bigImage
{
    [TKUIUtil showHUDInView:self.iconPickerController.view 
                   withText:NSLocalizedString(@"正在压缩", @"正在压缩") 
                  withImage:nil 
                 withCenter:self.iconPickerController.view.center ];
    [self performSelector:@selector(doCompressImage:) withObject:bigImage afterDelay:0.6];
}

- (void)doCompressImage:(UIImage *)bigImage
{
     UIImage *newImage = [UIImage imageWithData:[[bigImage compressedImageScaleFactor:640] compressedData]];
    [self.pictureBtn setImage:newImage forState:UIControlStateNormal];
    
    [TKUIUtil hiddenHUD];
    [self.iconPickerController dismissViewControllerAnimated:YES completion:nil];
    self.iconPickerController = nil;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)theTextView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    NSString *comment = [theTextView.text stringByReplacingCharactersInRange:range withString:text];
    NSUInteger length = [comment length];
    
    UIButton *sendButton = (UIButton *)self.navigationItem.rightBarButtonItem.customView;
    sendButton.enabled = (length != 0);
    
    if (length <= kMaxCommentLength) {
        self.countLabel.text = [NSString stringWithFormat:@"%@/%d",[NSNumber numberWithUnsignedInteger:length], kMaxCommentLength];
        return YES;
    } else {
        return NO;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark - ZSTLegicyCommunicatorDelegate

- (void)sendMessageResponse
{
    [self performSelector:@selector(dosSendMessageResponse) withObject:nil afterDelay:1];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)dosSendMessageResponse
{
    [TKUIUtil hiddenHUD];
    
    self.completionHandler(YES);
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)upLoadFileResponse:(NSString *)fileKey
{
    TKDINFO(@"fileKey == %@", fileKey);
    
    [self.engine sendMessage:self.circleID parentID:nil content:self.textView.text fileKey:fileKey];
}

- (void)upLoadVideoFileResponse:(NSString *)fileKey
{
    TKDINFO(@"fileKey == %@", fileKey);
    
    //这里有两个方法，不知道该用哪个
    [self.engine sendvideoMessage:self.circleID parentID:nil content:self.textView.text fileKey:fileKey videoTimeLength:0];
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"网络连接失败!",nil) withImage:nil withCenter:CGPointMake(160, 150)];
    [TKUIUtil hiddenHUDAfterDelay:2];

}

@end
