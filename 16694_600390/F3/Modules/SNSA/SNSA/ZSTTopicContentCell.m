//
//  ZSTTopicContentCell.m
//  SNSA
//
//  Created by pmit on 15/10/14.
//
//

#import "ZSTTopicContentCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImage/SDWebImageManager.h>
#import "CoreTextData.h"
#import "CTFrameParserConfig.h"
#import "CTFrameParser.h"

@interface ZSTTopicContentCell()

@end

@implementation ZSTTopicContentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.chatroomContentLB)
    {
        self.chatroomContentLB = [[CDTDisplayView alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30, 10)];
        self.chatroomContentLB.backgroundColor = [UIColor clearColor];
        self.chatroomContentLB.userInteractionEnabled = YES;
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToDetail:)];
        [self.chatroomContentLB addGestureRecognizer:tap];
        [self.contentView addSubview:self.chatroomContentLB];
        
        self.chatroomContentIV = [[UIView alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(self.chatroomContentLB.frame) + 5, WIDTH - 30, (WIDTH - 40) / 3 * 3 + 10)];
        [self.contentView addSubview:self.chatroomContentIV];
        
        for (NSInteger i = 0; i < 9; i++)
        {
            NSInteger ivWidth = i % 3;
            NSInteger ivHeight = i / 3;
            
            UIImageView *iv = [[UIImageView alloc] initWithFrame:CGRectMake(ivWidth * (WIDTH - 40) / 3 + ivWidth * 5, ivHeight * (WIDTH - 40) / 3 + (ivHeight) * 5, (WIDTH - 40) / 3, (WIDTH - 40) / 3)];
            iv.tag = 1000 + i;
            iv.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToBigPic:)];
            [iv addGestureRecognizer:tap];
            [self.chatroomContentIV addSubview:iv];
        }
    }
    
    self.chatroomContentLB.data = nil;
}

- (void)goToBigPic:(UITapGestureRecognizer *)tap
{
    if ([self.contentCellDelegate respondsToSelector:@selector(goToLookBigPic:Message:)])
    {
        [self.contentCellDelegate goToLookBigPic:tap.view.tag Message:self.chatContentDic];
    }
}

- (void)goToDetail:(UITapGestureRecognizer *)tap
{
    if ([self.contentCellDelegate respondsToSelector:@selector(jumpToDetail:)])
    {
        [self.contentCellDelegate jumpToDetail:self.chatContentDic];
    }
}

- (void)setCellData:(ZSTSNSAMessage *)chatContentDic
{
    self.chatContentDic = chatContentDic;
    NSString *chatContentString = chatContentDic.chatContent;
    
    if (!chatContentString || [chatContentString isEqualToString:@""])
    {
        self.chatroomContentLB.hidden = YES;
        self.chatroomContentIV.frame = CGRectMake(15, 0, self.chatroomContentIV.bounds.size.width, self.chatroomContentIV.bounds.size.height);
    }
    else
    {
        self.chatroomContentLB.hidden = NO;
    
        CTFrameParserConfig *config = [[CTFrameParserConfig alloc] init];
        config.width = self.chatroomContentLB.width;
        config.textColor = [UIColor blackColor];
        
        CTFrameParser *parser = [[CTFrameParser alloc] init];
        CoreTextData *data = [parser parseTemplateTopicContent:chatContentString config:config];

        self.chatroomContentLB.data = data;
        self.chatroomContentLB.height = data.height;
        self.chatroomContentLB.frame = CGRectMake(self.chatroomContentLB.frame.origin.x, self.chatroomContentLB.frame.origin.y, self.chatroomContentLB.bounds.size.width, data.height);
        [self.chatroomContentLB setNeedsDisplay];
    }
    
    NSString *chatImgArr = chatContentDic.imageArrString;
    NSArray *imgArr = [chatImgArr componentsSeparatedByString:@","];
    if (!imgArr || imgArr.count == 0 || [[imgArr firstObject] isEqualToString:@""])
    {
        self.chatroomContentIV.hidden = YES;
    }
    else if (imgArr.count == 1)
    {
        self.chatroomContentIV.hidden = NO;
        UIImageView *iv = (UIImageView *)[self.chatroomContentIV viewWithTag:1000];
        for (NSInteger i = 1; i < 9; i++)
        {
            UIImageView *hiddenIV = (UIImageView *)[self.chatroomContentIV viewWithTag:(1000 + i)];
            hiddenIV.hidden = YES;
        }
        __block CGFloat dealWidth;
        __block CGFloat dealHeight;
        id imgObj = imgArr[0];
        if ([imgObj rangeOfString:@"http://"].length > 0)
        {
            [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:imgArr[0]] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
                
                CGFloat imgWidth = image.size.width;
                CGFloat imgHeight = image.size.height;
                CGFloat maxWidth = (WIDTH - 30) / 2;
                CGFloat maxHeight = maxWidth * 1.5;
                
                if (imgWidth > maxWidth)
                {
                    CGFloat imgNowHeight = imgHeight * (maxWidth / imgWidth);
                    dealWidth = maxWidth;
                    if (imgNowHeight > maxHeight)
                    {
                        dealHeight = maxHeight;
                    }
                    else
                    {
                        dealHeight = imgNowHeight;
                    }
                }
                else
                {
                    dealWidth = imgWidth;
                    if (imgHeight > maxHeight)
                    {
                        dealHeight = maxHeight;
                    }
                    else
                    {
                        dealHeight = imgHeight;
                    }
                }
                
                UIImage *showImg = [self imageCompressForSize:image targetSize:CGSizeMake(dealWidth, dealHeight)];
                iv.image = showImg;
                iv.frame = CGRectMake(iv.frame.origin.x, iv.frame.origin.y, dealWidth, dealHeight);
                
            }];
        }
        else
        {
            NSData *imgData = [[NSData alloc] initWithBase64Encoding:imgObj];
            UIImage *img = [[UIImage alloc] initWithData:imgData];
            CGFloat imgWidth = img.size.width;
            CGFloat imgHeight = img.size.height;
            CGFloat maxWidth = (WIDTH - 30) / 2;
            CGFloat maxHeight = maxWidth * 1.5;
            if (imgWidth > maxWidth)
            {
                CGFloat imgNowHeight = imgHeight * (maxWidth / imgWidth);
                dealWidth = maxWidth;
                if (imgNowHeight > maxHeight)
                {
                    dealHeight = maxHeight;
                }
                else
                {
                    dealHeight = imgNowHeight;
                }
            }
            else
            {
                dealWidth = imgWidth;
                if (imgHeight > maxHeight)
                {
                    dealHeight = maxHeight;
                }
                else
                {
                    dealHeight = imgHeight;
                }
            }
            
            UIImage *showImg = [self imageCompressForSize:img targetSize:CGSizeMake(dealWidth, dealHeight)];
            iv.image = showImg;
            iv.frame = CGRectMake(iv.frame.origin.x, iv.frame.origin.y, dealWidth, dealHeight);
        }
        
        
        if (!self.chatroomContentLB.hidden)
        {
            self.chatroomContentIV.frame = CGRectMake(15, CGRectGetMaxY(self.chatroomContentLB.frame) + 5, dealWidth, dealHeight);
        }
        else
        {
            self.chatroomContentIV.frame = CGRectMake(15, 0, dealWidth, dealHeight);
        }

    }
    else
    {
        self.chatroomContentIV.hidden = NO;
        for (NSInteger i = 0; i < imgArr.count; i++)
        {
            UIImageView *iv = (UIImageView *)[self.chatroomContentIV viewWithTag:(1000 + i)];
            iv.frame = CGRectMake(iv.frame.origin.x, iv.frame.origin.y, (WIDTH - 40) / 3, (WIDTH - 40) / 3);
            iv.hidden = NO;
            id imgObj = imgArr[i];
            if ([imgObj isKindOfClass:[NSString class]])
            {
                if ([imgObj rangeOfString:@"http://"].length > 0)
                {
                    [iv sd_setImageWithURL:[NSURL URLWithString:imgObj]];
                }
                else
                {
                    NSData *imgData = [[NSData alloc] initWithBase64Encoding:imgObj];
                    [iv setImage:[UIImage imageWithData:imgData]];
                }
            }
        }
        
        NSInteger heightIndex = imgArr.count / 3;
        NSInteger lessNum = imgArr.count % 3;
        CGFloat height = 0;
        
        if (lessNum == 0)
        {
            height = heightIndex;
        }
        else
        {
            height = heightIndex + 1;
        }
        
        for (NSInteger i = imgArr.count; i < 9; i++)
        {
            UIImageView *iv = (UIImageView *)[self.chatroomContentIV viewWithTag:(1000 + i)];
            iv.hidden = YES;
        }
        
        if (!self.chatroomContentLB.hidden)
        {
            self.chatroomContentIV.frame = CGRectMake(15, CGRectGetMaxY(self.chatroomContentLB.frame), WIDTH - 30, (WIDTH - 40) / 3 * height + heightIndex* 5);
        }
        else
        {
            self.chatroomContentIV.frame = CGRectMake(15, 0, WIDTH - 30, (WIDTH - 40) / 3 * height + heightIndex* 5);
        }
        
    }
}

- (UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size
{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

- (UIImage *)dealImgWith:(UIImage *)img DealWidth:(CGFloat)dealWidth DealHeight:(CGFloat)dealHeight
{
    CGFloat imgWidth = img.size.width;
    CGFloat imgHeight = img.size.height;
    CGFloat maxWidth = (WIDTH - 30) / 2;
    CGFloat maxHeight = maxWidth * 1.5;
    if (imgWidth > maxWidth)
    {
        CGFloat imgNowHeight = imgHeight * (maxWidth / imgWidth);
        dealWidth = maxWidth;
        if (imgNowHeight > maxHeight)
        {
            dealHeight = maxHeight;
        }
        else
        {
            dealHeight = imgNowHeight;
        }
    }
    else
    {
        dealWidth = imgWidth;
        if (imgHeight > maxHeight)
        {
            dealHeight = maxHeight;
        }
        else
        {
            dealHeight = imgHeight;
        }
    }
    
    UIImage *showImg = [self imageCompressForSize:img targetSize:CGSizeMake(dealWidth, dealHeight)];
    return showImg;
}

@end
