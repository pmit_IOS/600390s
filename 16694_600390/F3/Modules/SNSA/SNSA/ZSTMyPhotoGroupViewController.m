//
//  ZSTMyPhotoGroupViewController.m
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import "ZSTMyPhotoGroupViewController.h"
#import <AssetsLibrary/ALAssetsLibrary.h>
#import "ZSTPhotoGroupCell.h"
#import <AssetsLibrary/ALAssetsGroup.h>
#import "ZSTMyGroupPhotoViewController.h"

@interface ZSTMyPhotoGroupViewController () <UITableViewDataSource,UITableViewDelegate,ZSTMyGroupPhotoViewControllerDelegate>

@property (strong,nonatomic) UITableView *myPhotoGroupTableView;
@property (strong,nonatomic) NSMutableArray *photoGroupsArr;
@property (strong,nonatomic) ALAssetsLibrary *assetsLibrary;

@end

@implementation ZSTMyPhotoGroupViewController

static NSString *const photoGroupCell = @"photoGroupCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = NO;
    self.modalPresentationCapturesStatusBarAppearance = NO;
    self.navigationController.navigationBar.translucent = NO;
    
    self.photoGroupsArr = [NSMutableArray array];
    self.title = @"照片";
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"取消" style:UIBarButtonItemStyleDone target:self action:@selector(dismiss)];
    [self buildGroupTableView];
    [self getPhotoGroup];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)buildGroupTableView
{
    self.myPhotoGroupTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.myPhotoGroupTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.myPhotoGroupTableView.dataSource = self;
    self.myPhotoGroupTableView.delegate = self;
    [self.myPhotoGroupTableView registerClass:[ZSTPhotoGroupCell class] forCellReuseIdentifier:photoGroupCell];
    [self.view addSubview:self.myPhotoGroupTableView];
}

- (void)getPhotoGroup
{
    ALAssetsLibraryGroupsEnumerationResultsBlock listGroupBlock =
    
    ^(ALAssetsGroup *group, BOOL *stop) {
        
        if (group!=nil) {
            
            [self.photoGroupsArr addObject:group];
            
        } else {
            
            [self.myPhotoGroupTableView performSelectorOnMainThread:@selector(reloadData)
             
                                             withObject:nil waitUntilDone:YES];
            
        }
        
    };
    
    ALAssetsLibraryAccessFailureBlock failureBlock = ^(NSError *error)
    {
        NSString *errorMessage = nil;
        switch ([error code]) {
            case ALAssetsLibraryAccessUserDeniedError:
            case ALAssetsLibraryAccessGloballyDeniedError:
                errorMessage = @"The user has declined access to it.";
                break;
            default:
                errorMessage = @"Reason unknown.";
                break;
        }
        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"Opps"
                                                           message:errorMessage delegate:self cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:nil, nil];
        [alertView show];
    };
    
    NSUInteger groupTypes = ALAssetsGroupAlbum | ALAssetsGroupEvent |
    
    ALAssetsGroupFaces | ALAssetsGroupSavedPhotos | ALAssetsGroupLibrary;
    
    self.assetsLibrary = [[ALAssetsLibrary alloc]  init];
    
    [self.assetsLibrary enumerateGroupsWithTypes:groupTypes
     
                                 usingBlock:listGroupBlock failureBlock:failureBlock];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.photoGroupsArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTPhotoGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:photoGroupCell];
    [cell createUI];
    ALAssetsGroup *group = [self.photoGroupsArr objectAtIndex:indexPath.row];
    CGImageRef posterImageRef = [group posterImage];
    UIImage *posterImage = [UIImage imageWithCGImage:posterImageRef];
    NSString *groupName = [group valueForProperty:ALAssetsGroupPropertyName];
    NSString *groupCount = [NSString stringWithFormat:@"%@",[group valueForProperty:ALAssetsGroupPropertyType]];
    [cell setCellData:posterImage Name:groupName Count:groupCount];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ALAssetsGroup *group = self.photoGroupsArr[indexPath.row];
    ZSTMyGroupPhotoViewController *myGPVC = [[ZSTMyGroupPhotoViewController alloc] init];
    myGPVC.groupName = [group valueForProperty:ALAssetsGroupPropertyName];
    myGPVC.selectGroup = group;
    myGPVC.maxCount = self.maxCount;
    myGPVC.groupPhotoDelegate = self;
    [self.navigationController pushViewController:myGPVC animated:YES];
}

- (void)dismiss
{
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)finishPickerImage:(NSArray *)selectedImgArr
{
    if ([self.photoGroupDelegate respondsToSelector:@selector(finishPickerImageAndShow:)])
    {
        [self.photoGroupDelegate finishPickerImageAndShow:selectedImgArr];
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
