//
//  ZSTChatroomNoDataCell.m
//  SNSA
//
//  Created by pmit on 15/10/13.
//
//

#import "ZSTChatroomNoDataCell.h"

@implementation ZSTChatroomNoDataCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.noDataLB)
    {
        self.noDataLB = [[UILabel alloc] init];
        self.noDataLB.center = CGPointMake(WIDTH / 2, 90);
        self.noDataLB.bounds = (CGRect){CGPointZero,{WIDTH,60}};
        self.noDataLB.textAlignment = NSTextAlignmentCenter;
        self.noDataLB.font = [UIFont systemFontOfSize:14.0f];
        self.noDataLB.textColor = RGBA(243, 243, 243, 1);
        [self.contentView addSubview:self.noDataLB];
    }
}

- (void)setCellLBNameString:(NSString *)tipString
{
    self.noDataLB.text = tipString;
}

@end
