//
//  ZSTTopicChatController.m
//  YouYun
//
//  Created by luobin on 6/7/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTTopicCommentController.h"
#import "ZSTCommentTableViewCell.h"
#import "TKUtil.h"

@interface ZSTTopicCommentSendingCell : UITableViewCell {
@private
    TKAsynImageView *_avtarImageView;
    NIAttributedLabel *_label;
}
@end

@implementation ZSTTopicCommentSendingCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImageView *avtarbg = [[[UIImageView alloc] initWithFrame:CGRectMake(10, 8, 40, 40)] autorelease];
        avtarbg.image = [ZSTModuleImage(@"module_snsa_icon_frame.png") stretchableImageWithLeftCapWidth:2 topCapHeight:2];
        avtarbg.backgroundColor = [UIColor clearColor];
        [self addSubview:avtarbg];
        
        _avtarImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(2, 2, 36, 36)];
        _avtarImageView.userInteractionEnabled = NO;
        _avtarImageView.defaultImage = ZSTModuleImage(@"module_snsa_default_avatar.png");
        _avtarImageView.backgroundColor = [UIColor whiteColor];
        
        [avtarbg addSubview:_avtarImageView];
        
        UIImageView *ellipsisImageView = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_snsa_feed-comment-ellipsis.png")];
        ellipsisImageView.frame = CGRectMake(60, 25, 16, 5);
        [self addSubview:ellipsisImageView];
        [ellipsisImageView release];
        
    }
    return self;
}

- (void)dealloc
{
    TKRELEASE(_avtarImageView);
    TKRELEASE(_label);
    [super dealloc];
}

@end


@interface ZSTTopicCommentController()<TKGrowingTextViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, retain) NSMutableArray *comments;

@property (nonatomic, retain) UILabel *failureLabel;

@end

@implementation ZSTTopicCommentController

@synthesize tableView;
@synthesize topic;
@synthesize engine;
@synthesize CID;
@synthesize comments;
@synthesize failureLabel;
@synthesize delegate;
@synthesize row;
@synthesize isDefaultCircle;

-(id)init
{
	self = [super init];
	if(self){
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillShow:) 
													 name:UIKeyboardWillShowNotification 
												   object:nil];
		
		[[NSNotificationCenter defaultCenter] addObserver:self 
												 selector:@selector(keyboardWillHide:) 
													 name:UIKeyboardWillHideNotification 
												   object:nil];		

        _showSendingCell = NO;
        isDefaultCircle = NO;
        self.comments = [NSMutableArray array];
	}
	
	return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    TKRELEASE(_sendBtn);
    TKRELEASE(containerView);
    
    self.failureLabel = nil;
    self.comments = nil;
    self.CID = nil;
    self.engine = nil;
    self.topic = nil;
    self.tableView = nil;
    [super dealloc];
}

#pragma mark - View lifecycle

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{
    [super viewDidLoad];
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    self.engine.moduleType = self.moduleType;
    
    lastContentOffsetY = 0.f;
    
    //self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    self.tableView = [[[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 480 - (IS_IOS_7?0:20) - 44 - 40 + (iPhone5?88:0) ) style:UITableViewStylePlain] autorelease];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor colorWithWhite:0.98 alpha:1];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    self.tableView.separatorColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
    
    UIView *tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 100)] autorelease];
    tableFooterView.backgroundColor = [UIColor clearColor];
    self.tableView.tableFooterView = tableFooterView;
    
    self.failureLabel = [[[UILabel alloc] initWithFrame:CGRectMake(0, 30, 320, 20)] autorelease];
    failureLabel.shadowOffset = CGSizeMake(0, 1);
    failureLabel.shadowColor = [UIColor whiteColor];
    failureLabel.backgroundColor = [UIColor clearColor];
    failureLabel.textAlignment = NSTextAlignmentCenter;
    failureLabel.font = [UIFont systemFontOfSize:[UIFont systemFontSize]];
    [tableFooterView addSubview:failureLabel];
    
    containerView = [[UIView alloc] initWithFrame:CGRectMake(0, self.view.height - 40, self.view.width, 40)];
    
	textView = [[TKGrowingTextView alloc] initWithFrame:CGRectMake(6, 3, 240, 40)];
    textView.contentInset = UIEdgeInsetsMake(0, 5, 0, 5);
    textView.userInteractionEnabled = NO;
	textView.minNumberOfLines = 1;
	textView.maxNumberOfLines = 3;
	textView.font = [UIFont systemFontOfSize:15.0f];
	textView.delegate = self;
    [textView setbackgroudImage];
    [textView setPlaceholder:NSLocalizedString(@"在此输入评论…", nil)];
    textView.internalTextView.scrollIndicatorInsets = UIEdgeInsetsMake(5, 0, 5, 0);
    textView.backgroundColor = [UIColor  whiteColor];
//    [textView becomeFirstResponder];
    
    [self.view addSubview:containerView];
    
    UIImage *rawEntryBackground = ZSTModuleImage(@"module_snsa_MessageEntryInputField.png");
    UIImage *entryBackground = [rawEntryBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *entryImageView = [[[UIImageView alloc] initWithImage:entryBackground] autorelease];
    entryImageView.frame = CGRectMake(5, 0, 248, 40);
    entryImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    UIImage *rawBackground = ZSTModuleImage(@"module_snsa_MessageEntryBackground.png");
    UIImage *background = [rawBackground stretchableImageWithLeftCapWidth:13 topCapHeight:22];
    UIImageView *imageView = [[[UIImageView alloc] initWithImage:background] autorelease];
    imageView.frame = CGRectMake(0, 0, containerView.frame.size.width, containerView.frame.size.height);
    imageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    
    textView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    // view hierachy
    [containerView addSubview:imageView];
    [containerView addSubview:textView];
    [containerView addSubview:entryImageView];
    
    UIImage *sendBtnBackground = [ZSTModuleImage(@"module_snsa_MessageEntrySendButton.png") stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    UIImage *selectedSendBtnBackground = [ZSTModuleImage(@"module_snsa_MessageEntrySendButtonPressed.png") stretchableImageWithLeftCapWidth:13 topCapHeight:0];
    
    _sendBtn = [[UIButton buttonWithType:UIButtonTypeCustom] retain];
    _sendBtn.enabled = NO;
	_sendBtn.frame = CGRectMake(containerView.frame.size.width - 69, 8, 63, 27);
    _sendBtn.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin;
	[_sendBtn setTitle:NSLocalizedString(@"发送", @"发送") forState:UIControlStateNormal];
    
    [_sendBtn setTitleShadowColor:[UIColor colorWithWhite:0 alpha:0.4] forState:UIControlStateNormal];
    _sendBtn.titleLabel.shadowOffset = CGSizeMake (0.0, -1.0);
    _sendBtn.titleLabel.font = [UIFont boldSystemFontOfSize:18.0f];
    
    [_sendBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[_sendBtn addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
    [_sendBtn setBackgroundImage:sendBtnBackground forState:UIControlStateNormal];
    [_sendBtn setBackgroundImage:selectedSendBtnBackground forState:UIControlStateSelected];
	[containerView addSubview:_sendBtn];
    containerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin;
    
    [self.view newNetworkIndicatorViewWithRefreshLoadBlock:^(TKNetworkIndicatorView *networkIndicatorView) {
        [engine getCommentsByMID:[[topic objectForKey:@"MID"] description]];
    }];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

#pragma mark Message

- (void)clearChatInput {
    textView.text = @"";
}

- (void)sendMessage {
    
    NSString *rightTrimmedMessage = [textView.text stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    
    // Don't send blank messages.
    if (rightTrimmedMessage.length == 0) {
        [self clearChatInput];
        return;
    }
    
    _sendBtn.enabled = NO;
    
    // sending a comment to the server
    [self.engine sendMessage:self.CID parentID:[[topic safeObjectForKey:@"MID"] description] content:textView.text fileKey:nil];
}

//Code from Brett Schumann
-(void) keyboardWillShow:(NSNotification *)note{
    // get keyboard size and loctaion
	CGRect keyboardBounds;
    [[note.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue: &keyboardBounds];
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    // Need to translate the bounds to account for rotation.
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - (keyboardBounds.size.height + containerFrame.size.height);
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    [UIView setAnimationDelegate:self];
	
	// set views with new info
	containerView.frame = containerFrame;
    self.tableView.frame = CGRectMake(0, 0, 320, containerFrame.origin.y);
	
	// commit animations
	[UIView commitAnimations];
}

- (void)animationDidStop:(NSString *)animationID finished:(BOOL)finished
{
//    if (finished) {
        [self.tableView scrollToBottom:YES];
//    }
}

-(void) keyboardWillHide:(NSNotification *)note{
    NSNumber *duration = [note.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [note.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
	
	// get a rect for the textView frame
	CGRect containerFrame = containerView.frame;
    containerFrame.origin.y = self.view.bounds.size.height - containerFrame.size.height;
	
	// animations settings
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationBeginsFromCurrentState:YES];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    
	// set views with new info
	containerView.frame = containerFrame;
    self.tableView.frame = CGRectMake(0, 0, 320, containerFrame.origin.y);
	
	// commit animations
	[UIView commitAnimations];
}

#pragma mark - TKGrowingTextViewDelegate

- (void)growingTextView:(TKGrowingTextView *)growingTextView willChangeHeight:(float)height
{
    float diff = (growingTextView.frame.size.height - height);
    
	CGRect r = containerView.frame;
    r.size.height -= diff;
    r.origin.y += diff;
	containerView.frame = r;
    self.tableView.frame = CGRectMake(0, 0, 320, r.origin.y);
}

- (void)growingTextViewDidChange:(TKGrowingTextView *)theGrowingTextView
{
    if (theGrowingTextView.text == nil || [theGrowingTextView.text length] == 0) {
        
        _sendBtn.enabled = NO;
        
        if (_showSendingCell) {
            _showSendingCell = NO;
            
            NSInteger rowNumber = [self tableView:self.tableView numberOfRowsInSection:0];
            
            ZSTCommentTableViewCell *previousCell = (ZSTCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowNumber - 1 inSection:0]];
            TKCellBackgroundView *bgView = (TKCellBackgroundView *)previousCell.backgroundView;
            bgView.position = ((rowNumber == 1)?CustomCellBackgroundViewPositionSingle : CustomCellBackgroundViewPositionBottom);
            [bgView setNeedsDisplay];
            
            [self.tableView deleteRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:rowNumber inSection:0], nil] withRowAnimation:UITableViewRowAnimationBottom];
        }
        
    } else {
        
        _sendBtn.enabled = YES;
        
        if (!_showSendingCell) {
            _showSendingCell = YES;
            
            NSInteger rowNumber = [self tableView:self.tableView numberOfRowsInSection:0];
            
            ZSTCommentTableViewCell *previousCell = (ZSTCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:rowNumber - 2 inSection:0]];
            TKCellBackgroundView *bgView = (TKCellBackgroundView *)previousCell.backgroundView;
            bgView.position = ((rowNumber == 2)?CustomCellBackgroundViewPositionTop : CustomCellBackgroundViewPositionMiddle);
            [bgView setNeedsDisplay];
            
            
            [self.tableView insertRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:rowNumber-1 inSection:0], nil] withRowAnimation:UITableViewRowAnimationBottom];
            [self.tableView scrollToBottom:YES];
        }
    }
    
}

- (NSDictionary *)commentForRow:(NSUInteger)theRow
{
    NSDictionary *comment = nil;
    
    NSString *content = [topic safeObjectForKey:@"MContent"];
    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    if (content != nil && ![content isEmptyOrWhitespace]) {
        
        if (theRow == 0) {
            comment = self.topic;
        } else {
            
            comment = [self.comments objectAtIndex:theRow - 1];
        }
        
    } else {
        
        comment = [self.comments objectAtIndex:theRow];
    }
    return comment;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)theTableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSInteger rowNumber = [self tableView:theTableView numberOfRowsInSection:0];
    
    if (indexPath.row == (rowNumber - 1) && _showSendingCell){
        return 56.f;
    }
    
    NSDictionary *comment = [self commentForRow:indexPath.row];
    return [ZSTCommentTableViewCell suggestSizeForComment:comment].height;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.tracking && lastContentOffsetY > scrollView.contentOffset.y) {
        [textView resignFirstResponder];
    }
    lastContentOffsetY = scrollView.contentOffset.y;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSString *content = [topic safeObjectForKey:@"MContent"];
    content = [content stringByTrimmingTrailingWhitespaceAndNewlineCharacters];
    if (content != nil && ![content isEmptyOrWhitespace]) {
        return [self.comments count] + 1 + (_showSendingCell?1:0);
    }
    
    return [self.comments count] + (_showSendingCell?1:0);
}

- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TKCellBackgroundView *custview = [[[TKCellBackgroundView alloc] init] autorelease];
    custview.fillColor = [UIColor colorWithWhite:0.98 alpha:1];
    custview.borderColor = [UIColor colorWithWhite:0.93 alpha:1];
    custview.shadowColor = [UIColor whiteColor];
    
    if(([theTableView numberOfRowsInSection:indexPath.section]-1) == 0){
        custview.position = CustomCellBackgroundViewPositionSingle;
    }
    else if(indexPath.row == 0){
        custview.position = CustomCellBackgroundViewPositionTop;
    }
    else if (indexPath.row == ([theTableView numberOfRowsInSection:indexPath.section]-1)){
        custview.position  = CustomCellBackgroundViewPositionBottom;
    }
    else{
        custview.position = CustomCellBackgroundViewPositionMiddle;
    }
    
    if (indexPath.row == ([theTableView numberOfRowsInSection:indexPath.section]-1) && _showSendingCell){
        ZSTTopicCommentSendingCell *cell = [[ZSTTopicCommentSendingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
        cell.backgroundView = custview;
        return [cell autorelease];
    }
    
    static NSString *identifier = @"ZSTCommentTableViewCell";
    ZSTCommentTableViewCell *cell = [theTableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil) {
        cell = [[[ZSTCommentTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.tag = indexPath.row;
    }
    cell.backgroundView = custview;
    
    cell.comment = [self commentForRow:indexPath.row];
    cell.soundView.hidden = indexPath.row;
    return cell;
}

- (void)requestDidFail:(NSError *)error method:(NSString *)method
{
    if ([method isEqualToString:ZSTHttpMethod_SendMessage]) {
        _sendBtn.enabled = YES;
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"网络连接失败!", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(160, 150)];
        [TKUIUtil hiddenHUDAfterDelay:2];
    } else if ([method isEqualToString:ZSTHttpMethod_GetCommentsByMID]) {
        [self.view refreshFailed];
    }
}

- (void)sendMessageResponse
{
    NSDictionary *comment = [NSDictionary dictionaryWithObjectsAndKeys: textView.text, @"MContent", @"哈哈", @"New_UName" , 
                             [TKUtil timeIntervalSince1970:[[NSDate date] timeIntervalSince1970]], @"AddTime", nil];
    [self.comments addObject:comment];
    
    if (_showSendingCell) {
        _showSendingCell = NO;
        
        NSInteger rowNumber = [self tableView:self.tableView numberOfRowsInSection:0];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:rowNumber-1 inSection:0], nil] withRowAnimation:UITableViewRowAnimationTop];
    }
    [self clearChatInput];
    
    if ((delegate != nil) && [delegate respondsToSelector:@selector(commentSendFinished:row:)]) {
        [delegate commentSendFinished:topic row:row];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshChat object:nil];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)getCommentsByMIDResponse:(NSArray *)msgs
{
    NSInteger rowNumber = [self tableView:self.tableView numberOfRowsInSection:0];
    
    textView.userInteractionEnabled = YES;
    self.comments = (NSMutableArray *)msgs;
    [self.view removeNetworkIndicatorView];
    self.tableView.tableFooterView = nil;
    self.failureLabel = nil;
    
    //更新主题的腐蚀线
    if ([msgs count]) {
        ZSTCommentTableViewCell *firstCell = (ZSTCommentTableViewCell *)[self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0]];
        TKCellBackgroundView *bgView = (TKCellBackgroundView *)firstCell.backgroundView;
        bgView.position = CustomCellBackgroundViewPositionTop;
        [bgView setNeedsDisplay];
    }
    
    NSMutableArray *indexPaths = [NSMutableArray arrayWithCapacity:[msgs count]];
    for (int i = 0; i < [msgs count]; i++) {
        [indexPaths addObject:[NSIndexPath indexPathForRow:i + rowNumber inSection:0]];
    }
    
//     [TKDataCache setCacheData:messages andURLKey:[NSString stringWithFormat:@"%@_%@", ZSTHttpMethod_GetMessages,self.CID]];
    
    [self.tableView insertRowsAtIndexPaths:indexPaths withRowAnimation:UITableViewRowAnimationTop];
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationRefreshChat object:nil];
    }

@end
