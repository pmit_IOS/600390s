//
//  ZSTSexSwitch.m
//  YouYun
//
//  Created by luobin on 6/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTSexSwitch.h"

@implementation ZSTSexSwitch

@synthesize sex;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        // Initialization code
        [self addTarget:self action:@selector(changeSex) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setFrame:(CGRect)frame
{
    [super setFrame:CGRectMake(frame.origin.x, frame.origin.y, 176, 26)];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)setSex:(ZST_Sex)theSex
{
    sex = theSex;
    [self setNeedsDisplay];
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)changeSex
{
    if (self.sex == ZST_Sex_Male) {
        self.sex = ZST_Sex_Female;
    } else {
        self.sex = ZST_Sex_Male;
    }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)automaticallyNotifiesObserversForKey:(NSString *)theKey {
    
    BOOL automatic = NO;
    if ([theKey isEqualToString:@"openingBalance"]) {
        automatic = NO;
    } else {
        automatic=[super automaticallyNotifiesObserversForKey:theKey];
    }
    return automatic;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    if (sex == ZST_Sex_Male) {
        [ZSTModuleImage(@"module_snsa_switch_male.png") drawInRect:self.bounds];
    } else {
        [ZSTModuleImage(@"module_snsa_switch_famale.png") drawInRect:self.bounds];
    }
    
}


@end
