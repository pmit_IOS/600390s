//
//  ZSTChatroomMyNewsMessageViewController.h
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomMyNewsMessageViewController : UIViewController

@property (strong,nonatomic) NSArray *aboutMeArr;
@property (copy,nonatomic) NSString *circleId;

@end
