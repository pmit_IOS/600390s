//
//  ZSTMyChatroomfooterCell.m
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import "ZSTMyChatroomfooterCell.h"
#import "ZSTChatroomTopicDetailViewController.h"
#import "ZSTMyChatroomViewController.h"
#import "ZSTSNSAThumShare.h"

@implementation ZSTMyChatroomfooterCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.turnRoundBtn)
    {
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBACOLOR(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(0, 0, WIDTH, 0.5);
        [self.layer addSublayer:line];
        
        UIView *turnView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, (WIDTH - 30) / 3, 40)];
        self.turnIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 20, 20)];
        self.turnIV.image = [UIImage imageNamed:@"chatroom_share.png"];
        [turnView addSubview:self.turnIV];
        [self.contentView addSubview:turnView];
        
        NSString *titlesString = @"转";
        CGSize titlesSize = [titlesString boundingRectWithSize:CGSizeMake(MAXFLOAT, 40) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
        
        self.turnTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, titlesSize.width, 40)];
        self.turnTitleLB.textColor = RGBACOLOR(136, 136, 136, 1);
        self.turnTitleLB.textAlignment = NSTextAlignmentLeft;
        self.turnTitleLB.font = [UIFont systemFontOfSize:12.0f];
        self.turnTitleLB.text = @"转";
        [turnView addSubview:self.turnTitleLB];
        
        self.turnLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.turnTitleLB.frame), 0, turnView.bounds.size.width - CGRectGetMaxX(self.turnTitleLB.frame) - 10, 40)];
        self.turnLB.textColor = RGBACOLOR(136, 136, 136, 1);
        self.turnLB.font = [UIFont systemFontOfSize:12.0f];
        self.turnLB.textAlignment = NSTextAlignmentCenter;
        [turnView addSubview:self.turnLB];
        
        self.turnRoundBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.turnRoundBtn.frame = CGRectMake(0, 0, turnView.bounds.size.width, turnView.bounds.size.height);
        self.turnRoundBtn.tag = 101;
        [self.turnRoundBtn addTarget:self action:@selector(topicClick:) forControlEvents:UIControlEventTouchUpInside];
        [turnView addSubview:self.turnRoundBtn];
        
        
        UIView *discussView = [[UIView alloc] initWithFrame:CGRectMake(15 + (WIDTH - 30) / 3, 0, (WIDTH - 30) / 3, 40)];
        self.discussIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 20, 20)];
        self.discussIV.image = [UIImage imageNamed:@"chatroom_discuss.png"];
        [discussView addSubview:self.discussIV];
        [self.contentView addSubview:discussView];
        
        self.discussTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, titlesSize.width, 40)];
        self.discussTitleLB.textColor = RGBACOLOR(136, 136, 136, 1);
        self.discussTitleLB.textAlignment = NSTextAlignmentLeft;
        self.discussTitleLB.font = [UIFont systemFontOfSize:12.0f];
        self.discussTitleLB.text = @"评";
        [discussView addSubview:self.discussTitleLB];
        
        self.discussLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.discussTitleLB.frame), 0, turnView.bounds.size.width - CGRectGetMaxX(self.discussTitleLB.frame) - 10, 40)];
        self.discussLB.textColor = RGBACOLOR(136, 136, 136, 1);
        self.discussLB.font = [UIFont systemFontOfSize:12.0f];
        self.discussLB.textAlignment = NSTextAlignmentCenter;
        [discussView addSubview:self.discussLB];
        
        self.discussBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.discussBtn.frame = CGRectMake(0, 0, discussView.bounds.size.width, discussView.bounds.size.height);
        self.discussBtn.tag = 102;
        [self.discussBtn addTarget:self action:@selector(topicClick:) forControlEvents:UIControlEventTouchUpInside];
        [discussView addSubview:self.discussBtn];
        
        UIView *goodView = [[UIView alloc] initWithFrame:CGRectMake(15 + (WIDTH - 30) / 3 * 2, 0, (WIDTH - 30) / 3, 40)];
        self.goodIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 20, 20)];
        self.goodIV.image = [UIImage imageNamed:@"chatroom_noGood.png"];
        [goodView addSubview:self.goodIV];
        [self.contentView addSubview:goodView];
        
        self.goodTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, titlesSize.width, 40)];
        self.goodTitleLB.textColor = RGBACOLOR(136, 136, 136, 1);
        self.goodTitleLB.textAlignment = NSTextAlignmentLeft;
        self.goodTitleLB.font = [UIFont systemFontOfSize:12.0f];
        self.goodTitleLB.text = @"赞";
        [goodView addSubview:self.goodTitleLB];
        
        self.goodLB = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(self.goodTitleLB.frame), 0, turnView.bounds.size.width - CGRectGetMaxX(self.goodTitleLB.frame) - 10, 40)];
        self.goodLB.textColor = RGBACOLOR(136, 136, 136, 1);
        self.goodLB.font = [UIFont systemFontOfSize:12.0f];
        self.goodLB.textAlignment = NSTextAlignmentCenter;
        [goodView addSubview:self.goodLB];
        
        self.goodBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.goodBtn.frame = CGRectMake(0, 0, goodView.bounds.size.width, goodView.bounds.size.height);
        self.goodBtn.tag = 103;
        [self.goodBtn addTarget:self action:@selector(topicClick:) forControlEvents:UIControlEventTouchUpInside];
        [goodView addSubview:self.goodBtn];
    }
}

- (void)setCellData:(ZSTSNSAMessage *)messageDic
{
    self.messageDic = messageDic;
    
    NSString *titlesString = @"转";
    CGSize titlesSize = [titlesString boundingRectWithSize:CGSizeMake(MAXFLOAT, 40) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    if (messageDic.shareCount == 0)
    {
        self.turnLB.hidden = YES;
        
        CGFloat start = ((WIDTH - 30) / 3 - self.turnIV.bounds.size.width - titlesSize.width - 3) / 2;
        self.turnIV.frame = CGRectMake(start, self.turnIV.frame.origin.y, self.turnIV.bounds.size.width, self.turnIV.bounds.size.height);
        self.turnTitleLB.frame = CGRectMake(CGRectGetMaxX(self.turnIV.frame) + 3, self.turnTitleLB.frame.origin.y, self.turnTitleLB.bounds.size.width, self.turnTitleLB.bounds.size.height);
    }
    else
    {
        self.turnLB.hidden = NO;
        self.turnLB.text = [NSString stringWithFormat:@"%@",@(messageDic.shareCount)];
    }
    
    if (messageDic.commentCount == 0)
    {
        self.discussLB.hidden = YES;
        
        CGFloat start = ((WIDTH - 30) / 3 - self.discussIV.bounds.size.width - titlesSize.width - 3) / 2;
        self.discussIV.frame = CGRectMake(start, self.discussIV.frame.origin.y, self.discussIV.bounds.size.width, self.discussIV.bounds.size.height);
        self.discussTitleLB.frame = CGRectMake(CGRectGetMaxX(self.discussIV.frame) + 3, self.discussTitleLB.frame.origin.y, self.discussTitleLB.bounds.size.width, self.discussTitleLB.bounds.size.height);
    }
    else
    {
        self.discussLB.hidden = NO;
        if (messageDic.commentCount > 100000)
        {
            self.discussLB.text = @"10万+";
        }
        else
        {
            self.discussLB.text = [NSString stringWithFormat:@"%@",@(messageDic.commentCount)];
        }
        
        
//        NSString *discussCountString = [NSString stringWithFormat:@"%@",@(messageDic.commentCount)];
        NSString *discussCountString = messageDic.commentCount > 100000 ? @"10万+" : [NSString stringWithFormat:@"%@",@(messageDic.commentCount)];
        CGSize discussCountStringSize = [discussCountString boundingRectWithSize:CGSizeMake(MAXFLOAT, 40) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
        CGFloat start = ((WIDTH - 30) / 3 - self.discussIV.bounds.size.width - titlesSize.width - discussCountStringSize.width - 8) / 2;
        self.discussIV.frame = CGRectMake(start, self.discussIV.frame.origin.y, self.discussIV.bounds.size.width, self.discussIV.bounds.size.height);
        self.discussTitleLB.frame = CGRectMake(CGRectGetMaxX(self.discussIV.frame) + 3, self.discussTitleLB.frame.origin.y, self.discussTitleLB.bounds.size.width, self.discussTitleLB.bounds.size.height);
        self.discussLB.frame = CGRectMake(CGRectGetMaxX(self.discussTitleLB.frame) + 5, self.discussLB.frame.origin.y, discussCountStringSize.width, 40);
    }
    
    if (messageDic.thumbupCount == 0)
    {
        self.goodLB.hidden = YES;
        
        CGFloat start = ((WIDTH - 30) / 3 - self.goodIV.bounds.size.width - titlesSize.width) / 2;
        self.goodIV.frame = CGRectMake(start, self.goodIV.frame.origin.y, self.goodIV.bounds.size.width, self.goodIV.bounds.size.height);
        self.goodTitleLB.frame = CGRectMake(CGRectGetMaxX(self.goodIV.frame), self.goodTitleLB.frame.origin.y, self.goodTitleLB.bounds.size.width, self.goodTitleLB.bounds.size.height);
    }
    else
    {
        self.goodLB.hidden = NO;
        if (messageDic.thumbupCount > 100000)
        {
            self.goodLB.text = @"10万+";
        }
        else
        {
            self.goodLB.text = [NSString stringWithFormat:@"%@",@(messageDic.thumbupCount)];
        }
        
        NSString *goodCountString = messageDic.thumbupCount > 100000 ? @"10万+" : [NSString stringWithFormat:@"%@",@(messageDic.thumbupCount)];
        CGSize goodCountStringSize = [goodCountString boundingRectWithSize:CGSizeMake(MAXFLOAT, 40) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
        CGFloat start = ((WIDTH - 30) / 3 - self.goodIV.bounds.size.width - titlesSize.width - goodCountStringSize.width - 8) / 2;
        self.goodIV.frame = CGRectMake(start, self.goodIV.frame.origin.y, self.goodIV.bounds.size.width, self.goodIV.bounds.size.height);
        self.goodTitleLB.frame = CGRectMake(CGRectGetMaxX(self.goodIV.frame) + 3, self.goodTitleLB.frame.origin.y, self.goodTitleLB.bounds.size.width, self.goodTitleLB.bounds.size.height);
        self.goodLB.frame = CGRectMake(CGRectGetMaxX(self.goodTitleLB.frame) + 5, self.goodLB.frame.origin.y, goodCountStringSize.width, 40);
        
        

    }

    BOOL isHas = messageDic.isThumb == 0 ? NO : YES;
    
    if (isHas)
    {
        self.goodIV.image = [UIImage imageNamed:@"charroom_good.png"];
        self.goodBtn.selected = YES;
    }
    else
    {
        self.goodIV.image = [UIImage imageNamed:@"chatroom_noGood.png"];
        self.goodBtn.selected = NO;
    }
}

- (void)topicClick:(UIButton *)sender
{
    
    if (sender.tag == 103)
    {
        if ([self.ZSTMyChatroomfooterCellDelegate isKindOfClass:[ZSTMyChatroomViewController class]])
        {
            sender.selected = !sender.isSelected;
            if (sender.isSelected)
            {
                self.goodIV.image = [UIImage imageNamed:@"charroom_good.png"];
            }
            else
            {
                self.goodIV.image = [UIImage imageNamed:@"chatroom_noGood.png"];
            }
        }
    }
    
    
    if ([self.ZSTMyChatroomfooterCellDelegate respondsToSelector:@selector(topicFunClick:MessageDic:Btn:)])
    {
        [self.ZSTMyChatroomfooterCellDelegate topicFunClick:sender.tag MessageDic:self.messageDic Btn:sender];
    }
}


@end
