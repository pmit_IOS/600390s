//
//  ZSTChatroomReportCell.m
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import "ZSTChatroomReportCell.h"

@implementation ZSTChatroomReportCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.reportTitleLB)
    {
        self.reportTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30 - 20, 44)];
        self.reportTitleLB.textAlignment = NSTextAlignmentLeft;
        self.reportTitleLB.textColor = RGBACOLOR(56, 56, 56, 1);
        self.reportTitleLB.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.reportTitleLB];
        
        self.singleBtn = [[PMRepairButton alloc] init];
        self.singleBtn.frame = CGRectMake(WIDTH - 15 - 20, 12, 20, 20);
        [self.singleBtn setImage:[UIImage imageNamed:@"chatroom_singletonNo.png"] forState:UIControlStateNormal];
        [self.singleBtn setImage:[UIImage imageNamed:@"chatroom_singletonYes.png"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.singleBtn];
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBACOLOR(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(15, 43.5, WIDTH - 15, 0.5);
        [self.contentView.layer addSublayer:line];
        
    }
}

- (void)setCellReportTitle:(NSString *)reportTitle
{
    self.reportTitleLB.text = reportTitle;
}

@end
