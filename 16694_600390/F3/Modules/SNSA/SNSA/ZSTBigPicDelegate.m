//
//  ZSTBigPicDelegate.m
//  SNSA
//
//  Created by pmit on 15/10/13.
//
//

#import "ZSTBigPicDelegate.h"

@implementation ZSTBigPicDelegate

static ZSTBigPicDelegate *_instance;

+ (ZSTBigPicDelegate *)defaultDelegate
{
    if (!_instance)
    {
        _instance = [[ZSTBigPicDelegate alloc] init];
    }
    
    return _instance;
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    for (UIView *v in scrollView.subviews){
        return v;
    }
    return nil;
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    [scrollView setZoomScale:1.0f];
    for (UIView *iv in scrollView.subviews)
    {
        if ([iv isKindOfClass:[UIImageView class]])
        {
            iv.frame = CGRectMake(0, 0, scrollView.bounds.size.width, scrollView.bounds.size.height);
        }
    }
}

@end
