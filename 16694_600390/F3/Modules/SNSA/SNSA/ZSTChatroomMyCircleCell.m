//
//  ZSTChatroomMyCircleCell.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatroomMyCircleCell.h"

@implementation ZSTChatroomMyCircleCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.circleIV)
    {
        
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(0, 79.5, WIDTH, 0.5);
        [self.contentView.layer addSublayer:line];
        
        self.circleIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.circleIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.circleIV];
        
        self.circlTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(75, 15, WIDTH - 75 - 15 - 50, 25)];
        self.circlTitleLB.textAlignment = NSTextAlignmentLeft;
        self.circlTitleLB.font = [UIFont systemFontOfSize:14.0f];
        self.circlTitleLB.textColor = RGBACOLOR(56, 56, 56, 1);
        [self.contentView addSubview:self.circlTitleLB];
        
        self.circleIntroduceLB = [[UILabel alloc] initWithFrame:CGRectMake(75, 40, WIDTH - 75 - 15 - 50, 25)];
        self.circleIntroduceLB.textAlignment = NSTextAlignmentLeft;
        self.circleIntroduceLB.font = [UIFont systemFontOfSize:12.0f];
        self.circleIntroduceLB.textColor = RGBACOLOR(159, 159, 159, 1);
        self.circleIntroduceLB.numberOfLines = 0;
        [self.contentView addSubview:self.circleIntroduceLB];
        
        
        self.memberIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 15 - 50, 20, 50, 30)];
        self.memberIV.contentMode = UIViewContentModeScaleAspectFit;
        self.memberIV.image = [UIImage imageNamed:@"chatroom_memberYes.png"];
        [self.contentView addSubview:self.memberIV];
        
        self.memberCountLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 15 - 50, 50, 50, 20)];
        self.memberCountLB.textAlignment = NSTextAlignmentCenter;
        self.memberCountLB.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.memberCountLB];
        
        self.memberBtn = [[PMRepairButton alloc] init];
        self.memberBtn.frame = CGRectMake(WIDTH - 15 - 40, 15, 40, 40);
        [self.memberBtn addTarget:self action:@selector(goToCircleMember:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.memberBtn];
        
        self.waitCareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.waitCareBtn.frame = CGRectMake(WIDTH - 15 - 50, 25, 50, 30);
        self.waitCareBtn.layer.borderColor = RGBA(248, 145, 70, 1).CGColor;
        self.waitCareBtn.layer.borderWidth = 0.5;
        self.waitCareBtn.layer.cornerRadius = 4.0f;
        [self.waitCareBtn setTitle:@"关注" forState:UIControlStateNormal];
        [self.waitCareBtn setTitleColor:RGBA(248, 145, 70, 1) forState:UIControlStateNormal];
        self.waitCareBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        [self.waitCareBtn addTarget:self action:@selector(careCircle:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.waitCareBtn];
    }
}

- (void)setCellData:(NSDictionary *)myCircleDic
{
    self.circleDic = myCircleDic;
    [self.circleIV setImageWithURL:[NSURL URLWithString:[myCircleDic safeObjectForKey:@"CImgUrl"]]];
    self.circlTitleLB.text = [myCircleDic safeObjectForKey:@"CName"];
    self.circleIntroduceLB.text = [myCircleDic safeObjectForKey:@"Description"];
    NSString *memberCount = [NSString stringWithFormat:@"%@",[myCircleDic safeObjectForKey:@"UserCount"]];
    self.memberCountLB.text = memberCount;
    
    if ([[myCircleDic safeObjectForKey:@"Description"] isEqualToString:@""])
    {
        self.circleIntroduceLB.hidden = YES;
        self.circlTitleLB.frame = CGRectMake(self.circlTitleLB.frame.origin.x, self.circlTitleLB.frame.origin.y, self.circlTitleLB.frame.size.width, 50);
    }
    else
    {
        self.circleIntroduceLB.hidden = NO;
        self.circlTitleLB.frame = CGRectMake(self.circlTitleLB.frame.origin.x, self.circlTitleLB.frame.origin.y, self.circlTitleLB.frame.size.width, 25);
    }
    
    if ([[myCircleDic safeObjectForKey:@"IsDefaultCircle"] boolValue])
    {
        self.memberIV.image = [UIImage imageNamed:@"chatroom_memberNo.png"];
        self.memberCountLB.textColor = RGBA(204, 204, 204, 1);
        self.memberBtn.enabled = NO;
    }
    else
    {
        self.memberIV.image = [UIImage imageNamed:@"chatroom_memberYes.png"];
        self.memberCountLB.textColor = RGBA(248, 145, 70, 1);
        self.memberBtn.enabled = YES;
    }
    
    if ([[myCircleDic safeObjectForKey:@"AuditStatus"] integerValue] != 1)
    {
        self.waitCareBtn.hidden = NO;
        self.memberBtn.hidden = YES;
        self.memberCountLB.hidden = YES;
        self.memberIV.hidden = YES;
        
        if ([[myCircleDic safeObjectForKey:@"AuditStatus"] integerValue] == 2)
        {
            self.waitCareBtn.layer.borderColor = RGBA(204, 204, 204, 1).CGColor;
            [self.waitCareBtn setTitle:@"待审核" forState:UIControlStateNormal];
            [self.waitCareBtn setTitleColor:RGBA(204, 204, 204, 1) forState:UIControlStateNormal];
        }
        else if ([[myCircleDic safeObjectForKey:@"AuditStatus"] integerValue] == 3)
        {
            self.waitCareBtn.layer.borderColor = RGBA(204, 204, 204, 1).CGColor;
            [self.waitCareBtn setTitle:@"驳回" forState:UIControlStateNormal];
            [self.waitCareBtn setTitleColor:RGBA(204, 204, 204, 1) forState:UIControlStateNormal];
        }
        else
        {
            self.waitCareBtn.layer.borderColor = RGBA(248, 145, 70, 1).CGColor;
            [self.waitCareBtn setTitle:@"关注" forState:UIControlStateNormal];
            [self.waitCareBtn setTitleColor:RGBA(248, 145, 70, 1) forState:UIControlStateNormal];
        }
        
    }
    else
    {
        self.waitCareBtn.hidden = YES;
        self.memberBtn.hidden = NO;
        self.memberCountLB.hidden = NO;
        self.memberIV.hidden = NO;
    }
}

- (void)goToCircleMember:(PMRepairButton *)sender
{
    if ([self.circleCellDelegate respondsToSelector:@selector(goToMember:)])
    {
        [self.circleCellDelegate goToMember:self.circleDic];
    }
}

- (void)careCircle:(PMRepairButton *)sender
{
    if ([self.circleCellDelegate respondsToSelector:@selector(goToCareThisCircle:)])
    {
        [self.circleCellDelegate goToCareThisCircle:self.circleDic];
    }
}

@end
