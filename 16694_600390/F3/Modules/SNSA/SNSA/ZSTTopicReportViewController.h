//
//  ZSTTopicReportViewController.h
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import <UIKit/UIKit.h>

@protocol ZSTTopicReportViewControllerDelegate <NSObject>

- (void)sendReportResult:(NSString *)result;

@end

@interface ZSTTopicReportViewController : UIViewController

@property (copy,nonatomic) NSString *circleId;
@property (copy,nonatomic) NSString *msgId;
@property (copy,nonatomic) NSString *reason;
@property (weak,nonatomic) id<ZSTTopicReportViewControllerDelegate> reportDelegate;


@end
