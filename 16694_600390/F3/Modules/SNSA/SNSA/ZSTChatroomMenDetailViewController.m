//
//  ZSTChatroomMenDetailViewController.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatroomMenDetailViewController.h"
#import "ZSTChatroomMenDetailCell.h"
#import <PMRepairButton.h>
#import <AddressBook/AddressBook.h>
#import <MessageUI/MessageUI.h>
#import "ZSTYouYunEngine.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <SDWebImageManager.h>

@interface ZSTChatroomMenDetailViewController () <UITableViewDataSource,UITableViewDelegate,MFMessageComposeViewControllerDelegate,ZSTYouYunEngineDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UIView *navBackView;
@property (strong,nonatomic) UITableView *menDetailTableView;
@property (strong,nonatomic) NSArray *detailArr;
@property (strong,nonatomic) NSArray *btnTitleArr;
@property (strong,nonatomic) NSArray *btnImgArr;
@property (strong,nonatomic) UIImageView *headerIV;
@property (strong,nonatomic) UILabel *introduceLB;
@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (strong,nonatomic) NSDictionary *menDetailDic;
@property (strong,nonatomic) NSArray *keyArr;
@property (strong,nonatomic) UILabel *headerNameLB;
@property (strong,nonatomic) UIImageView *sexIV;


@end

@implementation ZSTChatroomMenDetailViewController

static NSString *const detailCell = @"detailCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    
    self.detailArr = @[@"职业",@"公司",@"学校",@"所在地",@"家乡",@"电话"];
    self.btnTitleArr = @[@"电话",@"短信",@"保存"];
    self.btnImgArr = @[@"chatroom_menPhone.png",@"chatroom_menSMS.png",@"chatroom_menSave.png"];
    self.keyArr = @[@"Occupation",@"Company",@"School",@"Address",@"Hometown",@"Msisdn"];
    
    
//    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.navigationController.navigationBarHidden = YES;
    [self buildHeaderView];
    [self buildTableView];
    [self buildFooterView];
    [self getMenDetailMessage];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [self getBackView:self.navigationController.navigationBar];
}



#pragma mark - 固定头像
- (void)buildHeaderView
{
    UIImageView *headerBgIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, -10, WIDTH, WidthRate(480))];
    headerBgIV.image = [UIImage imageNamed:@"module_personal_personBg.png"];
    headerBgIV.userInteractionEnabled = YES;
    self.menDetailTableView.tableHeaderView = headerBgIV;
    
    self.headerIV = [[UIImageView alloc] init];
    self.headerIV.center = CGPointMake(WIDTH / 2, WidthRate(200));
    self.headerIV.bounds = (CGRect){CGPointZero,{WidthRate(240),WidthRate(240)}};
    self.headerIV.layer.masksToBounds = YES;
    self.headerIV.layer.cornerRadius = WidthRate(120);
    self.headerIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
    [headerBgIV addSubview:self.headerIV];
    
    // 返回按钮
    PMRepairButton *backBtn = [[PMRepairButton alloc] init];
    backBtn.frame = CGRectMake(WidthRate(20), WidthRate(60), WidthRate(60), WidthRate(60));
    [backBtn setImage:[UIImage imageNamed:@"module_personal_back.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [headerBgIV addSubview:backBtn];
    
    // 性别图片
    self.sexIV = [[UIImageView alloc] init];
    [headerBgIV addSubview:self.sexIV];
    self.sexIV.hidden = NO;
    
    // 名称
    self.headerNameLB = [[UILabel alloc] init];
    self.headerNameLB.backgroundColor = [UIColor clearColor];
    self.headerNameLB.textColor = [UIColor whiteColor];
    self.headerNameLB.textAlignment = NSTextAlignmentCenter;
    self.headerNameLB.font = [UIFont boldSystemFontOfSize:16.0f];
    self.headerNameLB.shadowColor = [UIColor darkGrayColor];
    self.headerNameLB.shadowOffset = CGSizeMake(1.0f, 1.0f);
    [headerBgIV addSubview:self.headerNameLB];
    self.headerNameLB.hidden = NO;
    
    UIView *clearView = [[UIView alloc] initWithFrame:CGRectMake(0, WidthRate(480) - WidthRate(60), WIDTH, WidthRate(60))];
    clearView.backgroundColor = [UIColor clearColor];
    [headerBgIV addSubview:clearView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, clearView.bounds.size.width, clearView.bounds.size.height)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.7;
    [clearView addSubview:alphaView];
    
    self.introduceLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, clearView.bounds.size.width - 30, clearView.bounds.size.height)];
    self.introduceLB.font = [UIFont systemFontOfSize:12.0f];
    self.introduceLB.textColor = [UIColor whiteColor];
    self.introduceLB.textAlignment = NSTextAlignmentLeft;
    self.introduceLB.text = @"这个人很懒，什么都没有写";
    [clearView addSubview:self.introduceLB];
    
    [self.view addSubview:headerBgIV];
}

- (void)getMenDetailMessage
{
//    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在加载个人信息，请稍后...", nil) withImage:nil];
    [self.engine getUserInfoWithMsisdn:self.chatMenOrigineMsisdn userId:@""];
}

- (void)buildTableView
{
    self.menDetailTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, WidthRate(460), WIDTH, HEIGHT - 30 - WidthRate(460)) style:UITableViewStylePlain];
    self.menDetailTableView.delegate = self;
    self.menDetailTableView.dataSource = self;
    self.menDetailTableView.separatorColor = UITableViewCellSeparatorStyleNone;
    [self.menDetailTableView registerClass:[ZSTChatroomMenDetailCell class] forCellReuseIdentifier:detailCell];
    [self.menDetailTableView setSeparatorColor:[UIColor whiteColor]];
    self.menDetailTableView.backgroundColor = RGBA(243, 243, 243, 1);
    [self.view addSubview:self.menDetailTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBA(243, 243, 243, 1);
    self.menDetailTableView.tableFooterView = footerView;
}

- (void)buildFooterView
{
    UIView *footerActionView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 50, WIDTH, 50)];
    footerActionView.backgroundColor = RGBA(246, 145, 78, 1);
    [self.view addSubview:footerActionView];
    
    for (NSInteger i = 0; i < self.btnTitleArr.count; i++)
    {
        UIView *btnBgView = [[UIView alloc] initWithFrame:CGRectMake(i * WIDTH / 3, 0, WIDTH / 3, 50)];
        btnBgView.backgroundColor = [UIColor clearColor];
        [footerActionView addSubview:btnBgView];
        
        UIImageView *btnIV = [[UIImageView alloc] initWithFrame:CGRectMake(20, 10, 30, 30)];
        btnIV.image = [UIImage imageNamed:self.btnImgArr[i]];
        btnIV.contentMode = UIViewContentModeScaleAspectFit;
        [btnBgView addSubview:btnIV];
        
        UILabel *btnTitle = [[UILabel alloc] initWithFrame:CGRectMake(55, 10, WIDTH / 3 - 55, 30)];
        btnTitle.text = self.btnTitleArr[i];
        btnTitle.textColor = [UIColor whiteColor];
        btnTitle.font = [UIFont systemFontOfSize:14.0f];
        [btnBgView addSubview:btnTitle];
        
        UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
        btn.frame = CGRectMake(0, 0, btnBgView.bounds.size.width, btnBgView.bounds.size.height);
        btn.tag = 100 + i;
        [btn addTarget:self action:@selector(doSomeAction:) forControlEvents:UIControlEventTouchUpInside];
        [btnBgView addSubview:btn];
        
    }
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = [UIColor whiteColor].CGColor;
    line.frame = CGRectMake(WIDTH / 3 - 1, 10, 1, 30);
    [footerActionView.layer addSublayer:line];
    
    CALayer *line2 = [CALayer layer];
    line2.backgroundColor = [UIColor whiteColor].CGColor;
    line2.frame = CGRectMake(WIDTH / 3 * 2 - 2, 10, 1, 30);
    [footerActionView.layer addSublayer:line2];
    
}

- (void)doSomeAction:(UIButton *)sender
{
    switch (sender.tag)
    {
        case 100:
        {
            UIAlertView *phoneAlert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"拨打电话", nil) message:[NSString stringWithFormat:@"是否拨打 %@",self.chatMenMsisdn] delegate:self cancelButtonTitle:NSLocalizedString(@"否",nil) otherButtonTitles:NSLocalizedString(@"是",nil), nil];
            phoneAlert.tag = 9998;
            [phoneAlert show];
        }
            break;
        case 101:
        {
            if( [MFMessageComposeViewController canSendText] ){
                
                MFMessageComposeViewController * controller = [[MFMessageComposeViewController alloc]init]; //autorelease];
                
                controller.recipients = [NSArray arrayWithObject:self.chatMenOrigineMsisdn];
                controller.messageComposeDelegate = self;
                
                [self presentViewController:controller animated:YES completion:nil];
                
//                [[[[controller viewControllers] lastObject] navigationItem] setTitle:@"测试短信"];//修改短信界面标题
            }
        }
            break;
        case 102:
        {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"保存号码", nil) message:[NSString stringWithFormat:@"是否将 %@ 保存到通讯录中",[self.menDetailDic safeObjectForKey:@"Name"]] delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
            alert.tag = 9999;
            [alert show];
        }
            break;
            
        default:
            break;
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult)result{
    
//    [controller dismissModalViewControllerAnimated:NO];//关键的一句   不能为YES
    [controller dismissViewControllerAnimated:NO completion:nil];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.detailArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomMenDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:detailCell];
    NSString *key = self.keyArr[indexPath.row];
    NSString *thisValue = @"";
    if (![self.menDetailDic safeObjectForKey:key] || [[self.menDetailDic safeObjectForKey:key] isEqualToString:@""])
    {
        thisValue = @"未设置";
    }
    else
    {
        thisValue = [self.menDetailDic safeObjectForKey:key];
    }
    
    [cell createUI];
    [cell setDetailCellData:self.detailArr[indexPath.row] RightTitle:thisValue];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (void)addNewsChatMan
{
//    ABAddressBookRef iPhoneAddressBook = ABAddressBookCreate();
    ABAddressBookRef addressBook = nil;
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 6.0)
    {
        addressBook = ABAddressBookCreateWithOptions(NULL, NULL);
        dispatch_semaphore_t sema = dispatch_semaphore_create(0);//发出访问通讯录的请求
        ABAddressBookRequestAccessWithCompletion(addressBook, ^(bool granted, CFErrorRef error){

            dispatch_semaphore_signal(sema);});
        dispatch_semaphore_wait(sema, DISPATCH_TIME_FOREVER);
    }
    else{
        addressBook = ABAddressBookCreate();
    }
    
    ABRecordRef newPerson = ABPersonCreate();
    __block CFErrorRef saveError = NULL;
    ABRecordSetValue(newPerson, kABPersonFirstNameProperty, (__bridge CFTypeRef)([self.menDetailDic safeObjectForKey:@"Name"]), &saveError);
    ABMutableMultiValueRef multiPhone = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABMultiValueAddValueAndLabel(multiPhone, (__bridge CFTypeRef)([self.menDetailDic safeObjectForKey:@"Msisdn"]), kABPersonPhoneMobileLabel, NULL);
    ABRecordSetValue(newPerson, kABPersonPhoneProperty, multiPhone, &saveError);
    CFRelease(multiPhone);
    //email
    ABMutableMultiValueRef multiEmail = ABMultiValueCreateMutable(kABMultiStringPropertyType);
    ABRecordSetValue(newPerson, kABPersonEmailProperty, multiEmail, &saveError);
    CFRelease(multiEmail);
    if (![self.menDetailDic safeObjectForKey:@"Icon"] || [[self.menDetailDic safeObjectForKey:@"Icon"] isEqualToString:@""])
    {

    }
    else
    {
//        UIImage *headerImg = [[SDWebImageManager sharedManager] imageWithURL:[NSURL URLWithString:[self.menDetailDic safeObjectForKey:@"Icon"]]];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[self.menDetailDic safeObjectForKey:@"Icon"]] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
            NSData *dataRef = UIImagePNGRepresentation(image);
            ABPersonSetImageData(newPerson, (__bridge CFDataRef)dataRef, &saveError);
            
        }];
        
        
    }
    
    ABAddressBookAddRecord(addressBook, newPerson, &saveError);
    if(ABAddressBookSave(addressBook, &saveError))
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"保存成功", nil) withImage:nil];
    }
}

- (void)backButtonAction
{
    [self popViewController];
}

- (void)getUserInfoDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    self.menDetailDic = [response safeObjectForKey:@"data"];
    if (![self.menDetailDic safeObjectForKey:@"Icon"] || [[self.menDetailDic safeObjectForKey:@"Icon"] isEqualToString:@""])
    {
        self.headerIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
    }
    else
    {
        [self.headerIV setImageWithURL:[NSURL URLWithString:[self.menDetailDic safeObjectForKey:@"Icon"]] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    }
    
    if (![self.menDetailDic safeObjectForKey:@"Signature"] || [[self.menDetailDic safeObjectForKey:@"Signature"] isEqualToString:@""])
    {
        self.introduceLB.text = @"这个人很懒，什么都没有写";
    }
    else
    {
        self.introduceLB.text = [self.menDetailDic safeObjectForKey:@"Signature"];
    }
    
    NSString *name = @"";
    
    name = [self.menDetailDic safeObjectForKey:@"Name"];
    if (name.length == 0 || [self validateMobile:name])
    {
        name = @"佚名";
    }
    
    CGSize nameSize = [name boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0f]} context:nil].size;
    
    self.headerNameLB.frame = CGRectMake((WIDTH - nameSize.width) / 2 + WidthRate(20), self.headerIV.frame.origin.y + self.headerIV.frame.size.height + WidthRate(30), nameSize.width, WidthRate(50));
    self.headerNameLB.text = name;
    
    NSInteger sexInteger = [[self.menDetailDic safeObjectForKey:@"Sex"] integerValue];
    self.sexIV.frame = CGRectMake(self.headerNameLB.frame.origin.x - WidthRate(40), self.headerIV.frame.origin.y + self.headerIV.frame.size.height + WidthRate(40), WidthRate(30), WidthRate(30));
    if (sexInteger == 1) {
        self.sexIV.image = [UIImage imageNamed:@"module_personal_sex0.png"];
    }
    else {
        self.sexIV.image = [UIImage imageNamed:@"module_personal_sex1.png"];
    }
    
    [self.menDetailTableView reloadData];
}

- (void)getUserInfoDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[2378]|7[0-9]|4[0-9])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        if (alertView.tag == 9998)
        {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"tel://%@",self.chatMenOrigineMsisdn]]];
        }
        else
        {
            [self addNewsChatMan];
        }
        
    }
}

@end
