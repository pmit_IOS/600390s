//
//  ZSTSNSAThumShare.m
//  SNSA
//
//  Created by pmit on 15/9/25.
//
//

#import "ZSTSNSAThumShare.h"

@implementation ZSTSNSAThumShare

- (instancetype)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init])
    {
        self.addtime = [[aDecoder decodeObjectForKey:@"addtime"] integerValue];
        self.headPhoto = [aDecoder decodeObjectForKey:@"headphoto"];
        self.mcid = [[aDecoder decodeObjectForKey:@"mcid"] integerValue];
        self.mid = [[aDecoder decodeObjectForKey:@"mid"] integerValue];
        self.msisdn = [aDecoder decodeObjectForKey:@"msisdn"];
        self.uid = [aDecoder decodeObjectForKey:@"uid"];
        self.uname = [aDecoder decodeObjectForKey:@"uname"];
        self.circleId = [aDecoder decodeObjectForKey:@"circleId"];
    }
    
    return self;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"addtime"] forKey:@"addtime"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"headphoto"] forKey:@"headphoto"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"mcid"] forKey:@"mcid"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"mid"] forKey:@"mid"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"msisdn"] forKey:@"msisdn"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"uid"] forKey:@"uid"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"uname"] forKey:@"uname"];
    [aCoder encodeObject:[self.thumbDic safeObjectForKey:@"circleId"] forKey:@"circleId"];

}

- (id)initWithDic:(NSDictionary *)dic
{
    self = [super init];
    if (self)
    {
        self.thumbDic = dic;
        self.addtime = [[dic safeObjectForKey:@"addtime"] integerValue];
        self.headPhoto = [dic safeObjectForKey:@"headphoto"];
        self.mcid = [[dic safeObjectForKey:@"mcid"] integerValue];
        self.mid = [[dic safeObjectForKey:@"mid"] integerValue];
        self.msisdn = [dic safeObjectForKey:@"msisdn"];
        self.uid = [dic safeObjectForKey:@"uid"];
        self.uname = [dic safeObjectForKey:@"uname"];
        self.circleId = [dic safeObjectForKey:@"circleId"];
    }
    
    return self;
}

@end
