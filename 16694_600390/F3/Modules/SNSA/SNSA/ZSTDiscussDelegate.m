//
//  ZSTDiscussDelegate.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTDiscussDelegate.h"
#import "ZSTTopicDetailCommentCell.h"
#import "ZSTSNSAMessageComments.h"

@implementation ZSTDiscussDelegate

static NSString *const discussCell = @"discussCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentArr.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSNSAMessageComments *comments = self.commentArr[indexPath.row];
    NSString *content = comments.commentContent;
    CGSize contentSize = [content boundingRectWithSize:CGSizeMake(WIDTH - 60 - 15, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    return 60 + contentSize.height + 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTTopicDetailCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:discussCell];
    [cell createUI];
    ZSTSNSAMessageComments *comments = self.commentArr[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setCellData:comments];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSNSAMessageComments *selectedComments = self.commentArr[indexPath.row];
    if ([self.discussFunDelegate respondsToSelector:@selector(commentToComments:)])
    {
        [self.discussFunDelegate commentToComments:selectedComments];
    }
}


@end
