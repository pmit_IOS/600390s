//
//  ZSTChatroomMenViewController.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import <UIKit/UIKit.h>

@class ZSTChatHomeViewController;

@interface ZSTChatroomMenViewController : UIViewController

@property (strong,nonatomic) NSDictionary *circleDic;
@property (weak,nonatomic) ZSTChatHomeViewController *homeVC;

@end
