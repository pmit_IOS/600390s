//
//  ZSTNewsTopicViewController.h
//  SNSA
//
//  Created by pmit on 15/9/14.
//
//

#import <UIKit/UIKit.h>
#import "ZSTYouYunEngine.h"

@protocol ZSTNewsTopicViewControllerDelegate <NSObject>

@optional
- (void)sendNewsTopicFinish:(NSArray *)imgDataArr Content:(NSString *)content imgString:(NSString *)imgUrlString MID:(NSString *)MID;
- (void)sendNewsTopicJump:(NSArray *)imageDataArr Content:(NSString *)content;

@end

@interface ZSTNewsTopicViewController : UIViewController

@property (nonatomic, strong) NSMutableArray *photoMenuItems;
@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (strong,nonatomic) NSMutableArray *imageDataArr;
@property (strong,nonatomic) NSArray *roomArrayOK;
@property (copy,nonatomic) NSString *circleId;
@property (weak,nonatomic) id<ZSTNewsTopicViewControllerDelegate> newsTopicDelegate;
@property (strong,nonatomic) NSArray *takePhotoArr;
@property (assign,nonatomic) BOOL isFromDeal;

- (void)chatUploadImage:(NSArray *)arrayOK;




@end
