//
//  ZSTChatroomMenDetailViewController.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomMenDetailViewController : UIViewController

@property (copy,nonatomic) NSString *chatMenMsisdn;
@property (copy,nonatomic) NSString *chatMenOrigineMsisdn;

@end
