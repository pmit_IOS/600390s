//
//  ZSTReasonInputCell.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import <UIKit/UIKit.h>

@interface ZSTReasonInputCell : UITableViewCell <UITextViewDelegate>

@property (strong,nonatomic) UITextView *inputTV;
@property (strong,nonatomic) UILabel *backUpLB;

- (void)createUI;

@end
