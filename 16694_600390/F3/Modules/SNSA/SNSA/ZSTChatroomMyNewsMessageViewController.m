//
//  ZSTChatroomMyNewsMessageViewController.m
//  SNSA
//
//  Created by pmit on 15/9/18.
//
//

#import "ZSTChatroomMyNewsMessageViewController.h"
#import <ZSTUtils.h>
#import "ZSTMyNewsMessageCell.h"
#import "ZSTDao+SNSA.h"
#import "ZSTChatroomTopicDetailViewController.h"
#import "ZSTYouYunEngine.h"
#import "ZSTSNSAMessage.h"
#import <BaseNavgationController.h>

@interface ZSTChatroomMyNewsMessageViewController () <UITableViewDelegate,UITableViewDataSource,ZSTYouYunEngineDelegate>

@property (strong,nonatomic) UITableView *newsMyMessageTableView;
@property (strong,nonatomic) ZSTYouYunEngine *engine;

@end

@implementation ZSTChatroomMyNewsMessageViewController

static NSString *const myNewsMessageCell = @"myNewsMessageCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.dao = [[ZSTDao alloc] init];
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"消息", nil)];
    [self buildTableView];
    
    [self.engine getAboutMe:self.circleId];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildTableView
{
    self.newsMyMessageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.newsMyMessageTableView.delegate = self;
    self.newsMyMessageTableView.dataSource = self;
    self.newsMyMessageTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.newsMyMessageTableView setSeparatorColor:[UIColor whiteColor]];
    [self.newsMyMessageTableView registerClass:[ZSTMyNewsMessageCell class] forCellReuseIdentifier:myNewsMessageCell];
    [self.view addSubview:self.newsMyMessageTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.aboutMeArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTMyNewsMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:myNewsMessageCell];
    [cell createUI];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell setCellData:self.aboutMeArr[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSString *mid = [NSString stringWithFormat:@"%@", [self.aboutMeArr[indexPath.row] safeObjectForKey:@"mid"]];
    
    NSArray *topicArr = [self.dao selectOneTopic:mid];
    if (topicArr.count == 0)
    {
        [self.engine getOneTopic:mid];
        
    }
    else
    {
        NSDictionary *messageDic = topicArr[0];
        ZSTSNSAMessage *topicMessage = [[ZSTSNSAMessage alloc] initWithMessageDicBySqlite:messageDic];
        ZSTChatroomTopicDetailViewController *detailVC = [[ZSTChatroomTopicDetailViewController alloc] init];
        UINavigationController *detailNav = [[UINavigationController alloc] initWithRootViewController:detailVC];
        detailVC.circleId = self.circleId;
        detailVC.messageDic = topicMessage;
        detailVC.isDiscuss = [[self.aboutMeArr[indexPath.row] safeObjectForKey:@"type"] integerValue] == 1 ? NO : YES;
        detailVC.isFromNews = YES;
        NSDictionary *aboutDic = self.aboutMeArr[indexPath.row];
        if ([[aboutDic safeObjectForKey:@"type"] integerValue] == 0)
        {
            detailVC.enterType = @"discuss";
        }
        else
        {
            detailVC.enterType = @"good";
        }

        detailNav.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
        [self.navigationController presentViewController:detailNav animated:YES completion:nil];
//        [self.navigationController pushViewController:detailVC animated:YES];
    }
    
//    ZSTChatroomTopicDetailViewController *detailVC = [[ZSTChatroomTopicDetailViewController alloc] init];
//    detailVC.topicId = mid;
//    detailVC.circleId = self.circleId;
//    
//    [self animationWithView:detailVC.view WithAnimationTransition:UIViewAnimationTransitionFlipFromRight];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void) animationWithView : (UIView *)view WithAnimationTransition : (UIViewAnimationTransition) transition
{
    [UIView animateWithDuration:0.7f animations:^{
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        [UIView setAnimationTransition:transition forView:view cache:YES];
    }];
}

- (void)getOneTopicDidSuccess:(NSDictionary *)response
{
//    NSDictionary *messageDic = [[response safeObjectForKey:@"data"] firstObject];
    NSDictionary *messageDic = [response safeObjectForKey:@"data"];
    ZSTSNSAMessage *message = [[ZSTSNSAMessage alloc] initWithMessageDic:messageDic];
    ZSTChatroomTopicDetailViewController *detailVC = [[ZSTChatroomTopicDetailViewController alloc] init];
    detailVC.circleId = self.circleId;
    detailVC.messageDic = message;
    detailVC.modalTransitionStyle = UIModalTransitionStyleFlipHorizontal;
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)getAboutMeDidSuccess:(NSDictionary *)response
{
    self.aboutMeArr = [response safeObjectForKey:@"data"];
    [self.newsMyMessageTableView reloadData];
    [self.engine getMessageHasRead:self.circleId];
}

- (void)getAboutMeDidFailure:(NSString *)resultString
{
    
}


@end
