//
//  ZSTTopicThumbCell.m
//  SNSA
//
//  Created by pmit on 15/10/22.
//
//

#import "ZSTTopicThumbCell.h"

@implementation ZSTTopicThumbCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.thunbBgView)
    {
        self.thunbBgView = [[UIView alloc] initWithFrame:CGRectMake(15, 0, WIDTH - 30, 15)];
        self.thunbBgView.backgroundColor = RGBA(243, 243, 243, 1);
        [self.contentView addSubview:self.thunbBgView];
        
        UIImageView *loveIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 2.5, 15, 15)];
        loveIV.contentMode = UIViewContentModeScaleAspectFit;
        loveIV.image = [UIImage imageNamed:@"chatroom_noGood.png"];
        [self.thunbBgView addSubview:loveIV];
        
        self.goodMenLB = [[TTTAttributedLabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(loveIV.frame) + 2.5, 2.5, self.thunbBgView.bounds.size.width - CGRectGetMaxX(loveIV.frame) - 2.5, 20)];
        self.goodMenLB.font = [UIFont systemFontOfSize:13];
        self.goodMenLB.textColor = RGBA(205, 154, 116, 1);
        self.goodMenLB.backgroundColor = RGBA(243, 243, 243, 1);
        self.goodMenLB.lineBreakMode = NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading;
        self.goodMenLB.numberOfLines = 0;
        self.goodMenLB.linkAttributes = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:NO] forKey:(NSString *)kCTUnderlineStyleAttributeName];
        [self.thunbBgView addSubview:self.goodMenLB];
        
    }
}

- (void)setCellData:(ZSTSNSAMessage *)message
{
    NSMutableArray *goodNameArr = [NSMutableArray array];
    
    if (message.thumArr.count == 0)
    {
        self.thunbBgView.hidden = YES;
    }
    else
    {
        self.thunbBgView.hidden = NO;
        for (NSInteger i = 0; i < message.thumArr.count; i++)
        {
            ZSTSNSAThumShare *thumbShare = message.thumArr[i];
            NSString *oneName = [thumbShare.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : thumbShare.uname;
            [goodNameArr addObject:oneName];
        }
        
        NSString *showMenNameString = [goodNameArr componentsJoinedByString:@","];
        
        [self.goodMenLB setText:showMenNameString afterInheritingLabelAttributesAndConfiguringWithBlock:^NSMutableAttributedString *(NSMutableAttributedString *mutableAttributedString) {
            
            
            
            return mutableAttributedString;
        }];
        
        for (ZSTSNSAThumShare *thumbShare in message.thumArr)
        {
            NSString *oneName = [thumbShare.uid integerValue] == [[ZSTF3Preferences shared].UID integerValue] ? @"我" : thumbShare.uname;
            NSRange range = [showMenNameString rangeOfString:oneName];
            [self.goodMenLB addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://goToMenDetailById=%@",thumbShare.msisdn]] withRange:range];
        }
        
        CGSize menNameSize = [showMenNameString boundingRectWithSize:CGSizeMake(self.goodMenLB.bounds.size.width, MAXFLOAT) options: NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0f]} context:nil].size;
        self.goodMenLB.frame = CGRectMake(self.goodMenLB.frame.origin.x, self.goodMenLB.frame.origin.y, self.goodMenLB.bounds.size.width, menNameSize.height);
        self.thunbBgView.frame = CGRectMake(self.thunbBgView.frame.origin.x, self.thunbBgView.frame.origin.y, self.thunbBgView.bounds.size.width, menNameSize.height + 5);
        
    }
    
}

@end
