//
//  ZSTMyChatroomViewController.h
//  SNSA
//
//  Created by pmit on 15/10/14.
//
//

#import <UIKit/UIKit.h>

@interface ZSTMyChatroomViewController : UIViewController

@property (copy,nonatomic) NSString *chatroomTitle;
@property (copy,nonatomic) NSString *circleId;

- (void)uploadImageAndJump:(NSArray *)arrayOK;

@end
