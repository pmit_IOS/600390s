//
//  ZSTContactDetailHeaderCellCell.h
//  YouYun
//
//  Created by luobin on 6/29/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FXLabel;
@class ZSTSexSwitch;

@interface ZSTContactDetailHeaderCell : UITableViewCell

@property (nonatomic, retain, readonly) TKAsynImageView *avtarImageView;
@property (nonatomic, retain, readonly) FXLabel *nameLabel;
@property (nonatomic, retain, readonly) UIImageView *adminImageView;
@property (nonatomic, retain, readonly) UIImageView *stateImageView;
@property (nonatomic, retain, readonly) UILabel *locationLabel;
@property (nonatomic, retain, readonly) ZSTSexSwitch *sexSwitch;

@end
