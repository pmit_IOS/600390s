//
//  CommonFunc.h
//  Ancient
//
//  Created by zj l on 12-4-9.
//  Copyright 2012年 YollerGames. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "TKDataCache.h"
#import "ZSTSNSCommunicator+YouYun.h"
#import "pinyin.h"
//#import "UserDetailVO.h"
#import "Reachability.h"
#import "ZSTChatBubble.h"

typedef enum
{
    LeftSilde = 1, //左侧滑
    MainList, //主列表
    ChatList, //聊天列表
    PeopleList, //人员列表
    isRegUser //用户进来时，之前是否注册过
} AlertTips;
#define kFaceSize 18 //表情尺寸
#define kCommentViewTextSize 300 //评论界面文字宽度

@interface CommonFunc : NSObject {
    NSMutableDictionary *isDisplayDic;
//    UINavigationController *nav;
}
@property (nonatomic, retain) UINavigationController *nav;
@property (nonatomic, retain) NSMutableDictionary *isDisplayDic;
@property (nonatomic, retain) NSArray *faceTilteArr; //表情名称数组
+ (CommonFunc *) sharedCommon;
+(NSString *)getShort:(NSString *)number;

-(NSString *) stringForHourValue: (int) sec;//change seconds to ccstring with format of "00:00:00"
//根据url，指定宽度，算出动态高度
-(NSInteger)getHeightByURL:(NSString *)url assignWidth:(NSInteger)assignWidth;
-(CGSize)getSizeByURL:(NSString *)url assignWidth:(CGFloat)assignWidth;
//根据原image和url，返回裁剪后的image
-(UIImage *)getHeadImage:(UIImage*)image withURL:(NSString*)url;
//排序 
-(NSArray*)sortArrInKey:(NSArray*)arr;

//cache保存用户信息到本地
-(void)updateUid:(NSString *)uid;
-(NSString*)getCurrentUid;

-(void)updateMyMessageId:(NSString *)mid;
-(NSString*)getMyMessageId;

-(void)updateMyCommentId:(NSString *)cid;
-(NSString*)getMyCommentId;

-(void)updateMyMetionId:(NSString *)mid;
-(NSString*)getMyMetionId;

//cache保存用户信息到本地
-(void)updateUVO:(NSDictionary *)uvo; //存自定义类型不管用，得是oc带的类型
-(NSDictionary *)getCurrentUVO;

//cache存取圈子列表
-(void)updateCircles:(NSArray*) arr;
-(NSDictionary*)getCircleByCID:(NSString*)cid;
//首字母
-(BOOL)searchResult:(NSString *)contactName searchText:(NSString *)searchT;
-(NSString *)getFirstLetter:(NSString*)sectionName;
//navigation bar
- (id)customControllerWithRootViewController:(UIViewController *)root;
//网络状态
-(BOOL)isExistNetWork;
//新版本提示，每个版本是否有提示过
-(BOOL)isAlertNewTips:(AlertTips)tips;
-(void)saveAlertNewTips:(AlertTips)tips;
//表情坐标
-(NSArray*)getFaceOrigin:(NSString *)message maxWidth:(CGFloat )maxWidth hasPic:(BOOL)hasPic;
//把[face00X]换成空格
-(NSString *)getDisplayStr:(NSString*)originStr;
//copy的时候更换表情文字
-(NSString *)copyConvertStr:(NSString *)pContent;
//计算聊天界面Cell高度
-(CGFloat)calculateHeightForCell:(NSDictionary*)cellDic maxWidth:(CGFloat)maxWidth faceSize:(CGFloat)faceSize;
-(NSAttributedString *)getMessageLabelString:(NSDictionary *)dic isSmall:(BOOL)isSmall content:(NSString *)content addName:(BOOL)addName;
//圈子动态
-(void)updateAllMessage:(NSString*)uid msg:(NSArray*)msg;
//-(NSArray*)getAllMessage:(NSString*)uid;
-(void)updateMyMessage:(NSString*)uid msg:(NSArray*)msg;
//-(NSArray*)getMyMessage:(NSString*)uid;

-(NSString *)trimStr:(NSString*)str;

@end
