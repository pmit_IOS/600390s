//
//  ZSTReportViewController.h
//  SNSA
//
//  Created by LiZhenQu on 14-5-20.
//
//

#import <UIKit/UIKit.h>

@interface ZSTReportViewController : UIViewController

@property (nonatomic, retain) NSString *circleId;
@property (nonatomic, retain) NSString *userId;
@property (nonatomic, retain) NSString *toUserId;

@end
