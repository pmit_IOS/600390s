//
//  ZSTMyGroupPhotoViewController.h
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/ALAssetsGroup.h>

@protocol ZSTMyGroupPhotoViewControllerDelegate <NSObject>

- (void)finishPickerImage:(NSArray *)selectedImgArr;

@end

@interface ZSTMyGroupPhotoViewController : UIViewController

@property (copy,nonatomic) NSString *groupName;
@property (strong,nonatomic) ALAssetsGroup *selectGroup;
@property (weak,nonatomic) id<ZSTMyGroupPhotoViewControllerDelegate> groupPhotoDelegate;
@property (assign,nonatomic) NSInteger maxCount;
@property (strong,nonatomic) NSMutableArray *selectPicArr;


@end
