//
//  ZSTSNSAMessageComments.h
//  SNSA
//
//  Created by pmit on 15/9/19.
//
//

#import <Foundation/Foundation.h>

@interface ZSTSNSAMessageComments : NSObject <NSCoding>

@property (assign,nonatomic) NSInteger addTime;
@property (copy,nonatomic) NSString *commentContent;
@property (copy,nonatomic) NSString *commentHeaderPhoto;
@property (assign,nonatomic) NSInteger mid;
@property (copy,nonatomic) NSString *msisdn;
@property (assign,nonatomic) NSInteger parentId;
@property (copy,nonatomic) NSString *toUserId;
@property (copy,nonatomic) NSString *uid;
@property (copy,nonatomic) NSString *uName;
@property (assign,nonatomic) NSInteger mcid;
@property (strong,nonatomic) NSDictionary *commentDic;
@property (strong,nonatomic) NSString *toUsername;
@property (strong,nonatomic) NSString *toUserPhoto;
@property (strong,nonatomic) NSString *toMsisdn;

- (id)initWithCommentDic:(NSDictionary *)commentDic;

@end
