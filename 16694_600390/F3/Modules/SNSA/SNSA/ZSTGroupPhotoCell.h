//
//  ZSTGroupPhotoCell.h
//  SNSA
//
//  Created by pmit on 15/10/27.
//
//

#import <UIKit/UIKit.h>
#import <AssetsLibrary/ALAsset.h>

@class PMRepairButton;
@class ZSTMyGroupPhotoViewController;

@protocol ZSTGroupPhotoCellDelegate <NSObject>

- (void)passImg:(ALAsset *)dealImg IsInsert:(BOOL)isInsert;

@end

@interface ZSTGroupPhotoCell : UITableViewCell

@property (strong,nonatomic) UIImageView *oneIV;
@property (strong,nonatomic) UIImageView *twoIV;
@property (strong,nonatomic) UIImageView *threeIV;
@property (strong,nonatomic) UIImageView *fourIV;

@property (strong,nonatomic) UIImageView *oneImg;
@property (strong,nonatomic) UIImageView *twoImg;
@property (strong,nonatomic) UIImageView *threeImg;
@property (strong,nonatomic) UIImageView *fourImg;
@property (strong,nonatomic) PMRepairButton *oneBtn;
@property (strong,nonatomic) PMRepairButton *twoBtn;
@property (strong,nonatomic) PMRepairButton *threeBtn;
@property (strong,nonatomic) PMRepairButton *fourBtn;

@property (strong,nonatomic) NSArray *photoArr;
@property (weak,nonatomic) id<ZSTGroupPhotoCellDelegate> cellDelegate;
@property (weak,nonatomic) ZSTMyGroupPhotoViewController *mgpVC;

- (void)createUI;
- (void)setCellData:(NSArray *)photoArr;

@end
