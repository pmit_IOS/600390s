//
//  ZSTSNSAThumShare.h
//  SNSA
//
//  Created by pmit on 15/9/25.
//
//

#import <Foundation/Foundation.h>

@interface ZSTSNSAThumShare : NSObject

@property (assign,nonatomic) NSInteger addtime;
@property (copy,nonatomic) NSString *uname;
@property (assign,nonatomic) NSInteger mid;
@property (assign,nonatomic) NSInteger mcid;
@property (copy,nonatomic) NSString *headPhoto;
@property (copy,nonatomic) NSString *uid;
@property (copy,nonatomic) NSString *circleId;
@property (copy,nonatomic) NSString *msisdn;
@property (assign,nonatomic) NSInteger isShare;
@property (strong,nonatomic) NSDictionary *thumbDic;

- (id)initWithDic:(NSDictionary *)dic;

@end
