//
//  ZSTOneTopicCommentCell.h
//  SNSA
//
//  Created by pmit on 15/10/22.
//
//

#import <UIKit/UIKit.h>
#import "CDTDisplayView.h"
#import "ZSTSNSAMessageComments.h"

@interface ZSTOneTopicCommentCell : UITableViewCell

@property (strong,nonatomic) UIView *commentsBgView;
@property (strong,nonatomic) CDTDisplayView *commentsContentView;
@property (strong,nonatomic) CALayer *line;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessageComments *)comments;

@end
