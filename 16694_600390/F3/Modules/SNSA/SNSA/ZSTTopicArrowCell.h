//
//  ZSTTopicArrowCell.h
//  SNSA
//
//  Created by pmit on 15/10/22.
//
//

#import <UIKit/UIKit.h>

@interface ZSTTopicArrowCell : UITableViewCell

@property (strong,nonatomic) UIImageView *arrowIV;

- (void)createUI;
- (void)setCellData:(ZSTSNSAMessage *)message;

@end
