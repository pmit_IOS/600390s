//
//  ZSTChatMenCell.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatMenCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTChatMenCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.chatMenHeaderIV)
    {
        CALayer *line = [CALayer layer];
        line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        line.frame = CGRectMake(0, 79.5, WIDTH, 0.5);
        [self.contentView.layer addSublayer:line];
        
        self.chatMenHeaderIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.chatMenHeaderIV.layer.masksToBounds = YES;
        self.chatMenHeaderIV.layer.cornerRadius = 25;
        self.chatMenHeaderIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.chatMenHeaderIV];
        
        self.chatMenNameLB = [[UILabel alloc] initWithFrame:CGRectMake(75, 15, WIDTH - 75 - 15 - 50, 25)];
        self.chatMenNameLB.textAlignment = NSTextAlignmentLeft;
        self.chatMenNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.chatMenNameLB.textColor = RGBACOLOR(56, 56, 56, 1);
        [self.contentView addSubview:self.chatMenNameLB];
        
        self.chatMenPhoneLB = [[UILabel alloc] initWithFrame:CGRectMake(75, 40, WIDTH - 75 - 15 - 50, 25)];
        self.chatMenPhoneLB.textAlignment = NSTextAlignmentLeft;
        self.chatMenPhoneLB.font = [UIFont systemFontOfSize:12.0f];
        self.chatMenPhoneLB.textColor = RGBACOLOR(159, 159, 159, 1);
        self.chatMenPhoneLB.numberOfLines = 0;
        [self.contentView addSubview:self.chatMenPhoneLB];
        
        self.chatMenPhoneCallIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 15 - 40, 15, 40, 50)];
        self.chatMenPhoneCallIV.contentMode = UIViewContentModeScaleAspectFit;
        self.chatMenPhoneCallIV.image = [UIImage imageNamed:@"chatroom_menPhoneYes.png"];
        [self.contentView addSubview:self.chatMenPhoneCallIV];
        
        self.chatMenPhoneCallBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.chatMenPhoneCallBtn.frame = CGRectMake(WIDTH - 15 - 40, 0, 40, 80);
        [self.chatMenPhoneCallBtn addTarget:self action:@selector(callMenPhone:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.chatMenPhoneCallBtn];
    }
}

- (void)setCellData:(NSDictionary *)chatMenDic
{
    self.chatMenDic = chatMenDic;
    [self.chatMenHeaderIV setImageWithURL:[NSURL URLWithString:[chatMenDic safeObjectForKey:@"AvatarImgUrl"]] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    NSString *chatMenName = [chatMenDic safeObjectForKey:@"UserName"];
    if (!chatMenName || [chatMenName isEqualToString:@""] || [chatMenName rangeOfString:@"****"].length > 0)
    {
        chatMenName = @"佚名";
    }
    
    self.chatMenNameLB.text = chatMenName;
    
    if ([[chatMenDic safeObjectForKey:@"ispublic"] integerValue] == 1)
    {
        self.chatMenPhoneLB.text = [chatMenDic safeObjectForKey:@"Msisdn"];
        if ([[chatMenDic safeObjectForKey:@"Msisdn"] rangeOfString:@"*"].length > 0)
        {
            self.chatMenPhoneCallIV.image = [UIImage imageNamed:@"chatroom_menPhoneNo.png"];
            self.chatMenPhoneCallBtn.enabled = NO;
        }
        else
        {
            self.chatMenPhoneCallIV.image = [UIImage imageNamed:@"chatroom_menPhoneYes.png"];
            self.chatMenPhoneCallBtn.enabled = YES;
        }
    }
    else
    {
        NSMutableString *phoneString = [[chatMenDic safeObjectForKey:@"Msisdn"] mutableCopy];
        [phoneString replaceCharactersInRange:NSMakeRange(4, 4) withString:@"****"];
        self.chatMenPhoneLB.text = phoneString;
        self.chatMenPhoneCallIV.image = [UIImage imageNamed:@"chatroom_menPhoneNo.png"];
        self.chatMenPhoneCallBtn.enabled = NO;
    }
}

- (void)callMenPhone:(UIButton *)sender
{
    if ([self.menCellDelegate respondsToSelector:@selector(callMenPhone:)])
    {
        [self.menCellDelegate callMenPhone:[self.chatMenDic safeObjectForKey:@"Msisdn"]];
    }
}

@end
