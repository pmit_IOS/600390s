//
//  ZSTChatroomMenViewController.m
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import "ZSTChatroomMenViewController.h"
#import <ZSTUtils.h>
#import "ZSTYouYunEngine.h"
#import "ZSTChatMenCell.h"
#import "ZSTChatroomMenDetailViewController.h"
#import <PMRepairButton.h>
#import "ZSTChatHomeViewController.h"

@interface ZSTChatroomMenViewController () <UITableViewDataSource,UITableViewDelegate,ZSTYouYunEngineDelegate,ZSTChatMenCellDelegate>

@property (strong,nonatomic) UITableView *chatMenTableView;
@property (strong,nonatomic) ZSTYouYunEngine *engine;
@property (strong,nonatomic) NSArray *chatMenArr;
@property (strong,nonatomic) UIView *exitAlertView;
@property (strong,nonatomic) UIView *shadowView;
@property (assign,nonatomic) BOOL isCall;
@property (copy,nonatomic) NSString *menPhone;

@end

@implementation ZSTChatroomMenViewController

static NSString *const chatMenCell = @"chatMenCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.isCall = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString([self.circleDic safeObjectForKey:@"CName"], nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    PMRepairButton *outCircleBtn = [[PMRepairButton alloc] init];
    outCircleBtn.frame = CGRectMake(0, 0, 30, 30);
    [outCircleBtn setImage:[UIImage imageNamed:@"chatroom_outCircle.png"] forState:UIControlStateNormal];
    [outCircleBtn addTarget:self action:@selector(outToCircle:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:outCircleBtn];
    
    [self buildChatMenTableView];
    [self buildShadowView];
    [self buildSureAlertView];
    self.engine = [ZSTYouYunEngine engineWithDelegate:self];
    [self.engine getUsersByCID:[self.circleDic safeObjectForKey:@"CID"]];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildChatMenTableView
{
    self.chatMenTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.chatMenTableView.delegate = self;
    self.chatMenTableView.dataSource = self;
    self.chatMenTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.chatMenTableView setSeparatorColor:[UIColor whiteColor]];
    [self.chatMenTableView registerClass:[ZSTChatMenCell class] forCellReuseIdentifier:chatMenCell];
    [self.view addSubview:self.chatMenTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatMenArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatMenCell *cell = [tableView dequeueReusableCellWithIdentifier:chatMenCell];
    [cell createUI];
    cell.menCellDelegate = self;
    NSDictionary *menDic = self.chatMenArr[indexPath.row];
    [cell setCellData:menDic];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

//- (void)

- (void)buildSureAlertView
{
    self.exitAlertView = [[UIView alloc] init];
    self.exitAlertView.center = CGPointMake(self.shadowView.centerX, self.shadowView.centerY - 64);
    self.exitAlertView.bounds = (CGRect){CGPointZero,{WIDTH - 60,100}};
    self.exitAlertView.backgroundColor = [UIColor whiteColor];
    [self.shadowView addSubview:self.exitAlertView];
    
    CALayer *lineOne = [CALayer layer];
    lineOne.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineOne.frame = CGRectMake(0, 59.5, self.exitAlertView.bounds.size.width, 0.5);
    [self.exitAlertView.layer addSublayer:lineOne];
    
    CALayer *lineTwo = [CALayer layer];
    lineTwo.backgroundColor = RGBACOLOR(151, 151, 151, 1).CGColor;
    lineTwo.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 59.5, 0.5, 40.5);
    [self.exitAlertView.layer addSublayer:lineTwo];
    
    UILabel *tipLb = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.exitAlertView.bounds.size.width , 60)];
    tipLb.textColor = RGBACOLOR(56, 56, 56, 1);
    tipLb.textAlignment = NSTextAlignmentCenter;
    tipLb.font = [UIFont systemFontOfSize:14.0f];
    tipLb.text = @"您确定要退出该圈子吗?";
    tipLb.tag = 920109;
    [self.exitAlertView addSubview:tipLb];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    cancelBtn.frame = CGRectMake(0, 60, self.exitAlertView.bounds.size.width / 2, 40);
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [cancelBtn addTarget:self action:@selector(cancelGoOut:) forControlEvents:UIControlEventTouchUpInside];
    [self.exitAlertView addSubview:cancelBtn];
    
    UIButton *sureExitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [sureExitBtn setTitle:@"确定" forState:UIControlStateNormal];
    [sureExitBtn setTitleColor:RGBACOLOR(56, 56, 56, 1) forState:UIControlStateNormal];
    sureExitBtn.frame = CGRectMake(self.exitAlertView.bounds.size.width / 2, 60, self.exitAlertView.bounds.size.width / 2, 40);
    [sureExitBtn addTarget:self action:@selector(goOutCircle:) forControlEvents:UIControlEventTouchUpInside];
    sureExitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.exitAlertView addSubview:sureExitBtn];
    
    self.exitAlertView.hidden = YES;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTChatroomMenDetailViewController *detailVC = [[ZSTChatroomMenDetailViewController alloc] init];
    detailVC.chatMenMsisdn = [self.chatMenArr[indexPath.row] safeObjectForKey:@"Msisdn"];
    detailVC.chatMenOrigineMsisdn = [self.chatMenArr[indexPath.row] safeObjectForKey:@"OriginalMsisdn"];
    [self.navigationController pushViewController:detailVC animated:YES];
}

- (void)getUsersByCIDResponse:(NSArray *)users
{
    self.chatMenArr = users;
    [self.chatMenTableView reloadData];
}

- (void)outToCircle:(PMRepairButton *)sender
{
    self.isCall = NO;
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.exitAlertView.hidden = NO;
        
    }];
}

- (void)quitCircleResponse
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil showHUD:self.view withText:@"退出成功！"];
    [TKUIUtil hiddenHUDAfterDelay:2];
    self.homeVC.isNeedRefresh = YES;
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (void)quitCircleFailed
{
    
}

- (void)goOutCircle:(UIButton *)sender
{
    if (self.isCall)
    {
        NSString *actionString = [NSString stringWithFormat:@"tel://%@",self.menPhone];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:actionString]];
        self.exitAlertView.hidden = YES;
        self.shadowView.hidden = YES;

    }
    else
    {
        [self.engine quitCircle:[self.circleDic safeObjectForKey:@"CID"]];
    }
    
}

- (void)cancelGoOut:(UIButton *)sender
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.exitAlertView.hidden = YES;
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.shadowView];
    
    UIView *alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    alphaView.backgroundColor = [UIColor blackColor];
    alphaView.alpha = 0.6;
    [self.shadowView addSubview:alphaView];
    self.shadowView.hidden = YES;
}

- (void)callMenPhone:(NSString *)menPhoneString
{
    self.isCall = YES;
    self.menPhone = menPhoneString;
    UILabel *tipLB = (UILabel *)[self.exitAlertView viewWithTag:920109];
    tipLB.text = [NSString stringWithFormat:@"确定拨打 %@ 吗?",menPhoneString];
    self.shadowView.hidden = NO;
    self.exitAlertView.hidden = NO;
}

@end
