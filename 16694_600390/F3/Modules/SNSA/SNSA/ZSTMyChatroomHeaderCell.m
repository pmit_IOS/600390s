//
//  ZSTMyChatroomHeaderCell.m
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import "ZSTMyChatroomHeaderCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTMyChatroomHeaderCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.headerIV)
    {
        self.headerIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 50, 50)];
        self.headerIV.layer.masksToBounds = YES;
        self.headerIV.layer.cornerRadius = 25;
        self.headerIV.userInteractionEnabled = YES;
        self.headerIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
        [self.contentView addSubview:self.headerIV];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToUsers:)];
        [self.headerIV addGestureRecognizer:tap];
        
        self.moreBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(WIDTH - 15 - 30, 25, 30, 30)];
        [self.moreBtn setImage:[UIImage imageNamed:@"chatroom_more.png"] forState:UIControlStateNormal];
        [self.moreBtn addTarget:self action:@selector(showMoreFunInFatherView:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.moreBtn];
        
        self.headerNameLB = [[UILabel alloc] initWithFrame:CGRectMake(80, 15, WIDTH - 80 - 30 - 15, 30)];
        self.headerNameLB.textAlignment = NSTextAlignmentLeft;
        self.headerNameLB.font = [UIFont systemFontOfSize:15.0f];
        self.headerNameLB.textColor = RGBACOLOR(248, 145, 70, 1);
        self.headerNameLB.text = @"小明真的碉堡了";
        [self.contentView addSubview:self.headerNameLB];
        self.headerNameLB.userInteractionEnabled = YES;
        UITapGestureRecognizer *headerNameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToUsers:)];
        [self.headerNameLB addGestureRecognizer:headerNameTap];
        
        NSString *timeString = @"今天14:00";
        CGSize timeStringSize = [timeString sizeWithFont:[UIFont systemFontOfSize:10.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 20)];
        self.headerTimeLB = [[UILabel alloc] init];
        self.headerTimeLB.frame = CGRectMake(80, 45, timeStringSize.width, 20);
        self.headerTimeLB.text = timeString;
        self.headerTimeLB.textAlignment = NSTextAlignmentLeft;
        self.headerTimeLB.font = [UIFont systemFontOfSize:10.0f];
        self.headerTimeLB.textColor = RGBACOLOR(159, 159, 159, 1);
        [self.contentView addSubview:self.headerTimeLB];
        
        self.headerBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.headerBtn.frame = CGRectMake(80 + timeStringSize.width + 10, 47.5, 40, 15);
        [self.headerBtn setTitle:@"置顶" forState:UIControlStateNormal];
        [self.headerBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        self.headerBtn.titleLabel.font = [UIFont systemFontOfSize:10.0f];
        [self.headerBtn setBackgroundColor:RGBACOLOR(76,205,247,1)];
        self.headerBtn.hidden = YES;
        [self.contentView addSubview:self.headerBtn];
        
        self.topBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.topBtn.frame = CGRectMake(CGRectGetMaxX(self.headerIV.frame), 15, WIDTH - 45 - CGRectGetMaxX(self.headerIV.frame), 50);
        [self.topBtn addTarget:self action:@selector(goToTopicDetail:) forControlEvents:UIControlEventTouchUpInside];
        [self.contentView addSubview:self.topBtn];
        
    }
    
    if (!self.isDetailCell)
    {
        self.topBtn.hidden = NO;
    }
    else
    {
        self.topBtn.hidden = YES;
    }
    
}

- (void)showMoreFunInFatherView:(PMRepairButton *)sender
{
    if ([self.headerCellDelegate respondsToSelector:@selector(showMoreFun:)])
    {
        [self.headerCellDelegate showMoreFun:self.topicMessage];
    }
}

- (void)goToTopicDetail:(UIButton *)sender
{
    if ([self.headerCellDelegate respondsToSelector:@selector(goTopicDetail:)])
    {
        [self.headerCellDelegate goTopicDetail:self.topicMessage];
    }
}

- (void)setCellData:(ZSTSNSAMessage *)messageDic
{
    NSInteger timeCount = messageDic.addTime;
    self.topicMessage = messageDic;
    
    NSString *showTimeString = [self makeTimeCount:timeCount];
    CGSize showTimeSize = [showTimeString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingTruncatesLastVisibleLine | NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:10.0f]} context:nil].size;
    self.headerTimeLB.frame = CGRectMake(self.headerTimeLB.origin.x, self.headerTimeLB.origin.y, showTimeSize.width, 20);
    self.headerTimeLB.text = showTimeString;
    
    self.headerBtn.frame = CGRectMake(CGRectGetMaxX(self.headerTimeLB.frame) + 10, self.headerBtn.frame.origin.y, self.headerBtn.bounds.size.width, self.headerBtn.bounds.size.height);
    
    NSString *headerImageString = messageDic.chatNewsAvatar;
    if (!headerImageString || [headerImageString isEqualToString:@""])
    {
        self.headerIV.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
    }
    else
    {
        [self.headerIV setImageWithURL:[NSURL URLWithString:headerImageString] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    }
    
    NSString *nameString = messageDic.chatNewsUserName;
    if ([self validateMobile:nameString])
    {
        nameString = @"佚名";
    }
    else
    {
        nameString = nameString;
    }
    
    self.headerNameLB.text = nameString;
    if (messageDic.isTop == 0)
    {
        self.headerBtn.hidden = YES;
    }
    else
    {
        self.headerBtn.hidden = NO;
    }
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[025-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[2378]|7[0-9]|4[0-9])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (NSString *)makeTimeCount:(NSInteger)timeCount
{
    NSString *timeType = @"";
    NSDateFormatter *fmt = [[NSDateFormatter alloc] init];
    NSDate *now = [NSDate date];
    
    NSTimeInterval delta = [now timeIntervalSince1970] - timeCount;
    
    if (delta <= 60)
    {
        timeType = @"刚刚";
    }
    else if (delta <= 3600 && delta > 60)
    {
        timeType = [NSString stringWithFormat:@"%.0f分钟前",delta / 60];
    }
    else if (delta <= 24 * 60 * 60 && delta > 3600)
    {
        timeType = [NSString stringWithFormat:@"%.0f小时前",delta / 3600];
    }else
    {
        fmt.dateFormat = @"MM-dd HH:mm";
        NSTimeInterval time = timeCount;//因为时差问题要加8小时 == 28800 sec
        NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
        timeType = [fmt stringFromDate: detaildate];
    }
    
    return timeType;
}

- (void)goToUsers:(UITapGestureRecognizer *)tap
{
    if ([self.headerCellDelegate respondsToSelector:@selector(goToUserDetail:)])
    {
        [self.headerCellDelegate goToUserDetail:self.topicMessage.chatMsisdn];
    }
}

@end
