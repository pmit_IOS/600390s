//
//  ZSTChatroomReportCell.h
//  SNSA
//
//  Created by pmit on 15/9/15.
//
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@interface ZSTChatroomReportCell : UITableViewCell

@property (strong,nonatomic) UILabel *reportTitleLB;
@property (strong,nonatomic) PMRepairButton *singleBtn;

- (void)createUI;
- (void)setCellReportTitle:(NSString *)reportTitle;

@end
