//
//  ZSTChatroomMenDetailCell.h
//  SNSA
//
//  Created by pmit on 15/9/16.
//
//

#import <UIKit/UIKit.h>

@interface ZSTChatroomMenDetailCell : UITableViewCell

@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UILabel *rightTitleLB;

- (void)createUI;
- (void)setDetailCellData:(NSString *)titleString RightTitle:(NSString *)rightTitleString;

@end
