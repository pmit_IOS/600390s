//
//  ZSTWAPViewController.m
//  WAP
//
//  Created by admin on 12-12-21.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTWAPViewController.h"
#import "ZSTModuleManager.h"
#import "ZSTUtils.h"

#define isRetina ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 960), [[UIScreen mainScreen] currentMode].size) : NO)

#define kCurrntWidth 320
#define kCurrntHeight 480
#define kRetinaWidth 640
#define kRetinaHeight 960

@interface ZSTWAPViewController ()
{
    NSString *url;
    BOOL isSecond;
}

@end

@implementation ZSTWAPViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils logoView];    

    _scrollToolBarView.hidden = YES;
    self.type = self.moduleType;
    [self initWithData];
}

- (void) initWithData
{
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    
    //    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"浏览器", nil) target:self selector:@selector (openMore)];
    
    NSString *baseUrl = [self.application.launchOptions objectForKey:@"InterfaceUrl"];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:dataManager.loginMsisdn forKey:@"msisdn"];
    [params setObject:dataManager.imsi forKey:@"imei"];
    if ([baseUrl rangeOfString:@"ecid"].location == NSNotFound && [baseUrl rangeOfString:@"ECID"].location == NSNotFound) {
        [params setObject:dataManager.ECECCID forKey:@"ecid"];
    }
    if ([baseUrl rangeOfString:@"module_type"].location == NSNotFound) {
        [params setObject:[NSNumber numberWithInteger:self.moduleType] forKey:@"module_type"];
    }
    if ([baseUrl rangeOfString:@"moduletype"].location == NSNotFound) {
        [params setObject:[NSNumber numberWithInteger:self.moduleType] forKey:@"moduletype"];
    }
    [params setObject:dataManager.UserId forKey:@"userId"];
    [params setObject:dataManager.platform forKey:@"clientType"];
    
    NSMutableDictionary *params2 = [NSMutableDictionary dictionary];
    [params2 setObject:[UIDevice machine] forKey:@"ua"];
    [params2 setObject:[NSNumber numberWithInt:isRetina?kRetinaWidth:kCurrntWidth] forKey:@"w"];
    [params2 setObject:[NSNumber numberWithInt:isRetina?kRetinaHeight:kCurrntHeight] forKey:@"h"];
    [params2 setObject:dataManager.platform forKey:@"platform"];
    
    NSString *currentAppUrlStr = [[[baseUrl stringByAddingQuery:params2] stringByAddingQuery:params] stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys: [ZSTUtils md5Signature:params], @"Md5Verify", nil]];
    
    if ([currentAppUrlStr rangeOfString:@"${termCookieStr}&"].location != NSNotFound) {
        
        currentAppUrlStr = [currentAppUrlStr stringByReplacingOccurrencesOfString:@"${termCookieStr}&" withString:@""];
    }
    
    url = currentAppUrlStr;
    
    [self setURL:url];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
}

//- (void) showGuideView
//{
//    UIView *guideView = [[UIView alloc] initWithFrame:CGRectMake(0,0, 320, 480+(iPhone5?88:0))];
//    guideView.backgroundColor = [UIColor blackColor];
//    guideView.tag = 99999999;
//    guideView.alpha = 0.7f;
//    
//    UILabel *signlal = [[UILabel alloc] init];
//    signlal.backgroundColor = [UIColor clearColor];
//    signlal.font = [UIFont systemFontOfSize:18];
//    signlal.textColor = [UIColor whiteColor];
//    signlal.text = @"如页面加载异常，请在浏览器打开";
//    signlal.numberOfLines = 0;
//    CGSize size = [signlal.text sizeWithFont:signlal.font constrainedToSize:CGSizeMake(150, 1000) lineBreakMode:NSLineBreakByWordWrapping];
//    signlal.frame = CGRectMake(65, 12, 150, size.height);
//    [guideView addSubview:signlal];
//    [signlal release];
//    
//    UIImageView *signImgView = [[UIImageView alloc] initWithFrame:CGRectMake(240, 2, 40, 54)];
//    signImgView.image = ZSTModuleImage(@"module_onlinea_tips.png");
//    [guideView addSubview:signImgView];
//    [signImgView release];
//    
//    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:[NSString stringWithFormat:@"%@_%d",FIRST_LAUNCH_ONLINEA,self.moduleType]];
//    
//    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
//                                                                                action:@selector(removeGuideView)];
//    [guideView addGestureRecognizer:singleTap];
//    
//    [self.view addSubview:guideView];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//- (void) removeGuideView
//{
//    [[[UIApplication sharedApplication].keyWindow viewWithTag:99999999] removeFromSuperview];
//}
//
//- (void) openMore
//{
//     [[UIApplication sharedApplication] openURL: [NSURL URLWithString:url]];
//    [self removeGuideView];
////    UIActionSheet *transmitSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:nil otherButtonTitles:@"用safari打开", nil];
////    [transmitSheet showInView:self.view.window];
////    [transmitSheet release];
//}

- (void)webViewDidStartLoad:(UIWebView*)webView {
    [super webViewDidStartLoad:webView];
    self.navigationItem.titleView = [ZSTUtils logoView];
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    [super webViewDidFinishLoad:webView];
    self.navigationItem.titleView = [ZSTUtils logoView];
    
    if (isSecond) {
        
        _scrollToolBarView.hidden = NO;
    } else {
        
        _scrollToolBarView.hidden = YES;
    }
}

- (void)webView:(UIWebView*)webView didFailLoadWithError:(NSError*)error {
    [super webView:webView didFailLoadWithError:error];
    self.navigationItem.titleView = [ZSTUtils logoView];
}


- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    [super webView:webView shouldStartLoadWithRequest:request navigationType:navigationType];
    
    _scrollToolBarView.hidden = YES;
    
    if (navigationType == UIWebViewNavigationTypeLinkClicked) {
    #ifdef DEBUG
        NSLog(@"－－－点击调用－－－－");
    #endif
        
        _scrollToolBarView.hidden = NO;
        isSecond = YES;
    }
    
    NSString *urlString = [[request URL] absoluteString];
    NSLog(@"%@",urlString);
   if ([request.URL.scheme caseInsensitiveCompare:@"native"] == NSOrderedSame)
   {
        NSArray *urlComps = [urlString componentsSeparatedByString:@"://"];
       NSArray *arrFucnameAndParameter = [(NSString*)[urlComps objectAtIndex:1] componentsSeparatedByString:@":"];
       NSString *funcStr = [arrFucnameAndParameter objectAtIndex:0];
       
       if([funcStr isEqualToString:@"setUser"]) {
           
           NSArray *array = [[arrFucnameAndParameter objectAtIndex:1] componentsSeparatedByString:@","];
           
           [ZSTF3Preferences shared].UserId = [array objectAtIndex:0];
           [ZSTF3Preferences shared].loginMsisdn = [array objectAtIndex:1];
           
            [webView stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"%@();",[array objectAtIndex:2]]];
       }
       
       return NO;
   }
    
    return YES;
}

#pragma mark UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:url]];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
