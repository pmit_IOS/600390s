//
//  ZSTShellDCarouselView.h
//  ShellD
//
//  Created by xuhuijun on 13-6-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TKAsynImageView.h"

@class ZSTShellFCarouselView;

@protocol ZSTShellFCarouselViewDataSource <NSObject>

@optional
- (NSInteger)numberOfViewsInCarouselView:(ZSTShellFCarouselView *)newsCarouselView;
- (NSDictionary *)carouselView:(ZSTShellFCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index;

@end

@protocol ZSTShellFCarouselViewDelegate <NSObject>

@optional
- (void)carouselView:(ZSTShellFCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;

@end

@interface ZSTShellFCarouselView : UIView <HJManagedImageVDelegate,UIScrollViewDelegate,TKAsynImageViewDelegate>

{
    UIScrollView *_scrollView;
    UILabel *_describeLabel;
    TKPageControl *_pageControl;
    NSTimer *_carouselTimer;
    
    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curSource;
    
    id<ZSTShellFCarouselViewDataSource> _carouselDataSource;
    id<ZSTShellFCarouselViewDelegate>  _carouselDelegate;
}


@property(nonatomic, assign) id<ZSTShellFCarouselViewDataSource> carouselDataSource;
@property(nonatomic, assign) id<ZSTShellFCarouselViewDelegate> carouselDelegate;

- (void)reloadData;
- (void)stopAnimation;
- (void)startAnimation;

@end
