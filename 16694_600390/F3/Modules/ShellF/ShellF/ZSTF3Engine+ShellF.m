//
//  ZSTF3Engine+ShellD.m
//  ShellD
//
//  Created by xuhuijun on 13-6-19.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+ShellF.h"
#import "ZSTGlobal+ShellF.h"

@implementation ZSTF3Engine (ShellF)

- (void)getShellFAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [params setSafeObject:@([ZSTF3Preferences shared].shellId).stringValue forKey:@"moduleid"];

    [[ZSTCommunicator shared] openAPIPostToPath:GetShellFAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellFADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
}


- (void)getShellFADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellBList = [response.jsonResponse safeObjectForKey:@"info"];
        if (![shellBList isKindOfClass:[NSArray class]]) {
            shellBList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellFADDidSucceed:)]) {
            [self.delegate getShellFADDidSucceed:shellBList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellFADDidFailed:)]) {
            [self.delegate getShellFADDidFailed:response.resultCode];
        }
    }
    
}


- (void)getShellFModuleAD
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[NSNumber numberWithInt:2] forKey:@"type"];
    [[ZSTCommunicator shared] openAPIPostToPath:GetShellFAD
                                         params:params
                                         target:self
                                       selector:@selector(getShellDModuleADResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
}


- (void)getShellFModuleADResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id shellBList = [response.jsonResponse safeObjectForKey:@"info"];
        if (![shellBList isKindOfClass:[NSArray class]]) {
            shellBList = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getShellDModuleADDidSucceed:)]) {
            [self.delegate getShellFModuleADDidSucceed:shellBList];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getShellDModuleADDidFailed:)]) {
            [self.delegate getShellFModuleADDidFailed:response.resultCode];
        }
    }
    
}


@end
