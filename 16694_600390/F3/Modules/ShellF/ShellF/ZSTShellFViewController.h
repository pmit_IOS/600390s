//
//  ZSTShellFViewController.h
//  ShellF
//
//  Created by LiZhenQu on 14-4-9.
//  Copyright (c) 2014年 xxxxx. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ZSTShellFCarouselView.h"
#import "ZSTF3Engine+ShellF.h"
#import "ZSTShellFSpeedBar.h"

@interface ZSTShellFViewController : ZSTModuleBaseViewController<ZSTShellFSpeedBarDelegate,ZSTShellFCarouselViewDataSource,ZSTShellFCarouselViewDelegate,UIScrollViewDelegate>

@property (nonatomic,strong)NSArray *shellFADArray;
@property (nonatomic,strong)NSArray *shellFModuleADArray;

@property (nonatomic,strong) ZSTShellFCarouselView *shellfCarouselView;
@property (strong, nonatomic) UIScrollView *speedBarScrollView;

@end
