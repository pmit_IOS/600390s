//
//  ZSTShellFRootViewController.h
//  ShellF
//
//  Created by LiZhenQu on 14-4-4.
//  Copyright (c) 2014年 xxxxx. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTShellFRootViewController : UITabBarController <UINavigationControllerDelegate,UITabBarControllerDelegate,ZSTModuleDelegate>


@end
