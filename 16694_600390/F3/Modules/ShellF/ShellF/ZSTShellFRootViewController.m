//
//  ZSTShellFRootViewController.m
//  ShellF
//
//  Created by LiZhenQu on 14-4-4.
//  Copyright (c) 2014年 xxxxx. All rights reserved.
//

#import "ZSTShellFRootViewController.h"
#import "ZSTUtils.h"
#import "ZSTGlobal+ShellF.h"
#import "ZSTShell.h"
#import "ZSTGlobal+F3.h"

@interface ZSTShellFRootViewController ()

@end

@implementation ZSTShellFRootViewController

@synthesize application;
@synthesize rootView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(pushViewController)
                                                 name: NotificationName_PushViewController
                                               object: nil];
    
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
//    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed: @"framework_top_bg.png"] forBarMetrics:UIBarMetricsDefault];
    self.navigationItem.titleView = [ZSTUtils logoView];
       
    //设置tabbar背景图片
    UIImage *image = [[UIImage imageNamed:@"framework_bottom_bg.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:25];
    
    NSString *minimumSystemVersion = @"4.0";
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    if ([systemVersion compare:minimumSystemVersion options:NSNumericSearch] != NSOrderedAscending)
    {
        [[self tabBar] setBackgroundImage:image];
    }else{
        self.tabBar.layer.contents = (id)image.CGImage;
        
    }
    
    self.delegate = self;
    //Tabbar配置信息从config.xml加载
    NSArray *viewControllers = [ZSTShell shellFviewControllerForTabBarController];
    for (UINavigationController *navController in viewControllers) {
        navController.delegate = self;
    }
    self.viewControllers = viewControllers;
    
    //设置默认选中
    NSUInteger selectedIndex = [NSLocalizedString(@"GP_DefaultBottomTabIndex", @"1") intValue];
    if (selectedIndex < 1 && selectedIndex > 3) {
        selectedIndex = 1;
    }
    self.selectedIndex = selectedIndex - 1;
    
}

- (BOOL)tabBarController:(UITabBarController *)tbc shouldSelectViewController:(UIViewController *)vc {
    UIViewController *tbSelectedController = tbc.selectedViewController;
    if ([tbSelectedController isEqual:vc]) {
        return NO;
    }
    return YES;
}

- (void)pushViewController
{
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    UIViewController *controller = per.pushViewController;
    controller.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:controller animated:YES];
    controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    per.isInPush = YES;
    
}

- (void)popViewController {
    
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    per.isInPush = NO;
    [self.navigationController popViewControllerAnimated:YES];
    
}

//- (void)pushViewControllerWithParam:(NSDictionary *)param
//{
//    NSMutableDictionary *moduleParams = [NSMutableDictionary dictionaryWithDictionary:param];
//    
//    ZSTModule *module = [[ZSTModuleManager shared] findModule:[[param safeObjectForKey:@"ModuleId"] integerValue]];
//    if (module) {
//        UIViewController *controller =[[ZSTModuleManager shared] launchModuleApplication:[[param safeObjectForKey:@"ModuleId"] integerValue] withOptions:moduleParams];
//        if (controller) {
//            controller.hidesBottomBarWhenPushed = YES;
//            controller.navigationItem.titleView = [ZSTUtils titleViewWithTitle:[param safeObjectForKey:@"Title"]];
//            controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
//            
//            [self.navigationController pushViewController:controller animated:YES];
//        }
//    }
//}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(BOOL)shouldAutorotate
{
    return NO;
}

-(NSUInteger)supportedInterfaceOrientations
{
    return (1 << UIInterfaceOrientationPortrait);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

- (UIViewController *)rootViewController;
{
    return self;
}
- (void)dealloc
{
	[super dealloc];
}

@end

