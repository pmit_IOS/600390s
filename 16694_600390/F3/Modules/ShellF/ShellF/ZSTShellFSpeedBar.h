//
//  ZSTShellBSpeedBar.h
//  shellB
//
//  Created by xuhuijun on 13-1-10.
//
//

#import <UIKit/UIKit.h>

@class ZSTShellFSpeedBar;

@protocol ZSTShellFSpeedBarDelegate <NSObject>

- (void)ZSTShellFSpeedBar:(ZSTShellFSpeedBar *)speedBar withParam:(NSDictionary *)param;

@end


@interface ZSTShellFSpeedBar : UIView

@property(strong)UILabel *titleLabel;
@property(strong)UIButton *speedBtn;
@property(strong,nonatomic)NSDictionary *moduleParams;


@property(strong)id<ZSTShellFSpeedBarDelegate>delegate;

- (void)configSpeedBarNormalImage:(UIImage *)normalImage
                    selectedImage:(UIImage *)selectedImage
                            param:(NSDictionary *)param;

- (void)speedBarInerSpace:(float) space;

@end
