//
//  ZSTMessageSettingViewController.m
//  PushA
//
//  Created by xuhuijun on 13-2-1.
//
//

#import "ZSTMessageSettingViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ZSTUtils.h"

#define  kZSTSettingItemType_Audio          @"Sound"
#define  kZSTSettingItemType_Shake          @"Vibrate"
#define  kZSTSettingItemType_CleanMsg       @"MessageClear"
#define  kZSTSettingItemType_ReportState    @"Report"
#define  kZSTSettingItemType_FullScreen     @"FullScreen"
#define  kZSTSettingItemType_NMSPackageType @"NoPicture"
#define  kZSTSettingItemType_SinaWeiBo      @"sinaWeiBo"


#define kZSTAudioSwitchTag          79
#define kZSTShakeSwitchTag          80
#define kZSTCleanMsgSwitchTag       81
#define kZSTReportStateSwitchTag    82
#define kZSTFullScreenSwitchTag     83
#define kZSTNMSPackageTypeSwitchTag 84
#define kZSTSynchronousSinaWeiBoSwitchTag 85


@implementation ZSTPushASettingItem

@synthesize title;
@synthesize type;

+(ZSTPushASettingItem *)itemWithTitle:(NSString *)aTitle type:(NSString *)aType
{
    ZSTPushASettingItem *item = [[ZSTPushASettingItem alloc] init];
    item.title = aTitle;
    item.type = aType;
    return [item autorelease];
}

- (void)dealloc
{
    [TKUIUtil hiddenHUD];
    self.type = nil;
    self.title = nil;
    [super dealloc];
}

@end



@interface ZSTMessageSettingViewController ()

@end

@implementation ZSTMessageSettingViewController
//@synthesize weiBoEngine = _weiBoEngine;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)initSettingItems
{
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = @" Sound, Vibrate, Report, FullScreen, MessageClear";

    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    _settingItems = (NSMutableArray *)[[GP_Setting_Items componentsSeparatedByString:@","] retain];
    
    if ([NSLocalizedString(@"GP_WITH_WEIBO", nil) isEqualToString:@"1"]) {
        [_settingItems addObject:@"sinaWeiBo"];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self initSettingItems];
    [self refreshArray];

    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-20-44) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor clearColor];
    _tableView.backgroundView = nil;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    //ios6 bug  必须要这样写， 否则底部会有空隙
    _tableView.tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,1.0f,1.0f)] autorelease];
   	_tableView.dataSource = self;
    _tableView.delegate = self;
    
    UIImageView *backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]] autorelease];
    backgroundView.frame = CGRectMake(0, 0, 320, 480-20-44+(iPhone5?88:0));
    [self.view addSubview:backgroundView];
	[self.view addSubview:_tableView];

    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(UpdateStingsView)
                                                 name: NotificationName_LoginMsisdnChange
                                               object: nil];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([NSLocalizedString(@"GP_WITH_WEIBO", nil) isEqualToString:@"1"]) {
        UISwitch *aSwitch = (UISwitch *)[self.view viewWithTag:kZSTSynchronousSinaWeiBoSwitchTag];
        aSwitch.on = [self.sinaWeiboEngine isLoggedIn];
    }
}

#pragma mark
#pragma WBEngineDelegate

- (void)engineAlreadyLoggedIn:(SinaWeibo *)engine
{
}

- (void)engineDidLogIn:(SinaWeibo *)engine
{
    [engine getUserInfo:engine.userID];
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)engine:(SinaWeibo *)engine didFailToLogInWithError:(NSError *)error
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"绑定失败,请重试!", @"") withImage:nil];
}
- (void)engineDidLogOut:(SinaWeibo *)engine
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"解除绑定成功!", nil) withImage:nil];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"screen_name"];
    [_tableView reloadData];
    
}

- (void)engineNotAuthorized:(SinaWeibo *)engine
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"该应用没有授权", nil) withImage:nil];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"screen_name"];
    [_tableView reloadData];
    
}
- (void)engineAuthorizeExpired:(SinaWeibo *)engine
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"该应用授权过期", nil) withImage:nil];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"screen_name"];
    [_tableView reloadData];
    
}

- (void)engine:(SinaWeibo *)engine parsingSucceededForScreenName:(NSString *)result
{
    [[NSUserDefaults standardUserDefaults] setObject:result forKey:@"screen_name"];
    [_tableView reloadData];
}

- (void)refreshArray
{
    _tableViewDataArray = [[NSMutableArray alloc] init];
    
    NSMutableArray *array = [NSMutableArray array];
    
    
    if ([_settingItems containsObject:kZSTSettingItemType_Audio]) {
         [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"声音提示", nil) type:kZSTSettingItemType_Audio]];
    }

   if ([_settingItems containsObject:kZSTSettingItemType_Shake] && [UIDevice isIPhone]) {
       [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"震动提示", nil) type:kZSTSettingItemType_Shake]];
   }

    if ([_settingItems containsObject:kZSTSettingItemType_CleanMsg]) {
        [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"信息清理提示(大于200条)", nil) type:kZSTSettingItemType_CleanMsg]];
   }

    if ([_settingItems containsObject:kZSTSettingItemType_ReportState]) {
        [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"报告状态", nil) type:kZSTSettingItemType_ReportState]];
    }

    if ([_settingItems containsObject:kZSTSettingItemType_FullScreen]) {
        [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"消息窗口全屏", nil) type:kZSTSettingItemType_FullScreen]];
    }

    if ([_settingItems containsObject:kZSTSettingItemType_NMSPackageType]) {
        [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"无图模式", nil) type:kZSTSettingItemType_NMSPackageType]];
    }

    if ([_settingItems containsObject:kZSTSettingItemType_SinaWeiBo]) {
        [array addObject:[ZSTPushASettingItem itemWithTitle:NSLocalizedString(@"新浪微博", nil) type:kZSTSettingItemType_SinaWeiBo]];
    }
        
    [_tableViewDataArray addObject:array];
    
}

-(void)UpdateStingsView
{
    [self refreshArray];
    [_tableView reloadData];
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDelegate－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [_tableViewDataArray objectAtIndex:section];
	return array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;              // Default is 1 if not implemented
{
    return [_tableViewDataArray count];
}


- (UISwitch *)addSwitch:(UITableViewCell *)cell itemType:(ZSTPushASettingItem *)item
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UISwitch *aSwitch = nil;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0) {
        aSwitch = [[[UISwitch alloc] initWithFrame:CGRectMake(210, 10, 60, 40)] autorelease] ;
    } else {
        aSwitch = [[[UISwitch alloc] initWithFrame:CGRectMake(190, 10, 60, 40)] autorelease] ;
    }
    
    [aSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:aSwitch];//为每个cell创建一个switch开关。
    return aSwitch;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{

    
    TKTableViewCell *cell = [[[TKTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                    reuseIdentifier:nil] autorelease];
    cell.customBackgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    cell.customSeparatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.cornerRadius = 4.0;
    [cell prepareForTableView:tableView indexPath:indexPath];
    NSArray *array = [_tableViewDataArray objectAtIndex:indexPath.section];
    ZSTPushASettingItem *item = [array objectAtIndex:indexPath.row];
    cell.textLabel.text =  item.title;
    cell.textLabel.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.backgroundColor = [UIColor clearColor];
        if ([item.type isEqualToString:kZSTSettingItemType_Audio])
        {
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTAudioSwitchTag;
            aSwitch.on = [ZSTF3Preferences shared].sound;
        }
        else if ([item.type isEqualToString:kZSTSettingItemType_Shake])
        {
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTShakeSwitchTag;
            aSwitch.on = [ZSTF3Preferences shared].shake;
        }
        else if ([item.type isEqualToString:kZSTSettingItemType_CleanMsg])
        {
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTCleanMsgSwitchTag;
            aSwitch.on = [ZSTF3Preferences shared].cleanUp;
        }
        else if ([item.type isEqualToString:kZSTSettingItemType_ReportState])
        {
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTReportStateSwitchTag;
            aSwitch.on = [ZSTF3Preferences shared].reportState;
        }
        else if ([item.type isEqualToString:kZSTSettingItemType_FullScreen])
        {
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTFullScreenSwitchTag;
            aSwitch.on = [ZSTF3Preferences shared].fullScreen;
        }
        else if ([item.type isEqualToString:kZSTSettingItemType_NMSPackageType])
        {
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTNMSPackageTypeSwitchTag;
    
            if ([ZSTF3Preferences shared].packageType == NMSPackageType_noPicture) {
                aSwitch.on = YES;
            }else{
                aSwitch.on = NO;
            }
        }
        else if ([item.type isEqualToString:kZSTSettingItemType_SinaWeiBo])
        {
    
            UILabel *loginMsisdnLable = [[UILabel alloc] initWithFrame:CGRectMake(70, 15, 110, 20)];
            NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:@"screen_name"];
            if ([name length] != 0 && name != nil) {
                loginMsisdnLable.text = [NSString stringWithFormat:@"(%@)",name];
            }
            loginMsisdnLable.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
            loginMsisdnLable.font = [UIFont systemFontOfSize:14];
            loginMsisdnLable.lineBreakMode = UILineBreakModeMiddleTruncation;
            loginMsisdnLable.backgroundColor = [UIColor clearColor];
            [cell.contentView addSubview:loginMsisdnLable];
            [loginMsisdnLable release];
    
            UISwitch *aSwitch = [self addSwitch:cell itemType:item];
            aSwitch.tag = kZSTSynchronousSinaWeiBoSwitchTag;
            aSwitch.on = [self.sinaWeiboEngine isLoggedIn];
        }else {
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    return cell;
}

-(void) switchChanged: (UISwitch*)sender
{
    switch (sender.tag) {
        case kZSTAudioSwitchTag:
            if (sender.on)
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"声音提示打开", nil)];
                [ZSTUtils playAlertSound:@"sms-received1" withExtension:@"caf"];
                
            }
            else
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"声音提示关闭", nil)];
            }
            [ZSTF3Preferences shared].sound = sender.on;
            break;
        case kZSTShakeSwitchTag:
            if (sender.on)
            {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"震动提示打开", nil)];
                
            }
            else
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"震动提示关闭", nil)];
            }
            [ZSTF3Preferences shared].shake = sender.on;
            break;
        case kZSTCleanMsgSwitchTag:
            if (sender.on)
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"信息自动清理功能打开", nil)];
                
                postNotificationOnMainThreadNoWait(NotificationName_CleanupMessages);
                
                [ZSTF3Preferences shared].cleanupDate = [NSDate date];
            }
            else
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"信息自动清理功能关闭", nil)];
            }
            [ZSTF3Preferences shared].cleanUp = sender.on;
            break;
            
        case kZSTReportStateSwitchTag:
        {
            [ZSTF3Preferences shared].reportState = sender.on;
            break;
            
        }
        case kZSTFullScreenSwitchTag:
        {
            [ZSTF3Preferences shared].fullScreen = sender.on;
            break;
            
        }
        case kZSTNMSPackageTypeSwitchTag:
        {
            if (sender.on) {
                [ZSTF3Preferences shared].packageType = NMSPackageType_noPicture;
            }else{
                [ZSTF3Preferences shared].packageType = NMSPackageType_normal;
            }
            break;
            
        }
        case kZSTSynchronousSinaWeiBoSwitchTag:
        {
            if (sender.on) {
                [self.sinaWeiboEngine logIn];
            }else if ([self.sinaWeiboEngine isLoggedIn] && !(sender.on))
            {
                [self.sinaWeiboEngine logOut];
            }
            break;
        }
        default:
            break;
    }
    [[ZSTF3Preferences shared] synchronize];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)dealloc
{
    [_tableViewDataArray release];
    [_settingItems release];
    [_tableView release];
    
    [super dealloc];
}

@end
