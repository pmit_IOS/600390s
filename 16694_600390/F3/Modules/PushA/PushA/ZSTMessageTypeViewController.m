//
//  switchViewController.m
//  F3_UI
//
//  Created by Xu Huijun on 11-6-29.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

//#import "switchViewController.h"

#import "ZSTMessageTypeViewController.h"
#import "TKUIUtil.h"
#import "ZSTMessageGroupTabelViewController.h"
#import  <QuartzCore/QuartzCore.h>
#import "ASIFormDataRequest.h"
#import "ElementParser.h"
#import "MsgTypeInfo.h"
#import "MsgTypeDao.h"
#import "MessageDao.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"
#import "ZSTEditView.h"
#import "ZSTMessagesViewController.h"
#import "ZSTShell.h"
#import "ZSTDao+pusha.h"
#import "ZSTF3Preferences.h"
#import "BaseNavgationController.h"
#import <PMRepairButton.h>

@implementation ZSTMessageTypeViewController

#pragma mark - ZSTLauncherViewDataSource


+(void)initialize
{
    [ZSTDao createTableIfNotExistForPushaModule];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(applicationDidFinishLaunching) name:UIApplicationDidFinishLaunchingNotification object:nil];
    
    if ([ZSTShell isModuleAvailable:-1]) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(forceHttpPoll) name:UIApplicationWillEnterForegroundNotification object:nil];
    }
    if ([ZSTShell isModuleAvailable:22]) {
        [[NSNotificationCenter defaultCenter] addObserver: self
                                                 selector: @selector(forceHttpPoll)
                                                     name: NotificationName_ForceHttpPoll
                                                   object: nil];
    }
    [self updateIconBadgeNumber];
}


+(void)applicationDidFinishLaunching
{
    if ([ZSTShell isModuleAvailable:-1] || [ZSTShell isModuleAvailable:22]) {
//        //开始http轮询
//        [ZSTF3Engine startHttpPoll];
        
        //自动清理检查
        if ([ZSTF3Preferences shared].cleanUp && [[ZSTF3Preferences shared].cleanupDate timeIntervalSinceNow] <= -(15.0*24*60*60)) {
            [self cleanUpMessageAlert];
        }
        //信息类别每启动一次可更新一次
        [ZSTUtils setNeedReloadMessageType];
    }
}

+(void)cleanUpMessageAlert
{
    if ([[MessageDao shareMessageDao] getCount] >= 200) {
        
        UIAlertView * cleanupMessageAlert = [[UIAlertView alloc] initWithTitle:nil
                                                                       message:NSLocalizedString(@"为了节约存储空间，是否要自动清理旧消息?", nil)
                                                                      delegate:self
                                                             cancelButtonTitle:NSLocalizedString(@"清理", nil)
                                                             otherButtonTitles:NSLocalizedString(@"取消",@"取消"),nil];
        cleanupMessageAlert.tag = 1023;
        [cleanupMessageAlert show];
        [cleanupMessageAlert release];
    }
}

+ (void)forceHttpPoll
{
    if ([ZSTShell isModuleAvailable:-1] || [ZSTShell isModuleAvailable:22]) {
        
        ZSTF3Engine *f3Engine = [[[ZSTF3Engine alloc] init] autorelease];
        [f3Engine enforceHttpPoll];
    }
}

+(void)updateIconBadgeNumber
{
    [UIApplication sharedApplication].applicationIconBadgeNumber = [[MessageDao shareMessageDao] getCount:NMSStateNotRead] ;
}

-(NSUInteger)numberOfItemsForLauncherView:(ZSTLauncherView *)launcherView
{
    return [_msgTypeInfo count];
}

-(ZSTLaucherItem *)launcherView:(ZSTLauncherView *)launcherView itemForIndex: (NSUInteger)index
{
    MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:index];
    
    ZSTLaucherItem *item = [ZSTLaucherItem itemWithTitle:info.typeName
                                                    icon:[info.iconKey isEqualToString:PERSONAL_IMAGE]?PERSONAL_IMAGE:[NSString stringWithFormat:GETFILEIMAGE_PATH,info.iconKey]
                                                     url:nil];
    item.badgeNumber = [[MessageDao shareMessageDao] getCountOfType:info.typeID state:NMSStateNotRead];
    item.enable = !info.isShielded;
    return item;
}

#pragma mark - ZSTLauncherViewDelegate

-(void)launcherView:(ZSTLauncherView *)launcherView didClickItemAtIndex:(NSUInteger)index
{
    ZSTMessageGroupTabelViewController *messageGroupTabelViewController = [[ZSTMessageGroupTabelViewController alloc] init];
    messageGroupTabelViewController.typeDelegate = self;
    MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:index];
    messageGroupTabelViewController.info = info;
    messageGroupTabelViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messageGroupTabelViewController animated:YES];
    [messageGroupTabelViewController release];
}

-(void)delAllMSGAction
{
    MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:_optionShieldView.tag];
    
    [ZSTF3Engine deleteMessagesOfType:info.typeID];
    
    postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    postNotificationOnMainThreadNoWait(NotificationName_UnreadNMSChanged);
    postNotificationOnMainThreadNoWait(NotificationName_SwitchNMSListUpdated);
    [_optionShieldView dismiss];
}

-(void)setShieldMSGAction
{
    MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:_optionShieldView.tag];
    [_f3Engine setMsgType:info.typeID action:info.isShielded?MsgTypeSetAction_NotShield:MsgTypeSetAction_Shield];
    [_optionShieldView dismiss];
}

-(void)cancelMSGAction
{
    [_optionShieldView dismiss];
    [_editView dismiss];
}

-(void)launcherView:(ZSTLauncherView *)launcherView didClickDeleteButtonAtIndex:(NSUInteger)index
{
    MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:index];
    UIImage *image = [UIImage imageNamed:@"optionUnshield.png"];
    [_optionShieldView release];
    _optionShieldView = [[ZSTEditView alloc] initWithImage:image];
    _optionShieldView.center = CGPointMake(320/2, (480-20-44)/2);
    
    UIButton *delAllMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    delAllMSGBtn.frame = CGRectMake(6, 12, 65, 55);
    [delAllMSGBtn addTarget:self action:@selector(delAllMSGAction) forControlEvents:UIControlEventTouchUpInside];
    delAllMSGBtn.showsTouchWhenHighlighted = YES;
    delAllMSGBtn.backgroundColor = [UIColor clearColor];
    
    UIButton *setShieldMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    setShieldMSGBtn.frame = CGRectMake(73, 12, 65, 55);
    [setShieldMSGBtn addTarget:self action:@selector(setShieldMSGAction) forControlEvents:UIControlEventTouchUpInside];
    setShieldMSGBtn.showsTouchWhenHighlighted = YES;
    setShieldMSGBtn.backgroundColor = [UIColor clearColor];
    
    UIButton *cancelMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelMSGBtn.frame = CGRectMake(139, 12, 65, 55);
    [cancelMSGBtn addTarget:self action:@selector(cancelMSGAction) forControlEvents:UIControlEventTouchUpInside];
    cancelMSGBtn.showsTouchWhenHighlighted = YES;
    cancelMSGBtn.backgroundColor = [UIColor clearColor];
    
    [_optionShieldView addSubview:delAllMSGBtn];
    if (![info.typeID isEqualToString:PERSONAL_MSGTYPE]) {
        [_optionShieldView addSubview:setShieldMSGBtn];
    }
    [_optionShieldView addSubview:cancelMSGBtn];
    
    if ([info.typeID isEqualToString:PERSONAL_MSGTYPE]) {
        _optionShieldView.image = [UIImage imageNamed:@"optionPersonal.png"];
    }else if (info.isShielded) {
        _optionShieldView.image = [UIImage imageNamed:@"optionUnshield.png"];
    }else {
        _optionShieldView.image = [UIImage imageNamed:@"optionShield.png"];
    }
    _optionShieldView.tag = index;
    [_optionShieldView show];
}

- (void)dealloc {
    
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    pre.isInPush = NO;
    
    [_msgTypeInfo release];
    [_launcherView release];
    [_optionShieldView release];
    [_f3Engine release]; _f3Engine = nil;
    [_activityView release]; _activityView = nil;
    [super dealloc];
}

- (void)updateView
{
    [_msgTypeInfo release];
    _msgTypeInfo = (NSMutableArray *)[[[MsgTypeDao shareMsgTypeDao] getMsgTypeInfos] retain];
    [_launcherView reloadData];
}

- (void)updateCount
{
    for (int i=0; i<[_msgTypeInfo count]; i++) {
        
        MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:i];
        NSUInteger count = [[MessageDao shareMessageDao] getCountOfType:info.typeID state:NMSStateNotRead];
        
        [_launcherView setBadgeNumber:count atIndex:i];
    }
}

-(void) onNMSListUpdated: (NSNotification *) notification
{
    [self performSelectorOnMainThread:@selector(updateView) withObject:nil waitUntilDone:NO];
}

-(void) onUnreadNMSChanged: (NSNotification *) notification
{
    [self performSelectorOnMainThread:@selector(updateCount) withObject:nil waitUntilDone:NO];
}

- (void)progressWillStart
{
    _activityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    _activityView.frame = CGRectMake(280.0f, 12.0f, 20.0f, 20.0f);
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:_activityView] autorelease];
    [_activityView startAnimating];
}

#pragma mark - View lifecycle
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.


- (void)viewDidLoad
{
    [super viewDidLoad];
    
//    self.navigationItem.titleView = [ZSTUtils logoView];
//    self.navigationItem.hidesBackButton = YES;
    
    // wei:2015-09-09 修改
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"信息中心", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    
    
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    pre.isInPush = YES;
    
    _launcherView = [[ZSTLauncherView alloc] init];
    _launcherView.columnCount = 3;
    _launcherView.rowCount = 3;
    _launcherView.isHorizontal = YES;
    _launcherView.frame = CGRectMake(0, 0, 320, 480-20-44-29+(iPhone5?88:0));
//    _launcherView.layer.contents = (id) [UIImage imageNamed:@"bg.png"].CGImage;

    _launcherView.delegate = self;
    _launcherView.dataSource = self;

    [self.view addSubview:_launcherView];

    
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onNMSListUpdated:)
                                                 name: NotificationName_NMSListUpdated
                                               object: nil];
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(onUnreadNMSChanged:)
                                                 name: NotificationName_UnreadNMSChanged
                                               object: nil];
    
    [self updateView];

    _f3Engine = [[ZSTF3Engine alloc] init];
    _f3Engine.delegate = self;
    
    [self refresh];

    [self createNavigationItem];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    else
    {
        [self forceRefresh];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}

- (void)loginDidCancel
{
    for (UIViewController *controller in self.navigationController.viewControllers) {
        
        if (![controller isKindOfClass:[self.rootViewController class]]) {
            
            [self.navigationController popToViewController:controller animated:YES];
        }
    }
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSNumber *shellId = [NSNumber numberWithInteger:[ZSTF3Preferences shared].shellId];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:kNotificationLoginCancel object:shellId];
}

- (void)loginFinish
{
    
}

- (void)refresh
{
    [self cancelMSGAction];

    if ([ZSTUtils messageTypeIsUpdate]) {
        [_f3Engine syncMsgType];
        [self progressWillStart];
    }
}

- (void)forceRefresh
{
    [self cancelMSGAction];

    if ([ZSTShell isModuleAvailable:-1]  || [ZSTShell isModuleAvailable:22]) {
        
        ZSTF3Engine *f3Engine = [[[ZSTF3Engine alloc] init] autorelease];
        [f3Engine enforceHttpPoll];
    }
}

-(void)swithToInboxViewAction
{
    [self cancelMSGAction];

    ZSTMessagesViewController *messageViewController = [[ZSTMessagesViewController alloc] init];
    messageViewController.typeDelegate = self;
//     [self.navigationController pushViewController:messageViewController animatedWithTransition:UIViewAnimationTransitionFlipFromRight];
    [self.navigationController pushViewController:messageViewController animated:YES];

}

-(void) createNavigationItem
{
//    UIButton *optionsBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    PMRepairButton *optionsBtn = [[PMRepairButton alloc] init];
    optionsBtn.frame = CGRectMake(280, 7, 30, 30);
    optionsBtn.imageEdgeInsets = UIEdgeInsetsMake(6, 6, 6, 6);
    [optionsBtn setImage:[UIImage imageNamed:@"icon_edit_n.png"] forState:UIControlStateNormal];
    [optionsBtn setImage:[UIImage imageNamed:@"icon_edit_p.png"] forState:UIControlStateHighlighted];
    [optionsBtn addTarget:self action:@selector(edit) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:optionsBtn] autorelease];
    
}

- (void)edit
{
    
    UIImage *image = ZSTModuleImage(@"module_pusha_option_edit_right_list.png");

    _editView = [[ZSTEditView alloc] initWithImage:image];
    _editView.frame = CGRectMake(175, 51, image.size.width, image.size.height);
    
    UIButton *swithMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    swithMSGBtn.frame = CGRectMake(6, 20, 65, 55);
    [swithMSGBtn addTarget:self action:@selector(forceRefresh) forControlEvents:UIControlEventTouchUpInside];
    swithMSGBtn.showsTouchWhenHighlighted = YES;
    swithMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:swithMSGBtn];
    
    
    UIButton *lockMSGBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    lockMSGBtn.frame = CGRectMake(6+65, 20, 65, 55);
    [lockMSGBtn addTarget:self action:@selector(swithToInboxViewAction) forControlEvents:UIControlEventTouchUpInside];
    lockMSGBtn.showsTouchWhenHighlighted = YES;
    lockMSGBtn.backgroundColor = [UIColor clearColor];
    [_editView addSubview:lockMSGBtn];
        
    [_editView show];
}

#pragma mark- ZSTF3EngineDelegate

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    if (  [response.type isEqualToString:@"SyncMsgType"]) {
        if ([_activityView isAnimating]) {
            [_activityView stopAnimating];
            [self createNavigationItem];
        }
        [TKUIUtil alertInView:self.view withTitle:@"同步消息分类失败" withImage:nil];
    }else{
        [TKUIUtil alertInView:self.view withTitle:@"设置失败" withImage:nil];
    }
}

- (void)syncMsgTypeResponse
{
    if ([_activityView isAnimating]) {
        [_activityView stopAnimating];
        [self createNavigationItem];
    }
    [self updateView];
//    [TKUIUtil alertInView:self.view withTitle:@"同步消息分类成功" withImage:nil];
}

- (void)setMsgTypeResponse
{
    MsgTypeInfo *info = [_msgTypeInfo objectAtIndex:_optionShieldView.tag];
    
    [_launcherView setEnable:info.isShielded atIndex:_optionShieldView.tag];
    
    //改变数据库中的屏蔽状态
    [[MsgTypeDao shareMsgTypeDao] setMsgTypeInfo:info.typeID shielded:!info.isShielded];
    info.isShielded = !info.isShielded;
}

@end
