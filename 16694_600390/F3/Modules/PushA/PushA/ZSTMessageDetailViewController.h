//
//  ViewoutViewController.h
//  F3Client
//
//  Created by iPhone on 11-6-16.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTScrollToolBarView.h"
#import <MessageUI/MessageUI.h>

#import "SDWebFileManagerDelegate.h"
#import "SDWebFileDownloaderDelegate.h"

@class ZSTEditView;

@interface ZSTMessageDetailViewController : UIViewController<UIWebViewDelegate,UIAlertViewDelegate,ZSTScrollToolBarViewDelegate,UIGestureRecognizerDelegate,SDWebFileDownloaderDelegate,SDWebFileManagerDelegate, UIActionSheetDelegate,MFMessageComposeViewControllerDelegate>
{
    UIWebView *_webview;
    
	UILabel *_labelTitle;
    UILabel *_sendDate;
    UILabel *_inboxName;
	
	NSString * _filePathString;
	NSString * _msgID;
    NSString * _pushId;
    NSString * _userPhoneNumber;
    NSString * _subject;
    
    TKAsynImageView *_iconImageView;
    
    UIView *_messageDescriptionView;
    
    BOOL webCanGoBack;
    BOOL webCanGoForward;
    BOOL selectFromList;
    
    ZSTScrollToolBarView *_scrollToolBarView;
    
    UIButton *_backButton;
    UIButton *_forwardButton;
    
    UIActivityIndicatorView *_activityIndicatorView;
    
    int start;
    int height;
    int end;
    
    ZSTEditView *_editView;
}

@property (nonatomic, retain) NSString * filePathString;
@property (nonatomic, retain) NSString * msgID;
@property (nonatomic, retain) NSString * pushId;
@property (nonatomic, retain) NSString * userPhoneNumber;
@property (nonatomic, retain) NSString * subject;
@property (nonatomic, retain) TKAsynImageView *iconImageView;
@property (nonatomic, retain) UILabel *sendDate;
@property (nonatomic, retain) UILabel *inboxName;
@property (nonatomic, assign) BOOL lazyLoad;
@property (nonatomic, retain) MessageInfo *curNmsInfo;

- (void)setIconImageViewUrl:(NSString *)iconKey type:(NSString *)type;

@end
