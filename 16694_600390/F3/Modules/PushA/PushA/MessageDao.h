//
//  InboxDao.h
//  F3
//
//  Created by 9588 on 9/26/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MessageInfo.h"
#import "MsgTypeInfo.h"

@interface MessageDao : NSObject {
    
}

+ (MessageDao *)shareMessageDao;

//插入信息
- (NSInteger)insertMessage:(MessageInfo *)nmsInfo;

// 删除所有信息
-(void) deleteAllMessage;

// 删除一条网信
-(void) deleteMessage: (NSInteger) ID;

// 删除过期的信息
-(BOOL) deleteMessageBeforeDate:(NSDate *)date;

// 删除商户所有信息
-(void)deleteMessagesOfType:(NSString *) typeId;

//// 设置网信状态
//-(BOOL) setMessage:(int)ID state:(NMSState)state;

// 设置网信状态
-(BOOL) setInMessage: (NSString *)pushId state:(NMSState)state;

//// 修改信息的锁定状态
//-(BOOL) setMessage:(int)ID isLocked:(BOOL)isLocked;

//修改信息
- (BOOL)updateMessageById:(MessageInfo *)nmsInfo;

//修改信息
- (BOOL)updateInMessageByPushId:(MessageInfo *)nmsInfo;

// 获取网信列表
-(NSArray *) getMessages;

//获取商户网信
-(MessageInfo *) getInMessageByPushId: (NSString *)pushId;

//获取过期信息列表
-(NSArray *) getMessagesBeforeDate:(NSDate *)date;

//获取商户网信列表
-(NSArray *) getInMessagesOfType: (NSString *)typeId;

//获取商户网信列表
-(NSArray *) getMessagesOfState: (NMSState)state;

//获取商户未读网信条数
-(NSUInteger) getCountOfType: (NSString *)typeId state:(NMSState)state;

//获取所有未读f3信息条数
-(NSUInteger) getCount:(NMSState)state;

//获取所有f3信息条数
-(NSUInteger) getCount;

//消息是否存在
- (BOOL)InMessageExist:(NSString *)pushId;

//设置发件状态
- (BOOL)setOutMessage:(NSInteger)ID MSGID:(NSString *)MSGID withState:(NMSState)state;

//设置群发消息状态
- (BOOL)setOutGroupMessage:(int)ID withState:(NMSState)state;

// 修改发件状态
- (BOOL) updateOutMessageMSGID:(NSString *)MSGID withState:(NMSState)state;

//获取重发信息的info
- (MessageInfo *)getOutMessageInfoByID:(int)ID;

@end
