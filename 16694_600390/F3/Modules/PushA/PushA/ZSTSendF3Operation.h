//
//  ZSTSendF3Operation.h
//  F3Engine
//
//  Created by luobin on 4/23/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MessageInfo;

@interface ZSTSendF3Operation : NSOperation

@property (nonatomic, readonly) MessageInfo *messageInfo;

@property (nonatomic, assign) BOOL alertWhenFinish;

@property (nonatomic , retain, readonly) NSError *error;

- (id)initWithF3Message:(MessageInfo *)messageInfo;

@end
