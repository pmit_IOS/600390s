//
//  switchViewController.h
//  F3_UI
//
//  Created by Xu Huijun on 11-6-29.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTLauncherView.h"
#import "ZSTEditView.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTLoginViewController.h"

@class ZSTMessageGroupTabelViewController;
@class ZSTEditView;
@class ZSTF3Engine;

@interface ZSTMessageTypeViewController : ZSTModuleBaseViewController <ZSTLauncherViewDataSource, ZSTLauncherViewDelegate,ZSTF3EngineDelegate,LoginDelegate>
{
    NSMutableArray *_msgTypeInfo;
    ZSTLauncherView *_launcherView;
    ZSTEditView *_optionShieldView;
    ZSTF3Engine *_f3Engine;
    UIActivityIndicatorView *_activityView;
    ZSTEditView *_editView;

}

- (void)refresh;

@end
