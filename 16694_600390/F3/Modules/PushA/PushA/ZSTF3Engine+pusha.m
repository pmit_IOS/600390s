//
//  ZSTF3Engine+News.m
//  News
//
//  Created by luobin on 7/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ZSTF3Engine+pusha.h"
#import "ZSTCommunicator.h"
#import "ZSTF3Preferences.h"
#import "SDWebFileManager.h"
#import "SDFileCache.h"
#import "ZSTLegacyResponse.h"
#import "ZSTSqlManager.h"
#import "ZSTHttpTimeRule.h"
#import "ZipArchive.h"
#import "ZSTUtils.h"
#import "JSON.h"
#import "ElementParser.h"
#import "ZSTLogUtil.h"
#import "MessageInfo.h"
#import "MessageDao.h"
#import "MsgTypeDao.h"
#import "ZSTUtils.h"
#import "ZipArchive.h"
#import "ZSTSendF3Operation.h"
#import "ZSTECMobileClientParams.h"

static NSOperationQueue * sendQueue;

static NSDate *lastPollingTime;

@interface ZSTF3Engine(Pusha_private)

/**
 *	@brief	实际强制执行http轮询方法
 */
- (void)doEnforceHttpPoll;

+ (void)doStartHttpPoll;

+ (void)doHttpPoll;

+ (void)httpPollRunLoopThreadEntry;

/**
 *	@brief	检查是否轮询
 *
 *	@param 	timeTule
 *
 *	@return	YES if need http poll
 */
+ (BOOL) checkIfNeedPolling:(ZSTHttpTimeRule *)timeTule;


/**
 *	@brief	恢复下载状态为"下载中"的信息
 */
+ (void)resumeDownloadMessage;


/**
 *	@brief	恢复下载状态为"发送中"的信息
 */
+ (void)resumeUploadMessage;


/**
 *	@brief	获取f3消息通知，同时会下载资源
 *
 *	@return 是否成功
 */
+ (BOOL) searchNMSPushs;


//获取消息信息
+ (BOOL)receiveNMS:(NSString *)pushId;


+ (BOOL)receiveNMSContent:(NSString *)url params:(NSDictionary *)params nmsInfo:(MessageInfo *)nmsInfo;


+ (void)execServerCommand:(NSString *)command cmdID:(int)cmdID;


+ (void)uploadSysLog:(int)cmdID;


+ (BOOL)getMsgTypeInfo:(NSString *)typeID;


+ (void)reportState:(MessageInfo *)nmsInfo;


+ (BOOL)cacheFile:(NSString *)pushId;


+ (BOOL)changeHtml:(NSURL *)htmlUrl replaceUrlString:(NSString *)replaceUrlString fileName:(NSString *)name;


+ (NSMutableDictionary *)paramsFromElement:(NSArray *)elements;


/**
 *	@brief       下载文件
 *
 *	@param key   根据iconkey
 *
 *	@returns     下载是否成功
 */
//+ (BOOL) downloadFile:(NSString *)key;


@end

@implementation ZSTF3Engine (pusha)

+(void)load
{
    if (sendQueue == nil) {
        sendQueue = [[NSOperationQueue alloc] init];
        sendQueue.maxConcurrentOperationCount = 1;
    }
}

+ (void) startHttpPoll
{
    static BOOL httpPollIsLaunched = NO;
    if (!httpPollIsLaunched) {
        httpPollIsLaunched = YES;
        [self performSelector:@selector(doStartHttpPoll) onThread:self.workThread withObject:nil waitUntilDone:NO];
    }
    TKDINFO(@"Http Poll Thread has been start!");
}

+ (BOOL) checkIfNeedPolling:(ZSTHttpTimeRule *)timeTule
{
    NSDate *curDate = [NSDate date];//获取当前日期
    
    NSDate *theLastPollingDate = [[NSUserDefaults standardUserDefaults] objectForKey:LastPollingTimeKey];
    
    NSArray *dateArray = [timeTule getTimePointsForDate:curDate];
    
    for (NSDate *pollingDate in dateArray) {
        
        //如果存在某个时间在上次轮询和当前时间之间说明需要轮询
        if (theLastPollingDate == nil || ([pollingDate isLaterThanDate:theLastPollingDate] && ![pollingDate isLaterThanDate:curDate])) {
            
            return YES;
        }
    }
    
    //如果最后轮询时间和当前时间不是在同一天，检查上一天是否需要轮询
    if (![theLastPollingDate isEqualToDateIgnoringTime:curDate]) {
        
        curDate = [curDate dateBySubtractingDays:1];
        NSArray *dateArray = [timeTule getTimePointsForDate:curDate];
        
        for (NSDate *pollingDate in dateArray) {
            
            //如果存在某个时间在上次轮询和当前时间之间说明需要轮询
            if ( [pollingDate isLaterThanDate:theLastPollingDate]&& ![pollingDate isLaterThanDate:curDate]) {
                
                return YES;
            }
        }
    }
    return NO;
}

+ (void) doStartHttpPoll
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    [self resumeDownloadMessage];
    [self resumeUploadMessage];
    
    NSTimer *timer = [[NSTimer alloc] initWithFireDate:[NSDate date] interval:NMS_UPDATE_LIST_INTERVAL target:self selector:@selector(doHttpPoll) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:timer forMode:NSRunLoopCommonModes];
    [timer release];
    
    [pool release];
}

+ (void)doHttpPoll
{
    ZSTHttpTimeRule *timeTule = [[ZSTHttpTimeRule alloc] initWithHTTPPollTimeRule:[ZSTF3Preferences shared].HTTPPollTimeRule];
    
    if ( [self checkIfNeedPolling:timeTule]) {
        [self searchNMSPushs];
    }
    
    [timeTule release];
}

- (void)doEnforceHttpPoll
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    
    if ([ZSTF3Engine searchNMSPushs]) {
        if ([self isValidDelegateForSelector:@selector(enforceHttpPollResponse)]) {
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(enforceHttpPollResponse) withObject:nil waitUntilDone:NO];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(requestDidFail:) withObject:nil waitUntilDone:NO];
        }
    }
    [pool release];
}

- (void)enforceHttpPoll
{
    [self performSelector:@selector(doEnforceHttpPoll) onThread:[ZSTF3Engine workThread] withObject:nil waitUntilDone:NO];
    
}

- (void)doReReceiveMessage:(NSString *)pushId
{
    if(![ZSTF3Engine receiveNMS:pushId])
    {
        if ([self isValidDelegateForSelector:@selector(enforceHttpPollResponse)]) {
#warning 原代码是@selector(receiveNMSResponse) 但是没有receiveNMSResponse这个方法
            //可能是receiveNMS:这个方法
//            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(receiveNMSResponse) withObject:nil waitUntilDone:NO];
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(receiveNMS:) withObject:nil waitUntilDone:NO];

        }
    } else {
        if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
            [(NSObject *)self.delegate performSelectorOnMainThread:@selector(requestDidFail:) withObject:nil waitUntilDone:NO];
        }
    }
}

- (void)reReceiveMessage:(NSString *)pushId
{
    [self performSelector:@selector(doReReceiveMessage:) onThread:[ZSTF3Engine workThread] withObject:pushId waitUntilDone:NO];
}

//恢复下载状态为"下载中"的信息
+ (void)resumeDownloadMessage
{
    NSArray *messages = [[MessageDao shareMessageDao] getMessagesOfState:NMSStateReceiving];
    for (MessageInfo *message in messages) {
        [self receiveNMS:message.pushId];
    }
}

//恢复下载状态为"发送中"的信息
+ (void)resumeUploadMessage
{
    NSArray *messages = [[MessageDao shareMessageDao] getMessagesOfState:NMSStateSending];
    for (MessageInfo *message in messages) {
        [self sendF3:message alertWhenFinish:NO];
    }
}

//获取消息内容
+ (BOOL)receiveNMSContent:(NSString *)url params:(NSDictionary *)params nmsInfo:(MessageInfo *)nmsInfo
{
    NSString *inboxPath = [ZSTUtils pathForInbox];
    NSString *zipPath = [inboxPath stringByAppendingPathComponent:@"inbox.zip"];
    
    BOOL bSuccess = [[ZSTLegicyCommunicator shared] downloadFileSynToMethod:url params:nil destinationFile:zipPath timeOutSeconds:180];
    
    if (!bSuccess || ![[NSFileManager defaultManager] fileExistsAtPath:zipPath]) {
        return NO;
    }
    
    NSString *unzipPath = [inboxPath stringByAppendingPathComponent: nmsInfo.pushId];
    
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    
    if (![zipArchive UnzipOpenFile: zipPath]) {
        [zipArchive CloseZipFile2];
        [zipArchive release];
        return NO;
    };
    
    [[NSFileManager defaultManager] removeItemAtPath:unzipPath error:nil];
    
    NSStringEncoding enc = CFStringConvertEncodingToNSStringEncoding (kCFStringEncodingGB_18030_2000);
    if (![zipArchive UnzipFileTo: unzipPath overWrite: YES fileNameEncoding:enc]) {
        [zipArchive CloseZipFile2];
        [zipArchive release];
        return NO;
    }
    
    [zipArchive CloseZipFile2];
    [zipArchive release];
    
    if ([[params allKeys] count] != 0) {
        
        NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager] enumeratorAtPath:unzipPath];
        NSString *fileName;
        while ((fileName = [enumerator nextObject]) != nil) {
            
            NSString *path = [unzipPath stringByAppendingPathComponent:fileName];
            
            if ([ZSTUtils isHTMLPath:fileName]) {
                NSError *error = nil;
                NSString *content = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:&error];
                if (error != nil) {
                    
                    continue;
                }
                
                NSArray *keys = [params allKeys];
                for (NSString *key in keys) {
                    content = [content stringByReplacingOccurrencesOfString:[NSString stringWithFormat:@"$%@$", key] withString:[params objectForKey:key]];
                }
                
                error = nil;
                [content writeToFile:path atomically:YES encoding:NSUTF8StringEncoding error:&error];
                if (error != nil) {
                    NSLog(@"write file has occur an error. file:%@, errorCode:%ld, localizedDescription = %@", path, (long)error.code, error.localizedDescription);
                }
            }
        }
    }
    
    if (![self cacheFile:nmsInfo.pushId]) {
        
        return  NO;
    }
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (void)reportState:(MessageInfo *)nmsInfo
{
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
    NSString * content = [NSString stringWithFormat:@"<Log>[%@][VIEW][Iphone][%@][%@][%@][%ld]</Log>",
                          [ZSTUtils formatDate:[NSDate date] format:@"yyyy-MM-dd"],[ZSTF3Preferences shared].loginMsisdn, nmsInfo.MSGID, nmsInfo.pushId, (long)nmsInfo.state];
    BOOL bSuccess = [self submitUserLog:content];
    if (!bSuccess) {
        [ZSTLogUtil logUserView:[NSString stringWithFormat:@"%d", nmsInfo.state==NMSStateNotRead?1:2] pushId:nmsInfo.pushId messageId:nmsInfo.MSGID];
    }
    [pool release];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)changeHtml:(NSURL *)htmlUrl replaceUrlString:(NSString *)replaceUrlString fileName:(NSString *)name
{
    NSString *htmlData = [NSString stringWithContentsOfURL:htmlUrl encoding:NSUTF8StringEncoding error:nil];
    
    NSURL *replaceUrl = [NSURL URLWithString:replaceUrlString];
    
    if (![replaceUrl isFileURL]) {
        
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
        NSString * diskCachePath = SDWIReturnRetained([[paths objectAtIndex:0] stringByAppendingPathComponent:@"ImageCache"]);
        
        NSString *fileLocalPath = [diskCachePath stringByAppendingPathComponent:name];
        
        SDWebFileManager *manager = [SDWebFileManager sharedManager];
        SDFileCache *fileCache = [SDFileCache sharedFileCache];
        NSData *fileData = [fileCache fileFromKey:replaceUrlString withFilePath:fileLocalPath];
        
        if ( !fileData) {
            [manager downloadWithURL:[NSURL URLWithString:replaceUrlString] delegate:(id<SDWebFileManagerDelegate>)self withFilePath:fileLocalPath];
        }
        
        htmlData =  [htmlData stringByReplacingOccurrencesOfString:replaceUrlString withString:fileLocalPath];
        
        [[NSFileManager defaultManager] removeItemAtURL:htmlUrl error:nil];
        [htmlData writeToURL:htmlUrl atomically:YES encoding:NSUTF8StringEncoding error:nil];
        
        SDWIRelease(diskCachePath);
    }
    return YES;
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL) cacheFile:(NSString *)pushId
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *inboxPath = [ZSTUtils pathForInbox];
    NSString *messagePath = [inboxPath stringByAppendingPathComponent: pushId];
    NSString *nmsconfigPath = [messagePath stringByAppendingPathComponent:@"NMSConfig.xml"];
    
    if (![fileManager fileExistsAtPath:nmsconfigPath]) {
        return YES;
    }
    
    NSArray *fileList = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:messagePath error:nil];
    
    NSMutableArray *htmlPathArray = [NSMutableArray array];
    
    for (NSString *file in fileList) {
        NSString *path = [messagePath stringByAppendingPathComponent:file];
        if ([fileManager fileExistsAtPath:path] && [file hasSuffix:@"htm"]) {
            [htmlPathArray addObject:path];
        }
    }
    
    NSString *responseString= [NSString stringWithContentsOfFile:nmsconfigPath encoding:NSUTF8StringEncoding error:nil];
    
    DocumentRoot * element = [Element parseXML:responseString];
    
    NSArray *localcaches = [element selectElements:@"NMSConfig Resources Resource"];
    
    for (Element *e in localcaches)
    {
        if ([[e attribute:@"Type"] isEqualToString:@"localcache"] && [e attribute:@"Type"] != nil) {
            
            NSString *LCName = [e attribute:@"LCName"];
            NSString *Url = [e attribute:@"Url"];
            NSString *File = [e attribute:@"File"];
            
            if (![fileManager fileExistsAtPath:[messagePath stringByAppendingPathComponent:File]])
            {
                for (NSString * htmlPath in htmlPathArray) {
                    BOOL success = [self changeHtml:[NSURL fileURLWithPath:htmlPath] replaceUrlString:Url fileName:LCName];
                    if (!success) {
                        return NO;
                    }
                }
            }
        }
    }
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSMutableDictionary *)paramsFromElement:(NSArray *)elements
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    for (Element *e in elements) {
        NSString *key = [[e attribute:@"Name"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        if (key != nil && [key length] != 0) {
            NSString *value = [[e attribute:@"Value"] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
            [params setObject:value forKey:key];
        }
    }
    return params;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//获取消息信息
+ (BOOL)receiveNMS:(NSString *)pushId
{
    
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LOGINMSISDN"];
    [params setSafeObject:pushId forKey:@"WID"];
    [params setSafeObject:@"0" forKey:@"f"];
    [params setSafeObject:[NSNumber numberWithInt:preferences.packageType] forKey:@"NMSPackageType"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIGetToMethod:RECEIVE_NMS_URL_PATH params:params timeOutSeconds:30];
    
    if (response.error) {
        return NO;
    }
    
    DocumentRoot * element = response.xmlResponse;
    
    MessageInfo *nmsInfo = [[[MessageInfo alloc] init] autorelease];
    nmsInfo.MSGID = CONTENTSTEXT("Response MSGInfo MSGID");
    nmsInfo.pushId = pushId;
    nmsInfo.state = NMSStateReceiving;
    nmsInfo.userID = CONTENTSTEXT("Response MSGInfo FromMsisdn");
    nmsInfo.userName = CONTENTSTEXT("Response MSGInfo ECName");
    nmsInfo.subject = CONTENTSTEXT("Response MSGInfo Subject");
    NSString *sendTime = CONTENTSTEXT("Response MSGInfo SendTime");
    nmsInfo.typeId = CONTENTSTEXT("Response MSGInfo MsgTypeID");
    nmsInfo.time = [ZSTUtils convertToDate:sendTime];
    nmsInfo.isReceived = YES;
    nmsInfo.isLocked = NO;
    nmsInfo.isPrivate = NO;
    nmsInfo.encryptKey = CONTENTSTEXT("Response MSGInfo EncryptKey");
    nmsInfo.report = CONTENTSBOOL("Response MSGInfo Report");
    
    //更新消息类型
    if (nmsInfo.typeId && ![nmsInfo.typeId isEqualToString:PERSONAL_MSGTYPE]) {
        int msgTypeVersion = CONTENTSINT("Response MSGInfo MsgTypeVersion");
        
        MsgTypeInfo *info = [[MsgTypeDao shareMsgTypeDao] getMsgTypeInfoByTypeID:nmsInfo.typeId];
        if (info == nil || info.version < msgTypeVersion) {
            [self getMsgTypeInfo:nmsInfo.typeId];
        }
    }
    
    BOOL bSuccess = NO;
    if ([[MessageDao shareMessageDao] InMessageExist:pushId]) {
        bSuccess = [[MessageDao shareMessageDao] updateInMessageByPushId:nmsInfo];
    } else {
        bSuccess = ([[MessageDao shareMessageDao] insertMessage:nmsInfo] != -1);
    }
    
    if (bSuccess) {
        
        postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
        
        NSArray *varElements = [element selectElements:@"Response MSGTemplates Var"];
        NSMutableDictionary *replacedParams = [self paramsFromElement:varElements];
        
        NSString *CDNUrl = CONTENTSTEXT("Response MSGInfo CDNUrlFromV09");
        if (CDNUrl == nil || [CDNUrl length] == 0) {
            
            NSMutableDictionary *params = [NSMutableDictionary dictionary];
            [params setSafeObject:pushId forKey:@"WID"];
            [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
            [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
            
            CDNUrl = [RECEIVE_NMS_CONTENT_URL_PATH stringByAddingQuery:params];
        }
        
        BOOL bSuccess = [self receiveNMSContent:CDNUrl params:replacedParams nmsInfo:nmsInfo];
        
        if (bSuccess) {
            
            nmsInfo.state = NMSStateNotRead;
            [[MessageDao shareMessageDao] setInMessage:pushId state:NMSStateNotRead];
            
            postNotificationOnMainThreadNoWait(NotificationName_UnreadNMSChanged);
            postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
            [ZSTUtils playNMSAlert];
            
            if (nmsInfo.report) {
                [self reportState:nmsInfo];
            }
            
            return YES;
        }
    }
    
    nmsInfo.state = NMSStateReceivedFailed;
    [[MessageDao shareMessageDao] setInMessage:pushId state:NMSStateReceivedFailed];
    postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
    [ZSTUtils playNMSAlert];
    
    return NO;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// 获取f3消息通知，同时会下载资源
+ (BOOL) searchNMSPushs
{
    [lastPollingTime release];
    lastPollingTime = [[NSDate date] retain];
    
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    
    NSString * SearchType = @"1";
    NSString * DaySpan = @"30";
    NSString * reportMsgIdString = @"8,9,10,11,12,13";
    
#pragma unused(SearchType)
#pragma unused(DaySpan)
    
    if (preferences.reportState) {
        NSMutableArray *reportArray = [NSMutableArray array];
        [reportArray addObjectsFromArray:[[MessageDao shareMessageDao] getMessagesOfState:NMSStateSended]];
        [reportArray addObjectsFromArray:[[MessageDao shareMessageDao] getMessagesOfState:NMSStateHaveArrived]];
        if ([reportArray count] != 0) {
            NSMutableArray *msgIdArray = [NSMutableArray array];
            for (MessageInfo *message in reportArray) {
                if (message.MSGID) {
                    [msgIdArray addObject:message.MSGID];
                }
            }
            reportMsgIdString = [msgIdArray componentsJoinedByString:@","];
        }
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *loginMsisdn = @"";
    if (!preferences.loginMsisdn || [preferences.loginMsisdn isEqualToString:@""]) {
        loginMsisdn = preferences.UserId;
    }
    else
    {
        loginMsisdn = preferences.loginMsisdn;
    }
    
    [params setSafeObject:loginMsisdn forKey:@"LOGINMSISDN"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:reportMsgIdString forKey:@"ReportMsgId"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:GETNMSLIST_URL_PATH replacedParams:params];
    if (response.error) {
        return NO;
    }
    
    DocumentRoot * element = response.xmlResponse;
    
    NSArray * NMSList = [element selectElements:@"Response NMSPushs NMSPush"];
    
    BOOL messagesDownloadSuccess = YES;
    for (Element *e in NMSList)
    {
        NSString *pushId = [[[e attribute:@"Content"] lastPathComponent] stringByDeletingPathExtension];
        if (pushId != nil && [pushId length] != 0) {
            
            if ([[MessageDao shareMessageDao] InMessageExist:pushId]) {
                continue;
            }
            if(![self receiveNMS:pushId])
            {
                messagesDownloadSuccess = NO;
            }
        }
    }
    
    //更新最后更新时间
    if (messagesDownloadSuccess) {
        [[NSUserDefaults standardUserDefaults] setObject:lastPollingTime forKey:LastPollingTimeKey];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //执行服务器指令
    NSArray *commands = [element selectElements:@"Response Commands Cmd"];
    for (Element *e in commands) {
        NSString *command = [[e contentsText] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        int cmdID = [[e attribute:@"id"] intValue];
        if (command != nil && [command length] != 0) {
            [self execServerCommand:command cmdID:cmdID];
        }
    }
    
    //状态报告
    BOOL shouldRefresh = NO;
    NSArray *Reports = [element selectElements:@"Response Reports Report"];
    for (Element *e in Reports) {
        NSString * MsgId = [e attribute:@"MsgId"];
        int Status = [[e attribute:@"Status"] intValue];
        if (MsgId != nil && [MsgId length] != 0 &&(Status == NMSStateHaveArrived || Status == NMSStateHaveRead)) {
            [[MessageDao shareMessageDao] updateOutMessageMSGID:MsgId withState:Status];
            shouldRefresh = YES;
        }
    }
    
    if (shouldRefresh) {
        postNotificationOnMainThreadNoWait(NotificationName_NMSListUpdated);
        postNotificationOnMainThreadNoWait(NotificationName_SwitchNMSListUpdated);
    }
    
    return messagesDownloadSuccess;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (void)execServerCommand:(NSString *)command cmdID:(int)cmdID
{
    //上传系统日志
    if ([command isEqualToString:@"CMD_UPLOAD_SYSTEM_LOG"]) {
        
        [self uploadSysLog:cmdID];
    }
    //上传输出日志
    else if([command isEqualToString:@"CMD_UPLOAD_INFO_LOG"])
    {
        
        
    }
    //上传联网日志
    else if([command isEqualToString:@"CMD_UPLOAD_CONNECT_LOG"])
    {
        
    }
    //上传查看日志
    else if([command isEqualToString:@"CMD_UPLOAD_VIEW_LOG"])
    {
        
    }
    //上传操作日志
    else if([command isEqualToString:@"CMD_UPLOAD_ACTION_LOG"])
    {
        
    }
    //上传IPPush日志
    else if([command isEqualToString:@"CMD_UPLOAD_IPPUSH_LOG"])
    {
        
    }
    //上传所有日志
    else if([command isEqualToString:@"CMD_UPLOAD_ALL_LOG"])
    {
        
    }
    //同步用户信息
    else if([command isEqualToString:@"CMD_SYNC_INFO_CENTER"])
    {
        
    }
    //立即检查更新
    else if([command isEqualToString:@"CMD_CHECK_UPDATE"])
    {
        
    }
    //锁定当前用户使用
    else if([command isEqualToString:@"CMD_LOCK_USER"])
    {
        
    }
    //解锁当前用户使用
    else if([command isEqualToString:@"CMD_UNLOCK_USER"])
    {
        
    }
    //初始化客户端，需要重新绑定、同步服务器信息等所有信息
    else if([command isEqualToString:@"CMD_RESET"])
    {
        
    }
    //需要重新获取服务器信息，但保留用户相关信息
    else if([command isEqualToString:@"CMD_RESET_SERVER"])
    {
        
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (void)uploadSysLog:(int)cmdID
{
    NSString *zipPath = [[ZSTUtils pathForECECC] stringByAppendingPathComponent: @"logs.zip"];
    ZipArchive *zipArchive = [[ZipArchive alloc] init];
    [zipArchive CreateZipFile2: zipPath];
    NSDirectoryEnumerator *enums = [[NSFileManager defaultManager] enumeratorAtPath: [ZSTUtils pathForLogs]];
    NSString *fileName = nil;
    while ((fileName = [enums nextObject]) != nil) {
        
        NSString *logPath = [[ZSTUtils pathForLogs] stringByAppendingPathComponent:fileName];
        [zipArchive addFileToZip: logPath newname: fileName];
    }
    [zipArchive CloseZipFile2];
    [zipArchive release];
    NSData *data = [NSData dataWithContentsOfFile:zipPath];
    [self uploadFile:data cmdId:cmdID extension:@"log"];
    [[NSFileManager defaultManager] removeItemAtPath:zipPath error:nil];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSInteger)saveF3Message:(MessageInfo *)messageInfo error:(NSError **)error
{
    NSInteger ID = [[MessageDao shareMessageDao] insertMessage:messageInfo];
    if (ID > 0 && messageInfo.ID <= 0) {
        NSArray *attachmentFileNames = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:[ZSTUtils pathForTempFinderOfOutbox] error: nil];
        if ([attachmentFileNames count]) {
            NSError *theError = nil;
            BOOL bSuccess = [[NSFileManager defaultManager] moveItemAtPath:[ZSTUtils pathForTempFinderOfOutbox] toPath:[ZSTUtils pathForSendFileOfOutbox:ID] error:&theError];
            if (!bSuccess) {
                TKDERROR(@"Copy file:'%@' toPath '%@' failed.\n%@", [ZSTUtils pathForTempFinderOfOutbox], [ZSTUtils pathForSendFileOfOutbox:ID], theError);
                if (error) {
                    *error = theError;
                }
                return -1;
            }
        }
    } else if (ID <= 0){
        
        return -1;
    }
    return ID;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
+ (void)sendF3:(MessageInfo *)messageInfo alertWhenFinish:(BOOL)flag
{
    ZSTSendF3Operation *sender = [[ZSTSendF3Operation alloc] initWithF3Message:messageInfo];
    sender.alertWhenFinish = flag;
    [sendQueue addOperation:sender];
    [sender release];
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)getUploadFileInfo:(unsigned long long)fileSize fileId:(NSString **)fileId  location:(unsigned long long *)location
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:*fileId forKey:@"fileId"];
    [params setSafeObject:[NSNumber numberWithUnsignedLongLong:fileSize] forKey:@"FileSize"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIGetToMethod:GETUPLOADFILEINFO_PATH params:params timeOutSeconds:30];
    DocumentRoot * element = response.xmlResponse;
    if (response.error) {
        [TKUIUtil alertInWindow:@"信息发送失败" withImage:[UIImage imageNamed:@"icon_warning.png"]];
        return NO;
    }
    *fileId = CONTENTSTEXT("Response FileId");
    *location = CONTENTSUNSIGNEDLONGLONG("Response Location");
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (BOOL)uploadFileBreakPoint:(NSString *)fileId data:(NSData *)data location:(unsigned long long)location progressDelegate:(id)progress
{
    NSData *newData = [data subdataWithRange:NSMakeRange(location, [data length]-location)];//断点续传内容获取
    
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *url = UPLOADFILEBREAKPOINT_PATH;
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:fileId forKey:@"FileId"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] uploadFileSynToMethod:url data:newData params:params progressDelegate:progress];
    if (response.error) {
        return NO;
    }
    return YES;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSString *)sendF3:(NSString *)user
             forward:(NSString *)forwardId
             subject:(NSString *)subject
             content:(NSString *)content
              fileId:(NSString *)fileId
         attachments:(NSArray *)attachments
              report:(BOOL) report
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:user forKey:@"ToUserID"];
    [params setSafeObject:subject forKey:@"Subject"];
    [params setSafeObject:(report ? @"1": @"0") forKey:@"Report"];
    [params setSafeObject:content forKey:@"Content"];
    [params setSafeObject:fileId forKey:@"AttachmentId"];
    [params setSafeObject:forwardId?forwardId:@"" forKey:@"OriMSGID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    NSMutableString *attachmentString = [NSMutableString string];
    for (NSString *name in attachments) {
        [attachmentString appendFormat:@"<Attachment Name=\"%@\"/>\n",name];
    }
    [params setSafeObject:attachmentString forKey:@"Attachments"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SENDNMS_URL_PATH replacedParams:params];
    if (response.error) {
        return nil;
    }
    
    DocumentRoot *element = response.xmlResponse;
    NSString *MSGID = CONTENTSTEXT("Response MSGID");
    return MSGID?MSGID:@"";
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    if ([self isValidDelegateForSelector:@selector(requestDidFail:)]) {
        [self.delegate requestDidFail:response];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)syncMsgTypeResponse:(ZSTLegacyResponse *)response
{
    DocumentRoot *element = response.xmlResponse;
    
    NSArray *MsgTypeElements = [element selectElements:@"Response MsgTypes MsgType"];
    
    if ([MsgTypeElements count]) {
        [[MsgTypeDao shareMsgTypeDao] deleteMsgTypeInfoWhichNoMessage];
    }
    
    for (Element *element in MsgTypeElements) {
        
        NSString *typeID = CONTENTSTEXT("MsgTypeID");
        if (typeID != nil) {
            
            MsgTypeInfo *info = [[MsgTypeInfo alloc] init];
            info.typeID = typeID;
            info.ecId = CONTENTSTEXT("ECID");
            info.typeName = CONTENTSTEXT("Name");
            info.iconKey = CONTENTSTEXT("IconKey");
            info.isShielded = CONTENTSBOOL("Status");
            info.version = CONTENTSINT("Version");
            info.orderMum = CONTENTSINT("Order");
            if ([[MsgTypeDao shareMsgTypeDao] insertOrReplaceMsgTypeInfo:info]) {
                
            }
            
            [info release];
        }
    }
    
    if ([self isValidDelegateForSelector:@selector(syncMsgTypeResponse)]) {
        [self.delegate syncMsgTypeResponse];
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)syncMsgType
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *loginMsisdn = @"";
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (!preferences.loginMsisdn || [preferences.loginMsisdn isEqualToString:@""])
    {
        loginMsisdn = preferences.UserId;
    }
    else
    {
        loginMsisdn = preferences.loginMsisdn;
    }
    
    [params setSafeObject:loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:SYNCMSGTYPE_URL_PATH
                                   replacedParams:params
                                         delegate:(id<ZSTLegicyCommunicatorDelegate>)self
                                     failSelector:@selector(requestDidFail:)
                                  succeedSelector:@selector(syncMsgTypeResponse:)
                                         userInfo:nil];
}

- (void)setMsgTypeResponse:(ZSTLegacyResponse *)response
{
    if ([self isValidDelegateForSelector:@selector(setMsgTypeResponse)]) {
        [self.delegate setMsgTypeResponse];
    }
}

- (void)setMsgType:(NSString *)typeID action:(MsgTypeSetAction)action
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:typeID forKey:@"typeID"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:[NSString stringWithFormat:@"%d", action] forKey:@"Action"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    [[ZSTLegicyCommunicator shared] openAPIPostToMethod:MSGTYPESET_URL_PATH
                                   replacedParams:params
                                         delegate:(id<ZSTLegicyCommunicatorDelegate>)self
                                     failSelector:@selector(requestDidFail:)
                                  succeedSelector:@selector(setMsgTypeResponse:)
                                         userInfo:nil];
}



//+ (BOOL) downloadFile:(NSString *)key
//{
//    if (key==nil||[key length]==0) {
//        return YES;
//    }
//
//    if ([[FileDao shareFileDao] fileExist:key]) {
//        return YES;
//    }
//    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
//    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
//    [params setSafeObject:preferences.ECECCID forKey:@"ECECCID"];
//    [params setSafeObject:key forKey:@"FileID"];
//    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
//
//    ZSTLegacyResponse *response = [[ZSTCommunicator shared] openSynAPIPostToMethod:[ZSTUtils httpSever:DOWNLOADFILE_URL_PATH]
//                                                              replacedParams:params];
//    if (response.error) {
//        return NO;
//    }
//
//    DocumentRoot * element = response.xmlResponse;
//
//    FileInfo *info = [[[FileInfo alloc] init] autorelease];
//    info.key = key;
//    NSString *FileString = CONTENTSTEXT("Response FileData");
//    info.data = [NSData dataWithBase64EncodedString:FileString];
//
//    info.suffix = CONTENTSTEXT("Response FileExtension");
//    info.updateTime = [NSDate date];
//
//    return [[FileDao shareFileDao] insertFileInfo:info];
//}
//
//+ (NSData *) downloadNewsImageData:(NSString *)fileID
//{
//    NSString *fileString = @"http://192.168.16.170:17911/Home/GetFile";
//
//    ZSTLegacyResponse *response = [[ZSTCommunicator shared] openSynAPIPostToMethod:[fileString stringByAppendingFormat:@"?FileID=%@",@"2"]
//                                                              replacedParams:nil];
//
//
//    if (response.error) {
//        return NO;
//    }
//
//    DocumentRoot * element = response.xmlResponse;
//
////    FileInfo *info = [[[FileInfo alloc] init] autorelease];
////    info.key = key;
////    NSString *FileString = CONTENTSTEXT("Response FileData");
////    info.data = [NSData dataWithBase64EncodedString:FileString];
////
////    info.suffix = CONTENTSTEXT("Response FileExtension");
////    info.updateTime = [NSDate date];
////
////    return [[FileDao shareFileDao] insertFileInfo:info];
//}


+ (BOOL) getMsgTypeInfo:(NSString *)typeID
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:typeID forKey:@"typeID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:GETMSGTYPEINFO_URL_PATH
                                                              replacedParams:params];
    if (response.error) {
        return NO;
    }
    
    DocumentRoot * element = response.xmlResponse;
    
    MsgTypeInfo *info = [[MsgTypeInfo alloc] init];
    info.typeID = typeID;
    info.typeName = CONTENTSTEXT("Response Name");
    info.ecId = CONTENTSTEXT("Response ECID");
    info.iconKey = CONTENTSTEXT("Response IconKey");
    info.version = CONTENTSINT("Response Version");
    info.orderMum = CONTENTSINT("Response Order");
    info.isShielded = NO;
    
    BOOL bSuccess = [[MsgTypeDao shareMsgTypeDao] insertOrReplaceMsgTypeInfo:info];
    
    if (bSuccess) {
        //        bSuccess = [ZSTF3Engine downloadFile:info.iconKey];
    }
    
    [info release];
    
    return bSuccess;
}

+ (NSString *) uploadFile:(NSData *)data cmdId:(int)cmdID extension:(NSString *)extension
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    NSString * CmdIDString= [NSString stringWithFormat:@"%d", cmdID];
	NSString * FileDataString = [data base64Encoding];
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:preferences.loginMsisdn forKey:@"LoginMsisdn"];
    [params setSafeObject:preferences.ECECCID forKey:@"ECID"];
    [params setSafeObject:FileDataString forKey:@"FileData"];
    [params setSafeObject:extension forKey:@"FileExtension"];
    [params setSafeObject:CmdIDString forKey:@"CmdID"];
    [params setSafeObject:[ZSTUtils md5Signature:params] forKey:@"MD5Verify"];
    
    ZSTLegacyResponse *response = [[ZSTLegicyCommunicator shared] openSynAPIPostToMethod:SUBMITSYSTEMLOG_URL_PATH
                                                              replacedParams:params];
    
	
    DocumentRoot * element = response.xmlResponse;
    
    int Result = CONTENTSINT("Response Result");
    if (Result == REQUEST_CODE_OK) {
        return CONTENTSTEXT("Response FileID");
    }
	return nil;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////未使用

//- (BOOL)requestUpdateClientToken:(NSString *)deviceToken
//{
//    NSString *lastDeviceToken = [[NSUserDefaults standardUserDefaults] stringForKey:DeviceTokenKey];
//
//    if (deviceToken == lastDeviceToken) {
//        return YES;
//    }
//
//    ASIFormDataRequest *request = [[[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[ZSTUtils httpSever:UPDATECLIENTTOKEN_URL_PATH]]] autorelease];
//
//    NSString * updateClientTokenStr = [ZSTF3Preferences stringFromBundlePath:@"UpdateClientToken.xml"];
//
//    updateClientTokenStr = [updateClientTokenStr stringByReplacingOccurrencesOfString:@"${LoginMsisdn}" withString:self.loginMsisdn];
//
//	updateClientTokenStr = [updateClientTokenStr stringByReplacingOccurrencesOfString:@"${ECECCID}" withString:self.ECECCID];
//
//	updateClientTokenStr = [updateClientTokenStr stringByReplacingOccurrencesOfString:@"${DeviceToken}" withString:deviceToken];
//
//    NSString * MD5OriString = [NSString stringWithFormat:@"LoginMsisdn=%@&Token=%@&LoginPassword=%@", self.loginMsisdn, deviceToken, self.loginMsisdn];
//	NSString * MD5NowString = [[MD5OriString md5] uppercaseString];
//	updateClientTokenStr = [updateClientTokenStr stringByReplacingOccurrencesOfString:@"${MD5Verify}" withString:MD5NowString];
//
//	request.postBody = (NSMutableData *) [updateClientTokenStr dataUsingEncoding:NSUTF8StringEncoding];
//	[request startSynchronous];
//
//    if (request.error == nil) {
//        DocumentRoot * nmsDoc = [Element parseXML:[request responseString]];
//        Element *resultElement = [nmsDoc selectElement: @"Response Result"];
//        int result = [[resultElement contentsText] intValue];
//        if (result == 1) {
//            [[NSUserDefaults standardUserDefaults] setObject:deviceToken forKey:DeviceTokenKey];
//            [[NSUserDefaults standardUserDefaults] synchronize];
//            return YES;
//        }
//    }
//
//    return NO;
//}
//
//

+ (void) deleteMessage:(MessageInfo *) nmsInfo
{
    // 从文件系统中删除
    NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
    [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
    
    NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
    [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
    // 从数据库中删除
    [[MessageDao shareMessageDao] deleteMessage:nmsInfo.ID];
    
}

+ (void) deleteAllMessage
{
    NSArray *nmss = [[MessageDao shareMessageDao] getMessages];
    for (MessageInfo *nmsInfo in nmss) {
        if (!nmsInfo.isLocked) {
            // 从文件系统中删除
            NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
            [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
            
            NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
            [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
            
        }
    }
    // 从数据库中删除
    [[MessageDao shareMessageDao] deleteAllMessage];
}

+ (void) deleteMessagesOfType:(NSString *)typeID
{
    NSArray *nmss = [[MessageDao shareMessageDao] getInMessagesOfType:typeID];
    for (MessageInfo *nmsInfo in nmss) {
        
        if (!nmsInfo.isLocked) {
            // 从文件系统中删除
            NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
            
            [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
            
            NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
            [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
        }
    }
    [[MessageDao shareMessageDao] deleteMessagesOfType:typeID];
}

+ (void) deleteExpiredMessage
{
    //这里得到过期短信，在程序内部得到，现在得时间得前15天保留
    NSTimeInterval  interval = 24*60*60*15;
    NSDate *date = [NSDate dateWithTimeIntervalSinceNow:-interval]; //前15天的日期
    NSArray *nmss = [[MessageDao shareMessageDao] getMessagesBeforeDate:date];
    for (MessageInfo *nmsInfo in nmss) {
        
        // 从文件系统中删除
        NSString *nmsPath = [[ZSTUtils pathForInbox] stringByAppendingPathComponent: [NSString stringWithFormat: @"%@", nmsInfo.pushId]];
        [[NSFileManager defaultManager] removeItemAtPath: nmsPath error: nil];
        
        NSString *attachmentPath = [ZSTUtils pathForSendFileOfOutbox:nmsInfo.ID];
        [[NSFileManager defaultManager] removeItemAtPath: attachmentPath error: nil];
        
    }
    
    [[MessageDao shareMessageDao] deleteMessageBeforeDate:date];
    [TKUIUtil alertInWindow:@"清理完成" withImage:[UIImage imageNamed:@"icon_smile_face.png"]];
}

@end
