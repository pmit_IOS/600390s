//
//  ECECCDao.m
//  F3
//
//  Created by 9588 on 9/26/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "MsgTypeDao.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "ZSTF3Preferences.h"

#define isPad (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)


@implementation MsgTypeDao

+ (MsgTypeDao *)shareMsgTypeDao
{
    static MsgTypeDao *dao;
    if (dao == nil) {
        @synchronized(self) {
            if (dao == nil) {
                dao = [[MsgTypeDao alloc] init];
            }
        }
    }
    return dao;
}

-(void)deleteMsgTypeInfo:(NSString *)typeID
{    
    // 从数据库中删除
    NSString * sql =  @"Delete from MsgTypeInfo where typeId = ?";
    [ZSTSqlManager executeUpdate:sql, typeID];
}

-(void)deleteAllMsgTypeInfo
{
    NSString * sql =  @"Delete from MsgTypeInfo";
    [ZSTSqlManager executeUpdate:sql];
}

- (void)deleteMsgTypeInfoWhichNoMessage
{
    NSString *sql = @"Delete from MsgTypeInfo where not exists ( select typeId from messageInfo where messageInfo.typeId = MsgTypeInfo.typeId )";
    [ZSTSqlManager executeUpdate:sql];
}

- (BOOL)insertMsgTypeInfo:(MsgTypeInfo *)info
{
    NSString *sql = @"Insert into MsgTypeInfo (typeId,typeName,ecId,iconKey,Version, isShielded)" 
    " values (?, ?, ?, ?, ?, ?) ";
    return [ZSTSqlManager executeUpdate:sql,
                                              info.typeID ,
                                              info.typeName,
                                              [ZSTUtils objectForArray:info.ecId] ,
                                              info.iconKey,
                                              [NSNumber numberWithInteger:info.version],
                                              [NSNumber numberWithBool:info.isShielded]
     ];
}

- (BOOL)insertOrReplaceMsgTypeInfo:(MsgTypeInfo *)info
{    
    NSString *sql = @"Insert or replace into MsgTypeInfo (typeId,typeName,ecId,iconKey,Version , OrderMum, isShielded) values (?, ?, ?, ?, ?, ?, ?) ";
    return [ZSTSqlManager executeUpdate:sql,
                                              info.typeID ,
                                              info.typeName,
                                              [ZSTUtils objectForArray:info.ecId],
                                              info.iconKey, 
                                              [NSNumber numberWithInteger:info.version],
                                              [NSNumber numberWithInteger:info.orderMum],
                                              [NSNumber numberWithBool:info.isShielded]
            ];
}

- (MsgTypeInfo *)_populateMsgTypeInfoFromResultSet: (NSDictionary *)resultSet
{
    MsgTypeInfo *info = [[MsgTypeInfo alloc] init];         
    info.ID = [[resultSet safeObjectForKey:@"ID"] integerValue];
    info.typeID = [resultSet safeObjectForKey:@"typeId"];
    info.typeName = [resultSet safeObjectForKey: @"typeName"];
    info.ecId = [resultSet safeObjectForKey: @"ecId"];
    info.iconKey = [resultSet safeObjectForKey: @"iconKey"];
    info.version = [[resultSet safeObjectForKey:@"Version"] integerValue];
    info.orderMum = [[resultSet safeObjectForKey:@"OrderMum"] integerValue];
    info.isShielded = [[resultSet safeObjectForKey:@"isShielded"] boolValue];
    return [info autorelease];
}

-(NSArray *) getMsgTypeInfos
{
    
    NSMutableArray *msgTypeInfos = [NSMutableArray array];
    
    NSString *sql = @"Select ID, typeId,typeName,ecId,iconKey,Version, OrderMum, isShielded from MsgTypeInfo order by OrderMum desc, typeId desc";
    
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql];
    
    for (NSDictionary *rs in resultSet)
    {
        [msgTypeInfos addObject: [self _populateMsgTypeInfoFromResultSet:rs]];
    }
    
    
    NSString *GP_Show_NewMessage = NSLocalizedString(@"GP_Show_NewMessage", nil);
    
    if ([GP_Show_NewMessage isEqualToString:@"1"]) {
        MsgTypeInfo *info = [[MsgTypeInfo alloc] init];
        info.ID = -1;
        info.typeID = PERSONAL_MSGTYPE;
        info.typeName = @"个人信息";
        info.iconKey = PERSONAL_IMAGE;
        [msgTypeInfos addObject:info];
        [info release];
    }

    if (isPad) {
        MsgTypeInfo *info = [[MsgTypeInfo alloc] init];
        info.ID = -2;
        info.typeID = @"-1";
        info.typeName = @"全部信息";
        info.iconKey = nil;
        [msgTypeInfos addObject:info];
        [info release];
    }
    
    return msgTypeInfos;
    
}

- (MsgTypeInfo *)getMsgTypeInfoByTypeID:(NSString *)typeID
{    
    NSString *sql = @"Select ID, typeId,typeName,ecId,iconKey,Version, OrderMum, isShielded from MsgTypeInfo where typeId = ?";
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql, typeID];
    for (NSDictionary *rs in resultSet)
    {
        return [self _populateMsgTypeInfoFromResultSet:rs];
    }
    return nil;
}

-(BOOL)setMsgTypeInfo:(NSString *)typeID shielded:(BOOL)isShielded
{
    
    NSString *sql = @"Update MsgTypeInfo Set isShielded = ? where typeId = ? ";
    
    return [ZSTSqlManager executeUpdate:sql, [NSNumber numberWithBool:isShielded], typeID];
    
}

-(NSData *)getIcon:(NSString *)typeId
{
    NSString *sql = @"select data from fileinfo, MsgTypeInfo where fileinfo.key = MsgTypeInfo.iconKey and MsgTypeInfo.iconKey != '' and typeId = ?";
    return [ZSTSqlManager dataForQuery:sql, typeId];
}

@end
