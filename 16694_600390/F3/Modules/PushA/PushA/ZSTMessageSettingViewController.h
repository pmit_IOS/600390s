//
//  ZSTMessageSettingViewController.h
//  PushA
//
//  Created by xuhuijun on 13-2-1.
//
//

#import <UIKit/UIKit.h>


@interface ZSTPushASettingItem : NSObject {
}

+(ZSTPushASettingItem *)itemWithTitle:(NSString *)title type:(NSString *)type;

@property (nonatomic, copy) NSString *title;
@property (nonatomic, retain) NSString *type;
@end



@interface ZSTMessageSettingViewController : UIViewController <UITableViewDelegate,UITableViewDataSource>

{
    NSMutableArray *_tableViewDataArray;
    NSMutableArray *_settingItems;
    UITableView *_tableView;
}


@end
