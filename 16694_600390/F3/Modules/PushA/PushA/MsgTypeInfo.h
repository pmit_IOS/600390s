//
//  ECECCInfo.h
//  F3_UI
//
//  Created by  on 11-8-24.
//  Copyright 2011年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MsgTypeInfo : NSObject

@property(nonatomic, assign) NSInteger ID;
@property(nonatomic, copy) NSString *typeID;
@property(nonatomic, copy) NSString *typeName;
@property(nonatomic, copy) NSString *ecId;
@property(nonatomic, copy) NSString *iconKey;
@property(nonatomic, assign) BOOL isShielded;
@property(nonatomic, assign) NSInteger version;
@property(nonatomic, assign) NSInteger orderMum;

@end
 