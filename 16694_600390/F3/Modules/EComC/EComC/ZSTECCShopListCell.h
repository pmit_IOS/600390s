//
//  ZSTECCShopListCell.h
//  EComC
//
//  Created by anqiu on 15/8/27.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SDWebImageManager.h>
#import <SDWebImage/UIImageView+WebCache.h>

@protocol ZSTECCShopListCellDelegate <NSObject>

-(void)goLook:(NSString *)shopId shopAddress:(NSString *)shopAddress shopName:(NSString *)shopName;

@end

@interface ZSTECCShopListCell : UITableViewCell<SDWebImageManagerDelegate>

@property (strong,nonatomic) UIImageView *logo;
@property (strong,nonatomic) UIImageView *shopLogoIV;
@property (strong,nonatomic) UILabel *shopNameLB;
@property (strong,nonatomic) UILabel *startTitleLB;
@property (strong,nonatomic) UILabel *distanceLB;
@property (strong,nonatomic) UILabel *lineLB;
@property (strong,nonatomic) id<ZSTECCShopListCellDelegate> delegate;

- (void)setCellData:(NSDictionary *)dic;
@end
