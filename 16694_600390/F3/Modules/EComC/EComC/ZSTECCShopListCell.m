//
//  ZSTECCShopListCell.m
//  EComC
//
//  Created by anqiu on 15/8/27.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopListCell.h"
#import "NSString+HexColor.h"
#import "NSString+HexColor.h"


@implementation ZSTECCShopListCell{
   
    NSString *shopId;
    UILabel *jiluLab;
    UILabel *kmLab;
    UIImageView *locationIcon;
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.contentView.backgroundColor = RGBA(224, 224, 224, 0.5);
        self.contentView.frame = CGRectMake(10, 10, 300, 250);
        
        ZSTECCRoundRectView *bg = [[ZSTECCRoundRectView alloc]initWithFrame:CGRectMake(10, 10, 300, 240) radius:4.0 borderWidth:1.0f borderColor:[UIColor whiteColor]];
        bg.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:bg];
        
        //
        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 40)];
        [bg addSubview:view1];
        
        self.logo= [[UIImageView alloc] initWithFrame:CGRectMake(15, 12, 15, 15)];
        self.shopNameLB.backgroundColor = [UIColor redColor];
        self.logo.image = [UIImage imageNamed:@"model_ecomc_shopLogo.png"];
        [view1 addSubview:self.logo];
        
        self.shopNameLB = [[UILabel alloc] initWithFrame:CGRectMake(40, 5, 180, 30)];
        self.shopNameLB.font = [UIFont systemFontOfSize:14];
        self.shopNameLB.textColor = [@"333333" colorValue];
        [view1 addSubview:self.shopNameLB];
//        
//        jiluLab = [[UILabel alloc] initWithFrame:(CGRectMake(WIDTH-95, 5, 30, 30))];
//        jiluLab.text = @"距您";
//        jiluLab.textColor = [@"#666666" colorValue];
//        jiluLab.font = [UIFont systemFontOfSize:14];

        //[view1 addSubview:jiluLab];
        
        kmLab = [[UILabel alloc] initWithFrame:(CGRectMake(WIDTH-45, 5, 20, 30))];
        kmLab.text = @"km";
        kmLab.textColor = [@"#666666" colorValue];
        kmLab.font = [UIFont systemFontOfSize:14];
        [view1 addSubview:kmLab];
        
        self.distanceLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-100, 5, 55, 30)];
        self.distanceLB.font = [UIFont systemFontOfSize:12];
        self.distanceLB.textColor = [@"#fe9b27" colorValue];
        self.distanceLB.textAlignment = NSTextAlignmentRight;
        [view1 addSubview:self.distanceLB];
        
        //
        self.shopLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 35, 300, 160)];
        self.shopLogoIV.contentMode = UIViewContentModeScaleAspectFit;
        [bg addSubview:self.shopLogoIV];
        
        UIControl *picBtn = [[UIControl alloc] initWithFrame:CGRectMake(0, 30, 300, 160)];
        [picBtn addTarget:self action:@selector(goLook) forControlEvents:UIControlEventTouchUpInside];
        [bg addSubview:picBtn];

        
        //
        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, 190, 300, 60)];
        [bg addSubview:view2];
        
        locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 10, 15)];
        [locationIcon setImage:[UIImage imageNamed:@"model_ecomc_location_icon.png"]];
        locationIcon.contentMode = UIViewContentModeScaleAspectFit;
        [view2 addSubview:locationIcon];
        
        self.startTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(20+15, 0, WIDTH - 35 - 110, 35)];
        self.startTitleLB.textAlignment = NSTextAlignmentLeft;
        self.startTitleLB.textColor = [UIColor lightGrayColor];
        self.startTitleLB.lineBreakMode = NSLineBreakByWordWrapping;
        self.startTitleLB.numberOfLines = 2;
        self.startTitleLB.font = [UIFont systemFontOfSize:13.0f];
        [view2 addSubview:self.startTitleLB];
        
        UIButton *goLookBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH-95, 10, 70, 30)];
        [goLookBtn setTitle:@"去瞧瞧" forState:UIControlStateNormal];
        goLookBtn.titleLabel.font = [UIFont systemFontOfSize:14];
        [goLookBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        goLookBtn.backgroundColor = [@"#fe9b27" colorValue];
        goLookBtn.layer.cornerRadius = 5;
        [view2 addSubview:goLookBtn];
        
        UIControl * controlBtn = [[UIControl alloc]initWithFrame:CGRectMake(WIDTH-110, 0, 90, 60)];
        [controlBtn addTarget:self action:@selector(goLook) forControlEvents:UIControlEventTouchUpInside];
        controlBtn.backgroundColor = [UIColor clearColor];
        controlBtn.tag = 2;
        [view2 addSubview:controlBtn];
       
    }
    return self;
}


- (void)setCellData:(NSDictionary *)dic
{
    [self.shopLogoIV setImageWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"shopLogo"]]];
    //[[SDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"shopLogo"]] delegate:self];
    
    self.shopNameLB.text = [dic safeObjectForKey:@"shopName"];
    
    shopId = [dic safeObjectForKey:@"id"];
    
    NSString *addressString = [dic safeObjectForKey:@"shopAddress"];
    CGSize addressSize = [addressString sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WIDTH - 35 - 110, MAXFLOAT)];
   
    
    if (addressSize.height < 32 || [addressString length] < 13) {
        self.startTitleLB.frame = CGRectMake(20 + 15, 15, WIDTH - 35 - 110, 15);
         locationIcon.frame = CGRectMake(15, 15, 10, 15);
    }else{
        self.startTitleLB.frame = CGRectMake(20 + 15, 5, WIDTH - 35 - 110, addressSize.height);
        locationIcon.frame = CGRectMake(15, 8, 10, 15);
    }
    
    if ([addressString length] < 14) {
        self.startTitleLB.frame = CGRectMake(20 + 15, 15, WIDTH - 35 - 110, 15);
        locationIcon.frame = CGRectMake(15, 15, 10, 15);
    }
    

    self.startTitleLB.text = addressString;
    
    NSString *distanceString = [NSString stringWithFormat:@"%.1lf",[[dic safeObjectForKey:@"distance"] doubleValue]];
    self.distanceLB.text = distanceString;

    if ([distanceString integerValue] > 999) {
        self.distanceLB.text = @">1000";
    }else if([distanceString integerValue]<0){
        self.distanceLB.hidden = YES;
        kmLab.hidden=YES;
    }
    
    
    //self.distanceLB.text = @"大于1000";
    
//    if (distanceString.length == 4) {
//        self.distanceLB.frame = CGRectMake(WIDTH-75, 5, 30, 30);
//        jiluLab.frame = CGRectMake(WIDTH-100, 5, 30, 30);
//    }else if (distanceString.length >4 && distanceString.length <7) {
//        self.distanceLB.frame = CGRectMake(WIDTH-90, 5, 45, 30);
//        jiluLab.frame = CGRectMake(WIDTH-115, 5, 30, 30);
//    }else if(distanceString.length >=7){
//        self.distanceLB.hidden = YES;
//        jiluLab.hidden = YES;
//        kmLab.hidden=YES;
//    }else{
//        self.distanceLB.frame = CGRectMake(WIDTH-70, 5, 25, 30);
//        jiluLab.frame = CGRectMake(WIDTH-95, 5, 30, 30);
//    }

}

//- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image
//{
//    CGFloat borderWH = image.size.width > image.size.height ? image.size.height : image.size.width;
//    
//    UIImage *newsImage = [self imageCompressForSize:image targetSize:CGSizeMake(borderWH, borderWH)];
//    self.shopLogoIV.image = newsImage;
//}
//
//- (UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size{
//    UIImage *newImage = nil;
//    CGSize imageSize = sourceImage.size;
//    CGFloat width = imageSize.width;
//    CGFloat height = imageSize.height;
//    CGFloat targetWidth = size.width;
//    CGFloat targetHeight = size.height;
//    CGFloat scaleFactor = 0.0;
//    CGFloat scaledWidth = targetWidth;
//    CGFloat scaledHeight = targetHeight;
//    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
//    if(CGSizeEqualToSize(imageSize, size) == NO){
//        CGFloat widthFactor = targetWidth / width;
//        CGFloat heightFactor = targetHeight / height;
//        if(widthFactor > heightFactor){
//            scaleFactor = widthFactor;
//        }
//        else{
//            scaleFactor = heightFactor;
//        }
//        scaledWidth = width * scaleFactor;
//        scaledHeight = height * scaleFactor;
//        if(widthFactor > heightFactor){
//            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
//        }else if(widthFactor < heightFactor){
//            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
//        }
//    }
//    
//    UIGraphicsBeginImageContext(size);
//    
//    CGRect thumbnailRect = CGRectZero;
//    thumbnailRect.origin = thumbnailPoint;
//    thumbnailRect.size.width = scaledWidth;
//    thumbnailRect.size.height = scaledHeight;
//    [sourceImage drawInRect:thumbnailRect];
//    newImage = UIGraphicsGetImageFromCurrentImageContext();
//    
//    if(newImage == nil){
//        NSLog(@"scale image fail");
//    }
//    
//    UIGraphicsEndImageContext();
//    
//    return newImage;
//    
//}


-(void)goLook{

    [_delegate goLook:shopId shopAddress:self.startTitleLB.text shopName:self.shopNameLB.text];
}

@end
