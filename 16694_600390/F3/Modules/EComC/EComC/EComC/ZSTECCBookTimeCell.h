//
//  ZSTECCBookTimeCell.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCBookTimeCell : UITableViewCell

@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UILabel *bookTimeLB;

- (void)createUI;
- (void)setCellDataWithTime:(NSString *)bookTimeString;

@end
