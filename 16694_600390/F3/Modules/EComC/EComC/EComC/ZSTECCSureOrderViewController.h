//
//  ZSTECCSureOrderViewController.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCSureOrderViewController : UIViewController 

@property (copy,nonatomic) NSString *selectedSeatTypeString;
@property (copy,nonatomic) NSString *selectedSexTypeString;
@property (copy,nonatomic) NSString *shopId;
@property (copy,nonatomic) NSString *shopAddress;
@property (copy,nonatomic) NSString *shopName;
@property (copy,nonatomic) NSString *bookTimeString;
@property (assign,nonatomic) NSInteger bookCount;
@property (copy,nonatomic) NSString *bookManNameString;
@property (copy,nonatomic) NSString *bookPhoneString;
@property (strong,nonatomic) NSArray *bookFoodArr;
@property (copy,nonatomic) NSString *backUpString;

@property (strong,nonatomic) ZSTF3Engine *engine;

@end
