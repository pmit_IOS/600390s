//
//  ZSTECCAddressCell.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCAddressCell.h"

@implementation ZSTECCAddressCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.shopNameLB)
    {
        self.shopNameLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - 30, 30)];
        self.shopNameLB.textColor = RGBA(85, 85, 85, 1);
        self.shopNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.shopNameLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.shopNameLB];
        
        self.shopAddressLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 40, WIDTH - 30, 30)];
        self.shopAddressLB.textColor = RGBA(153, 153, 153, 1);
        self.shopAddressLB.textAlignment = NSTextAlignmentLeft;
        self.shopAddressLB.font = [UIFont systemFontOfSize:11.0f];
        self.shopAddressLB.numberOfLines = 0;
        [self.contentView addSubview:self.shopAddressLB];
    }
}

- (void)setCellDataWithShopName:(NSString *)shopName ShopAddress:(NSString *)shopAddress
{
    self.shopNameLB.text = shopName;
    CGSize addressSize = [shopAddress sizeWithFont:[UIFont systemFontOfSize:11.0f] constrainedToSize:CGSizeMake(WIDTH - 30, MAXFLOAT)];
    self.shopAddressLB.frame = (CGRect){{15,40},addressSize};
    self.shopAddressLB.text = shopAddress;
}

@end
