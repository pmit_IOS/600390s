//
//  ZSTECCMessageCell.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCMessageCell.h"
#import "ZSTECCBookSeatViewController.h"
#import "NSString+HexColor.h"

@implementation ZSTECCMessageCell 

- (void)createUI
{
    if (!self.iconIV)
    {
        self.iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 15, 20, 20)];
        self.iconIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.iconIV];
        
        self.messageTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, WIDTH * 0.4, 30)];
        self.messageTitleLB.textAlignment = NSTextAlignmentLeft;
        self.messageTitleLB.font = [UIFont systemFontOfSize:14.0f];
        self.messageTitleLB.textColor = RGBA(85, 85, 85, 1);
        [self.contentView addSubview:self.messageTitleLB];
        
        self.nameTF = [[UITextField alloc] initWithFrame:CGRectMake(50, 10, WIDTH * 0.4, 30)];
        self.nameTF.placeholder = @"请输入您的姓氏";
        self.nameTF.font = [UIFont systemFontOfSize:14.0f];
        self.nameTF.hidden = YES;
        self.nameTF.delegate = self;
        self.nameTF.returnKeyType = UIReturnKeyDone;
        [self.contentView addSubview:self.nameTF];
        
        self.backUpTV = [[UITextView alloc] initWithFrame:CGRectMake(45, 5, WIDTH-50, 75)];
        self.backUpTV.delegate = self;
        self.backUpTV.font = [UIFont systemFontOfSize:14.0f];
        self.backUpTV.textColor = [UIColor blackColor];
        self.backUpTV.returnKeyType = UIReturnKeyDone;
        [self.contentView addSubview:self.backUpTV];
        
        self.placeholderLB = [[UILabel alloc] initWithFrame:CGRectMake(5, 10, WIDTH - 30, 20)];
        self.placeholderLB.text = @"备注:";
        self.placeholderLB.textColor = RGBA(205, 205, 205, 1);
        self.placeholderLB.textAlignment = NSTextAlignmentLeft;
        [self.backUpTV addSubview:self.placeholderLB];
        
        self.backUpTV.hidden = YES;
        
        self.rightLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH * 0.4 + 50, 10, WIDTH * 0.6 - 60, 30)];
        self.rightLB.textColor = RGBA(206, 206, 206, 1);
        self.rightLB.textAlignment = NSTextAlignmentRight;
        self.rightLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.rightLB];
        self.rightLB.hidden = YES;
        
        self.phoneTF = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH * 0.4 + 50, 10, WIDTH * 0.6 - 60, 30)];
        self.phoneTF.font = [UIFont systemFontOfSize:14.0f];
        self.phoneTF.textColor = RGBA(206, 206, 206, 1);
        self.phoneTF.placeholder = @"请输入手机号码";
        self.phoneTF.textAlignment = NSTextAlignmentRight;
        self.phoneTF.keyboardType = UIKeyboardTypeNumberPad;
        [self.contentView addSubview:self.phoneTF];
        self.phoneTF.delegate = self;
        self.phoneTF.hidden = YES;
        self.phoneTF.returnKeyType = UIReturnKeyDone;
        
        self.selectedSeatTag = -1;
        self.selectedSexTag = -1;
    }
}

- (void)setCellDataWithIconIV:(NSString *)iconName messageTitle:(NSString *)titleString isSeat:(BOOL)isSeat isCanChoose:(BOOL)isCanChoose isBackUp:(BOOL)isBackUp SeatTypeArr:(NSArray *)seatTypeArr SexTypeArr:(NSArray *)sexTypeArr
{
    
    if (isBackUp)
    {
        self.backUpTV.hidden = NO;
        self.iconIV.image = [UIImage imageNamed:iconName];
    }
    else
    {
        self.iconIV.image = [UIImage imageNamed:iconName];
        self.messageTitleLB.text = titleString;
        if (isCanChoose)
        {
            if (!isSeat)
            {
                self.messageTitleLB.textColor = RGBA(153, 153, 153, 1);
                self.nameTF.hidden = NO;
                self.messageTitleLB.hidden = YES;
                if (![self.contentView viewWithTag:100])
                {
                    NSDictionary *sexDic = sexTypeArr[0];
                    for (NSInteger i = 0;i < [[sexDic allKeys] count];i++)
                    {
                        NSString *sexString = [[sexDic allValues] objectAtIndex:i];
                        NSString *sexKey = [[sexDic allKeys] objectAtIndex:i];
                        UIButton *sexBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                        [sexBtn setTitle:sexString forState:UIControlStateNormal];
                        [sexBtn setTitleColor:RGBA(243, 149, 1, 1) forState:UIControlStateSelected];
                        sexBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                        sexBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
                        sexBtn.layer.borderWidth = 0.8;
                        sexBtn.layer.cornerRadius = 4.0f;
                        [sexBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
                        sexBtn.frame = CGRectMake(self.messageTitleLB.frame.origin.x + self.messageTitleLB.frame.size.width + 10 + (WidthRate(140) + 5) * i, 10, WidthRate(140) , 30);
                        sexBtn.tag = 100 + [sexKey integerValue];
                        [sexBtn addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
                        [self.contentView addSubview:sexBtn];
                    }
                }
            }
            else
            {
                self.messageTitleLB.textColor = RGBA(85, 85, 85, 1);
                if (![self.contentView viewWithTag:10000])
                {
                    NSDictionary *seatDic = seatTypeArr[0];
                    for (NSInteger i = 0;i < [[seatDic allKeys] count];i++)
                    {
                        NSString *seatString = [seatDic allValues][i];
                        NSString *seatKey = [seatDic allKeys][i];
                        UIButton *seatBtn = [UIButton buttonWithType:UIButtonTypeCustom];
                        [seatBtn setTitle:seatString forState:UIControlStateNormal];
                        seatBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
                        seatBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
                        seatBtn.layer.borderWidth = 0.8;
                        seatBtn.layer.cornerRadius = 4.0f;
                        seatBtn.tag = 10000 + [seatKey integerValue];
                        [seatBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
                        [seatBtn setTitleColor:RGBA(243, 149, 1, 1) forState:UIControlStateSelected];
                        seatBtn.frame = CGRectMake(self.messageTitleLB.frame.origin.x + self.messageTitleLB.frame.size.width + 10 + (WidthRate(140) + 5) * i, 10, WidthRate(140) , 30);
                        [seatBtn addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
                        [self.contentView addSubview:seatBtn];
                    }
                }
                
            }
        }
        else
        {
            self.messageTitleLB.textColor = RGBA(85, 85, 85, 1);
        }
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    if ([textView.text length] == 0)
    {
        self.placeholderLB.hidden = NO;
    }
    else
    {
        self.placeholderLB.hidden = YES;
    }
}

- (void)changeValue:(UIButton *)sender
{
    
    [self.nameTF resignFirstResponder];
    [self.phoneTF resignFirstResponder];
    
    UIButton *selectedSexBtn = (UIButton *)[self.contentView viewWithTag:self.selectedSexTag];
    UIButton *selectedSeatBtn = (UIButton *)[self.contentView viewWithTag:self.selectedSeatTag];
    
    if (sender.tag >= 10000)
    {
        selectedSeatBtn.selected = NO;
        selectedSeatBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        sender.selected = YES;
        self.selectedSeatTag = sender.tag;
        sender.layer.borderColor = RGBA(243, 149, 1, 1).CGColor;
        self.bookSeatVC.selectedSeatTypeString = [NSString stringWithFormat:@"%@",@(sender.tag - 10000)];
    }
    else
    {
        selectedSexBtn.selected = NO;
        selectedSexBtn.layer.borderColor = RGBA(153, 153, 153, 1).CGColor;
        sender.selected = YES;
        self.selectedSexTag = sender.tag;
        sender.layer.borderColor = RGBA(243, 149, 1, 1).CGColor;
        self.bookSeatVC.selectedSexTypeString = [NSString stringWithFormat:@"%@",@(sender.tag - 100)];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    
    if (textField == self.phoneTF)
    {
        NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        
        if (toBeString.length > 11 && range.length!=1){
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
    }
    else if (textField == self.nameTF)
    {
        NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string];
        if (toBeString.length > 5)
        {
            textField.text = [toBeString substringToIndex:5];
            return NO;
        }
    }
    
    
    
    return YES;
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == self.nameTF)
    {
        if (![self.nameTF.text isEqualToString:@""])
        {
            self.bookSeatVC.bookManNameString = self.nameTF.text;
        }
    }
    else if (textField == self.phoneTF)
    {
        if (![self.phoneTF.text isEqualToString:@""])
        {
            self.bookSeatVC.bookPhoneString = self.phoneTF.text;
        }
    }
    
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    if (textView == self.backUpTV)
    {
        self.bookSeatVC.backUpString = self.backUpTV.text;
    }
    
    
    
    [textView resignFirstResponder];
}

-(BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString*)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    NSString *str = [NSString stringWithFormat:@"%@%@", textView.text, text];
    if (str.length > 40)
    {
        textView.text = [textView.text substringToIndex:40];
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([self.messageTitleLB.text isEqualToString:@"联系方式"])
    {
        self.bookSeatVC.isNumKey = YES;
    }
    else
    {
        self.bookSeatVC.isNumKey = NO;
//        if (self.bookSeatVC.doneBtn)
//        {
            [self.bookSeatVC.doneBtn removeFromSuperview];
            self.bookSeatVC.doneBtn = nil;
//        }
    }
    
    return YES;
}

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.bookSeatVC.isNumKey = NO;
    if (self.bookSeatVC.doneBtn)
    {
        [self.bookSeatVC.doneBtn removeFromSuperview];
        self.bookSeatVC.doneBtn = nil;
    }
    
    return YES;
}

@end
