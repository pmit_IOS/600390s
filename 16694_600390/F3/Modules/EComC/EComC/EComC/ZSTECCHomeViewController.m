//
//  ZSTECCHomeViewController.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCHomeViewController.h"
#import "ZSTECCShopCell.h"
#import "ZSTECCShop.h"
#import "ZSTECCReservationViewComtroller.h"
#import "CCLocationManager.h"
#import "ZSTECCOrderListViewController.h"
#import "ZSTECCSearchViewController.h"
#import <ZSTLoginController.h>
#import <BaseNavgationController.h>
#import "MoreButton.h"
#import "ZSTWebViewController.h"
#import "ZSTECCShopListCell.h"
#import "GetMyLocation.h"
#import "ZSTECCMapViewController.h"



#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)

@interface ZSTECCHomeViewController () <UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate,ZSTLoginControllerDelegate>{

    CLLocationManager *locationmanager;
    
    int _currentPage;
    BOOL _hasMore;
    MoreButton *_moreButton;
    BOOL  isRefresh;
    BOOL _loading;  //是否正在加载中 待消费

}

@property (strong,nonatomic) NSMutableArray *shopNameArr;
@property (strong,nonatomic) UITableView *shopTableView;
@property (assign,nonatomic) NSInteger totalPage;
@property (strong,nonatomic) NSMutableArray *hasRefreshArr;
@property (assign,nonatomic) BOOL isLoadMore;

@end


@implementation ZSTECCHomeViewController{
    UIButton *leftBtn;
    UIButton *rightBtn;
    
    NSArray *bannerArray;
    
    float latitude;//纬度
    float longitude;//经度
    
    UILabel *locationLable;
    
    NSInteger firstTime;
    
    BOOL thread;
    
    UIView *seachBgView;
    
    NSString *bannerSetTime;
    NSString *shopListSetTime;
}

static NSString *const shopCell = @"shopCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTitleView];
    
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate  = self;
    self.engine = engine;
    
    self.view.backgroundColor = RGBA(224, 224, 224, 1);
    //[self.navigationController setNavigationBarHidden:YES];
    
    
    
//    self.navigationItem.rightBarButtonItem = [self aInitLeftBtn];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[self buildRightCustomView]];
    //self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"东圃一路66号   >", nil)];
    self.hasRefreshArr = [NSMutableArray array];
    self.shopNameArr = [NSMutableArray array];
    _currentPage = 1;
    self.isLoadMore = NO;
    firstTime =  1;
    thread = NO;
    
    [self initData];
    [self buildTableView];
    [self buildTableViewHeader];
    
    
    
    
    if ([self.navigationController.childViewControllers count] == 1)
    {
        
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    }
    

    NSUserDefaults *shopList = [NSUserDefaults standardUserDefaults];
    NSArray *shopArray = [[NSArray alloc] init];
    shopArray = [shopList arrayForKey:@"shopList"];
    shopListSetTime = [shopList stringForKey:@"shopListSetTime"];
    
    if (shopArray.count == 0 || [shopArray isKindOfClass:[NSNull class]]) {
          thread = NO;
         [self getMyLocation];;// 定位
    }else{
         [self.shopNameArr removeAllObjects];
         [self.shopNameArr addObjectsFromArray:shopArray];
        
         thread = YES;
         [self getMyLocation];
    }
    
    //检测更新
    if (shopListSetTime && bannerSetTime && ![shopListSetTime isEqualToString:@""] && ![bannerSetTime isEqualToString:@""]) {
        
        [self.engine checkDataUpdate:bannerSetTime andShopTime:shopListSetTime];
    }

    
//    NSUserDefaults *shopAddress = [NSUserDefaults standardUserDefaults];
//    locationLable.text =[shopAddress stringForKey:@"shopAddress"];
   
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
    
}

#pragma mark - 创建标题视图
-(void)createTitleView{
  
    self.titleView = [[UIView alloc] initWithFrame:CGRectMake(20, 0, 280, 44)];
    //self.titleView.backgroundColor = [UIColor grayColor];
    
    UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(220, 10, 15,20)];
    [locationIcon setImage:[UIImage imageNamed:@"model_ecomc_location_icon_big.png"]];
    //[self.titleView addSubview:locationIcon];
    
    
    NSDictionary *infoDictionary = [[NSBundle mainBundle] infoDictionary];
    CFShow((__bridge CFTypeRef)(infoDictionary));
    // app名称
    NSString *app_Name = [infoDictionary safeObjectForKey:@"CFBundleDisplayName"];
    
//    locationLable = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, 280, 20)];
//    locationLable.text = app_Name;
//    locationLable.textAlignment = NSTextAlignmentCenter;
//    locationLable.textColor = [UIColor whiteColor];
//    locationLable.font = [UIFont systemFontOfSize:17];
//    [self.titleView addSubview:locationLable];
    
    //self.navigationItem.titleView = self.titleView;
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:app_Name];

}


-(void)initData
{
    
    _hasMore = NO;
    
    _carouselView = [[ZSTECCCarouselView alloc]initWithFrame:CGRectMake(0, 0,WIDTH, WIDTH / 3)];
    _carouselView.carouselDataSource = self;
    _carouselView.carouselDelegate = self;
    _carouselView.haveTitle = NO;
    [_carouselView hideStateBar];
    [_carouselView pageControlState:pageControlStateRIGHT];
    [_carouselView startAnimation];
    _carouseData = nil;
    //轮播图获取
    
    NSUserDefaults *banner = [NSUserDefaults standardUserDefaults];
    NSArray *udBannerArray = [[NSArray alloc] init];
    udBannerArray = [banner arrayForKey:@"foodBannerArray"];
    bannerSetTime = [banner stringForKey:@"bannerSetTime"];
    
    
    if (udBannerArray.count == 0 || [udBannerArray isKindOfClass:[NSNull class]]) {
        [self.engine getEcomCCarousel];
    }else{
        _carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:udBannerArray];
        [_carouselView reloadData];
    }

}

- (UIView *)buildRightCustomView
{
    UIView *rightCustomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 50, 44)];
    UIImageView *searchIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 12, 20, 20)];
    searchIV.image = [UIImage imageNamed:@"ecomCSearch.png"];
    searchIV.contentMode = UIViewContentModeScaleAspectFit;
    [rightCustomView addSubview:searchIV];
    
    UIButton *searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    searchBtn.frame = CGRectMake(0, 12, 20, 20);
    [searchBtn addTarget:self action:@selector(goToSearch:) forControlEvents:UIControlEventTouchUpInside];
    [rightCustomView addSubview:searchBtn];
    
    UIImageView *orderIV = [[UIImageView alloc] initWithFrame:CGRectMake(30, 12, 20, 20)];
    orderIV.image = [UIImage imageNamed:@"model_ecomc_orderHead.png"];
    orderIV.contentMode = UIViewContentModeScaleAspectFit;
    [rightCustomView addSubview:orderIV];
    
    UIButton *orderBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    orderBtn.frame = CGRectMake(30, 12, 20, 20);
    [orderBtn addTarget:self action:@selector(orderBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [rightCustomView addSubview:orderBtn];
    
    return rightCustomView;
}


- (UIBarButtonItem *) aInitLeftBtn
{
    leftBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    leftBtn.frame = CGRectMake(10, 25, 20, 20);
    [leftBtn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(orderBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    leftBtn.layer.cornerRadius = leftBtn.frame.size.width / 2.0;
    leftBtn.layer.borderColor = [UIColor clearColor].CGColor;
    leftBtn.layer.masksToBounds = YES;
    
    NSString *path = nil;
    UIImage *image = nil;
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        image = [UIImage imageNamed:@"model_ecomc_orderHead.png"];
    } else {
        
        if (dic || [dic isKindOfClass:[NSDictionary class]]) {
            path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
        }
        
//        if (path && path.length > 0) {
//            
//            NSString *homePath = NSHomeDirectory();
//            NSString *imgTempPath = [path substringFromIndex:[path rangeOfString:@"tmp"].location];
//            NSString *finalPath = [NSString stringWithFormat:@"%@/%@",homePath,imgTempPath];
//            NSData *reader = [NSData dataWithContentsOfFile:finalPath];
//            image = [UIImage imageWithData:reader];
//            
//            if (!image) {
//                
//                image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
//            }
//            
//        } else {
        
            image = [UIImage imageNamed:@"model_ecomc_orderHead.png"];
        //}
    }
    
    [leftBtn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:leftBtn.imageView.frame];
    iv.image = image;
    iv.contentMode = UIViewContentModeScaleAspectFit;
    [leftBtn insertSubview:iv aboveSubview:leftBtn.imageView];
    
    
    return [[UIBarButtonItem alloc] initWithCustomView:leftBtn];
}


- (UIBarButtonItem *) aInitRightBtn
{
    rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(10, 25, 20, 20);
    [rightBtn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(goToSearch:) forControlEvents:UIControlEventTouchUpInside];
//    UIImage *image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    UIImage *image = [UIImage imageNamed:@"ecomCSearch.png"];
    
    [rightBtn setImage:image forState:UIControlStateNormal];
    
    //暂时解决btn图片不能显示的问题
    UIImageView *iv = [[UIImageView alloc] initWithFrame:rightBtn.imageView.frame];
    iv.image = image;
    [rightBtn insertSubview:iv aboveSubview:rightBtn.imageView];
    
    
    return [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
}


-(void)orderBtnAction{
    
    if (![ZSTF3Preferences shared].UserId || [[ZSTF3Preferences shared].UserId isEqualToString:@""])
    {
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = NO;
        controller.delegate = self;
        BaseNavgationController *navController = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navController animated:YES completion:^{
            
        }];
    }
    else
    {
        ZSTECCOrderListViewController * orderListView = [[ZSTECCOrderListViewController alloc] init];
        orderListView.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:orderListView animated:YES];
    }
    
   
    
}

- (void)getEcomCCarouselDidSucceed:(NSDictionary *)carouseles
{
    
    NSArray *arrays = [carouseles safeObjectForKey:@"dataList"];
    
    _carouseData = nil;
    if (arrays.count>0) {
        
        NSDateFormatter *format=[[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *date = [NSDate date];
        NSString *bannerSetTimeStr =  [format stringFromDate:date];
        
        NSUserDefaults *banner = [NSUserDefaults standardUserDefaults];
        [banner setObject:arrays forKey:@"foodBannerArray"];
        [banner setObject:bannerSetTimeStr forKey:@"bannerSetTime"];
        [banner synchronize];
        
         _carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:arrays];
        
        [_carouselView reloadData];
    }
}

- (void)getEcomCCarouselDidFailed:(int)resultCode
{
    NSLog(@"广告暂无数据！广告暂无数据！广告暂无数据！广告暂无数据！广告暂无数据！");
    
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
    
    
}

#pragma mark - 检查更新结果
-(void)checkUpdateDidSucceed:(NSDictionary *)responseInfo{
  
    BOOL hasBannerUpdate  = [[responseInfo safeObjectForKey:@"carousel"] boolValue];
    if (hasBannerUpdate) {
        [self.engine getEcomCCarousel];
    }

}

-(void)checkUpdateDidFailed:(NSDictionary *)failedInfo{


}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)buildTableView
{
    if ([[self.navigationController.childViewControllers firstObject] isKindOfClass:[ZSTECCHomeViewController class]])
    {
        self.shopTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 50) style:UITableViewStylePlain];
    }
    else
    {
        self.shopTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    }
    
    self.shopTableView.delegate = self;
    self.shopTableView.dataSource = self;
    self.shopTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.shopTableView registerClass:[ZSTECCShopCell class] forCellReuseIdentifier:shopCell];
    [self.view addSubview:self.shopTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    self.shopTableView.tableFooterView = footerView;
    
    if (_refreshHeaderView == nil) {
        
       	EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.shopTableView.bounds.size.height, self.view.frame.size.width, self.shopTableView.bounds.size.height)];
        view.delegate = self;
        [self.shopTableView addSubview:view];
        _refreshHeaderView = view;
    }
    
    [_refreshHeaderView refreshLastUpdatedDate];
}

- (void)buildTableViewHeader
{
    seachBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 50)];
    seachBgView.backgroundColor = RGBA(0, 0, 0, 0.5);
    seachBgView.hidden = YES;
    [_carouselView addSubview:seachBgView];
    UILabel *tipLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 250, 30)];
    tipLab.text = @"Sorry,您还没打开定位系统";
    tipLab.font = [UIFont systemFontOfSize:15];
    tipLab.textColor = [UIColor whiteColor];
    [seachBgView addSubview:tipLab];
    
    UIButton *goSeachBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH-90, 10, 80, 30)];
    [goSeachBtn  setTitle:@"去设置 》" forState:UIControlStateNormal];
    goSeachBtn.titleLabel.font = [UIFont systemFontOfSize:15];
    [goSeachBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [goSeachBtn addTarget:self action:@selector(toLocationSetting) forControlEvents:UIControlEventTouchUpInside];
    [seachBgView addSubview:goSeachBtn];
    
    CLAuthorizationStatus status = [CLLocationManager authorizationStatus];
    if (kCLAuthorizationStatusDenied == status || kCLAuthorizationStatusRestricted == status) {
        seachBgView.hidden = NO;
    }else{
        seachBgView.hidden = YES;
    }
    
    [self.shopTableView setTableHeaderView:_carouselView];
    self.shopTableView.tableHeaderView.backgroundColor = RGBACOLOR(224, 224, 224, 1);
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_hasMore) {
        return self.shopNameArr.count + 1;
    }
    
    return self.shopNameArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    //是否还有数据
    static NSString *moreCellIdentifier = @"MoreCell";
    if (indexPath.row == self.shopNameArr.count && _hasMore)
    {
        UITableViewCell *moreCell = [tableView dequeueReusableCellWithIdentifier:moreCellIdentifier];
        if (moreCell == nil)
        {
            moreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreCellIdentifier];
            _moreButton = [MoreButton button];
            [_moreButton setTitle:@"点击加载更多" forState:UIControlStateNormal];
            [_moreButton addTarget:self action:@selector(loadMoreData) forControlEvents:UIControlEventTouchUpInside];
            [moreCell.contentView addSubview:_moreButton];
        }
        
        return moreCell;
    }
    
//    ZSTECCShopCell *cell = [tableView dequeueReusableCellWithIdentifier:shopCell];
//    [cell createUI];
    
    static NSString *shopListCell= @"ZSTECCShopListCell";
    ZSTECCShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:shopListCell];
    if (!cell) {
        cell = [[ZSTECCShopListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shopListCell];
    }
    cell.delegate = self;
    NSLog(@"=====self.shopNameArr===>%@",self.shopNameArr);
    [cell setCellData:self.shopNameArr[indexPath.row]];//--
    cell.startTitleLB.tag = indexPath.row;
    cell.startTitleLB.userInteractionEnabled = YES;
    UITapGestureRecognizer *mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToMap:)];
    [cell.startTitleLB addGestureRecognizer:mapTap];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == self.shopNameArr.count) {
        return 44.0f;
    }
    
    return 255.0f;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

//    NSString *shopId = [self.shopNameArr[indexPath.row] safeObjectForKey:@"id"];
//    ZSTECCReservationViewComtroller *reservationView = [[ZSTECCReservationViewComtroller alloc] init];
//    reservationView.shopId = shopId;
//    reservationView.shopAddress = [self.shopNameArr[indexPath.row] safeObjectForKey:@"shopAddress"];
//    reservationView.shopName = [self.shopNameArr[indexPath.row] safeObjectForKey:@"shopName"];
//    reservationView.hidesBottomBarWhenPushed = YES;
//    reservationView.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString([self.shopNameArr[indexPath.row] objectForKey:@"shopName"], nil)];
//    [self.navigationController pushViewController:reservationView animated:YES];

}


-(void)goLook:(NSString *)shopId shopAddress:(NSString *)shopAddress shopName:(NSString *)shopName{

    ZSTECCReservationViewComtroller *reservationView = [[ZSTECCReservationViewComtroller alloc] init];
    reservationView.shopId = shopId;
    reservationView.shopAddress = shopAddress;
    reservationView.shopName = shopName;
    reservationView.hidesBottomBarWhenPushed = YES;
    reservationView.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(shopName, nil)];
    [self.navigationController pushViewController:reservationView animated:YES];
}

- (void)buildTableHeaderView
{
    
}

#pragma mark - 获取纬度与经度
-(void)getLat
{
    
    if (IS_IOS8) {
        
        [[CCLocationManager shareLocation] getLocationCoordinate:^(CLLocationCoordinate2D locationCorrrdinate) {
            
            
            NSLog(@"%f %f",locationCorrrdinate.latitude,locationCorrrdinate.longitude);
        }];
    }
    
}

#pragma mark - 获取当地城市
-(void)getCity
{
    
    if (IS_IOS8) {
        
        [[CCLocationManager shareLocation]getCity:^(NSString *cityString) {
            NSLog(@"%@",cityString);
            NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
            //locationLable.text = [NSString stringWithFormat:@"%@  >",cityString];
            
            
//            [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLatitude] doubleValue]] andLng:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLongitude] doubleValue]] andCurrentPage:self.currentPage PageSize:10];

        }];
        
    }
    
}

#pragma mark - 获取所有当地信息
-(void)getAllInfo
{
    __block NSString *string;
    
    if (IS_IOS8) {
        
        [[CCLocationManager shareLocation] getLocationCoordinate:^(CLLocationCoordinate2D locationCorrrdinate) {
            string = [NSString stringWithFormat:@"%f %f",locationCorrrdinate.latitude,locationCorrrdinate.longitude];
            
            NSString *lat = [NSString stringWithFormat:@"%f",locationCorrrdinate.latitude];
            NSString *lng = [NSString stringWithFormat:@"%f",locationCorrrdinate.longitude];
            
            dispatch_queue_t queue = dispatch_queue_create("", DISPATCH_QUEUE_CONCURRENT);
            dispatch_async(queue, ^{
                [self.engine getShopAddressInfoByUserId:[ZSTF3Preferences shared].UserId andLat:lat andLng:lng];
            });
            
            if (!thread) {
                [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在为您加载列表，请稍后", nil) withImage:nil];
            }
            
            [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",locationCorrrdinate.latitude] andLng:[NSString stringWithFormat:@"%f",locationCorrrdinate.longitude] andCurrentPage:1 PageSize:10];

            latitude = locationCorrrdinate.latitude;
            longitude =locationCorrrdinate.longitude;
            
        } withAddress:^(NSString *addressString) {
//            locationLable.text = [NSString stringWithFormat:@"%@  >",addressString];
//            string = [NSString stringWithFormat:@"%@\n%@",string,addressString];
        }];
        
        
       
    }
    
    
    
}

#pragma mark - 获取地址成功
-(void)getShopAddressInfoDidSucceed:(NSDictionary *)addressInfo{

    NSDictionary *addressComponent = [[NSDictionary alloc] init];
    addressComponent = [[addressInfo safeObjectForKey:@"result"] safeObjectForKey:@"addressComponent"];
    
    NSString *district =[addressComponent safeObjectForKey:@"district"];
    NSString *street = [addressComponent safeObjectForKey:@"street"];
    NSString *street_number = [addressComponent safeObjectForKey:@"street_number"];
    
    //locationLable.text = [NSString stringWithFormat:@"%@%@  >",street,street_number];
    
    NSUserDefaults *shopAddress = [NSUserDefaults standardUserDefaults];
    [shopAddress setObject:[addressInfo objectForKey:@"dataList"] forKey:@"shopAddress"];
    [shopAddress synchronize];

}

-(void)getShopAddressInfoDidFailed:(int)resultCode{


}


#pragma mark - ZSTEComBCarouselViewDataSource
- (NSInteger)numberOfViewsInCarouselView:(ZSTECCCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomCCarouseInfo *)carouselView:(ZSTECCCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    
    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate
- (void)carouselView:(ZSTECCCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    NSString *urlPath = [[_carouseData objectAtIndex:index] ecomCCarouseInfoLinkurl];
        if (urlPath == nil || ![urlPath isKindOfClass:[NSString class]] ||[urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
            return;
        }
//    EComCHomePageInfo * pageInfo = [[EComCHomePageInfo alloc]init];
//    pageInfo.ecomCHomeProductid = [NSString stringWithFormat:@"%d",[[_carouseData objectAtIndex:index] ecomBCarouseInfoProductid]];

        ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
        [webViewController setURL:urlPath];
        webViewController.hidesBottomBarWhenPushed = YES;
        webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
        [self.navigationController pushViewController:webViewController animated:NO];
}


- (void)getShopListDidSucceed:(NSDictionary *)addressInfo
{
    
    [TKUIUtil hiddenHUD];
     NSArray *arrays = [addressInfo safeObjectForKey:@"dataList"];
    
    if (arrays.count < 1) {
        return;
    }
    
    if (thread) {
        [self.shopNameArr removeAllObjects];
    }
    
    if (firstTime == 1) {
        firstTime++;
    }
    
    if (_currentPage >= [[addressInfo safeObjectForKey:@"totalPage"] integerValue]) {
        _hasMore = NO;
    }else{
        _hasMore = YES;
    }
    
    if (isRefresh) {
        _currentPage = 1;
        [self.shopNameArr addObject:[addressInfo safeObjectForKey:@"dataList"]];
    } else {
        _currentPage++;
        [self.shopNameArr addObjectsFromArray:[addressInfo safeObjectForKey:@"dataList"]];
    }
    
    if (arrays.count > 0) {
        
        NSDateFormatter *format=[[NSDateFormatter alloc] init];
        [format setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        
        NSDate *date = [NSDate date];
        NSString *shopListSetTimeStr =  [format stringFromDate:date];
        
        NSUserDefaults *shopList = [NSUserDefaults standardUserDefaults];
        [shopList setObject:arrays forKey:@"shopList"];
        [shopList setObject:shopListSetTimeStr forKey:@"shopListSetTime"];
        [shopList synchronize];
    }
    
    [self doneLoadingData];
    
}

- (void)getShopListDidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
}

//- (void)loadMoreData
//{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLatitude] doubleValue]] andLng:[NSString stringWithFormat:@"%f",[[ud objectForKey:CCLastLongitude] doubleValue]] andCurrentPage:self.currentPage PageSize:10];
//}

- (void)goToSearch:(UIButton *)sender
{
    ZSTECCSearchViewController *searchVC = [[ZSTECCSearchViewController alloc] init];
    searchVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchVC animated:YES];
}


-(void)dealloc{
    _carouselView.carouselDataSource = nil;
    _carouselView.carouselDelegate = nil;
    [self.engine cancelAllRequest];
}

- (void)loginDidFinish
{
    ZSTECCOrderListViewController * orderListView = [[ZSTECCOrderListViewController alloc] init];
    orderListView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:orderListView animated:YES];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [TKUIUtil hiddenHUD];
}


- (void)loadDataForPage:(int)pageIndex
{
    thread = NO;
    
    if (pageIndex == 1) {
        _currentPage = 1;
        
        isRefresh = YES;
    }else if (pageIndex > 1){

        isRefresh = NO;
    }
    
    
    [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",latitude] andLng:[NSString stringWithFormat:@"%f",longitude] andCurrentPage:_currentPage PageSize:10];
}


//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
    _loading = YES;
    _currentPage = 1;
    [self loadDataForPage:_currentPage];
}

- (void)loadMoreData
{
    
    if (_hasMore)
    {
        _loading = YES;
        [_moreButton displayIndicator];
        
        [self loadDataForPage:_currentPage+1];
    }
    
}

- (void)doneLoadingData
{
    _loading = NO;
    
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.shopTableView];
    
    if (self.shopNameArr.count != 0) {
        [self.shopTableView reloadData];
    }
}


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    [self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}


- (void)getMyLocation
{
    //判断用户是否使用GPS自动定位
    //    [self.view setUserInteractionEnabled:NO];
    __block CLLocation *location;
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
        
        do {
            location = [GetMyLocation getMyLocation];
        } while ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined);
        
        if([CLLocationManager locationServicesEnabled] &&
           ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse ))//如果打开GPS功能
        {
            self.location = [GetMyLocation getMyLocation];
            longitude = self.location.coordinate.longitude;
            latitude = self.location.coordinate.latitude;
            
            //获取定位地址
//           [self.engine getShopAddressInfoByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",latitude] andLng:[NSString stringWithFormat:@"%f",longitude]];
            
            //保存经纬度到 userDefaults 以便下次获取不到经纬度时用
            NSUserDefaults  *udLocation = [NSUserDefaults standardUserDefaults];
            [udLocation setObject:@(latitude) forKey:@"latitude"];
            [udLocation setObject:@(longitude) forKey:@"longitude"];
            [udLocation synchronize];
            
           [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:[NSString stringWithFormat:@"%f",latitude] andLng:[NSString stringWithFormat:@"%f",longitude] andCurrentPage:1 PageSize:10];
            
        }
        else if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied)//定位服务不可用
        {
            //从userDefaults获取上一次的经纬
            NSUserDefaults  *udLocation = [NSUserDefaults standardUserDefaults];
            NSString *latitudeStr = [udLocation stringForKey:@"latitude"];
            NSString *longitudeStr = [udLocation stringForKey:@"longitude"];
            
            [self.engine getShopListByUserId:[ZSTF3Preferences shared].UserId andLat:latitudeStr andLng:longitudeStr andCurrentPage:1 PageSize:10];
        }
    });
}

- (void)goToMap:(UITapGestureRecognizer *)tap
{
    NSInteger index = tap.view.tag;
    NSDictionary *oneDic = self.shopNameArr[index];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [param setValue:[NSString stringWithFormat:@"%@",[oneDic safeObjectForKey:@"lat"]] forKey:@"lat"];
    [param setValue:[NSString stringWithFormat:@"%@",[oneDic safeObjectForKey:@"lng"]] forKey:@"lng"];
    [param setValue:[oneDic safeObjectForKey:@"shopAddress"] forKey:@"address"];
    [self.engine getMapWithDictionary:param];
}

- (void)getMapSuccess:(NSDictionary *)response
{
    NSString *urlString = [response safeObjectForKey:@"url"];
    ZSTECCMapViewController *mapVC = [[ZSTECCMapViewController alloc] init];
    mapVC.urlString = urlString;
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];
}

- (void)getMapFailed:(int)resultCode
{
    
}

-(void)toLocationSetting{
    NSURL *url = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
    if ([[UIApplication sharedApplication] canOpenURL:url]) {
        //如果点击打开的话，需要记录当前的状态，从设置回到应用的时候会用到
        [[UIApplication sharedApplication] openURL:url];
        
    }
}

@end
