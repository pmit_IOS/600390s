//
//  ZSTECCShoppingCarCell.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShoppingCarCell.h"

@implementation ZSTECCShoppingCarCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.carFoodNameLB)
    {
        self.carFoodNameLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, (WIDTH - 30) * 0.4, 40)];
        self.carFoodNameLB.textColor = RGBA(85, 85, 85, 1);
        self.carFoodNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.carFoodNameLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.carFoodNameLB];
        
        self.carFoodPriceLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - 30) * 0.4 + 15, 10, (WIDTH - 30) * 0.3, 40)];
        self.carFoodPriceLB.textColor = RGBA(254, 78, 60, 1);
        self.carFoodPriceLB.font = [UIFont systemFontOfSize:14.0f];
        self.carFoodPriceLB.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.carFoodPriceLB];
        
        self.minusBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake((WIDTH - 30) * 0.7 + 15, 17, 25, 25)];
        [self.minusBtn setImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.minusBtn];
        
        self.carFoodCountLB = [[UILabel alloc] initWithFrame:CGRectMake(self.minusBtn.frame.origin.x + 25, 10, 30, 40)];
        self.carFoodCountLB.textAlignment = NSTextAlignmentCenter;
        self.carFoodCountLB.textColor = RGBA(85, 85, 85, 1);
        self.carFoodCountLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.carFoodCountLB];
        
        self.plusBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(self.carFoodCountLB.frame.origin.x + 30, 17, 25, 25)];
        [self.plusBtn setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.plusBtn];
        
    }
}

- (void)setCellDataFoodName:(NSString *)foodName FoodPrice:(NSString *)foodPrice FoodCount:(NSString *)foodCount
{
    self.carFoodNameLB.text = foodName;
    NSString *priceString = foodPrice;
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} range:NSMakeRange(0, 1)];
    self.carFoodPriceLB.attributedText = nodeString;
    self.carFoodCountLB.text = foodCount;
}

@end
