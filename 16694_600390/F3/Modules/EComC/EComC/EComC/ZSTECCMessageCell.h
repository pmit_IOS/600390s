//
//  ZSTECCMessageCell.h
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTECCBookSeatViewController;

@protocol ZSTECCMessageCellDelegate <NSObject>

- (void)changeSelectionWithIsSeat:(BOOL)isSeat;

@end

@interface ZSTECCMessageCell : UITableViewCell <UITextViewDelegate,UITextFieldDelegate>

@property (strong,nonatomic) UIImageView *iconIV;
@property (strong,nonatomic) UILabel *messageTitleLB;
@property (strong,nonatomic) UILabel *rightLB;
@property (assign,nonatomic) BOOL isBackUp;
@property (assign,nonatomic) BOOL isCanChoose;
@property (assign,nonatomic) BOOL isSeat;
@property (strong,nonatomic) UITextView *backUpTV;
@property (strong,nonatomic) UILabel *placeholderLB;
@property (strong,nonatomic) UITextField *nameTF;
@property (weak,nonatomic) id<ZSTECCMessageCellDelegate> delegate;
@property (assign,nonatomic) NSInteger selectedSexTag;
@property (assign,nonatomic) NSInteger selectedSeatTag;
@property (strong,nonatomic) UITextField *phoneTF;
@property (weak,nonatomic) ZSTECCBookSeatViewController *bookSeatVC;

- (void)createUI;
- (void)setCellDataWithIconIV:(NSString *)iconName messageTitle:(NSString *)titleString isSeat:(BOOL)isSeat isCanChoose:(BOOL)isCanChoose isBackUp:(BOOL)isBackUp SeatTypeArr:(NSArray *)seatTypeArr SexTypeArr:(NSArray *)sexTypeArr;


@end
