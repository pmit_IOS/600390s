//
//  ZSTECCOrderInfoViewController.h
//  EComC
//
//  Created by qiuguian on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"

@interface ZSTECCOrderInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTF3EngineECCDelegate>
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) ZSTF3Engine *engine;

@property (nonatomic,copy) NSString *bookType;
@property (nonatomic,copy) NSString *orderId;
@property (nonatomic,assign) BOOL isHasPay;
@property (copy,nonatomic) NSString *bespeakTypeName;
@property (strong,nonatomic) Class fromVC;

@end
