//
//  ZSTECCBalanceCell.m
//  EComC
//
//  Created by P&M on 15/10/23.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCBalanceCell.h"

@implementation ZSTECCBalanceCell

- (void)createBalanceUI
{
    if (!self.titleLB)
    {
        NSString *titleString = @"余额";
        CGSize stringSize = [titleString sizeWithFont:[UIFont systemFontOfSize:18.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, stringSize.width, 20)];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        self.titleLB.textColor = RGBA(85, 85, 85, 1);
        self.titleLB.text = titleString;
        [self.contentView addSubview:self.titleLB];
        
        NSString *exampleTimeString = @"当前可用余额¥0.6元";
        CGSize exampleSize = [exampleTimeString sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
        self.balanceLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, exampleSize.width, 20)];
        self.balanceLB.textColor = RGBA(153, 153, 153, 1);
        self.balanceLB.textAlignment = NSTextAlignmentRight;
        self.balanceLB.font = [UIFont systemFontOfSize:11.0f];
        self.balanceLB.text = exampleTimeString;
        self.accessoryView = self.balanceLB;
    }
}

- (void)setCellDataWithBalance:(NSString *)balanceString
{
    self.balanceLB.text = balanceString;
}


@end
