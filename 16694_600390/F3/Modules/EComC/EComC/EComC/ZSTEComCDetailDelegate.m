//
//  ZSTEComCDetailDelegate.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTEComCDetailDelegate.h"
#import "ZSTECCFoodDetailCell.h"
#import "ZSTECCFoodInfoViewController.h"

@implementation ZSTEComCDetailDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.detailFoodArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    ZSTECCFoodDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"foodDetailCell"];
    [cell createUI];
    cell.selectedArr = self.foodVC.selectedDishesArr;
    [cell getCellData:self.detailFoodArr[indexPath.row]];
    cell.minusBtn.tag = indexPath.row;
    cell.plusBtn.tag = indexPath.row + 1000;
    cell.isHasBook = NO;
    [cell.minusBtn addTarget:self action:@selector(changeBookValue:localHeight:) forControlEvents:UIControlEventTouchUpInside];
    [cell.plusBtn addTarget:self action:@selector(changeBookValue:localHeight:) forControlEvents:UIControlEventTouchUpInside];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    CGRect rectInTableView = [tableView rectForRowAtIndexPath:indexPath];
    CGRect rectInSuperview = [tableView convertRect:rectInTableView toView:[tableView superview]];
    cell.superHeight = rectInSuperview.origin.y;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.0f;
}

- (void)changeBookValue:(PMRepairButton *)sender localHeight:(NSInteger)height
{
    //大于1000是增加数量
    if (sender.tag >= 1000)
    {
        NSInteger cellIndex = sender.tag - 1000;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellIndex inSection:0];
        ZSTECCFoodDetailCell *cell = (ZSTECCFoodDetailCell *)[self.detailTableView cellForRowAtIndexPath:indexPath];
        
        NSDictionary *foodDic = self.detailFoodArr[cellIndex];
        NSString *dishId = [foodDic safeObjectForKey:@"dishId"];
        BOOL isHas = NO;
        for (NSInteger i = 0; i < self.foodVC.selectedDishesArr.count; i++)
        {
            NSDictionary *oneDic = self.foodVC.selectedDishesArr[i];
            if ([[[oneDic allKeys] firstObject] isEqualToString:dishId])
            {
                isHas = YES;
                break;
            }
        }
        
        //创建动画
        [self.foodVC createAnimation:cell.superHeight];
        
        if (isHas)
        {
            NSInteger numLBCount = [cell.numLB.text integerValue];
            numLBCount++;
            cell.numLB.text = [NSString stringWithFormat:@"%@",@(numLBCount)];
            [self.foodVC modifyFoodCount:foodDic IsHas:YES IsPlus:YES];

        }
        else
        {
            cell.minusBtn.hidden = NO;
            cell.numLB.hidden = NO;
            cell.numLB.text = @"1";
            [self.foodVC modifyFoodCount:foodDic IsHas:NO IsPlus:YES];
        }
        
    }
    else
    {
        NSInteger cellIndex = sender.tag;
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:cellIndex inSection:0];
        ZSTECCFoodDetailCell *cell = (ZSTECCFoodDetailCell *)[self.detailTableView cellForRowAtIndexPath:indexPath];
        NSDictionary *foodDic = self.detailFoodArr[cellIndex];
        [self.foodVC modifyFoodCount:foodDic IsHas:YES IsPlus:NO];
        if ([cell.numLB.text isEqualToString:@"1"])
        {
            cell.minusBtn.hidden = YES;
            cell.numLB.hidden = YES;
        }
        else
        {
            NSInteger numLBCount = [cell.numLB.text integerValue];
            numLBCount--;
            cell.numLB.text = [NSString stringWithFormat:@"%@",@(numLBCount)];
        }
    }
}

@end
