//
//  ZSTECCShopInfoAddressTableViewCell.h
//  EComC
//
//  Created by anqiu on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECCAutoLabel.h"

@protocol shopInfoAddressDelegate <NSObject>

-(void)callPhone:(NSString *)phone;

@end


@interface ZSTECCShopInfoAddressTableViewCell : UITableViewCell


@property (nonatomic,strong) UILabel *addressLabel;
@property (nonatomic,strong) UILabel *time;
@property (nonatomic,strong) UIButton *phone;
@property (nonatomic) NSInteger height;
@property (nonatomic,strong) id<shopInfoAddressDelegate> delegate;

-(void)createUI;

-(void)setCellData:(NSDictionary *)dic;

@end
