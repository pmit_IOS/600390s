//
//  ZSTECCMapViewController.h
//  EComC
//
//  Created by pmit on 15/8/28.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCMapViewController : UIViewController

@property (copy,nonatomic) NSString *urlString;
@property (copy,nonatomic) NSString *titleString;

@end
