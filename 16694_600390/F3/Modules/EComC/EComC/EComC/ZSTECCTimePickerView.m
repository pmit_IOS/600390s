//
//  ZSTECCTimePickerView.m
//  EComC
//
//  Created by pmit on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCTimePickerView.h"
#import "ZSTECCAutoLabel.h"

@interface ZSTECCTimePickerView ()

@property (strong,nonatomic) NSMutableArray *hourTimeArr;
@property (assign,nonatomic) BOOL isChangeDay;
@property (assign,nonatomic) BOOL isChangeHour;


@end

@implementation ZSTECCTimePickerView

- (void)createUIWithFrame:(CGRect)rect
{
    self.backgroundColor = RGBA(236, 236, 236, 1);
    self.i = 0;
    self.frame = rect;
    
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 80)];
    whiteView.backgroundColor = [UIColor whiteColor];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(10, 0, 60, 40);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.tag = 100;
    [cancelBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(clickBtnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(rect.size.width - 70, 0, 60, 30);
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancelBtn.tag = 101;
    [sureBtn addTarget:self action:@selector(clickBtnSure:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:sureBtn];
    
    //时间范围
    UILabel *timeLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 38, 80, 20)];
    timeLab.backgroundColor = [UIColor whiteColor];
    timeLab.text = [NSString stringWithFormat:@"时间范围:"];
    timeLab.font = [UIFont systemFontOfSize:14];
    [whiteView addSubview:timeLab];
    
    ZSTECCAutoLabel *rightLab = [[ZSTECCAutoLabel alloc] initWithFrame:CGRectMake(90, 40,WIDTH-90, 40)];
    rightLab.backgroundColor = [UIColor whiteColor];
    rightLab.lineBreakMode = UILineBreakModeCharacterWrap;
    rightLab.font = [UIFont systemFontOfSize:14];
    rightLab.numberOfLines = 2;
    NSMutableString *timeString = [[NSMutableString alloc] init];

    for (int i=0; i<self.timeArr.count; i++) {
        if (i == 0 ) {
            [timeString appendString:[NSString stringWithFormat:@"%@",self.timeArr[i]]];
        }else{
            [timeString appendString:[NSString stringWithFormat:@" %@",self.timeArr[i]]];
        }
      
    }
    rightLab.text = timeString;
    
//    int height = [rightLab getAttributedStringHeightWidthValue:200];
    CGFloat height = [timeString boundingRectWithSize:CGSizeMake(WIDTH-120, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.0f]} context:nil].size.height;
    rightLab.frame = CGRectMake(90, 40,WIDTH-120, height);
    [whiteView addSubview:rightLab];
    
    [self addSubview:whiteView];
    
    self.hourTimeArr = [NSMutableArray array];
    NSLog(@"timeaRR --> %@",self.timeArr);
    for (NSString *timeString in self.timeArr)
    {
        NSArray *oneTimeStringArr = [timeString componentsSeparatedByString:@"-"];
        NSInteger headTime = [[oneTimeStringArr firstObject] integerValue];
        NSInteger lastTime = [[oneTimeStringArr lastObject] integerValue];
        for (NSInteger i = headTime; i <= lastTime ; i++)
        {
            [self.hourTimeArr addObject:@(i)];
        }
    }
    
    self.minusArr = @[@"00",@"15",@"30",@"45"];
    
    
    self.selectedYear = [self.nowYear integerValue];
    self.selectedMonth = [self.nowMonth integerValue];
    self.selectedDay = [self.nowDay integerValue];
    
    
    
    
   
    
    NSLog(@"===>self.hourTimeArr==>%@",self.hourTimeArr);
    
    UIPickerView *pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, rect.size.width, rect.size.height - 40)];
    pick.delegate = self;
    pick.dataSource = self;
    [pick selectRow:0 inComponent:1 animated:YES];
    [pick selectRow:0 inComponent:2 animated:YES];
    [self addSubview:pick];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    
    if (component == 0)
    {
        return 7;
    }
    else if (component == 1)
    {
        NSInteger timeCount = 0;
        
        if ([self.nowDay integerValue] == self.selectedDay)
        {
            for (NSString *timeString in self.timeArr)
            {
                NSArray *headFootTime = [timeString componentsSeparatedByString:@"-"];
                NSInteger startTime = [[headFootTime firstObject] integerValue];
                NSInteger lastTime = [[headFootTime lastObject] integerValue];
                if ([self.nowHour integerValue] > lastTime)
                {
                    continue;
                }
                else
                {
                    if ([self.nowHour integerValue] < startTime)
                    {
                        NSInteger tillLastTime = [[headFootTime lastObject] integerValue] - [[headFootTime firstObject] integerValue] + 1;
                        timeCount += tillLastTime;
                    }
                    else
                    {
                        if ([self.nowMin integerValue]>= 30)
                        {
                            NSInteger tillLastTime = [[headFootTime lastObject] integerValue] - [self.nowHour integerValue];
                            timeCount += tillLastTime;
                        }
                        else
                        {
                            NSInteger tillLastTime = [[headFootTime lastObject] integerValue] - [self.nowHour integerValue] + 1;
                            timeCount += tillLastTime;
                        }
                        
                    }
                }
            }
        }
        else
        {
            for (NSString *timeString in self.timeArr)
            {
                NSArray *headFootTime = [timeString componentsSeparatedByString:@"-"];
                NSInteger lastTime = [[headFootTime lastObject] integerValue] - [[headFootTime firstObject] integerValue] + 1;
                timeCount += lastTime;
            }
        }
        
        return timeCount;
    }
    else
    {
        if ([self.nowMin integerValue] >= 25)
        {
            if ([self.nowHour integerValue] == self.selectedHour && [self.nowDay integerValue] == self.selectedDay)
            {
                if ([self.nowMin integerValue] > 25)
                {
                    return self.minusArr.count;
                }
                else
                {
                    return 1;
                }
            }
            else
            {
                return self.minusArr.count;
            }
            
        }
        else
        {
            if (self.nowMin == 0)
            {
                if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                {
                    return 2;
                }
                else
                {
                    return self.minusArr.count;
                }
            }
            else
            {
                if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                {
                    return 1;
                }
                else
                {
                    return self.minusArr.count;
                }
            }
        }
        
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    BOOL isBigMonth = NO;
    BOOL isTwo = NO;
    BOOL isRun = NO;
    
    if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
    {
        isRun = YES;
    }
    else
    {
        isRun = NO;
    }
    
    if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
        {
            isBigMonth = YES;
            isTwo = NO;
        }
    else if ([self.nowMonth isEqualToString:@"2"])
    {
        isBigMonth = NO;
        isTwo = YES;
    }
    else
    {
        isBigMonth = NO;
        isTwo = NO;
    }
    
    if (component == 0)
    {
        NSInteger showDay = [self.nowDay integerValue] + row;
        NSInteger showMonth = [self.nowMonth integerValue];
        if (isBigMonth)
        {
            if (showDay > 31)
            {
                showMonth += 1;
                if (showMonth > 12)
                {
                    showMonth = 1;
                }
                showDay = showDay - 31;
            }
        }
        else if (!isBigMonth && !isTwo)
        {
            if (showDay > 30)
            {
                showMonth += 1;
                showDay = showDay - 30;
            }
        }
        else if (isRun)
        {
            if (showDay > 29)
            {
                showMonth += 1;
                showDay = showDay - 29;
            }
        }
        else
        {
            if (showDay > 28)
            {
                showMonth += 1;
                showDay = showDay - 28;
            }
        }
        return [NSString stringWithFormat:@"%@月%@日",@(showMonth),@(showDay)];
    }
    else if (component == 1)
    {

        if ([self.nowDay integerValue] == self.selectedDay)
        {
            NSInteger lessTime = 0;
            for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
            {
                NSInteger thisTime = [self.hourTimeArr[i] integerValue];
                if ([self.nowHour integerValue] <= thisTime)
                {
                    lessTime = i;
                    break;
                }
            }
            
            if ([self.nowMin integerValue] >= 30)
            {
                if (row + lessTime + 1 >= self.hourTimeArr.count)
                {
                    [pickerView selectRow:1 inComponent:0 animated:YES];
                    [pickerView reloadAllComponents];
                    return [NSString stringWithFormat:@"%@",@([self.hourTimeArr[0] integerValue])];
                }
                else
                {
                    return [NSString stringWithFormat:@"%@",@([self.hourTimeArr[row + lessTime + 1] integerValue])];
                }
                
            }
            else
            {
               return [NSString stringWithFormat:@"%@",@([self.hourTimeArr[row + lessTime] integerValue])];
            }
            
        }
        else
        {
            return [NSString stringWithFormat:@"%@",@([self.hourTimeArr[row] integerValue])];
        }
        
        
    }
    else 
    {
        if ([self.nowMin integerValue] >= 25)
        {
            if ([self.nowMin integerValue] > 25)
            {
                return [NSString stringWithFormat:@"%@",self.minusArr[row]];
            }
            else
            {
                return [NSString stringWithFormat:@"%@",@(45)];
            }
        }
        else
        {
            if (self.nowMin == 0)
            {
                if (row == 0)
                {
                    if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                    {
                        return [NSString stringWithFormat:@"%@",@(30)];
                    }
                    else
                    {
                        return [NSString stringWithFormat:@"%@",self.minusArr[row]];
                    }
                }
                else
                {
                    if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                    {
                        return [NSString stringWithFormat:@"%@",@(45)];
                    }
                    else
                    {
                        return [NSString stringWithFormat:@"%@",self.minusArr[row]];
                    }
                }
            }
            else
            {
                if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                {
                    return [NSString stringWithFormat:@"%@",@(45)];
                }
                else
                {
                    return [NSString stringWithFormat:@"%@",self.minusArr[row]];
                }
            }
        }
    }
    
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        BOOL isBigMonth = NO;
        BOOL isTwo = NO;
        BOOL isRun = NO;
        
        if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
        {
            isRun = YES;
        }
        else
        {
            isRun = NO;
        }
        
        if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
        {
            isBigMonth = YES;
            isTwo = NO;
        }
        else if ([self.nowMonth isEqualToString:@"2"])
        {
            isBigMonth = NO;
            isTwo = YES;
        }
        else
        {
            isBigMonth = NO;
            isTwo = NO;
        }
        

        NSInteger showDay = [self.nowDay integerValue] + row;
        NSInteger showMonth = [self.nowMonth integerValue];
        if (isBigMonth)
        {
            if (showDay > 31)
            {
                showMonth += 1;
                if (showMonth > 12)
                {
                    showMonth = 1;
                }
                showDay = showDay - 31;
            }
        }
        else if (!isBigMonth && !isTwo)
        {
            if (showDay > 30)
            {
                showMonth += 1;
                showDay = showDay - 30;
            }
        }
        else if (isRun)
        {
            if (showDay > 29)
            {
                showMonth += 1;
                showDay = showDay - 29;
            }
        }
        else
        {
            if (showDay > 28)
            {
                showMonth += 1;
                showDay = showDay - 28;
            }
        }
        
        if (showMonth < [self.nowMonth integerValue])
        {
            self.selectedYear = [self.nowYear integerValue] + 1;
            
        }
        else
        {
            self.selectedYear = [self.nowYear integerValue];
        }
        
        self.selectedMonth = showMonth;
        self.selectedDay = showDay;
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
        
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
        self.selectedHour = [self.hourTimeArr[0] integerValue];
        self.selectedMinus = 0;
        
    }
    else if (component == 1)
    {
        if ([self.nowDay integerValue] == self.selectedDay)
        {
            NSInteger lessTime = 0;
            for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
            {
                NSInteger thisTime = [self.hourTimeArr[i] integerValue];
                if ([self.nowHour integerValue] <= thisTime)
                {
                    lessTime = i;
                    break;
                }
            }
            
            if ([self.nowMin integerValue] >= 30)
            {
                if (row + lessTime + 1 >= self.hourTimeArr.count)
                {
                    self.selectedHour = [self.hourTimeArr[0] integerValue];
                }
                else
                {
                    self.selectedHour = [self.hourTimeArr[row + lessTime + 1] integerValue];
                }
            }
            else
            {
                self.selectedHour = [self.hourTimeArr[row + lessTime] integerValue];
            }
            
            
        }
        else
        {
            self.selectedHour = [self.hourTimeArr[row] integerValue];
        }
        
        
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        
        if ([self.nowHour integerValue] == self.selectedHour && [self.nowDay integerValue] == self.selectedDay)
        {
            if ([self.nowMin integerValue] >= 25)
            {
                if ([self.nowMin integerValue] > 25)
                {
                    self.selectedMinus = 0;
                }
                else
                {
                    if (self.nowMin == 0)
                    {
                        self.selectedMinus = 30;
                    }
                    else
                    {
                        self.selectedMinus = 45;
                    }
                }
            }
            else
            {
                if (self.nowMin == 0)
                {
                    self.selectedMinus = 30;
                }
                else
                {
                    self.selectedMinus = 45;
                }
            }
        }
        else
        {
            self.selectedMinus = 0;
        }
        
    }
    else
    {
        if ([self.nowMin integerValue] >= 25)
        {
            if ([self.nowMin integerValue] > 25)
            {
               self.selectedMinus = [self.minusArr[row] integerValue];
            }
            else
            {
                self.selectedMinus = 45;
            }
        }
        else
        {
            if (self.nowMin == 0)
            {
                if (row == 0)
                {
                    if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                    {
                        self.selectedMinus = 30;
                    }
                    else
                    {
                        self.selectedMinus = [self.minusArr[row] integerValue];
                    }
                }
                else
                {
                    if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                    {
                        self.selectedMinus = 45;
                    }
                    else
                    {
                        self.selectedMinus = [self.minusArr[row] integerValue];
                    }
                }
            }
            else
            {
                if (self.selectedHour == [self.nowHour integerValue] && [self.nowDay integerValue] == self.selectedDay)
                {
                    self.selectedMinus = 45;
                }
                else
                {
                    self.selectedMinus = [self.minusArr[row] integerValue];
                }
            }
        }
        
        
    }
}

- (void)clickBtnSure:(UIButton *)sender
{
    if ([self.timeDelegate respondsToSelector:@selector(sureTimeSelected:)])
    {
        
        if (self.selectedYear == 0 || self.selectedMonth == 0 || self.selectedDay == 0)
        {
            self.selectedYear = [self.nowYear integerValue];
            self.selectedMonth = [self.nowMonth integerValue];
            self.selectedDay = [self.nowDay integerValue];
            //self.selectedHour = [self.nowHour integerValue];
            //self.selectedMinus = [self.nowMin integerValue];
        }
        else
        {
            BOOL isSuit = NO;
            for (NSString *timeString in self.timeArr)
            {
                NSArray *checkTimeArr = [timeString componentsSeparatedByString:@"-"];
                if (self.selectedHour < [[[[checkTimeArr firstObject] componentsSeparatedByString:@":"] firstObject] integerValue] || self.selectedHour > [[[[checkTimeArr lastObject] componentsSeparatedByString:@":"] firstObject] integerValue])
                {
                    continue;
                }
                else
                {
                    if (self.selectedHour == [[[[checkTimeArr firstObject] componentsSeparatedByString:@":"] firstObject] integerValue])
                    {
                        if (self.selectedMinus < [[[[checkTimeArr firstObject] componentsSeparatedByString:@":"] lastObject] integerValue])
                        {
                            isSuit = NO;
                        }
                        else
                        {
                            isSuit = YES;
                            break;
                        }
                    }
                    else if (self.selectedHour == [[[[checkTimeArr lastObject] componentsSeparatedByString:@":"] firstObject] integerValue])
                    {
                        if (self.selectedMinus > [[[[checkTimeArr firstObject] componentsSeparatedByString:@":"] lastObject] integerValue])
                        {
                            isSuit = NO;
                        }
                        else
                        {
                            isSuit = YES;
                            break;
                        }
                    }
                    else
                    {
                        isSuit = YES;
                        break;
                    }
                }
                
                if (self.selectedHour == [[self.hourTimeArr lastObject] integerValue]) {
                    
                    NSString *timePart = [self.timeArr lastObject];
                    NSArray *inTimePart = [timePart componentsSeparatedByString:@"-"];
                    NSString *lastTimePart = [inTimePart lastObject];
                    NSArray *lastTimePartArr = [lastTimePart componentsSeparatedByString:@":"];
                    NSInteger lastTimePartMin = [[lastTimePartArr lastObject] integerValue];
                    
                    if (self.selectedMinus >= lastTimePartMin || lastTimePartMin < 30 || self.selectedMinus > lastTimePartMin - 30)
                    {
                        isSuit = NO;
                    }
                    else
                    {
                        isSuit = YES;
                        break;
                    }
                    
                }
                else if (self.selectedHour == [[self.hourTimeArr lastObject] integerValue] - 1)
                {
                    NSString *timePart = [self.timeArr lastObject];
                    NSArray *inTimePart = [timePart componentsSeparatedByString:@"-"];
                    NSString *lastTimePart = [inTimePart lastObject];
                    NSArray *lastTimePartArr = [lastTimePart componentsSeparatedByString:@":"];
                    NSInteger lastTimePartMin = [[lastTimePartArr lastObject] integerValue];
                    if (lastTimePartMin == 0)
                    {
                        if (self.selectedMinus > 30)
                        {
                            isSuit = NO;
                        }
                        else
                        {
                            isSuit = YES;
                            break;
                        }
                    }
                    else
                    {
                        if (self.selectedMinus + 30 <= 60)
                        {
                            isSuit = YES;
                            break;
                        }
                        else
                        {
                            if (self.selectedMinus + 30 - 60 > lastTimePartMin)
                            {
                                isSuit = NO;
                            }
                            else
                            {
                                isSuit = YES;
                                break;
                            }
                        }
                    }
                }
            }
            
            if (!isSuit)
            {
                [TKUIUtil alertInView:self.window withTitle:NSLocalizedString(@"这个时间店铺暂不接受预定哦", nil) withImage:nil];
            }
            else
            {
                NSString *month = @"";
                if (self.selectedMonth >= 10) {
                    month = [NSString stringWithFormat:@"%ld",self.selectedMonth];
                }else{
                    month = [NSString stringWithFormat:@"0%ld",self.selectedMonth];
                }

                NSString *day = @"";
                if (self.selectedDay >= 10) {
                    day = [NSString stringWithFormat:@"%ld",self.selectedDay];
                }else{
                    day = [NSString stringWithFormat:@"0%ld",self.selectedDay];
                }
                
                NSString *hour = @"";
                if (self.selectedHour >= 10) {
                    hour = [NSString stringWithFormat:@"%ld",self.selectedHour];
                }else{
                    hour = [NSString stringWithFormat:@"0%ld",self.selectedHour];
                }
                
                NSString *mini = @"";
                if (self.selectedMinus >= 10) {
                    mini = [NSString stringWithFormat:@"%ld",self.selectedMinus];
                }else{
                    mini = [NSString stringWithFormat:@"0%ld",self.selectedMinus];
                }
                
                
                [self.timeDelegate sureTimeSelected:[NSString stringWithFormat:@"%@-%@-%@ %@:%@",@(self.selectedYear),month,day,hour,mini]];
            }
          
        }
        
        
    }
}

- (void)clickBtnCancel:(UIButton *)sender
{
    if ([self.timeDelegate respondsToSelector:@selector(cancelTimeSelected)])
    {
        [self.timeDelegate cancelTimeSelected];
    }
}

@end
