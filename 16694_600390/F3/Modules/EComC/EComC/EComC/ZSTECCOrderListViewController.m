//
//  ZSTECCOrderListViewController.m
//  EComC
//
//  Created by qiuguian on 8/10/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import "ZSTECCOrderListViewController.h"
#import "ZSTECCOrderListCell.h"
#import "ZSTECCOrderInfoViewController.h"
#import "ZSTECCEvaluateViewController.h"
#import "ZSTECCNoDataCell.h"
#import "ZSTECCPayWebViewController.h"
#import "ZSTGlobal+EComC.h"
#import "ZSTECCHomeViewController.h"
#import "MoreButton.h"


@interface ZSTECCOrderListViewController (){

    int _pageIndex;
    int _pageIndexed;
    BOOL _hasMore;
    BOOL _hasMored;
    MoreButton *_moreButton;
    BOOL  isRefresh;
    BOOL _loading;  //是否正在加载中 待消费
    BOOL _loadinged;  //是否正在加载中  已消费
    
    NSInteger totalPage;//总页数
}

@end

static NSString *const shopCell = @"orderListCell";

@implementation ZSTECCOrderListViewController{

    UIView *tabView;
    
    UIButton *isPayBtn;
    UIButton *notPayBtn;
    
    ZSTECCRoundRectView * isPayLine;
    ZSTECCRoundRectView * notPayLine;
    
    NSArray *initDataArry;
    
    NSMutableDictionary *orderDic;
    
    NSString *orderStatus;
    
    NSInteger firstTime;
    
    NSString *_orderCode;
    NSString *_orderId;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate = self;
    self.engine = engine;
    
    if (self.isFromHome)
    {
        self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    }
    else
    {
        self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"回到首页", @"") target:self selector:@selector (popToHomeVC)];
    }
    
    self.navigationItem.titleView= [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单列表", nil)];
    self.view.backgroundColor =RGBCOLOR(206, 206, 206);
    
    firstTime = 0;
    [self createTableView];
    
    //切换视图按钮
    tabView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH,40)];
    tabView.backgroundColor = [UIColor whiteColor];
    [self initTabBtnView];
    [self.view addSubview:tabView];
    
    if (_refreshHeaderView == nil) {
        
       	EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.orderTableView.bounds.size.height, self.view.frame.size.width, self.orderTableView.bounds.size.height)];
        view.delegate = self;
        [self.orderTableView addSubview:view];
        _refreshHeaderView = view;
    }
    
    [_refreshHeaderView refreshLastUpdatedDate];
    
     self.dataArray = [[NSMutableArray alloc] initWithCapacity:10];
     self.dataArrayed = [[NSMutableArray alloc] initWithCapacity:10];
    
    [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];

}

-(void)viewDidAppear:(BOOL)animated{
    
    if (![orderStatus isEqualToString:@"2"]) {
        orderStatus = @"1";//1:待消费,2:已消费
    }
    
    [self.dataArray removeAllObjects];
    [self.dataArrayed removeAllObjects];
    [self loadDataForPage:1];
    
}

#pragma mark - 初始化切换视图按钮
-(void)initTabBtnView{
    
    //待消费 按钮
    isPayBtn= [[UIButton alloc] initWithFrame:CGRectMake(0, 5, 160, 40-4-5)];
    isPayBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    isPayBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [isPayBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    isPayBtn.tag = 1;
    [isPayBtn setTitle:@"待消费" forState:UIControlStateNormal];
    [isPayBtn addTarget:self action:@selector(notPayBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview:isPayBtn];
    
    //底线
    isPayLine = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(0, 40-1) toPoint:CGPointMake(WIDTH/2, 30) borderWidth:1.0f borderColor:[UIColor redColor]];
    [tabView addSubview:isPayLine];
    
    //已消费 按钮
    notPayBtn = [[UIButton alloc] initWithFrame:CGRectMake(160, 5, 160, 40-4-5)];
    notPayBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    notPayBtn.titleLabel.font = [UIFont systemFontOfSize:14];
    [notPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    notPayBtn.tag =2 ;
    [notPayBtn setTitle:@"已消费" forState:UIControlStateNormal];
    [notPayBtn addTarget:self action:@selector(isPayBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [tabView addSubview: notPayBtn];
    
    //底线
    notPayLine = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(160, 40-1) toPoint:CGPointMake(320, 30) borderWidth:1.0f borderColor:[UIColor redColor]];
    notPayLine.hidden = YES;
    [tabView addSubview:notPayLine];
    
}

#pragma mark - 待消费
-(void)notPayBtnAction{
    
    //待消费
    isPayLine.hidden = NO;
    [isPayBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
    //已消费
    notPayLine.hidden = YES;
    [notPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    orderStatus = @"1";//1:待消费,2:已消费
    [self.dataArray removeAllObjects];
    [self.engine getEcomCOrderListByUserId:[ZSTF3Preferences shared].UserId andStatus:orderStatus curPage:@"1" pageSize:@"10"];
}

#pragma mark - 已消费
-(void)isPayBtnAction{
    
    //待消费
    isPayLine.hidden = YES;
    [isPayBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    //已消费
    notPayLine.hidden = NO;
    [notPayBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];

    orderStatus = @"2";//1:待消费,2:已消费
    _pageIndexed = 1;
    [self.dataArrayed removeAllObjects];
    [self.engine getEcomCOrderListByUserId:[ZSTF3Preferences shared].UserId andStatus:orderStatus curPage:[NSString stringWithFormat:@"%d",_pageIndexed] pageSize:@"10"];
}




#pragma mark - 创建tableView
-(void)createTableView{

    self.orderTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 40, WIDTH, HEIGHT - 64 - 40)];
    self.orderTableView.delegate = self;
    self.orderTableView.dataSource = self;
    [self.orderTableView registerClass:[ZSTECCOrderListCell class] forCellReuseIdentifier:shopCell];
    self.orderTableView.separatorStyle = UITableViewCellAccessoryNone;
    self.orderTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.view addSubview:self.orderTableView];
}


#pragma mark 数据源 tableView
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([orderStatus isEqualToString:@"1"]) {
        if (self.dataArray.count == 0) {
            return 1;
        }
    }else{
       
        if (self.dataArrayed.count == 0) {
            return 1;
        }
    }
    
    if ([orderStatus isEqualToString:@"1"]) {
        //待消费
        if (_hasMore) {
            return self.dataArray.count + 1;
        }
            
    }else{
        //己消费
        if (_hasMored) {
            return self.dataArrayed.count + 1;
        }
    }
    
    if ([orderStatus isEqualToString:@"1"]) {
        //待消费
        return self.dataArray.count;
    }else{
        //己消费
        return self.dataArrayed.count;
    }

    
    return 0;
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
   return 1;

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *shView = [[UIView alloc] init];
    shView.backgroundColor = RGBA(244, 244, 244, 244);
    return shView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([orderStatus isEqualToString:@"1"]) {
        if (self.dataArray.count == 0) {
            return 60.0f;
        }
        
        if (indexPath.section == self.dataArray.count) {
            return 44.0f;
        }

    }else{
       
        if (self.dataArrayed.count == 0) {
            return 60.0f;
        }
        
        if (indexPath.section == self.dataArrayed.count) {
            return 44.0f;
        }

    
    }
    
    return 145.0f;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *moreCellIdentifier = @"MoreCell";
    
    NSInteger num = 0;
    BOOL more = NO;
    if ([orderStatus isEqualToString:@"1"]) {
        num = self.dataArray.count;
        more = _hasMore;
    }else{
        num = self.dataArrayed.count;
        more = _hasMored;
    }
    
    
    
    [_moreButton hideIndicator];
    if (indexPath.section == num && more)
    {
        UITableViewCell *moreCell = [tableView dequeueReusableCellWithIdentifier:moreCellIdentifier];
        if (moreCell == nil)
        {
            moreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreCellIdentifier];
            _moreButton = [MoreButton button];
            [_moreButton setTitle:@"点击加载更多" forState:UIControlStateNormal];
            [_moreButton addTarget:self action:@selector(loadMoreData) forControlEvents:UIControlEventTouchUpInside];
            [moreCell.contentView addSubview:_moreButton];
        }
        
        return moreCell;
    }

    
    //无数据
    if ([orderStatus isEqualToString:@"1"]){
        if (self.dataArray.count == 0) {
            static NSString *noDataCell = @"noDataCell";
            ZSTECCNoDataCell *cell = (ZSTECCNoDataCell *)[tableView dequeueReusableCellWithIdentifier:noDataCell];
            if (!cell) {
                cell = [[ZSTECCNoDataCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noDataCell];
            }
            
            if (firstTime == 0) {
                cell.tip.text = @"";
                cell.hidden = YES;
            }else{
                cell.tip.text = @"暂无数据";
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
    }else{
        if (self.dataArrayed.count == 0) {
            static NSString *noDataCell = @"noDataCell";
            ZSTECCNoDataCell *cell = (ZSTECCNoDataCell *)[tableView dequeueReusableCellWithIdentifier:noDataCell];
            if (!cell) {
                cell = [[ZSTECCNoDataCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:noDataCell];
            }
            
            if (firstTime == 0) {
                cell.tip.text = @"";
                cell.hidden = YES;
            }else{
                cell.tip.text = @"暂无数据";
            }
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }

    
    }
    
    
    static NSString *orderListCell = @"orderListCell";
    ZSTECCOrderListCell *cell = (ZSTECCOrderListCell *)[tableView dequeueReusableCellWithIdentifier:orderListCell];
    if (!cell) {
        cell = [[ZSTECCOrderListCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:orderListCell];
    }
    cell.delagate = self;
    [cell createUI];
    
    if ([orderStatus isEqualToString:@"1"]) {
        //待消费
        [cell setCellData:[self.dataArray objectAtIndex:indexPath.section]];
    }else{
        //己消费
        [cell setCellData:[self.dataArrayed objectAtIndex:indexPath.section]];
    }
    
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return  cell;

}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
}

#pragma mark - 订单详情
- (void)orderInfoAction:(NSString*)orderId bookType:(NSString *)bookType AndHasPay:(BOOL)isHasPay BtypeName:(NSString *)bTypeName
{
    
    ZSTECCOrderInfoViewController *orderInfo = [[ZSTECCOrderInfoViewController alloc] init];
    orderInfo.bookType = bookType;
    orderInfo.orderId = orderId;
    orderInfo.isHasPay = isHasPay;
    orderInfo.bespeakTypeName = bTypeName;
    [self.navigationController pushViewController:orderInfo animated:YES];
    
}

#pragma mark - 跳转到评论
-(void)goToEvaluate:(NSString *)shopId andOrderId:(NSString *)orderId{
    
    ZSTECCEvaluateViewController *evaluateView = [[ZSTECCEvaluateViewController alloc] init];
    evaluateView.shopId = shopId;
    evaluateView.orderId = orderId;
    [self.navigationController pushViewController:evaluateView animated:YES];
    
}


#pragma mark - 返回个人订单列表信息
-(void)getEcomCOrderListDidSucceed:(NSDictionary *)orderList{
    
    [TKUIUtil hiddenHUD];
    initDataArry = [orderList safeObjectForKey:@"dataList"];
    totalPage = [[orderList safeObjectForKey:@"totalPage"] integerValue];
    
    if ([orderStatus isEqualToString:@"1"]) {
        if (_pageIndex >= totalPage) {
            
            _hasMore = NO;
            
        }else{
            _hasMore = YES;
        }
    }else{
        if (_pageIndexed >= totalPage) {
            
            _hasMored = NO;
            
        }else{
            _hasMored = YES;
        }
    
    }
    
    
    if (isRefresh) {
        
        if ([orderStatus isEqualToString:@"1"]) {
            //待消费
            _pageIndex = 1;
            [self.dataArray addObjectsFromArray:initDataArry];
        }else{
             //己消费
            _pageIndexed = 1;
            [self.dataArrayed addObjectsFromArray:initDataArry];
        }
        
    } else {
        
        if ([orderStatus isEqualToString:@"1"]) {
            //待消费
            _pageIndex++;
            [self.dataArray addObjectsFromArray:initDataArry];
        }else{
            //己消费
            _pageIndexed++;
            [self.dataArrayed addObjectsFromArray:initDataArry];
        }

    }
    
    firstTime = 1;
    [self doneLoadingData];
}

- (void)doneLoadingData
{
    if ([orderStatus isEqualToString:@"1"]) {
         _loading = NO;
    }else{
         _loadinged = NO;
    }
    
   
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.orderTableView];
    [self.orderTableView reloadData];
}

-(void)getEcomCOrderListDidFailed:(int)resultCode{

    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"加载失败", nil) withImage:nil];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.orderTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}


#pragma mark - 跳转到支付界页
-(void)payBtnAction:(NSString *)orderCode andOrderId:(NSString *)orderId{
    
    [TKUIUtil hiddenHUD];
    _orderCode = orderCode;
    _orderId = orderId;
    [self.engine validateGotoPayByOrderId:orderId];
    
}

-(void)validateGotoPayDidSucceed:(NSDictionary *)responseInfo{
   
    self.view.userInteractionEnabled = YES;
    NSString *urlString = [NSString stringWithFormat:@"%@?orderCode=%@",payMealUrl,_orderCode];
    ZSTECCPayWebViewController *payWebVC = [[ZSTECCPayWebViewController alloc] init];
    payWebVC.urlString = urlString;
    payWebVC.orderId = _orderId;
    [self.navigationController pushViewController:payWebVC animated:YES];

}

-(void)validateGotoPayDidFailed:(NSDictionary *)failedInfo{
  
    NSString *message = [failedInfo safeObjectForKey:@"message"];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];

}


#pragma mark - 取消订单
-(void)cancelBtnAction:(NSString *)orderId{
  
    [self.engine cancelOrderByOrderId:orderId andUserId:[ZSTF3Preferences shared].UserId];
}

-(void)cancelOrderDidSucceed:(NSDictionary *)cancelInfo{
  
    NSLog(@"取消订单 成功");
    
    NSString *currentPage = @"";
    if ([orderStatus isEqualToString:@"1"]) {
        currentPage = [NSString stringWithFormat:@"%d",_pageIndex];
        [self.dataArray removeAllObjects];
    }else{
        currentPage = [NSString stringWithFormat:@"%d",_pageIndexed];
        [self.dataArrayed removeAllObjects];
    }
    
    [self.engine getEcomCOrderListByUserId:[ZSTF3Preferences shared].UserId andStatus:orderStatus curPage:currentPage pageSize:@"10"];
}

-(void)cancelOrderDidFailed:(int)resultCode{

    NSLog(@"取消订单 失败");
}

- (void)popToHomeVC
{
    if ([[self.navigationController.childViewControllers firstObject] isKindOfClass:[ZSTECCHomeViewController class]])
    {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        ZSTECCHomeViewController *homeVC = (ZSTECCHomeViewController *)self.navigationController.childViewControllers[1];
        [self.navigationController popToViewController:homeVC animated:YES];
    }
}


- (void)loadDataForPage:(int)pageIndex
{
    
    int currentPage = 0;
    if ([orderStatus isEqualToString:@"1"]) {
        currentPage= _pageIndex;
    }else{
        currentPage= _pageIndexed;
    }
    
    
    if (pageIndex == 1) {
        
        if ([orderStatus isEqualToString:@"1"]) {
            _pageIndex = 1;
        }else{
            _pageIndexed = 1;
        }
        
        //请求订单
        [self.engine getEcomCOrderListByUserId:[ZSTF3Preferences shared].UserId andStatus:orderStatus curPage:[NSString stringWithFormat:@"%d",1] pageSize:@"10"];
        
        isRefresh = YES;
        
    }else if (pageIndex > 1){
        //请求订单
        [self.engine getEcomCOrderListByUserId:[ZSTF3Preferences shared].UserId andStatus:orderStatus curPage:[NSString stringWithFormat:@"%d",currentPage] pageSize:@"10"];
        
        isRefresh = NO;
    }
}


//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
     //加载第1页
    if ([orderStatus isEqualToString:@"1"]) {
       
        _loading = YES;
        _pageIndex = 1;
        [self loadDataForPage:_pageIndex];
    }else{
        //加载第1页
        _loadinged = YES;
        _pageIndexed = 1;
        [self loadDataForPage:_pageIndexed];
    
    }
}


- (void)loadMoreData
{
    
    if ([orderStatus isEqualToString:@"1"]) {
        if (_hasMore)
        {
            _loading = YES;
            [_moreButton displayIndicator];
            
            [self loadDataForPage:_pageIndex+1];
        }
    }else{
        
        if (_hasMored)
        {
            _loadinged = YES;
            [_moreButton displayIndicator];
            
            [self loadDataForPage:_pageIndexed+1];
        }

    }
    
}


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    [self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    if ([orderStatus isEqualToString:@"1"]) {
         return _loading;
    }else{
         return _loadinged;
    }
    return nil;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

- (void)dealloc
{
    self.dataArray = nil;
    self.dataArrayed = nil;
    [self.engine cancelAllRequest];
}
@end
