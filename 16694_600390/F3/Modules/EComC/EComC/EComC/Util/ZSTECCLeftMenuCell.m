//
//  ZSTECCLeftMenuCell.m
//  EComC
//
//  Created by 阮飞 on 15/8/8.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCLeftMenuCell.h"

@implementation ZSTECCLeftMenuCell

 
-(void)createUI{
    self.contentView.backgroundColor = RGBCOLOR(246, 246, 246);
    
    if (!self.category)
    {
        self.category = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 90, 60)];
        self.category.textColor = [UIColor grayColor];
        self.category.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.category];
    }
    
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.frame];
    self.selectedBackgroundView.backgroundColor = [UIColor whiteColor];
}

- (void)setCellDataWithTypeName:(NSString *)typeNameString
{
    self.category.text = typeNameString;
}

@end
