//
//  ZSTECCRoundRectView.h
//  EComC
//
//  Created by 阮飞 on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCRoundRectView : UIView
//画圆角边框
- (id)initWithFrame:(CGRect)frame radius:(CGFloat) radius  borderWidth:(CGFloat) border borderColor:(UIColor *) color;
//画线
- (id)initWithPoint:(CGPoint) beginPoint toPoint:(CGPoint) endPoint borderWidth:(CGFloat) border borderColor:(UIColor *) color;
- (void) showImage:(CGRect)frame image:(UIImage *) image;
- (void) showText:(CGRect) frame  text:(NSString *) text;
- (void) showText:(CGRect) frame  text:(NSString *) text font:(UIFont *)font;
@end
