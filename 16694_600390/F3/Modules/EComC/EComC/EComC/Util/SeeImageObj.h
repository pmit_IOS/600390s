//
//  SeeImageObj.h
//  F3
//
//  Created by anqiu on 15/8/31.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SeeImageObj : NSObject

@property (nonatomic,strong) NSString *name;
@property (nonatomic,strong) NSString *imgTitle;
@property (nonatomic,strong) NSString *imgContent;
@property (nonatomic,strong) NSString *url;
@property (nonatomic,strong) NSString *whidth;
@property (nonatomic,strong) NSString *height;

@end
