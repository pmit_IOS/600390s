//
//  ZSTECCLeftMenuCell.h
//  EComC
//
//  Created by 阮飞 on 15/8/8.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCLeftMenuCell : UITableViewCell

@property (nonatomic, strong) UILabel *category;

- (void)createUI;
- (void)setCellDataWithTypeName:(NSString *)typeNameString;

@end
