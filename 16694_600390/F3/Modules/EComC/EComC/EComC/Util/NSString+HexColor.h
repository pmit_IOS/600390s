//
//  HexColor.h
//  ShangHui
//
//  Created by qiuguian on 15/8/27.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIColor.h>

@interface NSString (HexColor)

-(UIColor *)colorValue;

@end
