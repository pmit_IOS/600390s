//
//  GetMyLocation.h
//  changeViewController
//
//  Created by pmit on 14/11/7.
//  Copyright (c) 2014年 wallace. All rights reserved.
//

#define SYSTEM_VERSION [[[UIDevice currentDevice] systemVersion]floatValue]

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface GetMyLocation : NSObject<CLLocationManagerDelegate>
@property(nonatomic,strong)CLLocationManager *clm;

+ (GetMyLocation *)defaultLocationManager;
+ (CLLocation *)getMyLocation;

@end
