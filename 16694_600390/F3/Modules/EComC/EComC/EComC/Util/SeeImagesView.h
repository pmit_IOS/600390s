//
//  SeeImagesView.h
//  F3
//
//  Created by anqiu on 15/8/31.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SeeImageObj.h"

@interface SeeImagesView : UIView
//是否开启 点击手势
@property (nonatomic,assign) BOOL isOpen;


//设置当前图片显示的 图片对象
- (void) setObj :(id) aa  ImageArray:(NSMutableArray *)array;

@end
