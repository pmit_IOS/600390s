//
//  ZSTECCShopInfoIntroduceTableViewCell.h
//  EComC
//
//  Created by anqiu on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCShopInfoIntroduceTableViewCell : UITableViewCell

@property (nonatomic,strong) UILabel *introduce;

-(void)createUI;

@end
