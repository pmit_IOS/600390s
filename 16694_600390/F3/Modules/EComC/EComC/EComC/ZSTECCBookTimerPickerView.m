//
//  ZSTECCBookTimerPickerView.m
//  EComC
//
//  Created by pmit on 15/9/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCBookTimerPickerView.h"
#import "ZSTECCAutoLabel.h"

@interface ZSTECCBookTimerPickerView() <UIPickerViewDataSource,UIPickerViewDelegate>

@property (strong,nonatomic) NSMutableArray *hourTimeArr;
@property (assign,nonatomic) BOOL hasChangeAnotherDay;
@property (assign,nonatomic) BOOL isChangeDay;
@property (assign,nonatomic) BOOL isChangeHour;

@end

@implementation ZSTECCBookTimerPickerView

- (void)createUIWithFrame:(CGRect)rect
{
    self.backgroundColor = RGBA(236, 236, 236, 1);
    self.i = 0;
    self.frame = rect;
    self.hasChangeAnotherDay = NO;
    UIView *whiteView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, rect.size.width, 80)];
    whiteView.backgroundColor = [UIColor whiteColor];
    
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(10, 0, 60, 40);
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    cancelBtn.tag = 100;
    [cancelBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancelBtn addTarget:self action:@selector(clickBtnCancel:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:cancelBtn];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(rect.size.width - 70, 0, 60, 30);
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    cancelBtn.tag = 101;
    [sureBtn addTarget:self action:@selector(clickBtnSure:) forControlEvents:UIControlEventTouchUpInside];
    [whiteView addSubview:sureBtn];
    
    //时间范围
    UILabel *timeLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 38, 80, 20)];
    timeLab.backgroundColor = [UIColor whiteColor];
    timeLab.text = [NSString stringWithFormat:@"时间范围:"];
    timeLab.font = [UIFont systemFontOfSize:14];
    [whiteView addSubview:timeLab];
    
    ZSTECCAutoLabel *rightLab = [[ZSTECCAutoLabel alloc] initWithFrame:CGRectMake(90, 40,WIDTH-90, 40)];
    rightLab.backgroundColor = [UIColor whiteColor];
    rightLab.lineBreakMode = UILineBreakModeCharacterWrap;
    rightLab.font = [UIFont systemFontOfSize:14];
    rightLab.numberOfLines = 2;
    NSMutableString *timeString = [[NSMutableString alloc] init];
    
    for (int i=0; i<self.timeArr.count; i++) {
        if (i == 0 ) {
            [timeString appendString:[NSString stringWithFormat:@"%@",self.timeArr[i]]];
        }else{
            [timeString appendString:[NSString stringWithFormat:@" %@",self.timeArr[i]]];
        }
        
    }
    rightLab.text = timeString;
    
    //    int height = [rightLab getAttributedStringHeightWidthValue:200];
    CGFloat height = [timeString boundingRectWithSize:CGSizeMake(WIDTH-120, MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14.0f]} context:nil].size.height;
    rightLab.frame = CGRectMake(90, 40,WIDTH-120, height);
    [whiteView addSubview:rightLab];
    
    [self addSubview:whiteView];
    
    self.hourTimeArr = [NSMutableArray array];
    for (NSString *timeString in self.timeArr)
    {
        NSArray *oneTimeStringArr = [timeString componentsSeparatedByString:@"-"];
        NSInteger headTime = [[oneTimeStringArr firstObject] integerValue];
        NSInteger lastTime = [[oneTimeStringArr lastObject] integerValue];
        for (NSInteger i = headTime; i <= lastTime ; i++)
        {
            [self.hourTimeArr addObject:@(i)];
        }
    }
    
    self.minusArr = @[@"00",@"15",@"30",@"45"];
    
    
    self.selectedYear = [self.nowYear integerValue];
    self.selectedMonth = [self.nowMonth integerValue];
    self.selectedDay = [self.nowDay integerValue];
    
    NSInteger lastHour = [[self.hourTimeArr lastObject] integerValue];
    
    NSString *lastTime = [self.timeArr lastObject];
    NSString *lastTimeString = [[lastTime componentsSeparatedByString:@"-"] lastObject];
    NSInteger lastMin = [[[lastTimeString componentsSeparatedByString:@":"] lastObject] integerValue];
    
    NSInteger tillHour = ([self.nowMin integerValue] + self.afterNowMinutes) / 60;
    
    
    
    if ([self.nowDay integerValue] == self.selectedDay)
    {
        if ([self.nowHour integerValue] + tillHour > lastHour || [self.nowHour integerValue] + tillHour >= 24)
        {
            self.isChangeDay = YES;
        }
        else if ([self.nowHour integerValue] + tillHour == lastHour)
        {
            if ([self.nowMin integerValue] + self.afterNowMinutes <= lastMin)
            {
                self.isChangeDay = NO;
            }
            else
            {
                self.isChangeDay = YES;
            }
        }
        else
        {
            self.isChangeDay = NO;
        }
    }
    else
    {
        self.isChangeDay = NO;
    }
    
    BOOL isInHour = NO;
    
    if ([self.nowHour integerValue] < [[self.hourTimeArr firstObject] integerValue] || [self.nowHour integerValue] > [[self.hourTimeArr lastObject] integerValue])
    {
        isInHour = NO;
        self.isChangeHour = YES;
    }
    else
    {
        for (id oneHour in self.hourTimeArr)
        {
            if ([oneHour integerValue] == [self.nowHour integerValue]) {
                isInHour = YES;
                break;
            }
        }
    }
    
    if (isInHour)
    {
        self.isChangeHour = NO;
    }
    else
    {
        self.isChangeHour = YES;
    }
    
    if ([self.nowMin integerValue] + self.afterNowMinutes / 60 > 1)
    {
        self.isChangeHour = YES;
    }
    else
    {
        if ([self.nowMin integerValue] + self.afterNowMinutes > 45)
        {
            self.isChangeHour = YES;
        }
        else
        {
            self.isChangeHour = NO;
        }
    }
    
    if (self.isChangeDay)
    {
        BOOL isBigMonth = NO;
        BOOL isTwo = NO;
        BOOL isRun = NO;
        
        if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
        {
            isRun = YES;
        }
        else
        {
            isRun = NO;
        }
        
        if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
        {
            isBigMonth = YES;
            isTwo = NO;
        }
        else if ([self.nowMonth isEqualToString:@"2"])
        {
            isBigMonth = NO;
            isTwo = YES;
        }
        else
        {
            isBigMonth = NO;
            isTwo = NO;
        }
        
        NSInteger showDay = [self.nowDay integerValue] + 1;
        NSInteger showMonth = [self.nowMonth integerValue];
        if (isBigMonth)
        {
            if (showDay > 31)
            {
                showMonth += 1;
                if (showMonth > 12)
                {
                    showMonth = 1;
                }
                showDay = showDay - 31;
            }
        }
        else if (!isBigMonth && !isTwo)
        {
            if (showDay > 30)
            {
                showMonth += 1;
                showDay = showDay - 30;
            }
        }
        else if (isRun)
        {
            if (showDay > 29)
            {
                showMonth += 1;
                showDay = showDay - 29;
            }
        }
        else
        {
            if (showDay > 28)
            {
                showMonth += 1;
                showDay = showDay - 28;
            }
        }
        
        self.selectedDay = showDay;
        self.selectedMonth = showMonth;
        self.selectedHour = [self.hourTimeArr[0] integerValue];
        self.selectedMinus = [self.minusArr[0] integerValue];
    }
    else
    {
        self.selectedDay = [self.nowDay integerValue];
        self.selectedMonth = [self.nowMonth integerValue];
        
        if (self.isChangeHour)
        {
            BOOL afterIsIn = NO;
            NSInteger showIndex = 0;
            for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
            {
                if ([self.nowHour integerValue] + tillHour == [self.hourTimeArr[i] integerValue])
                {
                    afterIsIn = YES;
                    showIndex = i;
                    break;
                }
            }
            
            if (afterIsIn)
            {
                self.selectedHour = [self.nowHour integerValue] + tillHour;
                NSInteger tillMin = [self.nowMin integerValue] + self.afterNowMinutes - tillHour * 60;
                if (tillMin <= 15)
                {
                    self.selectedMinus = 15;
                }
                else if (tillMin <= 30)
                {
                    self.selectedMinus = 30;
                }
                else
                {
                    self.selectedMinus = 45;
                }
            }
            else
            {
                NSInteger showIndex = 0;
                for (NSInteger i = 0; i < self.hourTimeArr.count - 1; i++)
                {
                    if ([self.nowHour integerValue] + tillHour > [self.hourTimeArr[i] integerValue] && [self.nowHour integerValue] + tillHour < [self.hourTimeArr[i + 1] integerValue])
                    {
                        showIndex = i + 1;
                        break;
                    }
                }
                
                self.selectedHour = [self.hourTimeArr[showIndex] integerValue];
                self.selectedMinus = 0;
            }
        }
        else
        {
            self.selectedHour = [self.nowHour integerValue];
            NSInteger mins = [self.nowMin integerValue] + self.afterNowMinutes;
            if (mins <= 15)
            {
                self.selectedMinus = 15;
            }
            else if (mins <= 30)
            {
                self.selectedMinus = 30;
            }
            else
            {
                self.selectedMinus = 45;
            }
        }
    }
    
    
    
    UIPickerView *pick = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 40, rect.size.width, rect.size.height - 40)];
    pick.delegate = self;
    pick.dataSource = self;
    [pick selectRow:0 inComponent:1 animated:YES];
    [pick selectRow:0 inComponent:2 animated:YES];
    [self addSubview:pick];
    
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    NSInteger tillHour = ([self.nowMin integerValue] + self.afterNowMinutes) / 60;
    
    if (component == 0)
    {
        return 7;
    }
    else if (component == 1)
    {
        NSInteger timeCount = 0;
        if (self.isChangeDay)
        {
            timeCount = self.hourTimeArr.count;
        }
        else
        {
            if ([self.nowDay integerValue] == self.selectedDay)
            {
                NSInteger showIndex = 0;
                BOOL afterIsIn = NO;
                for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
                {
                    if ([self.nowHour integerValue] + tillHour == [self.hourTimeArr[i] integerValue])
                    {
                        afterIsIn = YES;
                        showIndex = i;
                        break;
                    }
                }
                
                if (!afterIsIn)
                {
                    for (NSInteger i = 0; i < self.hourTimeArr.count - 1; i++)
                    {
                        if ([self.nowHour integerValue] + tillHour > [self.hourTimeArr[i] integerValue] && [self.nowHour integerValue] + tillHour < [self.hourTimeArr[i + 1] integerValue])
                        {
                            showIndex = i + 1;
                            break;
                        }
                    }
                }
                
                timeCount = self.hourTimeArr.count - showIndex - 1;
            }
            else
            {
                timeCount = self.hourTimeArr.count;
            }
        }
        
        return timeCount;
    }
    else
    {
        NSInteger tillMin = [self.nowMin integerValue] + self.afterNowMinutes - tillHour * 60;
        if (self.selectedHour > [self.nowHour integerValue] + tillHour)
        {
            return self.minusArr.count;
        }
        else
        {
            if ([self.nowDay integerValue] == self.selectedDay)
            {
                if (tillMin <= 15)
                {
                    return 3;
                }
                else if (tillMin <= 30)
                {
                    return 2;
                }
                else if (tillMin <= 45)
                {
                    return 1;
                }
                else
                {
                    return self.minusArr.count;
                }
            }
            else
            {
                return self.minusArr.count;
            }
            
        }
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    BOOL isBigMonth = NO;
    BOOL isTwo = NO;
    BOOL isRun = NO;
    
    if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
    {
        isRun = YES;
    }
    else
    {
        isRun = NO;
    }
    
    if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
    {
        isBigMonth = YES;
        isTwo = NO;
    }
    else if ([self.nowMonth isEqualToString:@"2"])
    {
        isBigMonth = NO;
        isTwo = YES;
    }
    else
    {
        isBigMonth = NO;
        isTwo = NO;
    }
    
    NSInteger tillHour = ([self.nowMin integerValue] + self.afterNowMinutes) / 60;
    
    if (component == 0)
    {
        if (self.isChangeDay)
        {
            NSInteger showDay = [self.nowDay integerValue] + 1 + row;
            NSInteger showMonth = [self.nowMonth integerValue];
            if (isBigMonth)
            {
                if (showDay > 31)
                {
                    showMonth += 1;
                    if (showMonth > 12)
                    {
                        showMonth = 1;
                    }
                    showDay = showDay - 31;
                }
            }
            else if (!isBigMonth && !isTwo)
            {
                if (showDay > 30)
                {
                    showMonth += 1;
                    showDay = showDay - 30;
                }
            }
            else if (isRun)
            {
                if (showDay > 29)
                {
                    showMonth += 1;
                    showDay = showDay - 29;
                }
            }
            else
            {
                if (showDay > 28)
                {
                    showMonth += 1;
                    showDay = showDay - 28;
                }
            }
            
            return [NSString stringWithFormat:@"%@月%@日",@(showMonth),@(showDay)];
        }
        else
        {
            NSInteger showDay = [self.nowDay integerValue] + row;
            NSInteger showMonth = [self.nowMonth integerValue];
            if (isBigMonth)
            {
                if (showDay > 31)
                {
                    showMonth += 1;
                    if (showMonth > 12)
                    {
                        showMonth = 1;
                    }
                    showDay = showDay - 31;
                }
            }
            else if (!isBigMonth && !isTwo)
            {
                if (showDay > 30)
                {
                    showMonth += 1;
                    showDay = showDay - 30;
                }
            }
            else if (isRun)
            {
                if (showDay > 29)
                {
                    showMonth += 1;
                    showDay = showDay - 29;
                }
            }
            else
            {
                if (showDay > 28)
                {
                    showMonth += 1;
                    showDay = showDay - 28;
                }
            }
            
            return [NSString stringWithFormat:@"%@月%@日",@(showMonth),@(showDay)];
        }
    }
    else if (component == 1)
    {
        if (self.isChangeDay)
        {
            return [NSString stringWithFormat:@"%@",self.hourTimeArr[row]];
        }
        else
        {
            if ([self.nowDay integerValue] == self.selectedDay)
            {
                NSInteger showIndex = 0;
                BOOL afterIsIn = NO;
                for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
                {
                    if ([self.nowHour integerValue] + tillHour == [self.hourTimeArr[i] integerValue])
                    {
                        afterIsIn = YES;
                        showIndex = i;
                        break;
                    }
                }
                
                if (!afterIsIn)
                {
                    for (NSInteger i = 0; i < self.hourTimeArr.count - 1; i++)
                    {
                        if ([self.nowHour integerValue] + tillHour > [self.hourTimeArr[i] integerValue] && [self.nowHour integerValue] + tillHour < [self.hourTimeArr[i + 1] integerValue])
                        {
                            showIndex = i + 1;
                            break;
                        }
                    }
                }

                return [NSString stringWithFormat:@"%@",self.hourTimeArr[row + showIndex]];
                
            }
            else
            {
                return [NSString stringWithFormat:@"%@",self.hourTimeArr[row]];
            }
        }
    }
    else
    {
        if (self.isChangeDay)
        {
            return [NSString stringWithFormat:@"%@",self.minusArr[row]];
        }
        else
        {
            if ([self.nowDay integerValue] == self.selectedDay)
            {
                NSInteger tillMin = [self.nowMin integerValue] + self.afterNowMinutes - tillHour * 60;
                if (self.selectedHour > [self.nowHour integerValue] + tillHour)
                {
                    return [NSString stringWithFormat:@"%@",self.minusArr[row]];
                }
                else
                {
                    if (tillMin <= 15)
                    {
                        if (row == 0)
                        {
                            return @"15";
                        }
                        else if (row == 1)
                        {
                            return @"30";
                        }
                        else
                        {
                            return @"45";
                        }
                    }
                    else if (tillMin <= 30)
                    {
                        if (row == 0)
                        {
                            return @"30";
                        }
                        else
                        {
                            return @"45";
                        }
                    }
                    else
                    {
                        return @"45";
                    }
                }
            }
            else
            {
                return [NSString stringWithFormat:@"%@",self.minusArr[row]];
            }
            
        }
            
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSInteger tillHour = ([self.nowMin integerValue] + self.afterNowMinutes) / 60;
    
    if (component == 0)
    {
        BOOL isBigMonth = NO;
        BOOL isTwo = NO;
        BOOL isRun = NO;
        
        if ([self.nowYear integerValue] % 4 == 0 || ([self.nowYear integerValue] % 100 == 0 && [self.nowYear integerValue] % 400 == 0))
        {
            isRun = YES;
        }
        else
        {
            isRun = NO;
        }
        
        if ([self.nowMonth isEqualToString:@"1"] | [self.nowMonth isEqualToString:@"3"] | [self.nowMonth isEqualToString:@"5"] | [self.nowMonth isEqualToString:@"7"] | [self.nowMonth isEqualToString:@"8"] | [self.nowMonth isEqualToString:@"10"] | [self.nowMonth isEqualToString:@"12"])
        {
            isBigMonth = YES;
            isTwo = NO;
        }
        else if ([self.nowMonth isEqualToString:@"2"])
        {
            isBigMonth = NO;
            isTwo = YES;
        }
        else
        {
            isBigMonth = NO;
            isTwo = NO;
        }
        
        if (self.isChangeDay)
        {
            NSInteger showDay = [self.nowDay integerValue] + row + 1;
            NSInteger showMonth = [self.nowMonth integerValue];
            if (isBigMonth)
            {
                if (showDay > 31)
                {
                    showMonth += 1;
                    if (showMonth > 12)
                    {
                        showMonth = 1;
                    }
                    showDay = showDay - 31;
                }
            }
            else if (!isBigMonth && !isTwo)
            {
                if (showDay > 30)
                {
                    showMonth += 1;
                    showDay = showDay - 30;
                }
            }
            else if (isRun)
            {
                if (showDay > 29)
                {
                    showMonth += 1;
                    showDay = showDay - 29;
                }
            }
            else
            {
                if (showDay > 28)
                {
                    showMonth += 1;
                    showDay = showDay - 28;
                }
            }
            
            if (showMonth < [self.nowMonth integerValue])
            {
                self.selectedYear = [self.nowYear integerValue] + 1;
                
            }
            else
            {
                self.selectedYear = [self.nowYear integerValue];
            }
            
            self.selectedMonth = showMonth;
            self.selectedDay = showDay;
        }
        else
        {
            NSInteger showDay = [self.nowDay integerValue] + row;
            NSInteger showMonth = [self.nowMonth integerValue];
            if (isBigMonth)
            {
                if (showDay > 31)
                {
                    showMonth += 1;
                    if (showMonth > 12)
                    {
                        showMonth = 1;
                    }
                    showDay = showDay - 31;
                }
            }
            else if (!isBigMonth && !isTwo)
            {
                if (showDay > 30)
                {
                    showMonth += 1;
                    showDay = showDay - 30;
                }
            }
            else if (isRun)
            {
                if (showDay > 29)
                {
                    showMonth += 1;
                    showDay = showDay - 29;
                }
            }
            else
            {
                if (showDay > 28)
                {
                    showMonth += 1;
                    showDay = showDay - 28;
                }
            }
            
            if (showMonth < [self.nowMonth integerValue])
            {
                self.selectedYear = [self.nowYear integerValue] + 1;
                
            }
            else
            {
                self.selectedYear = [self.nowYear integerValue];
            }
            
            self.selectedMonth = showMonth;
            self.selectedDay = showDay;
            
            if (self.isChangeDay)
            {
                self.selectedHour = [self.hourTimeArr[0] integerValue];
            }
            else
            {
                if (self.selectedDay != [self.nowDay integerValue])
                {
                    self.selectedHour = [self.hourTimeArr[0] integerValue];
                }
                else
                {
                    NSString *firstTimeString = [self.timeArr firstObject];
                    NSInteger firstTimeHour = [[[[[firstTimeString componentsSeparatedByString:@"-"] firstObject] componentsSeparatedByString:@":"] firstObject] integerValue];
                    
                    if ([self.nowDay integerValue] == self.selectedDay)
                    {
                        if ([self.nowHour integerValue] < firstTimeHour)
                        {
                            self.selectedHour = [self.hourTimeArr[0] integerValue];
                        }
                        
                            NSInteger showIndex = 0;
                            BOOL afterIsIn = NO;
                            for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
                            {
                                if ([self.nowHour integerValue] + tillHour == [self.hourTimeArr[i] integerValue])
                                {
                                    afterIsIn = YES;
                                    showIndex = i;
                                    break;
                                }
                            }
                            
                            if (!afterIsIn)
                            {
                                for (NSInteger i = 0; i < self.hourTimeArr.count - 1; i++)
                                {
                                    if ([self.nowHour integerValue] + tillHour > [self.hourTimeArr[i] integerValue] && [self.nowHour integerValue] + tillHour < [self.hourTimeArr[i + 1] integerValue])
                                    {
                                        showIndex = i + 1;
                                        break;
                                    }
                                }
                            }
                        
                        self.selectedHour = [self.hourTimeArr[showIndex + row] integerValue];
                        
                    }
                    else
                    {
                        self.selectedHour = [self.hourTimeArr[0] integerValue];
                    }
                }
            }
            
        }
        
        if (self.isChangeDay)
        {
            self.selectedMinus = [self.minusArr[0] integerValue];
        }
        else
        {
            if (self.selectedDay == [self.nowDay integerValue])
            {
                if (self.selectedHour > [self.nowHour integerValue] + tillHour)
                {
                    self.selectedMinus = 0;
                }
                else
                {
                    NSInteger tillMin = [self.nowMin integerValue] + self.afterNowMinutes - tillHour * 60;
                    if (tillMin <= 15)
                    {
                        self.selectedMinus = 15;
                    }
                    else if (tillMin <= 30)
                    {
                        self.selectedMinus = 30;
                    }
                    else
                    {
                        self.selectedMinus = 45;
                    }
                }
            }
            else
            {
                self.selectedMinus = 0;
            }
        }
        
        [pickerView reloadComponent:1];
        [pickerView reloadComponent:2];
        
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    else if (component == 1)
    {
        if (self.isChangeDay)
        {
            self.selectedHour = [self.hourTimeArr[0] integerValue];
        }
        else
        {
            if (self.selectedDay != [self.nowDay integerValue])
            {
                self.selectedHour = [self.hourTimeArr[0] integerValue];
            }
            else
            {
                NSString *firstTimeString = [self.timeArr firstObject];
                NSInteger firstTimeHour = [[[[[firstTimeString componentsSeparatedByString:@"-"] firstObject] componentsSeparatedByString:@":"] firstObject] integerValue];
                
                if ([self.nowDay integerValue] == self.selectedDay)
                {
                    if ([self.nowHour integerValue] < firstTimeHour)
                    {
                        self.selectedHour = [self.hourTimeArr[0] integerValue];
                    }
                    
                    NSInteger showIndex = 0;
                    BOOL afterIsIn = NO;
                    for (NSInteger i = 0; i < self.hourTimeArr.count; i++)
                    {
                        if ([self.nowHour integerValue] + tillHour == [self.hourTimeArr[i] integerValue])
                        {
                            afterIsIn = YES;
                            showIndex = i;
                            break;
                        }
                    }
                    
                    if (!afterIsIn)
                    {
                        for (NSInteger i = 0; i < self.hourTimeArr.count - 1; i++)
                        {
                            if ([self.nowHour integerValue] + tillHour > [self.hourTimeArr[i] integerValue] && [self.nowHour integerValue] + tillHour < [self.hourTimeArr[i + 1] integerValue])
                            {
                                showIndex = i + 1;
                                break;
                            }
                        }
                    }
                    
                    self.selectedHour = [self.hourTimeArr[showIndex + row] integerValue];
                    
                }
                else
                {
                    self.selectedHour = [self.hourTimeArr[0] integerValue];
                }
            }
        }
        
        if (self.isChangeDay)
        {
            self.selectedMinus = [self.minusArr[0] integerValue];
        }
        else
        {
            if (self.selectedDay == [self.nowDay integerValue])
            {
                if (self.selectedHour > [self.nowHour integerValue] + tillHour)
                {
                    self.selectedMinus = 0;
                }
                else
                {
                    NSInteger tillMin = [self.nowMin integerValue] + self.afterNowMinutes - tillHour * 60;
                    if (tillMin <= 15)
                    {
                        self.selectedMinus = 15;
                    }
                    else if (tillMin <= 30)
                    {
                        self.selectedMinus = 30;
                    }
                    else
                    {
                        self.selectedMinus = 45;
                    }
                }
            }
            else
            {
                self.selectedMinus = 0;
            }
        }
        
        [pickerView reloadComponent:2];
        [pickerView selectRow:0 inComponent:2 animated:YES];
    }
    else
    {
        if (self.isChangeDay)
        {
            self.selectedMinus = [self.minusArr[row] integerValue];
        }
        else
        {
            if (self.selectedHour > [self.nowHour integerValue] + tillHour)
            {
                self.selectedMinus = [self.minusArr[row] integerValue];
            }
            else
            {
                NSInteger tillMin = [self.nowMin integerValue] + self.afterNowMinutes - tillHour * 60;
                if (tillMin <= 15)
                {
                    if (row == 0)
                    {
                        self.selectedMinus = 15;
                    }
                    else if (row == 1)
                    {
                        self.selectedMinus = 30;
                    }
                    else
                    {
                        self.selectedMinus = 45;
                    }
                }
                else if (tillMin <= 30)
                {
                    if (row == 0)
                    {
                        self.selectedMinus = 30;
                    }
                    else
                    {
                        self.selectedMinus = 45;
                    }
                }
                else
                {
                    self.selectedMinus = 45;
                }
            }
        }
    }
}

- (void)clickBtnSure:(UIButton *)sender
{
    NSString *month = @"";
    if (self.selectedMonth >= 10) {
        month = [NSString stringWithFormat:@"%ld",self.selectedMonth];
    }else{
        month = [NSString stringWithFormat:@"0%ld",self.selectedMonth];
    }
    
    NSString *day = @"";
    if (self.selectedDay >= 10) {
        day = [NSString stringWithFormat:@"%ld",self.selectedDay];
    }else{
        day = [NSString stringWithFormat:@"0%ld",self.selectedDay];
    }
    
    NSString *hour = @"";
    if (self.selectedHour >= 10) {
        hour = [NSString stringWithFormat:@"%ld",self.selectedHour];
    }else{
        hour = [NSString stringWithFormat:@"0%ld",self.selectedHour];
    }
    
    NSString *mini = @"";
    if (self.selectedMinus >= 10) {
        mini = [NSString stringWithFormat:@"%ld",self.selectedMinus];
    }else{
        mini = [NSString stringWithFormat:@"0%ld",self.selectedMinus];
    }

    
    if ([self.timeDelegate respondsToSelector:@selector(sureTimeSelected:)])
    {
        [self.timeDelegate sureTimeSelected:[NSString stringWithFormat:@"%@-%@-%@ %@:%@",@(self.selectedYear),month,day,hour,mini]];
    }
    
}


- (void)clickBtnCancel:(UIButton *)sender
{
    if ([self.timeDelegate respondsToSelector:@selector(cancelTimeSelected)])
    {
        [self.timeDelegate cancelTimeSelected];
    }
}

@end
