//
//  ZSTECCAddressCell.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCAddressCell : UITableViewCell

@property (strong,nonatomic) UILabel *shopNameLB;
@property (strong,nonatomic) UILabel *shopAddressLB;

- (void)createUI;
- (void)setCellDataWithShopName:(NSString *)shopName ShopAddress:(NSString *)shopAddress;

@end
