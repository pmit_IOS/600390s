//
//  ZSTECCShopReviewsCell.m
//  EComC
//
//  Created by 阮飞 on 15/8/8.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopReviewsCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import "UIImageView+WebCache.h"
#import "SeeImageObj.h"
#import "SeeImagesView.h"

@implementation ZSTECCShopReviewsCell{
    
    UIView *headView;
    UIView *contentView;
    
    ZSTECCRoundRectView * _line;
    
    UIImageView *star1;
    UIImageView *star2;
    UIImageView *star3;
    UIImageView *star4;
    UIImageView *star5;
    
    UILabel *lableCon;
    
    UILabel *bottomSpace;
    
    SeeImagesView *imageBtn1;
    SeeImagesView *imageBtn2;
    SeeImagesView *imageBtn3;
    SeeImagesView *imageBtn4;
    
    SeeImageObj *obj1;
    SeeImageObj *obj2;
    SeeImageObj *obj3;
    SeeImageObj *obj4;
    
    NSMutableArray *imageArr;
    
}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        self.contentView.backgroundColor =[UIColor whiteColor];
        
            headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)];
            headView.backgroundColor = [UIColor whiteColor];
            [self.contentView addSubview:headView];
            
            //_line = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(0, 0) toPoint:CGPointMake(320, 40) borderWidth:1.0f borderColor:[UIColor lightGrayColor]];
            _line.alpha = 0.5;
            //[headView addSubview:_line];
            
            self.headImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 5, 50, 50)];
            self.headImage.layer.masksToBounds = YES;
            self.headImage.layer.cornerRadius = 25;
            self.headImage.contentMode = UIViewContentModeScaleAspectFit;
            [headView addSubview:self.headImage];
            
            self.name = [[UILabel alloc] initWithFrame:CGRectMake(50+15, 5, 120, 20)];
            self.name.font = [UIFont systemFontOfSize:12];
            self.name.textColor = [UIColor grayColor];
            [headView addSubview:self.name];
            
            self.time = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-130, 5, 120, 20)];
            self.time.textColor = [UIColor grayColor];
            self.time.font = [UIFont systemFontOfSize:12];
            self.time.textAlignment = UITextAlignmentRight;
            [headView addSubview:self.time];
            
            
            UIView *starView = [[UIView alloc] initWithFrame:CGRectMake(65, 25, 250, 25)];
            [headView addSubview:starView];
            
            star1 = [[UIImageView alloc] initWithFrame:CGRectMake(0, 10, 15, 15)];
            [star1 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
            [starView addSubview:star1];
            
            star2 = [[UIImageView alloc] initWithFrame:CGRectMake(15+2, 10, 15, 15)];
            [star2 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
            [starView addSubview:star2];
            
            star3 = [[UIImageView alloc] initWithFrame:CGRectMake(15*2+4, 10, 15, 15)];
            [star3 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
            [starView addSubview:star3];
            
            star4 = [[UIImageView alloc] initWithFrame:CGRectMake(15*3+6, 10, 15, 15)];
            [star4 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
            [starView addSubview:star4];
            
            star5 = [[UIImageView alloc] initWithFrame:CGRectMake(15*4+8, 10, 15, 15)];
            [star5 setImage:[UIImage imageNamed:@"model_ecomc_starGray.png"]];
            [starView addSubview:star5];
            
            
            _line = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(10, 60) toPoint:CGPointMake(300, 40) borderWidth:1.0f borderColor:[UIColor lightGrayColor]];
            _line.alpha = 0.5;
            //[headView addSubview:_line];
            
            
            contentView = [[UIView alloc] initWithFrame:CGRectMake(0, 60, 320, 135)];
            contentView.backgroundColor = [UIColor whiteColor];
            [self.contentView addSubview:contentView];
            
            self.imageBtn = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10+70, 50, 50)];
            //    [contentView addSubview:self.foodImages];
            
            
            self.reviewContent = [[UILabel alloc] init];
            self.reviewContent.frame = CGRectMake(10, 0, 300, self.labelHeight);
            self.reviewContent.textColor = [UIColor grayColor];
            self.reviewContent.font = [UIFont systemFontOfSize:14];
            [contentView addSubview:self.reviewContent];
            //contentView.frame = CGRectMake(0, 65, 320, 5+self.labelHeight+60);
            
            bottomSpace = [[UILabel alloc] initWithFrame:CGRectMake(0, 70+135, 320, 10)];
            bottomSpace.backgroundColor = RGBA(206, 206, 206, 1);
            bottomSpace.alpha = 0.5;
            [self.contentView addSubview:bottomSpace];
            
            
            imageBtn1 = [[SeeImagesView alloc] init];
            imageBtn1.tag = 101;
            [contentView addSubview:imageBtn1];
            
            imageBtn2 = [[SeeImagesView alloc] init];
            imageBtn2.tag = 102;
            [contentView addSubview:imageBtn2];
            
            imageBtn3 = [[SeeImagesView alloc] init];
            imageBtn3.tag = 103;
            [contentView addSubview:imageBtn3];
            
            imageBtn4 = [[SeeImagesView alloc] init];
            imageBtn4.tag = 104;
            [contentView addSubview:imageBtn4];
            
            obj1 = [[SeeImageObj alloc]init];
            obj2 = [[SeeImageObj alloc]init];
            obj3 = [[SeeImageObj alloc]init];
            obj4 = [[SeeImageObj alloc]init];
    }
    
    return self;
}


-(void)setCellData:(NSDictionary *)dic{
    
    [imageBtn1 removeAllSubviews];
    [imageBtn2 removeAllSubviews];
    [imageBtn3 removeAllSubviews];
    [imageBtn4 removeAllSubviews];
    
    NSString *headUrl = [NSString stringWithFormat:@"%@", [dic safeObjectForKey:@"portraitUrl"]];
    
    if ([headUrl isEqualToString:@""] || [headUrl isKindOfClass:[NSNull class]] ) {
        
        [self.headImage setImage:[UIImage imageNamed:@"model_ecomc_review_defualtImage.png"]];
    }else{
        [self.headImage setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"portraitUrl"]]]];
    }
    
    self.name.text = [dic safeObjectForKey:@"userName"];
    
    if (self.name.text.length == 11) {
        self.name.text = [NSString stringWithFormat:@"%@***%@",[self.name.text substringToIndex:2],[self.name.text substringFromIndex:8]];
    }
    
    self.time.text = [dic safeObjectForKey:@"createdOn"];
    [self.levelImages setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"levelImages"]]]];
    
    _imageArray = [dic safeObjectForKey:@"images"];
    
    self.labelHeight = 0;
    if ([dic safeObjectForKey:@"evaluateDesc"]) {
        
        self.reviewContent.lineBreakMode = NSLineBreakByWordWrapping;
        self.reviewContent.numberOfLines = 0;
        self.reviewContent.text = [dic safeObjectForKey:@"evaluateDesc"];
        CGSize reviewLabSize = [self.reviewContent.text sizeWithFont:self.reviewContent.font constrainedToSize:CGSizeMake(320.f, MAXFLOAT)  lineBreakMode:self.reviewContent.lineBreakMode];
        self.labelHeight = reviewLabSize.height;
        if (self.labelHeight <50) {
            self.labelHeight = 30;
        }
        
        BOOL hasImage = NO;
        if (_imageArray.count == 0) {
            hasImage = NO;
        }else{
            hasImage = YES;
        }
        
        [self height:self.labelHeight andImageNum:hasImage];
    }
    
    self.reviewContent.frame = CGRectMake(10, -10, 320, self.labelHeight);
    
    
    obj1.name = @"";
    
    //无图片
    if (_imageArray.count < 1) {
        contentView.frame = CGRectMake(0, 65, 320, 15+self.labelHeight);
        bottomSpace.frame = CGRectMake(0, 70+self.labelHeight, 320, 10);
    }else{
        contentView.frame = CGRectMake(0, 65, 320, 15+self.labelHeight+55);
        bottomSpace.frame = CGRectMake(0, 70+self.labelHeight+55, 320, 10);
        
        for (int i=0; i<_imageArray.count; i++) {
            if (i==0) {
                obj1.name  = [NSString stringWithFormat:@"%@",[_imageArray[0] safeObjectForKey:@"url"]];
            }else if(i==1){
                obj2.name  = [NSString stringWithFormat:@"%@",[_imageArray[1] safeObjectForKey:@"url"]];
            }else if(i==2){
                obj3.name  = [NSString stringWithFormat:@"%@",[_imageArray[2] safeObjectForKey:@"url"]];
            }else if(i==3){
                obj4.name  = [NSString stringWithFormat:@"%@",[_imageArray[3] safeObjectForKey:@"url"]];
            }
        }
        
        [imageArr removeAllObjects];
        if (_imageArray.count == 1) {
            imageArr = [NSMutableArray arrayWithObjects:obj1,nil];
        }else if(_imageArray.count == 2){
            imageArr = [NSMutableArray arrayWithObjects:obj1,obj2,nil];
        }else if(_imageArray.count == 3){
            imageArr = [NSMutableArray arrayWithObjects:obj1,obj2,obj3,nil];
        }else if(_imageArray.count == 4){
            imageArr = [NSMutableArray arrayWithObjects:obj1,obj2,obj3,obj4,nil];
        }
        
        for (int i=0; i<_imageArray.count; i++) {
            
            NSString *imageUrl = [NSString stringWithFormat:@"%@",[_imageArray[i] safeObjectForKey:@"url"]];
            
            [self initFoodImages:i height:self.labelHeight imageUrl:imageUrl];
        }

        
        //NSMutableArray *arr = [NSMutableArray arrayWithObjects:obj1,nil];
    }
    

    int level =[[dic safeObjectForKey:@"evaluateStar"] intValue];
    
    if (level == 1) {
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 2){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 3){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star3 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 4){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star3 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star4 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }else if(level == 5){
        [star1 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star2 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star3 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star4 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
        [star5 setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    }
}


-(void)initFoodImages:(int)num height:(float)height imageUrl:(NSString *)imageUrl{
    
    
    if ( num<=4 ) {
        if (num == 0) {
            obj1.whidth = @"320";
            obj1.height = @"400";
            obj1.imgTitle= @"";
            obj1.imgContent = self.reviewContent.text;
            imageBtn1.frame = CGRectMake(10+60*num, self.labelHeight-10, 50, 50);
            [imageBtn1 setObj:obj1 ImageArray:imageArr];
            imageBtn1.isOpen = YES;
        }
        if (num == 1) {
            obj2.whidth = @"320";
            obj2.height = @"400";
            obj2.imgTitle= @"";
            obj2.imgContent = self.reviewContent.text;
            imageBtn2.frame = CGRectMake(10+60*num, self.labelHeight-10, 50, 50);
            [imageBtn2 setObj:obj2 ImageArray:imageArr];
            imageBtn2.isOpen = YES;
        }
        if (num == 2) {
            obj3.whidth = @"320";
            obj3.height = @"400";
            obj3.imgTitle= @"";
            obj3.imgContent = self.reviewContent.text;
            imageBtn3.frame = CGRectMake(10+60*num, self.labelHeight-10, 50, 50);
            [imageBtn3 setObj:obj3 ImageArray:imageArr];
            imageBtn3.isOpen = YES;
        }
        if (num == 3) {
            obj4.whidth = @"320";
            obj4.height = @"400";
            obj4.imgTitle= @"";
            obj4.imgContent = self.reviewContent.text;
            imageBtn4.frame = CGRectMake(10+60*num, self.labelHeight-10, 50, 50);
            [imageBtn4 setObj:obj4 ImageArray:imageArr];
            imageBtn4.isOpen = YES;
        }
    }
    
}

-(void)clickImage:(UIImageView *)btn{
    
    //[_delegate clickImage:btn imageArray:_imageArray];
}

-(void)height:(NSInteger)height andImageNum:(BOOL)image{
    
    [_delegate passHeight:height andImageNum:image];
}

@end
