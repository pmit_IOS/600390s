//
//  ZSTECCOrderProductCell.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCOrderProductCell : UITableViewCell

@property (strong,nonatomic) UILabel *orderFoodNameLB;
@property (strong,nonatomic) UILabel *orderFoodCountLB;
@property (strong,nonatomic) UILabel *orderFoodPriceLB;

- (void)createUI;
- (void)setCellDataWithFoodName:(NSString *)foodName FoodCount:(NSString *)foodCount FoodPrice:(NSString *)foodPrice;

@end
