//
//  ZSTECCSearchResultDelegate.h
//  EComC
//
//  Created by pmit on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol  ZSTECCCSearchDelegate <NSObject>

- (void)sureString:(NSString *)searchKey;
- (void)cleanHistory;

@end

@interface ZSTECCSearchResultDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *resultArr;
@property (weak,nonatomic) id<ZSTECCCSearchDelegate> searchHistoryDelegate;

@end
