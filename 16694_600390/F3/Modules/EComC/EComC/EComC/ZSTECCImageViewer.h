//
//  ZSTECCImageViewer.h
//  EComC
//
//  Created by anqiu on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCImageViewer : UIViewController<UIScrollViewDelegate>

@property (strong, nonatomic)  UIScrollView *sv;
@property (strong, nonatomic)  UIImageView *iv;
@property (strong, nonatomic)  UIView *loadingView;

- (void)loadImage:(NSString *)imageURL;

@end
