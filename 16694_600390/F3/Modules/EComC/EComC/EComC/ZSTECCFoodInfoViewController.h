//
//  ZSTECCFoodInfoViewController.h
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"

@interface ZSTECCFoodInfoViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTF3EngineECCDelegate>


@property (nonatomic, strong) UITableView *leftMenuTableView;
@property (nonatomic, strong) UITableView *rightFoodTableView;
@property (copy,nonatomic) NSString *selectedSeatTypeString;
@property (copy,nonatomic) NSString *selectedSexTypeString;
@property (copy,nonatomic) NSString *shopId;
@property (copy,nonatomic) NSString *shopAddress;
@property (copy,nonatomic) NSString *shopName;
@property (copy,nonatomic) NSString *bookTimeString;
@property (assign,nonatomic) NSInteger bookCount;
@property (copy,nonatomic) NSString *bookManNameString;
@property (copy,nonatomic) NSString *bookPhoneString;
@property (strong,nonatomic) ZSTF3Engine *engine;
@property (strong,nonatomic) NSMutableArray *selectedDishesArr;
@property (copy,nonatomic) NSString *backUpString;

- (void)modifyFoodCount:(NSDictionary *)foodDic IsHas:(BOOL)isHas IsPlus:(BOOL)isPlus;
-(void)createAnimation:(NSInteger)hieght;

@end
