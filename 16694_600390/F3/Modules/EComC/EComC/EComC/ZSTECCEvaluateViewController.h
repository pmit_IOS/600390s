//
//  ZSTECCEvaluateViewController.h
//  EComC
//
//  Created by qiuguian on 8/11/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"

@interface ZSTECCEvaluateViewController : UIViewController<UITextViewDelegate,ZSTF3EngineECCDelegate>

@property (nonatomic, strong) ZSTF3Engine *engine;
@property (nonatomic, strong) NSString *shopId;
@property (nonatomic, strong) NSString *orderId;

@end
