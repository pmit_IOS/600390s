//
//  ZSTECCOrderInfoCell.h
//  EComC
//
//  Created by qiuguian on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCOrderInfoCell : UITableViewCell

@property (nonatomic,strong) UILabel *codeLabel;
@property (nonatomic,strong) UILabel *payStatus;
@property (nonatomic,strong) UILabel *orderTime;
@property (nonatomic, strong) UILabel *bookTime;
@property (nonatomic, strong) UILabel *bookType;
@property (nonatomic, strong) UILabel *bookPersonNum;
@property (nonatomic, strong) UILabel *deskType;
@property (nonatomic, strong) UILabel *orderMoney;
@property (nonatomic,strong) UILabel *orderPriceTitle;
@property (nonatomic,strong) UIView *bg;


-(void)createUI;
-(void)setCellData:(NSMutableDictionary *)dic;

@end
