//
//  ZSTECCBookSeatViewController.h
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"
#import "ZSTECCTimePickerView.h"
#import "ZSTPeopleNumPickerView.h"
#import "ZSTECCMessageCell.h"
#import "ZSTECCBookTimerPickerView.h"

@class ZSTECCReservationViewComtroller;

@interface ZSTECCBookSeatViewController : UIViewController <ZSTF3EngineDelegate,ZSTF3EngineECCDelegate,ZSTPeopleNumPickerViewDelegate,ZSTECCBookTimerPickerViewDelegate,ZSTECCMessageCellDelegate>

@property (strong,nonatomic) UITableView *seatMessageTableView;
//@property (strong,nonatomic) ZSTECCTimePickerView *pickView;
@property (strong,nonatomic) ZSTECCBookTimerPickerView *pickView;
@property (copy,nonatomic) NSString *selectedSeatTypeString;
@property (copy,nonatomic) NSString *selectedSexTypeString;
@property (strong,nonatomic) ZSTF3Engine *engine;
@property (copy,nonatomic) NSString *shopId;
@property (copy,nonatomic) NSString *shopAddress;
@property (copy,nonatomic) NSString *shopName;
@property (strong,nonatomic) ZSTPeopleNumPickerView *peoplePickerView;
@property (strong,nonatomic) ZSTECCReservationViewComtroller *fatherVC;

@property (copy,nonatomic) NSString *bookTimeString;
@property (assign,nonatomic) NSInteger bookCount;
@property (copy,nonatomic) NSString *bookManNameString;
@property (copy,nonatomic) NSString *bookPhoneString;
@property (copy,nonatomic) NSString *backUpString;
@property (assign,nonatomic) BOOL isNumKey;
@property (strong,nonatomic) UIButton *doneBtn;

@end
