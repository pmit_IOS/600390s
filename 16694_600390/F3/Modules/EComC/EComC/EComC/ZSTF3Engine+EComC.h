//
//  ZSTF3Engine+EComC.h
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTF3Engine.h"

@protocol ZSTF3EngineECCDelegate <ZSTF3EngineDelegate>

@optional

#pragma mark - ---------------protocol----------------
#pragma mark 获取主页轮播列表
-(void)getEcomCCarouselDidSucceed:(NSDictionary *)carouseles;
-(void)getEcomCCarouselDidFailed:(int)resultCode;

#pragma mark 返回个人订单列表信息
-(void)getEcomCOrderListDidSucceed:(NSDictionary *)orderList;
-(void)getEcomCOrderListDidFailed:(int)resultCode;

#pragma mark 根据经纬度获取当前位置
-(void)getShopAddressInfoDidSucceed:(NSDictionary *)addressInfo;
-(void)getShopAddressInfoDidFailed:(int)resultCode;

#pragma mark 获取店铺列表
-(void)getShopListDidSucceed:(NSDictionary *)addressInfo;
-(void)getShopListDidFailed:(int)resultCode;

- (void)getInitOrderInfoSuccessd:(NSDictionary *)response;
- (void)getInitOrderInfoFailed:(int)resultCode;

- (void)getDishesInfoSuccessed:(NSDictionary *)response;
- (void)getDishesInfoFailed:(int)resultCode;

#pragma mark - 获取店铺信息
-(void)getShopDetailDidSucceed:(NSDictionary *)shopDetail;
-(void)getShopDetailDidFailed:(int)resultCode;

- (void)saveSeatDidSuccess:(NSDictionary *)response;
- (void)saveSeatDidFailed:(NSDictionary *)failResponse;

- (void)saveMealDidSuccess:(NSDictionary *)response;
- (void)saveMealDidFailed:(NSDictionary *)failResponse;

- (void)requestMemberCardDiscountAndBalanceSucceed:(NSDictionary *)response;
- (void)requestMemberCardDiscountAndBalanceFaild:(NSDictionary *)response;

#pragma mark - 获取店铺信息 用户评价列表
-(void)getShopReviewListDidSucceed:(NSDictionary *)shopReview;
-(void)getShopReviewListDidFailed:(int)resultCode;

#pragma mark - 订单详情
-(void)getOrderInfoDidSucceed:(NSDictionary *)orderInfo;
-(void)getOrderInfoDidFailed:(int)resultCode;

#pragma mark - 发表评论
-(void)sendEvaluateDidSucceed:(NSDictionary *)evaluateInfo;
-(void)sendEvaluateDidFailed:(int)resultCode;

#pragma mark - 获取订单明细
-(void)getListOrderDetailDidSucceed:(NSDictionary *)ListDetial;
-(void)getListOrderDetailFailed:(int)resultCode;

#pragma mark - 取消订单
-(void)cancelOrderDidSucceed:(NSDictionary *)cancelInfo;
-(void)cancelOrderDidFailed:(int)resultCode;

- (void)getMapSuccess:(NSDictionary *)response;
- (void)getMapFailed:(int)resultCode;

#pragma mark - 查询数据资源是否有更新
-(void)checkUpdateDidSucceed:(NSDictionary *)responseInfo;
-(void)checkUpdateDidFailed:(NSDictionary *)failedInfo;

#pragma mark - 定座点餐按钮之前验证接口
-(void)validateReserveMealDidSucceed:(NSDictionary *)responseInfo;
-(void)validateReserveMealDidFailed:(NSDictionary *)failedInfo;

#pragma mark - 去付款按钮之前验证接口
-(void) validateGotoPayDidSucceed:(NSDictionary *)responseInfo;
-(void) validateGotoPayDidFailed:(NSDictionary *)failedInfo;



@end




@interface ZSTF3Engine (EComC)

#pragma mark - ---------------method----------------
- (void)getEcomCCarousel;

#pragma mark 获取个人订单列表信息
-(void)getEcomCOrderListByUserId:(NSString *)userId andStatus:(NSString *)orderStatus curPage:(NSString *)curPage pageSize:(NSString *)pageSize;

#pragma mark 根据经纬度获取当前位置
-(void)getShopAddressInfoByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng;

#pragma mark 获取店铺列表
-(void)getShopListByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng andCurrentPage:(NSInteger)currentPage PageSize:(NSInteger)pageSize;

#pragma mark - 初始化订单
- (void)getInitShopInfoByShopId:(NSString *)shopId;

#pragma mark - 初始化菜单
- (void)getShopDishesByShopId:(NSString *)shopId AndUserId:(NSString *)userId;

#pragma mark - 获取店铺信息
-(void)getShopDetailByShopId:(NSString *)shopId;

#pragma mark - 获取店铺信息 用户评价列表
-(void)getShopReviewListByShopId:(NSString *)shopId andCurPage:(NSString *)curPage andPageSize:(NSString *)pageSize;

#pragma mark - 订座
- (void)saveOrderSeatMealByParam:(NSDictionary *)param;

#pragma mark - 订餐
- (void)saveMealByParam:(NSDictionary *)param;

#pragma mark - 会员卡折扣和余额
- (void)requestMemberCardDiscountAndBalance:(NSDictionary *)param;

#pragma mark - 结算
- (void)payMealByOrderId:(NSString *)orderId;

#pragma mark - 订单详情
-(void)getOrderInfoByOrderId:(NSString *)orderId andUserId:(NSString *)userId;

#pragma mark - 获取订单明细
-(void)getListOrderDetailByUserId:(NSString *)userId orderId:(NSString *)orderId curPage:(NSString *)curPage pageSize:(NSString *)pageSize;

#pragma mark - 发表评论
-(void)sendEvaluateByUserId:(NSString *)userId content:(NSString *)content shopId:(NSString *)shopId starRate:(NSString *)starRate data:(NSData *)data orderId:(NSString *)orderId;

#pragma mark - 取消订单
-(void)cancelOrderByOrderId:(NSString *)orderId andUserId:(NSString *)userId;

#pragma mark - 获取地址
- (void) getMapWithDictionary:(NSDictionary *)param;

- (void)searchShopListWithDictionary:(NSDictionary *)param;

#pragma mark - 查询数据资源是否有更新
-(void)checkDataUpdate:(NSString *)carouselCacheTime andShopTime:(NSString *)shopListCacheTime;

#pragma mark - 定座点餐按钮之前验证接口
-(void)validateReserveMealWithDictionary:(NSDictionary *)param;

#pragma mark - 去付款按钮之前验证接口
-(void) validateGotoPayByOrderId:(NSString *)orderId;

@end