//
//  ZSTECCOrderFoodInfoCell.h
//  EComC
//
//  Created by anqiu on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCOrderFoodInfoCell : UITableViewCell

@property (nonatomic,strong) UILabel *foodName;
@property (nonatomic,strong) UILabel *number;
@property (nonatomic,strong) UILabel *money;

-(void)createUI;

-(void)setCellData:(NSMutableDictionary *)dic;

@end
