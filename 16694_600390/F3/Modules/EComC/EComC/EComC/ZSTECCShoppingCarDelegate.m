//
//  ZSTECCShoppingCarDelegate.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShoppingCarDelegate.h"
#import "ZSTECCShoppingCarCell.h"
#import "ZSTECCFoodInfoViewController.h"

@implementation ZSTECCShoppingCarDelegate


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.shoppingCarArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *shoppingCarCell = @"shoppingCarCell";
    
    ZSTECCShoppingCarCell *cell = [tableView dequeueReusableCellWithIdentifier:shoppingCarCell];
    
    if (!cell) {
        cell = [[ZSTECCShoppingCarCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shoppingCarCell];
    }
    
    [cell createUI];

    
//    NSDictionary *foodDic = [[self.shoppingCarArr[indexPath.row] allValues] firstObject];
    NSDictionary *dishDic = self.shoppingCarArr[indexPath.row];
    NSString *dishKey = [[dishDic allKeys] firstObject];
    NSArray *cateArr = [dishDic safeObjectForKey:dishKey];
    NSDictionary *foodDic = [[NSDictionary alloc] init];
    NSInteger count = 0;
    for (NSDictionary *cateDic in cateArr)
    {
        NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
        foodDic = [cateDic safeObjectForKey:cateKey];
        count += [[cateDic safeObjectForKey:@"dishCount"] integerValue];
    }
    NSString *foodName = [foodDic safeObjectForKey:@"dishesName"];
    NSString *foodPrice = [NSString stringWithFormat:@"￥%.2lf",[[foodDic safeObjectForKey:@"price"] doubleValue]];
    NSString *foodCount = [NSString stringWithFormat:@"%@",@(count)];
    [cell setCellDataFoodName:foodName FoodPrice:foodPrice FoodCount:foodCount];
    
    cell.plusBtn.tag = 10000 + indexPath.row;
    cell.minusBtn.tag = 1000 + indexPath.row;
    
    [cell.plusBtn addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
    [cell.minusBtn addTarget:self action:@selector(changeValue:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (void)changeValue:(UIButton *)sender
{
    if (sender.tag >= 10000)
    {
        NSInteger cellIndex = sender.tag - 10000;
        NSDictionary *dishDic = self.shoppingCarArr[cellIndex];
        NSString *dishKey = [[dishDic allKeys] firstObject];
        NSArray *cateArr = [dishDic safeObjectForKey:dishKey];
        NSDictionary *foodDic = [[NSDictionary alloc] init];
        NSInteger count = 0;
        for (NSDictionary *cateDic in cateArr)
        {
            NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
            foodDic = [cateDic safeObjectForKey:cateKey];
            count += [[cateDic safeObjectForKey:@"dishCount"] integerValue];
        }
        
        [self.foodVC modifyFoodCount:foodDic IsHas:YES IsPlus:YES];
        
    }
    else
    {
        NSInteger cellIndex = sender.tag - 1000;
        NSDictionary *dishDic = self.shoppingCarArr[cellIndex];
        NSString *dishKey = [[dishDic allKeys] firstObject];
        NSArray *cateArr = [dishDic safeObjectForKey:dishKey];
        NSDictionary *foodDic = [[NSDictionary alloc] init];
        NSInteger count = 0;
        for (NSDictionary *cateDic in cateArr)
        {
            NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
            foodDic = [cateDic safeObjectForKey:cateKey];
            count += [[cateDic safeObjectForKey:@"dishCount"] integerValue];
        }
        [self.foodVC modifyFoodCount:foodDic IsHas:YES IsPlus:NO];
    }
}

@end
