//
//  ZSTECCSearchResultDelegate.m
//  EComC
//
//  Created by pmit on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCSearchResultDelegate.h"
#import "ZSTECCShopCell.h"

@implementation ZSTECCSearchResultDelegate

static NSString *const shopCell = @"shopCell";

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.resultArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"historyCell"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"historyCell"];
    }
    
    cell.textLabel.text = self.resultArr[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:14.0f];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *searchKey = cell.textLabel.text;
    if ([self.searchHistoryDelegate respondsToSelector:@selector(sureString:)])
    {
        [self.searchHistoryDelegate sureString:searchKey];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 40;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH - 40, 40)];
    sHeaderView.backgroundColor = RGBA(244, 244, 244, 1);
    UIButton *clearBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [clearBtn setTitle:@"清除历史记录" forState:UIControlStateNormal];
    clearBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [clearBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    clearBtn.center = CGPointMake((WIDTH - 40) / 2, 15);
    clearBtn.bounds = CGRectMake(0, 0, 100, 30);
    clearBtn.layer.cornerRadius = 3.0f;
    clearBtn.layer.borderColor = [UIColor blackColor].CGColor;
    clearBtn.layer.borderWidth = 0.5f;
    [clearBtn addTarget:self action:@selector(clearSearchHistory:) forControlEvents:UIControlEventTouchUpInside];
    [sHeaderView addSubview:clearBtn];
    
    return sHeaderView;
}

- (void)clearSearchHistory:(UIButton *)sender
{
    if ([self.searchHistoryDelegate respondsToSelector:@selector(cleanHistory)])
    {
        [self.searchHistoryDelegate cleanHistory];
    }
}

@end
