//
//  ZSTGlobal+EComC.h
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

//#define FoodShopUrl @"http://wechat.pmit.cn"
//#define FoodShopUrl @"http://192.168.1.8:8081/ChuanPiao-v2"
#define FoodShopUrl @"http://192.168.1.26:10123"
//#define FoodShopUrl @"http://192.168.1.3:8081"

#define cardInfosUrl @"http://192.168.1.26:10123"

#pragma mark - 获取主页轮播图
#define getBannerURL FoodShopUrl @"/mealshopclient/meal_shopclient!getMealAdvertCompany"

#pragma mark - 获取个人订单列表信息
#define listOrderURL FoodShopUrl @"/mealorderclient/meal_orderclient!listOrder"

#pragma mark - 根据经纬度获取当前位置
#define getAddressInfoURL FoodShopUrl @"/mealshopclient/meal_shopclient!getAddressInfo"

#pragma mark - 获取店铺列表
#define getShopDCListUrl FoodShopUrl @"/mealshopclient/meal_shopclient!listShop"

#pragma mark - 初始化订餐数据
#define getInitOrderInfoUrl FoodShopUrl @"/mealorderclient/meal_orderclient!getInitOrderInfo"

#pragma mark - 菜单分类
#define getSelectedDishesUrl FoodShopUrl @"/mealdishesclient/meal_dishesclient!selectCateAndDish"

#pragma mark - 获取店铺信息
#define getShopDetailUrl FoodShopUrl @"/mealshopclient/meal_shopclient!detail"

#pragma mark - 获取店铺信息 用户评价
#define getShopReviewListUrl FoodShopUrl @"/mealshopclient/meal_shopclient!listComment"

#pragma mark - 保存订座点餐
#define saveOrderSeatMealUrl FoodShopUrl @"/mealorderclient/meal_orderclient!saveOrderSeat"

#pragma mark - 保存点餐
#define saveMealUrl FoodShopUrl @"/mealorderclient/meal_orderclient!saveOrderSeatMeal"

#pragma mark - 支付
#define payMealUrl FoodShopUrl @"/mealorderclient/meal_orderclient!alipayOrder.action"

#pragma mark - 订单详情
#define getOrderInfoUrl FoodShopUrl @"/mealorderclient/meal_orderclient!getOrder"

#pragma mark - 获取订单明细
#define listOrderDetailUrl FoodShopUrl @"/mealorderclient/meal_orderclient!listOrderDetail"

#pragma mark -  发表评论
#define sendEvaluateUrl FoodShopUrl @"/mealevaluateclient/meal_evaluateclient!evaluate"

#pragma mark - 取消订单
#define cancelOrderUrl FoodShopUrl @"/mealorderclient/meal_orderclient!cancelOrder"

#define showMap FoodShopUrl @"/mealmapclient/meal_map_client!getMapUrl"

#pragma mark - 查询数据资源是否有更新
#define checkUpdateUrl FoodShopUrl @"/mealresourcemodifytimeclient/meal_resource_modify_time_client!indexIsExpire"

#pragma mark - 定座点餐按钮之前验证接口
#define validateReserveMealUrl FoodShopUrl @"/mealorderclient/meal_orderclient!validateReserveMeal"

#pragma mark - 去付款按钮之前验证接口
#define validateGotoPayUrl FoodShopUrl @"/mealorderclient/meal_orderclient!validateGotoPay"

