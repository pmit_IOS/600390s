//
//  ZSTECCShopInfoAddressTableViewCell.m
//  EComC
//
//  Created by anqiu on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopInfoAddressTableViewCell.h"
#import "ZSTECCAutoLabel.h"


@implementation ZSTECCShopInfoAddressTableViewCell{
    
    UIView *v1;
    UIView *v2;
    UIView *v3;
    
    NSString *phone;
    UILabel *phoneLab;
    NSInteger timeHeight;
    
}

-(void)createUI{
    
    if (!v1)
    {
        v1 = [[UIView alloc] initWithFrame:CGRectMake(0, 10, 320, 20)];
        [self.contentView addSubview:v1];
        UILabel *l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 20)];
        l1.font = [UIFont systemFontOfSize:14];
        l1.text = @"联系电话:";
        l1.textColor = [UIColor grayColor];
        [v1 addSubview:l1];
        
        phoneLab = [[UILabel alloc] initWithFrame:CGRectMake(10+70, 0, 230, 20)];
        phoneLab.font = [UIFont systemFontOfSize:14];
        phoneLab.textColor = [UIColor redColor];
        [v1 addSubview:phoneLab];
        
        self.phone = [[UIButton alloc] initWithFrame:CGRectMake(10+70, 5, 130, 20)];
        [self.phone addTarget:self action:@selector(callPhone) forControlEvents:UIControlEventTouchUpInside];

        [v1 addSubview:self.phone];
        
        v2 = [[UIView alloc] initWithFrame:CGRectMake(0, 10+25, 320, 20)];
        [self.contentView addSubview:v2];
        UILabel *l21 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 20)];
        l21.text = @"预约时间:";
        l21.font = [UIFont systemFontOfSize:14];
        l21.textColor = [UIColor grayColor];
        [v2 addSubview:l21];
        self.time = [[UILabel alloc] init];
        self.time.font = [UIFont systemFontOfSize:14];
        self.time.textColor = [UIColor grayColor];
        [v2 addSubview:self.time];
        
        v3 = [[UIView alloc] initWithFrame:CGRectMake(0, 10+25+20, 320, 20)];
        [self.contentView addSubview:v3];
        UILabel *l31 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 20)];
        l31.text = @"店铺地址:";
        l31.font = [UIFont systemFontOfSize:14];
        l31.textColor = [UIColor grayColor];
        [v3 addSubview:l31];
        
        self.addressLabel = [[UILabel alloc] init];
        self.addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.addressLabel.textColor = [UIColor grayColor];
        [v3 addSubview:self.addressLabel];
    }
}



-(void)setCellData:(NSDictionary *)dic{
    
    phone = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"shopTel"]];
    
    phoneLab.text = phone;

    NSString *timeStr = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"businessTime"]];

    self.time.text = timeStr;
    
    if (self.time.text) {
        self.time.lineBreakMode = NSLineBreakByWordWrapping;
        self.time.numberOfLines = 0;
        CGSize timeLabSize = [timeStr sizeWithFont:self.time.font constrainedToSize:CGSizeMake(230.f, MAXFLOAT)  lineBreakMode:self.time.lineBreakMode];

        timeHeight = timeLabSize.height;
        
        if (timeHeight <=20) {
            timeHeight = 30;
            self.time.frame = CGRectMake(10+70, 0, 230, 20);
            v2.frame = CGRectMake(0, 25+10, 320, timeHeight);
        }else{
            self.time.frame = CGRectMake(10+70, -5, 230, timeHeight);
            v2.frame = CGRectMake(0, 25+10, 320, timeHeight);
        }
        
        
    }
    
    
    
    //地址不为空
    if ([dic safeObjectForKey:@"shopAddress"]){
        self.addressLabel.text = @"";
        self.addressLabel.lineBreakMode = NSLineBreakByWordWrapping;
        self.addressLabel.numberOfLines = 0;
        self.addressLabel.font = [UIFont systemFontOfSize:14];

        NSString *addStr = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"shopAddress"]];
        self.addressLabel.text = addStr;

        CGSize reviewLabSize = [addStr sizeWithFont:self.addressLabel.font constrainedToSize:CGSizeMake(230.f, MAXFLOAT)  lineBreakMode:self.addressLabel.lineBreakMode];

        _height = reviewLabSize.height;

        if (_height <=20) {
            _height = 20 ;
            
            v3.frame = CGRectMake(0, 10+20+timeHeight, 320,_height);
            self.addressLabel.frame = CGRectMake(10+70, 0, 230, _height);
        }else{
            
            v3.frame = CGRectMake(0, 10+20+timeHeight, 320, 10+_height);
            self.addressLabel.frame = CGRectMake(10+70, -2, 230, _height+10);
        }
        
    }
    
    
}


-(void)callPhone{

    [self.delegate callPhone:phone];
}


@end
