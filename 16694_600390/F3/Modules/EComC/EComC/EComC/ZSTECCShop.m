//
//  ZSTECCShop.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShop.h"

@implementation ZSTECCShop

@end


@implementation EcomCCarouseInfo

+ (id)ecomCCarouseWithArray:(NSArray *) arr
{
    NSMutableArray * dataArr = [[NSMutableArray alloc]init];
    for (NSDictionary * dic in arr) {
        [dataArr addObject:[[self alloc] initWithDictionary:dic]];
    }
    return  dataArr;
    
}
- (id)initWithDictionary:(NSDictionary *) dic
{
    EcomCCarouseInfo * carouse = [[EcomCCarouseInfo alloc]init];
    carouse.ecomCCarouseInfoCarouselurl = [dic safeObjectForKey:@"carouselurl"];
    carouse.ecomCCarouseInfoLinkurl = [dic safeObjectForKey:@"linkurl"];
    carouse.ecomCCarouseInfoTitle = [dic safeObjectForKey:@"title"];
    carouse.ecomCCarouseInfoProductid = [[dic safeObjectForKey:@"productid"]intValue];
    return carouse;
}

@end