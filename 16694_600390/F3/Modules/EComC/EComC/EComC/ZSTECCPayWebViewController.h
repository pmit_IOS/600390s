//
//  ZSTECCPayWebViewController.h
//  EComC
//
//  Created by pmit on 15/8/20.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCPayWebViewController : UIViewController

@property (copy,nonatomic) NSString *urlString;
@property (strong,nonatomic) NSString *orderId;
@property (strong,nonatomic) Class fromVC;

@end
