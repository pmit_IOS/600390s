//
//  ZSTECCFoodInfoViewController.m
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCFoodInfoViewController.h"
#import "ZSTECCLeftMenuCell.h"
#import "ZSTEComCDetailDelegate.h"
#import "ZSTECCFoodDetailCell.h"
#import "ZSTECCSureOrderViewController.h"
#import "ZSTECCShoppingCarDelegate.h"
#import "ZSTECCShoppingCarCell.h"
#import <PMCallButton.h>

@interface ZSTECCFoodInfoViewController () <UIAlertViewDelegate>

@property (strong,nonatomic) ZSTEComCDetailDelegate *rightFoodDelegate;
@property (strong,nonatomic) PMRepairButton *shopCarBtn;
@property (strong,nonatomic) UILabel *priceLB;
@property (strong,nonatomic) UIView *clearView;
@property (assign,nonatomic) BOOL isCarHidden;
@property (strong,nonatomic) UIView *shoppingCarView;
@property (strong,nonatomic) ZSTECCShoppingCarDelegate *shoppingCarDelegate;
@property (strong,nonatomic) UITableView *shoppingCarTableView;
@property (strong,nonatomic) UILabel *shoppingCarViewPriceLB;
@property (strong,nonatomic) NSArray *dishesTypeArr;
@property (strong,nonatomic) NSArray *dishesDetailArr;
@property (nonatomic,strong) UIBezierPath *path;

@end

@implementation ZSTECCFoodInfoViewController{
  
    UIButton *numLab;
    UIButton *numLab1;
    
    CALayer     *layer;
    NSInteger    _cnt;
    UIImageView *_imageView;
    UIView *outView;
}

static NSString *const foodDetailCell = @"foodDetailCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.selectedDishesArr = [NSMutableArray array];
    self.rightFoodDelegate = [[ZSTEComCDetailDelegate alloc] init];
    self.rightFoodDelegate.foodVC = self;
    self.shoppingCarDelegate = [[ZSTECCShoppingCarDelegate alloc] init];
    self.shoppingCarDelegate.foodVC = self;
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (sureExit)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"点餐信息", nil)];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self createTableView];
    [self buildBottomToolView];
    [self buildDarkView];
    [self buildShoppingCarView];
    
    [self getShopDishes];
    
    outView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 500)];
    outView.hidden = YES;
    [self.view addSubview:outView];
    self.path = [UIBezierPath bezierPath];

}

#pragma mark - 初始化左侧菜单与右侧食品 视图
-(void)createTableView{
   
    self.leftMenuTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH * 0.3, HEIGHT - 64 - 50)];
    self.leftMenuTableView.backgroundColor = RGBCOLOR(246, 246, 246);
    self.leftMenuTableView.delegate = self;
    self.leftMenuTableView.dataSource = self;
    self.leftMenuTableView.separatorStyle = UITableViewCellAccessoryNone;
    [self.view addSubview:self.leftMenuTableView];
    
    self.rightFoodTableView = [[UITableView alloc] initWithFrame:CGRectMake(WIDTH * 0.3, 0, WIDTH * 0.7, HEIGHT-64-60)];
    self.rightFoodTableView.delegate = self.rightFoodDelegate;
    self.rightFoodTableView.dataSource = self.rightFoodDelegate;
    [self.rightFoodTableView registerClass:[ZSTECCFoodDetailCell class] forCellReuseIdentifier:foodDetailCell];
    [self.view addSubview:self.rightFoodTableView];

    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    self.rightFoodTableView.tableFooterView = footerView;
}


#pragma mark - 数据源
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    return self.dishesTypeArr.count;
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{

    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    static NSString *leftMenuCell = @"leftMenuCell";
    ZSTECCLeftMenuCell *cell = (ZSTECCLeftMenuCell *)[tableView dequeueReusableCellWithIdentifier:leftMenuCell];
    if (!cell) {
        cell = [[ZSTECCLeftMenuCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:leftMenuCell];
    }
    [cell createUI];
    NSDictionary *oneDishes = self.dishesTypeArr[indexPath.row];
    NSString *typeName = [[oneDishes allKeys] firstObject];
    [cell setCellDataWithTypeName:typeName];
    
    return  cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.rightFoodDelegate.detailTableView = self.rightFoodTableView;
    NSDictionary *thisTypeDic = self.dishesTypeArr[indexPath.row];
    NSString *key = [[thisTypeDic allKeys] firstObject];
    NSArray *detailArr = [thisTypeDic objectForKey:key];
    self.rightFoodDelegate.detailFoodArr = detailArr;
    [self.rightFoodTableView reloadData];
    
    if (indexPath.row < 7)
    {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:indexPath.row inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    else
    {
        [tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:7 inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
//    self.rightFoodDelegate.detailFoodArr = 
}

- (void)buildBottomToolView
{
    UIView *toolView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 50 - 64, WIDTH, 50)];
    toolView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:toolView];
    
    CALayer *topLine = [CALayer layer];
    topLine.backgroundColor = [UIColor lightGrayColor].CGColor;
    topLine.frame = CGRectMake(0, 0, WIDTH, 1);
    [toolView.layer addSublayer:topLine];
    
    self.shopCarBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(15, -10, 56, 56)];
    self.shopCarBtn.backgroundColor = RGBA(255, 199, 0, 1);
    self.shopCarBtn.layer.cornerRadius = 28.0f;
//    [self.shopCarBtn setImage:[UIImage imageNamed:@"ecomCShopCar"] forState:UIControlStateNormal];
    [self.shopCarBtn addTarget:self action:@selector(showShopCar:) forControlEvents:UIControlEventTouchUpInside];
    [toolView addSubview:self.shopCarBtn];
    
    UIImageView *carIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ecomCShopCar.png"]];
    carIV.frame = CGRectMake(10, 15, 36, 36);
    carIV.contentMode = UIViewContentModeScaleAspectFit;
    [self.shopCarBtn addSubview:carIV];
    
    numLab = [[UIButton alloc] initWithFrame:CGRectMake(30, 5, 20, 20)];
    //[numLab setTitle:@"20" forState:UIControlStateNormal];
    numLab.hidden = YES;
    numLab.layer.cornerRadius = 10.0f;
    numLab.titleLabel.font = [UIFont systemFontOfSize:10];
    [numLab setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [numLab setBackgroundColor:[UIColor whiteColor]];
    numLab.backgroundColor = [UIColor redColor];
    [self.shopCarBtn addSubview:numLab];
    
    self.priceLB = [[UILabel alloc] initWithFrame:CGRectMake(80, 10, WIDTH - 80 - 15 - 100, 30)];
    self.priceLB.textAlignment = NSTextAlignmentLeft;
    self.priceLB.font = [UIFont systemFontOfSize:16.0f];
    self.priceLB.textColor = RGBA(254, 78, 60, 1);
    self.priceLB.text = @"￥0.00";
    [toolView addSubview:self.priceLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 15 - 100, 10, 100, 30);
    sureBtn.backgroundColor = RGBA(254, 78, 60, 1);
    sureBtn.layer.cornerRadius = 4.0f;
    [sureBtn addTarget:self action:@selector(sureOrder:) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setTitle:@"选好了" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [toolView addSubview:sureBtn];
    
}

- (void)buildShoppingCarView
{
    self.shoppingCarView = [[UIView alloc] initWithFrame:CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6)];
    self.shoppingCarView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.shoppingCarView];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    line.frame = CGRectMake(0, 38, WIDTH, 1);
    [self.shoppingCarView.layer addSublayer:line];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(15, -18, 56, 56);
    button.layer.cornerRadius = 28.0f;
    button.backgroundColor = RGBA(255, 199, 0, 1);
    [button addTarget:self action:@selector(dismissShoppingCarView:) forControlEvents:UIControlEventTouchUpInside];
    [self.shoppingCarView addSubview:button];
    
    UIImageView *carIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"ecomCShopCar.png"]];
    carIV.frame = CGRectMake(10, 15, 36, 36);
    carIV.contentMode = UIViewContentModeScaleAspectFit;
    [button addSubview:carIV];
    
    numLab1 = [[UIButton alloc] initWithFrame:CGRectMake(30, 5, 20, 20)];
    //[numLab setTitle:@"20" forState:UIControlStateNormal];
    numLab1.hidden = YES;
    numLab1.layer.cornerRadius = 10.0f;
    numLab1.titleLabel.font = [UIFont systemFontOfSize:10];
    [numLab1 setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [numLab1 setBackgroundColor:[UIColor whiteColor]];
    numLab1.backgroundColor = [UIColor redColor];
    [button addSubview:numLab1];
    
    PMCallButton *deleteBtn = [[PMCallButton alloc] initWithFrame:CGRectMake(WIDTH - 15 - 120, 0, 120, 38)];
    [deleteBtn setImage:[UIImage imageNamed:@"ZSTEECTrash.png"] forState:UIControlStateNormal];
    [deleteBtn setTitle:@"清空购物车" forState:UIControlStateNormal];
    [deleteBtn setTitleColor:RGBA(153, 153, 153, 1) forState:UIControlStateNormal];
    [deleteBtn addTarget:self action:@selector(clearShoppingCar:) forControlEvents:UIControlEventTouchUpInside];
    deleteBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.shoppingCarView addSubview:deleteBtn];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(dismissShoppingCarViewByGesture:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self.shoppingCarView addGestureRecognizer:swipe];
    
    self.shoppingCarTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 39, WIDTH, (HEIGHT - 64) * 0.6 - 39 - 60) style:UITableViewStylePlain];
    self.shoppingCarTableView.delegate = self.shoppingCarDelegate;
    self.shoppingCarTableView.dataSource = self.shoppingCarDelegate;
    self.shoppingCarTableView.backgroundColor = [UIColor whiteColor];
    [self.shoppingCarTableView registerClass:[ZSTECCShoppingCarCell class] forCellReuseIdentifier:@"shoppingCarCell"];
    [self.shoppingCarView addSubview:self.shoppingCarTableView];
    
    self.shoppingCarDelegate.shoppingCarTableView = self.shoppingCarTableView;
    
    UIView *shoppingCarFooterView = [[UIView alloc] init];
    shoppingCarFooterView.backgroundColor = [UIColor whiteColor];
    self.shoppingCarTableView.tableFooterView = shoppingCarFooterView;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, self.shoppingCarView.bounds.size.height - 60, WIDTH, 60)];
    view.backgroundColor = [UIColor whiteColor];
    CALayer *bottomLine = [CALayer layer];
    bottomLine.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    bottomLine.frame = CGRectMake(0, 0, WIDTH, 1);
    [view.layer addSublayer:bottomLine];
    [self.shoppingCarView addSubview:view];
    
    self.shoppingCarViewPriceLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH / 2, 40)];
    self.shoppingCarViewPriceLB.font = [UIFont systemFontOfSize:16.0f];
    self.shoppingCarViewPriceLB.textColor = RGBA(254, 78, 60, 1);
    [view addSubview:self.shoppingCarViewPriceLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 15 - 100, 15, 100, 30);
    sureBtn.backgroundColor = RGBA(254, 78, 60, 1);
    sureBtn.layer.cornerRadius = 4.0f;
    [sureBtn addTarget:self action:@selector(okCar:) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setTitle:@"选好了" forState:UIControlStateNormal];
    [sureBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [view addSubview:sureBtn];
    
}

- (void)buildDarkView
{
    self.clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.clearView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.clearView];
    
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.clearView.bounds.size.width, self.clearView.bounds.size.height)];
    darkView.backgroundColor = [UIColor blackColor];
    darkView.alpha = 0.8;
    [self.clearView addSubview:darkView];
    
    UITapGestureRecognizer *tap  = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissCarView:)];
    [darkView addGestureRecognizer:tap];
    
    self.clearView.hidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (void)sureOrder:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    if (self.selectedDishesArr.count == 0)
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有选择食物呀!", nil) withImage:nil];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        ZSTECCSureOrderViewController *orderVC = [[ZSTECCSureOrderViewController alloc] init];
        orderVC.shopId = self.shopId;
        orderVC.shopName = self.shopName;
        orderVC.shopAddress = self.shopAddress;
        orderVC.bookTimeString = self.bookTimeString;
        orderVC.selectedSeatTypeString = self.selectedSeatTypeString;
        orderVC.selectedSexTypeString = self.selectedSexTypeString;
        orderVC.bookManNameString = self.bookManNameString;
        orderVC.bookPhoneString = self.bookPhoneString;
        orderVC.bookCount = self.bookCount;
        orderVC.bookFoodArr = self.selectedDishesArr;
        orderVC.backUpString = self.backUpString;
        [self.navigationController pushViewController:orderVC animated:YES];
        self.view.userInteractionEnabled = YES;
    }
    
}

- (void)showShopCar:(UIButton *)sender
{
    self.clearView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)dismissShoppingCarView:(UIButton *)sender
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)dismissShoppingCarViewByGesture:(UISwipeGestureRecognizer *)swipe
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)okCar:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    if (self.selectedDishesArr.count == 0)
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有选择食物呀!", nil) withImage:nil];
        self.view.userInteractionEnabled = YES;
    }
    else
    {
        if (self.selectedDishesArr.count == 0)
        {
            [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有选择食物呀!", nil) withImage:nil];
        }
        else
        {
            ZSTECCSureOrderViewController *orderVC = [[ZSTECCSureOrderViewController alloc] init];
            orderVC.shopId = self.shopId;
            orderVC.shopName = self.shopName;
            orderVC.shopAddress = self.shopAddress;
            orderVC.bookTimeString = self.bookTimeString;
            orderVC.selectedSeatTypeString = self.selectedSeatTypeString;
            orderVC.selectedSexTypeString = self.selectedSexTypeString;
            orderVC.bookManNameString = self.bookManNameString;
            orderVC.bookPhoneString = self.bookPhoneString;
            orderVC.bookCount = self.bookCount;
            orderVC.bookFoodArr = self.selectedDishesArr;
            orderVC.backUpString = self.backUpString;
            [self.navigationController pushViewController:orderVC animated:YES];
            self.view.userInteractionEnabled = YES;
        }
    }
}

- (void)dismissCarView:(UITapGestureRecognizer *)tap
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shoppingCarView.frame = CGRectMake(0, (HEIGHT - 64) * 0.4 + HEIGHT, WIDTH, (HEIGHT - 64) * 0.6);
    }];
}

- (void)getShopDishes
{
    [self.engine getShopDishesByShopId:self.shopId AndUserId:[ZSTF3Preferences shared].UserId];
}

- (void)getDishesInfoSuccessed:(NSDictionary *)response
{
    self.dishesTypeArr = [response safeObjectForKey:@"dataList"];
    [self.leftMenuTableView reloadData];
    if (self.dishesTypeArr.count == 0)
    {
        
    }
    else
    {
        NSIndexPath *ip=[NSIndexPath indexPathForRow:0 inSection:0];
        [self.leftMenuTableView selectRowAtIndexPath:ip animated:YES scrollPosition:UITableViewScrollPositionBottom];
        [self tableView:self.leftMenuTableView didSelectRowAtIndexPath:ip];
    }
    
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
    self.engine.delegate = nil;
}

#pragma mark - 创建加入购物车动画
-(void)createAnimation:(NSInteger)hieght{
    
    [_path moveToPoint:CGPointMake(250, hieght+50)];
    [_path addQuadCurveToPoint:CGPointMake(55, HEIGHT-100) controlPoint:CGPointMake(50, 20)];
    [self startAnimation];
}


- (void)modifyFoodCount:(NSDictionary *)foodDic IsHas:(BOOL)isHas IsPlus:(BOOL)isPlus
{
    NSString *dishId = [foodDic safeObjectForKey:@"dishId"];
    NSString *cateId = [foodDic safeObjectForKey:@"cateId"];
    
    if (!isHas)
    {
//        NSDictionary *newsSelectedDic = @{dishId:foodDic,@"dishCount":@(1)};
//        [self.selectedDishesArr addObject:newsSelectedDic];
        NSDictionary *newsSelectedDic = @{dishId:@[@{cateId:foodDic,@"dishCount":@(1)}]};
        [self.selectedDishesArr addObject:newsSelectedDic];
    }
    else
    {
        if (isPlus)
        {
            for (NSInteger i = 0; i < self.selectedDishesArr.count; i++)
            {
                NSMutableDictionary *oneDic = [self.selectedDishesArr[i] mutableCopy];
                if ([[[oneDic allKeys] firstObject] isEqualToString:dishId])
                {
                    NSMutableArray *dishArr = [[oneDic safeObjectForKey:dishId] mutableCopy];
                    BOOL isHasCate = NO;
                    for (NSInteger j = 0; j < dishArr.count; j++)
                    {
                        NSMutableDictionary *cateDic = [dishArr[j] mutableCopy];
                        NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
                        if ([cateKey isEqualToString:cateId])
                        {
                            isHasCate = YES;
                            NSInteger count = [[cateDic safeObjectForKey:@"dishCount"] integerValue] + 1;
                            [cateDic setValue:@(count) forKey:@"dishCount"];
                            [dishArr replaceObjectAtIndex:j withObject:cateDic];
                            [oneDic setValue:dishArr forKey:dishId];
                            [self.selectedDishesArr replaceObjectAtIndex:i withObject:oneDic];
                            break;
                        }
                    }
                    
                    if (!isHasCate)
                    {
                        NSDictionary *newsCateDic = @{cateId:foodDic,@"dishCount":@(1)};
                        [dishArr addObject:newsCateDic];
                        [oneDic setValue:dishArr forKey:dishId];
                        [self.selectedDishesArr replaceObjectAtIndex:i withObject:oneDic];
                    }
                    
                    break;
                }
            }
        }
        else
        {
            for (NSInteger i = 0; i < self.selectedDishesArr.count; i++)
            {
                NSMutableDictionary *oneDic = [self.selectedDishesArr[i] mutableCopy];
                if ([[[oneDic allKeys] firstObject] isEqualToString:dishId])
                {
                    NSMutableArray *dishArr = [[oneDic safeObjectForKey:dishId] mutableCopy];
                    for (NSInteger j = 0; j < dishArr.count; j++)
                    {
                        NSMutableDictionary *cateDic = [dishArr[j] mutableCopy];
                        NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
                        if ([cateKey isEqualToString:cateId])
                        {
                            NSInteger count = [[cateDic safeObjectForKey:@"dishCount"] integerValue] - 1;
                            if (count == 0)
                            {
                                [dishArr removeObject:cateDic];
                                
                                if (dishArr.count == 0)
                                {
                                    [self.selectedDishesArr removeObject:oneDic];
                                }
                                else
                                {
                                    [oneDic setValue:dishArr forKey:dishId];
                                    [self.selectedDishesArr replaceObjectAtIndex:i withObject:oneDic];
                                }
                            }
                            else
                            {
                                [cateDic setValue:@(count) forKey:@"dishCount"];
                                [dishArr replaceObjectAtIndex:j withObject:cateDic];
                                [oneDic setValue:dishArr forKey:dishId];
                                [self.selectedDishesArr replaceObjectAtIndex:i withObject:oneDic];
                            }
                            
                            break;
                        }
                    }
                    
                    break;
                }
            }
        }
    }
    
    [self.rightFoodTableView reloadData];
    self.shoppingCarDelegate.shoppingCarArr = self.selectedDishesArr;
    [self.shoppingCarTableView reloadData];
    
    [self refreshPrice];
}

- (void)refreshPrice
{
    double price = 0;
    NSInteger dishNum;
    dishNum = 0;
    for (NSDictionary *oneDic in self.selectedDishesArr)
    {
        NSString *key = [[oneDic allKeys] firstObject];
        NSArray *dishArr = [oneDic safeObjectForKey:key];
        for (NSDictionary *cateDic in dishArr)
        {
            NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
            double onePrice = [[[cateDic safeObjectForKey:cateKey] safeObjectForKey:@"price"] doubleValue];
            NSInteger num = [[cateDic safeObjectForKey:@"dishCount"] integerValue];
            price += onePrice * num;
            dishNum += num;
        }
        
    }
    
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:[NSString stringWithFormat:@"￥%.2lf",price]];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} range:NSMakeRange(0, 1)];
    self.priceLB.attributedText = nodeString;
    self.shoppingCarViewPriceLB.attributedText = nodeString;
    
    
    if (dishNum > 0) {
        numLab.hidden = NO;
        numLab1.hidden = NO;
        [numLab setTitle:[NSString stringWithFormat:@"%ld",dishNum] forState:UIControlStateNormal];
        [numLab1 setTitle:[NSString stringWithFormat:@"%ld",dishNum] forState:UIControlStateNormal];
    }else{
        numLab.hidden = YES;
        numLab1.hidden = YES;
    }
}

- (void)clearShoppingCar:(UIButton *)sender
{
    self.selectedDishesArr = [NSMutableArray array];
    [self.rightFoodTableView reloadData];
    self.shoppingCarDelegate.shoppingCarArr = self.selectedDishesArr;
    [self.shoppingCarTableView reloadData];
    [self refreshPrice];
}

- (void)sureExit
{
    if (self.selectedDishesArr.count != 0)
    {
        UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"离开时购物车将会被清空哦,确定要离开?" delegate:self cancelButtonTitle:@"留下" otherButtonTitles:@"狠心离开", nil];
        [tipAlert show];
    }
    else
    {
        [self popViewController];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 1)
    {
        self.selectedDishesArr = [NSMutableArray array];
        [self.rightFoodTableView reloadData];
        [self.shoppingCarTableView reloadData];
        [self popViewController];
    }
}

//-(void)tapnow:(UITapGestureRecognizer *)S{
//    [textView resignFirstResponder];
//}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    self.view.userInteractionEnabled = YES;
}


//获取 window的 最外层的视图
- (UIView *) GetWindowView
{
    UIView *_parentView = nil;
    NSArray* windows = [UIApplication sharedApplication].windows;
    UIWindow * _window = [windows lastObject];
    //keep the first subview
    if(_window.subviews.count > 0){
        _parentView = [_window.subviews lastObject];
        //[_parentView removeFromSuperview];
    }
    return _parentView;
}

-(void)startAnimation
{
    
    if (!layer) {
        outView.hidden = NO;
        //_btn.enabled = NO;
        layer = [CALayer layer];
        
        layer.contents = (__bridge id)[UIImage imageNamed:@"model_ecomc_shopCart.png"].CGImage;
        layer.contentsGravity = kCAGravityResizeAspectFill;
        layer.bounds = CGRectMake(250, 0, 10, 10);
        [layer setCornerRadius:CGRectGetHeight([layer bounds]) / 2];
        layer.masksToBounds = YES;
        layer.position =CGPointMake(50, 150);
        [layer removeAllAnimations];
        [outView.layer addSublayer:layer];
    }
    [self groupAnimation];
}
-(void)groupAnimation
{
    CAKeyframeAnimation *animation = [CAKeyframeAnimation animationWithKeyPath:@"position"];
    animation.path = _path.CGPath;
    animation.rotationMode = kCAAnimationRotateAuto;
    animation.removedOnCompletion = YES;
    
    CABasicAnimation *expandAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    expandAnimation.duration = 0.5f;
    expandAnimation.fromValue = [NSNumber numberWithFloat:1];
    expandAnimation.toValue = [NSNumber numberWithFloat:2.0f];
    expandAnimation.timingFunction=[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseIn];
    expandAnimation.removedOnCompletion = YES;
    
    CABasicAnimation *narrowAnimation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
    narrowAnimation.beginTime = 0.5;
    narrowAnimation.fromValue = [NSNumber numberWithFloat:2.0f];
    narrowAnimation.duration = 1.5f;
    narrowAnimation.toValue = [NSNumber numberWithFloat:0.5f];
    narrowAnimation.removedOnCompletion = YES;
    narrowAnimation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseOut];
    
    CAAnimationGroup *groups = [CAAnimationGroup animation];
    groups.animations = @[animation,expandAnimation,narrowAnimation];
    groups.duration = 0.5f;
    groups.removedOnCompletion=YES;
    groups.fillMode=kCAFillModeForwards;
    groups.delegate = self;
    [layer addAnimation:groups forKey:@"group"];
    layer.position =CGPointMake(0, HEIGHT);
}

-(void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag
{
    [_path removeAllPoints];
    [layer removeFromSuperlayer];
    outView.hidden = YES;
    layer = nil;
    if (anim == [layer animationForKey:@"group"]) {
        CATransition *animation = [CATransition animation];
        animation.duration = 0.25f;
        CABasicAnimation *shakeAnimation = [CABasicAnimation animationWithKeyPath:@"transform.translation.y"];
        shakeAnimation.duration = 0.25f;
        shakeAnimation.fromValue = [NSNumber numberWithFloat:-5];
        shakeAnimation.toValue = [NSNumber numberWithFloat:5];
        shakeAnimation.autoreverses = YES;
    }
}
@end
