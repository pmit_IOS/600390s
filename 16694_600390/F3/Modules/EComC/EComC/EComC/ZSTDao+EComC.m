//
//  ZSTDao+EComC.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTDao+EComC.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"
#import <math.h>

#import "ZSTECCFoodShopModel.h"
#import "ZSTECCAdvertBannerModel.h"

//点餐主页轮播图表
#define CREATE_TABLE_ADVERTBANNER [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [fs_advertBanner] (\
id INTEGER PRIMARY KEY AUTOINCREMENT, \
title  VARCHAR DEFAULT '', \
carouselurl VARCHAR DEFAULT '', \
linkurl VARCHAR DEFAULT '', \
);"]

//店铺
#define CREATE_TABLE_FOODSHOP [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [fs_foodShop] (\
id INTEGER PRIMARY KEY AUTOINCREMENT, \
shopName VARCHAR DEFAULT '', \
shopLogo VARCHAR DEFAULT '', \
shopAddress VARCHAR DEFAULT '', \
latitude VARCHAR DEFAULT '', \
longitude VARCHAR DEFAULT '', \
);"]


@implementation ZSTDao (EComC)


#pragma mark - 创建点餐主页轮播图表
-(void)createTableIfNotExistAdvertBanner{

    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_ADVERTBANNER];
}

#pragma mark － 加店铺
-(BOOL)addFoodShop:(ZSTECCFoodShopModel *)shop{
  
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO fs_foodShop (shopName, shopLogo, shopAddress, latitude, longitude) VALUES (?,?,?,?,?)"],
    [TKUtil wrapNilObject:shop.shopName],
    [TKUtil wrapNilObject:shop.shopLogo],
    [TKUtil wrapNilObject:shop.shopAddress],
    [TKUtil wrapNilObject:shop.latitude],
    [TKUtil wrapNilObject:shop.longtude]
     ];
    return success;

}

#pragma mark － 获取本地店铺列表
- (NSArray *)getShopsList:(float)latitude andLng:(float)longtude
{
    double PI = 3.14159265358979323846 ;
    double R = 6378.137;
    NSString *querySql = [NSString stringWithFormat:@"SELECT id,shopName,shopLogo,shopAddress, (acosf(sinf(%f)*sinf(%f*sh.latitude)+cosf(%f)*cosf(%f*sh.latitude)*cosf(%fsh.longitude-%f))*%f)distance FROM fs_foodShop sh order by distance decs",(PI/180)*latitude,PI/180,(PI/180)*longtude,PI/180,PI/180,(PI/180)*longtude,R];

    
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

#pragma mark - 通过shopid删除店铺
-(BOOL)delectShopById:(NSString *)id{
    
    
    return nil;
}



@end
