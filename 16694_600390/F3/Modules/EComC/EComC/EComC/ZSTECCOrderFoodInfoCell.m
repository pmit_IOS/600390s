//
//  ZSTECCOrderFoodInfoCell.m
//  EComC
//
//  Created by anqiu on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCOrderFoodInfoCell.h"

@implementation ZSTECCOrderFoodInfoCell


-(void)createUI{
    
    if (!self.foodName)
    {
        self.foodName = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 180, 30)];
        self.foodName.textColor = [UIColor grayColor];
        self.foodName.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:self.foodName];
        
        self.number = [[UILabel alloc] initWithFrame:CGRectMake(10+190, 5, 40, 30)];
        self.number.textColor = [UIColor grayColor];
        self.number.font = [UIFont systemFontOfSize:15];
        [self.contentView addSubview:self.number];
        
        self.money = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-100-10, 5, 100, 30)];
        self.money.textColor = [UIColor grayColor];
        self.money.font = [UIFont systemFontOfSize:15];
        self.money.textAlignment = UITextAlignmentRight;
        [self.contentView addSubview:self.money];
    }
    
}

-(void)setCellData:(NSMutableDictionary *)dic{
    
    self.foodName.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"dishesName"]];
    self.number.text = [NSString stringWithFormat:@"%@x",[dic objectForKey:@"dishesCount"]];
    self.money.text = [NSString stringWithFormat:@"%@",[dic objectForKey:@"dishesPrice"]];
    
}



-(NSString *)filterParam:(NSString *)param{

    if (param) {
        return param;
    }
    return @"";
}
@end
