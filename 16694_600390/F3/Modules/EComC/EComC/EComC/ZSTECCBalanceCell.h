//
//  ZSTECCBalanceCell.h
//  EComC
//
//  Created by P&M on 15/10/23.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCBalanceCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLB;
@property (strong, nonatomic) UILabel *balanceLB;

- (void)createBalanceUI;
- (void)setCellDataWithBalance:(NSString *)balanceString;

@end
