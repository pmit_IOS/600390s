//
//  ZSTECCShoppingCarCell.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PMRepairButton.h>

@interface ZSTECCShoppingCarCell : UITableViewCell

@property (strong,nonatomic) UILabel *carFoodNameLB;
@property (strong,nonatomic) UILabel *carFoodPriceLB;
@property (strong,nonatomic) UILabel *carFoodCountLB;
@property (strong,nonatomic) PMRepairButton *minusBtn;
@property (strong,nonatomic) PMRepairButton *plusBtn;

- (void)createUI;
- (void)setCellDataFoodName:(NSString *)foodName FoodPrice:(NSString *)foodPrice FoodCount:(NSString *)foodCount;

@end
