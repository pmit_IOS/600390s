//
//  ZSTECCOrderListCell.m
//  EComC
//
//  Created by qiuguian on 8/10/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import "ZSTECCOrderListCell.h"

@implementation ZSTECCOrderListCell{

    UIView *codeView;
    UIView *shopView;
    UIView *payView;
    
    UILabel *payStatusLab;
    
    NSString *bespeakType;
    
    NSString *orderId;
    
    BOOL isHasPay;
    
    NSString *bespeakTypeName;
}

#pragma mark - 创建cell视图
-(void)createUI{
    
    self.contentView.backgroundColor = [UIColor whiteColor];
    
    if (!self.totalMoney)
    {
        codeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 35)];
        codeView.backgroundColor = RGBCOLOR(206, 206, 206);
        [self.contentView addSubview:codeView];
        
        UIView *bg = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 35)];
        bg.backgroundColor = [UIColor whiteColor];
        [codeView addSubview:bg];
        
        UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 1)];
        line.backgroundColor = RGBA(244, 244, 244, 1);
        //[bg addSubview:line];
        
        UILabel *orderLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 40, 30)];
        orderLab.text = @"订单号:";
        orderLab.font = [UIFont systemFontOfSize:10];
        orderLab.textColor =  RGBA(206, 206, 206, 1);
        orderLab.textAlignment = NSTextAlignmentLeft;
        [bg addSubview:orderLab];
        
        self.orderCode = [[UILabel alloc] initWithFrame:CGRectMake(55, 5,150, 30)];
        self.orderCode.font = [UIFont systemFontOfSize:10];
        self.orderCode.textColor = RGBA(206, 206, 206, 1);
        self.orderCode.textAlignment = NSTextAlignmentLeft;
        [bg addSubview:self.orderCode];
        
        self.time = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-120-10, 5, 120, 30)];
        self.time.font = [UIFont systemFontOfSize:10];
        self.time.textColor = RGBA(206, 206, 206, 1);
        self.time.textAlignment = NSTextAlignmentLeft;
        [bg addSubview:self.time];
        
        line = [[UILabel alloc] initWithFrame:CGRectMake(10, 34, 300, 1)];
        line.backgroundColor = RGBA(244, 244, 244, 1);
        line.alpha = 0.5;
        [bg addSubview:line];
        
        
        //
        shopView = [[UIView alloc] initWithFrame:CGRectMake(0, 35, 320, 60)];
        shopView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:shopView];
        
        self.shopName = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 150, 50)];
        self.shopName.font = [UIFont systemFontOfSize:14];
        self.shopName.textColor = RGBA(65, 65, 65, 1);
        self.shopName.textAlignment = UITextAlignmentLeft;
        [shopView addSubview:self.shopName];
        
        self.bookType = [[UILabel alloc] initWithFrame:CGRectMake(10+150, 5, 80, 50)];
        self.bookType.font = [UIFont systemFontOfSize:14];
        self.bookType.textColor = RGBA(65, 65, 65, 1);
        self.bookType.textAlignment = UITextAlignmentLeft;
        [shopView addSubview:self.bookType];
        
        self.totalMoney = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH-70-10, 5, 70, 50)];
        self.totalMoney.font = [UIFont systemFontOfSize:14];
        self.totalMoney.textColor = RGBA(65, 65, 65, 1);
        self.totalMoney.textAlignment = UITextAlignmentRight;
        [shopView addSubview:self.totalMoney];
        
        line = [[UILabel alloc] initWithFrame:CGRectMake(10, 59, 300, 1)];
        line.backgroundColor = RGBA(244, 244, 244, 1);
        line.alpha = 0.5;
        [shopView addSubview:line];
        
        UIControl * control = [[UIControl alloc]initWithFrame:shopView.bounds];
        [control addTarget:self action:@selector(orderInfoAction) forControlEvents:UIControlEventTouchUpInside];
        [shopView addSubview:control];
        
        //
        payView  = [[UIView alloc] initWithFrame:CGRectMake(0, 35+60, 320, 50)];
        payView.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:payView];
        
        payStatusLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 50, 25)];
        payStatusLab.font = [UIFont systemFontOfSize:14];
        payStatusLab.textColor = [UIColor grayColor];
        payStatusLab.textAlignment = UITextAlignmentLeft;
        [payView addSubview:payStatusLab];
        
        self.cancellBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 160, 10, 70, 25)];
        [self.cancellBtn setTitle:@"取消" forState:UIControlStateNormal];
        [self.cancellBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        self.cancellBtn.layer.cornerRadius =4;
        self.cancellBtn.layer.borderColor = [UIColor lightGrayColor].CGColor;
        self.cancellBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.cancellBtn.layer.borderWidth =1;
        [self.cancellBtn addTarget:self action:@selector(cancelAction) forControlEvents:UIControlEventTouchUpInside];
        [payView addSubview: self.cancellBtn];
        
        self.payBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 10 - 70, 10, 70, 25)];
        [self.payBtn setTitle:@"去付款" forState:UIControlStateNormal];
        [self.payBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
        self.payBtn.layer.cornerRadius = 4;
        self.payBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.payBtn.layer.borderColor = RGBA(251, 53, 46, 1).CGColor;
        self.payBtn.layer.borderWidth =1;
        [self.payBtn addTarget:self action:@selector(payAction) forControlEvents:UIControlEventTouchUpInside];
        [payView addSubview:self.payBtn];
        
        self.reviewBtn = [[UIButton alloc] initWithFrame:CGRectMake(WIDTH - 10 - 70, 10, 70, 25)];
        [self.reviewBtn setTitle:@"去评价" forState:UIControlStateNormal];
        [self.reviewBtn setTitleColor:RGBA(251, 53, 46, 1) forState:UIControlStateNormal];
        [self.reviewBtn  setHidden:YES];
        [self.reviewBtn addTarget:self action:@selector(goToEvaluate) forControlEvents:UIControlEventTouchUpInside];
        self.reviewBtn.layer.cornerRadius = 4;
        self.reviewBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
        self.reviewBtn.layer.borderColor = RGBA(251, 53, 46, 1).CGColor;
        self.reviewBtn.layer.borderWidth =1;
        [payView addSubview:self.reviewBtn];
        
        line = [[UILabel alloc] initWithFrame:CGRectMake(0, 50, 320, 1)];
        line.backgroundColor = [UIColor lightGrayColor];
        //[payView addSubview:line];
        line.alpha = 0.5;
    }

}


#pragma mark - 设置数据
-(void)setCellData:(NSMutableDictionary *)dic{

    
    self.orderCode.text = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"orderCode"]];
    self.time.text =  [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"createdOn"]];
    self.shopName.text = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"shopName"]];
    self.bookType.text = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"bespeakTypeName"]];
    bespeakTypeName = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"bespeakTypeName"]];
    
    if (![dic safeObjectForKey:@"orderPrice"] || [[dic safeObjectForKey:@"orderPrice"] isKindOfClass:[NSNull class]])
    {
       self.totalMoney.hidden = YES;
    }
    else
    {
        self.totalMoney.text =[self filterParam:[NSString stringWithFormat:@"￥%@",[dic safeObjectForKey:@"orderPrice"]]];
        self.totalMoney.hidden = NO;
    }
    
    self.isPay = [[dic safeObjectForKey:@"isPay"] boolValue];
    payStatusLab.text = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"payStatusName"]];
    
    self.shopId = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"shopId"]];
    bespeakType = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"bespeakType"]];
    orderId = [NSString stringWithFormat:@"%@",[dic safeObjectForKey:@"id"]];
    
    //待消费
    if ([[dic safeObjectForKey:@"orderStatus"] isEqualToString:@"1"]) {
        
        self.reviewBtn.hidden = YES;
        
        if ([[dic safeObjectForKey:@"payStatus"] isEqualToString:@"1"]) {
            self.payBtn.hidden = YES;
            self.cancellBtn.frame = CGRectMake(10+130+100, 10, 70, 25);
            self.cancellBtn.hidden = YES;
            isHasPay = YES;
        }else{
            if ([bespeakTypeName isEqualToString:@"订座"])
            {
                self.payBtn.hidden = YES;
                self.cancellBtn.frame = CGRectMake(WIDTH - 10 - 70, 10, 70, 25);
            }
            else
            {
                self.payBtn.hidden = NO;
                self.cancellBtn.frame = CGRectMake(10+120, 10, 70, 25);
                
            }
            
            
            self.cancellBtn.hidden = NO;
            isHasPay = NO;
        }
    }
    
    //已消费
    if ([[dic safeObjectForKey:@"orderStatus"] isEqualToString:@"2"]) {
        self.payBtn.hidden = YES;
        self.cancellBtn.hidden = YES;
        self.reviewBtn.hidden = NO;
        
        [self.reviewBtn setTitle:[dic safeObjectForKey:@"isevaluateName"] forState:UIControlStateNormal];
        if ([[dic safeObjectForKey:@"isevaluateName"] isEqualToString:@"已评价"])
        {
            self.reviewBtn.layer.borderWidth = 0;
            [self.reviewBtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
        else
        {
            self.reviewBtn.layer.borderWidth = 1;
            [self.reviewBtn setTitleColor:RGBA(251, 53, 46, 1) forState:UIControlStateNormal];
        }
        
        if ([[dic objectForKey:@"isevaluate"] isEqualToString:@"1"]) {
            self.reviewBtn.enabled = NO;
        }else{
            self.reviewBtn.enabled =YES;
        }
    }

}


-(void)orderInfoAction{

    [_delagate orderInfoAction:orderId bookType:bespeakType AndHasPay:isHasPay BtypeName:bespeakTypeName];
}



-(void)goToEvaluate{
    
    [_delagate goToEvaluate:self.shopId andOrderId:orderId];
}

-(void)payAction{

    [_delagate payBtnAction:self.orderCode.text andOrderId:orderId];
}

-(void)cancelAction{
    
    [_delagate cancelBtnAction:orderId];
}

-(NSString *)filterParam:(NSString *)param{
    
    if (param) {
        return param;
    }
    return @"";
}

@end
