//
//  ZSTEComCDetailDelegate.h
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ZSTECCFoodInfoViewController;

@interface ZSTEComCDetailDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *detailFoodArr;
@property (strong,nonatomic) UITableView *detailTableView;
@property (weak,nonatomic) ZSTECCFoodInfoViewController *foodVC;

@end
