//
//  ZSTECCNoDataCell.h
//  EComC
//
//  Created by anqiu on 15/8/19.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTECCNoDataCell : UITableViewCell

@property (nonatomic,strong) UILabel *tip;

@end
