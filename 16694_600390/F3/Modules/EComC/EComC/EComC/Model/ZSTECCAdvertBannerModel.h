//
//  ZSTECCAdvertBannerModel.h
//  EComC
//
//  Created by anqiu on 15/8/25.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTECCAdvertBannerModel : NSObject

@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *carouselurl;
@property (nonatomic, strong) NSString *linkurl;

@end
