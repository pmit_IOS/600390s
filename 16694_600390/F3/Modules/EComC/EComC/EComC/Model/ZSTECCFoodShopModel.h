//
//  ZSTECCFoodShopModel.h
//  EComC
//
//  Created by anqiu on 15/8/25.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTECCFoodShopModel : NSObject

@property (nonatomic,strong) NSString *shopName;
@property (nonatomic,strong) NSString *shopLogo;
@property (nonatomic, strong) NSString *shopAddress;
@property (nonatomic,strong) NSString *latitude;
@property (nonatomic, strong) NSString *longtude;

@end
