//
//  ZSTECCShop.h
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTECCShop : NSObject

@end


//轮播图

@interface EcomCCarouseInfo : NSObject

@property (nonatomic,retain) NSString * ecomCCarouseInfoCarouselurl;
@property (nonatomic,retain) NSString * ecomCCarouseInfoTitle;
@property (nonatomic,retain) NSString * ecomCCarouseInfoLinkurl;
@property (nonatomic,assign) int ecomCCarouseInfoProductid;

+ (id)ecomCCarouseWithArray:(NSArray *) arr;
- (id)initWithDictionary:(NSDictionary *) dic;

@end