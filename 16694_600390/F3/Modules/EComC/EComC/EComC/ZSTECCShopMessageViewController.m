//
//  ZSTECCShopMessageViewController.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopMessageViewController.h"
#import "ZSTECCShopReviewsCell.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ZSTF3Engine.h"
#import "ZSTECCShopInfoAddressTableViewCell.h"
#import "ZSTECCImageViewer.h"
#import "MoreButton.h"
#import "ZSTECCMapViewController.h"

#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)

@interface ZSTECCShopMessageViewController()
{
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    NSInteger _pagenum;
    
    NSInteger starRate;
    
    int _currentPage;
    BOOL _hasMore;
    MoreButton *_moreButton;
    BOOL  isRefresh;
    BOOL _loading;  //是否正在加载中 待消费
    
    NSMutableArray *hasImageArra;
    
    
}


@property (strong,nonatomic) NSMutableArray *shopNameArr;
@property (strong,nonatomic) NSMutableArray *hasRefreshArr;
@property (assign,nonatomic) BOOL isLoadMore;
@property (strong,nonatomic) UILabel *confuTitleLB;

@end

#define imageStar(i) [[NSString stringWithFormat:@"200%d",i] intValue]

@implementation ZSTECCShopMessageViewController{
    
    UILabel *heightLable;
    UILabel *heightLableAdd;
    ZSTECCAutoLabel *heightShopIntro;
    ZSTECCAutoLabel *heightShopIntro1;
    ZSTECCAutoLabel *heightShopIntro2;
    
    
    NSMutableDictionary *dic;
    
    NSInteger labelHeight;
    NSInteger adHeight;
    NSInteger introHeight;
    
    UIImageView *starImage1;
    UIImageView *starImage2;
    UIImageView *starImage3;
    UIImageView *starImage4;
    UIImageView *starImage5;
    
    UIView * sectionView;
    
    NSMutableDictionary *shopInfoDic;
    
    BOOL btnDown;
    BOOL hasImage;
    
    NSString *introduceStr;
    NSString *str;
    
    ZSTECCAutoLabel *introduceLabel;
    UIButton *tempBtn;
    UIButton *tempBtnCover;
    UILabel *tableSectionLabel;
    UIView *tableSectionView;
    
    NSMutableArray *reviewArray;
    

}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate = self;
    self.engine = engine;
    [self initData];
    
    btnDown = NO;
    hasImage = NO;
    self.hasRefreshArr = [NSMutableArray array];
    self.shopNameArr = [NSMutableArray array];
    _currentPage = 1;

    self.view.frame = CGRectMake(0, 47, WIDTH, HEIGHT - 64 - 47);
    
    
    self.reviewArray = [[NSMutableArray alloc] initWithCapacity:10];
    
    introduceStr =@"";
    //初始化商铺介绍高度
    
     [TKUIUtil showHUD:self.view withText:NSLocalizedString(@"正在加载..." , nil)];
    //获取店铺信息
    [self.engine getShopDetailByShopId:self.shopId];
    
    
}

#pragma mark - 满意度 图标
-(void)initStars:(NSInteger)level{
    
    sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    sectionView.backgroundColor = [UIColor whiteColor];
    starImage1 = [[UIImageView alloc] initWithFrame:CGRectMake(110+5, 15, 20, 20)];
    starImage1.tag = 2000;
    [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    [sectionView addSubview:starImage1];
    
    starImage2 = [[UIImageView alloc] initWithFrame:CGRectMake(110+30*1, 15, 20, 20)];
    starImage2.tag = 2001;
    [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    [sectionView addSubview:starImage2];
    
    starImage3 = [[UIImageView alloc] initWithFrame:CGRectMake(105+30*2, 15, 20, 20)];
    [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    starImage3.tag = 2002;
    [sectionView addSubview:starImage3];
    
    starImage4 = [[UIImageView alloc] initWithFrame:CGRectMake(100+30*3, 15, 20, 20)];
    [starImage4 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];;
    starImage4.tag = 2003;
    [sectionView addSubview:starImage4];
    
    starImage5 = [[UIImageView alloc] initWithFrame:CGRectMake(95+30*4, 15, 20, 20)];
    [starImage5 setImage:[UIImage imageNamed:@"model_ecomc_big_starGray.png"]];
    starImage5.tag = 2004;
    [sectionView addSubview:starImage5];
    
    if (level == 1) {
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 2){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 3){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 4){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage4 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }else if (level == 5){
        [starImage1 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage2 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage3 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage4 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
        [starImage5 setImage:[UIImage imageNamed:@"model_ecomc_big_star.png"]];
    }
    
    UILabel *levelLabel = [[UILabel alloc] initWithFrame:CGRectMake(100+30*4+25, 15, 40, 25)];
    levelLabel.text =[NSString stringWithFormat:@"%ld.0",(long)level];
    levelLabel.font = [UIFont systemFontOfSize:19];
    levelLabel.textColor = [UIColor lightGrayColor];
    [sectionView addSubview:levelLabel];
    
    heightLable= [[UILabel alloc] init];
    heightLable.lineBreakMode = UILineBreakModeWordWrap;
    heightLable.numberOfLines = 0;
    heightLable.font = [UIFont systemFontOfSize:14];
}

- (void)buildTableView
{
    self.shopMessageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, self.view.frame.size.height) style:UITableViewStylePlain];
    self.shopMessageTableView.delegate = self;
    self.shopMessageTableView.dataSource = self;
    self.shopMessageTableView.backgroundColor = RGBA(244, 244, 244, 1);
    self.shopMessageTableView.separatorStyle = UITableViewCellAccessoryNone;
    [self.view addSubview:self.shopMessageTableView];
    
    if (_refreshHeaderView == nil) {
        
       	EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.shopMessageTableView.bounds.size.height, self.view.frame.size.width, self.shopMessageTableView.bounds.size.height)];
        view.delegate = self;
        [self.shopMessageTableView addSubview:view];
        _refreshHeaderView = view;
    }
    
    [_refreshHeaderView refreshLastUpdatedDate];

}

- (void)buildTableViewHeader
{
    [self.shopMessageTableView setTableHeaderView:_carouselView];
    self.shopMessageTableView.tableHeaderView.backgroundColor = RGBACOLOR(224, 224, 224, 1);
}

-(void)initData
{
    
    _hasMore = NO;
    
    _carouselView = [[ZSTECCCarouselView alloc]initWithFrame:CGRectMake(0, 0, 320, 160)];
    _carouselView.carouselDataSource = self;
    _carouselView.carouselDelegate = self;
    _carouselView.haveTitle = NO;
    [_carouselView hideStateBar];
    [_carouselView pageControlState:pageControlStateRIGHT];
    
    _carouseData = nil;
    
    //[self.view addSubview:_carouselView];
    //轮播图获取
    //[self.engine getEcomCCarousel];
    
}


#pragma mark - 数据源
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }else if (section == 1){
        return 1;
    }
    
    if (_hasMore) {
        return self.reviewArray.count + 1;
    }
    
    return self.reviewArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0){
        
        return  50+adHeight;
    }else if (indexPath.section == 1){
        
        if ([introduceStr isEqualToString:@""]) {
            return 10; //无介绍时
        }else{
            return  30+introHeight+10;
        }
    
    }
    
    //无数据时
    if (self.reviewArray.count == 0 ) {
        return 0;
    }
    
    if (indexPath.row == self.reviewArray.count) {
        return 44.0f;
    }
    

    //无图片
    if (IS_IOS7 && !IS_IOS8) {
        
        dic = [[NSMutableDictionary alloc] init];
        dic = [self.reviewArray objectAtIndex:indexPath.row];
        NSArray *imageArray = [dic objectForKey:@"images"];
        UILabel *evaluateLab = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 320, 20)];
        evaluateLab.lineBreakMode = NSLineBreakByWordWrapping;
        evaluateLab.numberOfLines = 0;
        evaluateLab.font = [UIFont systemFontOfSize:14];
        evaluateLab.text = [dic objectForKey:@"evaluateDesc"];
        CGSize reviewLabSize = [evaluateLab.text sizeWithFont:evaluateLab.font constrainedToSize:CGSizeMake(210.f, MAXFLOAT)  lineBreakMode:evaluateLab.lineBreakMode];
        labelHeight = reviewLabSize.height;
        if (labelHeight < 50) {
            labelHeight = 30;
        }
        if (imageArray.count == 0)
        {
            return 10+70+labelHeight;
        }
        return 10+70+labelHeight+55;
    }
    else
    {
        if (!hasImage) {
            return 10+70+labelHeight;
        }
        else
        {
            return 10+70+labelHeight+55;
        }
    }
    
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        static NSString *defualts2 = @"ZSTECCShopInfoAddressTableViewCell";
        ZSTECCShopInfoAddressTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:defualts2];
        
        if (!cell) {
            cell = [[ZSTECCShopInfoAddressTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:defualts2];
            [cell createUI];
           
        }
         cell.delegate = self ;
        [cell setCellData:shopInfoDic];
        cell.height = adHeight;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UITapGestureRecognizer *mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToMap:)];
        [cell addGestureRecognizer:mapTap];
        
        return  cell;
    }
    
    
    if(indexPath.section ==1){
        
        static NSString *defualts1 = @"defualts1";
        UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:defualts1];
        
        UILabel *line;
        
        if (!cell) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:defualts1];
            
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.contentView.backgroundColor = RGBACOLOR(224, 224, 224,0.5);
            tableSectionView = [[UIView alloc] init];
            tableSectionView.backgroundColor = [UIColor whiteColor];
            [cell.contentView addSubview:tableSectionView];
            
            line = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
            line.backgroundColor = [UIColor lightGrayColor];
            line.alpha =0.5;
            //[tableSectionView addSubview:line];
            
            tableSectionLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 70, 30)];
            tableSectionLabel.text = @"商家介绍:";
            tableSectionLabel.font = [UIFont systemFontOfSize:14];
            tableSectionLabel.textColor = [UIColor grayColor];
            [tableSectionView addSubview:tableSectionLabel];
            
            introduceLabel = [[ZSTECCAutoLabel alloc] init];
            introduceLabel.textColor = [UIColor grayColor];
            introduceLabel.font = [UIFont systemFontOfSize:14];
            [tableSectionView addSubview:introduceLabel];
            
            tempBtn = [[UIButton alloc] init];
            [cell.contentView addSubview:tempBtn];
            
            tempBtnCover = [[UIButton alloc] init];
            [tempBtnCover addTarget:self action:@selector(tapAction:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:tempBtnCover];
        }
        
        if ([introduceStr isEqualToString:@""]) {
            tableSectionLabel.hidden = YES;
            line.hidden = YES;
            tableSectionView.frame = CGRectMake(0, 0, 320, 0);
        }else{
            tableSectionView.frame = CGRectMake(0, 0, 320, introHeight+10+20);
        }
        
        introduceLabel.frame = CGRectMake(10+70, 6, 230, introHeight);
        introduceLabel.text =introduceStr;
        tempBtn.frame = CGRectMake(150,introHeight+10, 15, 15);
        tempBtnCover.frame = CGRectMake(130,introHeight+10, 60, 20);
        
        if (btnDown) {
            [tempBtn setBackgroundImage:[UIImage imageNamed:@"model_ecomc_up.png"] forState:UIControlStateNormal];
        }else{
            [tempBtn setBackgroundImage:[UIImage imageNamed:@"model_ecomc_down.png"] forState:UIControlStateNormal];
        }
        
        if ([introduceStr isEqualToString:@""])
        {
            tempBtn.hidden = YES;
            tempBtnCover.hidden = YES;
        }
        else
        {
            tempBtn.hidden = NO;
            tempBtnCover.hidden = NO;
        }
        
        return  cell;
    }
    
    //评论
    if(indexPath.section == 2){
        //更多数据
        static NSString *moreCellIdentifier = @"MoreCell";
        if (indexPath.row == self.reviewArray.count && _hasMore)
        {
            UITableViewCell *moreCell = [tableView dequeueReusableCellWithIdentifier:moreCellIdentifier];
            if (moreCell == nil)
            {
                moreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreCellIdentifier];
                _moreButton = [MoreButton button];
                [_moreButton addTarget:self action:@selector(loadMoreData) forControlEvents:UIControlEventTouchUpInside];
                [moreCell.contentView addSubview:_moreButton];
            }
            
            return moreCell;
        }

        static NSString  *shopMessageCell = @"shopMessageCell";
        ZSTECCShopReviewsCell *cell = [tableView dequeueReusableCellWithIdentifier:shopMessageCell];
        if (!cell) {
            cell = [[ZSTECCShopReviewsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:shopMessageCell];
        }
        //[cell createUI];
        cell.delegate = self;

        [cell setCellData:[self.reviewArray objectAtIndex:indexPath.row]];
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return  cell;
    }
    
    
    return nil;
    
}


//-(void)loadMoreData
//{
//    [self.engine getShopReviewListByShopId:self.shopId andCurPage:[NSString stringWithFormat:@"%ld",self.currentPage] andPageSize:[NSString stringWithFormat:@"%ld",self.pageSize]];
//}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section == 2) {
        // NSString *sectionTitle = [self tableView:tableView titleForHeaderInSection:section];
        if (!self.confuTitleLB)
        {
            self.confuTitleLB = [[UILabel alloc] init];
            self.confuTitleLB.text = @"商家满意度";
            self.confuTitleLB.textColor = [UIColor grayColor];
            self.confuTitleLB.frame = CGRectMake(10, 10, 120, 30);
            self.confuTitleLB.font=[UIFont fontWithName:@"Arial" size:15];
            
            [sectionView addSubview:self.confuTitleLB];
        }
       
        
        return sectionView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    
    if (section == 2) {
        return 50;
    }
    return 0;
}


//商家满意度
-(void)starImage:(int)num{
    
    for (int i=0; i<=num; i++) {
        [self initStar:num forEeach:i];
    }
    
}


-(void)initStar:(int)tag forEeach:(int)i{
    
    UIImageView *starImage = (UIImageView *)[self.view viewWithTag:imageStar(i)];
    
    [starImage setImage:[UIImage imageNamed:@"model_ecomc_star.png"]];
    
}


- (void)getEcomCCarouselDidSucceed:(NSArray *)carouseles
{
    _carouseData = nil;
    _carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:carouseles];
    if (_carouseData.count>0) {
        [_carouselView reloadData];
    }
}
- (void)getEcomCCarouselDidFailed:(int)resultCode
{
    [TKUIUtil alertInView:self.view withTitle:@"广告暂无数据！" withImage:nil];
}

#pragma mark - ZSTEComBCarouselViewDataSource
- (NSInteger)numberOfViewsInCarouselView:(ZSTECCCarouselView *)newsCarouselView;
{
    return [_carouseData count];
}

- (EcomCCarouseInfo *)carouselView:(ZSTECCCarouselView *)carouselView infoForViewAtIndex:(NSInteger)index
{
    
    if ([_carouseData  count] != 0) {
        return [_carouseData  objectAtIndex:index];
    }
    return nil;
}

#pragma mark - ZSTEComBCarouselViewDelegate
- (void)carouselView:(ZSTECCCarouselView *)carouselView didSelectedViewAtIndex:(NSInteger)index;
{
    //NSString *urlPath = [[_carouseData objectAtIndex:index] ecomBCarouseInfoLinkurl];
    //    if (urlPath == nil || [urlPath length] == 0 || [urlPath isEmptyOrWhitespace]) {
    //        return;
    //    }
    //    EComCHomePageInfo * pageInfo = [[EComCHomePageInfo alloc]init];
    //    pageInfo.ecombHomeProductid = [NSString stringWithFormat:@"%d",[[_carouseData objectAtIndex:index] ecomBCarouseInfoProductid]];
    //
    //    //    ZSTEComBGoodsShowViewController * showViewcontroller = [[ZSTEComBGoodsShowViewController alloc]init];
    //    ZSTGoodsShowViewController *showViewcontroller = [[ZSTGoodsShowViewController alloc] init];
    //    showViewcontroller.pageInfo = pageInfo;
    //    showViewcontroller.hidesBottomBarWhenPushed = YES;
    //    [self.navigationController pushViewController:showViewcontroller animated:YES];
    //    [showViewcontroller release];
    
    //
    //    ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
    //    [webViewController setURL:urlPath];
    //    webViewController.hidesBottomBarWhenPushed = YES;
    //    webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    //
    //    [self.navigationController pushViewController:webViewController animated:NO];
    //    [webViewController release];
}

-(void)callPhone:(NSString *)phone{
 
    UIWebView*callWebview =[[UIWebView alloc] init];
    NSURL *telURL =[NSURL URLWithString:[NSString stringWithFormat:@"tel:%@",phone]];
    [callWebview loadRequest:[NSURLRequest requestWithURL:telURL]];
    [self.view addSubview:callWebview];
}

#pragma mark - 商铺介绍扩展
-(void)tapAction:(UIButton *)sender{
    
    if (!btnDown) {
        heightShopIntro1 = [[ZSTECCAutoLabel alloc] init];
        heightShopIntro1.font = [UIFont systemFontOfSize:14];
        heightShopIntro1.text = introduceStr;
        introHeight = [heightShopIntro1 getAttributedStringHeightWidthValue:230];
        btnDown = YES;
    }else{
        
        if ([introduceStr isEqualToString:@""]) {
            introHeight = 0;
        }else{
            introHeight = 37;
        }
        
        btnDown = NO;
    }
    [self.shopMessageTableView reloadData];
}

#pragma mark - 店铺信息
-(void)getShopDetailDidSucceed:(NSDictionary *)shopDetail{
    
    [TKUIUtil hiddenHUD];
    if (shopDetail.count == 0) {
        return;
    }
    
    self.lat = [shopDetail safeObjectForKey:@"lat"];
    self.lng = [shopDetail safeObjectForKey:@"lng"];
    
    //轮播图
    _carouseData = nil;
    _carouseData = [EcomCCarouseInfo ecomCCarouseWithArray:(NSArray *)[shopDetail objectForKey:@"banners"]];
    if (_carouseData.count>0) {
        [_carouselView reloadData];
    }
    
    // 店铺信息
    shopInfoDic = [[NSMutableDictionary alloc] init];
    [shopInfoDic setValue:[shopDetail safeObjectForKey:@"businessTime"] forKey:@"businessTime"];
    [shopInfoDic setValue:[shopDetail safeObjectForKey:@"shopAddress"] forKey:@"shopAddress"];
    [shopInfoDic setValue:[shopDetail safeObjectForKey:@"shopTel"] forKey:@"shopTel"];
    
    heightLableAdd = [[UILabel  alloc] init];
    heightLableAdd.lineBreakMode = NSLineBreakByWordWrapping;
    heightLableAdd.numberOfLines = 0;
    heightLableAdd.font = [UIFont systemFontOfSize:14];
    heightLableAdd.text = [shopDetail safeObjectForKey:@"shopAddress"];
    
    NSString *timeStr = [shopInfoDic safeObjectForKey:@"businessTime"];
    
    CGSize timeLabSize = [timeStr sizeWithFont:heightLableAdd.font constrainedToSize:CGSizeMake(230.f, MAXFLOAT)  lineBreakMode:heightLableAdd.lineBreakMode];
    adHeight = timeLabSize.height;
    
    NSString *addStr = [shopDetail safeObjectForKey:@"shopAddress"];

    CGSize addLabSize = [addStr sizeWithFont:heightLableAdd.font constrainedToSize:CGSizeMake(230.f, MAXFLOAT)  lineBreakMode:heightLableAdd.lineBreakMode];
    adHeight += addLabSize.height;
    
    
    //商家介绍
    introduceStr = [NSString stringWithFormat:@"%@",[shopDetail safeObjectForKey:@"shopDesc"]];
    introHeight = 37;

    [self initStars:[[shopDetail safeObjectForKey:@"starRate"] integerValue]];

    [self buildTableView];
    [self buildTableViewHeader];
    
    
    //评价列表
    [self.engine getShopReviewListByShopId:self.shopId andCurPage:[NSString stringWithFormat:@"%d",_currentPage] andPageSize:@"10"];
    
}

-(void)getShopDetailDidFailed:(int)resultCode{
    NSLog(@"====shopDetail=  失败===>");
    
    
}

#pragma mark - 店铺评价列表
-(void)getShopReviewListDidSucceed:(NSDictionary *)shopReview{
    
    [TKUIUtil hiddenHUD];
    
    if (_currentPage >= [[shopReview safeObjectForKey:@"totalPage"] intValue]) {
        _hasMore = NO;
    }else{
        _hasMore = YES;
    }
    
    if (isRefresh) {
        _currentPage = 1;
        [self.reviewArray addObject:[shopReview safeObjectForKey:@"items"]];

    
    } else {
      
        _currentPage++;
        [self.reviewArray addObjectsFromArray:[shopReview safeObjectForKey:@"items"]];
    }
    
    [self doneLoadingData];
    
}

-(void)getShopReviewListDidFailed:(int)resultCode{
    
    NSLog(@"====店铺评价列表失败===>");
}


//获取cell高度
-(void)passHeight:(NSInteger)height andImageNum:(BOOL)image{

    labelHeight = height;
    hasImage = image;
}

//图片浏览
-(void)clickImage:(UIImageView *)button imageArray:(NSArray *)imageArray{
    
    //    ZSTECCImageViewer *imageView = [[ZSTECCImageViewer alloc] init];
    //    imageView.imageArray = imageArray;
    //    [self.navigationController pushViewController:imageView animated:YES];
    
}

- (void)loadDataForPage:(int)pageIndex
{
    if (pageIndex == 1) {
        _currentPage = 1;
        
        [self.engine getShopReviewListByShopId:self.shopId andCurPage:[NSString stringWithFormat:@"%d",_currentPage] andPageSize:@"10"];
        
        isRefresh = YES;
    }else if (pageIndex > 1){;
        
        [self.engine getShopReviewListByShopId:self.shopId andCurPage:[NSString stringWithFormat:@"%d",_currentPage] andPageSize:@"10"];
        
        isRefresh = NO;
    }
}


//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
    _loading = YES;
    _currentPage = 1;
    [self loadDataForPage:_currentPage];
}

- (void)loadMoreData
{
    
    if (_hasMore)
    {
        _loading = YES;
        [_moreButton displayIndicator];
        
        [self loadDataForPage:_currentPage+1];
    }
    
}

- (void)doneLoadingData
{
    _loading = NO;
    
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.shopMessageTableView];
    
    if (self.reviewArray.count != 0) {
        [self.shopMessageTableView reloadData];
    }
}


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    //[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
     return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

- (void)dealloc
{
    [self.reviewArray removeAllObjects];
    [self.engine cancelAllRequest];
}

- (void)goToMap:(UITapGestureRecognizer *)tap
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在进入地图导航,请稍后...", nil) withImage:nil];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [param setValue:self.lat forKey:@"lat"];
    [param setValue:self.lng forKey:@"lng"];
    [param setValue:[shopInfoDic safeObjectForKey:@"shopAddress"] forKey:@"address"];
    [self.engine getMapWithDictionary:param];
}

- (void)getMapSuccess:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    NSString *urlString = [response safeObjectForKey:@"url"];
    ZSTECCMapViewController *mapVC = [[ZSTECCMapViewController alloc] init];
    mapVC.urlString = urlString;
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];
    
}

- (void)getMapFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
}

@end
