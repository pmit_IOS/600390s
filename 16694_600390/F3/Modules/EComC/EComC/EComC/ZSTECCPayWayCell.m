//
//  ZSTECCPayWayCell.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCPayWayCell.h"

@implementation ZSTECCPayWayCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUIWithIsTitle:(BOOL)isTitle
{
    if (isTitle)
    {
        if (!self.payTitleLB)
        {
            self.payTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - 20, 20)];
            self.payTitleLB.text = @"支付方式";
            self.payTitleLB.textColor = RGBA(85, 85, 85, 1);
            self.payTitleLB.textAlignment = NSTextAlignmentLeft;
            self.payTitleLB.font = [UIFont systemFontOfSize:14.0f];
            [self.contentView addSubview:self.payTitleLB];
            
        }
    }
    else
    {
        if (!self.payWayIV)
        {
            self.payWayIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 80, 30)];
            self.payWayIV.contentMode = UIViewContentModeScaleAspectFit;
            [self.contentView addSubview:self.payWayIV];
            
            self.checkIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 20 - 15, 10, 20, 20)];
            self.checkIV.contentMode = UIViewContentModeScaleAspectFit;
            self.checkIV.hidden = YES;
            self.checkIV.image = [UIImage imageNamed:@"ok.png"];
            [self.contentView addSubview:self.checkIV];
        }
    }
}

- (void)setCellDataWithPayWay:(BOOL)isWeChat
{
    if (isWeChat)
    {
        self.payWayIV.image = [UIImage imageNamed:@"wechatPay.png"];
    }
    else
    {
        self.payWayIV.image = [UIImage imageNamed:@"alipay.png"];
    }
}


@end
