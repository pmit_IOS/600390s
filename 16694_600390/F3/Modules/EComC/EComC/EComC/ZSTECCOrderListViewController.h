//
//  ZSTECCOrderListViewController.h
//  EComC
//
//  Created by qiuguian on 8/10/15.
//  Copyright (c) 2015 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECCOrderListCell.h"
#import "ZSTF3Engine+EComC.h"


@interface ZSTECCOrderListViewController : UIViewController<UITableViewDataSource,UITableViewDelegate,ZSTECCOrderListCellDelegate,ZSTF3EngineECCDelegate,EGORefreshTableHeaderDelegate>{
   
    EGORefreshTableHeaderView *_refreshHeaderView;
}

@property (nonatomic,strong) UITableView *orderTableView;
@property (nonatomic, strong) NSMutableArray *dataArray; //待消费
@property (nonatomic,strong) NSMutableArray *dataArrayed; //已消费
@property (nonatomic,strong) ZSTF3Engine *engine;
@property (nonatomic,assign) BOOL isFromHome;

@end
