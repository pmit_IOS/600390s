//
//  ZSTECCShoppingCarDelegate.h
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@class ZSTECCFoodInfoViewController;

@interface ZSTECCShoppingCarDelegate : NSObject <UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSArray *shoppingCarArr;
@property (weak,nonatomic) ZSTECCFoodInfoViewController *foodVC;
@property (strong,nonatomic) UITableView *shoppingCarTableView;

@end
