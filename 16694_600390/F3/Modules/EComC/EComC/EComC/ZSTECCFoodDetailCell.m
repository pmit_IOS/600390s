//
//  ZSTECCFoodDetailCell.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCFoodDetailCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTECCFoodDetailCell

- (void)createUI
{
    if (!self.foodIV)
    {
        self.foodIV = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 50, 50)];
        self.foodIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.foodIV];
        
        self.foodTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(70, 10, WIDTH * 0.7 - 70 - 10, 20)];
        self.foodTitleLB.textColor = RGBA(85, 85, 85, 1);
        self.foodTitleLB.font = [UIFont systemFontOfSize:14.0f];
        self.foodTitleLB.numberOfLines = 0;
        self.foodTitleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.foodTitleLB];
        
        self.minusBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(WidthRate(250), 70, 25, 25)];
        [self.minusBtn setImage:[UIImage imageNamed:@"minus.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.minusBtn];
        
        self.numLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(250) + 25, 70, WidthRate(170) - 25, 25)];
        self.numLB.textAlignment = NSTextAlignmentCenter;
        self.numLB.font = [UIFont systemFontOfSize:14.0f];
        self.numLB.textColor = RGBA(85, 85, 85, 1);
        self.numLB.text = @"0";
        [self.contentView addSubview:self.numLB];
        
        self.plusBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(WidthRate(420), 70, 25, 25)];
        [self.plusBtn setImage:[UIImage imageNamed:@"plus.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.plusBtn];
        
        self.priceLB = [[UILabel alloc] initWithFrame:CGRectMake(10, 70, WidthRate(420) - 10, 20)];
        self.priceLB.font = [UIFont systemFontOfSize:12.0f];
        self.priceLB.textColor = RGBA(187, 187, 187, 1);
        [self.contentView addSubview:self.priceLB];
        
    }
}

- (void)getCellData:(NSDictionary *)foodDetailDic
{
    [self.foodIV setImageWithURL:[NSURL URLWithString:[foodDetailDic safeObjectForKey:@"attachUrl"]] placeholderImage:[UIImage imageNamed:@"module_personal_avater_deafaut.png"]];
    NSString *foodName = [foodDetailDic safeObjectForKey:@"dishesName"];
    CGSize nameSize = [foodName sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WIDTH * 0.7 - 80, MAXFLOAT)];
    self.foodTitleLB.frame = CGRectMake(70, 10, WIDTH * 0.7 - 80, nameSize.height);
    self.foodTitleLB.text = [foodDetailDic safeObjectForKey:@"dishesName"];
    NSString *priceString = [NSString stringWithFormat:@"%.2lf元/份",[[foodDetailDic safeObjectForKey:@"price"] doubleValue]];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    NSInteger location = [priceString rangeOfString:@"元"].location;
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f],UITextAttributeTextColor:RGBA(254, 77, 61, 1)} range:NSMakeRange(0, location)];
    self.priceLB.attributedText = nodeString;
    BOOL isHas = NO;
    NSString *dishId = [foodDetailDic safeObjectForKey:@"dishId"];
    for (NSInteger i = 0; i < self.selectedArr.count; i++)
    {
        NSDictionary *oneDic = self.selectedArr[i];
        if ([[[oneDic allKeys] firstObject] isEqualToString:dishId])
        {
            isHas = YES;
            NSInteger count = 0;
            NSArray *cateArr = [oneDic safeObjectForKey:dishId];
            for (NSDictionary *cateDic in cateArr)
            {
                count += [[cateDic safeObjectForKey:@"dishCount"] integerValue];
            }
            
            self.numLB.text = [NSString stringWithFormat:@"%@",@(count)];
            break;
        }
    }
    
    if (isHas)
    {
        self.numLB.hidden = NO;
        self.minusBtn.hidden = NO;
    }
    else
    {
        self.numLB.hidden = YES;
        self.minusBtn.hidden = YES;
    }
    
}

@end
