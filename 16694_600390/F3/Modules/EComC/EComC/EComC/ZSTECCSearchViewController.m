//
//  ZSTECCSearchViewController.m
//  EComC
//
//  Created by pmit on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCSearchViewController.h"
#import "ZSTECCSearchResultDelegate.h"
#import "ZSTECCShopListCell.h"
#import "ZSTECCMapViewController.h"
#import "ZSTECCReservationViewComtroller.h"
#import "MoreButton.h"

@interface ZSTECCSearchViewController () <UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,ZSTECCShopListCellDelegate,ZSTECCCSearchDelegate>

@property (strong,nonatomic) UITableView *searchHistoryTableView;
@property (strong,nonatomic) NSMutableArray *historyArr;
@property (strong,nonatomic) UITableView *searchResultTableView;
@property (strong,nonatomic) ZSTECCSearchResultDelegate *searchDelegate;
@property (assign,nonatomic) NSInteger currentPage;
@property (assign,nonatomic) NSInteger totalPage;
@property (strong,nonatomic) NSMutableArray *shopNameArr;
@property (assign,nonatomic) BOOL hasMore;
@property (assign,nonatomic) BOOL isRefresh;
@property (assign,nonatomic) BOOL loading;
@property (strong,nonatomic) MoreButton *moreButton;
@property (strong,nonatomic) UIView *historyView;
@property (assign,nonatomic) BOOL isNewSearch;
@property (strong,nonatomic) UIButton *cancelBtn;

@end

@implementation ZSTECCSearchViewController{

//    int _currentPage;
//    BOOL _hasMore;
//    MoreButton *_moreButton;
//    BOOL  isRefresh;
//    BOOL _loading;  //是否正在加载中 待消费
    NSString *searchKeyStr;
    BOOL isLoadMore;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildSearchTextView];
    self.view.backgroundColor = RGBA(244, 244, 244, 1);
    self.searchDelegate = [[ZSTECCSearchResultDelegate alloc] init];
    self.searchDelegate.searchHistoryDelegate = self;
    [self buildSearchResultTableView];
    [self buildHistoryView];
    [self buildSearchHistoryTableView];
    self.shopNameArr = [NSMutableArray array];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    [self.searchTF becomeFirstResponder];
    self.currentPage = 1;
    isLoadMore = NO;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"history"] || [[[NSUserDefaults standardUserDefaults] objectForKey:@"history"] isKindOfClass:[NSNull class]])
    {
        self.historyArr = [NSMutableArray array];
        self.searchHistoryTableView.hidden = YES;
    }
    else
    {
        self.historyArr = [[[NSUserDefaults standardUserDefaults] objectForKey:@"history"] mutableCopy];
        
        if (self.historyArr.count == 0)
        {
            self.historyArr = [NSMutableArray array];
            self.searchHistoryTableView.hidden = YES;
        }
        else
        {
            self.searchDelegate.resultArr = self.historyArr;
            self.searchHistoryTableView.hidden = NO;
        }
        
        [self.searchHistoryTableView reloadData];
    }
    
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildSearchTextView
{
    self.searchTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(105), 7, WIDTH - WidthRate(250), 30)];
    [self.searchTF setBorderStyle:UITextBorderStyleRoundedRect];
    //搜索框背景色
    [self.searchTF setBackgroundColor:[UIColor whiteColor]];
    [self.searchTF.layer setCornerRadius:5.0];
    [self.searchTF.layer setBorderWidth:0];
    self.searchTF.placeholder = @"请输入店铺名称";
    [self.searchTF setValue:[UIFont boldSystemFontOfSize:11] forKeyPath:@"_placeholderLabel.font"];
    [self.searchTF setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, self.searchTF.bounds.size.width, self.searchTF.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [self.searchTF setValue:RGBA(160,160,160,1) forKeyPath:@"_placeholderLabel.textColor"];
    
    self.searchTF.returnKeyType = UIReturnKeySearch;
    self.searchTF.textColor = [UIColor darkGrayColor];
    self.searchTF.font = [UIFont systemFontOfSize:12];
    self.searchTF.delegate = self;
    
    UIImage * image = [UIImage imageNamed:@"search.png"];
    
    UIImageView *searchIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 20)];
    searchIcon.image = image;
    searchIcon.contentMode = UIViewContentModeScaleAspectFit;
    
    self.searchTF.leftView = searchIcon;
    self.searchTF.leftViewMode = UITextFieldViewModeAlways;
    self.navigationItem.titleView = self.searchTF;
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (resignBack)];
    self.cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    self.cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.cancelBtn addTarget:self action:@selector(cancelSelected) forControlEvents:UIControlEventTouchUpInside];
    self.cancelBtn.frame = CGRectMake(0, 0, 40, 20);
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.cancelBtn];
}

- (void)buildHistoryView
{
    self.historyView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64)];
    self.historyView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.view addSubview:self.historyView];
}

- (void)buildSearchHistoryTableView
{
    self.searchHistoryTableView = [[UITableView alloc] initWithFrame:CGRectMake(20, 20, WIDTH - 40, HEIGHT - 64 - 20) style:UITableViewStylePlain];
    self.searchHistoryTableView.delegate = self.searchDelegate;
    self.searchHistoryTableView.dataSource = self.searchDelegate;
    self.searchHistoryTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.historyView addSubview:self.searchHistoryTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    self.searchHistoryTableView.tableFooterView = footerView;
}

- (void)buildSearchResultTableView
{
    self.searchResultTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.searchResultTableView.delegate = self;
    self.searchResultTableView.dataSource = self;
    self.searchResultTableView.backgroundColor = RGBA(244, 244, 244, 1);
//    [self.searchResultTableView registerClass:[ZSTECCShopListCell class] forCellReuseIdentifier:@"resultCell"];
    self.searchResultTableView.separatorStyle = UITableViewCellSelectionStyleNone;
    [self.view addSubview:self.searchResultTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
//    return self.historyArr.count;
    if (_hasMore) {
        return self.shopNameArr.count + 1;
    }
    return self.shopNameArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    //是否还有数据
    static NSString *moreCellIdentifier = @"MoreCell";
    if (indexPath.row == self.shopNameArr.count && _hasMore)
    {
        UITableViewCell *moreCell = [tableView dequeueReusableCellWithIdentifier:moreCellIdentifier];
        if (moreCell == nil)
        {
            moreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:moreCellIdentifier];
            _moreButton = [MoreButton button];
            [_moreButton setTitle:@"点击加载更多" forState:UIControlStateNormal];
            [_moreButton addTarget:self action:@selector(loadMoreData) forControlEvents:UIControlEventTouchUpInside];
            [moreCell.contentView addSubview:_moreButton];
        }
        
        return moreCell;
    }

    
    ZSTECCShopListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"resultCell"];
    if (!cell)
    {
        cell = [[ZSTECCShopListCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"resultCell"];
    }
    cell.delegate = self;
    [cell setCellData:self.shopNameArr[indexPath.row]];
    cell.startTitleLB.tag = indexPath.row;
    cell.startTitleLB.userInteractionEnabled = YES;
    UITapGestureRecognizer *mapTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToMap:)];
    [cell.startTitleLB addGestureRecognizer:mapTap];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.shopNameArr.count == indexPath.row) {
        return 44.0f;
    }
    
    return 255.0f;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.searchTF.text = self.historyArr[indexPath.row];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ([self isContainsEmoji:string])
    {
        return NO;
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""])
    {
        [textField resignFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        self.isNewSearch = YES;
        self.historyView.frame = CGRectMake(0, HEIGHT, self.historyView.bounds.size.width, self.historyView.bounds.size.height);
        NSString *searchString = textField.text;
        BOOL isHasSame = NO;
        for (NSString *historyString in self.historyArr)
        {
            if ([historyString isEqualToString:searchString])
            {
                isHasSame = YES;
                break;
            }
        }
        
        if (!isHasSame)
        {
            [self.historyArr addObject:searchString];
            [[NSUserDefaults standardUserDefaults] setValue:self.historyArr forKey:@"history"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
        [self searchWithString:textField.text];
    }
    
    return YES;
}

- (BOOL)isContainsEmoji:(NSString *)string {
    __block BOOL isEomji = NO;
    [string enumerateSubstringsInRange:NSMakeRange(0, [string length]) options:NSStringEnumerationByComposedCharacterSequences usingBlock:
     ^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
         const unichar hs = [substring characterAtIndex:0];
         if (0xd800 <= hs && hs <= 0xdbff) {
             if (substring.length > 1) {
                 const unichar ls = [substring characterAtIndex:1];
                 const int uc = ((hs - 0xd800) * 0x400) + (ls - 0xdc00) + 0x10000;
                 if (0x1d000 <= uc && uc <= 0x1f77f) {
                     isEomji = YES;
                 }
             }
         } else if (substring.length > 1) {
             const unichar ls = [substring characterAtIndex:1];
             if (ls == 0x20e3) {
                 isEomji = YES;
             }
         } else {
             if (0x2100 <= hs && hs <= 0x27ff && hs != 0x263b) {
                 isEomji = YES;
             } else if (0x2B05 <= hs && hs <= 0x2b07) {
                 isEomji = YES;
             } else if (0x2934 <= hs && hs <= 0x2935) {
                 isEomji = YES;
             } else if (0x3297 <= hs && hs <= 0x3299) {
                 isEomji = YES;
             } else if (hs == 0xa9 || hs == 0xae || hs == 0x303d || hs == 0x3030 || hs == 0x2b55 || hs == 0x2b1c || hs == 0x2b1b || hs == 0x2b50|| hs == 0x231a ) {
                 isEomji = YES;
             }
         }
     }];
    return isEomji;
}

- (void)searchWithString:(NSString *)searchKey
{
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    id lat = [ud stringForKey:@"latitude"];
    id lng = [ud stringForKey:@"longitude"];
    if (!lat || [lat isKindOfClass:[NSNull class]])
    {
        lat = @"";
    }
    
    if (!lng || [lng isKindOfClass:[NSNull class]])
    {
        lng = @"";
    }
    
    NSDictionary *param = @{@"curPage":@(_currentPage),@"pageSize":@(10),@"shopName":searchKey,@"lat":lat,@"lng":lng};
    
    if (!isLoadMore) {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在搜索,请稍后...", nil) withImage:nil];
    }
    
    [self.engine searchShopListWithDictionary:param];
}

- (void)getShopListDidSucceed:(NSDictionary *)addressInfo
{
    [TKUIUtil hiddenHUD];
    self.historyView.frame = CGRectMake(0, HEIGHT, self.historyView.bounds.size.width, self.historyView.bounds.size.height);
    if (_currentPage >= [[addressInfo objectForKey:@"totalPage"] integerValue]) {
        _hasMore = NO;
    }else{
        _hasMore = YES;
    }
    
    if (self.isNewSearch)
    {
        self.shopNameArr =[NSMutableArray array];
        self.currentPage = 1;
        [self.shopNameArr addObjectsFromArray:[addressInfo objectForKey:@"dataList"]];
    }
    else
    {
        if (self.isRefresh) {
            _currentPage = 1;
            [self.shopNameArr addObject:[addressInfo objectForKey:@"dataList"]];
        } else {
            _currentPage++;
            [self.shopNameArr addObjectsFromArray:[addressInfo objectForKey:@"dataList"]];
        }
    }
    
     [self doneLoadingData];
}

- (void)getShopListDidFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
}

- (void)goToMap:(UITapGestureRecognizer *)tap
{
    NSInteger index = tap.view.tag;
    NSDictionary *oneDic = self.shopNameArr[index];
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"ecid"];
    [param setValue:[NSString stringWithFormat:@"%@",[oneDic safeObjectForKey:@"lat"]] forKey:@"lat"];
    [param setValue:[NSString stringWithFormat:@"%@",[oneDic safeObjectForKey:@"lng"]] forKey:@"lng"];
    [param setValue:[oneDic safeObjectForKey:@"shopAddress"] forKey:@"address"];
    [self.engine getMapWithDictionary:param];
}

- (void)getMapSuccess:(NSDictionary *)response
{
    NSString *urlString = [response safeObjectForKey:@"url"];
    ZSTECCMapViewController *mapVC = [[ZSTECCMapViewController alloc] init];
    mapVC.urlString = urlString;
    mapVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:mapVC animated:YES];
}

-(void)goLook:(NSString *)shopId shopAddress:(NSString *)shopAddress shopName:(NSString *)shopName{
    
    ZSTECCReservationViewComtroller *reservationView = [[ZSTECCReservationViewComtroller alloc] init];
    reservationView.shopId = shopId;
    reservationView.shopAddress = shopAddress;
    reservationView.shopName = shopName;
    reservationView.hidesBottomBarWhenPushed = YES;
    reservationView.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(shopName, nil)];
    [self.navigationController pushViewController:reservationView animated:YES];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.cancelBtn.hidden = NO;
        self.historyView.frame = CGRectMake(0, 0, WIDTH, HEIGHT - 64);
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)resignBack
{
    [self.searchTF resignFirstResponder];
    [self popViewController];
}

- (void)sureString:(NSString *)searchKey
{
    searchKeyStr = searchKey;
    self.isNewSearch = YES;
    self.cancelBtn.hidden = YES;
    [self.searchTF resignFirstResponder];
    [self searchWithString:searchKey];
}

- (void)cancelSelected
{
    [self.searchTF resignFirstResponder];
    [UIView animateWithDuration:0.3f animations:^{
        
        self.historyView.frame = CGRectMake(0, HEIGHT, WIDTH, HEIGHT - 64);
        
    } completion:^(BOOL finished) {
        
        self.cancelBtn.hidden = YES;
        
    }];
}


- (void)loadDataForPage:(NSInteger)pageIndex
{
    if (pageIndex == 1) {
        _currentPage = 1;
        
        _isRefresh = YES;
    }else if (pageIndex > 1){
        
        _isRefresh = NO;
    }
    
    [self searchWithString:searchKeyStr];
}


//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
    _loading = YES;
    _currentPage = 1;
    [self loadDataForPage:_currentPage];
}

- (void)loadMoreData
{
   isLoadMore = YES;
    self.isNewSearch = NO;
    if (_hasMore)
    {
        _loading = YES;
        [_moreButton displayIndicator];
        
        [self loadDataForPage:_currentPage+1];
    }
    
}

- (void)doneLoadingData
{
    _loading = NO;
    
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.searchResultTableView];
    
    if (self.shopNameArr.count != 0) {
        [self.searchResultTableView reloadData];
    }
}


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
    [self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
    return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
    return [NSDate date];
}

- (void)cleanHistory
{
    [self.historyArr removeAllObjects];
    self.searchDelegate.resultArr = self.historyArr;
    [self.searchHistoryTableView reloadData];
    [[NSUserDefaults standardUserDefaults] setValue:self.historyArr forKey:@"history"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
