//
//  ZSTECCShopInfoIntroduceTableViewCell.m
//  EComC
//
//  Created by anqiu on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopInfoIntroduceTableViewCell.h"

@implementation ZSTECCShopInfoIntroduceTableViewCell

-(void)createUI{
    
    self.contentView.backgroundColor = RGBCOLOR(206, 206, 206);
    UIView *v1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 70)];
    v1.backgroundColor = [UIColor whiteColor];
    [self.contentView addSubview:v1];
    
    UILabel *line = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 300, 1)];
    line.backgroundColor = [UIColor lightGrayColor];
    line.alpha =0.5;
    [v1 addSubview:line];
    
    UILabel *l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 80, 30)];
    l1.text = @"商家介绍:";
    l1.font = [UIFont systemFontOfSize:14];
    l1.textColor = [UIColor grayColor];
    [v1 addSubview:l1];
    
    self.introduce = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 0, 210, 40)];
    self.introduce.textColor = [UIColor grayColor];
    self.introduce.text = @"2013年12月2日 - IOS系统提取百度云下载的文件独家发布,IOS系统提取百度云下载的文件独家发布在苹果的设备用";
    self.introduce.font = [UIFont systemFontOfSize:14];
    self.introduce.lineBreakMode = UILineBreakModeWordWrap;
    self.introduce.numberOfLines = 2;
    [v1 addSubview:self.introduce];

}

@end
