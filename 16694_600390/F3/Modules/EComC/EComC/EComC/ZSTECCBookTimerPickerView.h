//
//  ZSTECCBookTimerPickerView.h
//  EComC
//
//  Created by pmit on 15/9/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTECCBookTimerPickerViewDelegate <NSObject>

- (void)sureTimeSelected:(NSString *)stringDate;
- (void)cancelTimeSelected;

@end

@interface ZSTECCBookTimerPickerView : UIView

@property (copy,nonatomic) NSString *nowYear;
@property (copy,nonatomic) NSString *nowMonth;
@property (copy,nonatomic) NSString *nowDay;
@property (copy,nonatomic) NSString *nowHour;
@property (copy,nonatomic) NSString *nowMin;
@property (strong,nonatomic) NSArray *timeArr;
@property (assign,nonatomic) NSInteger i;
@property (assign,nonatomic) NSInteger showDay;
@property (assign,nonatomic) NSInteger showMonth;
@property (assign,nonatomic) NSInteger selectedYear;
@property (assign,nonatomic) NSInteger selectedMonth;
@property (assign,nonatomic) NSInteger selectedDay;
@property (assign,nonatomic) NSInteger selectedHour;
@property (assign,nonatomic) NSInteger selectedMinus;
@property (strong,nonatomic) NSArray *minusArr;
@property (assign,nonatomic) NSInteger afterNowMinutes;

@property (weak,nonatomic) id<ZSTECCBookTimerPickerViewDelegate> timeDelegate;

- (void)createUIWithFrame:(CGRect)rect;

@end
