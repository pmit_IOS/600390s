//
//  ZSTECCMapViewController.m
//  EComC
//
//  Created by pmit on 15/8/28.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCMapViewController.h"
#import <ZSTUtils.h>

@interface ZSTECCMapViewController () <UIWebViewDelegate>

@end

@implementation ZSTECCMapViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"店铺地图", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    [self createWebUI];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)createWebUI
{
    UIWebView *mapWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    mapWebView.delegate = self;
    [mapWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    [self.view addSubview:mapWebView];
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在加载地图,请稍后", nil) withImage:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [TKUIUtil hiddenHUD];
}

@end
