//
//  ZSTECCOrderInfoCell.m
//  EComC
//
//  Created by qiugian on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCOrderInfoCell.h"

@implementation ZSTECCOrderInfoCell

-(void)createUI{

    if (!self.codeLabel)
    {
        self.contentView.backgroundColor = RGBCOLOR(244, 244, 244);
        
        UIView *bg =[[UIView alloc] initWithFrame:CGRectMake(0, 10, 320, 340)];
        bg.backgroundColor = [UIColor whiteColor];
        [self.contentView addSubview:bg];
        
        self.bg = bg;
        
        UILabel *l1 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10, 80, 30)];
        l1.text = @"订单编号:";
        l1.textAlignment = UITextAlignmentLeft;
        l1.textColor = [UIColor grayColor];
        l1.font = [UIFont systemFontOfSize:15];
        [bg addSubview:l1];
        
        self.codeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10, 190, 30)];
        self.codeLabel.textAlignment = UITextAlignmentLeft;
        self.codeLabel.font = [UIFont systemFontOfSize:15];
        self.codeLabel.textColor = [UIColor grayColor];
        [bg addSubview:self.codeLabel];
        
        
        UILabel *l2 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+40, 80, 30)];
        l2.text = @"订单状态:";
        l2.textAlignment = UITextAlignmentLeft;
        l2.font = [UIFont systemFontOfSize:15];
        l2.textColor = [UIColor grayColor];
        [bg addSubview:l2];
        
        self.payStatus = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10+40, 150, 30)];
        self.payStatus.textAlignment = UITextAlignmentLeft;
        self.payStatus.font = [UIFont systemFontOfSize:15];
        self.payStatus.textColor = [UIColor grayColor];
        [bg addSubview:self.payStatus];
        
        
        UILabel *l3 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+40*2, 80, 30)];
        l3.text = @"下单时间:";
        l3.textAlignment = UITextAlignmentLeft;
        l3.font = [UIFont systemFontOfSize:15];
        l3.textColor = [UIColor grayColor];
        [bg addSubview:l3];
        
        self.orderTime = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10+40*2, 150, 30)];
        self.orderTime.textAlignment = UITextAlignmentLeft;
        self.orderTime.font = [UIFont systemFontOfSize:15];
        self.orderTime.textColor = [UIColor grayColor];
        [bg addSubview:self.orderTime];
        
        UILabel *l4 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+40*3, 80, 30)];
        l4.text = @"预约时间:";
        l4.textAlignment = UITextAlignmentLeft;
        l4.font = [UIFont systemFontOfSize:15];
        l4.textColor = [UIColor grayColor];
        [bg addSubview:l4];
        
        self.bookTime = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10+40*3, 150, 30)];
        self.bookTime.textAlignment = UITextAlignmentLeft;
        self.bookTime.font = [UIFont systemFontOfSize:15];
        self.bookTime.textColor = [UIColor grayColor];
        [bg addSubview:self.bookTime];
        
        UILabel *l5 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+40*4, 80, 30)];
        l5.text = @"预约类别:";
        l5.textAlignment = UITextAlignmentLeft;
        l5.font = [UIFont systemFontOfSize:15];
        l5.textColor = [UIColor grayColor];
        [bg addSubview:l5];
        
        self.bookType = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10+40*4, 150, 30)];
        self.bookType.textAlignment = UITextAlignmentLeft;
        self.bookType.font = [UIFont systemFontOfSize:15];
        self.bookType.textColor = [UIColor grayColor];
        [bg addSubview:self.bookType];
        
        UILabel *l6 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+40*5, 80, 30)];
        l6.text = @"预约人数:";
        l6.textAlignment = UITextAlignmentLeft;
        l6.font = [UIFont systemFontOfSize:15];
        l6.textColor = [UIColor grayColor];
        [bg addSubview:l6];
        
        self.bookPersonNum = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10+40*5, 150, 30)];
        self.bookPersonNum.textAlignment = UITextAlignmentLeft;
        self.bookPersonNum.font = [UIFont systemFontOfSize:15];
        self.bookPersonNum.textColor = [UIColor grayColor];
        [bg addSubview:self.bookPersonNum];
        
        UILabel *l7 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10 + 240, 80, 30)];
        l7.text = @"餐桌类型:";
        l7.textAlignment = UITextAlignmentLeft;
        l7.font = [UIFont systemFontOfSize:15];
        l7.textColor = [UIColor grayColor];
        [bg addSubview:l7];
        
        self.deskType = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10 + 240, 150, 30)];
        self.deskType.textAlignment = UITextAlignmentLeft;
        self.deskType.font = [UIFont systemFontOfSize:15];
        self.deskType.textColor = [UIColor grayColor];
        [bg addSubview:self.deskType];
        
        UILabel *l8 = [[UILabel alloc] initWithFrame:CGRectMake(10, 10+40*7, 80, 30)];
        l8.text = @"订单金额:";
        l8.textAlignment = UITextAlignmentLeft;
        l8.font = [UIFont systemFontOfSize:15];
        l8.textColor = [UIColor grayColor];
        [bg addSubview:l8];
        self.orderPriceTitle = l8;
        
        self.orderMoney = [[UILabel alloc] initWithFrame:CGRectMake(10+90, 10+40*7, 150, 30)];
        self.orderMoney.textAlignment = UITextAlignmentLeft;
        self.orderMoney.font = [UIFont systemFontOfSize:15];
        self.orderMoney.textColor = [UIColor grayColor];
        [bg addSubview:self.orderMoney];
    }
    
}



-(void)setCellData:(NSDictionary *)dic{

    self.codeLabel.text = [self filterParam:[dic safeObjectForKey:@"orderCode"]];
    self.payStatus.text = [self filterParam:[dic safeObjectForKey:@"payStatusName"]];
    self.orderTime.text = [self filterParam:[dic safeObjectForKey:@"createdOn"]];
    self.bookTime.text = [self filterParam:[dic safeObjectForKey:@"bespeakTime"]];
    self.bookType.text = [self filterParam:[dic safeObjectForKey:@"bespeakTypeName"]];
    self.bookPersonNum.text = [self filterParam:[NSString stringWithFormat:@"%@人",[dic safeObjectForKey:@"bespeakPersonCount"]]];
    self.deskType.text = [self filterParam:[dic safeObjectForKey:@"tableTypeName"]];
    
    if (![dic safeObjectForKey:@"orderPrice"] || [[dic safeObjectForKey:@"orderPrice"] isKindOfClass:[NSNull class]])
    {
        self.orderPriceTitle.hidden = YES;
        self.orderMoney.hidden = YES;
    }
    else
    {
        self.orderMoney.text = [self filterParam:[NSString stringWithFormat:@"%@元",[dic safeObjectForKey:@"orderPrice"]]];
        self.orderMoney.hidden = NO;
        self.orderPriceTitle.hidden = NO;
    }
    
    if ([[dic safeObjectForKey:@"bespeakTypeName"] isEqualToString:@"订座"])
    {
        self.orderPriceTitle.hidden = YES;
    }
    else
    {
        self.orderPriceTitle.hidden = NO;
    }
    
    
}


-(NSString *)filterParam:(NSString *)param{
    
    if (!param || [param isKindOfClass:[NSNull class]] ||[param isEqualToString:@"null"]) {
        return @"";
    }
    return param;
}

@end
