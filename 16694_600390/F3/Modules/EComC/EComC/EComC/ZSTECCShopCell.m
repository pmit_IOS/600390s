//
//  ZSTECCShopCell.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCShopCell.h"

@implementation ZSTECCShopCell

- (void)createUI
{
    if (!self.shopLogoIV)
    {
        self.shopLogoIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 60, 60)];
        self.shopLogoIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.shopLogoIV];
        
        self.shopNameLB = [[UILabel alloc] initWithFrame:CGRectMake(90, 10, WIDTH - 80 - 15, 20)];
        self.shopNameLB.textAlignment = NSTextAlignmentLeft;
        self.shopNameLB.font = [UIFont systemFontOfSize:14.0f];
        self.shopNameLB.textColor = [UIColor blackColor];
        [self.contentView addSubview:self.shopNameLB];
        
        UIImageView *locationIcon = [[UIImageView alloc] initWithFrame:CGRectMake(90, 35, 10, 15)];
        [locationIcon setImage:[UIImage imageNamed:@"model_ecomc_location_icon.png"]];
        locationIcon.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:locationIcon];
        
        self.startTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(90+15, 35, WIDTH - 90 - 100, 35)];
        self.startTitleLB.textAlignment = NSTextAlignmentLeft;
        self.startTitleLB.textColor = [UIColor lightGrayColor];
        self.startTitleLB.numberOfLines = 0;
        self.startTitleLB.font = [UIFont systemFontOfSize:11.5f];
        [self.contentView addSubview:self.startTitleLB];
        
//        self.lineLB = [[UILabel alloc] initWithFrame:CGRectMake(90, 45, 1, 20)];
//        self.lineLB.backgroundColor = [UIColor lightGrayColor];
//        [self.contentView addSubview:self.lineLB];
        
        self.distanceLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - 70, 35, 60, 20)];
        self.distanceLB.textAlignment = NSTextAlignmentRight;
        self.distanceLB.textColor = [UIColor lightGrayColor];
        self.distanceLB.font = [UIFont systemFontOfSize:14.0f];
        [self.contentView addSubview:self.distanceLB];
    }
}

- (void)setCellData:(NSDictionary *)dic
{
//    [self.shopLogoIV setImageWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"shopLogo"]]];
//    [[SDWebImageManager sharedManager] downloadWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"shopLogo"]] delegate:self];
    [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:[dic safeObjectForKey:@"shopLogo"]] options:SDWebImageRetryFailed progress:^(NSInteger receivedSize, NSInteger expectedSize) {
        
    } completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
       
        CGFloat borderWH = image.size.width > image.size.height ? image.size.height : image.size.width;
        
        UIImage *newsImage = [self imageCompressForSize:image targetSize:CGSizeMake(borderWH, borderWH)];
        self.shopLogoIV.image = newsImage;
        
    }];
    
    self.shopNameLB.text = [dic safeObjectForKey:@"shopName"];
    
    NSString *addressString = [dic safeObjectForKey:@"shopAddress"];
    CGSize addressSize = [addressString sizeWithFont:[UIFont systemFontOfSize:11.5f] constrainedToSize:CGSizeMake(WIDTH - 90 - 100, MAXFLOAT)];
    self.startTitleLB.frame = CGRectMake(90 + 15, 35, WIDTH - 90 - 100, addressSize.height);
    self.startTitleLB.text = addressString;
    
    NSString *distanceString = [NSString stringWithFormat:@"%.1lfkm",[[dic safeObjectForKey:@"distance"] doubleValue]];
    self.distanceLB.text = distanceString;
    
    
}

- (void)webImageManager:(SDWebImageManager *)imageManager didFinishWithImage:(UIImage *)image
{
    CGFloat borderWH = image.size.width > image.size.height ? image.size.height : image.size.width;
    
    UIImage *newsImage = [self imageCompressForSize:image targetSize:CGSizeMake(borderWH, borderWH)];
    self.shopLogoIV.image = newsImage;
}

- (UIImage *) imageCompressForSize:(UIImage *)sourceImage targetSize:(CGSize)size{
    UIImage *newImage = nil;
    CGSize imageSize = sourceImage.size;
    CGFloat width = imageSize.width;
    CGFloat height = imageSize.height;
    CGFloat targetWidth = size.width;
    CGFloat targetHeight = size.height;
    CGFloat scaleFactor = 0.0;
    CGFloat scaledWidth = targetWidth;
    CGFloat scaledHeight = targetHeight;
    CGPoint thumbnailPoint = CGPointMake(0.0, 0.0);
    if(CGSizeEqualToSize(imageSize, size) == NO){
        CGFloat widthFactor = targetWidth / width;
        CGFloat heightFactor = targetHeight / height;
        if(widthFactor > heightFactor){
            scaleFactor = widthFactor;
        }
        else{
            scaleFactor = heightFactor;
        }
        scaledWidth = width * scaleFactor;
        scaledHeight = height * scaleFactor;
        if(widthFactor > heightFactor){
            thumbnailPoint.y = (targetHeight - scaledHeight) * 0.5;
        }else if(widthFactor < heightFactor){
            thumbnailPoint.x = (targetWidth - scaledWidth) * 0.5;
        }
    }
    
    UIGraphicsBeginImageContext(size);
    
    CGRect thumbnailRect = CGRectZero;
    thumbnailRect.origin = thumbnailPoint;
    thumbnailRect.size.width = scaledWidth;
    thumbnailRect.size.height = scaledHeight;
    [sourceImage drawInRect:thumbnailRect];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    
    if(newImage == nil){
        NSLog(@"scale image fail");
    }
    
    UIGraphicsEndImageContext();
    
    return newImage;
    
}

@end
