//
//  ZSTECCDiscountCell.m
//  EComC
//
//  Created by P&M on 15/10/23.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCDiscountCell.h"

@implementation ZSTECCDiscountCell

- (void)createDiscountUI
{
    if (!self.titleLB)
    {
        NSString *titleString = @"会员折扣";
        CGSize stringSize = [titleString sizeWithFont:[UIFont systemFontOfSize:18.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, stringSize.width, 20)];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        self.titleLB.textColor = RGBA(85, 85, 85, 1);
        self.titleLB.text = titleString;
        [self.contentView addSubview:self.titleLB];
        
        NSString *exampleTimeString = @"当前折扣为0.6折";
        CGSize exampleSize = [exampleTimeString sizeWithFont:[UIFont systemFontOfSize:15.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
        self.discountLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, exampleSize.width, 20)];
        self.discountLB.textColor = RGBA(153, 153, 153, 1);
        self.discountLB.textAlignment = NSTextAlignmentRight;
        self.discountLB.font = [UIFont systemFontOfSize:11.0f];
        self.discountLB.text = exampleTimeString;
        self.accessoryView = self.discountLB;
    }
}

- (void)setCellDataWithDiscount:(NSString *)discountString
{
    self.discountLB.text = discountString;
}


@end
