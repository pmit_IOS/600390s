//
//  ZSTECCOrderInfoViewController.m
//  EComC
//
//  Created by qiuguian on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCOrderInfoViewController.h"
#import "ZSTECCOrderInfoCell.h"
#import "ZSTECCOrderFoodInfoCell.h"
#import "ZSTECCSureOrderViewController.h"
#import "ZSTECCPayWebViewController.h"
#import "ZSTGlobal+EComC.h"
#import "MoreButton.h"
#import <PMRepairButton.h>
#import "ZSTECCPayWebViewController.h"
#import "ZSTECCHomeViewController.h"
#import <BaseNavgationController.h>

@interface ZSTECCOrderInfoViewController (){
  
    int _currentPage;
    BOOL _hasMore;
    MoreButton *_moreButton;
    BOOL  isRefresh;
    BOOL _loading;  //是否正在加载中 待消费

}

@property (strong,nonatomic) UIButton *payBtn;
@property (strong,nonatomic) UIButton *cancelBtn;
@property (strong,nonatomic) UIButton *hasPayBtn;
@property (strong,nonatomic) PMRepairButton *showAllBtn;
@property (assign,nonatomic) BOOL isShowAll;


@end

@implementation ZSTECCOrderInfoViewController
{
    
    UIView *bottomView;
    NSDictionary *dic;
    NSMutableArray *foodArray;
    
    NSString *orderCode;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
    engine.delegate = self;
    self.engine = engine;
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (sureExit)];
    self.navigationItem.titleView= [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单详情", nil)];
    
    self.view.backgroundColor = RGBCOLOR(244, 244, 244);
    
    [self createTable];
    
     foodArray = [[NSMutableArray alloc] initWithCapacity:10];
    
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在加载订单数据,请稍后...", nil) withImage:nil];
    [self.engine getOrderInfoByOrderId:self.orderId andUserId:[ZSTF3Preferences shared].UserId];
     
}


#pragma mark - 创建table视图
-(void)createTable{
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, HEIGHT-64-60)];
    _tableView.dataSource = self;
    _tableView.delegate= self;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.backgroundColor = RGBCOLOR(244, 244, 244);
    [self.view addSubview:self.tableView];
}

#pragma mark - 创建底部视图
-(void)createBottomView{
    
    bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 50, WIDTH, 50)];
    [self.view addSubview:bottomView];
    
    self.cancelBtn = [[UIButton alloc] initWithFrame:bottomView.bounds];
    [self.cancelBtn setBackgroundColor:RGBA(173, 173, 173, 1)];
    [self.cancelBtn setTitle:@"取消订单" forState:UIControlStateNormal];
    [self.cancelBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.cancelBtn addTarget:self action:@selector(cancelOrder:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:self.cancelBtn];
    
    UIButton *hasPayBtn = [[UIButton alloc] initWithFrame:bottomView.bounds];
    [hasPayBtn setBackgroundColor:RGBA(173, 173, 173, 1)];
    [hasPayBtn setTitle:@"已支付" forState:UIControlStateNormal];
    [hasPayBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.hasPayBtn = hasPayBtn;
    [bottomView addSubview:hasPayBtn];
    
    self.payBtn = [[UIButton alloc] initWithFrame:bottomView.bounds];
    [self.payBtn setBackgroundColor:[UIColor redColor]];
    [self.payBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.payBtn setTitle:@"去付款" forState:UIControlStateNormal];
    [self.payBtn addTarget:self action:@selector(payBtnAction) forControlEvents:UIControlEventTouchUpInside];
    self.payBtn.titleLabel.font = [UIFont systemFontOfSize:14];

    [bottomView addSubview:self.payBtn];
    
}


#pragma  mark - 创建数据源
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    if ([self.bookType isEqualToString:@"1"]) {
        return 2;
    }
    
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if (section == 1) {
        
//        if (_hasMore) {
//            return foodArray.count + 1;
//        }
//        return foodArray.count;
        if (foodArray.count <= 3)
        {
            return foodArray.count;
        }
        else
        {
            if (self.isShowAll)
            {
                return foodArray.count;
            }
            else
            {
                return 3;
            }
        }
        
    }
    
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 1) {
    
        return 40;
        
    }else if (indexPath.section == 2){
        return 40;
    }
    
    return 330;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.section == 0) {
        static NSString *orderInfoCell = @"ZSTECCOrderInfoCell";
        
        ZSTECCOrderInfoCell *cell = (ZSTECCOrderInfoCell *)[tableView dequeueReusableCellWithIdentifier:orderInfoCell];
        if (!cell) {
            cell = [[ZSTECCOrderInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:orderInfoCell];
        }
        
        [cell createUI];
        [cell setCellData:dic];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    if (indexPath.section == 1) {
        
        
        static NSString *foodInfoCell = @"ZSTECCOrderFoodInfoCell";
        
        ZSTECCOrderFoodInfoCell *cell = (ZSTECCOrderFoodInfoCell *)[tableView dequeueReusableCellWithIdentifier:foodInfoCell];
        if (!cell) {
            cell = [[ZSTECCOrderFoodInfoCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:foodInfoCell];
        }
        
        [cell createUI];
        [cell setCellData:[foodArray objectAtIndex:indexPath.row]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    
    return nil;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
    if (section ==1 && foodArray.count >0) {
        
        UILabel * label = [[UILabel alloc] init];
        label.frame = CGRectMake(10, 15, 300, 20);
        label.font=[UIFont fontWithName:@"Arial" size:15.0f];
        label.textColor = [UIColor darkGrayColor];
        label.text = @"已点菜品";
        
        
        UIView * sectionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 50)];
        [sectionView setBackgroundColor:RGBA(244, 244, 244, 1)];
        [sectionView addSubview:label];
        return sectionView;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 1 && foodArray.count >0) {
        return 50;//0.1f;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 1)
    {
        UIView *sFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
        sFooterView.backgroundColor = [UIColor whiteColor];
        
        self.showAllBtn = [[PMRepairButton alloc] init];
        self.showAllBtn.frame = CGRectMake(0, 10, WIDTH, 20);
        [self.showAllBtn setImage:[UIImage imageNamed:@"model_ecomc_down.png"] forState:UIControlStateNormal];
        [self.showAllBtn setImage:[UIImage imageNamed:@"model_ecomc_up.png"] forState:UIControlStateSelected];
        [self.showAllBtn addTarget:self action:@selector(changeIsShowAll:) forControlEvents:UIControlEventTouchUpInside];
        self.showAllBtn.selected = self.isShowAll;
        [sFooterView addSubview:self.showAllBtn];
        return sFooterView;
    }
    
    return nil;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 1)
    {
        if (foodArray.count <= 3)
        {
            self.showAllBtn.hidden = YES;
            return 0;
        }
        else
        {
            self.showAllBtn.hidden = NO;
            return 40;
        }
        
    }
    
    return 0;
}

#pragma mark 订单详情
-(void)getOrderInfoDidSucceed:(NSDictionary *)orderInfo{
    
    [TKUIUtil hiddenHUD];
//    dic = [[NSDictionary alloc] init];
//    dic = orderInfo;
    dic = orderInfo;
    self.bookType = [orderInfo safeObjectForKey:@"bespeakType"];
    orderCode = [orderInfo safeObjectForKey:@"orderCode"];
    
    NSString *payStatus = [orderInfo safeObjectForKey:@"payStatus"];
    
    if ([self.bookType isEqualToString:@"0"])
    {
        [self createBottomView];
        self.payBtn.hidden = YES;
        self.cancelBtn.hidden = YES;
        self.hasPayBtn.hidden = YES;
    }else{
        
        if ([payStatus isEqualToString:@"1"]) {
            self.payBtn.hidden = YES;
            self.cancelBtn.hidden = YES;
            [bottomView removeFromSuperview];
        }else{
            self.payBtn.hidden = NO;
            self.cancelBtn.hidden = YES;
            [self createBottomView];
        }
    
    }
    
    //订餐类型
    if ([self.bookType isEqualToString:@"1"]) {
        [self.engine getListOrderDetailByUserId:[ZSTF3Preferences shared].UserId orderId:self.orderId curPage:@"1" pageSize:@"10"];
    }

    [self.tableView reloadData];
}

-(void)getOrderInfoDidFailed:(int)resultcode{
    NSLog(@"订单详情 失败");
    [TKUIUtil hiddenHUD];
    
}


#pragma mark 菜单列表
-(void)getListOrderDetailDidSucceed:(NSDictionary *)ListDetial{
    
    foodArray = [ListDetial safeObjectForKey:@"dataList"];
    
    [self.tableView reloadData];
}

-(void)getListOrderDetailFailed:(int)resultCode{
    
    NSLog(@"4");
}


#pragma mark - 跳转到支付界页
-(void)payBtnAction{
    
    [self.engine validateGotoPayByOrderId:self.orderId];
   
}

#pragma mark - 去付款按钮之前验证通过
-(void)validateGotoPayDidSucceed:(NSDictionary *)responseInfo{
    
    self.view.userInteractionEnabled = YES;
    [TKUIUtil hiddenHUD];
    NSString *urlString = [NSString stringWithFormat:@"%@?orderCode=%@",payMealUrl,orderCode];
    ZSTECCPayWebViewController *payWebVC = [[ZSTECCPayWebViewController alloc] init];
    payWebVC.urlString = urlString;
    payWebVC.orderId = self.orderId;
    [self.navigationController pushViewController:payWebVC animated:YES];
    
}

#pragma mark - 去付款按钮之前验证未通过
-(void)validateGotoPayDidFailed:(NSDictionary *)failedInfo{
    
    NSString *message = [failedInfo safeObjectForKey:@"message"];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    
}


- (void)cancelOrder:(UIButton *)sender
{
    [self.engine cancelOrderByOrderId:self.orderId andUserId:[ZSTF3Preferences shared].UserId];
}

-(void)cancelOrderDidSucceed:(NSDictionary *)cancelInfo{
    
    NSLog(@"取消订单 成功");
    [self popViewController];
}

-(void)cancelOrderDidFailed:(int)resultCode{
    
    NSLog(@"取消订单 失败");
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == _tableView)
    {
        CGFloat sectionHeaderHeight = 50;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)changeIsShowAll:(PMRepairButton *)sender
{
    sender.selected = !sender.isSelected;
    if (sender.isSelected)
    {
        self.isShowAll = YES;
    }
    else
    {
        self.isShowAll = NO;
    }
    
    NSIndexSet *indexSet=[[NSIndexSet alloc] initWithIndex:1];
    [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [_tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

#pragma mark - 返回事件
-(void)sureExit{
   
    if (self.fromVC == [[ZSTECCPayWebViewController alloc] class]) {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }else{
        [self popViewController];
    }

}

@end
