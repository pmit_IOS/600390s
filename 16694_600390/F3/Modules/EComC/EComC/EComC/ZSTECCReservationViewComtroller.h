//
//  ZSTECCReservationViewComtroller.h
//  EComC
//
//  Created by qiuguian on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECCCarouselView.h"
#import "ZSTF3Engine+EComC.h"
#import "ZSTModuleBaseViewController.h"
#import "ZSTLoginViewController.h"

@class ZSTF3Engine;

@interface ZSTECCReservationViewComtroller : ZSTModuleBaseViewController<UITableViewDataSource,UITableViewDelegate,TKLoadMoreViewDelegate,ZSTF3EngineECCDelegate,LoginDelegate>{
    
    TKLoadMoreView *_loadMoreView;
    BOOL _isRefreshing;
    BOOL _isLoadingMore;
    BOOL _hasMore;
    NSInteger _pagenum;

}

@property (nonatomic, strong) UITableView *shopInfoTableView;
@property (nonatomic, strong) ZSTECCCarouselView * carouselView;
@property (retain,nonatomic) NSArray * carouseData;
@property (nonatomic, retain) ZSTF3Engine *engine;
@property (copy,nonatomic) NSString *shopId;
@property (strong,nonatomic) UIView *shadowView;
@property (copy,nonatomic) NSString *shopAddress;
@property (copy,nonatomic) NSString *shopName;
@property (strong,nonatomic) UIButton *bookInfoBtn;
@property (strong,nonatomic) UIButton *shopInfoBtn;
@property (copy,nonatomic) NSString *lng;
@property (copy,nonatomic) NSString *lat;

@end
