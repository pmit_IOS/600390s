//
//  ZSTECCSearchViewController.h
//  EComC
//
//  Created by pmit on 15/8/18.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine+EComC.h"

@interface ZSTECCSearchViewController : UIViewController <ZSTF3EngineECCDelegate,EGORefreshTableHeaderDelegate>{

   EGORefreshTableHeaderView *_refreshHeaderView;
}

@property (strong,nonatomic) ZSTF3Engine *engine;
@property (strong,nonatomic) UITextField *searchTF;

@end
