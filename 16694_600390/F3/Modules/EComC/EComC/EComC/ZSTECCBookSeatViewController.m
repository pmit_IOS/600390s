//
//  ZSTECCBookSeatViewController.m
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCBookSeatViewController.h"
#import "ZSTECCMessageCell.h"
#import "ZSTECCFoodInfoViewController.h"
#import "ZSTECCReservationViewComtroller.h"
#import "ZSTECCReservationViewComtroller.h"
#import "ZSTECCOrderListViewController.h"
#import <ZSTLoginController.h>
#import <BaseNavgationController.h>

@interface ZSTECCBookSeatViewController() <UITableViewDataSource,UITableViewDelegate,ZSTLoginControllerDelegate>

@property (strong,nonatomic) NSArray *titleArr;
@property (strong,nonatomic) NSArray *iconArr;
@property (strong,nonatomic) UIView *shadowView;
@property (assign,nonatomic) NSInteger maxCount;
@property (strong,nonatomic) NSArray *seatTypeArr;
@property (strong,nonatomic) NSArray *sexArr;
@property (strong,nonatomic) NSIndexPath *nowIndexPath;
@property (copy,nonatomic) NSString *selectedTimeString;
@property (copy,nonatomic) NSString *selectedCountString;
@property (strong,nonatomic) NSArray *timeArr;
@property (weak,nonatomic) UIButton *doneInKeyboardButton;
@property (assign,nonatomic) NSInteger afterNowMinutes;

@end


#define IS_IOS7 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
#define IS_IOS8 ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8)

@implementation ZSTECCBookSeatViewController{

    NSArray *timeArr;
    UIView *textEdit;
    UITextView  *textViewF;
    BOOL isTextView;
    UIButton *bookBtn1;
    UIButton *bookBtn2;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        self.view.frame = CGRectMake(0, 47, WIDTH, HEIGHT - 47);
    }
    return self;
}

static NSString *const messageCell = @"messageCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.titleArr = @[@[@"预约时间",@"预约人数",@"餐桌类型"],@[@"输入联系人",@"联系方式"]];
    self.iconArr = @[@[@"model_ecomc_timev.png",@"model_ecomc_personNumv.png",@"model_ecomc_deskTypev.png"],@[@"model_ecomc_namev.png",@"model_ecomc_phonev.png"],@[@"model_ecomc_seatnotev.png"]];
    [self buildTableView];
    [self buildBottomBtnView];
    [self buildHideView];
//    [self createYMSelected];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self registerForKeyboardNotifications];
    
    isTextView = NO;
//
    
    textEdit = [[UIView alloc] initWithFrame:CGRectMake(5, 0, 310, 80)];
    textEdit.backgroundColor = [UIColor whiteColor];
    textEdit.layer.cornerRadius = 5;
    textEdit.layer.borderWidth =1;
     textEdit.hidden = YES;
    textEdit.layer.borderColor = RGBA(224, 224, 224, 1).CGColor;
    [self.view addSubview:textEdit];
    
    UIImageView *iconIV = [[UIImageView alloc] initWithFrame:CGRectMake(15, 5, 20, 20)];
    iconIV.contentMode = UIViewContentModeScaleAspectFit;
    iconIV.image = [UIImage imageNamed:@"model_ecomc_seatnotev.png"];
    [textEdit addSubview:iconIV];
    
    textViewF= [[UITextView alloc] initWithFrame:CGRectMake(45, 0, 280, 60)];
    [textEdit addSubview:textViewF];
    
    UILabel *lb = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, WIDTH - 30, 20)];
    lb.text = @"备注:";
    lb.textColor = RGBA(205, 205, 205, 1);
    lb.textAlignment = NSTextAlignmentLeft;
    [textViewF addSubview:lb];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在努力加载数据，请稍后...", nil) withImage:nil];
    [self.engine getInitShopInfoByShopId:self.shopId];
}

- (void)buildTableView
{
    self.seatMessageTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 47 - 50) style:UITableViewStylePlain];
    self.seatMessageTableView.delegate = self;
    self.seatMessageTableView.dataSource = self;
    [self.seatMessageTableView registerClass:[ZSTECCMessageCell class] forCellReuseIdentifier:messageCell];
    self.seatMessageTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.view addSubview:self.seatMessageTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBA(244, 244, 244, 1);
    self.seatMessageTableView.tableFooterView = footerView;
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArr.count + 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section < self.titleArr.count)
    {
        return [[self.titleArr objectAtIndex:section] count];
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTECCMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:messageCell];
    cell.bookSeatVC = self;
    cell.delegate = self;
    [cell createUI];
    if (indexPath.section < self.titleArr.count)
    {
        BOOL isCanChoose = NO;
        BOOL isSeat = NO;
        if (indexPath.section == 0 && indexPath.row == 2)
        {
            isCanChoose = YES;
            isSeat = YES;
        }
        else if (indexPath.section == 1 && indexPath.row == 0)
        {
            isCanChoose = YES;
            isSeat = NO;
        }
          //显示预定时间
//        if (indexPath.section==0 && indexPath.row == 0) {
//            cell.phoneTF.hidden = NO;
//            cell.phoneTF.enabled = NO;
//            cell.phoneTF.placeholder =[NSString stringWithFormat:@""];
//        }
        
        [cell setCellDataWithIconIV:self.iconArr[indexPath.section][indexPath.row] messageTitle:self.titleArr[indexPath.section][indexPath.row] isSeat:isSeat isCanChoose:isCanChoose isBackUp:NO SeatTypeArr:self.seatTypeArr SexTypeArr:self.sexArr];
    }
   
    if (indexPath.section == self.titleArr.count)
    {
        [cell setCellDataWithIconIV:self.iconArr[indexPath.section][indexPath.row] messageTitle:@"备注" isSeat:NO isCanChoose:NO isBackUp:YES SeatTypeArr:self.seatTypeArr SexTypeArr:self.sexArr];
    }
    
    if (indexPath.section && indexPath.row == 1)
    {
        cell.phoneTF.hidden = NO;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section < self.titleArr.count)
    {
        return 50;
    }
    if (indexPath.section > self.titleArr.count)
    {
        return 0;
    }
    else
    {
        return 90;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(244, 244, 244, 1);
    return sectionHeaderView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *oneIndexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    NSIndexPath *twoIndexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    ZSTECCMessageCell *oneCell = (ZSTECCMessageCell *)[tableView cellForRowAtIndexPath:oneIndexPath];
    ZSTECCMessageCell *twoCell = (ZSTECCMessageCell *)[tableView cellForRowAtIndexPath:twoIndexPath];
    [oneCell.nameTF resignFirstResponder];
    [oneCell.phoneTF resignFirstResponder];
    
    [twoCell.nameTF resignFirstResponder];
    [twoCell.phoneTF resignFirstResponder];
    
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        self.nowIndexPath = indexPath;
        [self showTimePicker];
    }
    else if (indexPath.section == 0 && indexPath.row == 1)
    {
        self.nowIndexPath = indexPath;
        [self showCountView];
    }
    else
    {
        
    }
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.seatMessageTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)buildHideView
{
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    shadowView.backgroundColor = [UIColor lightGrayColor];
    shadowView.alpha = 0.5;
    shadowView.hidden = YES;
    self.shadowView = shadowView;
    [self.view addSubview:shadowView];
}

#pragma mark - 年月选择器
- (void)createYMSelected
{
    NSDate *nowDate = [NSDate new];
    NSCalendar *calendar =  [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit | NSWeekdayCalendarUnit |
    NSHourCalendarUnit | NSMinuteCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:nowDate];
    NSInteger year = [dateComponent year];
    NSInteger month = [dateComponent month];
    NSInteger day = [dateComponent day];
    NSInteger hour = [dateComponent hour];
    NSInteger minute = [dateComponent minute];
    
    NSLog(@"year is: %ld", year);
    NSLog(@"month is: %ld", month);
    NSLog(@"day is: %ld", day);
    NSLog(@"hour is: %ld", hour);
    NSLog(@"minute is: %ld", minute);
    
//    ZSTECCTimePickerView *pickView = [[ZSTECCTimePickerView alloc] init];
    ZSTECCBookTimerPickerView *pickView = [[ZSTECCBookTimerPickerView alloc] init];
    self.pickView = pickView;
    pickView.timeArr = self.timeArr;
    self.pickView.timeDelegate = self;
    pickView.nowYear = [NSString stringWithFormat:@"%@",@(year)];
    pickView.nowMonth = [NSString stringWithFormat:@"%@",@(month)];
    pickView.nowDay = [NSString stringWithFormat:@"%@",@(day)];
    pickView.nowHour = [NSString stringWithFormat:@"%@",@(hour)];
    pickView.nowMin = [NSString stringWithFormat:@"%@",@(minute)];
    pickView.timeArr = self.timeArr;
    pickView.afterNowMinutes = self.afterNowMinutes;
    
    
    NSLog(@"timeArra==>%@",pickView.timeArr);
    
    [pickView createUIWithFrame:CGRectMake(0, HEIGHT, WIDTH, 256)];
    [self.view addSubview:pickView];
}

- (void)showTimePicker
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT - 256 - 64 - 47, WIDTH, 256);
    }];
}

- (void)cancelPickerView
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 256);
    }];
}

- (void)getInitOrderInfoSuccessd:(NSDictionary *)response
{
    NSLog(@"response===>%@",response);
    
    [TKUIUtil hiddenHUD];
    self.maxCount = [[response safeObjectForKey:@"yyrsCount"] integerValue];
    self.seatTypeArr = [response safeObjectForKey:@"czlxList"];
    self.sexArr = [response safeObjectForKey:@"yyrlxList"];
    self.timeArr = [response safeObjectForKey:@"yysjList"];
    self.afterNowMinutes = [[response safeObjectForKey:@"afterNowMinutes"] integerValue];
    [self createYMSelected];
    [self.seatMessageTableView reloadData];
    [self buildCountView];
}

- (void)getInitOrderInfoFailed:(int)resultCode
{
    [TKUIUtil hiddenHUD];
}

- (void)buildCountView
{
    ZSTPeopleNumPickerView *pickView = [[ZSTPeopleNumPickerView alloc] init];
    self.peoplePickerView = pickView;
    pickView.maxCount = self.maxCount;
    pickView.peopleDelegate = self;
    [pickView createPickerView:CGRectMake(0, HEIGHT, WIDTH, 256)];
    [self.view addSubview:pickView];
}

- (void)showCountView
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.peoplePickerView.frame = CGRectMake(0, HEIGHT - 300 - 64, WIDTH, 256);
    }];
}

- (void)sureSelected:(NSInteger)count
{
    self.shadowView.hidden = YES;
    
    ZSTECCMessageCell *cell = (ZSTECCMessageCell *)[self.seatMessageTableView cellForRowAtIndexPath:self.nowIndexPath];
    cell.rightLB.hidden = NO;
    cell.rightLB.text = [NSString stringWithFormat:@"%@人",@(count)];
    self.bookCount = count;
    self.selectedCountString = [NSString stringWithFormat:@"%@",@(count)];
    [UIView animateWithDuration:0.3f animations:^{
        self.peoplePickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 256);
    }];
}

- (void)cancelSeleted
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.peoplePickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 256);
    }];
}

- (void)sureTimeSelected:(NSString *)stringDate
{
    ZSTECCMessageCell *cell = (ZSTECCMessageCell *)[self.seatMessageTableView cellForRowAtIndexPath:self.nowIndexPath];
    cell.phoneTF.hidden = YES;
    cell.rightLB.hidden = NO;
    cell.rightLB.text = stringDate;
    self.selectedTimeString = stringDate;
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 256);
    }];
}

- (void)cancelTimeSelected
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 256);
    }];
}

- (void)buildBottomBtnView
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 47 - 64 - 50, WIDTH, 50)];
    bottomView.backgroundColor = [UIColor whiteColor];
    bottomView.tag = 920109;
    [self.view addSubview:bottomView];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(206, 206, 206, 1).CGColor;
    line.frame = CGRectMake(0, 0, WIDTH, 0.8);
    //[bottomView.layer addSublayer:line];
    
    //订座 按钮
    bookBtn1 = [[UIButton alloc] initWithFrame:CGRectMake(10,5, 130, 40)];
    bookBtn1.titleLabel.textAlignment = UITextAlignmentCenter;
    bookBtn1.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    bookBtn1.titleLabel.textColor = [UIColor whiteColor];
    bookBtn1.backgroundColor = RGBA(243, 149, 1, 1);
    bookBtn1.layer.cornerRadius =5;
    [bookBtn1 setTitle:@"订座" forState:UIControlStateNormal];
    [bookBtn1 addTarget:self action:@selector(bookSeat:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:bookBtn1];
    
    //订位点餐 按钮
    bookBtn2 = [[UIButton alloc] initWithFrame:CGRectMake(180, 5, 130, 40)];
    bookBtn2.titleLabel.textAlignment = UITextAlignmentCenter;
    bookBtn2.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [bookBtn2 setBackgroundColor:[UIColor blackColor]];
    bookBtn2.backgroundColor = RGBA(231, 52, 16, 1);
    bookBtn2.layer.cornerRadius =5;
    [bookBtn2 addTarget:self action:@selector(bookFood:) forControlEvents:UIControlEventTouchUpInside];
    [bookBtn2 setTitle:@"订座点餐" forState:UIControlStateNormal];
    [bottomView addSubview:bookBtn2];
}

- (void)bookSeat:(UIButton *)sender
{
    NSString *message = @"";
    if (!self.selectedTimeString || [self.selectedTimeString isEqualToString:@""])
    {
        message = @"您还没有选择日期!";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if(![self validateCurrentTime:self.selectedTimeString]){
       
        message = @"预约时间至少提前半个小时!";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if (!self.selectedCountString || [self.selectedCountString isEqualToString:@"0"])
    {
        message = @"您还没有确定人数!";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if (!self.selectedSeatTypeString || [self.selectedSeatTypeString isEqualToString:@""])
    {
        message = @"您还没有选择座位类型";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if (!self.bookManNameString || [self.bookManNameString isEqualToString:@""])
    {
        message = @"您还没有填写联系人姓名";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];

    }
    else if (!self.bookPhoneString || [self.bookPhoneString isEqualToString:@""])
    {
        message = @"您还没有填写手机号码";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];

    }
    else if (![self validateMobile:self.bookPhoneString])
    {
        message = @"您填写的手机号码不正确";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else
    {
        if (![ZSTF3Preferences shared].UserId || [[ZSTF3Preferences shared].UserId isEqualToString:@""]) {
            
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = NO;
            controller.delegate = self;
            BaseNavgationController *navController = [[BaseNavgationController alloc] initWithRootViewController:controller];
            
            [self.navigationController presentViewController:navController animated:YES completion:^{
                
            }];
        }
        else
        {
            bookBtn1.enabled = NO;
            
            NSMutableDictionary *param = [NSMutableDictionary dictionary];
            [param setValue:self.shopId forKey:@"shopId"];
            [param setValue:[ZSTF3Preferences shared].UserId forKey:@"userId"];
            [param setValue:self.shopName forKey:@"shopName"];
            [param setValue:self.selectedTimeString forKey:@"bespeakTime"];
            [param setValue:self.selectedCountString forKey:@"bespeakPersonCount"];
            [param setValue:self.selectedSexTypeString forKey:@"contactType"];
            [param setValue:self.bookManNameString forKey:@"contactName"];
            [param setValue:self.bookPhoneString forKey:@"contactTel"];
            [param setValue:self.selectedSeatTypeString forKey:@"tableType"];
            [param setValue:self.backUpString forKey:@"orderRemark"];
            [self.engine saveOrderSeatMealByParam:param];
            
        }
        
    }
}

- (void)bookFood:(UIButton *)sender
{
    NSString *message = @"";
    if (!self.selectedTimeString || [self.selectedTimeString isEqualToString:@""])
    {
        message = @"您还没有选择日期!";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }else if(![self validateCurrentTime:self.selectedTimeString]){
        
        message = @"预约时间至少提前半个小时!";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if (!self.selectedCountString || [self.selectedCountString isEqualToString:@"0"])
    {
        message = @"您还没有确定人数!";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if (!self.selectedSeatTypeString || [self.selectedSeatTypeString isEqualToString:@""])
    {
        message = @"您还没有选择座位类型";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else if (!self.bookManNameString || [self.bookManNameString isEqualToString:@""])
    {
        message = @"您还没有填写联系人姓名";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
        
    }
    else if (!self.bookPhoneString || [self.bookPhoneString isEqualToString:@""])
    {
        message = @"您还没有填写手机号码";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
        
    }
    else if (![self validateMobile:self.bookPhoneString])
    {
        message = @"您填写的手机号码不正确";
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];
    }
    else
    {

        NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
        [params setObject:self.shopId forKey:@"shopId"];
        [params setObject:self.shopName forKey:@"shopName"];
        [params setObject:self.selectedTimeString forKey:@"bespeakTime"];
        [params setObject:@(self.bookCount) forKey:@"bespeakPersonCount"];
        [params setObject:self.selectedSeatTypeString forKey:@"tableType"];
        
        [self.engine validateReserveMealWithDictionary:params];
        
    }
    
}

-(void)validateReserveMealDidSucceed:(NSDictionary *)responseInfo{
    
    
    ZSTECCFoodInfoViewController *foodInfoVC = [[ZSTECCFoodInfoViewController alloc] init];
    foodInfoVC.shopId = self.shopId;
    foodInfoVC.shopName = self.shopName;
    foodInfoVC.shopAddress = self.shopAddress;
    foodInfoVC.bookTimeString = self.selectedTimeString;
    foodInfoVC.selectedSeatTypeString = self.selectedSeatTypeString;
    foodInfoVC.selectedSexTypeString = self.selectedSexTypeString;
    foodInfoVC.bookManNameString = self.bookManNameString;
    foodInfoVC.bookPhoneString = self.bookPhoneString;
    foodInfoVC.bookCount = self.bookCount;
    foodInfoVC.backUpString = self.backUpString;
    [self.fatherVC.navigationController pushViewController:foodInfoVC animated:YES];

}

-(void)validateReserveMealDidFailed:(NSDictionary *)failedInfo{
   
    NSString *message = [failedInfo safeObjectForKey:@"message"];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(message, nil) withImage:nil];

}


- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,181,189,1700,1709,1705
     */
    NSString * MOBILE = @"^1(3[0-9]|4[0-9]|5[0-35-9]|7[0-9]|8[0125-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[278])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349|700|705|709)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)saveSeatDidSuccess:(NSDictionary *)response
{
    ZSTECCOrderListViewController *orderListVC = [[ZSTECCOrderListViewController alloc] init];
    [self.navigationController pushViewController:orderListVC animated:YES];
}

- (void)saveSeatDidFailed:(NSDictionary *)failResponse
{
    bookBtn1.enabled = YES;
    
    NSString *msgCode = [failResponse safeObjectForKey:@"messageCode"];
    NSString *msg = [failResponse safeObjectForKey:@"message"];
    if ([msgCode integerValue] == 0) {
        [self showAlertWithMsg:msg];
    }else{
        [self showAlertWithMsg:@"系统繁忙，请稍后再试"];
    }
    
   
}

- (void)keyBoardShow:(NSNotification *)notify
{
    NSDictionary *userInfo = [notify userInfo];
    NSLog(@"userInfo --> %@",userInfo);
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    
    CGFloat y = keyboardRect.origin.y;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    NSArray *subviews = [self.view subviews];
    
    UIView *kView = [self findKeyboard];
    if (!self.doneBtn && self.isNumKey)
    {
        self.doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.doneBtn setTitle:@"完 成" forState:UIControlStateNormal];
        self.doneBtn.frame = CGRectMake(0, kView.bounds.size.height - 53, 106, 53);
        [self.doneBtn addTarget:self action:@selector(dimissBtn:) forControlEvents:UIControlEventTouchUpInside];
        [kView addSubview:self.doneBtn];
    }
    else
    {
        self.doneBtn.hidden = NO;
    }
    
    
    for (UIView *sub in subviews) {
        
        if ([sub isKindOfClass:[ZSTECCTimePickerView class]] || [sub isKindOfClass:[ZSTPeopleNumPickerView class]] || [sub isEqual:[self.view viewWithTag:920109]])
        {
            continue;
        }
        CGFloat maxY = CGRectGetMaxY(sub.frame);
        if (maxY > y - 2) {
            sub.center = CGPointMake(CGRectGetWidth(self.view.frame)/2.0, sub.center.y - maxY + y - 2);
        }
    }
    [UIView commitAnimations];
    
    self.fatherVC.bookInfoBtn.userInteractionEnabled = NO;
    self.fatherVC.shopInfoBtn.userInteractionEnabled = NO;
}

- (void)keyBoardDismiss:(NSNotification *)notify
{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.25];
    NSArray *subviews = [self.view subviews];
    for (UIView *sub in subviews) {
        
        if ([sub isKindOfClass:[ZSTECCTimePickerView class]] || [sub isKindOfClass:[ZSTPeopleNumPickerView class]] || [sub isEqual:[self.view viewWithTag:920109]])
        {
            continue;
        }
        
        if (sub.center.y < CGRectGetHeight(self.view.frame)/2.0) {
            sub.center = CGPointMake(CGRectGetWidth(self.view.frame)/2.0, (HEIGHT - 47 - 64 - 50) / 2);
        }
    }
    [UIView commitAnimations];
    self.fatherVC.bookInfoBtn.userInteractionEnabled = YES;
    self.fatherVC.shopInfoBtn.userInteractionEnabled = YES;
    if (self.doneBtn)
    {
        [self.doneBtn removeFromSuperview];
        self.doneBtn = nil;
    }
}

-(void)finishAction{
    //隐藏完成按钮
    self.doneInKeyboardButton.hidden = YES;
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];//关闭键盘
}

- (void)loginDidFinish
{
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.shopId forKey:@"shopId"];
    [param setValue:[ZSTF3Preferences shared].UserId forKey:@"userId"];
    [param setValue:self.shopName forKey:@"shopName"];
    [param setValue:self.selectedTimeString forKey:@"bespeakTime"];
    [param setValue:self.selectedCountString forKey:@"bespeakPersonCount"];
    [param setValue:self.selectedSexTypeString forKey:@"contactType"];
    [param setValue:self.bookManNameString forKey:@"contactName"];
    [param setValue:self.bookPhoneString forKey:@"contactTel"];
    [param setValue:self.selectedSeatTypeString forKey:@"tableType"];
    [param setValue:self.backUpString forKey:@"orderRemark"];
    [self.engine saveOrderSeatMealByParam:param];
    
}


-(void)textViewWillEditing:(UITextView *)textView{
    
}


- (void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWasShown:) name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter]  addObserver:self selector:@selector(keyboardWasHidden:) name:UIKeyboardWillHideNotification object:nil];
}


- (void) keyboardWasShown:(NSNotification *) notif
{
    NSDictionary *info = [notif userInfo];
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    self.peoplePickerView.hidden = YES;
    self.pickView.hidden = YES;
    
    [UIView beginAnimations:@"myAnimations" context:nil];
    CGRect rect = self.view.frame;
    if (IS_IOS7 && !IS_IOS8) {
        rect.origin.y = - 80;
    }else{
        rect.origin.y = - 120;
    }
    
//    UIView *kView = [self findKeyboard];
//    if (!self.doneBtn && self.isNumKey)
//    {
//        self.doneBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//        [self.doneBtn setTitle:@"完 成" forState:UIControlStateNormal];
//        self.doneBtn.frame = CGRectMake(0, kView.bounds.size.height - 53, 106, 53);
//        [self.doneBtn addTarget:self action:@selector(dimissBtn:) forControlEvents:UIControlEventTouchUpInside];
//        [kView addSubview:self.doneBtn];
//    }
//    else
//    {
//        self.doneBtn.hidden = NO;
//    }

    
    self.view.frame = rect;
    [UIView commitAnimations];


}
- (void) keyboardWasHidden:(NSNotification *) notif
{

    NSDictionary *info = [notif userInfo];
    
    NSValue *value = [info objectForKey:UIKeyboardFrameBeginUserInfoKey];
    CGSize keyboardSize = [value CGRectValue].size;
    
    self.peoplePickerView.hidden = NO;
    self.pickView.hidden = NO;
    
    [UIView beginAnimations:@"myAnimations" context:nil];
    CGRect rect = self.view.frame;
    rect.origin.y = +50;
    self.view.frame = rect;
    [UIView commitAnimations];

    
}



- (UIView *)findKeyboard
{
    UIView *keyboardView = nil;
    NSArray *windows = [[UIApplication sharedApplication] windows];
    for (UIWindow *window in [windows reverseObjectEnumerator])//逆序效率更高，因为键盘总在上方
    {
        keyboardView = [self findKeyboardInView:window];
        if (keyboardView)
        {
            return keyboardView;
        }
    }
    return nil;
}

- (UIView *)findKeyboardInView:(UIView *)view
{
    for (UIView *subView in [view subviews])
    {
        if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)
        {
            if (strstr(object_getClassName(subView), "UIInputSetContainerView"))
            {
                return subView;
            }
        }
        else
        {
            if (strstr(object_getClassName(subView), "UIKeyboard"))
            {
                return subView;
            }
        }
    }
    return nil;
}

- (void)dimissBtn:(UIButton *)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    ZSTECCMessageCell *cell =  (ZSTECCMessageCell *)[self.seatMessageTableView cellForRowAtIndexPath:indexPath];
    [cell.phoneTF resignFirstResponder];
    [sender removeFromSuperview];
    self.doneBtn = nil;
}

-(void)showAlertWithMsg:(NSString *)string{
    
    UIView *promptView = [[UIView alloc] init];
    promptView.frame = CGRectMake(40, 180, 240, 75);
    promptView.backgroundColor = [UIColor whiteColor];
    promptView.layer.cornerRadius = 8;
    promptView.layer.masksToBounds = YES;
    promptView.layer.borderWidth = 1;
    promptView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    [self.view.window addSubview:promptView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 200, 18)];
    textLabel.numberOfLines = 0;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textAlignment = NSTextAlignmentCenter;
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(200, 50) lineBreakMode:NSLineBreakByCharWrapping];
    textLabel.frame = CGRectMake(20, 20, 200, size.height);
    textLabel.text = string;
    [promptView addSubview:textLabel];
    
    
    [UIView animateWithDuration:0.5
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         promptView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:2
                                               delay:2
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              promptView.alpha = 0;
                                          }
                                          completion:^(BOOL finished) {
                                          }
                          ];
                     }
     ];
    
}




#pragma mark - 时间比较
-(BOOL)validateCurrentTime:(NSString *)timeStr{
    
    NSDateFormatter *format=[[NSDateFormatter alloc] init];
    [format setDateFormat:@"yyyy-MM-dd HH:mm"];
    
    NSDate *fromdate=[format dateFromString:timeStr];
    NSTimeZone *fromzone = [NSTimeZone systemTimeZone];
    NSInteger frominterval = [fromzone secondsFromGMTForDate: fromdate];
    NSDate *fromDate = [fromdate  dateByAddingTimeInterval: frominterval];
    
    //获取当前时间
    NSDate *adate = [NSDate date];
    NSTimeZone *zone = [NSTimeZone systemTimeZone];
    NSInteger interval = [zone secondsFromGMTForDate: adate];
    NSDate *localeDate = [adate  dateByAddingTimeInterval: interval];
    
    double intervalTime = [fromDate timeIntervalSinceReferenceDate] - [localeDate timeIntervalSinceReferenceDate];
    long lTime = labs((long)intervalTime);
    NSInteger iMinutes = (lTime / 60) % 60;
    NSInteger iHours = labs(lTime/3600);
    
    
    NSLog(@"相差%ld时%ld分", iHours,iMinutes);
    
    if (intervalTime < 0) {
        return false;
    }
    
    if (iHours<1 && iMinutes>0)
    {
        if (iMinutes < 30) {
             return false;
        }else{
             return true;
        }
        
    }else{
        return true;
    }
}

@end
