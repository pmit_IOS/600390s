//
//  ZSTECCPayWebViewController.m
//  EComC
//
//  Created by pmit on 15/8/20.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCPayWebViewController.h"
#import "ZSTECCOrderListViewController.h"
#import "ZSTECCOrderInfoViewController.h"
#import "ZSTECCSureOrderViewController.h"

@interface ZSTECCPayWebViewController () <UIWebViewDelegate,UIAlertViewDelegate>

@property (strong,nonatomic) UIWebView *payWebView;

@end

@implementation ZSTECCPayWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildNavigation];
    [self createWebView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildNavigation
{
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"支付宝支付", nil)] ;
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"暂不支付", @"") target:self selector:@selector (sureExit)];
}

- (void)createWebView
{
    self.payWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.payWebView.delegate = self;
    [self.view addSubview:self.payWebView];
    [self.payWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [NSString stringWithFormat:@"%@",request.URL];
    if ([urlString rangeOfString:@"ios"].length > 0)
    {
        if ([urlString rangeOfString:@"payFinish"].length > 0)
        {
            [self pushToOrderListVC];
        }
        
        return NO;
    }
    
    return YES;
}

- (void)pushToOrderListVC
{
    ZSTECCOrderListViewController *orderListVC = [[ZSTECCOrderListViewController alloc] init];
    orderListVC.isFromHome = NO;
    [self.navigationController pushViewController:orderListVC animated:YES];
}

- (void)sureExit
{
    UIAlertView *tipAlert = [[UIAlertView alloc] initWithTitle:@"温馨提示" message:@"离开后您可以继续在订单列表中继续完成支付,确定要离开?" delegate:self cancelButtonTitle:@"继续支付" otherButtonTitles:@"离开", nil];
    [tipAlert show];
}

#pragma mark - alert代理
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (buttonIndex == 1)
    {
        if (self.fromVC == [[ZSTECCSureOrderViewController alloc] class]) {
            
            ZSTECCOrderInfoViewController *orderInfoView = [[ZSTECCOrderInfoViewController alloc] init];
            orderInfoView.orderId = self.orderId;
            orderInfoView.fromVC = self.class;
            [self.navigationController pushViewController:orderInfoView animated:YES];
            
        }else{
           
            [self popViewController];
        }
        
    }
}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在努力加载页面，请稍后", nil) withImage:nil];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [TKUIUtil hiddenHUD];
}

@end
