//
//  ZSTECCShopMessageViewController.h
//  EComC
//
//  Created by pmit on 15/8/10.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTECCCarouselView.h"
#import "ZSTF3Engine+EComC.h"
#import "ZSTECCShopReviewsCell.h"
#import "ZSTECCShopInfoAddressTableViewCell.h"

@class ZSTF3Engine;
@interface ZSTECCShopMessageViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,ZSTECCCarouselViewDataSource,ZSTECCCarouselViewDelegate,ZSTF3EngineECCDelegate,ZSTECCShopReviewsCellDelegate,EGORefreshTableHeaderDelegate,shopInfoAddressDelegate>
{
    EGORefreshTableHeaderView *_refreshHeaderView;
}

@property (strong,nonatomic) UITableView *shopMessageTableView;
@property (nonatomic, strong) ZSTECCCarouselView * carouselView;
@property (retain,nonatomic) NSArray * carouseData;
@property (nonatomic, retain) ZSTF3Engine *engine;
@property (nonatomic, strong) NSMutableArray *reviewArray;

@property (copy,nonatomic) NSString *lng;
@property (copy,nonatomic) NSString *lat;

@property (nonatomic, strong) NSString *shopId;

@end
