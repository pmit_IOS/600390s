//
//  ZSTECCSureOrderViewController.m
//  EComC
//
//  Created by pmit on 15/8/11.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCSureOrderViewController.h"
#import "ZSTECCAddressCell.h"
#import "ZSTECCBookTimeCell.h"
#import "ZSTECCDiscountCell.h"
#import "ZSTECCPayWayCell.h"
#import "ZSTECCBalanceCell.h"
#import "ZSTECCOrderProductCell.h"
#import "ZSTF3Engine+EComC.h"
#import "ZSTGlobal+EComC.h"
#import "ZSTECCPayWebViewController.h"
#import <PMRepairButton.h>
#import <ZSTLoginController.h>
#import <BaseNavgationController.h>

@interface ZSTECCSureOrderViewController() <UITableViewDataSource,UITableViewDelegate,ZSTF3EngineECCDelegate,ZSTLoginControllerDelegate>

@property (strong,nonatomic) UITableView *orderSureTableView;
@property (strong,nonatomic) NSString *payWay;
@property (strong,nonatomic) NSIndexPath *currentIndexPath;
@property (strong,nonatomic) UILabel *priceLB;
@property (strong,nonatomic) PMRepairButton *showAllBtn;
@property (assign,nonatomic) BOOL isShowAll;
@property (strong,nonatomic) UIButton *submitBtn;

@property (strong, nonatomic) NSString *discountString;
@property (strong, nonatomic) NSString *balanceString;

@end

@implementation ZSTECCSureOrderViewController

static NSString *const addressCell = @"addressCell";//地址
static NSString *const bookTimeCell = @"bookTimeCell";//预约时间
static NSString *const discountCell = @"discountCell";//会员折扣
static NSString *const payWayCell = @"payWayCell";//支付方式
static NSString *const balanceCell = @"balanceCell";//余额支付
static NSString *const orderProductCell = @"orderProductCell";//订单
static NSString *const tapCell = @"tapCell";

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"订单确认", nil)];
    self.currentIndexPath = [NSIndexPath indexPathForRow:-1 inSection:2];
    [self buildTableView];
    [self buildBottomView];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self requestMemberCardDiscountAndBalance];
}

- (void)buildTableView
{
    self.orderSureTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64 - 60) style:UITableViewStylePlain];
    self.orderSureTableView.delegate = self;
    self.orderSureTableView.dataSource = self;
    self.orderSureTableView.backgroundColor = RGBA(244, 244, 244, 1);
    [self.orderSureTableView registerClass:[ZSTECCAddressCell class] forCellReuseIdentifier:addressCell];
    [self.orderSureTableView registerClass:[ZSTECCBookTimeCell class] forCellReuseIdentifier:bookTimeCell];
    [self.orderSureTableView registerClass:[ZSTECCDiscountCell class] forCellReuseIdentifier:discountCell];
    [self.orderSureTableView registerClass:[ZSTECCPayWayCell class] forCellReuseIdentifier:payWayCell];
    [self.orderSureTableView registerClass:[ZSTECCBalanceCell class] forCellReuseIdentifier:balanceCell];
    [self.orderSureTableView registerClass:[ZSTECCOrderProductCell class] forCellReuseIdentifier:orderProductCell];
    [self.orderSureTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:tapCell];
    [self.view addSubview:self.orderSureTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBA(244, 244, 244, 1);
    self.orderSureTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 7; //最后一个用来去掉横线
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 3)
    {
        return 2;
    }
    else if (section == 5)
    {
        if (self.bookFoodArr.count > 3)
        {
            if (self.isShowAll)
            {
                return self.bookFoodArr.count;
            }
            else
            {
                return 3;
            }
            
        }
        else
        {
            return self.bookFoodArr.count;
        }
        
    }
    else
    {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        ZSTECCAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:addressCell];
        [cell createUI];
        [cell setCellDataWithShopName:self.shopName ShopAddress:self.shopAddress];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 1)
    {
        ZSTECCBookTimeCell *cell = [tableView dequeueReusableCellWithIdentifier:bookTimeCell];
        [cell createUI];
        [cell setCellDataWithTime:self.bookTimeString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 2)
    {
        ZSTECCDiscountCell *cell = [tableView dequeueReusableCellWithIdentifier:discountCell];
        [cell createDiscountUI];
        [cell setCellDataWithDiscount:self.discountString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 3)
    {
        BOOL isTitle = NO;
        BOOL isWeChat = NO;
        if (indexPath.row == 0)
        {
            isTitle = YES;
        }
        else
        {
            isTitle = NO;
        }
        
        ZSTECCPayWayCell *cell = [tableView dequeueReusableCellWithIdentifier:payWayCell];
        [cell createUIWithIsTitle:isTitle];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (indexPath.row == 1)
        {
            isWeChat = NO;
        }else{
            [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        [cell setCellDataWithPayWay:isWeChat];
        return cell;
    }
    else if (indexPath.section == 4)
    {
        ZSTECCBalanceCell *cell = [tableView dequeueReusableCellWithIdentifier:balanceCell];
        [cell createBalanceUI];
        [cell setCellDataWithBalance:self.balanceString];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if (indexPath.section == 7){
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"defaults"];
        if (!cell) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"defaults"];
        }
        return cell;
    }
    else
    {
       
        ZSTECCOrderProductCell *cell = [tableView dequeueReusableCellWithIdentifier:orderProductCell];
        [cell createUI];
        NSDictionary *dishDic = self.bookFoodArr[indexPath.row];
        NSString *dishKey = [[dishDic allKeys] firstObject];
        NSArray *cateArr = [dishDic safeObjectForKey:dishKey];
        NSDictionary *foodDic = [[NSDictionary alloc] init];
        NSInteger count = 0;
        for (NSDictionary *cateDic in cateArr)
        {
            NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
            foodDic = [cateDic safeObjectForKey:cateKey];
            count += [[cateDic safeObjectForKey:@"dishCount"] integerValue];
        }
        NSString *foodName = [foodDic safeObjectForKey:@"dishesName"];
        NSString *foodPrice = [NSString stringWithFormat:@"%.2lf",[[foodDic safeObjectForKey:@"price"] doubleValue]];
        NSString *foodCount = [NSString stringWithFormat:@"%@",@(count)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setCellDataWithFoodName:foodName FoodCount:foodCount FoodPrice:foodPrice];
        
        if (indexPath.row == self.bookFoodArr.count - 1) {
          [cell setSeparatorInset:UIEdgeInsetsZero];
        }
        
        return cell;
        
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3 && indexPath.row == 1)
    {
        if (self.currentIndexPath.row != 1)
        {
            if (self.currentIndexPath.row >= 0)
            {
                ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:self.currentIndexPath]).checkIV.hidden = YES;
            }
            ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:indexPath]).checkIV.hidden = NO;
            self.currentIndexPath = indexPath;
            self.payWay = @"wechat";
        }
        else
        {
            self.payWay = @"alipay";
        }
        
    }
    else if (indexPath.section == 3 && indexPath.row == 2)
    {
        if (self.currentIndexPath.row != 2)
        {
            if (self.currentIndexPath.row >= 0)
            {
                ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:self.currentIndexPath]).checkIV.hidden = YES;
            }
            ((ZSTECCPayWayCell *)[tableView cellForRowAtIndexPath:indexPath]).checkIV.hidden = NO;
            self.currentIndexPath = indexPath;
            self.payWay = @"alipay";
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        return 80;
    }
    else if (indexPath.section == 1)
    {
        return 40;
    }
    else if (indexPath.section == 2)
    {
        return 40;
    }
    else if (indexPath.section == 3)
    {
        return 40;
    }
    else if (indexPath.section == 4)
    {
        return 40;
    }
    else if(indexPath.section == 6){
         return 0;
    }
    else{
        if (self.isShowAll)
        {
            if (indexPath.row == self.bookFoodArr.count)
            {
                return 40;
            }
        }
        else
        {
            if (indexPath.row == 3)
            {
                return 40;
            }
        }
        
        return 50;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 10;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sectionHeaderView = [[UIView alloc] init];
    sectionHeaderView.backgroundColor = RGBA(244, 244, 244, 1);
    return sectionHeaderView;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.orderSureTableView)
    {
        CGFloat sectionHeaderHeight = 10;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)buildBottomView
{
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, HEIGHT - 64 - 60, WIDTH, 60)];
    bottomView.backgroundColor = [UIColor whiteColor];
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(0, 0, WIDTH, 1);
    line.backgroundColor = RGBA(244, 244, 244, 1).CGColor;
    [bottomView.layer addSublayer:line];
    [self.view addSubview:bottomView];
    
    self.priceLB = [[UILabel alloc] initWithFrame:CGRectMake(20, 10, WIDTH / 2, 40)];
    self.priceLB.textColor = RGBA(250, 52, 46, 1);
    self.priceLB.textAlignment = NSTextAlignmentLeft;
    self.priceLB.font = [UIFont systemFontOfSize:16.0f];
    double price = 0;
    for (NSDictionary *oneDic in self.bookFoodArr)
    {
        NSString *key = [[oneDic allKeys] firstObject];
        NSArray *dishArr = [oneDic safeObjectForKey:key];
        for (NSDictionary *cateDic in dishArr)
        {
            NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
            double onePrice = [[[cateDic safeObjectForKey:cateKey] safeObjectForKey:@"price"] doubleValue];
            NSInteger num = [[cateDic safeObjectForKey:@"dishCount"] integerValue];
            price += onePrice * num;
        }
        
    }
    
    NSString *priceString = [NSString stringWithFormat:@"￥%.2lf",price];
    NSMutableAttributedString *nodeString = [[NSMutableAttributedString alloc] initWithString:priceString];
    [nodeString addAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]} range:NSMakeRange(0, 1)];
    self.priceLB.attributedText = nodeString;
    [bottomView addSubview:self.priceLB];
    
    _submitBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    _submitBtn.frame = CGRectMake(WIDTH / 2 + 20, 10, WIDTH / 2 - 40, 40);
    _submitBtn.backgroundColor = RGBA(250, 52, 46, 1);
    _submitBtn.layer.cornerRadius = 4.0f;
    _submitBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [_submitBtn setTitle:@"提交订单" forState:UIControlStateNormal];
    [_submitBtn addTarget:self action:@selector(submitOrder:) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:_submitBtn];
}

#pragma mark - 提交订单响应事件
- (void)submitOrder:(UIButton *)sender
{
    self.view.userInteractionEnabled = NO;
    
    if (![ZSTF3Preferences shared].UserId || [[ZSTF3Preferences shared].UserId isEqualToString:@""]) {
        
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = NO;
        controller.delegate = self;
        BaseNavgationController *navController = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navController animated:YES completion:^{
            
        }];
    }
    else
    {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在提交订单，请稍后...", nil) withImage:nil];
        NSMutableArray *dishMArr = [NSMutableArray array];
        NSMutableArray *cateMArr = [NSMutableArray array];
        NSMutableArray *countMArr = [NSMutableArray array];
        
        for (NSDictionary *oneDic in self.bookFoodArr)
        {
            NSString *dishKey = [[oneDic allKeys] firstObject];
            NSArray *dishArr = [oneDic safeObjectForKey:dishKey];
            for (NSDictionary *cateDic in dishArr)
            {
                NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
                NSDictionary *foodDic = [cateDic safeObjectForKey:cateKey];
                NSString *dishId = [foodDic safeObjectForKey:@"dishId"];
                [dishMArr addObject:dishId];
                NSString *cateId = [foodDic safeObjectForKey:@"cateId"];
                [cateMArr addObject:cateId];
                [countMArr addObject:@([[cateDic safeObjectForKey:@"dishCount"] integerValue])];
            }
        }
        
        NSString *dishesIds = [dishMArr componentsJoinedByString:@","];
        NSString *cateIds = [cateMArr componentsJoinedByString:@","];
        NSString *dishesCounts = [countMArr componentsJoinedByString:@","];
        
        NSMutableDictionary *param = [NSMutableDictionary dictionary];
        [param setValue:self.shopId forKey:@"shopId"];
        [param setValue:[ZSTF3Preferences shared].UserId forKey:@"userId"];
        [param setValue:self.shopName forKey:@"shopName"];
        [param setValue:self.bookTimeString forKey:@"bespeakTime"];
        [param setValue:@(self.bookCount) forKey:@"bespeakPersonCount"];
        [param setValue:self.selectedSexTypeString forKey:@"contactType"];
        [param setValue:self.bookManNameString forKey:@"contactName"];
        [param setValue:self.bookPhoneString forKey:@"contactTel"];
        [param setValue:self.selectedSeatTypeString forKey:@"tableType"];
        [param setValue:dishesIds forKey:@"dishesIds"];
        [param setValue:cateIds forKey:@"cateIds"];
        [param setValue:dishesCounts forKey:@"dishesCounts"];
        [param setValue:self.backUpString forKey:@"orderRemark"];
        [self.engine saveMealByParam:param];
    }
}

#pragma mark - 保存订单成功后跳转到支付页面
- (void)saveMealDidSuccess:(NSDictionary *)response
{
    self.view.userInteractionEnabled = YES;
    [TKUIUtil hiddenHUD];
    NSString *orderCode = [response safeObjectForKey:@"orderCode"];
    NSString *orderId = [response safeObjectForKey:@"orderId"];
    NSString *urlString = [NSString stringWithFormat:@"%@?orderCode=%@",payMealUrl,orderCode];
    ZSTECCPayWebViewController *payWebVC = [[ZSTECCPayWebViewController alloc] init];
    payWebVC.urlString = urlString;
    payWebVC.orderId = orderId;
    payWebVC.fromVC = self.class;
    [self.navigationController pushViewController:payWebVC animated:YES];
}

#pragma mark - 保存订单失败
- (void)saveMealDidFailed:(NSDictionary *)failResponse
{
    self.view.userInteractionEnabled = YES;
    [TKUIUtil hiddenHUD];
    NSString *failMessage = [failResponse safeObjectForKey:@"message"];
    if (failMessage && ![failMessage isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:failMessage withImage:nil];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 5)
    {
        UIView *sFooterView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
        sFooterView.backgroundColor = [UIColor whiteColor];
        
        self.showAllBtn = [[PMRepairButton alloc] init];
        self.showAllBtn.frame = CGRectMake(0, 10, WIDTH, 20);
        [self.showAllBtn setImage:[UIImage imageNamed:@"model_ecomc_down.png"] forState:UIControlStateNormal];
        [self.showAllBtn setImage:[UIImage imageNamed:@"model_ecomc_up.png"] forState:UIControlStateSelected];
        [self.showAllBtn addTarget:self action:@selector(changeIsShowAll:) forControlEvents:UIControlEventTouchUpInside];
        self.showAllBtn.selected = self.isShowAll;
        [sFooterView addSubview:self.showAllBtn];
        return sFooterView;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 5)
    {
        if (self.bookFoodArr.count <= 3)
        {
            self.showAllBtn.hidden = YES;
            return 0;
        }
        else
        {
            self.showAllBtn.hidden = NO;
            return 40;
        }
        
    }
    
    return 0;
}

- (void)changeIsShowAll:(PMRepairButton *)sender
{
    sender.selected = !sender.isSelected;
    if (sender.isSelected)
    {
        self.isShowAll = YES;
    }
    else
    {
        self.isShowAll = NO;
    }
    
    NSIndexSet *indexSet=[[NSIndexSet alloc]initWithIndex:3];
    [self.orderSureTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
    
    [self.orderSureTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)loginDidFinish
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在提交订单，请稍后...", nil) withImage:nil];
    NSMutableArray *dishMArr = [NSMutableArray array];
    NSMutableArray *cateMArr = [NSMutableArray array];
    NSMutableArray *countMArr = [NSMutableArray array];
    
    for (NSDictionary *oneDic in self.bookFoodArr)
    {
        NSString *dishKey = [[oneDic allKeys] firstObject];
        NSArray *dishArr = [oneDic safeObjectForKey:dishKey];
        for (NSDictionary *cateDic in dishArr)
        {
            NSString *cateKey = [[[cateDic allKeys] firstObject] isEqualToString:@"dishCount"] ? [[cateDic allKeys] lastObject] : [[cateDic allKeys] firstObject];
            NSDictionary *foodDic = [cateDic safeObjectForKey:cateKey];
            NSString *dishId = [foodDic safeObjectForKey:@"dishId"];
            [dishMArr addObject:dishId];
            NSString *cateId = [foodDic safeObjectForKey:@"cateId"];
            [cateMArr addObject:cateId];
            [countMArr addObject:@([[cateDic safeObjectForKey:@"dishCount"] integerValue])];
        }
    }
    
    NSString *dishesIds = [dishMArr componentsJoinedByString:@","];
    NSString *cateIds = [cateMArr componentsJoinedByString:@","];
    NSString *dishesCounts = [countMArr componentsJoinedByString:@","];
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:self.shopId forKey:@"shopId"];
    [param setValue:[ZSTF3Preferences shared].UserId forKey:@"userId"];
    [param setValue:self.shopName forKey:@"shopName"];
    [param setValue:self.bookTimeString forKey:@"bespeakTime"];
    [param setValue:@(self.bookCount) forKey:@"bespeakPersonCount"];
    [param setValue:self.selectedSexTypeString forKey:@"contactType"];
    [param setValue:self.bookManNameString forKey:@"contactName"];
    [param setValue:self.bookPhoneString forKey:@"contactTel"];
    [param setValue:self.selectedSeatTypeString forKey:@"tableType"];
    [param setValue:dishesIds forKey:@"dishesIds"];
    [param setValue:cateIds forKey:@"cateIds"];
    [param setValue:dishesCounts forKey:@"dishesCounts"];
    [param setValue:self.backUpString forKey:@"orderRemark"];
    [self.engine saveMealByParam:param];
}

#pragma mark - 请求会员卡折扣和余额
- (void)requestMemberCardDiscountAndBalance
{
    // 请求参数
    NSString *client = @"ios";
    //NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    NSString *ecid = @"600395";
    NSString *termuserId = [ZSTF3Preferences shared].UserId;
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"termuserId":termuserId};
    
    [self.engine requestMemberCardDiscountAndBalance:param];
}

// 请求会员卡折扣和余额数据成功
- (void)requestMemberCardDiscountAndBalanceSucceed:(NSDictionary *)response
{
    self.discountString = [response safeObjectForKey:@"discount"];
    self.balanceString = [response safeObjectForKey:@"balance"];
}

- (void)requestMemberCardDiscountAndBalanceFaild:(NSDictionary *)response
{
    
}


- (void)dealloc
{
    [self.engine cancelAllRequest];
}

@end
