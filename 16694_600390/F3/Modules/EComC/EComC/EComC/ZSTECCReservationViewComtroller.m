//
//  ZSTECCReservationViewComtroller.m
//  EComC
//
//  Created by qiuguian  on 15/8/7.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTECCReservationViewComtroller.h"
#import "ZSTECCFoodInfoViewController.h"
#import "ZSTECCCarouselView.h"
#import "ZSTECCShop.h"
#import "ZSTECCShopCell.h"
#import "ZSTECCShopReviewsCell.h"
#import "ZSTECCBookSeatViewController.h"
#import "ZSTECCShopMessageViewController.h"
#import "NSString+HexColor.h"

@interface ZSTECCReservationViewComtroller ()

@property (strong,nonatomic) UIViewController *currentVC;


@end


static NSString *const shopCell = @"shopCell";

@implementation ZSTECCReservationViewComtroller{
    UIView *titleBtnView;
    UIView *sitInfoView;
    UIView *shopInfoView;
    ZSTECCRoundRectView * _line;
    ZSTECCRoundRectView * _bookLine;
    ZSTECCRoundRectView * _shopLine;
    
    ZSTECCRoundRectView * bgView;
    ZSTECCRoundRectView * bottomView;
    
    
    UIImageView * rightImage;
}

#pragma mark - viewDidLoad 方法
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.view.backgroundColor =[UIColor whiteColor];
    
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,200, 44)];
//    view.backgroundColor = [UIColor redColor];
//    UILabel *labeltile = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,200, 44)];
//    labeltile.text = self.shopName;
//    NSLog(@"======labeltile.text===>%@",labeltile.text);
//    
//    labeltile.backgroundColor = [UIColor redColor];
//    [view addSubview:labeltile];
//    //labeltile.textAlignment = NSTextAlignmentCenter;
//    labeltile.textColor = [UIColor whiteColor];
//    [self.navigationItem.titleView addSubview:view];

    
    //切换视图按钮
    titleBtnView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH,50)];
    titleBtnView.backgroundColor =[UIColor whiteColor];
    [self initTiltBtnView];
    [self.view addSubview:titleBtnView];

//    
//    //订座信息 视图
//    sitInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-64-50)];
//    sitInfoView.backgroundColor =RGBCOLOR(246, 246, 246);
////    [self initSitInfoView];
//    [self.view addSubview:sitInfoView];
//    
//    //店铺信息 视图
//    shopInfoView = [[UIView alloc] initWithFrame:CGRectMake(0, 50, WIDTH, HEIGHT-64-50)];
//    shopInfoView.backgroundColor =[UIColor whiteColor];
//    shopInfoView.hidden = YES;
//    [self.view addSubview:shopInfoView];
    
    [self buildChildVC];
}

#pragma mark - 初始化切换视图按钮
-(void)initTiltBtnView{
   
    //订座信息 按钮
    self.bookInfoBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 5, WIDTH / 2, 50-4-5)];
    self.bookInfoBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    [self.bookInfoBtn setTitleColor:[@"#ff750f" colorValue] forState:UIControlStateNormal];
    self.bookInfoBtn.tag = 1;
    self.bookInfoBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.bookInfoBtn setTitle:@"订座信息" forState:UIControlStateNormal];
    [self.bookInfoBtn addTarget:self action:@selector(bookInfoBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [titleBtnView addSubview:self.bookInfoBtn];
    
    //订座信息 底线
    _bookLine = [[ZSTECCRoundRectView alloc]initWithPoint:CGPointMake(0, 50-4) toPoint:CGPointMake(WIDTH/2, 40) borderWidth:4.0f borderColor:[@"#ff750f" colorValue]];
    [titleBtnView addSubview:_bookLine];
    
    //店铺信息 按钮
    self.shopInfoBtn = [[UIButton alloc] initWithFrame:CGRectMake(160, 5, WIDTH / 2, 50-4-5)];
    self.shopInfoBtn.titleLabel.textAlignment = UITextAlignmentCenter;
    self.shopInfoBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.shopInfoBtn setTitleColor:[@"#333333" colorValue] forState:UIControlStateNormal];
    self.shopInfoBtn.tag =2 ;
    [self.shopInfoBtn setTitle:@"商家信息" forState:UIControlStateNormal];
    [self.shopInfoBtn addTarget:self action:@selector(shopInfoAction) forControlEvents:UIControlEventTouchUpInside];
    [titleBtnView addSubview: self.shopInfoBtn];

    //订座信息 底线
    _shopLine = [[ZSTECCRoundRectView alloc] initWithPoint:CGPointMake(160, 50 - 4) toPoint:CGPointMake(320, 40) borderWidth:4.0f borderColor:[@"#ff750f" colorValue]];
    _shopLine.hidden = YES;
    [titleBtnView addSubview:_shopLine];
    
}


#pragma  mark - 初始化订座信息视图
-(void)initSitInfoView{
    
    //底部视图
    bottomView = [[ZSTECCRoundRectView alloc]initWithFrame:CGRectMake(0, HEIGHT-64-50-60, 320,60) radius:0.0 borderWidth:0.0f borderColor:[UIColor grayColor]];
    bottomView.backgroundColor = RGBA(252, 252, 252, 1);
    [sitInfoView addSubview:bottomView];
    
    //订座 按钮
    UIButton *bookBtn1 = [[UIButton alloc] initWithFrame:CGRectMake(10,10, 130, 40)];
    bookBtn1.titleLabel.textAlignment = UITextAlignmentCenter;
    bookBtn1.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    bookBtn1.titleLabel.textColor = [UIColor whiteColor];
    bookBtn1.backgroundColor = [UIColor redColor];
    bookBtn1.layer.cornerRadius =5;
    [bookBtn1 setTitle:@"订座" forState:UIControlStateNormal];
    bookBtn1.tag =3;
    [bookBtn1 addTarget:self action:@selector(bookSitAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:bookBtn1];
    
    //订位点餐 按钮
    UIButton *bookBtn2 = [[UIButton alloc] initWithFrame:CGRectMake(180, 10, 130, 40)];
    bookBtn2.titleLabel.textAlignment = UITextAlignmentCenter;
    bookBtn2.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [bookBtn2 setBackgroundColor:[UIColor blackColor]];
    bookBtn2.backgroundColor = [UIColor lightGrayColor];
    bookBtn2.layer.cornerRadius =5;
    bookBtn2.tag = 4;
    [bookBtn2 addTarget:self action:@selector(foodInfoAction) forControlEvents:UIControlEventTouchUpInside];
    [bookBtn2 setTitle:@"订位点餐" forState:UIControlStateNormal];
    [bottomView addSubview:bookBtn2];

}

#pragma mark - 切换订座信息视图
-(void)bookInfoBtnAction{
    //订座信息
    sitInfoView.hidden = NO;
    _bookLine.hidden = NO;
    [self.bookInfoBtn setTitleColor:[@"#ff750f" colorValue] forState:UIControlStateNormal];
    
    //店铺信息
    shopInfoView.hidden = YES;
    _shopLine.hidden = YES;
    [self.shopInfoBtn setTitleColor:[@"#333333" colorValue] forState:UIControlStateNormal];
    
    if (![self.currentVC isKindOfClass:[ZSTECCBookSeatViewController class]])
    {
        [self transitionFromViewController:self.currentVC toViewController:self.childViewControllers[0] duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
            self.currentVC = self.childViewControllers[0];
        } completion:^(BOOL finished) {
            
        }];
    }
    
}

#pragma mark - 切换店铺信息视图
-(void)shopInfoAction{
    //订座信息
    sitInfoView.hidden = YES;
    _bookLine.hidden = YES;
    [self.bookInfoBtn setTitleColor:[@"#333333" colorValue] forState:UIControlStateNormal];
    
    //店铺信息
    shopInfoView.hidden = NO;
    _shopLine.hidden = NO;
    [self.shopInfoBtn setTitleColor:[@"#ff750f" colorValue] forState:UIControlStateNormal];
    
    if (![self.currentVC isKindOfClass:[ZSTECCShopMessageViewController class]])
    {
        [self transitionFromViewController:self.currentVC toViewController:self.childViewControllers[1] duration:0.3 options:UIViewAnimationOptionTransitionNone animations:^{
            self.currentVC = self.childViewControllers[1];
        } completion:^(BOOL finished) {
            
        }];
    }
    
}


- (void)buildChildVC
{
    ZSTECCBookSeatViewController *bookSeatMessage = [[ZSTECCBookSeatViewController alloc] init];
    bookSeatMessage.fatherVC = self;
    bookSeatMessage.shopId = self.shopId;
    bookSeatMessage.shopAddress = self.shopAddress;
    bookSeatMessage.shopName = self.shopName;
    [self addChildViewController:bookSeatMessage];
    self.currentVC = bookSeatMessage;
    NSLog(@"value-->%@",[NSValue valueWithCGRect:bookSeatMessage.view.frame]);
    [self.view addSubview:bookSeatMessage.view];
    
    ZSTECCShopMessageViewController *shopMessage = [[ZSTECCShopMessageViewController alloc] init];
//    shopMessage.fatherVC = self;
    shopMessage.shopId = self.shopId;
    [self addChildViewController:shopMessage];
}


#pragma mark - 确定订位
-(void)bookSitAction{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"请写完整资料，谢谢！" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - 确定订位点餐
-(void)foodInfoAction{
    ZSTECCFoodInfoViewController *foodView = [[ZSTECCFoodInfoViewController alloc] init];
    foodView.shopId = self.shopId;
    [self.navigationController pushViewController:foodView animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}


@end
