//
//  ZSTF3Engine+EComC.m
//  EComC
//
//  Created by pmit on 15/8/5.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "ZSTF3Engine+EComC.h"
#import "ZSTGlobal+EComC.h"


@implementation ZSTF3Engine (EComC)

#pragma mark - 点餐系统
#pragma mark - 获取主页轮播图
- (void)getEcomCCarousel
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[ZSTF3Preferences shared].UserId forKey:@"userId"];
    [params setSafeObject:@"1" forKey:@"curPage"];
    [params setSafeObject:@"5" forKey:@"pageSize"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getBannerURL params:params target:self selector:@selector(getEcomCCarouselResponse:userInfo:)];
}
- (void)getEcomCCarouselResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
   
    if (response.resultCode == ZSTResultCode_OK) {
        NSDictionary *carouseles = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getEcomCCarouselDidSucceed:)]) {
            [self.delegate getEcomCCarouselDidSucceed:carouseles];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getEcomCCarouselDidFailed:)]) {
            [self.delegate getEcomCCarouselDidFailed:response.resultCode];
            
            
        }
    }
}


#pragma mark - 获取个人订单列表信息
/**
 *  <#Description#>
 *
 *  @param userId      用户标识
 *  @param orderStatus 订单状态(1:待消费,2:已消费)
 */
-(void)getEcomCOrderListByUserId:(NSString *)userId andStatus:(NSString *)orderStatus curPage:(NSString *)curPage pageSize:(NSString *)pageSize{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:orderStatus forKey:@"orderStatus"];
    [params setSafeObject:userId forKey:@"userId"];
    [params setSafeObject:curPage forKey:@"curPage"];
    [params setSafeObject:pageSize forKey:@"pageSize"];

//    [[ZSTCommunicator shared] openAPIPostToPath:[NSString stringWithFormat:listOrderURL]
//                                         params:params
//                                         target:self
//                                       selector:@selector(getEcomCOrderListResponse:userInfo:)];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:listOrderURL params:params target:self selector:@selector(getEcomCOrderListResponse:userInfo:)];
}

#pragma mark 结果返回
-(void)getEcomCOrderListResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *orderList = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getEcomCOrderListDidSucceed:)]) {
            [self.delegate getEcomCOrderListDidSucceed:orderList];
        }
        
    }else{
       
        if ([self isValidDelegateForSelector:@selector(getEcomCOrderListDidFailed:)]) {
            [self.delegate getEcomCOrderListDidFailed:response.resultCode];
        }
    
    }

}


#pragma mark - 根据经纬度获取当前位置
/**
 *  <#Description#>
 *
 *  @param userId 用户标识
 *  @param lat    纬度
 *  @param lng    经度
 */
-(void)getShopAddressInfoByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng{
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:userId forKey:@"userId"];
    [params setSafeObject:lat forKey:@"lat"];
    [params setSafeObject:lng forKey:@"lng"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getAddressInfoURL
                                         params:params
                                         target:self
                                       selector:@selector(getShopAddressInfoResponse:userInfo:)];
    

}

#pragma mark 结果返回
-(void)getShopAddressInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo{

    NSLog(@"userInfo --> %@",response.jsonResponse);
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *addressInfo = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getShopAddressInfoDidSucceed:)]) {
            [self.delegate getShopAddressInfoDidSucceed:addressInfo];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getShopAddressInfoDidFailed:)]) {
            [self.delegate getShopAddressInfoDidFailed:response.resultCode];
        }
        
    }
}



#pragma mark - 获取店铺列表
/**
 *  <#Description#>
 *
 *  @param userId 用户标识
 *  @param lat    纬度
 *  @param lng    经度
 */
-(void)getShopListByUserId:(NSString *)userId andLat:(NSString *)lat andLng:(NSString *)lng andCurrentPage:(NSInteger)currentPage PageSize:(NSInteger)pageSize
{

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:userId forKey:@"userId"];
    [params setSafeObject:lat forKey:@"lat"];
    [params setSafeObject:lng forKey:@"lng"];
    [params setSafeObject:@(currentPage) forKey:@"curPage"];
    [params setSafeObject:@(pageSize) forKey:@"pageSize"];
    
//    [[ZSTCommunicator shared] openAPIPostToPath:getShopListURL
//                                         params:params
//                                         target:self
//                                       selector:@selector(getShopAddressInfoResponse:userInfo:)];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopDCListUrl params:params target:self selector:@selector(getShopListResponse:userInfo:)];
}


#pragma mark 结果返回
-(void)getShopListResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *shopList = [response.jsonResponse safeObjectForKey:@"data"];
        
        if ([self isValidDelegateForSelector:@selector(getShopListDidSucceed:)]) {
            [self.delegate getShopListDidSucceed:shopList];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getShopListDidFailed:)]) {
            [self.delegate getShopListDidFailed:response.resultCode];
        }
        
    }
}

- (void)getInitShopInfoByShopId:(NSString *)shopId
{
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setSafeObject:shopId forKey:@"shopId"];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getInitOrderInfoUrl params:param target:self selector:@selector(getInitshopInfoResponse:userInfo:)];
}

- (void)getInitshopInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *orderInitDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getInitOrderInfoSuccessd:)])
        {
            [self.delegate getInitOrderInfoSuccessd:orderInitDic];
        }
        else
        {
            [self.delegate getInitOrderInfoFailed:response.resultCode];
        }
    }
}

- (void)getShopDishesByShopId:(NSString *)shopId AndUserId:(NSString *)userId
{
    NSDictionary *param = @{@"shopId":shopId,@"userId":userId};
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getSelectedDishesUrl params:param target:self selector:@selector(getShopDishesResponse:userInfo:)];
}

- (void)getShopDishesResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSLog(@"response --> %@",response.jsonResponse);
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *dishesDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getDishesInfoSuccessed:)])
        {
            [self.delegate getDishesInfoSuccessed:dishesDic];
        }
        else
        {
            [self.delegate getDishesInfoFailed:response.resultCode];
        }
    }
}


#pragma mark - 获取店铺信息
-(void)getShopDetailByShopId:(NSString *)shopId{
    
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:shopId forKey:@"shopId"];
    
//    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopDetailUrl params:param target:self selector:@selector(getShopDetailResponse:userInfo:)];
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopDetailUrl params:param target:self selector:@selector(getShopDetailResponse:userInfo:)];

}

#pragma mark 结果返回
-(void)getShopDetailResponse:(ZSTResponse *)response userInfo:(id)userInfo{

    
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *shopInfoDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getShopDetailDidSucceed:)])
        {
            [self.delegate getShopDetailDidSucceed:shopInfoDic];
        }
        else
        {
            [self.delegate getShopDetailDidFailed:response.resultCode];
        }
    }
}

#pragma mark - 获取店铺信息 用户评价列表
-(void)getShopReviewListByShopId:(NSString *)shopId andCurPage:(NSString *)curPage andPageSize:(NSString *)pageSize{
    
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:shopId forKey:@"shopId"];
    [param setValue:curPage forKey:@"currentPage"];
    [param setValue:pageSize forKey:@"pageSize"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopReviewListUrl params:param target:self selector:@selector(getshopReviewListResponse:userInfo:)];
    
}

#pragma mark 结果返回
-(void)getshopReviewListResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK)
    {
        NSDictionary *shopReviewDic = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getShopReviewListDidSucceed:)])
        {
            [self.delegate getShopReviewListDidSucceed:shopReviewDic];
        }
    }else{
        [self.delegate getShopReviewListDidFailed:response.resultCode];
    }
    
}

- (void)saveOrderSeatMealByParam:(NSDictionary *)param
{
    [[ZSTCommunicator shared] openAPIPostDCToPaths:saveOrderSeatMealUrl params:param target:self selector:@selector(saveOrderSeatMealResponse:UserInfo:)];
}

- (void)saveOrderSeatMealResponse:(ZSTResponse *)response UserInfo:(id)userInfo
{
    NSLog(@"saveDic --> %@",response.jsonResponse);
    if (response.resultCode == ZSTResultCode_OK)
    {
        if ([self isValidDelegateForSelector:@selector(saveSeatDidSuccess:)])
        {
            [self.delegate saveSeatDidSuccess:[response.jsonResponse safeObjectForKey:@"data"]];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(saveSeatDidFailed:)])
        {
            [self.delegate saveSeatDidFailed:response.jsonResponse];
        }
    }
}

- (void)saveMealByParam:(NSDictionary *)param
{
    [[ZSTCommunicator shared] openAPIPostDCToPaths:saveMealUrl params:param target:self selector:@selector(saveOrderMealResponse:UserInfo:)];
}

- (void)saveOrderMealResponse:(ZSTResponse *)response UserInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK)
    {
        if ([self isValidDelegateForSelector:@selector(saveMealDidSuccess:)])
        {
            [self.delegate saveMealDidSuccess:[response.jsonResponse safeObjectForKey:@"data"]];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(saveMealDidFailed:)])
        {
            [self.delegate saveMealDidFailed:response.jsonResponse];
        }
    }
}

#pragma mark - 请求会员卡折扣和余额数据
- (void)requestMemberCardDiscountAndBalance:(NSDictionary *)param
{
    // cardDiscountAndBalance
    [[ZSTCommunicator shared] openAPIPostMemberCardToPaths:getShopReviewListUrl params:param target:self selector:@selector(requestMemberCardDiscountAndBalanceResponse:UserInfo:)];
}

- (void)requestMemberCardDiscountAndBalanceResponse:(ZSTResponse *)response UserInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK)
    {
        if ([self isValidDelegateForSelector:@selector(requestMemberCardDiscountAndBalanceSucceed:)])
        {
            [self.delegate requestMemberCardDiscountAndBalanceSucceed:[response.jsonResponse safeObjectForKey:@"data"]];
        }
    }
    else
    {
        if ([self isValidDelegateForSelector:@selector(requestMemberCardDiscountAndBalanceFaild:)])
        {
            [self.delegate requestMemberCardDiscountAndBalanceFaild:response.jsonResponse];
        }
    }
}


#pragma mark - 订单详情
-(void)getOrderInfoByOrderId:(NSString *)orderId andUserId:(NSString *)userId{
    
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:orderId forKey:@"id"];
    [param setValue:userId forKey:@"userId"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getOrderInfoUrl params:param target:self selector:@selector(getOrderInfoResponse:userInfo:)];
    
}

#pragma mark 结果返回
-(void)getOrderInfoResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *orderInfo = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getOrderInfoDidSucceed:)]) {
            [self.delegate getOrderInfoDidSucceed:orderInfo];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getOrderInfoDidFailed:)]) {
            [self.delegate getOrderInfoDidFailed:response.resultCode];
        }
        
    }
}

#pragma mark - 发表评论   此点餐系统没用到此方法
-(void)sendEvaluateByUserId:(NSString *)userId content:(NSString *)content shopId:(NSString *)shopId starRate:(NSString *)starRate data:(NSData *)data orderId:(NSString *)orderId
{
    
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:userId forKey:@"userId"];
    [param setValue:content forKey:@"evaluateDesc"];
    [param setValue:shopId forKey:@"shopId"];
    [param setValue:starRate forKey:@"evaluateStar"];
    [param setValue:orderId forKey:@"orderId"];
    
    [[ZSTCommunicator shared] uploadFileTopath:sendEvaluateUrl
                                        params:param
                                          data:data
                                        suffix:@"png"
                                        target:self
                                      selector:@selector(sendEvaluateRespone:userInfo:)
                                      userInfo:nil];
    
}

#pragma mark 结果返回
-(void)sendEvaluateRespone:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *orderInfo = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(sendEvaluateDidSucceed:)]) {
            [self.delegate sendEvaluateDidSucceed:orderInfo];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(sendEvaluateDidFailed:)]) {
            [self.delegate sendEvaluateDidFailed:response.resultCode];
        }
        
    }
    
}


#pragma mark - 获取订单明细
-(void)getListOrderDetailByUserId:(NSString *)userId orderId:(NSString *)orderId curPage:(NSString *)curPage pageSize:(NSString *)pageSize{
    
    NSDictionary *param = [[NSMutableDictionary alloc] init];
    [param setValue:userId forKey:@"userId"];
    [param setValue:curPage forKey:@"curPage"];
    [param setValue:pageSize forKey:@"pageSize"];
    [param setValue:orderId forKey:@"orderId"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:listOrderDetailUrl params:param target:self selector:@selector(getListOrderDetailResponse:userInfo:)];
}

#pragma mark 结果返回
-(void)getListOrderDetailResponse:(ZSTResponse *)response userInfo:(id)userInfo{
    
    if (response.resultCode == ZSTResultCode_OK) {
        
        NSDictionary *orderInfo = [response.jsonResponse safeObjectForKey:@"data"];
        if ([self isValidDelegateForSelector:@selector(getListOrderDetailDidSucceed:)]) {
            [self.delegate getListOrderDetailDidSucceed:orderInfo];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getListOrderDetailFailed:)]) {
            [self.delegate getListOrderDetailFailed:response.resultCode];
        }
        
    }
}

#pragma mark - 取消订单
-(void)cancelOrderByOrderId:(NSString *)orderId andUserId:(NSString *)userId{
    
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setValue:orderId forKey:@"id"];
    [params setValue:userId forKey:@"userId"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:cancelOrderUrl params:params target:self selector:@selector(cancelOrderResponse:userInfo:)];

}

#pragma mark 结果返回
-(void)cancelOrderResponse:(ZSTResponse *)response userInfo:(NSString *)userInfo{

    if (response.resultCode == ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(cancelOrderDidSucceed:)]) {
            [self.delegate cancelOrderDidSucceed:response.jsonResponse];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(cancelOrderDidFailed:)]) {
            [self.delegate cancelOrderDidFailed:response.resultCode];
        }
        
    }

}

- (void)getMapWithDictionary:(NSDictionary *)param
{
    [[ZSTCommunicator shared] openAPIPostDCToPaths:showMap params:param target:self selector:@selector(getMapUrlResponse:userInfo:)];
}

- (void)getMapUrlResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(getMapSuccess:)]) {
            [self.delegate getMapSuccess:response.jsonResponse];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getMapFailed:)]) {
            [self.delegate getMapFailed:response.resultCode];
        }
        
    }
}

- (void)searchShopListWithDictionary:(NSDictionary *)param
{
    [[ZSTCommunicator shared] openAPIPostDCToPaths:getShopDCListUrl params:param target:self selector:@selector(getShopListResponse:userInfo:)];
}


#pragma mark - 查询数据资源是否有更新
-(void)checkDataUpdate:(NSString *)carouselCacheTime andShopTime:(NSString *)shopListCacheTime{
    NSMutableDictionary *params = [[NSMutableDictionary alloc] init];
    [params setObject:carouselCacheTime forKey:@"carouselCacheTime"];
    [params setObject:shopListCacheTime forKey:@"shopListCacheTime"];
    
    [[ZSTCommunicator shared] openAPIPostDCToPaths:checkUpdateUrl params:params target:self selector:@selector(checkDataUpdate:userInfo:)];
    
}

-(void)checkDataUpdate:(ZSTResponse *)response userInfo:(id)userInfo{

    if (response.resultCode == ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(checkUpdateDidSucceed:)]) {
            [self.delegate checkUpdateDidSucceed:response.jsonResponse];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(getMapFailed:)]) {
            [self.delegate checkUpdateDidFailed:response.jsonResponse];
        }
    }

}


#pragma mark - 定座点餐按钮之前验证接口
-(void)validateReserveMealWithDictionary:(NSDictionary *)param{

   [[ZSTCommunicator shared] openAPIPostDCToPaths:validateReserveMealUrl params:param target:self selector:@selector(validateReserveMealResponse:userInfo:)];

}

-(void)validateReserveMealResponse:(ZSTResponse *)response userInfo:(id)userInfo{
  
    if (response.resultCode == ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(validateReserveMealDidSucceed:)]) {
            [self.delegate validateReserveMealDidSucceed:response.jsonResponse];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(validateReserveMealDidFailed:)]) {
            [self.delegate validateReserveMealDidFailed:response.jsonResponse];
        }
        
    }


}


#pragma mark - 去付款按钮之前验证接口
-(void) validateGotoPayByOrderId:(NSString *)orderId{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc] init];
    [param setObject:orderId forKey:@"orderId"];

   [[ZSTCommunicator shared] openAPIPostDCToPaths:validateGotoPayUrl params:param target:self selector:@selector(validateGotoPayResponse:userInfo:)];
}


-(void)validateGotoPayResponse:(ZSTResponse *)response userInfo:(id)userInfo{

    if (response.resultCode == ZSTResultCode_OK) {
        
        if ([self isValidDelegateForSelector:@selector(validateGotoPayDidSucceed:)]) {
            [self.delegate validateGotoPayDidSucceed:response.jsonResponse];
        }
        
    }else{
        
        if ([self isValidDelegateForSelector:@selector(validateGotoPayDidFailed:)]) {
            [self.delegate validateGotoPayDidFailed:response.jsonResponse];
        }
        
    }

}
@end
