//
//  ZSTF3Engine+CoverA.m
//  CoverA
//
//  Created by xuhuijun on 13-1-16.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+CoverA.h"
#import "ZSTGlobal+CoverA.h"
#import "ZSTCommunicator.h"


@implementation ZSTF3Engine (CoverA)

- (void)getCoverAmessage
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:[ZSTF3Preferences shared].ECECCID forKey:@"ECID"];
    [params setSafeObject:[ZSTF3Preferences shared].loginMsisdn forKey:@"Msisdn"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:GetCoverMessage
                                         params:params
                                         target:self
                                       selector:@selector(getCoverAMessageResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:YES];
}


- (void)getCoverAMessageResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id coverAMessages = [response.jsonResponse safeObjectForKey:@"info"];
        if (![coverAMessages isKindOfClass:[NSArray class]]) {
            coverAMessages = [NSArray array];
        }
        if ([self isValidDelegateForSelector:@selector(getCoverAMessageDidSucceed:)]) {
            [self.delegate getCoverAMessageDidSucceed:coverAMessages];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(getCoverAMessageDidFailed:)]) {
            [self.delegate getCoverAMessageDidFailed:response.resultCode];
        }
    }
}

@end
