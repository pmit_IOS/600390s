//
//  ZSTCoverAView.m
//  F3
//
//  Created by xuhuijun on 13-1-14.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTCoverAView.h"
#import "ZSTUtils.h"
#import "ZSTF3Engine+CoverA.h"
#import "ZSTModuleAddition.h"
#import "ChromeProgressBar.h"

#define imageBufer 5
#define progressViewHeight 2

@interface ZSTCoverAView ()

@property (nonatomic) BOOL animationInCurse;

- (void) _startInternetAnimations:(NSArray *)urls;
@end


@implementation ZSTCoverAView


@synthesize imagesArray,timeTransition, isLoop,imageUrls,tapRecognizer,recognizer;
@synthesize animationInCurse, delegate,progressView,perView,remainTime,currentNum,titleLabel,descriptionLabel,coverArray;

- (void)dealloc
{
    [self.recognizer release];
    [self.tapRecognizer release];
    
    [imagesArray release];
    [imageUrls release];
    [progressView release];
    [perView release];
    [titleLabel release];
    [descriptionLabel release];
    [coverArray release];
    [super dealloc];
    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
        _window.windowLevel = UIWindowLevelNormal;
        _window.hidden = YES;
        
        self.layer.masksToBounds = YES;
        
        isAnimating = YES;
        
        self.perView = [[UIView alloc] initWithFrame:self.frame];
        self.perView.userInteractionEnabled = YES;
        [_window addSubview:self.perView];
        
        self.tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapFrom:)];
        self.tapRecognizer.numberOfTapsRequired = 1;
        [self addGestureRecognizer:self.tapRecognizer];
        
        self.recognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeFrom:)];
        [self.recognizer setDirection:(UISwipeGestureRecognizerDirectionLeft)];
        [self addGestureRecognizer:self.recognizer];
        
        self.swipeImage = [[UIImageView alloc] initWithImage:ZSTModuleImage(@"module_covera_flip.png")];
        self.swipeImage.frame = CGRectMake(320-141, 335+(iPhone5 ? 88:0), 141, 24);
        self.swipeImage.hidden = YES;
        [self.perView addSubview:self.swipeImage];
        
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 360+(iPhone5 ? 88:0), 304, 30)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.text = @"";
        self.titleLabel.textColor = [UIColor whiteColor];
        self.titleLabel.font = [UIFont systemFontOfSize:18];
        [self.perView addSubview:self.titleLabel];
        
        self.descriptionLabel = [[UILabel alloc] initWithFrame:CGRectMake(8, 395+(iPhone5 ? 88:0), 304, 70)];
        self.descriptionLabel.backgroundColor = [UIColor clearColor];
        self.descriptionLabel.text = @"";
        self.descriptionLabel.numberOfLines = 0;
        self.descriptionLabel.textColor = [UIColor whiteColor];
        self.descriptionLabel.font = [UIFont systemFontOfSize:13];
        [self.perView addSubview:self.descriptionLabel];
        
        self.progressView = [[ChromeProgressBar alloc] initWithFrame:CGRectMake(0, 480-progressViewHeight+(iPhone5 ? 88:0), 320, progressViewHeight)];
        [self.perView addSubview:self.progressView];
    }
    return self;
}
- (void) alignLabelWithTop:(UILabel *)label {
    CGSize maxSize = CGSizeMake(label.frame.size.width, 70);
    label.adjustsFontSizeToFitWidth = NO;
    
    CGSize actualSize = [label.text sizeWithFont:label.font constrainedToSize:maxSize lineBreakMode:label.lineBreakMode];
    CGRect rect = label.frame;
    rect.size.height = actualSize.height;
    label.frame = rect;
}

- (void) animateWithURLs:(NSArray *)urls transitionDuration:(float)duration loop:(BOOL)shouldLoop
{
    self.imagesArray      = [[NSMutableArray alloc] init];
    self.imageUrls        = [NSMutableArray arrayWithArray:urls];
    self.timeTransition   = duration;
    self.isLoop           = shouldLoop;
    self.animationInCurse = NO;
    
    self.layer.masksToBounds = YES;
    
    if (urls) {
        [self performSelector:@selector(_startInternetAnimations:) withObject:urls];
    } else {
        
        [self performSelector:@selector(_startdefaultInternetAnimations) withObject:nil];
    }
}

- (void) _startInternetAnimations:(NSArray *)urls
{
    NSInteger bufferSize = 0;
    if ([urls count]) {
        bufferSize = (imageBufer < urls.count) ? imageBufer : urls.count;
    }
    
    for (int i = 0; i < bufferSize; i ++) {
        
        UIImage *image = ZSTModuleImage(CORVER_IMG);
        
        TKAsynImageView *coverImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(self.frame.origin.x,
                                                                                            self.frame.origin.y,
                                                                                            self.frame.size.width,
                                                                                            self.frame.size.height-progressViewHeight)];
        coverImageView.backgroundColor = [UIColor lightGrayColor];
        coverImageView.defaultImage = image;
        coverImageView.adjustsImageWhenHighlighted = NO;
        coverImageView.asynImageDelegate = self;
        coverImageView.callbackOnSetImage = self;
        [coverImageView clear];
        coverImageView.url = [NSURL URLWithString:[urls objectAtIndex:i]];
        [coverImageView loadImage];
        [self.imagesArray addObject:coverImageView];
        [coverImageView release];
    }
    
    [self performSelector:@selector(reSet:) withObject:[NSNumber numberWithInt:0]];
}

- (void) _startdefaultInternetAnimations
{
    UIImage *image = ZSTModuleImage(CORVER_IMG);
    
    TKAsynImageView *coverImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(self.frame.origin.x,
                                                                                        self.frame.origin.y,
                                                                                        self.frame.size.width,
                                                                                        self.frame.size.height-progressViewHeight)];
    coverImageView.backgroundColor = [UIColor lightGrayColor];
    coverImageView.defaultImage = image;
    coverImageView.adjustsImageWhenHighlighted = NO;
    coverImageView.asynImageDelegate = self;
    coverImageView.callbackOnSetImage = self;
    //        [coverImageView clear];
    //        coverImageView.url = [NSURL URLWithString:[urls objectAtIndex:i]];
    //        [coverImageView loadImage];
    [self.imagesArray addObject:coverImageView];
    [coverImageView release];
    
    [self performSelector:@selector(reSet:) withObject:[NSNumber numberWithInt:0]];
}


- (void)_removeImage:(TKAsynImageView *)asynImage
{
    [asynImage removeFromSuperview];
    asynImage.alpha = 1.0;
}

- (void)reSet:(NSNumber*)num
{
    self.swipeImage.hidden = NO;
    if ([self.coverArray count]) {
        if ([[[self.coverArray objectAtIndex:[num intValue]] safeObjectForKey:@"Title"] isKindOfClass:[NSString class]])
        {
            self.titleLabel.text = [[self.coverArray objectAtIndex:[num intValue]] safeObjectForKey:@"Title"];
        }
        else
        {
            self.titleLabel.text = @"";
        }
        
        if ([[[self.coverArray objectAtIndex:[num intValue]] safeObjectForKey:@"Description"] isKindOfClass:[NSString class]])
        {
            self.descriptionLabel.text = [[self.coverArray objectAtIndex:[num intValue]] safeObjectForKey:@"Description"];
        }
        else
        {
            self.descriptionLabel.text = @"";
        }
        
        
        [self alignLabelWithTop:self.descriptionLabel];
    }
    
    self.currentNum = num;
    if (self.imagesArray.count < 1) {
        return;
    }
    TKAsynImageView *currentImage = [self.imagesArray objectAtIndex:[num intValue]];
    TKAsynImageView *perImage = [self.imagesArray objectAtIndex:(([num intValue]-1) == -1 ? ([self.imagesArray count]-1) : ([num intValue]-1))];
    [self addSubview:currentImage];
    [self bringSubviewToFront:perImage];
    
    if ([self.coverArray count] != 1 && [self.imagesArray count] != 1) {
        [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            perImage.alpha = 0.0;
        } completion:^(BOOL finished) {
            [self performSelector:@selector(_removeImage:) withObject:perImage afterDelay:0];
            
        }];
        
        if (_animationTimer != nil) {
            [_animationTimer invalidate];
            _animationTimer = nil;
        }
        self.remainTime = 0;
        [self.progressView setProgress:0 animated:YES];
        _animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(animatedProgress:) userInfo:num repeats:YES];
    }
}

- (void)animatedProgress:(NSTimer*)timer
{
    self.remainTime = self.remainTime + 0.1;
    float totalTime = self.timeTransition *19;
    
    if (self.remainTime/totalTime >= 1.0) {
        
        if ([timer.userInfo intValue] != ([self.imagesArray count]-1)) {
            [self performSelector:@selector(reSet:) withObject:[NSNumber numberWithInt:([timer.userInfo intValue]+1)] afterDelay:0];
        }else if (self.isLoop){
            [self performSelector:@selector(reSet:)  withObject:[NSNumber numberWithInt:0] afterDelay:0];
        }else{
            [self dismissCoverA];
        }
    }
    [self.progressView setProgress:self.remainTime/totalTime animated:YES];
    
}


#pragma mark - HJManagedImageVDelegate

-(void) managedImageSet:(HJManagedImageV*)mi
{
    //    NSUInteger index = [self.imageUrls indexOfObject:[NSString stringWithFormat:@"%@", mi.url]];
    //    NSString *imagePath = [[ZSTUtils pathForCover] stringByAppendingPathComponent:[NSString stringWithFormat:@"cover_%d.png",index]];
    //    NSData* data = UIImagePNGRepresentation(mi.image);
    //    [data writeToFile:imagePath atomically:YES];
}

#pragma mark - TKAsynImageViewDelegate

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
    
}

- (void)handleTapFrom:(UIGestureRecognizer *)gesture
{
    if ([self.delegate respondsToSelector:@selector(coverAViewDidClicked:)]) {
        [self.delegate coverAViewDidClicked:self];
    }
    if (isAnimating) {
        isAnimating = NO;
        [self suspend];
    }else{
        isAnimating = YES;
        [self reStart];
    }
}

- (void)suspend
{
    if (_animationTimer != nil) {
        [_animationTimer invalidate];
        _animationTimer = nil;
    }
}

- (void)reStart
{
    _animationTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self selector:@selector(animatedProgress:) userInfo:self.currentNum repeats:YES];
    
}



- (void)handleSwipeFrom:(UIGestureRecognizer *)gesture
{
    
    if ([self.delegate respondsToSelector:@selector(coverAViewDidSlided:)]) {
        [self.delegate coverAViewDidSlided:self];
    }
    [self dismissCoverA];
}

- (void) coverAShow
{
    [_window insertSubview:self belowSubview:self.perView];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
    NSString *path = [paths    objectAtIndex:0];
    NSString *filename = [path stringByAppendingPathComponent:@"CoverA.plist"];
    NSMutableArray *data = [[NSMutableArray alloc] initWithContentsOfFile:filename];
    self.coverArray = [(NSArray *)data retain];
    
    _window.windowLevel = UIWindowLevelStatusBar;
    _window.hidden = NO;
    [self.engine getCoverAmessage];
    
}


- (void)dismissCoverA
{
    [UIView animateWithDuration:0.5 animations:^{
        _window .frame = CGRectMake(-480, 0, 320, 480);
        
    } completion:^(BOOL finished) {
        
        [self removeFromSuperview];
        _window.windowLevel = UIWindowLevelNormal;
        _window.hidden = YES;
        self.swipeImage.hidden = YES;
        [self suspend];
    }];
    
    
    if ([self.delegate respondsToSelector:@selector(coverAViewDidDismiss:)]) {
        [self.delegate coverAViewDidDismiss:self];
    }
}

#pragma mark - ZSTF3EngineCoverADelegate

- (void)getCoverAMessageDidSucceed:(NSArray *)coverAArray
{
    self.imageUrls = NULL;
    if ([coverAArray count]) {
        
        self.imageUrls = [NSMutableArray array];
        self.coverArray = [coverAArray retain];
        
        
        NSArray *paths=NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,NSUserDomainMask,YES);
        NSString *path=[paths    objectAtIndex:0];
        NSString *filename=[path stringByAppendingPathComponent:@"CoverA.plist"];
        
        [self.coverArray writeToFile:filename atomically:YES];
        
        for (int i = 0; i < [coverAArray count]; i ++ ) {
            NSDictionary *params = (NSDictionary *)[coverAArray objectAtIndex:i];
            [self.imageUrls addObject:[params safeObjectForKey:@"FileUrl"]];
        }
        
        //        [self animateWithURLs:self.imageUrls
        //           transitionDuration:3.0
        //                         loop:YES];
    }else{
        //        [self dismissCoverA];
    }
    
    [self animateWithURLs:self.imageUrls
       transitionDuration:3.0
                     loop:YES];
}

- (void)getCoverAMessageDidFailed:(int)resultCode
{
    self.imageUrls = [NSMutableArray array];
    if ([self.coverArray count]) {
        for (int i = 0; i < [self.coverArray count]; i ++ ) {
            NSDictionary *params = (NSDictionary *)[self.coverArray objectAtIndex:i];
            [self.imageUrls addObject:[params safeObjectForKey:@"FileUrl"]];
        }
        
        //        [self animateWithURLs:self.imageUrls
        //           transitionDuration:3.0
        //                         loop:YES];
    }else{
        //        [self dismissCoverA];
        
    }
    
    [self animateWithURLs:self.imageUrls
       transitionDuration:3.0
                     loop:YES];
}

@end
