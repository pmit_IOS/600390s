//
//  ZSTDao+PicA.m
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTDao+PicA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"
#import "ZSTGlobal+PicA.h"


#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [PicADetailList_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            msgid VARCHAR DEFAULT '', \
                                                                            title VARCHAR DEFAULT '',\
                                                                            shareurl VARCHAR DEFAULT '', \
                                                                            actionurl VARCHAR DEFAULT '', \
                                                                            detailid VARCHAR DEFAULT '', \
                                                                            imgurl VARCHAR DEFAULT '',  \
                                                                            description VARCHAR DEFAULT '',\
                                                                            UNIQUE(detailid)\
                                                                            );\
                                                                            CREATE TABLE IF NOT EXISTS [PicAList_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            categoryid VARCHAR DEFAULT '', \
                                                                            msgid VARCHAR DEFAULT '', \
                                                                            title VARCHAR DEFAULT '', \
                                                                            imgwidth FLOAT DEFAULT 0, \
                                                                            imgheight FLOAT DEFAULT 0, \
                                                                            imgurl VARCHAR DEFAULT '', \
                                                                            favoritescount VARCHAR DEFAULT '', \
                                                                            addtime VARCHAR DEFAULT '',\
                                                                            UNIQUE(msgid)\
                                                                            );\
                                                                            CREATE TABLE IF NOT EXISTS [PicAFavList_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            categoryid VARCHAR DEFAULT '', \
                                                                            msgid VARCHAR DEFAULT '', \
                                                                            title VARCHAR DEFAULT '', \
                                                                            imgwidth FLOAT DEFAULT 0, \
                                                                            imgheight FLOAT DEFAULT 0, \
                                                                            imgurl VARCHAR DEFAULT '', \
                                                                            favoritescount VARCHAR DEFAULT '', \
                                                                            addtime VARCHAR DEFAULT '',\
                                                                            UNIQUE(msgid)\
                                                                            );\
                                                                            CREATE TABLE IF NOT EXISTS [PicACategory_%@] (\
                                                                            ID INTEGER PRIMARY KEY AUTOINCREMENT, \
                                                                            categoryid INTEGER DEFAULT 0 UNIQUE, \
                                                                            categoryname VARCHAR DEFAULT ''\
                                                                            );", @(moduleType), @(moduleType), @(moduleType), @(moduleType)]

TK_FIX_CATEGORY_BUG(PicA)


@implementation ZSTDao (PicA)

- (void)createTableIfNotExistForPicAModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
}


- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO PicACategory_%@ (categoryid, categoryname) VALUES (?,?)", @(self.moduleType)],
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:categoryName]];
    return success;
}


- (BOOL)deleteAllPicACategories
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM PicACategory_%ld", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}


- (NSArray *)getPicACategories
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, categoryid, categoryname FROM PicACategory_%ld",(long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}


- (BOOL)addPicAListWithCategoryId:(NSString *)categoryid
                            msgid:(NSString *)msgid
                            title:(NSString *)title
                            width:(float)width
                           height:(float)height
                           imgurl:(NSString *)imgurl
                   favoritescount:(NSString *)favoritescount
                          addtime:(NSString *)addtime
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO PicAList_%ld (categoryid, msgid , title, imgwidth,imgheight, imgurl , favoritescount , addtime ) VALUES (?,?,?,?,?,?,?,?)", (long)self.moduleType],
                    [TKUtil wrapNilObject:categoryid],
                    [TKUtil wrapNilObject:msgid],
                    [TKUtil wrapNilObject:title],
                    [NSNumber numberWithFloat:width],
                    [NSNumber numberWithFloat:height],
                    [TKUtil wrapNilObject:imgurl],
                    [TKUtil wrapNilObject:favoritescount],
                    [TKUtil wrapNilObject:addtime]];
    return success;
}


- (BOOL)deleteAllPicAWithCategoryId:(NSString *)categoryid
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM PicAList_%ld where categoryid = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL,categoryid]) {
        
        return NO;
    }
    return YES;
    
}
- (NSArray *)picAListAtPage:(int)pageNum pageCount:(int)pageCount category:(NSString *)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, categoryid, msgid , title , imgwidth,imgheight,  imgurl , favoritescount , addtime  FROM  PicAList_%ld where categoryid = ? limit ?,?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                                       [TKUtil wrapNilObject:categoryID],
                                                                       [NSNumber numberWithInt:(pageNum-1)*pageCount],
                                                                       [NSNumber numberWithInt:pageCount],
                                                                       nil]];
    return results;
}


- (NSUInteger)picACountOfCategory:(NSString *)categoryID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) FROM PicAList_%ld where categoryid = ? ", (long)self.moduleType], (long)categoryID];
}

- (BOOL)addPicADetailListWithCategoryId:(NSString *)msgid
                                  msgid:(NSString *)title
                                  title:(NSString *)shareurl
                               shareurl:(NSString *)actionurl
                              actionurl:(NSString *)detailid
                               detailid:(NSString *)imgurl
                                 imgurl:(NSString *)description;
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO PicADetailList_%ld (msgid , title , shareurl , actionurl , detailid , imgurl, description ) VALUES (?,?,?,?,?,?,?)", (long)self.moduleType],
                    [TKUtil wrapNilObject:msgid],
                    [TKUtil wrapNilObject:title],
                    [TKUtil wrapNilObject:shareurl],
                    [TKUtil wrapNilObject:actionurl],
                    [TKUtil wrapNilObject:detailid],
                    [TKUtil wrapNilObject:imgurl],
                    [TKUtil wrapNilObject:description]];
    return success;
}


- (BOOL)deletePicADetailWithMsgid:(NSString *)msgid detailid:(NSString *)detailid;
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM PicADetailList_%ld where msgid = ? and detailid = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL,msgid,detailid]) {
        
        return NO;
    }
    return YES;
}


- (NSArray *)getPicADetailListWithMsgid:(NSString *)msgid
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, msgid, title , shareurl , actionurl , detailid , imgurl , description  FROM PicADetailList_%ld where msgid = ?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql , msgid];
    return results;
}


- (BOOL)addFavPicListWithCategoryId:(NSString *)categoryid
                              msgid:(NSString *)msgid
                              title:(NSString *)title
                              width:(float)width
                             height:(float)height
                             imgurl:(NSString *)imgurl
                     favoritescount:(NSString *)favoritescount
                            addtime:(NSString *)addtime
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO PicAFavList_%ld (categoryid, msgid , title, imgwidth,imgheight, imgurl , favoritescount , addtime ) VALUES (?,?,?,?,?,?,?,?)", (long)self.moduleType],
                    [TKUtil wrapNilObject:categoryid],
                    [TKUtil wrapNilObject:msgid],
                    [TKUtil wrapNilObject:title],
                    [NSNumber numberWithFloat:width],
                    [NSNumber numberWithFloat:height],
                    [TKUtil wrapNilObject:imgurl],
                    [TKUtil wrapNilObject:favoritescount],
                    [TKUtil wrapNilObject:addtime]];
    return success;
}


- (NSArray *)getFavPicAList
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, categoryid, msgid , title , imgwidth,imgheight,  imgurl , favoritescount , addtime  FROM PicAFavList_%ld ", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

- (BOOL)deleteFavPicWithMsgid:(NSString *)msgid
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM PicAFavList_%ld where msgid = ?", (long)self.moduleType];
    if (![ZSTSqlManager executeUpdate:deleteSQL,msgid]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)isExsitFavPicWithMsgid:(NSString *)msgid
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, categoryid, msgid , title , imgwidth,imgheight,  imgurl , favoritescount , addtime  FROM PicAFavList_%ld where msgid = ?", (long)self.moduleType];
    NSArray *results = [ZSTSqlManager executeQuery:querySql , [TKUtil wrapNilObject:msgid]];
    if ([results count]) {
        return YES;
    }
    return NO;
}



@end
