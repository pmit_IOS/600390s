//
//  ZSTGlobal+PicA.h
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef enum {
    
    fav_AddType = 1 //收藏
    
} AddType;


#define PicABaseURL @"http://mod.pmit.cn/PicA"//最新接口
//#define PicABaseURL @"http://192.168.21.10:90/PicA"

#define GetPicACatagory PicABaseURL @"/GetCategory"

#define GetPicAList PicABaseURL @"/GetList"

#define GetPicADetailList PicABaseURL @"/GetDetailList"

#define GetPicAAddCount PicABaseURL @"/AddCount"

