//
//  ZSTF3Engine+PicA.m
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+PicA.h"
#import "PicA+Info.h"
#import "ZSTDao+PicA.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"

@implementation ZSTF3Engine (PicA)


- (void)getPicACategory
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetPicACatagory stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getPicACategoryResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
}

- (void)getPicACategoryResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
       id categories = [ZSTPicACategory categoryPicaListWithDic:response.jsonResponse];
        if (![categories isKindOfClass:[NSMutableArray class]]) {
            categories = [NSArray array];
        }
        
        [self.dao deleteAllPicACategories];
        //入库
        [ZSTSqlManager beginTransaction];
        for (ZSTPicACategory *category in categories){
            [self.dao addCategory:category.categoryid
                     categoryName:category.categoryname];
        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getCategoryDidSucceed:)]) {
            [self.delegate getCategoryDidSucceed:categories];
        }
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getCategoryDidFailed:)]) {
            [self.delegate getCategoryDidFailed:[response.jsonResponse objectForKey:@"notice"]];
        }
    }
}

- (void)getPicAListWithCategoryId:(NSString *)categoryid size:(int)size pageindex:(int)pageindex
{
    //如果是loadMore (pageNum>1),则从本地数据获取
	if (pageindex>1){
        NSArray *tempLocalNews = [self.dao picAListAtPage:pageindex pageCount:size category:categoryid];
        if ([tempLocalNews count] == 0) {
            [self _getPicAListWithCategoryId:categoryid size:size pageindex:pageindex];
        } else {
            
            NSMutableArray *result = [NSMutableArray array];
            for (NSDictionary *dic in tempLocalNews) {
                
                ZSTPicAList *info = [ZSTPicAList picaListInfoWithDic:dic];
                if (info != nil) {
                    [result addObject:info];
                }
            }
            if ([self isValidDelegateForSelector:@selector(getListDidSucceed:hasmore:)]) {
                BOOL hasmore = ([tempLocalNews count] >= 10);
                [self.delegate getListDidSucceed:result hasmore:hasmore];
            }
        }
	} else {
        [self _getPicAListWithCategoryId:categoryid size:size pageindex:pageindex];
	}
}

- (void)_getPicAListWithCategoryId:(NSString *)categoryid size:(int)size pageindex:(int)pageindex
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:[categoryid integerValue]] forKey:@"categoryid"];
    [params setSafeObject:[NSNumber numberWithInteger:size] forKey:@"size"];
    [params setSafeObject:[NSNumber numberWithInteger:pageindex] forKey:@"pageindex"];
    
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetPicAList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getPicAListResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:NO];
}

- (void)getPicAListResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
       id lists = [ZSTPicAList picaListWithDic:response.jsonResponse];
        if (![lists isKindOfClass:[NSMutableArray class]]) {
            lists = [NSMutableArray array];
        }
        NSDictionary *params = [userInfo objectForKey:@"Params"];
        NSInteger categoryID = [[params objectForKey:@"categoryid"] integerValue];//入库的时候加入分类id
        NSInteger pageindex = [[params objectForKey:@"pageindex"] integerValue];

        //如果获取到TOPIC_PAGE_SIZE条(count)，则删除原数据(因为不连续)
        if ([lists count] == 10 && pageindex == 1) {
            [self.dao deleteAllPicAWithCategoryId:[NSString stringWithFormat:@"%ld",(long)categoryID]];
        }
                
        if ([self isValidDelegateForSelector:@selector(getListDidSucceed:hasmore:)]) {
            [self.delegate getListDidSucceed:lists hasmore:[[response.jsonResponse objectForKey:@"hasmore"] boolValue]];
        }
        
        //入库
        [ZSTSqlManager beginTransaction];
        for (ZSTPicAList *list in lists){
            [self.dao addPicAListWithCategoryId:@(categoryID).stringValue
                                          msgid:@(list.msgid).stringValue
                                          title:list.title
                                          width:list.width
                                         height:list.height
                                         imgurl:list.imgurl
                                 favoritescount:list.favoritescount
                                        addtime:list.addtime];
        }
        [ZSTSqlManager commit];
        
        
      
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getListDidFailed:)]) {
            [self.delegate getListDidFailed:[response.jsonResponse objectForKey:@"notice"]];
        }
    }
}

- (void)getPicADetailListWithMessageId:(NSString *)messageid
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:messageid forKey:@"msgid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetPicADetailList stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getPicADetailListResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:NO];
}


- (void)getPicADetailListResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id detaillist = [ZSTPicADetailList detailPicaListWithDic:response.jsonResponse];
        if (![detaillist isKindOfClass:[NSArray class]]) {
            detaillist = [NSArray array];
        }
        
        NSDictionary *params = [userInfo objectForKey:@"Params"];
        NSString *msgid = [params objectForKey:@"msgid"];//
        NSString *title = [response.jsonResponse objectForKey:@"title"];
        NSString *shareurl = [response.jsonResponse objectForKey:@"shareurl"];
        NSString *actionurl = [response.jsonResponse objectForKey:@"actionurl"];
        
        if ([self isValidDelegateForSelector:@selector(getDetailListDidSucceed:title:shareurl:actionurl:msgid:)]) {
            [self.delegate getDetailListDidSucceed:detaillist title:title shareurl:shareurl actionurl:actionurl msgid:msgid];
        }
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getDetailListDidFailed:)]) {
            [self.delegate getDetailListDidFailed:[response.jsonResponse objectForKey:@"notice"]];
        }
    }
}

- (void)addPicACountWithMessageId:(NSString *)messageid type:(AddType)type
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:messageid forKey:@"msgid"];
    [params setSafeObject:[NSNumber numberWithInteger:type] forKey:@"type"];

    
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetPicAAddCount stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(addPicACountResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
    
    
}

- (void)addPicACountResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id count = [response.jsonResponse objectForKey:@"count"];
        
        if ([self isValidDelegateForSelector:@selector(addCountDidSucceed:)]) {
            [self.delegate addCountDidSucceed:[count integerValue]];
        }
    } else {
        
        if ([self isValidDelegateForSelector:@selector(addCountDidFailed:)]) {
            [self.delegate addCountDidFailed:[response.jsonResponse objectForKey:@"notice"]];
        }
    }
}



@end
