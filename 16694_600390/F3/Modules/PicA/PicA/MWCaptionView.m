//
//  MWCaptionView.m
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 30/12/2011.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "MWCaptionView.h"
#import "MWPhoto.h"
#import <QuartzCore/QuartzCore.h>
#import <PMRepairButton.h>

static const CGFloat labelPadding = 10;

// Private
@interface MWCaptionView () {
    id<MWPhoto> _photo;
    UILabel *_titlelabel;
    UITextView *_descriptionView;
    PMRepairButton *_favBtn;
    PMRepairButton *_shareBtn;
    PMRepairButton *_downloadBtn;
    PMRepairButton *_linkBtn;
    PMRepairButton *_linkLabel;
    UILabel *_indexLabel;
    UIImageView *_backgroundView;
}
@end

@implementation MWCaptionView
@synthesize delegate,zstDao;

- (id)initWithPhoto:(id<MWPhoto>)photo {
    self = [super initWithFrame:CGRectMake(0, 0, 320, 44)]; // Random initial frame
    if (self) {
        _photo = [photo retain];
        self.opaque = NO;
        self.backgroundColor = [UIColor clearColor];
        _backgroundView = [[UIImageView alloc] initWithImage:[ZSTModuleImage(@"module_pica_biz_pic_words_bg.png") stretchableImageWithLeftCapWidth:1 topCapHeight:10]];
        _backgroundView.frame = self.frame;
        _backgroundView.userInteractionEnabled = YES;
        [self addSubview:_backgroundView];
        _backgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        
        self.autoresizingMask = UIViewAutoresizingFlexibleWidth|UIViewAutoresizingFlexibleTopMargin|UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        [self setupCaption];
            }
    return self;
}

- (CGSize)sizeThatFits:(CGSize)size {
    CGSize textSize = [_descriptionView.text sizeWithFont:_descriptionView.font
                              constrainedToSize:CGSizeMake(size.width, CGFLOAT_MAX)
                                  lineBreakMode:NSLineBreakByCharWrapping];
    
   
    if (textSize.height <= 45 && textSize.height != 0) {
        textSize.height = 45;
        _descriptionView.scrollEnabled = NO;
    } else if (textSize.height == 54) {
        textSize.height = 60;
        _descriptionView.scrollEnabled = YES;
    }else if (textSize.height >= 72) {
        textSize.height = 80;
        _descriptionView.scrollEnabled = YES;
    }
    else
    {
        
    }
    CGRect descriptionFrame = _descriptionView.frame;
    
    descriptionFrame.size.height = textSize.height+10;
    _descriptionView.frame = descriptionFrame;
    
    float height = textSize.height + labelPadding * 2+25+25;
    
    CGRect downloadbtnFrame = _downloadBtn.frame;
    downloadbtnFrame.origin.y = height -44;
    _downloadBtn.frame = downloadbtnFrame;
    
    CGRect sharebtnFrame = _shareBtn.frame;
    sharebtnFrame.origin.y = height -44;
    _shareBtn.frame = sharebtnFrame;
    
    CGRect favbtnFrame = _favBtn.frame;
    favbtnFrame.origin.y = height -44;
    _favBtn.frame = favbtnFrame;
    
    CGRect linkbtnFrame = _linkBtn.frame;
    linkbtnFrame.origin.y = height -44;
    _linkBtn.frame = linkbtnFrame;
    
    CGRect linkLabelFrame = _linkLabel.frame;
    linkLabelFrame.origin.y = height -44;
    _linkLabel.frame = linkLabelFrame;
    
    return CGSizeMake(size.width, height);
}

- (void)setIndexWithText:(NSString *)indexText
{
    _indexLabel.text = indexText;
}

- (void)setZstDao:(ZSTDao *)thezstDao
{
    if (thezstDao != zstDao) {
        [zstDao release];
        zstDao = [thezstDao retain];
                
        if ([zstDao isExsitFavPicWithMsgid:[_photo msgid]]) {
            _favBtn.selected = YES;
        }
    }    
}

- (void)setupCaption {
    
    
    //self.backgroundColor = [UIColor yellowColor];
    
    UIView * backgroundView_black = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height * 3 )];
    backgroundView_black.backgroundColor = [UIColor blackColor];
    backgroundView_black.alpha = 0.65;
    [self addSubview:backgroundView_black];
    
    if (_titlelabel == nil) {
        _titlelabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5,self.bounds.size.width-30*2,20)];
        _titlelabel.textColor = [UIColor whiteColor];
        _titlelabel.backgroundColor = [UIColor clearColor];
        _titlelabel.font = [UIFont boldSystemFontOfSize:18];
        _titlelabel.shadowColor = [UIColor blackColor];
        _titlelabel.shadowOffset = CGSizeMake(1, 1);
        _titlelabel.textAlignment = NSTextAlignmentLeft;
        [self addSubview:_titlelabel];
    }
    
    if ([_photo respondsToSelector:@selector(caption)]) {
        _titlelabel.text = [_photo title] ? [_photo title] : @" ";
    }
    
    if (_indexLabel == nil) {
        _indexLabel = [[UILabel alloc] initWithFrame:CGRectMake(self.bounds.size.width-32*2, 5,30*2,20)];
//        _indexLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, self.bounds.size.height + 40,30*2,20)];
        _indexLabel.textColor = [UIColor whiteColor];
        _indexLabel.backgroundColor = [UIColor clearColor];
        _indexLabel.font = [UIFont systemFontOfSize:14];
        _indexLabel.shadowColor = [UIColor blackColor];
        _indexLabel.shadowOffset = CGSizeMake(1, 1);
        _indexLabel.textAlignment = NSTextAlignmentRight;
        [self addSubview:_indexLabel];
    }
    
    if (_descriptionView == nil) {
        _descriptionView = [[UITextView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_indexLabel.frame),
                                                                        self.bounds.size.width,
                                                                        self.bounds.size.height)];
        _descriptionView.editable = NO;
        _descriptionView.opaque = NO;
        _descriptionView.backgroundColor = [UIColor clearColor];
        _descriptionView.textAlignment = NSTextAlignmentCenter;
        _descriptionView.textColor = [UIColor whiteColor];
        _descriptionView.font = [UIFont systemFontOfSize:14];
        _descriptionView.textAlignment = NSTextAlignmentLeft;

        [self addSubview:_descriptionView];
    }
    if ([_photo respondsToSelector:@selector(caption)]) {
        _descriptionView.text = [_photo caption] ? [NSString stringWithFormat:@"      %@",[_photo caption]] : @" ";
    }

    if (_downloadBtn == nil) {
//        _downloadBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _downloadBtn = [[PMRepairButton alloc] init];
        _downloadBtn.frame = CGRectMake(self.bounds.size.width - (54+10)*2, self.bounds.size.height, 54, 44);
        [_downloadBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_download.png") forState:UIControlStateNormal];
        [_downloadBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_download.png") forState:UIControlStateHighlighted];
        [_downloadBtn addTarget:self action:@selector(downloadAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_downloadBtn];
    }
    
    if (_shareBtn == nil) {
//        _shareBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _shareBtn = [[PMRepairButton alloc] init];
        _shareBtn.frame = CGRectMake(self.bounds.size.width - (54+10)*3, self.bounds.size.height , 54, 44);
        [_shareBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_share_hi.png") forState:UIControlStateNormal];
        [_shareBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_share.png") forState:UIControlStateHighlighted];
        [_shareBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_share.png") forState:UIControlStateSelected];
        [_shareBtn addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_shareBtn];
    }
    
    if (_favBtn == nil) {
//        _favBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _favBtn = [[PMRepairButton alloc] init];
        _favBtn.frame = CGRectMake(self.bounds.size.width - (54+10)*1, self.bounds.size.height, 54, 44);
        [_favBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_fav_empty.png") forState:UIControlStateNormal];
        [_favBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_fav_hi.png") forState:UIControlStateHighlighted];
        [_favBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_fav_hi.png") forState:UIControlStateSelected];
        [_favBtn addTarget:self action:@selector(favAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_favBtn];
    }
    
    if (_linkBtn == nil) {
//        _linkBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _linkBtn = [[PMRepairButton alloc] init];
        _linkBtn.frame = CGRectMake(-5, self.bounds.size.height, 54, 44);
        [_linkBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_link.png") forState:UIControlStateNormal];
        [_linkBtn setImage:ZSTModuleImage(@"module_pica_imageset_toolbar_link.png") forState:UIControlStateHighlighted];
        [_linkBtn addTarget:self action:@selector(linkAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_linkBtn];
        _linkBtn.alpha = 1;
        
//        _linkLabel = [UIButton buttonWithType:UIButtonTypeCustom];
        _linkLabel = [[PMRepairButton alloc] init];
        _linkLabel.frame = CGRectMake(30, self.bounds.size.height, 54, 44);
        [_linkLabel setTitle:@"去看看" forState:UIControlStateNormal];
        [_linkLabel setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _linkLabel.backgroundColor = [UIColor clearColor];
        _linkLabel.titleLabel.font = [UIFont systemFontOfSize:14];
        _linkLabel.titleLabel.textAlignment = NSTextAlignmentLeft;
        [_linkLabel addTarget:self action:@selector(linkAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_linkLabel];
        _linkLabel.alpha = 1;

    }
    
    if ([_photo respondsToSelector:@selector(caption)]) {
        
        if ([[_photo linkText] length] == 0 || [[_photo linkText] isEqualToString:@""]) {
            _linkLabel.hidden = YES;
            _linkBtn.hidden = YES;
        }
    }
}

- (void)linkAction:(id)sender
{
    if ([delegate respondsToSelector:@selector(captionView:photo:linkBtnDidSelected:)]) {
        [delegate captionView:self photo:_photo linkBtnDidSelected:sender];
    }
}

- (void)downloadAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(captionView:photo:downloadBtnDidSelected:)]) {
        [delegate captionView:self photo:_photo downloadBtnDidSelected:sender];
    }
}

- (void)shareAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(captionView:photo:shareBtnDidSelected:)]) {
        [delegate captionView:self photo:_photo shareBtnDidSelected:sender];
    }
}

- (void)favAction:(UIButton *)sender
{
    if ([delegate respondsToSelector:@selector(captionView:photo:favBtnDidSelected:isAdd:)]) {
        [delegate captionView:self photo:_photo favBtnDidSelected:sender isAdd:_favBtn.selected];
    }
    
    if (_favBtn.selected) {
        _favBtn.selected = NO;
    }else{
        _favBtn.selected = YES;
    }
}

- (void)dealloc {
    
    [_titlelabel release];
    [_indexLabel release];
    [_backgroundView release];
    [_descriptionView release];
    [_photo release];
    [super dealloc];
}

@end
