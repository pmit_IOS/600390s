//
//  ZSTPicAViewController.h
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTModuleBaseViewController.h"
#import "ColumnBar.h"
#import "EGORefreshTableHeaderView.h"
#import "MoreButton.h"
#import "MWPhotoBrowser.h"

#import "HHPhotosWaterfallCell.h"
#import "TKWaterfallsView.h"
#import "ZSTLoginViewController.h"

@interface ZSTPicAViewController : ZSTModuleBaseViewController<TKWaterfallsViewDelegate, TKWaterfallsViewDataSource,MWPhotoBrowserDelegate,EGORefreshTableHeaderDelegate,ColumnBarDelegate,ColumnBarDataSource,UIScrollViewDelegate,LoginDelegate>
{
    BOOL _loading;  //是否正在加载中
    
    int _pageIndex; //当前加载的页数，页面索引
    int _pageSize;  //每页数据条数
    
    BOOL _hasMore;  //是否有更多数据，是否长更多按钮
    
    
    EGORefreshTableHeaderView *_refreshHeaderView;  //下拽刷新的试图
    MoreButton *_moreButton;                        //更多按钮，需要变更加载状态
    
}

@property(nonatomic, retain) ColumnBar *topbar;   //顶部控制栏
@property (nonatomic, retain) NSArray *photos;

@property(nonatomic, retain) NSMutableArray *categories;         //分类数组
@property(nonatomic, retain) TKWaterfallsView *waterFlow;          //表视图
@property(nonatomic, retain) NSMutableArray *dataArray;         //数据数组

@property(nonatomic, retain) UITableViewCell *moreCell;         //更多按钮所在的行

- (void)aotuLoadData;
- (void)doneLoadingData;
- (void)reloadView;

@end
