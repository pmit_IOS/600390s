//
//  MWPhotoBrowser.m
//  MWPhotoBrowser
//
//  Created by Michael Waterfall on 14/10/2010.
//  Copyright 2010 d3i. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "MWPhotoBrowser.h"
#import "MWZoomingScrollView.h"
#import "MBProgressHUD.h"
#import <SDWebImage/SDImageCache.h>
#import "ZSTPicAWebViewController.h"
#import "ZSTHHSinaShareController.h"
#import <SDKExport/WXApi.h>
#import "ZSTUtils.h"
#import <MessageUI/MessageUI.h>
#import "ZSTF3Engine+PicA.h"
#import "ZSTDao+PicA.h"
#import "ZSTF3Preferences.h"

#import "ZSTFavViewController.h"
#import "BaseNavgationController.h"
#import <ZSTShareView.h>
#import <ZSTNewShareView.h>

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define PADDING                 10
#define PAGE_INDEX_TAG_OFFSET   1000
#define PAGE_INDEX(page)        ([(page) tag] - PAGE_INDEX_TAG_OFFSET)

#define NotificationName_WXShareSucceed @"WX_share_succeed"
#define NotificationName_WXShareFaild   @"WX_share_faild"

// Private
@interface MWPhotoBrowser ()<ZSTHHSinaShareControllerDelegate,MFMessageComposeViewControllerDelegate,ZSTShareViewDelegate,ZSTNewShareViewDelegate> {
    
	// Data
    id <MWPhotoBrowserDelegate> _delegate;
    NSUInteger _photoCount;
    NSMutableArray *_photos;
	NSArray *_depreciatedPhotoData; // Depreciated
	
	// Views
	UIScrollView *_pagingScrollView;
	
	// Paging
	NSMutableSet *_visiblePages, *_recycledPages;
	NSUInteger _currentPageIndex;
	NSUInteger _pageIndexBeforeRotation;
	
	// Navigation & controls
	UIToolbar *_toolbar;
//	NSTimer *_controlVisibilityTimer;
	UIBarButtonItem *_previousButton, *_nextButton, *_actionButton;
    UIActionSheet *_actionsSheet;
    MBProgressHUD *_progressHUD;
    
    // Appearance
    UIImage *_navigationBarBackgroundImageDefault, 
    *_navigationBarBackgroundImageLandscapePhone;
    UIColor *_previousNavBarTintColor;
    UIBarStyle _previousNavBarStyle;
    UIStatusBarStyle _previousStatusBarStyle;
    UIBarButtonItem *_previousViewControllerBackButton;
    
    // Misc
    BOOL _displayActionButton;
	BOOL _performingLayout;
	BOOL _rotating;
    BOOL _viewIsActive; // active as in it's in the view heirarchy
    BOOL _didSavePreviousStateOfNavBar;
    
    BOOL isshow;
    UIView * _blackView;
}

// Private Properties
@property (nonatomic, retain) UIColor *previousNavBarTintColor;
@property (nonatomic, retain) UIBarButtonItem *previousViewControllerBackButton;
@property (nonatomic, retain) UIImage *navigationBarBackgroundImageDefault, *navigationBarBackgroundImageLandscapePhone;
@property (nonatomic, retain) UIActionSheet *actionsSheet;
@property (nonatomic, retain) MBProgressHUD *progressHUD;
//@property (strong,nonatomic) ZSTShareView *shareView;
@property (strong,nonatomic) UIView *clearView;
@property (strong,nonatomic) ZSTNewShareView *shareView;
@property (assign,nonatomic) NSInteger wxSharePoint;
@property (assign,nonatomic) NSInteger wxFriendSharePoint;
@property (assign,nonatomic) NSInteger weiboSharePoint;
@property (assign,nonatomic) NSInteger qqSharePoint;
@property (assign,nonatomic) NSInteger smsSharePoint;

// Private Methods

// Layout
- (void)performLayout;

// Nav Bar Appearance
- (void)setNavBarAppearance:(BOOL)animated;
- (void)storePreviousNavBarAppearance;
- (void)restorePreviousNavBarAppearance:(BOOL)animated;

// Paging
- (void)tilePages;
- (BOOL)isDisplayingPageForIndex:(NSUInteger)index;
- (MWZoomingScrollView *)pageDisplayedAtIndex:(NSUInteger)index;
- (MWZoomingScrollView *)pageDisplayingPhoto:(id<MWPhoto>)photo;
- (MWZoomingScrollView *)dequeueRecycledPage;
- (void)configurePage:(MWZoomingScrollView *)page forIndex:(NSUInteger)index;
- (void)didStartViewingPageAtIndex:(NSUInteger)index;

// Frames
- (CGRect)frameForPagingScrollView;
- (CGRect)frameForPageAtIndex:(NSUInteger)index;
- (CGSize)contentSizeForPagingScrollView;
- (CGPoint)contentOffsetForPageAtIndex:(NSUInteger)index;
- (CGRect)frameForToolbarAtOrientation:(UIInterfaceOrientation)orientation;
- (CGRect)frameForCaptionView:(MWCaptionView *)captionView atIndex:(NSUInteger)index;

// Navigation
- (void)updateNavigation;
- (void)jumpToPageAtIndex:(NSUInteger)index;
- (void)gotoPreviousPage;
- (void)gotoNextPage;

// Controls
//- (void)hideControlsAfterDelay;
- (void)setControlsHidden:(BOOL)hidden animated:(BOOL)animated permanent:(BOOL)permanent;
- (void)toggleControls;
//- (BOOL)areControlsHidden;

// Data
- (NSUInteger)numberOfPhotos;
- (id<MWPhoto>)photoAtIndex:(NSUInteger)index;
- (UIImage *)imageForPhoto:(id<MWPhoto>)photo;
- (void)loadAdjacentPhotosIfNecessary:(id<MWPhoto>)photo;
- (void)releaseAllUnderlyingPhotos;

// Actions
- (void)savePhoto;
- (void)copyPhoto;
- (void)emailPhoto;

@end

// Handle depreciations and supress hide warnings
@interface UIApplication (DepreciationWarningSuppresion)
- (void)setStatusBarHidden:(BOOL)hidden animated:(BOOL)animated;
@end

// MWPhotoBrowser
@implementation MWPhotoBrowser

// Properties
@synthesize previousNavBarTintColor = _previousNavBarTintColor;
@synthesize navigationBarBackgroundImageDefault = _navigationBarBackgroundImageDefault,
navigationBarBackgroundImageLandscapePhone = _navigationBarBackgroundImageLandscapePhone;
@synthesize displayActionButton = _displayActionButton, actionsSheet = _actionsSheet;
@synthesize progressHUD = _progressHUD;
@synthesize previousViewControllerBackButton = _previousViewControllerBackButton;
@synthesize piclist;

#pragma mark - NSObject

- (id)init {
    if ((self = [super init])) {
        
        // Defaults
        self.wantsFullScreenLayout = YES;
        self.hidesBottomBarWhenPushed = YES;
        _photoCount = NSNotFound;
		_currentPageIndex = 0;
		_performingLayout = NO; // Reset on view did appear
		_rotating = NO;
        _viewIsActive = NO;
        _visiblePages = [[NSMutableSet alloc] init];
        _recycledPages = [[NSMutableSet alloc] init];
        _photos = [[NSMutableArray alloc] init];
        _displayActionButton = NO;
        _didSavePreviousStateOfNavBar = NO;
        
        isshow = NO;
        
        // Listen for MWPhoto notifications
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(handleMWPhotoLoadingDidEndNotification:)
                                                     name:MWPHOTO_LOADING_DID_END_NOTIFICATION
                                                   object:nil];
    }
    return self;
}

- (id)initWithDelegate:(id <MWPhotoBrowserDelegate>)delegate {
    if ((self = [self init])) {
        _delegate = delegate;
	}
	return self;
}

- (id)initWithPhotos:(NSArray *)photosArray {
	if ((self = [self init])) {
		_depreciatedPhotoData = photosArray;
	}
	return self;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
//    [_previousNavBarTintColor release];
//    [_navigationBarBackgroundImageDefault release];
//    [_navigationBarBackgroundImageLandscapePhone release];
//    [_previousViewControllerBackButton release];
//	[_pagingScrollView release];
//	[_visiblePages release];
//	[_recycledPages release];
//	[_toolbar release];
//	[_previousButton release];
//	[_nextButton release];
//    [_actionButton release];
//  	[_depreciatedPhotoData release];
    [self releaseAllUnderlyingPhotos];
    [[SDImageCache sharedImageCache] clearMemory]; // clear memory
//    [_photos release];
//    [_progressHUD release];
//    [super dealloc];
}

- (void)releaseAllUnderlyingPhotos {
    for (id p in _photos) { if (p != [NSNull null]) [p unloadUnderlyingImage]; } // Release photos
}

- (void)didReceiveMemoryWarning {
	
	// Release any cached data, images, etc that aren't in use.
    [self releaseAllUnderlyingPhotos];
	[_recycledPages removeAllObjects];
	
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
}

- (void)showPhotosIsEmpty:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:text
                                                   delegate:self
                                          cancelButtonTitle:@"确定"
                                          otherButtonTitles:nil, nil];
    
    [alert show];
}


- (void)openMyFav
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        
        return;
    }
    
    ZSTFavViewController *fav = [[ZSTFavViewController alloc] init];
    fav.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:fav animated:YES];
}

#pragma mark - View Loading

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
    //初始化导航条
#pragma mark ---navigationItem.titleView
    self.navigationController.navigationBar.translucent = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"图片展示"];
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"我的", nil) target:self selector:@selector (openMyFav)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    
	_blackView = [[UIView alloc]initWithFrame:CGRectMake(0, - 44 - (IS_IOS_7?20:0) , 320, 44 + (IS_IOS_7?20:0))];
    _blackView.backgroundColor = [UIColor blackColor];
    [self.view addSubview: _blackView];
    [self.view sendSubviewToBack:_blackView];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareSucceed:) name:NotificationName_WXShareSucceed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareFaild:) name:NotificationName_WXShareFaild object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboSuccess:) name:@"sendWeiboSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendQQShareSuccess:) name:@"QQShareSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendQQShareFail:) name:@"QQShareFail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboShareFail:) name:@"sendWeiboFailure" object:nil];
    
//    // View
//
//    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
//    btn.backgroundColor = [UIColor clearColor];
//    btn.frame = CGRectMake(0, 0, 54, 44);
//    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 7, 0, 0);
//    btn.titleLabel.shadowOffset = CGSizeMake(0, -0.5f);
//    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
//    [btn addTarget:self action:@selector(popViewController) forControlEvents:UIControlEventTouchUpInside];
//    [btn setBackgroundImage:ZSTModuleImage(@"module_pica_imageset_back.png") forState:UIControlStateNormal];
//    [btn setBackgroundImage:ZSTModuleImage(@"module_pica_imageset_back_hi.png")  forState:UIControlStateHighlighted];    
//    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithCustomView:btn] autorelease];
//
//	self.view.backgroundColor = [UIColor blackColor];
	
	// Setup paging scrolling view
	CGRect pagingScrollViewFrame = [self frameForPagingScrollView];
	_pagingScrollView = [[UIScrollView alloc] initWithFrame:pagingScrollViewFrame];
	_pagingScrollView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
	_pagingScrollView.pagingEnabled = YES;
	_pagingScrollView.delegate = self;
	_pagingScrollView.showsHorizontalScrollIndicator = NO;
	_pagingScrollView.showsVerticalScrollIndicator = NO;
	_pagingScrollView.backgroundColor = [UIColor blackColor];
    _pagingScrollView.contentSize = [self contentSizeForPagingScrollView];
	[self.view addSubview:_pagingScrollView];
    
    // Toolbar
//    _toolbar = [[UIToolbar alloc] initWithFrame:[self frameForToolbarAtOrientation:self.interfaceOrientation]];
//    _toolbar.tintColor = nil;
//    if ([[UIToolbar class] respondsToSelector:@selector(appearance)]) {
//        [_toolbar setBackgroundImage:nil forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsDefault];
//        [_toolbar setBackgroundImage:nil forToolbarPosition:UIToolbarPositionAny barMetrics:UIBarMetricsLandscapePhone];
//    }
//    _toolbar.barStyle = UIBarStyleBlackTranslucent;
//    _toolbar.autoresizingMask = UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleWidth;
    
    // Toolbar Items
//    _previousButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MWPhotoBrowser.bundle/images/UIBarButtonItemArrowLeft.png"] style:UIBarButtonItemStylePlain target:self action:@selector(gotoPreviousPage)];
//    _nextButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"MWPhotoBrowser.bundle/images/UIBarButtonItemArrowRight.png"] style:UIBarButtonItemStylePlain target:self action:@selector(gotoNextPage)];
//    _actionButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAction target:self action:@selector(actionButtonPressed:)];
    
    // Update
    [self reloadData];
    [self buildDarkView];
//    [self buildShareView];
    if ([[ZSTF3Preferences shared].UserId isEqualToString:@""] || [[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""])
    {
        [self createShareView:NO AndResponse:nil AndIconUrl:self.piclist.imgurl];
    }
    else
    {
        [self.engine getPointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
    }
    
	// Super
    [super viewDidLoad];
    
    
	
}

- (void)performLayout {
    
    // Setup
    _performingLayout = YES;
    NSUInteger numberOfPhotos = [self numberOfPhotos];
    
	// Setup pages
    [_visiblePages removeAllObjects];
    [_recycledPages removeAllObjects];
    
    // Toolbar
    if (numberOfPhotos > 1 || _displayActionButton) {
//        [self.view addSubview:_toolbar];
    } else {
//        [_toolbar removeFromSuperview];
    }
    
//    // Toolbar items & navigation
//    UIBarButtonItem *fixedLeftSpace = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:self action:nil] autorelease];
//    fixedLeftSpace.width = 32; // To balance action button
//    UIBarButtonItem *flexSpace = [[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil] autorelease];
//    NSMutableArray *items = [[NSMutableArray alloc] init];
//    if (_displayActionButton) [items addObject:fixedLeftSpace];
//    [items addObject:flexSpace];
//    if (numberOfPhotos > 1) [items addObject:_previousButton];
//    [items addObject:flexSpace];
//    if (numberOfPhotos > 1) [items addObject:_nextButton];
//    [items addObject:flexSpace];
//    if (_displayActionButton) [items addObject:_actionButton];
//    [_toolbar setItems:items];
//    [items release];
	[self updateNavigation];
    
    // Navigation buttons
//    if ([self.navigationController.viewControllers objectAtIndex:0] == self) {
//        // We're first on stack so show done button
//        UIBarButtonItem *doneButton = [[[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"Done", nil) style:UIBarButtonItemStylePlain target:self action:@selector(doneButtonPressed:)] autorelease];
//        // Set appearance
//        if ([UIBarButtonItem respondsToSelector:@selector(appearance)]) {
//            [doneButton setBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//            [doneButton setBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsLandscapePhone];
//            [doneButton setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
//            [doneButton setBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsLandscapePhone];
//            [doneButton setTitleTextAttributes:[NSDictionary dictionary] forState:UIControlStateNormal];
//            [doneButton setTitleTextAttributes:[NSDictionary dictionary] forState:UIControlStateHighlighted];
//        }
//        self.navigationItem.rightBarButtonItem = doneButton;
//    } else {
//        // We're not first so show back button
//        UIViewController *previousViewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-2];
//        NSString *backButtonTitle = previousViewController.navigationItem.backBarButtonItem ? previousViewController.navigationItem.backBarButtonItem.title : previousViewController.title;
//        UIBarButtonItem *newBackButton = [[[UIBarButtonItem alloc] initWithTitle:backButtonTitle style:UIBarButtonItemStylePlain target:nil action:nil] autorelease];
//        // Appearance
//        if ([UIBarButtonItem respondsToSelector:@selector(appearance)]) {
//            [newBackButton setBackButtonBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
//            [newBackButton setBackButtonBackgroundImage:nil forState:UIControlStateNormal barMetrics:UIBarMetricsLandscapePhone];
//            [newBackButton setBackButtonBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsDefault];
//            [newBackButton setBackButtonBackgroundImage:nil forState:UIControlStateHighlighted barMetrics:UIBarMetricsLandscapePhone];
//            [newBackButton setTitleTextAttributes:[NSDictionary dictionary] forState:UIControlStateNormal];
//            [newBackButton setTitleTextAttributes:[NSDictionary dictionary] forState:UIControlStateHighlighted];
//        }
//        self.previousViewControllerBackButton = previousViewController.navigationItem.backBarButtonItem; // remember previous
//        previousViewController.navigationItem.backBarButtonItem = newBackButton;
//    }
    
    // Content offset
	_pagingScrollView.contentOffset = [self contentOffsetForPageAtIndex:_currentPageIndex];
    [self tilePages];
    _performingLayout = NO;
    
}

// Release any retained subviews of the main view.
- (void)viewDidUnload {
	_currentPageIndex = 0;
//    [_pagingScrollView release], _pagingScrollView = nil;
//    [_visiblePages release], _visiblePages = nil;
//    [_recycledPages release], _recycledPages = nil;
//    [_toolbar release], _toolbar = nil;
//    [_previousButton release], _previousButton = nil;
//    [_nextButton release], _nextButton = nil;
    self.progressHUD = nil;
    [super viewDidUnload];
}

#pragma mark - Appearance

- (void)viewWillAppear:(BOOL)animated {
    
	// Super
	[super viewWillAppear:animated];
//	self.wantsFullScreenLayout = YES;
//    
//	// Layout manually (iOS < 5)
//    if (SYSTEM_VERSION_LESS_THAN(@"5")) [self viewWillLayoutSubviews];
//    
//    // Status bar
//    if (self.wantsFullScreenLayout && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
//        _previousStatusBarStyle = [[UIApplication sharedApplication] statusBarStyle];
//        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleBlackTranslucent animated:animated];
//    }
//    
//    // Navigation bar appearance
//    if (!_viewIsActive && [self.navigationController.viewControllers objectAtIndex:0] != self) {
//        [self storePreviousNavBarAppearance];
//    }
//    [self setNavBarAppearance:animated];
    
    // Update UI
//	[self hideControlsAfterDelay];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    
    // Check that we're being popped for good
    if ([self.navigationController.viewControllers objectAtIndex:0] != self &&
        ![self.navigationController.viewControllers containsObject:self]) {
        
        // State
        _viewIsActive = NO;
        
        // Bar state / appearance
        [self restorePreviousNavBarAppearance:animated];
        
    }
    
    // Controls
    [self.navigationController.navigationBar.layer removeAllAnimations]; // Stop all animations on nav bar
    [NSObject cancelPreviousPerformRequestsWithTarget:self]; // Cancel any pending toggles from taps
    [self setControlsHidden:NO animated:NO permanent:YES];
    
    // Status bar
    if (self.wantsFullScreenLayout && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
        [[UIApplication sharedApplication] setStatusBarStyle:_previousStatusBarStyle animated:animated];
    }
    
	// Super
	[super viewWillDisappear:animated];
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    _viewIsActive = YES;
}

#pragma mark - Nav Bar Appearance

- (void)setNavBarAppearance:(BOOL)animated {
       self.navigationController.navigationBar.backgroundImage = [ZSTModuleImage(@"module_pica_biz_pic_bar_bg.png") stretchableImageWithLeftCapWidth:1 topCapHeight:10];
       self.navigationController.navigationBar.translucent = YES;
}

- (void)storePreviousNavBarAppearance {
    self.navigationController.navigationBar.backgroundImage = [ZSTModuleImage(@"module_pica_biz_pic_bar_bg.png") stretchableImageWithLeftCapWidth:1 topCapHeight:10];
    self.navigationController.navigationBar.translucent = YES;
}

- (void)restorePreviousNavBarAppearance:(BOOL)animated {
    self.navigationController.navigationBar.backgroundImage = [ZSTModuleImage(@"module_pica_biz_pic_bar_bg.png") stretchableImageWithLeftCapWidth:1 topCapHeight:10];
    self.navigationController.navigationBar.translucent = YES;
}

#pragma mark - Layout

- (void)viewWillLayoutSubviews {
    
    // Super
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"5")) [super viewWillLayoutSubviews];
	
	// Flag
	_performingLayout = YES;
	
	// Toolbar
	_toolbar.frame = [self frameForToolbarAtOrientation:self.interfaceOrientation];
	
	// Remember index
	NSUInteger indexPriorToLayout = _currentPageIndex;
	
	// Get paging scroll view frame to determine if anything needs changing
	CGRect pagingScrollViewFrame = [self frameForPagingScrollView];
    
	// Frame needs changing
	_pagingScrollView.frame = pagingScrollViewFrame;
	
	// Recalculate contentSize based on current orientation
	_pagingScrollView.contentSize = [self contentSizeForPagingScrollView];
	
	// Adjust frames and configuration of each visible page
	for (MWZoomingScrollView *page in _visiblePages) {
        NSUInteger index = PAGE_INDEX(page);
		page.frame = [self frameForPageAtIndex:index];
        page.captionView.frame = [self frameForCaptionView:page.captionView atIndex:index];
		[page setMaxMinZoomScalesForCurrentBounds];
	}
	
	// Adjust contentOffset to preserve page location based on values collected prior to location
	_pagingScrollView.contentOffset = [self contentOffsetForPageAtIndex:indexPriorToLayout];
	[self didStartViewingPageAtIndex:_currentPageIndex]; // initial
    
	// Reset
	_currentPageIndex = indexPriorToLayout;
	_performingLayout = NO;
    
}

#pragma mark - Rotation

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation {
    return YES;
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    
	// Remember page index before rotation
	_pageIndexBeforeRotation = _currentPageIndex;
	_rotating = YES;
	
}

- (void)willAnimateRotationToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
	
	// Perform layout
	_currentPageIndex = _pageIndexBeforeRotation;
    
	// Layout manually (iOS < 5)
    if (SYSTEM_VERSION_LESS_THAN(@"5")) [self viewWillLayoutSubviews];
	
	// Delay control holding
//	[self hideControlsAfterDelay];
	
}

- (void)didRotateFromInterfaceOrientation:(UIInterfaceOrientation)fromInterfaceOrientation {
	_rotating = NO;
}

#pragma mark - Data

- (void)reloadData {
    
    // Reset
    _photoCount = NSNotFound;
    
    // Get data
    NSUInteger numberOfPhotos = [self numberOfPhotos];
    [self releaseAllUnderlyingPhotos];
    [_photos removeAllObjects];
    for (int i = 0; i < numberOfPhotos; i++) [_photos addObject:[NSNull null]];
    
    // Update
    [self performLayout];
    
    // Layout
    if (SYSTEM_VERSION_LESS_THAN(@"5")) [self viewWillLayoutSubviews];
    else [self.view setNeedsLayout];
    
}

- (NSUInteger)numberOfPhotos {
    if (_photoCount == NSNotFound) {
        if ([_delegate respondsToSelector:@selector(numberOfPhotosInPhotoBrowser:)]) {
            _photoCount = [_delegate numberOfPhotosInPhotoBrowser:self];
        } else if (_depreciatedPhotoData) {
            _photoCount = _depreciatedPhotoData.count;
        }
    }
    if (_photoCount == NSNotFound) _photoCount = 0;
    return _photoCount;
}

- (id<MWPhoto>)photoAtIndex:(NSUInteger)index {
    id <MWPhoto> photo = nil;
    if (index < _photos.count) {
        if ([_photos objectAtIndex:index] == [NSNull null]) {
            
            if (_delegate) {
                if ([_delegate respondsToSelector:@selector(photoBrowser:photoAtIndex:)]) {
                    photo = [_delegate photoBrowser:self photoAtIndex:index];
                } else if (_depreciatedPhotoData && index < _depreciatedPhotoData.count) {
                    photo = [_depreciatedPhotoData objectAtIndex:index];
                }
            }
            
            if (photo) [_photos replaceObjectAtIndex:index withObject:photo];
            
        } else {
            photo = [_photos objectAtIndex:index];
        }
    }
    return photo;
}

- (MWCaptionView *)captionViewForPhotoAtIndex:(NSUInteger)index {
    MWCaptionView *captionView = nil;
    if ([_delegate respondsToSelector:@selector(photoBrowser:captionViewForPhotoAtIndex:)]) {
        captionView = [_delegate photoBrowser:self captionViewForPhotoAtIndex:index];
    } else {
        id <MWPhoto> photo = [self photoAtIndex:index];
        NSLog(@"oneDic --> %@,%@",photo.title,photo.shareurl);
        if ([photo respondsToSelector:@selector(caption)]) {
            if ([photo caption]) captionView = [[MWCaptionView alloc] initWithPhoto:photo];
        }
    }
    captionView.alpha =  1; // Initial alpha
    captionView.delegate = self;
    return captionView;
}

- (UIImage *)imageForPhoto:(id<MWPhoto>)photo {
	if (photo) {
		// Get image or obtain in background
		if ([photo underlyingImage]) {
			return [photo underlyingImage];
		} else {
            [photo loadUnderlyingImageAndNotify];
		}
	}
	return nil;
}

- (void)loadAdjacentPhotosIfNecessary:(id<MWPhoto>)photo {
    MWZoomingScrollView *page = [self pageDisplayingPhoto:photo];
    if (page) {
        // If page is current page then initiate loading of previous and next pages
        NSUInteger pageIndex = PAGE_INDEX(page);
        if (_currentPageIndex == pageIndex) {
            if (pageIndex > 0) {
                // Preload index - 1
                id <MWPhoto> photo = [self photoAtIndex:pageIndex-1];
                if (![photo underlyingImage]) {
                    [photo loadUnderlyingImageAndNotify];
                    MWLog(@"Pre-loading image at index %i", pageIndex-1);
                }
            }
            if (pageIndex < [self numberOfPhotos] - 1) {
                // Preload index + 1
                id <MWPhoto> photo = [self photoAtIndex:pageIndex+1];
                if (![photo underlyingImage]) {
                    [photo loadUnderlyingImageAndNotify];
                    MWLog(@"Pre-loading image at index %i", pageIndex+1);
                }
            }
        }
    }
}

#pragma mark - MWPhoto Loading Notification

- (void)handleMWPhotoLoadingDidEndNotification:(NSNotification *)notification {
    id <MWPhoto> photo = [notification object];
    MWZoomingScrollView *page = [self pageDisplayingPhoto:photo];
    if (page) {
        if ([photo underlyingImage]) {
            // Successful load
            [page displayImage];
            [self loadAdjacentPhotosIfNecessary:photo];
        } else {
            // Failed to load
            [page displayImageFailure];
        }
    }
}

#pragma mark - Paging

- (void)tilePages {
	
	// Calculate which pages should be visible
	// Ignore padding as paging bounces encroach on that
	// and lead to false page loads
	CGRect visibleBounds = _pagingScrollView.bounds;
	NSInteger iFirstIndex = (int)floorf((CGRectGetMinX(visibleBounds)+PADDING*2) / CGRectGetWidth(visibleBounds));
	NSInteger iLastIndex  = (int)floorf((CGRectGetMaxX(visibleBounds)-PADDING*2-1) / CGRectGetWidth(visibleBounds));
    if (iFirstIndex < 0) iFirstIndex = 0;
    if (iFirstIndex > [self numberOfPhotos] - 1) iFirstIndex = [self numberOfPhotos] - 1;
    if (iLastIndex < 0) iLastIndex = 0;
    if (iLastIndex > [self numberOfPhotos] - 1) iLastIndex = [self numberOfPhotos] - 1;
	
	// Recycle no longer needed pages
    NSInteger pageIndex;
	for (MWZoomingScrollView *page in _visiblePages) {
        pageIndex = PAGE_INDEX(page);
		if (pageIndex < (NSUInteger)iFirstIndex || pageIndex > (NSUInteger)iLastIndex) {
			[_recycledPages addObject:page];
            [page prepareForReuse];
			[page removeFromSuperview];
			MWLog(@"Removed page at index %i", PAGE_INDEX(page));
		}
	}
	[_visiblePages minusSet:_recycledPages];
    while (_recycledPages.count > 2) // Only keep 2 recycled pages
        [_recycledPages removeObject:[_recycledPages anyObject]];
	
	// Add missing pages
	for (NSUInteger index = (NSUInteger)iFirstIndex; index <= (NSUInteger)iLastIndex; index++) {
		if (![self isDisplayingPageForIndex:index]) {
            
            // Add new page
			MWZoomingScrollView *page = [self dequeueRecycledPage];
			if (!page) {
				page = [[MWZoomingScrollView alloc] initWithPhotoBrowser:self];
			}
			[self configurePage:page forIndex:index];
			[_visiblePages addObject:page];
			[_pagingScrollView addSubview:page];
			MWLog(@"Added page at index %i", index);
            
            // Add caption
            MWCaptionView *captionView = [self captionViewForPhotoAtIndex:index];
            captionView.frame = [self frameForCaptionView:captionView atIndex:index];
            [_pagingScrollView addSubview:captionView];
            
            if ([self numberOfPhotos] > 0) {
                [captionView setIndexWithText:[NSString stringWithFormat:@"%@/%@", @(index+1), @([self numberOfPhotos])]];
            } else {
                [captionView setIndexWithText:@""];
            }
            [captionView setZstDao:self.dao];
            captionView.alpha = [UIApplication sharedApplication].statusBarHidden ? 0:1;
            page.captionView = captionView;
            
		}
	}
}

- (BOOL)isDisplayingPageForIndex:(NSUInteger)index {
	for (MWZoomingScrollView *page in _visiblePages)
		if (PAGE_INDEX(page) == index) return YES;
	return NO;
}

- (MWZoomingScrollView *)pageDisplayedAtIndex:(NSUInteger)index {
	MWZoomingScrollView *thePage = nil;
	for (MWZoomingScrollView *page in _visiblePages) {
		if (PAGE_INDEX(page) == index) {
			thePage = page; break;
		}
	}
	return thePage;
}

- (MWZoomingScrollView *)pageDisplayingPhoto:(id<MWPhoto>)photo {
	MWZoomingScrollView *thePage = nil;
	for (MWZoomingScrollView *page in _visiblePages) {
		if (page.photo == photo) {
			thePage = page; break;
		}
	}
	return thePage;
}

- (void)configurePage:(MWZoomingScrollView *)page forIndex:(NSUInteger)index {
	page.frame = [self frameForPageAtIndex:index];
    page.tag = PAGE_INDEX_TAG_OFFSET + index;
    page.photo = [self photoAtIndex:index];
}

- (MWZoomingScrollView *)dequeueRecycledPage {
	MWZoomingScrollView *page = [_recycledPages anyObject];
	if (page) {
		[_recycledPages removeObject:page];
	}
	return page;
}

// Handle page changes
- (void)didStartViewingPageAtIndex:(NSUInteger)index {
    
    // Release images further away than +/-1
    NSUInteger i;
    if (index > 0) {
        // Release anything < index - 1
        for (i = 0; i < index-1; i++) { 
            id photo = [_photos objectAtIndex:i];
            if (photo != [NSNull null]) {
                [photo unloadUnderlyingImage];
                [_photos replaceObjectAtIndex:i withObject:[NSNull null]];
                MWLog(@"Released underlying image at index %i", i);
            }
        }
    }
    if (index < [self numberOfPhotos] - 1) {
        // Release anything > index + 1
        for (i = index + 2; i < _photos.count; i++) {
            id photo = [_photos objectAtIndex:i];
            if (photo != [NSNull null]) {
                [photo unloadUnderlyingImage];
                [_photos replaceObjectAtIndex:i withObject:[NSNull null]];
                MWLog(@"Released underlying image at index %i", i);
            }
        }
    }
    
    // Load adjacent images if needed and the photo is already
    // loaded. Also called after photo has been loaded in background
    id <MWPhoto> currentPhoto = [self photoAtIndex:index];
    if ([currentPhoto underlyingImage]) {
        // photo loaded so load ajacent now
        [self loadAdjacentPhotosIfNecessary:currentPhoto];
    }
    
}

#pragma mark - Frame Calculations

- (CGRect)frameForPagingScrollView {
    CGRect frame = self.view.bounds;// [[UIScreen mainScreen] bounds];
    frame.origin.x -= PADDING;
    frame.size.width += (2 * PADDING);
    return frame;
}

- (CGRect)frameForPageAtIndex:(NSUInteger)index {
    // We have to use our paging scroll view's bounds, not frame, to calculate the page placement. When the device is in
    // landscape orientation, the frame will still be in portrait because the pagingScrollView is the root view controller's
    // view, so its frame is in window coordinate space, which is never rotated. Its bounds, however, will be in landscape
    // because it has a rotation transform applied.
    CGRect bounds = _pagingScrollView.bounds;
    CGRect pageFrame = bounds;
    pageFrame.size.width -= (2 * PADDING);
    pageFrame.origin.x = (bounds.size.width * index) + PADDING;
    return pageFrame;
}

- (CGSize)contentSizeForPagingScrollView {
    // We have to use the paging scroll view's bounds to calculate the contentSize, for the same reason outlined above.
    CGRect bounds = _pagingScrollView.bounds;
    return CGSizeMake(bounds.size.width * [self numberOfPhotos], bounds.size.height);
}

- (CGPoint)contentOffsetForPageAtIndex:(NSUInteger)index {
	CGFloat pageWidth = _pagingScrollView.bounds.size.width;
	CGFloat newOffset = index * pageWidth;
	return CGPointMake(newOffset, 0);
}

- (CGRect)frameForToolbarAtOrientation:(UIInterfaceOrientation)orientation {
    CGFloat height = 44;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone &&
        UIInterfaceOrientationIsLandscape(orientation)) height = 32;
	return CGRectMake(0, self.view.bounds.size.height - height, self.view.bounds.size.width, height);
}

- (CGRect)frameForCaptionView:(MWCaptionView *)captionView atIndex:(NSUInteger)index {
    CGRect pageFrame = [self frameForPageAtIndex:index];
    captionView.frame = CGRectMake(0, 0, pageFrame.size.width, 44); // set initial frame
    CGSize captionSize = [captionView sizeThatFits:CGSizeMake(pageFrame.size.width, 0)];
    CGRect captionFrame = CGRectMake(pageFrame.origin.x, pageFrame.size.height - captionSize.height - (_toolbar.superview?_toolbar.frame.size.height:0), pageFrame.size.width, captionSize.height);
    return captionFrame;
}

#pragma mark - UIScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
	
    // Checks
	if (!_viewIsActive || _performingLayout || _rotating) return;
	
	// Tile pages
	[self tilePages];
	
	// Calculate current page
	CGRect visibleBounds = _pagingScrollView.bounds;
	NSInteger index = (NSInteger)(floorf(CGRectGetMidX(visibleBounds) / CGRectGetWidth(visibleBounds)));
    if (index < 0) index = 0;
	if (index > [self numberOfPhotos] - 1) index = [self numberOfPhotos] - 1;
	NSUInteger previousCurrentPage = _currentPageIndex;
	_currentPageIndex = index;
	if (_currentPageIndex != previousCurrentPage) {
        [self didStartViewingPageAtIndex:index];
    }
	
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
	// Update nav when page changes
	[self updateNavigation];
}

#pragma mark - Navigation

- (void)updateNavigation {
    	
	// Buttons
	_previousButton.enabled = (_currentPageIndex > 0);
	_nextButton.enabled = (_currentPageIndex < [self numberOfPhotos]-1);
	
}

- (void)jumpToPageAtIndex:(NSUInteger)index {
	
	// Change page
	if (index < [self numberOfPhotos]) {
		CGRect pageFrame = [self frameForPageAtIndex:index];
		_pagingScrollView.contentOffset = CGPointMake(pageFrame.origin.x - PADDING, 0);
		[self updateNavigation];
	}
	
	// Update timer to give more time
//	[self hideControlsAfterDelay];
	
}

- (void)gotoPreviousPage { [self jumpToPageAtIndex:_currentPageIndex-1]; }
- (void)gotoNextPage { [self jumpToPageAtIndex:_currentPageIndex+1]; }

#pragma mark - Control Hiding / Showing

// If permanent then we don't set timers to hide again
- (void)setControlsHidden:(BOOL)hidden animated:(BOOL)animated permanent:(BOOL)permanent {
    
	// Status bar and nav bar positioning
//    if (self.wantsFullScreenLayout) {
//        
//        // Get status bar height if visible
//        CGFloat statusBarHeight = 0;
//        if (![UIApplication sharedApplication].statusBarHidden) {
//            CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
//            statusBarHeight = MIN(statusBarFrame.size.height, statusBarFrame.size.width);
//        }
//        
//        // Status Bar
//        if ([UIApplication instancesRespondToSelector:@selector(setStatusBarHidden:withAnimation:)]) {
//            [[UIApplication sharedApplication] setStatusBarHidden:hidden withAnimation:animated?UIStatusBarAnimationFade:UIStatusBarAnimationNone];
//        } else {
//            [[UIApplication sharedApplication] setStatusBarHidden:hidden animated:animated];
//        }
//        
//        // Get status bar height if visible
//        if (![UIApplication sharedApplication].statusBarHidden) {
//            CGRect statusBarFrame = [[UIApplication sharedApplication] statusBarFrame];
//            statusBarHeight = MIN(statusBarFrame.size.height, statusBarFrame.size.width);
//        }
//        
//        // Set navigation bar frame
//        CGRect navBarFrame = self.navigationController.navigationBar.frame;
//        navBarFrame.origin.y = statusBarHeight;
//        self.navigationController.navigationBar.frame = navBarFrame;
//        
//    }
    
    // Captions
    NSMutableSet *captionViews = [[NSMutableSet alloc] initWithCapacity:_visiblePages.count];
    for (MWZoomingScrollView *page in _visiblePages) {
        if (page.captionView) [captionViews addObject:page.captionView];
    }
	
	// Animate
    if (animated) {
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.35];
    }
    CGFloat alpha = isshow ? 0 : 1;
	[self.navigationController.navigationBar setAlpha:alpha];
//	[_toolbar setAlpha:alpha];
    for (UIView *v in captionViews) v.alpha = alpha;
	if (animated) [UIView commitAnimations];
	
	// Control hiding timer
	// Will cancel existing timer but only begin hiding if
	// they are visible
//	if (!permanent) [self hideControlsAfterDelay];
	
}


// Enable/disable control visiblity timer
//- (void)hideControlsAfterDelay {
//	[self areControlsHidden];
//}

//- (BOOL)areControlsHidden { return (_toolbar.alpha == 0); /* [UIApplication sharedApplication].isStatusBarHidden; */ }
- (void)hideControls
{
    [self setControlsHidden:YES animated:YES permanent:NO];
}
- (void)toggleControls
{
    isshow = !isshow;
    [self.view bringSubviewToFront:_blackView];

//    if (isshow) {
//        [self.view bringSubviewToFront:_blackView];
//    }
//    else
//    {
//        [self.view sendSubviewToBack:_blackView];
//    }
    [self setControlsHidden:![UIApplication sharedApplication].isStatusBarHidden animated:NO permanent:NO];
}

#pragma mark - Properties

- (void)setInitialPageIndex:(NSUInteger)index {
    // Validate
    if (index >= [self numberOfPhotos]) index = [self numberOfPhotos]-1;
    _currentPageIndex = index;
	if ([self isViewLoaded]) {
        [self jumpToPageAtIndex:index];
        if (!_viewIsActive) [self tilePages]; // Force tiling if view is not visible
    }
}

#pragma mark - Misc

- (void)doneButtonPressed:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)actionButtonPressed:(id)sender {
    if (_actionsSheet) {
        // Dismiss
        [_actionsSheet dismissWithClickedButtonIndex:_actionsSheet.cancelButtonIndex animated:YES];
    } else {
        id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];
        if ([self numberOfPhotos] > 0 && [photo underlyingImage]) {
            
            // Keep controls hidden
            [self setControlsHidden:NO animated:YES permanent:YES];
            
            // Sheet
            if ([MFMailComposeViewController canSendMail]) {
                self.actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Save", nil), NSLocalizedString(@"Copy", nil), NSLocalizedString(@"Email", nil), nil];
            } else {
                self.actionsSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self
                                                        cancelButtonTitle:NSLocalizedString(@"Cancel", nil) destructiveButtonTitle:nil
                                                        otherButtonTitles:NSLocalizedString(@"Save", nil), NSLocalizedString(@"Copy", nil), nil];
            }
            _actionsSheet.actionSheetStyle = UIActionSheetStyleBlackTranslucent;
            if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
                [_actionsSheet showFromBarButtonItem:sender animated:YES];
            } else {
                [_actionsSheet showInView:self.view];
            }
            
        }
    }
}


#pragma mark- ZSTHHSinaShareControllerDelegate

- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}
#pragma mark wxShare

- (void)wxShareSucceed
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"微信分享成功", nil) withImage:nil];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0) {
        NSString* installUrl =  [WXApi getWXAppInstallUrl];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:installUrl]];
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"分享成功!" , nil)];
    }
}

#pragma mark - UIActionSheetDelegate

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];

    if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]
        || [[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈" , nil)]){
        
        if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"提示" , nil)
                                  message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                  otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
            alert.tag = 108;
            [alert show];
            return;
        }
        // 发送内容给微信
        
        WXMediaMessage *message = [WXMediaMessage message];
        [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
        message.title = photo.title;
        
        if ([photo.caption length] != 0) {
            if ([photo.caption length] >50) {
                message.description = [photo.caption substringToIndex:50];
            }else{
                message.description = photo.caption;
            }
        }else{
            message.description = NSLocalizedString(@"点击查看详情" , nil);
        }
        
        
        WXImageObject *ext = [WXImageObject object];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
        ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
        message.mediaObject = ext;
        
        WXWebpageObject *webpage = [WXWebpageObject object];
        webpage.webpageUrl = [photo.shareurl length] ? photo.shareurl:[NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
        message.mediaObject = webpage;
        
        SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.message = message;
        
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信好友" , nil)]) {
            req.scene = WXSceneSession;
        }
        else  if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"微信朋友圈"  , nil)]){
            req.scene = WXSceneTimeline;
        }
        
        [WXApi sendReq:req];
        
    }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"短信" , nil)]){
        
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
        
        if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
            
            MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
            
            if ([photo.shareurl length] != 0) {
                    picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在“%@”手机客户端，看到了图集《%@》,分享给你 %@", nil),appDisplayName,photo.title,photo.shareurl];
            }else{
                NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
                    picker.body = [NSString stringWithFormat:NSLocalizedString(@"我在“%@”手机客户端，看到了图集《%@》,分享给你,手机客户端下载地址：%@", nil),appDisplayName,photo.title,sharebaseurl];
            }
        
            
            picker.messageComposeDelegate = self;
            
            [self presentModalViewController: picker animated:YES];
            
        } else {
            
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
        }
    }else{
        
        ZSTHHSinaShareController *sinaShare = [[ZSTHHSinaShareController alloc] init];
        sinaShare.delegate = self;
        if ([photo.shareurl length] != 0) {
            sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"#分享图集#:%@,%@", nil),photo.title,photo.shareurl];
        }else{
            NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];

            sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"#分享图集#:%@,下载客户端一起分享吧，%@", nil),photo.title,sharebaseurl];

        }
        if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"新浪微博" , nil)]) {
            sinaShare.shareType = sinaWeibo_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"新浪微博", nil);
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"QQ空间" , nil)]){
            sinaShare.shareType = QQ_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"QQ空间", nil);
        }else if ([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString: NSLocalizedString(@"腾讯微博" , nil)]){
            sinaShare.shareType = TWeibo_ShareType;
            sinaShare.navigationItem.title = NSLocalizedString(@"腾讯微博", nil);
        }
        
        UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:sinaShare];
        [self presentModalViewController:n animated:YES];

    }
}


- (void)shareAction
{
    id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];
    NSString *sharebaseurl = [NSString stringWithFormat:NSLocalizedString(@"GP_shareUrl", nil)];
    
    self.shareView.shareString = [NSString stringWithFormat:NSLocalizedString(@"#分享图集#:%@,下载客户端一起分享吧，%@", nil),photo.title,sharebaseurl];
    
    self.clearView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        self.shareView.frame = CGRectMake(0, HEIGHT - 64 - self.shareView.frame.size.height, WIDTH, self.shareView.frame.size.height);
    }];
    
    
    
//    UIActionSheet *shareActionSheet = nil;
//    NSMutableArray *shareNames = [NSMutableArray array];
//    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
//    if (pre.SinaWeiBo) {
//        [shareNames addObject:NSLocalizedString(@"新浪微博",nil)];
//    }
//    if (pre.TWeiBo) {
//        [shareNames addObject:NSLocalizedString(@"腾讯微博",nil)];
//    }
//    if (pre.QQ) {
//        [shareNames addObject:NSLocalizedString(@"QQ空间",nil)];
//    }
//    if (pre.WeiXin) {
//        [shareNames addObject:NSLocalizedString(@"微信好友",nil)];
//        [shareNames addObject:NSLocalizedString(@"微信朋友圈",nil)];
//    }
//    
//    [shareNames addObject:NSLocalizedString(@"短信",nil)];
//    
//    shareActionSheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"分享到", nil)
//                                                   delegate:self
//                                          cancelButtonTitle:nil
//                                     destructiveButtonTitle:nil
//                                          otherButtonTitles:nil];
//    
//    for (NSString * title in shareNames) {
//        [shareActionSheet addButtonWithTitle:title];
//    }
//    [shareActionSheet addButtonWithTitle:NSLocalizedString(@"取消",nil)];
//    shareActionSheet.cancelButtonIndex = shareActionSheet.numberOfButtons-1;
//    shareActionSheet.tag = 101;
//    [shareActionSheet showInView:self.view.window];
//    [shareActionSheet release];
}

- (void)createShareView:(BOOL)isLogin AndResponse:(NSDictionary *)response AndIconUrl:(NSString *)iconUrl
{
    NSInteger shareCount = 0;
    if (!self.shareView)
    {
        self.shareView = [[ZSTNewShareView alloc] init];
        self.shareView.shareDelegate = self;
        self.shareView.backgroundColor = [UIColor whiteColor];
        
        if ([ZSTF3Preferences shared].qqKey && [[ZSTF3Preferences shared].qqKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].qqKey isEqualToString:@""])
        {
            self.shareView.isHasQQ = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasQQ = NO;
        }
        
        if ([ZSTF3Preferences shared].wxKey && [[ZSTF3Preferences shared].wxKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].wxKey isEqualToString:@""])
        {
            self.shareView.isHasWX = YES;
            shareCount += 2;
        }
        else
        {
            self.shareView.isHasWX = NO;
        }
        
        if ([ZSTF3Preferences shared].weiboKey && [[ZSTF3Preferences shared].weiboKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].weiboKey isEqualToString:@""])
        {
            self.shareView.isHasWeiBo = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasWeiBo = NO;
        }
        
        
        NSInteger heights = shareCount / 4 + 1;
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, heights * 100);
        [self.view addSubview: self.shareView];
    }

    id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];
    
    
    if (isLogin)
    {
        self.shareView.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.shareView.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        self.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
//        self.shareView.shareString = shareString;
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.shareDelegate = self;
        
        self.shareView.isTodayWX = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXin"] integerValue] == 0 ? NO : YES;
        self.shareView.isTodayWXFriedn = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXinFriends"] integerValue] == 0 ? NO : YES;
        self.shareView.isTodayWeiBo = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiBo"] integerValue] == 0 ? NO : YES;
        self.shareView.isTodayQQ = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareQq"] integerValue] == 0 ? NO : YES;
        self.shareView.shareString = photo.shareurl;
        [self.shareView createShareUI];
        [self.shareView checkIsHasShare];
    }
    else
    {
        self.shareView.isTodayWX = YES;
        self.shareView.isTodayWXFriedn = YES;
        self.shareView.isTodayWeiBo = YES;
        self.shareView.isTodayQQ = YES;
        [self.shareView checkIsHasShare];
        
        self.shareView.shareString = photo.shareurl;
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.shareDelegate = self;
        
        [ self.shareView createShareUI];
        [self.shareView checkIsHasShare];
    }
}

- (void)buildDarkView
{
    self.clearView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.clearView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.clearView];
    
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    darkView.backgroundColor = [UIColor blackColor];
    darkView.alpha = 0.8;
    [self.clearView addSubview:darkView];
    
    self.clearView.hidden = YES;
}


- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo downloadBtnDidSelected:(UIButton *)sender
{
    [self savePhoto];
}
- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo shareBtnDidSelected:(UIButton *)sender
{
    [self shareAction];

}

//  --- 赞 ---
- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo favBtnDidSelected:(UIButton *)sender isAdd:(BOOL)isadd
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        
        return;
    }
    
    if (isadd) {
         [self.dao addFavPicListWithCategoryId:@(self.piclist.categoryid).stringValue
                                        msgid:@(self.piclist.msgid).stringValue
                                        title:self.piclist.title
                                        width:self.piclist.width
                                       height:self.piclist.height
                                       imgurl:self.piclist.imgurl
                               favoritescount:self.piclist.favoritescount
                                      addtime:self.piclist.addtime];
        [self.engine addPicACountWithMessageId:@(self.piclist.msgid).stringValue type:fav_AddType];

    }else{
        BOOL cancel = [self.dao deleteFavPicWithMsgid:@(self.piclist.msgid).stringValue];
        if (cancel) {
            [TKUIUtil alertInView:self.view withTitle:@"取消收藏" withImage:nil];
        }
    }
    [self reloadData];
}

- (void)captionView:(MWCaptionView *)captionview photo:(id<MWPhoto>)photo linkBtnDidSelected:(UIButton *)sender
{
    ZSTPicAWebViewController *webVC = [[ZSTPicAWebViewController alloc] init];
    webVC.linkurl = [photo.linkText length] ? photo.linkText : @"";
    [self.navigationController pushViewController:webVC animated:YES];
    self.wantsFullScreenLayout = NO;
    self.navigationController.navigationBar.translucent = NO;
}

- (void)addCountDidSucceed:(NSInteger)count
{
    [TKUIUtil alertInView:self.view withTitle:@"收藏成功" withImage:nil];
    self.piclist.favoritescount = [NSString stringWithFormat:@"%ld",(long)count];
}

- (void)addCountDidFailed:(id)notice
{
    [TKUIUtil alertInView:self.view withTitle:@"收藏失败" withImage:nil];
}


#pragma mark - Action Sheet Delegate

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (actionSheet == _actionsSheet) {           
        // Actions 
        self.actionsSheet = nil;
        if (buttonIndex != actionSheet.cancelButtonIndex) {
            if (buttonIndex == actionSheet.firstOtherButtonIndex) {
                [self savePhoto]; return;
            } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 1) {
                [self copyPhoto]; return;	
            } else if (buttonIndex == actionSheet.firstOtherButtonIndex + 2) {
                [self emailPhoto]; return;
            }
        }
    }
//    [self hideControlsAfterDelay]; // Continue as normal...
}

#pragma mark - MBProgressHUD

- (MBProgressHUD *)progressHUD {
    if (!_progressHUD) {
        _progressHUD = [[MBProgressHUD alloc] initWithView:self.view];
        _progressHUD.minSize = CGSizeMake(120, 120);
        _progressHUD.minShowTime = 1;
        self.progressHUD.customView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"MWPhotoBrowser.bundle/images/Checkmark.png"]];
        [self.view addSubview:_progressHUD];
    }
    return _progressHUD;
}

- (void)showProgressHUDWithMessage:(NSString *)message {
    self.progressHUD.labelText = message;
    self.progressHUD.mode = MBProgressHUDModeIndeterminate;
    [self.progressHUD show:YES];
    self.navigationController.navigationBar.userInteractionEnabled = NO;
}

- (void)hideProgressHUD:(BOOL)animated {
    [self.progressHUD hide:animated];
    self.navigationController.navigationBar.userInteractionEnabled = YES;
}

- (void)showProgressHUDCompleteMessage:(NSString *)message {
    if (message) {
        if (self.progressHUD.isHidden) [self.progressHUD show:YES];
        self.progressHUD.labelText = message;
        self.progressHUD.mode = MBProgressHUDModeCustomView;
        [self.progressHUD hide:YES afterDelay:1.5];
    } else {
        [self.progressHUD hide:YES];
    }
    self.navigationController.navigationBar.userInteractionEnabled = YES;
}

#pragma mark - Actions

- (void)savePhoto {
    id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];
    if ([photo underlyingImage]) {
        [self showProgressHUDWithMessage:[NSString stringWithFormat:@"%@\u2026" , NSLocalizedString(@"保存", @"Displayed with ellipsis as '保存...' when an item is in the process of being saved")]];
        [self performSelector:@selector(actuallySavePhoto:) withObject:photo afterDelay:0];
    }
}

- (void)actuallySavePhoto:(id<MWPhoto>)photo {
    if ([photo underlyingImage]) {
        UIImageWriteToSavedPhotosAlbum([photo underlyingImage], self, 
                                       @selector(image:didFinishSavingWithError:contextInfo:), nil);
    }
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo {
 
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        [self showProgressHUDCompleteMessage: error ? NSLocalizedString(@"保存失败,请检查图片库是否可以访问", @"Informing the user a process has failed") : NSLocalizedString(@"保存成功", @"Informing the user an item has been saved")];
    }else{
        [self showProgressHUDCompleteMessage: error ? NSLocalizedString(@"保存失败", @"Informing the user a process has failed") : NSLocalizedString(@"保存成功", @"Informing the user an item has been saved")];
    }
}

- (void)copyPhoto {
    id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];
    if ([photo underlyingImage]) {
        [self showProgressHUDWithMessage:[NSString stringWithFormat:@"%@\u2026" , NSLocalizedString(@"Copying", @"Displayed with ellipsis as 'Copying...' when an item is in the process of being copied")]];
        [self performSelector:@selector(actuallyCopyPhoto:) withObject:photo afterDelay:0];
    }
}

- (void)actuallyCopyPhoto:(id<MWPhoto>)photo {
    if ([photo underlyingImage]) {
        [[UIPasteboard generalPasteboard] setData:UIImagePNGRepresentation([photo underlyingImage])
                                forPasteboardType:@"public.png"];
        [self showProgressHUDCompleteMessage:NSLocalizedString(@"Copied", @"Informing the user an item has finished copying")];
//        [self hideControlsAfterDelay]; // Continue as normal...
    }
}

- (void)emailPhoto {
    id <MWPhoto> photo = [self photoAtIndex:_currentPageIndex];
    if ([photo underlyingImage]) {
        [self showProgressHUDWithMessage:[NSString stringWithFormat:@"%@\u2026" , NSLocalizedString(@"Preparing", @"Displayed with ellipsis as 'Preparing...' when an item is in the process of being prepared")]];
        [self performSelector:@selector(actuallyEmailPhoto:) withObject:photo afterDelay:0];
    }
}

- (void)actuallyEmailPhoto:(id<MWPhoto>)photo {
    if ([photo underlyingImage]) {
        MFMailComposeViewController *emailer = [[MFMailComposeViewController alloc] init];
        emailer.mailComposeDelegate = self;
        [emailer setSubject:NSLocalizedString(@"Photo", nil)];
        [emailer addAttachmentData:UIImagePNGRepresentation([photo underlyingImage]) mimeType:@"png" fileName:@"Photo.png"];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
            emailer.modalPresentationStyle = UIModalPresentationPageSheet;
        }
        [self presentModalViewController:emailer animated:YES]; 
        [self hideProgressHUD:NO];
    }
}

#pragma mark Mail Compose Delegate

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    if (result == MFMailComposeResultFailed) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Email", nil)
                                                         message:NSLocalizedString(@"Email failed to send. Please try again.", nil)
                                                        delegate:nil cancelButtonTitle:NSLocalizedString(@"Dismiss", nil) otherButtonTitles:nil];
		[alert show];
    }
	[self dismissViewControllerAnimated:YES completion:nil];
}

- (void)dismissShareView
{
    self.clearView.hidden = YES;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.bounds.size.height);
        
    }];
}

- (void)getPointDidSucceed:(NSDictionary *)response
{
    [self createShareView:YES AndResponse:response AndIconUrl:self.piclist.imgurl];
}

- (void)getPointDidFailed:(NSString *)response
{
    
}

- (void)showDefaultShare
{
    
}

- (void)showSNSShare
{
    
}

- (void)sendQQShareSuccess:(NSNotification *)notification
{
    [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                userId:[ZSTF3Preferences shared].UserId
                              pointNum:self.weiboSharePoint
                             pointType:8
                            costAmount:@""
                                  desc:@""];
}

- (void)sendQQShareFail:(NSNotification *)notification
{
    
}

- (void)sendWeiBoInfoFailured
{
    
}

- (void)wxShareSucceed:(NSNotification *)notification
{    
    if ([ZSTWXShareType shareInstance].isWXShare)
    {
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.wxSharePoint
                                 pointType:6
                                costAmount:@""
                                      desc:@""];
    }
    else
    {
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.wxFriendSharePoint
                                 pointType:7
                                costAmount:@""
                                      desc:@""];
    }
    
}

- (void)wxShareFaild:(NSNotification *)notification
{
    
}

- (void)sendWeiboSuccess:(NSNotification *)notification
{
    [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                userId:[ZSTF3Preferences shared].UserId
                              pointNum:self.weiboSharePoint
                             pointType:5
                            costAmount:@""
                                  desc:@""];
}

- (void)updatePointDidSucceed:(NSDictionary *)response
{
    NSString *message = [[response safeObjectForKey:@"data"] safeObjectForKey:@"PointNum"];
    NSString *string = [NSString stringWithFormat:@"恭喜你获得%@积分",message];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(string, nil) withImage:nil];
}

- (void)updatePointDidFailed:(NSString *)response
{
    NSString *string = response;
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(string, nil) withImage:nil];
}

@end
