//
//  PicA+Info.h
//  PicA
//
//  Created by xuhuijun on 13-8-9.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTPicACategory : NSObject

@property (nonatomic, assign) NSInteger categoryid;
@property (nonatomic, retain) NSString *categoryname;

+ (NSMutableArray *)categoryPicaListWithDic:(NSDictionary *)dic;
+ (id)categoryPicaListinfoWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end


@interface ZSTPicAList : NSObject

@property (nonatomic, assign) NSInteger categoryid;
@property (nonatomic, assign) NSInteger msgid;
@property (nonatomic, assign) float width;
@property (nonatomic, assign) float height;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *imgurl;
@property (nonatomic, retain) NSString *favoritescount;
@property (nonatomic, retain) NSString * addtime;

+ (NSMutableArray *)picaListWithDic:(NSDictionary *)dic;
+ (id)picaListInfoWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;


@end

@interface ZSTPicADetailList : NSObject

@property (nonatomic, assign) int msgid;
@property (nonatomic, retain) NSString *title;
@property (nonatomic, retain) NSString *shareurl;
@property (nonatomic, retain) NSString *actionurl;
@property (nonatomic, assign) int detailid;
@property (nonatomic, retain) NSString *imgurl;
@property (nonatomic, retain) NSString *description;


+ (NSMutableArray *)detailPicaListWithDic:(NSDictionary *)dic;
+ (id)detailPicaListInfoWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;


@end