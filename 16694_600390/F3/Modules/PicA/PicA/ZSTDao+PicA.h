//
//  ZSTDao+PicA.h
//  PicA
//
//  Created by xuhuijun on 13-8-8.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTDao.h"

@interface ZSTDao (PicA)

- (void)createTableIfNotExistForPicAModule;



//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 添加分类
 *
 *	@param 	categoryID      分类ID
 *	@param 	categoryName 	分类名称
 *	@return	操作是否成功
 */

- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName;

/**
 *	@brief 删除分类
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllPicACategories;

/**
 *	@brief  获取所有分类
 *	@return	操作是否成功
 */

- (NSArray *)getPicACategories;



/**
 *	@brief 添加列表
 *
 *	@param 	categoryID      分类ID
 *	@param 	msgid 	        信息id
 *	@return	操作是否成功
 */

- (BOOL)addPicAListWithCategoryId:(NSString *)categoryid
                            msgid:(NSString *)msgid
                            title:(NSString *)title
                            width:(float)width
                            height:(float)height
                           imgurl:(NSString *)imgurl
                   favoritescount:(NSString *)favoritescount
                          addtime:(NSString *)addtime;



/**
 *	@brief 删除列表图片
 *
 *	@return	操作是否成功
 */

- (BOOL)deleteAllPicAWithCategoryId:(NSString *)categoryid;


/**
 *	@brief  获取列表
 *	@return	操作是否成功
 */

- (NSArray *)picAListAtPage:(int)pageNum pageCount:(int)pageCount category:(NSString *)categoryID;


- (NSUInteger)picACountOfCategory:(NSString *)categoryID;
/**
 *	@brief 添加列表详细
 *
 *	@param 	categoryID      分类ID
 *	@param 	msgid 	        信息id
 *	@return	操作是否成功
 */

- (BOOL)addPicADetailListWithCategoryId:(NSString *)msgid
                            msgid:(NSString *)title
                            title:(NSString *)shareurl
                         shareurl:(NSString *)actionurl
                        actionurl:(NSString *)detailid
                         detailid:(NSString *)imgurl
                           imgurl:(NSString *)description;



/**
 *	@brief 删除列表图片
 *
 *	@return	操作是否成功
 */

- (BOOL)deletePicADetailWithMsgid:(NSString *)msgid detailid:(NSString *)detailid;


/**
 *	@brief  获取列表
 *	@return	操作是否成功
 */

- (NSArray *)getPicADetailListWithMsgid:(NSString *)msgid;


/**
 *	@brief  添加喜欢
 *	@return	操作是否成功
 */


- (BOOL)addFavPicListWithCategoryId:(NSString *)categoryid
                            msgid:(NSString *)msgid
                            title:(NSString *)title
                            width:(float)width
                           height:(float)height
                           imgurl:(NSString *)imgurl
                   favoritescount:(NSString *)favoritescount
                          addtime:(NSString *)addtime;

- (NSArray *)getFavPicAList;


- (BOOL)deleteFavPicWithMsgid:(NSString *)msgid;

- (BOOL)isExsitFavPicWithMsgid:(NSString *)msgid;

 

@end
