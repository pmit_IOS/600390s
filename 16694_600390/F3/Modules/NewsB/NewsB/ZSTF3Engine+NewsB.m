//
//  ZSTF3Engine+NewsB.m
//  NewsB
//
//  Created by xuhuijun on 13-7-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine+NewsB.h"
#import "ZSTGlobal+NewsB.h"
#import "ZSTDao+NewsB.h"
#import "ZSTSqlManager.h"
#import "ZSTResponse.h"
#import "JSON.h"

#define NewsbContentCachePath [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingPathComponent:@"/NewsB/content"]

@implementation ZSTF3Engine (NewsB)

////////////////////////////////////////////////////////////////// category ///////////////////////////////////////////////////////////////////////

- (void)getNewsBCategory
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetNewsBCatagory stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsBCategoryResponse:userInfo:)
                                       userInfo:nil
                                      matchCase:NO];
    
}

- (void)getNewsBCategoryResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id categories = [response.jsonResponse safeObjectForKey:@"info"];
        if (![categories isKindOfClass:[NSArray class]]) {
            categories = [NSArray array];
        }
        
        [self.dao deleteAllNewsBCategories];
        
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *category in categories){
            NSInteger categoryID = [[category safeObjectForKey:@"categoryid"] integerValue];
            [self.dao addCategory:categoryID
                     categoryName:[category safeObjectForKey:@"categoryname"]
                         orderNum:[[category safeObjectForKey:@"ordernum"] intValue]];

        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getCategoryDidSucceed:)]) {
            [self.delegate getCategoryDidSucceed:categories];
        }
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getCategoryDidFailed:)]) {
            [self.delegate getCategoryDidFailed:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

////////////////////////////////////////////////////////////////// Broadcast ///////////////////////////////////////////////////////////////////////

- (void)getNewsBBroadcast:(NSString *)categoryID
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:[categoryID integerValue]] forKey:@"categoryid"];

    [[ZSTCommunicator shared] openAPIPostToPath:[GetNewsBCarousel stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsBBroadcastResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:NO];
}

- (void)getNewsBBroadcastResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        id categories = [response.jsonResponse safeObjectForKey:@"info"];
        if (![categories isKindOfClass:[NSArray class]]) {
            categories = [NSArray array];
        }
        
        NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
        NSInteger categoryID = [[params safeObjectForKey:@"categoryid"] integerValue];
        
        if ([(NSArray * )categories count]) {
            [self.dao deleteNewsBBroadcastOfcategoryID:categoryID];
        }
        
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *category in categories){
            
            NSInteger msgId = [[category safeObjectForKey:@"msgid"] integerValue];
            [self.dao addBroadcast:msgId
                        categoryid:categoryID
                             title:[category safeObjectForKey:@"title"]
                       carouselurl:[category safeObjectForKey:@"carouselurl"]
                          orderNum:[[category safeObjectForKey:@"ordernum"] intValue]
                           linkurl:[category safeObjectForKey:@"linkurl"]];
        }
        [ZSTSqlManager commit];
        
        if ([self isValidDelegateForSelector:@selector(getBroadcastDidSucceed:)]) {
            [self.delegate getBroadcastDidSucceed:categories];
        }
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getBroadcastDidFailed:)]) {
            [self.delegate getBroadcastDidFailed:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}


////////////////////////////////////////////////////////////////// list ///////////////////////////////////////////////////////////////////////

- (void)_getNewsB:(NSString *)categoryID
             size:(NSInteger)size
        pageindex:(NSInteger)pageindex

{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setSafeObject:[NSNumber numberWithInteger:[categoryID integerValue]] forKey:@"categoryid"];
    [params setSafeObject:[NSNumber numberWithInteger:size] forKey:@"size"];
    [params setSafeObject:[NSNumber numberWithInteger:pageindex] forKey:@"pageindex"];
    
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetNewsBListByPage stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsBListResponse:userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:NO];
}

- (void)getNewsB:(NSString *)categoryID
            size:(NSInteger)size
       pageindex:(NSInteger)pageindex
{
    
    //如果是loadMore (pageNum>1),则从本地数据获取
	if (pageindex>1){
        
        NSArray *tempLocalNews = [self.dao newsBAtPage:pageindex pageCount:size category:[categoryID integerValue]];
        if ([tempLocalNews count] == 0) {
            [self _getNewsB:categoryID size:size pageindex:pageindex];
        } else {
            if ([self isValidDelegateForSelector:@selector(getNewsBListDidSucceed:hasMore:isRefresh:)]) {
                BOOL hasmore = ([tempLocalNews count] >= 10);
                [self.delegate getNewsBListDidSucceed:tempLocalNews hasMore:hasmore isRefresh:YES];
            }
        }
	} else {
        
        [self _getNewsB:categoryID size:size pageindex:1];
	}
}


- (void)getNewsBListResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSInteger categoryID = [[params safeObjectForKey:@"categoryid"] integerValue];
    NSInteger pageindex = [[params safeObjectForKey:@"pageindex"] integerValue];

    
    if (response.resultCode == ZSTResultCode_OK) {
        id newsBList = [response.jsonResponse safeObjectForKey:@"info"];
        if (![newsBList isKindOfClass:[NSArray class]]) {
            newsBList = [NSArray array];
        }
        
        //如果获取到TOPIC_PAGE_SIZE条(count)，则删除原数据(因为不连续)
        if ([newsBList count] == 10 && pageindex == 1) {
            [self.dao deleteAllNewsBOfCategory:categoryID];
        }
        
        BOOL hasmore = [[response.jsonResponse safeObjectForKey:@"hasmore"] boolValue];

        if ([self isValidDelegateForSelector:@selector(getNewsBListDidSucceed:hasMore:isRefresh:)]) {
            [self.delegate getNewsBListDidSucceed:newsBList hasMore:hasmore isRefresh:pageindex == 1? YES:NO];
        }        
        //入库
        [ZSTSqlManager beginTransaction];
        for (NSMutableDictionary *post in newsBList) {
            NSInteger msgID = [[post safeObjectForKey:@"msgid"] integerValue];
            
            if (![self.dao newsBExist:msgID]) {
                [self.dao addNews:msgID
                       categoryID:[[post safeObjectForKey:@"categoryid"] intValue]
                            title:[post safeObjectForKey:@"title"]
                           source:[post safeObjectForKey:@"source"]
                          iconurl:[post safeObjectForKey:@"iconurl"]
                         orderNum:[[post safeObjectForKey:@"ordernum"]intValue]
                          addTime:[post safeObjectForKey:@"addtime"]
                          linkUrl:[post safeObjectForKey:@"linkurl"]];
            }
        }
        [ZSTSqlManager commit];
        
    } else {
        
        if ([self isValidDelegateForSelector:@selector(getNewsBListDidFailed:)]) {
            [self.delegate getNewsBListDidFailed:[response.jsonResponse safeObjectForKey:@"notice"]];
        }
    }
}

////////////////////////////////////////////////////////////////// News content /////////////////////////////////////////////////////////////////////

//新闻内容缓存到文件中
- (void)getNewsBContent:(NSInteger)newsID userInfo:(id)anUserInfo version:(NSInteger)version
{
//    NSFileManager *fileManager = [NSFileManager defaultManager];
//    NSError *error;
//    if (![fileManager fileExistsAtPath:NewsbContentCachePath]) {
//        if (![fileManager createDirectoryAtPath:NewsbContentCachePath withIntermediateDirectories:YES attributes:nil error:&error]) {
//            if ([self isValidDelegateForSelector:@selector(getNewsBContentDidFailed:)]) {
//                ZSTResponse *response = [[ZSTResponse alloc] init];
//                response.request = nil;
//                response.stringResponse = nil;
//                response.userInfo = anUserInfo;
//                response.resultCode = ZSTResultCode_Other;
//                response.errorMsg = @"Directory create Failed.";
//                [self.delegate performSelector:@selector(getNewsBContentDidFailed:) withObject:response afterDelay:0];
//            }
//            return;
//        }
//    }
//    
    NSDictionary *userInfo = [NSDictionary dictionaryWithObjectsAndKeys:@(newsID), @"newsbid", anUserInfo, @"userInfo", @(version), @"version", nil];
    
//    //检查缓存是否有数据,如果有直接从缓存获取
//    NSString *contentPath = [NewsbContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%d_%d.json", newsID, version]];
//    BOOL isDirectory;
//    if ([fileManager fileExistsAtPath:contentPath isDirectory:&isDirectory]) {
//        if (!isDirectory) {
//            NSDictionary *attributes = [fileManager attributesOfItemAtPath:contentPath error:&error];
//            if (attributes && [attributes fileSize]) {
//                NSString *content = [NSString stringWithContentsOfFile:contentPath encoding:NSUTF8StringEncoding error:&error];
//                if (content) {
//                    id json = [content JSONValue];
//                    if (json) {
//                        [self performSelector:@selector(getNewsBContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:json, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
//                        return;
//                    }
//                    
//                } else {
//                    TKDERROR(@"error == %@", error);
//                }
//            } else {
//                TKDERROR(@"error == %@", error);
//            }
//        }
//    }
//    //从缓存获取失败，删除缓存，重新从网络获取
//    [fileManager removeItemAtPath:contentPath error:&error];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
//    [params setSafeObject:@(44351) forKey:@"msgid"];
//    [params setSafeObject:@(44399) forKey:@"msgid"];
    [params setSafeObject:@(newsID) forKey:@"msgid"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetNewsBContent stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsBContentResponse:userInfo:)
                                       userInfo:userInfo
                                      matchCase:YES];
}

- (void)getNewsBContentResponse:(NSDictionary *)param
{
    id results = [param safeObjectForKey:@"json"];
    id userInfo = [param safeObjectForKey:@"userInfo"];
    if ([self isValidDelegateForSelector:@selector(getNewsBContentDidSucceed:userInfo:)]) {
        [self.delegate getNewsBContentDidSucceed:results userInfo:[userInfo safeObjectForKey:@"userInfo"]];
    }
}

- (void)getNewsBContentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        [self performSelector:@selector(getNewsBContentResponse:) withObject:[NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"json", userInfo, @"userInfo", nil] afterDelay:0.5];
        NSNumber *newsID = [userInfo safeObjectForKey:@"newsbid"];
        NSNumber *version = [userInfo safeObjectForKey:@"version"];
        
        NSString *contentPath = [NewsbContentCachePath stringByAppendingPathComponent:[NSString stringWithFormat:@"/%@_%@.json", newsID, version]];
        NSDictionary *param = [NSDictionary dictionaryWithObjectsAndKeys:response.jsonResponse, @"results", contentPath, @"contentPath", nil];
#warning 下面一行本来是注释掉的
        [self performSelector:@selector(writeNewsBContentToCache:) withObject:param afterDelay:0];
    } else {
        if ([self isValidDelegateForSelector:@selector(getNewsBContentDidFailed:)]) {
            [self.delegate getNewsBContentDidFailed:response.resultCode userInfo:userInfo];
        }
    }
}

- (void) writeNewsBContentToCache:(NSDictionary *)param
{
    NSError *error = nil;
    if (![[[param safeObjectForKey:@"results"] JSONRepresentation] writeToFile:[param safeObjectForKey:@"contentPath"] atomically:YES encoding:NSUTF8StringEncoding error:&error]) {
        TKDERROR(@"Write news content failed, error == %@", error);
    }
}

//获取评论列表
- (void)getNewsBComment:(NSInteger)msgId
                   sinceId:(NSInteger)sinceId
                fetchCount:(NSInteger)fetchCount
                 orderType:(NSString *)orderType
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:@(msgId) forKey:@"MsgID"];
    [params setSafeObject: [NSString stringWithFormat:@"%@", @(fetchCount)] forKey:@"Size"];
    [params setSafeObject:@(sinceId) forKey:@"SinceID"];
    [params setSafeObject: orderType forKey:@"OrderType"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[GetInfoComment stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:                [NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(getNewsBCommentResponse: userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
    
}

- (void)getNewsBCommentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode != ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(getNewsBCommentDidFailed:)]) {
            [self.delegate getNewsBCommentDidFailed:response.resultCode];
        }
        return;
    }
    
    id comments = [response.jsonResponse safeObjectForKey:@"Info"];
    if (![comments isKindOfClass:[NSArray class]]) {
        comments = [NSArray array];
    }
    
    NSDictionary *params = [userInfo safeObjectForKey:@"Params"];
    NSString *sortType = [params safeObjectForKey:@"OrderType"];
    
    if ([self isValidDelegateForSelector:@selector(getNewsBCommentDidSucceed:isLoadMore:hasMore:isFinish:)]) {
        [self.delegate getNewsBCommentDidSucceed:comments isLoadMore:[sortType isEqualToString:SortType_Asc] hasMore:[[response.jsonResponse safeObjectForKey:@"HasMore"] boolValue] isFinish:YES];
    }
}

//添加评论
- (void)addNewsBComment:(NSInteger)msgId
                replyTitle:(NSString *)replyTitle
                   content:(NSString *)content
                  parentId:(NSInteger)parentId
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    
    [params setSafeObject:@(msgId) forKey:@"MsgID"];
    [params setSafeObject: replyTitle forKey:@"ReplyTitle"];
    [params setSafeObject:content forKey:@"Content"];
    [params setSafeObject: @(parentId) forKey:@"ParentID"];
    
    [[ZSTCommunicator shared] openAPIPostToPath:[InfoAddComment stringByAddingQuery:[NSDictionary dictionaryWithObjectsAndKeys:                [NSNumber numberWithInteger:self.moduleType] ,@"ModuleType", nil]]
                                         params:params
                                         target:self
                                       selector:@selector(addNewsBCommentResponse: userInfo:)
                                       userInfo:[NSDictionary dictionaryWithObjectsAndKeys:params, @"Params", nil]
                                      matchCase:YES];
    
}

- (void)addNewsBCommentResponse:(ZSTResponse *)response userInfo:(id)userInfo
{
    if (response.resultCode == ZSTResultCode_OK) {
        if ([self isValidDelegateForSelector:@selector(addNewsBCommentDidSucceed:)]) {
            [self.delegate addNewsBCommentDidSucceed:userInfo];
        }
    } else {
        if ([self isValidDelegateForSelector:@selector(addArticleACommentDidFailed:userInfo:)]) {
            [self.delegate addNewsBCommentDidFailed:response.resultCode userInfo:userInfo];
        }
    }
}



@end
