//
//  ZSTNewsBFontView.h
//  NewsB
//
//  Created by xuhuijun on 13-7-26.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import <UIKit/UIKit.h>



@protocol ZSTNewsBFontViewDelegate <NSObject>

- (void)newsbFontViewDidDismiss;
- (void)newsbFontViewSmallBtnDidSelect:(UIButton *)sender;
- (void)newsbFontViewMiddleBtnDidSelect:(UIButton *)sender;
- (void)newsbFontViewLargeBtnDidSelect:(UIButton *)sender;

@end

@interface ZSTNewsBFontView : UIView
{
    UIImageView *bgView;
    UIButton *dismissBtn;
}

@property (nonatomic,assign)id<ZSTNewsBFontViewDelegate>delegate;

@end