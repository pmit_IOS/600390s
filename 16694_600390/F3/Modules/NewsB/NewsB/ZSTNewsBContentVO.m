//
//  NewsContentVO.m
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ZSTNewsBContentVO.h"

@implementation ZSTNewsBContentVO

@synthesize title;//主标题
@synthesize source;
@synthesize originalurl;
@synthesize addtime;
@synthesize contentArr;
@synthesize topfigureurl;
@synthesize shareurl;



+ (id)voWithDic:(NSDictionary *)dic
{
    return [[[self alloc] initWithDic: dic] autorelease];
}

- (id)initWithDic:(NSDictionary *)dic
{
    if( (self=[super init])) {
        self.title =  [dic safeObjectForKey:@"title"];
        self.source = [dic safeObjectForKey:@"source"];
        self.originalurl = [dic safeObjectForKey:@"originalurl"];
        self.addtime = [dic safeObjectForKey:@"addtime"];
        self.contentArr = [dic safeObjectForKey:@"content"];
        self.topfigureurl = [dic safeObjectForKey:@"topfigureurl"];
        self.shareurl = [dic safeObjectForKey:@"shareurl"];
        self.iconUrl = [dic safeObjectForKey:@"iconurl"];

    }
    return self;
}

- (void)dealloc 
{
    self.title = nil;
    self.source = nil;
    self.originalurl = nil;
    self.addtime = nil;
    self.contentArr = nil;
    self.topfigureurl = nil;
    self.shareurl = nil;
    self.iconUrl = nil;
    [super dealloc];
}

@end
