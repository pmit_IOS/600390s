//
//  NewsContentVO.h
//  News
//
//  Created by admin on 12-7-25.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTNewsBContentVO : NSObject{
    NSDictionary *dict;
}

@property (nonatomic, retain) NSString *title;//主标题
@property (nonatomic, retain) NSString *source;//来源
@property (nonatomic, retain) NSString *originalurl;
@property (nonatomic, retain) NSString *addtime;
@property (nonatomic, retain) NSArray *contentArr;
@property (nonatomic, retain) NSString *topfigureurl;
@property (nonatomic, retain) NSString *shareurl;
@property (nonatomic, retain) NSString *iconUrl;


+ (id)voWithDic:(NSDictionary *)dic;
- (id)initWithDic:(NSDictionary *)dic;

@end
