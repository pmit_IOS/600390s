//
//  ZSTF3Engine+NewsB.h
//  NewsB
//
//  Created by xuhuijun on 13-7-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTF3Engine.h"


@protocol ZSTF3EngineNewsBDelegate <ZSTF3EngineDelegate>

@optional

- (void)getCategoryDidSucceed:(NSArray *)categories;
- (void)getCategoryDidFailed:(id)notice;

- (void)getBroadcastDidSucceed:(NSArray *)broadcast;
- (void)getBroadcastDidFailed:(id)notice;

- (void)getNewsBListDidSucceed:(NSArray *)newsBList hasMore:(BOOL)hasmore isRefresh:(BOOL)isRefresh;
- (void)getNewsBListDidFailed:(id)notice;

- (void)getNewsBContentDidSucceed:(NSDictionary *)newsContent userInfo:(id)userInfo;
- (void)getNewsBContentDidFailed:(int)resultCode userInfo:(id)userInfo;

- (void)getNewsBCommentDidSucceed:(NSArray *)theCommentList isLoadMore:(BOOL)isLoadMore hasMore:(BOOL)hasMore isFinish:(BOOL)isFinish;
- (void)getNewsBCommentDidFailed:(int)resultCode;

- (void)addNewsBCommentDidSucceed:(id)userInfo;
- (void)addNewsBCommentDidFailed:(int)resultCode userInfo:(id)userInfo;


@end

@interface ZSTF3Engine (NewsB)

- (void)getNewsBCategory;

- (void)getNewsBBroadcast:(NSString *)categoryID;

- (void)getNewsB:(NSString *)categoryID
        size:(NSInteger)size
       pageindex:(NSInteger)pageindex;

- (void)getNewsBContent:(NSInteger)newsID userInfo:(id)anUserInfo version:(NSInteger)version;

//获取评论列表
- (void)getNewsBComment:(NSInteger)msgId
                   sinceId:(NSInteger)sinceId
                fetchCount:(NSInteger)fetchCount
                 orderType:(NSString *)orderType;

//添加评论
- (void)addNewsBComment:(NSInteger)msgId
                replyTitle:(NSString *)replyTitle
                   content:(NSString *)content
                  parentId:(NSInteger)parentId;


@end
