//
//  ZSTDao+NewsB.h
//  NewsB
//
//  Created by xuhuijun on 13-7-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTDao.h"

@interface ZSTDao (NewsB)

- (void)createTableIfNotExistForNewsBModule;

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 添加分类
 *
 *	@param 	categoryID      分类ID
 *	@param 	categoryName 	分类名称
 *	@param 	orderNum        排序字段
 *  @param  defaultFlag     是否是默认头条
 *	@return	操作是否成功
 */
- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           orderNum:(NSInteger)orderNum;

/**
 *	@brief 删除分类
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllNewsBCategories;


/**
 *	@brief  获取所有分类
 *	@return	操作是否成功
 */
- (NSArray *)getNewsBCategories;

/**
 *	@brief 添加新闻
 *
 *  @param 	msgID           新闻ID
 *	@param 	categoryID      分类ID
 *	@param 	title           新闻标题
 *	@param 	source         新闻信息摘要
 *	@param 	iconurl      ICon图片编号
 *	@param 	orderNum        新闻信息来源
 *  @param  addTime         添加时间
 *	@return	操作是否成功
 */
- (BOOL)addNews:(NSInteger)msgID
     categoryID:(NSInteger)categoryID
          title:(NSString *)title
        source:(NSString *)source
     iconurl:(NSString *)iconurl
       orderNum:(NSInteger)orderNum
        addTime:(NSString *)addTime
        linkUrl:(NSString *)linkurl;

/**
 *	@brief  设置新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	操作是否成功
 */
- (BOOL)setNewsB:(NSInteger)newsID isRead:(BOOL)isRead;

/**
 *	@brief  获取新闻是否已读
 *
 *  @param  isRead         设置是否已读
 *	@return	新闻是否已读
 */
- (BOOL)getNewsBIsRead:(NSInteger)newsID;

/**
 *	@brief 删除新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteNewsB:(NSInteger)newsID;

/**
 *	@brief 删除所有轮播新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteNewsBBroadcastOfcategoryID:(NSInteger)categoryID;

/**
 *	@brief 添加轮播新闻
 *
 *	@return	操作是否成功
 */

- (BOOL)addBroadcast:(NSInteger)msgid
          categoryid:(NSInteger)categoryID
               title:(NSString *)title
         carouselurl:(NSString *)carouselurl
            orderNum:(NSInteger)orderNum
             linkurl:(NSString *)linkurl;
/**
 *	@brief 删除指定分类的新闻
 *
 *	@return	操作是否成功
 */
- (BOOL)deleteAllNewsBOfCategory:(NSInteger)categoryID;

/**
 *	@brief 判断新闻是否已存在
 *
 *	@return	新闻是否存在
 */
- (BOOL)newsBExist:(NSInteger)newsID;

/**
 *	@brief 获取轮播新闻
 *
 *	@return	轮播新闻
 */
- (NSArray *)getBroadcastNewsB:(NSInteger)categoryID;

/**
 *	@brief 获取新闻
 *
 *  @param 	pageNum             页数
 *	@param 	pageCount           每页新闻条数
 *	@param 	categoryID          分类ID
 *	@return	新闻列表
 */
- (NSArray *)newsBAtPage:(NSInteger)pageNum pageCount:(NSInteger)pageCount category:(NSInteger)categoryID;

/**
 *	@brief 返回分类下的新闻条数
 *
 *	@return	新闻条数
 */
- (NSUInteger)newsBCountOfCategory:(NSInteger)categoryID;

/**
 *	@brief 返回分类下的最新的新闻的ID
 *
 *	@return	分类下的最新的新闻的ID
 */
- (NSUInteger) getMaxNewsBIdOfCategory:(NSInteger)categoryID;

- (NSUInteger) getMinNewsBIdOfCategory:(NSInteger)categoryID;

@end
