//
//  ZSTDao+NewsB.m
//  NewsB
//
//  Created by xuhuijun on 13-7-18.
//  Copyright (c) 2013年 xuhuijun. All rights reserved.
//

#import "ZSTDao+NewsB.h"
#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUtil.h"
#import "ZSTGlobal+NewsB.h"



#define CREATE_TABLE_CMD(moduleType) [NSString stringWithFormat:@"CREATE TABLE IF NOT EXISTS [NewsBList_%@] (\
ID INTEGER PRIMARY KEY AUTOINCREMENT, \
msgid INTEGER DEFAULT 0 , \
categoryid INTEGER DEFAULT 0, \
title VARCHAR DEFAULT '',\
source VARCHAR DEFAULT '', \
iconurl VARCHAR DEFAULT '', \
addtime VARCHAR DEFAULT '', \
ordernum INTEGER  DEFAULT 0, \
linkurl VARCHAR DEFAULT '', \
isread INTEGER  DEFAULT 0 ,\
UNIQUE(msgid, categoryid)\
);\
CREATE TABLE IF NOT EXISTS [NewsBcarouseles_%@] (\
ID INTEGER PRIMARY KEY AUTOINCREMENT, \
msgid INTEGER DEFAULT 0 UNIQUE, \
categoryid INTEGER DEFAULT 0, \
title VARCHAR DEFAULT '', \
carouselurl VARCHAR DEFAULT '', \
ordernum INTEGER  DEFAULT 0 ,\
linkurl VARCHAR DEFAULT ''\
);\
CREATE TABLE IF NOT EXISTS [NewsBCategory_%@] (\
ID INTEGER PRIMARY KEY AUTOINCREMENT, \
categoryid INTEGER DEFAULT 0 UNIQUE, \
categoryname VARCHAR DEFAULT '', \
linkurl VARCHAR DEFAULT '', \
ordernum INTEGER  DEFAULT 0 \
);", @(moduleType), @(moduleType), @(moduleType)]

TK_FIX_CATEGORY_BUG(NewsB)

@implementation ZSTDao (NewsB)

- (void)createTableIfNotExistForNewsBModule
{
    [ZSTSqlManager executeSqlWithSqlStrings:CREATE_TABLE_CMD(self.moduleType)];
    
    [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"IF NOT EXISTS (SELECT  * FROM syscolumns WHERE   id = OBJECT_ID('NewsBList_%@') AND name = %@) alter table NewsBList_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),@"linkurl",@(self.moduleType),@"linkurl"]];
    
    [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"IF NOT EXISTS (SELECT  * FROM syscolumns WHERE   id = OBJECT_ID('NewsBcarouseles_%@') AND name = %@) alter table NewsBcarouseles_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),@"linkurl",@(self.moduleType),@"linkurl"]];
    
    //    [self addcolumnTableWithName:@"linkurl"];
}

//- (BOOL) addcolumnTableWithName:(NSString *)name
//{
//    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"IF NOT EXISTS (SELECT  * FROM syscolumns WHERE   id = OBJECT_ID('NewsBCategory_%@') AND name = %@) alter table NewsBCategory_%@ add column %@ VARCHAR  DEFAULT ''",@(self.moduleType),name,@(self.moduleType),name]];
//    return success;
//}

- (BOOL)addCategory:(NSInteger)categoryID
       categoryName:(NSString *)categoryName
           orderNum:(NSInteger)orderNum
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO NewsBCategory_%@ (categoryid, categoryname, ordernum) VALUES (?,?,?)", @(self.moduleType)],
                    @(categoryID),
                    [TKUtil wrapNilObject:categoryName],
                    @(orderNum)];
    return success;
    
}

- (BOOL)deleteAllNewsBCategories
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM NewsBCategory_%@", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL]) {
        
        return NO;
    }
    return YES;
}

- (NSArray *)getNewsBCategories
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, categoryid, categoryname, ordernum FROM NewsBCategory_%@ ORDER BY ordernum desc", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql];
    return results;
}

- (BOOL)addNews:(NSInteger)msgID
     categoryID:(NSInteger)categoryID
          title:(NSString *)title
         source:(NSString *)source
        iconurl:(NSString *)iconurl
       orderNum:(NSInteger)orderNum
        addTime:(NSString *)addTime
        linkUrl:(NSString *)linkurl
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO NewsBList_%@ (msgid, categoryid, title, source, iconurl, ordernum, addtime, linkurl) VALUES (?,?,?,?,?,?,?,?)", @(self.moduleType)],
                    [NSNumber numberWithInteger:msgID],
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:title],
                    [TKUtil wrapNilObject:source],
                    [TKUtil wrapNilObject:iconurl],
                    [NSNumber numberWithInteger:orderNum],
                    [TKUtil wrapNilObject:addTime],
                    [TKUtil wrapNilObject:linkurl]
                    ];
    return success;
}

- (BOOL)setNewsB:(NSInteger)newsID isRead:(BOOL)isRead
{
    NSString *updateSQL = [NSString stringWithFormat:@"UPDATE NewsBList_%@ set isread = ?  WHERE msgid = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:updateSQL, [NSNumber numberWithInt:isRead? 1 : 0], [NSNumber numberWithDouble:newsID]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)getNewsBIsRead:(NSInteger)newsID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT isread FROM NewsBList_%@ where msgid = ?", @(self.moduleType)], [NSNumber numberWithDouble:newsID]];
    
}

- (BOOL)deleteNewsB:(NSInteger)newsID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM NewsBList_%@ WHERE msgid = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, [NSNumber numberWithDouble:newsID]]) {
        
        return NO;
    }
    return YES;
}

- (BOOL)addBroadcast:(NSInteger)msgid
          categoryid:(NSInteger)categoryID
               title:(NSString *)title
         carouselurl:(NSString *)carouselurl
            orderNum:(NSInteger)orderNum
             linkurl:(NSString *)linkurl
{
    BOOL success = [ZSTSqlManager executeUpdate:[NSString stringWithFormat:@"INSERT OR REPLACE INTO NewsBcarouseles_%@ (msgid,categoryid, title, carouselurl, orderNum, linkurl) VALUES (?,?,?,?,?,?)", @(self.moduleType)],
                    [NSNumber numberWithInteger:msgid],
                    [NSNumber numberWithInteger:categoryID],
                    [TKUtil wrapNilObject:title],
                    [TKUtil wrapNilObject:carouselurl],
                    [NSNumber numberWithInteger:orderNum],
                    [TKUtil wrapNilObject:linkurl]
                    ];
    return success;
}

- (BOOL)deleteNewsBBroadcastOfcategoryID:(NSInteger)categoryID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM NewsBcarouseles_%@ where categoryid = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, @(categoryID)]) {
        return NO;
    }
    return YES;
}

- (BOOL)deleteAllNewsBOfCategory:(NSInteger)categoryID
{
    NSString *deleteSQL = [NSString stringWithFormat:@"DELETE FROM NewsBList_%@ where categoryid = ?", @(self.moduleType)];
    if (![ZSTSqlManager executeUpdate:deleteSQL, @(categoryID)]) {
        return NO;
    }
    return YES;
}

- (BOOL)newsBExist:(NSInteger)newsID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) as cnt FROM NewsBList_%@ where msgid = ?", @(self.moduleType)], @(newsID)];
    
}

- (NSArray *)getBroadcastNewsB:(NSInteger)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, msgid, title, carouselurl, ordernum, linkurl FROM NewsBcarouseles_%@ where categoryid = ?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql , @(categoryID)];
    return results;
}

- (NSArray *)newsBAtPage:(NSInteger)pageNum pageCount:(NSInteger)pageCount category:(NSInteger)categoryID
{
    NSString *querySql = [NSString stringWithFormat:@"SELECT ID, msgid, categoryid, title, source, iconurl, ordernum, addtime, linkurl, isread FROM NewsBList_%@ where categoryid = ? ORDER BY AddTime desc limit ?,?", @(self.moduleType)];
    NSArray *results = [ZSTSqlManager executeQuery:querySql arguments:[NSArray arrayWithObjects:
                                                                       @(categoryID),
                                                                       @((pageNum-1)*pageCount),
                                                                       @(pageCount),
                                                                       nil]];
    return results;
}

- (NSUInteger)newsBCountOfCategory:(NSInteger)categoryID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"SELECT COUNT(ID) FROM NewsBList_%@ where categoryid = ? ", @(self.moduleType)], @(categoryID)];
}

- (NSUInteger) getMaxNewsBIdOfCategory:(NSInteger)categoryID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select msgid from NewsBList_%@ where categoryid = ? order by AddTime desc limit 1", @(self.moduleType)], @(categoryID)];
}

- (NSUInteger) getMinNewsBIdOfCategory:(NSInteger)categoryID
{
    return [ZSTSqlManager intForQuery:[NSString stringWithFormat:@"select msgid from NewsBList_%@ where categoryid = ? order by AddTime asc limit 1", @(self.moduleType)], @(categoryID)];
}


@end
