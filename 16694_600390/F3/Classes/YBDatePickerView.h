//
//  YBDatePickerView.h
//  changeViewController
//
//  Created by EapinZhang on 15/4/3.
//  Copyright (c) 2015年 wallace. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol YBDatePickerViewDelegate <NSObject>

- (void)cancelPickerView;
- (void)surePickerView:(NSString *)selectedYear And:(NSString *)selectedMonth;

@end

@interface YBDatePickerView : UIView <UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak,nonatomic) id<YBDatePickerViewDelegate> myDelegate;
@property (copy,nonatomic) NSString *nowYear;
@property (copy,nonatomic) NSString *nowMonth;

- (void)createUIWithFrame:(CGRect)rect;


@end
