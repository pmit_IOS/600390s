//
//  ZSTNewPassWordViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-23.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewPassWordViewController.h"
#import "ZSTAgreementViewController.h"
//#import "ZSTAccountViewController.h"
#import "ZSTAccountManagementVC.h"

#define  kZSTRegister_Register          1        //注册
#define  kZSTRegister_Forget            2        //找回密码
#define  kZSTRegister_ChangePassword    4        //修改密码

@interface ZSTNewPassWordViewController ()
{
    BOOL isAgree;
}

@end

@implementation ZSTNewPassWordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.naviImgView.image = [UIImage imageNamed:@"framework_top_bg.png"];
    isAgree = NO;
    [self.choicebtn setImage:ZSTModuleImage(@"module_orderinfo_pay_on.png") forState:UIControlStateNormal];
    
    NSString *title = @"返回";
    CGSize size = [title sizeWithFont:[UIFont systemFontOfSize:14]
                    constrainedToSize:CGSizeMake(121, 28)
                        lineBreakMode:NSLineBreakByTruncatingTail];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.frame = CGRectMake(12, 7, size.width + 20, 30);
    btn.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
    btn.titleLabel.shadowOffset = CGSizeMake(0, -0.5f);
    [btn setTitleShadowColor:[UIColor colorWithWhite:0.1 alpha:1] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_normal.png"] stretchableImageWithLeftCapWidth:3   topCapHeight:9] forState:UIControlStateNormal];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_pressed.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateHighlighted];
    [btn setBackgroundImage:[[UIImage imageNamed:@"TKCommonLib.bundle/NaivagationBar/btn_navigationBaritemBack_disable.png"] stretchableImageWithLeftCapWidth:30 topCapHeight:9] forState:UIControlStateDisabled];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitle:title forState:UIControlStateNormal];
    [btn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    [self.naviView addSubview:btn];
    
    UIImage *imageN = ZSTModuleImage(@"module_login_btn_green_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_login_btn_green_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.sumbitBtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.sumbitBtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
//    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
//    self.agreementLabel.text = [NSString stringWithFormat:@"《%@用户协议》",appDisplayName];
}

- (void)backAction:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.passwordField becomeFirstResponder];
    
    switch (self.type) {
        case kZSTRegister_Register:
            self.titleLabel.text = @"注册";
            break;
        case kZSTRegister_Forget:
        
            self.titleLabel.text = @"找回密码";
            break;
            
        case kZSTRegister_ChangePassword:
           
            self.titleLabel.text = @"修改密码";
            break;
        default:
            break;
    }
}

- (IBAction)sumbitAction:(id)sender
{
    [self.passwordField resignFirstResponder];
    [self.passwordagainField resignFirstResponder];
    
    NSString *passwordStr = [self.passwordField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *passwordagainStr = [self.passwordagainField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    
    if (passwordStr.length == 0 || passwordagainStr.length == 0) {
        [TKUIUtil alertInWindow:@"请输入密码" withImage:nil];
        return;
    }
    
    if (![passwordagainStr isEqualToString:passwordStr]) {
        
         [TKUIUtil alertInWindow:@"输入的两次密码不相同" withImage:nil];
        return;
    }
    
    [TKUIUtil showHUD:self.view];
    
    if (self.type == kZSTRegister_Register) {
        [self.engine registerWithMsisdn:self.phoneNum password:passwordStr verificationCode:self.verificationCode];
    } else if (self.type == kZSTRegister_Forget || self.type == kZSTRegister_ChangePassword) {
        
        [self.engine updatePasswordWithMsisdn:self.phoneNum verificationCode:self.verificationCode password:passwordStr OperType:2];
    }
}

- (void)registerDidSucceed:(NSDictionary *)response
{
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
    
    [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
    [ZSTF3Preferences shared].loginMsisdn = self.phoneNum;
    
    [self performSelector:@selector(showAlert) withObject:nil afterDelay:2.0f];
}

- (void) showAlert
{
    if (self.type == kZSTRegister_ChangePassword) {
        for (UIViewController *viewcontroller in self.navigationController.viewControllers) {
            
            if ([viewcontroller isKindOfClass:[ZSTAccountManagementVC class]]) {
                
                [self.navigationController popToViewController:viewcontroller animated:YES];
            }
        }
    } else {
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^(void) {
            
        }];
    }
}

- (void)registerDidFailed:(NSString*)message
{
    [TKUIUtil hiddenHUD];
    
   [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)reloadData
{
    [self sumbitAction:nil];
}

- (IBAction)showAgreeAction:(id)sender
{
    ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
    [navController release];
    [controller release];
}

- (IBAction)agreeAction:(id)sender
{
    if (isAgree) {
        
        self.sumbitBtn.enabled = YES;
        [self.choicebtn setImage:ZSTModuleImage(@"module_orderinfo_pay_on.png") forState:UIControlStateNormal];
        isAgree = NO;
    } else {
        
        self.sumbitBtn.enabled = NO;
        [self.choicebtn setImage:ZSTModuleImage(@"module_orderinfo_pay_off.png") forState:UIControlStateNormal];
        isAgree = YES;
    }
}

- (void)updatePasswordDidSucceed:(NSDictionary *)response
{
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
    
//    [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
//    [ZSTF3Preferences shared].loginMsisdn = self.phoneNum;
    
    [self performSelector:@selector(showAlert) withObject:nil afterDelay:2.0f];
}

- (void)updatePasswordDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    self.titleLabel = nil;
    self.passwordField = nil;
    self.passwordagainField = nil;
    self.naviView = nil;
    self.naviImgView = nil;
    self.sumbitBtn = nil;
    [super dealloc];
}

@end
