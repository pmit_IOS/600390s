//
//  ZSTNewShareView.m
//  F3
//
//  Created by pmit on 15/8/27.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewShareView.h"
#import <TencentOpenAPI/QQApiInterface.h>
#import <SDWebImage/SDWebImageManager.h>

@implementation ZSTNewShareView


- (void)createShareUI
{
    if (!self.snsBtn)
    {
        if (self.isHasWX)
        {
            UIView *wxView = [self getShareItemView:self.wxSharePoint Title:@"微信好友" Frame:CGRectMake(0, 0, WIDTH / 4, 100) ShareImage:@"share_wechat.png" Tag:WEIXINTAG];
            [self addSubview:wxView];
            
            UIView *wxFriendView = [self getShareItemView:self.wxFriendSharePoint Title:@"朋友圈" Frame:CGRectMake(WIDTH / 4, 0, WIDTH / 4, 100) ShareImage:@"share_friend.png" Tag:WEIXINFRIENDTAG];
            [self addSubview:wxFriendView];
            
            if (self.isHasWeiBo)
            {
                UIView *weiboView = [self getShareItemView:self.weiboSharePoint Title:@"新浪微博" Frame:CGRectMake(WIDTH / 4 * 2, 0, WIDTH / 4, 100) ShareImage:@"share_weibo.png" Tag:WEIBOTAG];
                [self addSubview:weiboView];
                
                if (self.isHasQQ)
                {
                    UIView *qqView = [self getShareItemView:self.qqSharePoint Title:@"QQ好友" Frame:CGRectMake(WIDTH / 4 * 3, 0, WIDTH / 4, 100) ShareImage:@"share_qq.png" Tag:QQTAG];
                    [self addSubview:qqView];
                    
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(0, 100, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(WIDTH / 4 ,100, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
                else
                {
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(WIDTH / 4 * 3, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(0, 100, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
                
                
            }
            else
            {
                if (self.isHasQQ)
                {
                    UIView *qqView = [self getShareItemView:self.qqSharePoint Title:@"QQ好友" Frame:CGRectMake(WIDTH / 4 * 2, 0, WIDTH / 4, 100) ShareImage:@"share_qq.png" Tag:QQTAG];
                    [self addSubview:qqView];
                    
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(WIDTH / 4 * 3, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(0, 100, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
                else
                {
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(WIDTH / 4 * 2, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(WIDTH / 4 * 3, 0, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
            }
            
        }
        else
        {
            if (self.isHasWeiBo)
            {
                UIView *weiboView = [self getShareItemView:self.weiboSharePoint Title:@"新浪微博" Frame:CGRectMake(0, 0, WIDTH / 4, 100) ShareImage:@"share_weibo.png" Tag:WEIBOTAG];
                [self addSubview:weiboView];
                
                if (self.isHasQQ)
                {
                    UIView *qqView = [self getShareItemView:self.qqSharePoint Title:@"QQ好友" Frame:CGRectMake(WIDTH / 4, 0, WIDTH / 4, 100) ShareImage:@"share_qq.png" Tag:QQTAG];
                    [self addSubview:qqView];
                    
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(WIDTH / 4 * 2, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(WIDTH / 4 * 3, 0, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
                else
                {
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(WIDTH / 4, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(WIDTH / 4 * 2, 0, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
            }
            else
            {
                if (self.isHasQQ)
                {
                    UIView *qqView = [self getShareItemView:self.qqSharePoint Title:@"QQ好友" Frame:CGRectMake(0, 0, WIDTH / 4, 100) ShareImage:@"share_qq.png" Tag:QQTAG];
                    [self addSubview:qqView];
                    
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(WIDTH / 4, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(WIDTH / 4 * 2, 0, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
                else
                {
                    UIView *smsView = [self getShareItemView:self.smsSharePoint Title:@"短信好友" Frame:CGRectMake(0, 0, WIDTH / 4, 100)  ShareImage:@"share_sms.png" Tag:SMSTAG];
                    [self addSubview:smsView];
                    
                    UIView *moreView = [self getShareItemView:0 Title:@"更多" Frame:CGRectMake(WIDTH / 4, 0, WIDTH / 4, 100) ShareImage:@"share_more.png" Tag:MORETAG];
                    [self addSubview:moreView];
                }
            }
        }
        
        
        UISwipeGestureRecognizer *swiper = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(shareViewDismiss:)];
        swiper.direction = UISwipeGestureRecognizerDirectionDown;
        [self addGestureRecognizer:swiper];
    }
}

- (UIView *)getShareItemView:(NSInteger)itemSharePoint Title:(NSString *)itemShareTitle Frame:(CGRect)itemShare ShareImage:(NSString *)itemShareImageName Tag:(NSInteger)itemTag
{
    UIView *itemView = [[UIView alloc] initWithFrame:itemShare];
    itemView.tag = itemTag * 100000;
    UIImageView *itemIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 10, WIDTH / 4 - 10, 45)];
    [itemIV setImage:[UIImage imageNamed:itemShareImageName]];
    itemIV.contentMode = UIViewContentModeScaleAspectFit;
    [itemView addSubview:itemIV];
    
    UILabel *itemLB = [[UILabel alloc] initWithFrame:CGRectMake(5, 60, WIDTH / 4 - 10, 20)];
    itemLB.text = itemShareTitle;
    itemLB.textAlignment = NSTextAlignmentCenter;
    itemLB.font = [UIFont systemFontOfSize:12.0f];
    [itemView addSubview:itemLB];
    
    UIButton *spBGBtn  = [UIButton buttonWithType:UIButtonTypeCustom];
    spBGBtn.tag = itemTag * 10000;
    spBGBtn.frame = CGRectMake(10, 80, WIDTH / 4 - 20, 15);
    [spBGBtn setBackgroundImage:[UIImage imageNamed:@"share_pointbg.png"] forState:UIControlStateNormal];
    spBGBtn.titleLabel.font = [UIFont systemFontOfSize:12.0];
    [spBGBtn setTitle:[NSString stringWithFormat:@"+%@分",@(itemSharePoint)] forState:UIControlStateNormal];
    [itemView addSubview:spBGBtn];
    
    
    UIButton *itemBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    itemBtn.tag = itemTag;
    itemBtn.frame = CGRectMake(0, 0, itemView.bounds.size.width, itemView.bounds.size.height);
    [itemBtn addTarget:self action:@selector(shareAction:) forControlEvents:UIControlEventTouchUpInside];
    [itemView addSubview:itemBtn];
    
    if ([itemShareTitle isEqualToString:@"短信好友"] || [itemShareTitle isEqualToString:@"更多"])
    {
        if ([itemShareTitle isEqualToString:@"短信好友"])
        {
            self.snsBtn = itemBtn;
        }
        
        spBGBtn.hidden = YES;
    }
    
    return itemView;
}

- (void)shareAction:(UIButton *)sender
{
    
    NSData *data;
    __block UIImage *iconImg;
    
    if (!self.imgUrlString || [self.imgUrlString isEqualToString:@""])
    {
        iconImg = [UIImage imageNamed:@"icon.png"];
        data = UIImageJPEGRepresentation(iconImg, 0.5);
        iconImg = [UIImage imageWithData:data];
    }
    else
    {
//        iconImg = [[SDWebImageManager sharedManager] imageWithURL:[NSURL URLWithString:self.imgUrlString]];
        [[SDWebImageManager sharedManager] downloadImageWithURL:[NSURL URLWithString:self.imgUrlString] options:SDWebImageRetryFailed progress:nil completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, BOOL finished, NSURL *imageURL) {
            
            iconImg = [UIImage imageWithData:data];
            
        }];
//        data = UIImageJPEGRepresentation(iconImg, 0.5);
//        iconImg = [UIImage imageWithData:data];
        
        
    }
    
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    
    if (sender.tag == WEIXINTAG)
    {
        if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
        {
            [ZSTWXShareType shareInstance].isWXShare = YES;
            
            WXMediaMessage *message = [WXMediaMessage message];
            message.title = appDisplayName;
            message.description = self.shareString;
            [message setThumbImage:iconImg];
            
            WXWebpageObject *ext = [WXWebpageObject object];
            if (!self.shareUrlString || [self.shareUrlString isEqualToString:@""])
            {
                ext.webpageUrl = [NSString stringWithFormat:@"ci.pmit.cn/d/%@",[ZSTF3Preferences shared].ECECCID];
            }
            else
            {
                ext.webpageUrl = self.shareUrlString;
            }
            
            message.mediaObject = ext;
            message.mediaTagName = @"WECHAT_TAG_JUMP_SHOWRANK";
            
            SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
            req.bText = NO;
            req.message = message;
            req.scene = WXSceneSession;
            NSLog(@"%@",@([WXApi sendReq:req]));
        }
    }
    else if (sender.tag == WEIXINFRIENDTAG)
    {
        if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
        {

            WXMediaMessage *message = [WXMediaMessage message];
            message.title = self.shareString;
//            message.description = self.shareString;
            [message setThumbImage:iconImg];
            
            WXWebpageObject *ext = [WXWebpageObject object];
            if (!self.shareUrlString || [self.shareUrlString isEqualToString:@""])
            {
                ext.webpageUrl = [NSString stringWithFormat:@"ci.pmit.cn/d/%@",[ZSTF3Preferences shared].ECECCID];
            }
            else
            {
                ext.webpageUrl = self.shareUrlString;
            }

            
            
            message.mediaObject = ext;
            message.mediaTagName = @"WECHAT_TAG_JUMP_SHOWRANK";
            
            SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
            req.bText = NO;
            req.message = message;
            req.scene = WXSceneTimeline;
            [WXApi sendReq:req];
            
            
            [ZSTWXShareType shareInstance].isWXShare = NO;
        }
    }
    else if (sender.tag == WEIBOTAG)
    {
        ZSTF3ClientAppDelegate *myDelegate = (ZSTF3ClientAppDelegate*)[[UIApplication sharedApplication] delegate];
        WBAuthorizeRequest *authRequest = [WBAuthorizeRequest request];
        
        authRequest.redirectURI = [ZSTF3Preferences shared].weiboRedirect;
        authRequest.scope = @"all";
        WBMessageObject *message = [WBMessageObject message];
        message.text = self.shareString;
        
        WBImageObject *image = [WBImageObject object];
        image.imageData = data;
        message.imageObject = image;
        
        WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:message authInfo:authRequest access_token:myDelegate.wbtoken];
        request.userInfo = @{@"ShareMessageFrom": @"ZSTNewsBContentViewController",
                             @"Other_Info_1": [NSNumber numberWithInt:123],
                             @"Other_Info_2": @[@"obj1", @"obj2"],
                             @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
        [WeiboSDK sendRequest:request];
    }
    else if (sender.tag == QQTAG)
    {
//        QQApiTextObject *txtObj = [QQApiTextObject objectWithText:self.shareString];
//        SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:txtObj];
//        QQApiSendResultCode sent = [QQApiInterface sendReq:req];
//        [self handleSendResult:sent];
        
        QQApiNewsObject* img = [QQApiNewsObject objectWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"ci.pmit.cn/d/%@",[ZSTF3Preferences shared].ECECCID]] title:appDisplayName description:self.shareString previewImageData:data];
        SendMessageToQQReq* req = [SendMessageToQQReq reqWithContent:img];
        
        QQApiSendResultCode sent = [QQApiInterface sendReq:req];
        [self handleSendResult:sent];
        
    }
    else if (sender.tag == SMSTAG)
    {
        if ([self.shareDelegate respondsToSelector:@selector(showSNSShare)])
        {
            [self.shareDelegate showSNSShare];
        }
    }
    else if (sender.tag == MORETAG)
    {
        if ([self.shareDelegate respondsToSelector:@selector(showDefaultShare)])
        {
            [self.shareDelegate showDefaultShare];
        }
    }
}

- (void)handleSendResult:(QQApiSendResultCode)sendResult
{
    switch (sendResult)
    {
        case EQQAPIAPPNOTREGISTED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"App未注册" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIMESSAGECONTENTINVALID:
        case EQQAPIMESSAGECONTENTNULL:
        case EQQAPIMESSAGETYPEINVALID:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"发送参数错误" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIQQNOTINSTALLED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"未安装手Q" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            
            break;
        }
        case EQQAPIQQNOTSUPPORTAPI:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"API接口不支持" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPISENDFAILD:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"发送失败" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            
            break;
        }
        default:
        {
            break;
        }
    }
}

- (void)shareViewDismiss:(UISwipeGestureRecognizer *)swiper
{
    if ([self.shareDelegate respondsToSelector:@selector(dismissShareView)])
    {
        [self.shareDelegate dismissShareView];
    }
}

- (void)checkIsHasShare
{
    if (self.isTodayWX)
    {
        UIView *itemView = [self viewWithTag:(WEIXINTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(WEIXINTAG * 10000)];
        sgBtn.hidden = YES;
    }
    else
    {
        UIView *itemView = [self viewWithTag:(WEIXINTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(WEIXINTAG * 10000)];
        sgBtn.hidden = NO;
    }
    
    if (self.isTodayWXFriedn)
    {
        UIView *itemView = [self viewWithTag:(WEIXINFRIENDTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(WEIXINFRIENDTAG * 10000)];
        sgBtn.hidden = YES;
    }
    else
    {
        UIView *itemView = [self viewWithTag:(WEIXINFRIENDTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(WEIXINFRIENDTAG * 10000)];
        sgBtn.hidden = NO;
    }
    
    if (self.isTodayWeiBo)
    {
        UIView *itemView = [self viewWithTag:(WEIBOTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(WEIBOTAG * 10000)];
        sgBtn.hidden = YES;
    }
    else
    {
        UIView *itemView = [self viewWithTag:(WEIBOTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(WEIBOTAG * 10000)];
        sgBtn.hidden = NO;
    }
    
    if (self.isTodayQQ)
    {
        UIView *itemView = [self viewWithTag:(QQTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(QQTAG * 10000)];
        sgBtn.hidden = YES;
    }
    else
    {
        UIView *itemView = [self viewWithTag:(QQTAG * 100000)];
        UIButton *sgBtn = (UIButton *)[itemView viewWithTag:(QQTAG * 10000)];
        sgBtn.hidden = NO;
    }
}


@end
