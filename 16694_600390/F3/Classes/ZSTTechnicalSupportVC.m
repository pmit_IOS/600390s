//
//  ZSTTechnicalSupportVC.m
//  F3
//
//  Created by P&M on 15/9/7.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTTechnicalSupportVC.h"
#import "ZSTUtils.h"

@interface ZSTTechnicalSupportVC () <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) UIImageView *markImgView;

@end

@implementation ZSTTechnicalSupportVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed: @"framework_top_bg.png"];
    //self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"技术支持", nil)];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    _webView.backgroundColor = [UIColor clearColor];
    [_webView setUserInteractionEnabled:YES];  //是否支持交互
    [_webView setDelegate:self];
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [self.view addSubview:_webView];
    
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",@"http://www.pmit.cn"]];//创建URL
    
    [_webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)backAction
{
    _markImgView.image = ZSTModuleImage(@"module_snsa_orderinfo_pay_on.png");
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)webViewDidStartLoad:(UIWebView *)webView //网页加载时调用
{
}

- (void)webViewDidFinishLoad:(UIWebView *)webView //网页完成加载时调用
{
    if (self.source) {
        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_webView.frame), 320, 60)];
        bottomView.backgroundColor = RGBCOLOR(241, 241, 241);
        [self.view addSubview:bottomView];
        
        _markImgView = [[UIImageView alloc] initWithFrame:CGRectMake(80, 20, 15, 15)];
        _markImgView.image = ZSTModuleImage(@"module_snsa_orderinfo_pay_off.png");
        [bottomView addSubview:_markImgView];
        
        UITapGestureRecognizer *singleRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backAction)];
        singleRecognizer.numberOfTapsRequired = 1;
        [bottomView addGestureRecognizer:singleRecognizer];
    }
    else {
        self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(dismissModalViewController)];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
