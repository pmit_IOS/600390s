//
//  ZSTRegisterPassViewController.m
//  F3
//
//  Created by pmit on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTRegisterPassViewController.h"
#import "PMRepairButton.h"
#import "ZSTF3Engine.h"
#import "ZSTAccountManagementVC.h"
#import "ZSTMineCenterViewController.h"

#define trueNum(num) (num + 64)

@interface ZSTRegisterPassViewController () <ZSTF3EngineDelegate>

@property (strong,nonatomic) UIScrollView *rScrollView;
@property (strong,nonatomic) ZSTF3Engine *engine;
@property (strong,nonatomic) UITextField *firstPassTF;
@property (strong,nonatomic) UITextField *secondPassTF;

@end


@implementation ZSTRegisterPassViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"设置密码", nil)];
    //self.navigationItem.leftBarButtonItem =  [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.view.backgroundColor = [UIColor whiteColor];
    self.rScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.rScrollView.showsHorizontalScrollIndicator = NO;
    self.rScrollView.showsVerticalScrollIndicator = NO;
    self.rScrollView.contentSize = CGSizeMake(0, 240);
    self.rScrollView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapResignFirst:)];
    [self.rScrollView addGestureRecognizer:tap];
    
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self.view addSubview:self.rScrollView];
    
    [self buildStatusView];
    [self buildCodeView];
    [self buildBottomView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildStatusView
{
    UIImageView *statusIV = [[UIImageView alloc] initWithFrame:CGRectMake(25, 35, WIDTH - 50, 10)];
    statusIV.contentMode = UIViewContentModeScaleAspectFit;
    statusIV.image = [UIImage imageNamed:@"statusTwo.png"];
    [self.rScrollView addSubview:statusIV];
    
    UIImageView *numIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fullBorder.png"]];
    numIV.center = CGPointMake(WIDTH - 25, 22);
    numIV.bounds = CGRectMake(0, 0, 35, 25);
    [self.rScrollView addSubview:numIV];
    
    PMRepairButton *numBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
    numBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [numBtn setTitle:@"100%" forState:UIControlStateNormal];
    [numBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [numIV addSubview:numBtn];
}

- (void)buildCodeView
{
    UIView *passwordView = [[UIView alloc] initWithFrame:CGRectMake(15, 80, WIDTH - 50, 91)];
    passwordView.backgroundColor = [UIColor whiteColor];
    [self.rScrollView addSubview:passwordView];
    
    UIImageView *lockIconOne = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    lockIconOne.image = [UIImage imageNamed:@"rLock.png"];
    lockIconOne.contentMode = UIViewContentModeScaleAspectFit;
    [passwordView addSubview:lockIconOne];
    
    self.firstPassTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 5, passwordView.bounds.size.width - 40, 30)];
    self.firstPassTF.placeholder = @"请输入密码";
    self.firstPassTF.secureTextEntry = YES;
    [self.firstPassTF setValue:[UIFont boldSystemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    [passwordView addSubview:self.firstPassTF];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(234, 234, 234, 1).CGColor;
    line.frame = CGRectMake(10, 40, passwordView.size.width, 0.5);
    [passwordView.layer addSublayer:line];
    
    UIImageView *lockIconTwo = [[UIImageView alloc] initWithFrame:CGRectMake(10, 55.5, 20, 20)];
    lockIconTwo.image = [UIImage imageNamed:@"rLock.png"];
    lockIconTwo.contentMode = UIViewContentModeScaleAspectFit;
    [passwordView addSubview:lockIconTwo];
    
    self.secondPassTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 53.5, passwordView.bounds.size.width - 40, 30)];
    self.secondPassTF.placeholder = @"请输入密码";
    self.secondPassTF.secureTextEntry = YES;
    [self.secondPassTF setValue:[UIFont boldSystemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
    [passwordView addSubview:self.secondPassTF];
    
    CALayer *line2 = [CALayer layer];
    line2.backgroundColor = RGBA(234, 234, 234, 1).CGColor;
    line2.frame = CGRectMake(10, 90.5, passwordView.size.width, 0.5);
    [passwordView.layer addSublayer:line2];
}

- (void)buildBottomView
{
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(25, 200, WIDTH - 50, 40);
    UIImage *norImg = [self buttonImageFromColor:RGBA(244, 125, 54, 1)];
    UIImage *selImg = [self buttonImageFromColor:RGBA(193, 193, 193, 1)];
    [sureBtn setTitle:@"确  定" forState:UIControlStateNormal];
    [sureBtn setTitle:@"两次输入不一致" forState:UIControlStateSelected];
    [sureBtn setBackgroundImage:norImg forState:UIControlStateNormal];
    [sureBtn setBackgroundImage:selImg forState:UIControlStateSelected];
    [sureBtn addTarget:self action:@selector(goToRegister:) forControlEvents:UIControlEventTouchUpInside];
    [self.rScrollView addSubview:sureBtn];
    
}

- (void)tapResignFirst:(UITapGestureRecognizer *)tap
{
    [self.firstPassTF resignFirstResponder];
    [self.secondPassTF resignFirstResponder];
}

- (UIImage *)buttonImageFromColor:(UIColor *)color{
    
    CGRect rect = CGRectMake(0, 0, WIDTH  - 50, 40);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext(); return img;
}

- (void)goToRegister:(UIButton *)sender
{
    if ([self.firstPassTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入密码", nil) withImage:nil];
    }
    else if ([self.secondPassTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请在输入一次密码", nil) withImage:nil];
    }
    else if (![self.secondPassTF.text isEqualToString:self.firstPassTF.text])
    {
        sender.selected = YES;
    }
    else
    {
        sender.selected = NO;
        if (self.registerType == ZSTRegisterTypeSecondRegister)
        {
            [self.engine registerWithMsisdn:self.phoneString password:self.firstPassTF.text verificationCode:self.smsCodeString];
        }
        else
        {
            [self.engine updatePasswordWithMsisdn:self.phoneString verificationCode:self.smsCodeString password:self.firstPassTF.text OperType:2];
        }
    }
}

- (void)registerDidSucceed:(NSDictionary *)response
{
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
    
    [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
    [ZSTF3Preferences shared].loginMsisdn = self.phoneString;
    
    [self performSelector:@selector(showAlert) withObject:nil afterDelay:2.0f];
}

- (void)registerDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}


- (void) showAlert
{
    if (self.registerType == ZSTRegisterTypeSecondChangePhoe || self.registerType == ZSTRegisterTypeSecondChangePass) {
        for (UIViewController *viewcontroller in self.navigationController.viewControllers) {
            
            if ([viewcontroller isKindOfClass:[ZSTAccountManagementVC class]]) {
                
                [self.navigationController popToViewController:viewcontroller animated:YES];
            }
            else
            {
                if ([viewcontroller isKindOfClass:[ZSTMineCenterViewController class]])
                {
                    ((ZSTMineCenterViewController *)viewcontroller).isShareApp = YES;
                }
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    } else {
        
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
}

- (void)updatePasswordDidSucceed:(NSDictionary *)response
{
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:message withImage:nil];
    
    [self performSelector:@selector(showAlert) withObject:nil afterDelay:2.0f];

}

- (void)updatePasswordDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}



@end
