//
//  PMMembersPrivilegeViewController.m
//  F3
//
//  Created by P&M on 15/9/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersPrivilegeViewController.h"
#import "ZSTUtils.h"

@interface PMMembersPrivilegeViewController ()

@end

@implementation PMMembersPrivilegeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"会员特权"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:nil selector:@selector(popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    if ([self.discount isEqualToString:@"discount"]) {
        [self getMemberDiscountInfo];
    }
    else {
        [self getMemberExclusiveInfo];
    }
    
    [self createUI];
}

- (void)createUI
{
    // 标题
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WidthRate(180), 30)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"评价奖励";
    titleLabel.textColor = [UIColor blackColor];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.view addSubview:titleLabel];
    
    // 文本
    UILabel *contextLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WIDTH - WidthRate(72), 100)];
    contextLab.backgroundColor = [UIColor clearColor];
    contextLab.text = @"对于单价大于或等于10元以上商品，会员评价商品可获得金条，等级越高，评价获得的金条就越多。";
    contextLab.textColor = [UIColor grayColor];
    contextLab.numberOfLines = 0;
    contextLab.textAlignment = NSTextAlignmentRight;
    contextLab.font = [UIFont systemFontOfSize:13.0f];
    [self.view addSubview:contextLab];
}

#pragma mark - 获取会员折扣说明
- (void)getMemberDiscountInfo
{
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *cardSetId = @"F70B35CDCD4B7476E040810A90115241";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"cardSetId":cardSetId};
    
    [self.engine getMemberDiscountInfo:param];
}

// 获得会员折扣成功
- (void)getMemberDiscountInfoSucceed:(NSDictionary *)response
{
    NSLog(@"response === %@", response);
}

- (void)getMemberDiscountInfoFaild:(NSString *)response
{
    
}


#pragma mark - 获取专享模块说明
- (void)getMemberExclusiveInfo
{
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid};
    
    [self.engine getMemberExclusiveInfo:param];
}

// 获得专享模块成功
- (void)getMemberExclusiveInfoSucceed:(NSDictionary *)response
{
    NSLog(@"response === %@", response);
}

- (void)getMemberExclusiveInfoFaild:(NSString *)response
{
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
