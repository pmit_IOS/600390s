//
//  ZSTPersonalCenterCell.h
//  F3
//
//  Created by P&M on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRepairButton.h"

@interface ZSTPersonalCenterCell : UITableViewCell

@property (strong, nonatomic) UIImageView *cellIma;
@property (strong, nonatomic) UILabel *cellTitle;
@property (strong, nonatomic) UILabel *membersLab;
@property (strong, nonatomic) UILabel *integralLab;
//@property (strong, nonatomic) UIButton *signInBtn;
@property (strong,nonatomic)  PMRepairButton *signInBtn;
@property (strong, nonatomic) UILabel *signedInLab;
@property (strong, nonatomic) UILabel *accountLab;
@property (strong, nonatomic) UILabel *integralTip;

- (void)createPersonalCenterUI;

@end
