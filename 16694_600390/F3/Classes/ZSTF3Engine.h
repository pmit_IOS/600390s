//
//  ZSTF3Engine.h
//  F3Engine
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "ZSTLegacyResponse.h"
#import "ZSTLegicyCommunicator.h"
#import "SDWebFileManagerDelegate.h"
#import "SDWebFileDownloaderDelegate.h"
#import "ZSTF3Preferences.h"
#import "ZSTCommunicator.h"

@class MessageInfo;
@class ZSTECMobileClientParams;
@class ZSTDao;

@protocol ZSTF3EngineDelegate <NSObject>

@optional

- (void)requestDidFail:(ZSTLegacyResponse *)response;

- (void)registMobileClientDidResponse:(NSString *)loginMsisdn loginPassword:(NSString *)loginPassword;

- (void)checkClientVersionResponse:(NSString *)versionURL updateNote:(NSString *)updateNote updateVersion:(int)updateVersion;

- (void)getRegChecksumResponse;

- (void)submitAdviceResponse;

//个人中心
- (void)updateECClientDidSucceed:(NSDictionary *)response;
- (void)updateECClientDidFailed:(NSString *)response;

- (void)getECClientParamsDidSucceed:(NSDictionary *)response;
- (void)getECClientParamsDidFailed:(NSString *)response;

- (void)automaticLoginDidSucceed:(NSDictionary *)response;
- (void)automaticLoginDidFailed:(NSString *)response;

- (void)loginDidSucceed:(NSDictionary *)response;
- (void)loginDidFailed:(NSString *)response;

- (void)registerDidSucceed:(NSDictionary *)response;
- (void)registerDidFailed:(NSString *)response;

- (void)getVerificationCodeDidSucceed:(NSDictionary *)response;
- (void)getVerificationCodeDidFailed:(NSString *)response;

- (void)checkVerificationCodeDidSucceed:(NSDictionary *)response;
- (void)checkVerificationCodeDidFailed:(NSString *)response;

- (void)updatePasswordDidSucceed:(NSDictionary *)response;
- (void)updatePasswordDidFailed:(NSString *)response;

- (void)updateMSisdnDidSucceed:(NSDictionary *)response;
- (void)updateMSisdnDidFailed:(NSString *)response;

- (void)getUserInfoDidSucceed:(NSDictionary *)response;
- (void)getUserInfoDidFailed:(NSString *)response;

- (void)getPointDidSucceed:(NSDictionary *)response;
- (void)getPointDidFailed:(NSString *)response;

- (void)updatePointDidSucceed:(NSDictionary *)response;
- (void)updatePointDidFailed:(NSString *)response;

- (void)updateUserInfoDidSucceed:(NSDictionary *)response;
- (void)updateUserInfoDidFailed:(NSString *)response;

- (void)uploadFileDidSucceed:(NSDictionary *)response;
- (void)uploadFileDidFailed:(NSString *)respone;

- (void)checkPasswordDidSucceed:(NSDictionary *)response;
- (void)checkPasswordDidFailed:(NSString *)response;

- (void)logoutDidSucceed:(NSDictionary *)response;
- (void)logoutDidFailed:(NSString *)response;

- (void)loginAuthorizeDidSucceed:(NSDictionary *)response;
- (void)loginAuthorizeDidFailed:(NSString *)response;

- (void)thirdLoginDidSucceed:(NSDictionary *)response;
- (void)thirdLoginDidFailed:(NSString *)response;

- (void)checkPhoneNumIsExistSuccess:(NSDictionary *)response;

- (void)bindingMobileDidSucceed:(NSDictionary *)response;
- (void)bindingMobileDidFailed:(NSString *)response;

- (void)finishedLoginDidSucceed:(NSDictionary *)response;
- (void)finishedLoginDidFailed:(NSString *)response;

-(void)postSystemExceptionLogSucceed:(NSDictionary *)reponse;
-(void)postSystemExceptionLogFailed:(NSString *)response;

-(void)changeUserInfoSucceed:(NSDictionary *)reponse;
-(void)changeUserInfoFailed:(NSString *)response;


- (void)requestMembersCardSucceed:(NSDictionary *)response;
- (void)requestMembersCardFaild:(NSString *)response;

- (void)getMembersCardSucceed:(NSDictionary *)response;
- (void)getMembersCardFaild:(NSString *)response;

- (void)getMembersCardInfoSucceed:(NSDictionary *)response;
- (void)getMembersCardInfoFaild:(NSString *)response;

- (void)getMemberInfoSucceed:(NSDictionary *)response;
- (void)getMemberInfoFaild:(NSString *)response;

- (void)getChangeMemberCardMobileWithAuthcodeSucceed:(NSDictionary *)response;
- (void)getChangeMemberCardMobileWithAuthcodeFaild:(NSString *)response;

- (void)getMemberCardRechargeInitInfoSucceed:(NSDictionary *)response;
- (void)getMemberCardRechargeInitInfoFaild:(NSString *)response;

- (void)requestMemberRechargeSucceed:(NSDictionary *)response;
- (void)requestMemberRechargeFaild:(NSString *)response;

- (void)requestMemberRechargePaymentSucceed:(NSDictionary *)response;
- (void)requestMemberRechargePaymentFaild:(NSString *)response;

- (void)changeMemberCardInfoSucceed:(NSDictionary *)response;
- (void)changeMemberCardInfoFaild:(NSString *)response;

- (void)getMemberCardBalanceInfoSucceed:(NSDictionary *)response;
- (void)getMemberCardBalanceInfoFaild:(NSString *)response;

- (void)requestMemberRechargeRecordsSucceed:(NSDictionary *)response;
- (void)requestMemberRechargeRecordsFaild:(NSString *)response;

- (void)getMembersCardDetailsSucceed:(NSDictionary *)response;
- (void)getMembersCardDetailsFaild:(NSString *)response;


- (void)getMemberDiscountInfoSucceed:(NSDictionary *)response;
- (void)getMemberDiscountInfoFaild:(NSString *)response;

- (void)getMemberExclusiveInfoSucceed:(NSDictionary *)response;
- (void)getMemberExclusiveInfoFaild:(NSString *)response;


@end

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief  数据操作总入口
 *  
 */

@interface ZSTF3Engine : NSObject<ZSTLegicyCommunicatorDelegate,SDWebFileManagerDelegate,SDWebFileDownloaderDelegate> {
//    ZSTZSTLogUtil *_logger;
    
}

@property (nonatomic, assign) NSInteger moduleType;
@property (nonatomic, retain) ZSTDao *dao;
@property (nonatomic, assign) id delegate;

- (BOOL) isValidDelegateForSelector:(SEL)selector;

-(void)cancelAllRequest;

- (void)cancel:(NSURL *)theUrl;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSThread *)workThread;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief    获取客户端参数
 *
 *	@returns  操作是否成功
 */
+ (BOOL)updateECMobileClientParams;

/**
 *	@brief    手动更新后获取客户端参数
 */
- (void)enUpdateECMobileClientParams;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	绑定客户端，phoneNumber为空时会从服务器获取到虚拟手机号
 *
 *	@param 	phoneNumber 	要绑定的电话号码
 *	@param 	checksum        校验码
 *
 *	@return	操作是否成功
 */
+ (BOOL)registMobileClientFor:(NSString *)phoneNumber checksum:(NSString *)checksum;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	手动绑定获取验证码
 *
 *	@param 	phoneNumber 	要绑定的手机号码
 */
- (void)getRegChecksum:(NSString *)phoneNumber;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	绑定客户端，phoneNumber为空时会从服务器获取到虚拟手机号
 *  
 *	@param 	phoneNumber 	要绑定的电话号码
 *	@param 	checksum        校验码
 *  
 */
- (void)registMobileClient:(NSString *)phoneNumber checksum:(NSString *)checksum;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief 检查版本
 */
- (void)checkClientVersion;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	提交建议
 *
 */
- (void)submitAdvice:(NSString *)submitAdvice;



/**
 *	@brief	提交指定时间之前的用户日志
 *
 *	@param 	date 	指定日期
 */
+(void)submitUserLogBeforeDate:(NSDate *)date;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	提交用户日志
 *
 *	@param 	content 内容
 */
+ (BOOL)submitUserLog:(NSString *)content;


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	同步push参数
 *
 */
+ (BOOL)syncPushNotificationParams;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/**
 *	@brief	上传系统日志
 *
 */
+ (void)uploadSysLog;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// ########################################## 我是腐蚀线  #####################################
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
- (void) updateECClientVisitInfoWithMsisdn:(NSString *)msisdn;

- (void) getECClientParams;

- (void) automaticLoginWithMsisdn:(NSString *)msisdn userId:(NSString *)userid;

- (void) loginWithMsisdn:(NSString *)msisdn password:(NSString *)password;

- (void) getVerificationCodeWithMsisdn:(NSString *)msisdn OperType:(NSInteger)opertype;

- (void) checkVerificationCodeWithMsisdn:(NSString *)msisdn verificationCode:(NSString *)code OperType:(NSInteger)opertype;

- (void) registerWithMsisdn:(NSString *)msisdn password:(NSString *)password verificationCode:(NSString *)code;

- (void) getPointWithMsisdn:(NSString *)msisdn userId:(NSString *)userid;

- (void) updatePointWithMsisdn:(NSString *)msisdn
                        userId:(NSString *)userid
                      pointNum:(NSInteger)pointnum
                     pointType:(NSInteger)pointtype
                    costAmount:(NSString *)costamount
                          desc:(NSString *)desc;

- (void) updatePasswordWithMsisdn:(NSString *)msisdn
                 verificationCode:(NSString *)code
                         password:(NSString *)password
                         OperType:(int)opertype;

- (void) updateMsisdnWithMsisdn:(NSString *)msisdn
                         userId:(NSString *)userid
               verificationCode:(NSString *)code
                       OperType:(int)opertype;

- (void) getUserInfoWithMsisdn:(NSString *)msisdn userId:(NSString *)userid;

- (void) updateUserInfoWith:(NSString *)msisdn
                     userId:(NSString *)userid
                    iconUrl:(NSString *)iconurl
                       name:(NSString *)name
                   birthday:(NSInteger)birthday
                        sex:(int)sex
                    address:(NSString *)address
                  signature:(NSString *)signature
                phonePublic:(NSInteger)phonePublic;

- (void) uploadFileWith:(NSString *)msisdn userId:(NSString *)userid data:(NSData *)data;

- (void) checkPasswordWithMsisdn:(NSString *)msisdn userId:(NSString *)userid;

- (void) logoutWithMsisdn:(NSString *)msisdn userId:(NSString *)userid;

- (void)getThirdLoginAuthorizeEcid:(NSString *)ecid;
- (void)thirdLoginWithDictionary:(NSDictionary *)dict;
- (void)checkPhoneNumIsExist:(NSString *)phoneMsisdn;
- (void)bindingMobileWithMsisdn:(NSString *)msisdn password:(NSString *)password;
- (void)finishedLoginWithPassWord:(NSString *)password AndUserId:(NSString *)userId MobilePhone:(NSString *)msisdn Code:(NSString *)code;
- (void)bugSubmit:(NSString *)bugInfo;


#pragma mark - 上传崩溃日志
-(void)postSystemExceptionLog:(NSString *)ecid logInfo:(NSString *)logInfo;
- (void)changeUserInfoWithParam:(NSDictionary *)param;

#pragma mark - 会员卡
- (void)requestMemberCard:(NSDictionary *)param;
- (void)getMemberCard:(NSDictionary *)param;
- (void)getMemberCardInfo:(NSDictionary *)param;
- (void)getMemberInfo:(NSDictionary *)param;
- (void)getChangeMemberCardMobileWithAuthcode:(NSDictionary *)param;
- (void)getMemberCardRechargeInitInfo:(NSDictionary *)param;
- (void)requestMemberRechargeWithParam:(NSDictionary *)param;
- (void)requestMemberRechargePayment:(NSDictionary *)param;
- (void)changeMemberCardInfo:(NSDictionary *)param;
- (void)getMemberCardBalanceInfo:(NSDictionary *)param;
- (void)requestMemberRechargeRecords:(NSDictionary *)param;
- (void)getMembersCardDetails:(NSDictionary *)param;
- (void)getMembersCardShopInfos:(NSDictionary *)param;
- (void)getMemberDiscountInfo:(NSDictionary *)param;
- (void)getMemberExclusiveInfo:(NSDictionary *)param;

@end
