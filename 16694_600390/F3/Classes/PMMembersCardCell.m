//
//  PMMembersCardCell.m
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersCardCell.h"

@implementation PMMembersCardCell

- (void)createTitleUI
{
    if (!self.titleLabel) {
        
        // 会员卡标题
        self.titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, (self.contentView.frame.size.height - 30) / 2, 100, 30)];
        self.titleLabel.backgroundColor = [UIColor clearColor];
        self.titleLabel.textColor = [UIColor blackColor];
        self.titleLabel.textAlignment = NSTextAlignmentLeft;
        self.titleLabel.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.titleLabel];
        
        // 会员卡余额
        self.balanceLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(345), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(260), WidthRate(50))];
        self.balanceLab.backgroundColor = [UIColor clearColor];
        self.balanceLab.textColor = RGBA(252, 41, 27, 1);
        self.balanceLab.textAlignment = NSTextAlignmentRight;
        self.balanceLab.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.balanceLab];
        
        // 充值送积分
        self.integralLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(345), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(260), WidthRate(50))];
        self.integralLab.backgroundColor = [UIColor clearColor];
        self.integralLab.textColor = [UIColor darkGrayColor];
        self.integralLab.textAlignment = NSTextAlignmentRight;
        self.integralLab.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.integralLab];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
}

@end
