//
//  ZSTModulController.m
//  F3
//
//  Created by luobin on 7/10/12.
//  Copyright (c) 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ElementParser.h"
#import "ZSTGlobal+Module.h"

@interface ZSTModule ()
@property (nonatomic, readwrite) NSInteger ID;                   //插件ID
@property (nonatomic, readwrite) NSInteger defaultModuleType;
@property (nonatomic, retain, readwrite) NSString *name;                 //插件名称
@property (nonatomic, retain, readwrite) NSString *package;              //插件包名
@property (nonatomic, readwrite) Class entryPoint;               //插件入口类
@property (nonatomic, readwrite) Class settingEntryPoint;        //设置插件入口类
@property (nonatomic, readwrite) BOOL hasSetting;                //插件是否有设置项
@end

@implementation ZSTModule

@synthesize ID;
@synthesize name;
@synthesize package;
@synthesize entryPoint;
@synthesize settingEntryPoint;
@synthesize hasSetting;
@synthesize defaultModuleType;

- (void)dealloc
{
    self.name = nil;
    self.package = nil;
    [super dealloc];
}

+ (id)pluginWithElement:(Element *)element;
{
    ZSTModule *plugin = [[ZSTModule alloc] init];
    plugin.ID = [[[element selectElement:@"ModuleID"] contentsNumber] integerValue];
    plugin.defaultModuleType = [[[element selectElement:@"DefaultModuleType"] contentsNumber] integerValue]; 
    
    NSString *name = [[element selectElement:@"ModuleName"] contentsText];
    NSAssert(name != nil ,@"Module name must not be null.");
    plugin.name = name?name:@"";
    
    NSString *package = [[element selectElement:@"ModulePackage"] contentsText];
    NSAssert(package != nil ,@"Module package must not be null.");
    plugin.package = package?package:@"";
    
    NSString *entryPoint = [[element selectElement:@"ModuleEnter"] contentsText];
    NSAssert(entryPoint != nil ,@"Plugin entryPoint must not be null.");
    Class entryPointClass = NSClassFromString(entryPoint);
    NSAssert(entryPointClass != nil ,@"Can't find Plugin entryPoint class:%@.", entryPoint);
    NSAssert([entryPointClass conformsToProtocol:NSProtocolFromString(@"ZSTModuleDelegate")], @"Plugin entryPoint class must conform protocol ZSTModuleDelegate.");
    plugin.entryPoint = entryPointClass;
    
    NSString *settingEntryPoint = [[element selectElement:@"SettingEnter"] contentsText];
    Class settingEntryPointClass = NSClassFromString(settingEntryPoint);
    plugin.settingEntryPoint = settingEntryPointClass;
    
    return [plugin autorelease];
}

- (UIImage *)moduleIconN
{
    if (self.ID == -1) {
        return [UIImage imageNamed:@"bottom_img_inbox_n.png"];
    } else if (self.ID == -2) {
        return [UIImage imageNamed:@"bottom_img_outbox_n.png"];
    } else if (self.ID == -3) {
        return [UIImage imageNamed:@"bottom_img_setting_n.png"];
    } else {
         return [UIImage imageNamed:[NSString stringWithFormat:@"Module.bundle/%@/module_Icon_n.png", self.package]];
    }
}

- (UIImage *)moduleIconP
{
    if (self.ID == -1) {
        return [UIImage imageNamed:@"bottom_img_inbox_p.png"];
    } else if (self.ID == -2) {
        return [UIImage imageNamed:@"bottom_img_outbox_p.png"];
    } else if (self.ID == -3) {
        return [UIImage imageNamed:@"bottom_img_setting_p.png"];
    } else {
        return [UIImage imageNamed:[NSString stringWithFormat:@"Module.bundle/%@/module_Icon_p.png", self.package]];
    }
}

- (BOOL)hasSetting
{
//    return [[self.element selectElement:@"ModuleEnter"] contentsNumber];
    return NO;
}

@end
