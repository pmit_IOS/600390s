//
//  LongPressButton.h
//  F3_UI
//
//  Created by luobin on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

#define UIControlEventLongPress (1<<9)

@interface ZSTLongPressButton : UIButton {
    NSTimer *_timer;
}

@end
