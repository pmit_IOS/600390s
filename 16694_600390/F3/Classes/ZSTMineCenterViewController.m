//
//  ZSTMineCenterViewController.m
//  F3
//
//  Created by P&M on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTMineCenterViewController.h"
#import "ZSTPersonalCenterCell.h"
#import "ZSTLoginController.h"
#import "BaseNavgationController.h"
#import "ZSTAccountManagementVC.h"
#import "ZSTSuggestedQuestionsViewController.h"
#import "ZSTWebViewController.h"
#import "ZSTAboutViewController.h"
#import "ZSTInformViewController.h"
#import "ZSTModuleAddition.h"
#import "ZSTPersonalInfo.h"
#import "ZSTShareSettingViewController.h"
#import "UIImage+Compress.h"
#import "UIImageView+WebCache.h"
#import "ZSTPersonalAboutViewController.h"
#import "ZSTPersonInfoViewController.h"
#import "ZSTSettingViewController.h"
#import "ZSTSuggestViewController.h"
#import "ZSTPersonalViewController.h"
#import "ZSTNewShareView.h"
#import "ZSTWXShareType.h"
#import "ZSTMessageTypeViewController.h"
#import "PMMembersCardViewController.h"
#import "SWCountingLabel.h"

@interface ZSTMineCenterViewController () <ZSTLoginControllerDelegate,ZSTNewShareViewDelegate,MFMessageComposeViewControllerDelegate>
{
    NSString *sharePoint;
    NSInteger passwordStatus;
    UserInfo *userInfo;
}

@property (strong,nonatomic) NSDictionary *infoDic;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) ZSTNewShareView *shareView;
@property (assign,nonatomic) NSInteger wxSharePoint;
@property (assign,nonatomic) NSInteger wxFriendSharePoint;
@property (assign,nonatomic) NSInteger weiboSharePoint;
@property (assign,nonatomic) NSInteger qqSharePoint;
@property (assign,nonatomic) NSInteger smsSharePoint;
@property (strong,nonatomic) UIActivityIndicatorView *activityView;
@property (strong,nonatomic) NSString *jumpTypeString;
@property (assign,nonatomic) BOOL hasNext;

@property (strong, nonatomic) SWCountingLabel *myBalanceLab;
@property (strong, nonatomic) SWCountingLabel *myIntegralLab;

@end

@implementation ZSTMineCenterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareSucceed:) name:NotificationName_WXShareSucceed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareFaild:) name:NotificationName_WXShareFaild object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboSuccess:) name:@"sendWeiboSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendQQShareSuccess:) name:@"QQShareSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendQQShareFail:) name:@"QQShareFail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sendWeiboShareFail:) name:@"sendWeiboFailure" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(thirdLoginSuccess:) name:@"thirdLoginSuccess" object:nil];
    
    // 初始化数据
    self.imageArray = @[@[@"module_personal_share.png"],@[@"module_personal_integral.png",@"module_personal_account.png",@"module_personal_share.png"],@[@"module_personal_integral.png"],@[@"module_personal_setting.png",@"module_personal_proposal.png",@"module_personal_about.png"]];
    self.titleArray = @[@[@"我的会员"],@[@"个人积分",@"登录账号",@"分享App"],@[@"信息推送"],@[@"系统设置",@"提提建议",@"关于"]];
    
    // 初始化 tableView
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT) style:UITableViewStylePlain];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = RGBA(236, 236, 236, 1);
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[ZSTPersonalCenterCell class] forCellReuseIdentifier:@"personalCell"];
    [self.view addSubview:self.tableView];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    self.isShareApp = NO;
    
    [self createHeaderViewUI];
    
    // 请求用户数据
    if ([ZSTF3Preferences shared].UserId && [ZSTF3Preferences shared].UserId.length > 0) {
        
        self.activityView.hidden = NO;
        [self.activityView startAnimating];
        NSString *loginMsisdn = @"";
        if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"4"]) {
            loginMsisdn = @"";
        }
        else {
            loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
        }
        
        [self.engine getUserInfoWithMsisdn:loginMsisdn userId:[ZSTF3Preferences shared].UserId];
        [_tableView reloadData];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES];
    
    // headerView 背景动画特效
    [UIView animateWithDuration:0.07f animations:^{
        
        // 改变背景图大小
        self.headerViewBgIma.frame = CGRectMake(13, 13, WIDTH - 26, WidthRate(420) - 26);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.07f animations:^{
            
            self.headerViewBgIma.frame = CGRectMake(12, 12, WIDTH - 24, WidthRate(420) - 24);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.07f animations:^{
                
                self.headerViewBgIma.frame = CGRectMake(11, 11, WIDTH - 22, WidthRate(420) - 22);
                
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.07f animations:^{
                    
                    self.headerViewBgIma.frame = CGRectMake(10, 10, WIDTH - 20, WidthRate(420) - 20);
                    
                } completion:^(BOOL finished) {
                    
                    [UIView animateWithDuration:0.07f animations:^{
                        
                        self.headerViewBgIma.frame = CGRectMake(9, 9, WIDTH - 18, WidthRate(420) - 18);
                        
                    } completion:^(BOOL finished) {
                        
                        [UIView animateWithDuration:0.07f animations:^{
                            
                            self.headerViewBgIma.frame = CGRectMake(8, 8, WIDTH - 16, WidthRate(420) - 16);
                            
                        } completion:^(BOOL finished) {
                            
                            [UIView animateWithDuration:0.07f animations:^{
                                
                                self.headerViewBgIma.frame = CGRectMake(7, 7, WIDTH - 14, WidthRate(420) - 14);
                                
                            } completion:^(BOOL finished) {
                                
                                [UIView animateWithDuration:0.07f animations:^{
                                    
                                    self.headerViewBgIma.frame = CGRectMake(6, 6, WIDTH - 12, WidthRate(420) - 12);
                                    
                                } completion:^(BOOL finished) {
                                    
                                    [UIView animateWithDuration:0.07f animations:^{
                                        
                                        self.headerViewBgIma.frame = CGRectMake(5, 5, WIDTH - 10, WidthRate(420) - 10);
                                        
                                    } completion:^(BOOL finished) {
                                       
                                        [UIView animateWithDuration:0.07f animations:^{
                                            
                                            self.headerViewBgIma.frame = CGRectMake(4, 4, WIDTH - 8, WidthRate(420) - 8);
                                            
                                        } completion:^(BOOL finished) {
                                           
                                            [UIView animateWithDuration:0.07f animations:^{
                                                
                                                self.headerViewBgIma.frame = CGRectMake(3, 3, WIDTH - 6, WidthRate(420) - 6);
                                                
                                            } completion:^(BOOL finished) {
                                               
                                                [UIView animateWithDuration:0.07f animations:^{
                                                    
                                                    self.headerViewBgIma.frame = CGRectMake(2, 2, WIDTH - 4, WidthRate(420) - 4);
                                                    
                                                } completion:^(BOOL finished) {
                                                    
                                                    [UIView animateWithDuration:0.07f animations:^{
                                                        
                                                        self.headerViewBgIma.frame = CGRectMake(1, 1, WIDTH - 2, WidthRate(420) - 2);
                                                        
                                                    } completion:^(BOOL finished) {
                                                       
                                                        [UIView animateWithDuration:0.07f animations:^{
                                                            
                                                            self.headerViewBgIma.frame = CGRectMake(0, 0, WIDTH, WidthRate(420));
                                                            
                                                        } completion:^(BOOL finished) {
                                                            
                                                        }];
                                                    }];
                                                }];
                                            }];
                                        }];
                                    }];
                                }];
                            }];
                        }];
                    }];
                }];
            }];
        }];
    }];
    
    
    if (self.isShareApp == YES) {
        if ([ZSTF3Preferences shared].UserId && [ZSTF3Preferences shared].UserId.length > 0) {
            //[TKUIUtil showHUD:self.view];
            self.activityView.hidden = NO;
            [self.activityView startAnimating];
            NSString *loginMsisdn = @"";
            if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"4"]) {
                loginMsisdn = @"";
            }
            else {
                loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
            }
            
            [self.engine getUserInfoWithMsisdn:loginMsisdn userId:[ZSTF3Preferences shared].UserId];
            
            if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_APP]) {
                
                [self.engine checkPasswordWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
            }
        }
        self.isShareApp = NO;
        [_tableView reloadData];
    }
    
    NSIndexPath *indexPath0 = [NSIndexPath indexPathForRow:0 inSection:0];
    ZSTPersonalCenterCell *cell0 = (ZSTPersonalCenterCell *)[self.tableView cellForRowAtIndexPath:indexPath0];
    
    NSIndexPath *indexPath1 = [NSIndexPath indexPathForRow:1 inSection:0];
    ZSTPersonalCenterCell *cell1 = (ZSTPersonalCenterCell *)[self.tableView cellForRowAtIndexPath:indexPath1];
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) {
        
        self.avatarBg.hidden = YES;
        self.avatarImage.hidden = YES;
        self.sexIma.hidden = YES;
        self.nameLabel.hidden = YES;
        
        cell0.integralLab.hidden = YES;
        cell0.signInBtn.hidden = NO;
        cell0.signedInLab.hidden = YES;
        cell1.accountLab.hidden = YES;
        
        self.loginBtn.hidden = NO;
        
        [self.activityView stopAnimating];
        self.activityView.hidden = YES;
    }
    else {
        
        self.loginBtn.hidden = YES;
        
        self.avatarBg.hidden = NO;
        self.avatarImage.hidden = NO;
        self.sexIma.hidden = NO;
        self.nameLabel.hidden = NO;
        
        cell0.integralLab.hidden = NO;
        cell1.accountLab.hidden = NO;
    }
}

- (void)createHeaderViewUI
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(420))];
    headerView.backgroundColor = RGBA(243, 243, 243, 1);
    
    // 背景图片
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(14, 14, WIDTH - 28, WidthRate(420) - 28)];
    bgImage.image = [UIImage imageNamed:@"module_personal_personBg.png"];
    self.headerViewBgIma = bgImage;
    [headerView addSubview:bgImage];
    bgImage.userInteractionEnabled = YES;
    
    
    UITapGestureRecognizer *infoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToPersonInfo:)];
    [bgImage addGestureRecognizer:infoTap];
    
    
    self.activityView = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(WIDTH - 40, 10, 20, 20)];
    [bgImage addSubview:self.activityView];
    [self.activityView startAnimating];
    
    // 登录按钮
    self.loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.loginBtn.frame = CGRectMake((WIDTH - WidthRate(200)) / 2, (headerView.frame.size.height - WidthRate(80)) / 2, WidthRate(200), WidthRate(80));
    [self.loginBtn setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.loginBtn addTarget:self action:@selector(loginButtonAction:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginBtn.layer setCornerRadius:5.0f];
    [self.loginBtn.layer setBorderWidth:0.5f];
    [self.loginBtn.layer setBorderColor:RGBA(162, 229, 118, 1).CGColor];
    [self.loginBtn setBackgroundColor:RGBA(162, 229, 118, 1)];
    [headerView addSubview:self.loginBtn];
    self.loginBtn.hidden = YES;
    
    // 返回按钮
    UIImageView *btnImage = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(20), WidthRate(20), WidthRate(60), WidthRate(60))];
    btnImage.image = [UIImage imageNamed:@"module_personal_back.png"];
    [headerView addSubview:btnImage];
    
    PMRepairButton *backBtn = [[PMRepairButton alloc] init];
    backBtn.frame = CGRectMake(0, 0, WidthRate(100), WidthRate(100));
    [backBtn setBackgroundColor:[UIColor clearColor]];
    [backBtn addTarget:self action:@selector(backButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [headerView insertSubview:backBtn atIndex:999];
    
    // 头像背景
    self.avatarBg = [[UIImageView alloc] initWithFrame:CGRectMake((WIDTH - WidthRate(240)) / 2, WidthRate(55), WidthRate(240), WidthRate(240))];
    self.avatarBg.image = [UIImage imageNamed:@"module_personal_avatarBg.png"];
    [headerView addSubview:self.avatarBg];
    self.avatarBg.hidden = NO;
    self.avatarBg.userInteractionEnabled = YES;
    
    // 头像图片
    self.avatarImage = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(5), WidthRate(5), WidthRate(230), WidthRate(230))];
    self.avatarImage.image = [UIImage imageNamed:@"module_personal_my_avater_defaut.png"];
    self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width / 2.0f;
    self.avatarImage.layer.masksToBounds = YES;
    self.avatarImage.userInteractionEnabled = YES;
//    self.avatarImage.contentMode = UIViewContentModeScaleAspectFit;
    [self.avatarBg addSubview:self.avatarImage];
    self.avatarImage.hidden = NO;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatarImage)];
    [self.avatarImage addGestureRecognizer:tap];
    
    // 改变头像
    UIImageView *changeIma = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(140), WidthRate(190), WidthRate(60), WidthRate(60))];
    changeIma.image = [UIImage imageNamed:@"module_personal_changeIma.png"];
    [self.avatarBg insertSubview:changeIma atIndex:1000];
    
    UITapGestureRecognizer *changeAvatar = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(changeAvatarImage)];
    [changeIma addGestureRecognizer:changeAvatar];
    changeIma.userInteractionEnabled = YES;
    
    // 性别图片
    self.sexIma = [[UIImageView alloc] init];
    [headerView insertSubview:self.sexIma atIndex:9999];
    self.sexIma.hidden = NO;
    
    // 用户名、昵称
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.backgroundColor = [UIColor clearColor];
    self.nameLabel.textColor = [UIColor whiteColor];
    self.nameLabel.textAlignment = NSTextAlignmentCenter;
    self.nameLabel.font = [UIFont boldSystemFontOfSize:16.0f];
    self.nameLabel.shadowColor = [UIColor darkGrayColor];
    self.nameLabel.shadowOffset = CGSizeMake(1.0f, 1.0f);
    [headerView insertSubview:self.nameLabel atIndex:9999];
    self.nameLabel.hidden = NO;
    
    self.tableView.tableHeaderView = headerView;
}

#pragma mark - tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.titleArray[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 15)];
    headerView.backgroundColor = RGBA(243, 243, 243, 1);
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        return 50.0f;
    }
    else {
        return 1;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] init];
    
    if (section == 0) {
        
        footerView.frame = CGRectMake(0, 0, WIDTH, 50);
        footerView.backgroundColor = [UIColor whiteColor];
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(WidthRate(375), WidthRate(5), WidthRate(1), 50 - WidthRate(10));
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [footerView.layer addSublayer:layer];
        
        // 我的余额
        UILabel *myBalance = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 0, WidthRate(315), 20)];
        myBalance.backgroundColor = [UIColor clearColor];
        myBalance.text = @"我的余额";
        myBalance.textColor = [UIColor grayColor];
        myBalance.textAlignment = NSTextAlignmentCenter;
        myBalance.font = [UIFont systemFontOfSize:13.0f];
        [footerView addSubview:myBalance];
        
        // 我的余额数目
        self.myBalanceLab = [[SWCountingLabel alloc] initWithFrame:CGRectMake(WidthRate(30), 20, WidthRate(315), 25)];
        self.myBalanceLab.backgroundColor = [UIColor clearColor];
        self.myBalanceLab.text = @"0.00";
        self.myBalanceLab.textColor = RGBA(231, 117, 119, 1);
        self.myBalanceLab.textAlignment = NSTextAlignmentCenter;
        self.myBalanceLab.font = [UIFont systemFontOfSize:16.0f];
        [footerView addSubview:self.myBalanceLab];
        
        // 我的积分
        UILabel *myIntegral = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(405), 0, WidthRate(315), 20)];
        myIntegral.backgroundColor = [UIColor clearColor];
        myIntegral.text = @"我的积分";
        myIntegral.textColor = [UIColor grayColor];
        myIntegral.textAlignment = NSTextAlignmentCenter;
        myIntegral.font = [UIFont systemFontOfSize:13.0f];
        [footerView addSubview:myIntegral];
        
        // 我的积分数目
        self.myIntegralLab = [[SWCountingLabel alloc] initWithFrame:CGRectMake(WidthRate(405), 20, WidthRate(315), 25)];
        self.myIntegralLab.backgroundColor = [UIColor clearColor];
        self.myIntegralLab.text = @"0";
        self.myIntegralLab.textColor = RGBA(231, 117, 119, 1);
        self.myIntegralLab.textAlignment = NSTextAlignmentCenter;
        self.myIntegralLab.font = [UIFont systemFontOfSize:16.0f];
        [footerView addSubview:self.myIntegralLab];
    }
    else {
        footerView.frame = CGRectMake(0, 0, WIDTH, 1);
        footerView.backgroundColor = RGBA(243, 243, 243, 1);
    }
    
    return footerView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTPersonalCenterCell *personalCell = [tableView dequeueReusableCellWithIdentifier:@"personalCell"];
    
    [personalCell createPersonalCenterUI];
    
    // 实现静态数据
    personalCell.cellIma.image = [UIImage imageNamed:self.imageArray[indexPath.section][indexPath.row]];
    personalCell.cellTitle.text = self.titleArray[indexPath.section][indexPath.row];
    
    
    // 实现请求数据
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        personalCell.membersLab.hidden = NO;
        personalCell.membersLab.text = @"白金会员";
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(WidthRate(110), personalCell.frame.size.height - 1.0f, WIDTH - WidthRate(110), 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [personalCell.layer addSublayer:layer];
        
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        
        personalCell.integralLab.hidden = NO;
        
        personalCell.signInBtn.hidden = NO;
        [personalCell.signInBtn addTarget:self action:@selector(signInButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        personalCell.signedInLab.hidden = YES;
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(WidthRate(110), personalCell.frame.size.height - 1.0f, WIDTH - WidthRate(110), 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [personalCell.layer addSublayer:layer];
    }
    else if (indexPath.section == 1 && indexPath.row == 1) {
        
        personalCell.accountLab.hidden = NO;
        personalCell.accountLab.text = [ZSTF3Preferences shared].loginMsisdn;
        
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(WidthRate(110), personalCell.frame.size.height - 1.0f, WIDTH - WidthRate(110), 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [personalCell.layer addSublayer:layer];
    }
    else if (indexPath.section == 1 && indexPath.row == 2) {
        personalCell.integralTip.hidden = NO;
        
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 2 && indexPath.row == 0) {
        
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 3 && indexPath.row == 0) {
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(WidthRate(110), personalCell.frame.size.height - 1.0f, WIDTH - WidthRate(110), 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [personalCell.layer addSublayer:layer];
        
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else if (indexPath.section == 3 && indexPath.row == 1) {
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(WidthRate(110), personalCell.frame.size.height - 1.0f, WIDTH - WidthRate(110), 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [personalCell.layer addSublayer:layer];
        
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    else {
        // 告诉用户有下一级页面
        personalCell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    }
    
    [self tableView:tableView heightForHeaderInSection:indexPath.section];
    
    return personalCell;
}

#pragma mark －－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    if (indexPath.section == 0 && indexPath.row == 0) {// 会员卡
        
        PMMembersCardViewController *membersCardVC = [[PMMembersCardViewController alloc] init];
        [self.navigationController pushViewController:membersCardVC animated:YES];
    }
    
    else if (indexPath.section == 1 && indexPath.row == 1) {// 登录账号
        
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
            
            self.jumpTypeString = @"loginType";
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];

            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            
            return;
        }
        
        ZSTAccountManagementVC *controller = [[ZSTAccountManagementVC alloc] init];
        controller.passwordStatus = passwordStatus;
        controller.hidesBottomBarWhenPushed = YES;
        self.isShareApp = YES;
        [self.navigationController pushViewController:controller animated:YES];
        
    }
    
    else if (indexPath.section == 1 && indexPath.row == 2) {// 分享App
        
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
            
            self.jumpTypeString = @"shareType";
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
            
            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            
            return;
        }
        
        [UIView animateWithDuration:0.3f animations:^{
            
            self.shadowView.hidden = NO;
            self.shareView.frame = CGRectMake(0, HEIGHT - self.shareView.frame.size.height, WIDTH, self.shareView.frame.size.height);
            
        } completion:^(BOOL finished) {
            self.isShareApp = YES;
        }];
    }
    
    else if (indexPath.section == 2 && indexPath.row == 0) {// 信息推送
        
        ZSTMessageTypeViewController *messageTypeVC = [[ZSTMessageTypeViewController alloc] init];
        [self.navigationController pushViewController:messageTypeVC animated:YES];
    }
    
    else if (indexPath.section == 3 && indexPath.row == 0) {// 系统设置
    
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
            
            self.jumpTypeString = @"settingType";
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
            
            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            
            return;
        }
        
        ZSTSettingViewController *settingVC = [[ZSTSettingViewController alloc] init];
        BOOL isPublic = NO;
        if (userInfo.phonePublic == 0)
        {
            isPublic = NO;
        }
        else
        {
            isPublic = YES;
        }
        
        settingVC.isPhonePublic = isPublic;
        [self.navigationController pushViewController:settingVC animated:YES];
    }
    
    else if (indexPath.section == 3 && indexPath.row == 1) {// 提提建议
        
        ZSTSuggestViewController *suggestVC = [[ZSTSuggestViewController alloc] init];
        [self.navigationController pushViewController:suggestVC animated:YES];
    }
    
    else if (indexPath.section == 3 && indexPath.row == 2) {// 关于
        
        NSString *settingAbout = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"setting_about"];
        NSRange range = [settingAbout rangeOfString:@","];
        if ([settingAbout hasPrefix:@"http://"] && range.location == NSNotFound) {
            ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
            [webView setURL:settingAbout];
            webView.type = self.moduleType;
            webView.hidesBottomBarWhenPushed = YES;
            webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:webView animated:YES];
        }else{
            
            ZSTPersonalAboutViewController *personalAboutVC = [[ZSTPersonalAboutViewController alloc] init];
            [self.navigationController pushViewController:personalAboutVC animated:YES];
        }
    }
}

#pragma mark - 返回按钮响应事件
- (void)backButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - 登录按钮响应事件
- (void)loginButtonAction:(UIButton *)button
{
    self.hasNext = NO;
    ZSTLoginController *controller = [[ZSTLoginController alloc] init];
    controller.isFromSetting = YES;
    controller.delegate = self;
    BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
    
    [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        self.isShareApp = YES;
    }];
}


#pragma mark - 改变头像图片
- (void)changeAvatarImage
{
    UIActionSheet *actionSheet = nil;
    //是否支持拍照
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"获取图片"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"拍照", @"本地图片", nil];
    }
    else {
        actionSheet = [[UIActionSheet alloc] initWithTitle:@"获取图片"
                                                  delegate:self
                                         cancelButtonTitle:@"取消"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@"本地图片", nil];
    }
    
    [actionSheet showInView:self.view];
}

#pragma mark UIActionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    self.iconPickerController = [[UIImagePickerController alloc] init];
    [_iconPickerController.navigationBar setTintColor:RGBCOLOR(27, 140, 233)];
    
    if ( buttonIndex == 0 && [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) { // 拍照
        
        _iconPickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        _iconPickerController.cameraFlashMode = UIImagePickerControllerCameraFlashModeAuto;
        _iconPickerController.showsCameraControls = YES;
        
    } else if(buttonIndex != actionSheet.cancelButtonIndex) { // 本地图片
        
        _iconPickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
    }
    _iconPickerController.allowsEditing = YES;
    _iconPickerController.delegate = self;
    [self presentViewController:_iconPickerController animated:YES completion:nil];
}

//- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated{
//    if (_iconPickerController.sourceType == UIImagePickerControllerSourceTypePhotoLibrary  && [navigationController.viewControllers count] <=2) {
//        viewController.wantsFullScreenLayout = NO;
//        navigationController.navigationBar.translucent = NO;
//    }else{
//        viewController.wantsFullScreenLayout = YES;
//        navigationController.navigationBar.translucent = YES;
//    }
//}

////////////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark UIImagePickerControllerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    NSString *mediaType =[NSString stringWithFormat:@"%@", [info objectForKey:UIImagePickerControllerMediaType]];
    if (![mediaType isEqualToString:@"public.image"]) {
        return;
    }
    
    //UIImage *tempImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    UIImage *tempImage = [info objectForKey:UIImagePickerControllerEditedImage];
    [self compressImage:tempImage];
}

- (void)compressImage:(UIImage *)bigImage
{
    [self performSelector:@selector(doCompressImage:) withObject:bigImage afterDelay:0.6];
    UIImage *newImage = [UIImage imageWithData:[[bigImage compressedImageScaleFactor:640] compressedData]];
    self.avatarImage.image = newImage;
    NSData *data = UIImageJPEGRepresentation(newImage,0.5);
    self.activityView.hidden = NO;
    [self.activityView startAnimating];
    [self.engine uploadFileWith:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId data:data];
}

- (void)doCompressImage:(UIImage *)bigImage
{
//    NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
//    ZSTPersonalHeaderCell *cell = (ZSTPersonalHeaderCell *)[self.tableView cellForRowAtIndexPath:indexPath];
//    cell.avatarImgView.image = bigImage;
    
    self.avatarImage.image = bigImage;
    
    [self.iconPickerController dismissViewControllerAnimated:YES completion:nil];
    self.iconPickerController = nil;
}


#pragma mark - 签到按钮响应事件
- (void)signInButtonAction:(UIButton *)button
{
    if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
        
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            self.isShareApp = YES;
        }];
        
        return;
    }
    
    [TKUIUtil showHUD:self.view];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    ZSTPersonalCenterCell *cell = (ZSTPersonalCenterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSString *integral = [NSString stringWithFormat:@"%@", cell.integralLab.text];
    //从开头抽取字符串
    NSString *integralString = [integral substringToIndex:integral.length - 2];
    
    [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                userId:[ZSTF3Preferences shared].UserId
                              pointNum:[integralString intValue]
                             pointType:0
                            costAmount:@""
                                  desc:@""];
}

#pragma mark - 获得用户信息
- (void)getUserInfoDidSucceed:(NSDictionary *)response
{
//    [TKUIUtil hiddenHUD];
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    
    // 用户头像、性别、用户名
    NSString *iconString = [NSString stringWithFormat:@"%@", [[response safeObjectForKey:@"data"] safeObjectForKey:@"Icon"]];
    [self.avatarImage setImageWithURL:[NSURL URLWithString:iconString] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    
    //CGSize nameSize = [name sizeWithFont:[UIFont boldSystemFontOfSize:16.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 30)];
    NSString *name = nil;
    name = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Name"];
    if (name.length == 0) {
        name = @"佚名";
    }
    CGSize nameSize = [name boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16.0f]} context:nil].size;
    
    self.nameLabel.frame = CGRectMake((WIDTH - nameSize.width) / 2 + WidthRate(20), self.avatarBg.frame.origin.y + self.avatarBg.frame.size.height + WidthRate(30), nameSize.width, WidthRate(50));
    self.nameLabel.text = name;
    
    NSInteger sexInteger = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"Sex"] integerValue];
    self.sexIma.frame = CGRectMake(self.nameLabel.frame.origin.x - WidthRate(40), self.avatarBg.frame.origin.y + self.avatarBg.frame.size.height + WidthRate(40), WidthRate(30), WidthRate(30));
    if (sexInteger == 1) {
        self.sexIma.image = [UIImage imageNamed:@"module_personal_sex0.png"];
    }
    else {
        self.sexIma.image = [UIImage imageNamed:@"module_personal_sex1.png"];
    }
    
    
    self.infoDic = [response safeObjectForKey:@"data"];
    
    // 我的余额、我的积分
    //double totalBalance = [[response safeObjectForKey:@"balance"] doubleValue];
    double totalBalance = 568.00;
    [self withNumberLabel:self.myBalanceLab changeNumber:totalBalance withType:0];
    
    //NSInteger totalIntegral = [[response safeObjectForKey:@"integral"] integerValue];
    NSInteger totalIntegral = 9088;
    [self withNumberLabel:self.myIntegralLab changeNumber:totalIntegral withType:1];
    
    // 积分
    NSString *pointNum = nil;
    pointNum = [[response safeObjectForKey:@"data"] safeObjectForKey:@"PointNum"];
    NSString *pointNum2 = nil;
    
    // 积分大于1万分，以“万”字显示
    if ([pointNum integerValue] >= 10000) {
        NSArray *array = [NSArray arrayWithObjects:@"10000",@"20000",@"30000",@"40000",@"50000",@"60000",@"70000",@"80000",@"90000",@"100000",@"110000",@"120000",@"130000",@"140000",@"150000",@"160000",@"170000",@"180000",@"190000",@"200000",@"210000",@"220000",@"230000",@"240000",@"250000",@"260000",@"270000",@"280000",@"290000",@"300000",@"310000",@"320000",@"330000",@"340000",@"350000",@"360000",@"370000",@"380000",@"390000",@"400000",@"410000",@"420000",@"430000",@"440000",@"450000",@"460000",@"470000",@"480000",@"490000",@"500000", nil];
        for (NSInteger i = 0; i < array.count; i++) {
            if ([pointNum integerValue] >= [array[i] integerValue]) {
                pointNum2 = [NSString stringWithFormat:@"%ld万", i+1];
            }
        }
        pointNum = pointNum2;
    }
    
    // 个人积分
    NSString *integral = [NSString stringWithFormat:@"%@ 分", pointNum];// 9860 分
    
    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:integral];
    [string addAttribute:NSForegroundColorAttributeName value:RGBA(231, 117, 119, 1) range:NSMakeRange(0, [integral  rangeOfString:@"分"].location)];// 9860 为红色，取值为从位置0开始取到‘分’的前一位
    [string addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange([integral  rangeOfString:@"分"].location, 1)];// ‘分’为灰色，取值为从位置‘分’开始，取一位
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    ZSTPersonalCenterCell *cell = (ZSTPersonalCenterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    [cell.integralLab setAttributedText:string];
    
    // 电话号码
    NSIndexPath *secondIndexPath = [NSIndexPath indexPathForRow:1 inSection:1];
    ZSTPersonalCenterCell *accountCell = (ZSTPersonalCenterCell *)[self.tableView cellForRowAtIndexPath:secondIndexPath];
    accountCell.accountLab.text = [NSString stringWithFormat:@"%@",[[response safeObjectForKey:@"data"] safeObjectForKey:@"Msisdn"]];
    
    
    // 签到
    NSInteger signInteger = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"SignIn"] integerValue];
    if (signInteger == 1) {
        cell.signInBtn.hidden = YES;
        cell.signedInLab.hidden = NO;
    }
    else {
        cell.signInBtn.hidden = NO;
        cell.signedInLab.hidden = YES;
    }
    
    
    userInfo = [UserInfo userInfoWithdic:[response safeObjectForKey:@"data"]];
    [ZSTF3Preferences shared].snsaStates = userInfo.phonePublic;
    
    if ([[response safeObjectForKey:@"data"] safeObjectForKey:@"weixinopen_openid"] && [[[response safeObjectForKey:@"data"] safeObjectForKey:@"weixinopen_openid"] isKindOfClass:[NSString class]] && ![[[response safeObjectForKey:@"data"] objectForKey:@"weixinopen_openid"] isEqualToString:@""])
    {
        [ZSTF3Preferences shared].weixinOpenId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"weixinopen_openid"];
    }
    
    if ([[response safeObjectForKey:@"data"] safeObjectForKey:@"qq_openid"] && [[[response safeObjectForKey:@"data"] safeObjectForKey:@"qq_openid"] isKindOfClass:[NSString class]] && ![[[response safeObjectForKey:@"data"] objectForKey:@"qq_openid"] isEqualToString:@""])
    {
        [ZSTF3Preferences shared].qqOpenId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"qq_openid"];
        NSLog(@"qqOpenId --> %@",[ZSTF3Preferences shared].qqOpenId);
    }
    
    if ([[response safeObjectForKey:@"data"] safeObjectForKey:@"sina_openid"] && [[[response safeObjectForKey:@"data"] safeObjectForKey:@"sina_openid"] isKindOfClass:[NSString class]] && ![[[response safeObjectForKey:@"data"] objectForKey:@"sina_openid"] isEqualToString:@""])
    {
        [ZSTF3Preferences shared].sinaOpenId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"sina_openid"];
    }
    
    if (self.hasNext)
    {
        [self goToNextStep];
    }

    
    [self.engine getPointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
}


// 我的余额、我的积分数字变动效果实现
- (void)withNumberLabel:(SWCountingLabel *)label changeNumber:(double)number withType:(NSInteger)type
{
    label.method = UILabelCountingMethodLinear;
    if (type == 0) {
        label.format = @"%.2f";
        [label countFrom:0 to:number withDuration:1.0];
    }
    else {
        label.format = @"%d";
        [label countFrom:1 to:number withDuration:1.0];
    }
}

- (void)getUserInfoDidFailed:(NSString *)response
{
//    [TKUIUtil hiddenHUD];
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)updatePointDidSucceed:(NSDictionary *)response
{
//    [TKUIUtil hiddenHUD];
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    NSString *message = [[response safeObjectForKey:@"data"] safeObjectForKey:@"PointNum"];
    NSString *string = [NSString stringWithFormat:@"恭喜你获得%@积分",message];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(string, nil) withImage:nil];
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    ZSTPersonalCenterCell *cell = (ZSTPersonalCenterCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    
    NSInteger costNum = [message integerValue];
    
    NSString *integral = [NSString stringWithFormat:@"%@", cell.integralLab.text];
    //从开头抽取字符串: 150 分
    NSString *integralString = [integral substringToIndex:integral.length - 2];
    
    NSInteger originalNum = [integralString integerValue];
    NSInteger currentNum = originalNum + costNum;
    
    NSString *pointNum = nil;
    pointNum = [NSString stringWithFormat:@"%ld", (long)currentNum];
    NSString *pointNum2 = nil;
    
    // 积分大于1万分，以“万”字显示
    if ([pointNum integerValue] >= 10000) {
        NSArray *array = [NSArray arrayWithObjects:@"10000",@"20000",@"30000",@"40000",@"50000",@"60000",@"70000",@"80000",@"90000",@"100000",@"110000",@"120000",@"130000",@"140000",@"150000",@"160000",@"170000",@"180000",@"190000",@"200000",@"210000",@"220000",@"230000",@"240000",@"250000",@"260000",@"270000",@"280000",@"290000",@"300000",@"310000",@"320000",@"330000",@"340000",@"350000",@"360000",@"370000",@"380000",@"390000",@"400000",@"410000",@"420000",@"430000",@"440000",@"450000",@"460000",@"470000",@"480000",@"490000",@"500000", nil];
        for (NSInteger i = 0; i < array.count; i++) {
            if ([pointNum integerValue] >= [array[i] integerValue]) {
                pointNum2 = [NSString stringWithFormat:@"%ld万", i+1];
            }
        }
        pointNum = pointNum2;
    }
    
    NSString *integral2 = [NSString stringWithFormat:@"%@ 分", pointNum];
    //NSString *integral2 = [NSString stringWithFormat:@"%@ 分", @(currentNum)];
    
    NSMutableAttributedString *string2 = [[NSMutableAttributedString alloc] initWithString:integral2];
    [string2 addAttribute:NSForegroundColorAttributeName value:RGBA(231, 117, 119, 1) range:NSMakeRange(0, [integral2  rangeOfString:@"分"].location)];
    [string2 addAttribute:NSForegroundColorAttributeName value:[UIColor grayColor] range:NSMakeRange([integral2  rangeOfString:@"分"].location, 1)];
    [cell.integralLab setAttributedText:string2];
    
    cell.integralLab.hidden = NO;
    cell.signInBtn.hidden = YES;
    cell.signedInLab.hidden = NO;
    
    [self.engine getPointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
}

- (void)updatePointDidFailed:(NSString *)response
{
//    [TKUIUtil hiddenHUD];
    //[TKUIUtil alertInWindow:response withImage:nil];
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    NSString *string = response;
    [self showAlertWithMsg:string];
}

- (void)showAlertWithMsg:(NSString *)string
{
    UIView *promptView = [[UIView alloc] init];
    promptView.frame = CGRectMake(40, 180, 240, 75);
    promptView.backgroundColor = [UIColor whiteColor];
    promptView.layer.cornerRadius = 8;
    promptView.layer.masksToBounds = YES;
    promptView.layer.borderWidth = 1;
    promptView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    [self.view.window addSubview:promptView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 200, 18)];
    textLabel.numberOfLines = 0;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textAlignment = NSTextAlignmentCenter;
//    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(200, 50) lineBreakMode:NSLineBreakByCharWrapping];
     CGSize size = [string boundingRectWithSize:CGSizeMake(200, 50) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14.0f]} context:nil].size;
    textLabel.frame = CGRectMake(20, 20, 200, size.height);
    textLabel.text = string;
    [promptView addSubview:textLabel];
    
    [UIView animateWithDuration:0.2
                          delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         promptView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.8
                                               delay:0.8
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              promptView.alpha = 0;
                                          }
                                          completion:^(BOOL finished) {
                                          }
                          ];
                     }
     ];
    
}

#pragma mark - 分享赚积分
- (void)getPointDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    
    sharePoint = [[response safeObjectForKey:@"data"] safeObjectForKey:@"SharePoint"];
    
    if ([sharePoint intValue] == 0) {
        
        sharePoint = @"0";
    }
    
    if (!self.shadowView)
    {
        [self buildHideView];
    }
    
    NSInteger shareCount = 0;
    if (!self.shareView)
    {
        self.shareView = [[ZSTNewShareView alloc] init];
        self.shareView.backgroundColor = [UIColor whiteColor];
        
        if ([ZSTF3Preferences shared].qqKey && [[ZSTF3Preferences shared].qqKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].qqKey isEqualToString:@""] && [QQApi isQQSupportApi] && [QQApi isQQInstalled])
        {
            self.shareView.isHasQQ = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasQQ = NO;
        }
        
        if ([ZSTF3Preferences shared].wxKey && [[ZSTF3Preferences shared].wxKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].wxKey isEqualToString:@""] && [WXApi isWXAppSupportApi] && [WXApi isWXAppInstalled])
        {
            self.shareView.isHasWX = YES;
            shareCount += 2;
        }
        else
        {
            self.shareView.isHasWX = NO;
        }
        
        if ([ZSTF3Preferences shared].weiboKey && [[ZSTF3Preferences shared].weiboKey isKindOfClass:[NSString class]] && ![[ZSTF3Preferences shared].weiboKey isEqualToString:@""])
        {
            self.shareView.isHasWeiBo = YES;
            shareCount += 1;
        }
        else
        {
            self.shareView.isHasWeiBo = NO;
        }
        
        self.shareView.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.shareView.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        self.weiboSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.wxSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinPointNum"] integerValue];
        self.wxFriendSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiXinFriendsPointNum"] integerValue];
        self.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareQqPointNum"] integerValue];
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】地址：http://ci.pmit.cn/d/%@", nil),appDisplayName,[ZSTF3Preferences shared].ECECCID];
        self.shareView.shareString = shareString;
        self.shareView.qqSharePoint = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"ShareWeiBoPointNum"] integerValue];
        self.shareView.shareDelegate = self;
        NSInteger heights = shareCount / 4 + 1;
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, heights * 100);
        [ self.shareView createShareUI];
        [self.view addSubview: self.shareView];
    }
    
    self.shareView.isTodayWX = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXin"] integerValue] == 0 ? NO : YES;
    self.shareView.isTodayWXFriedn = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiXinFriends"] integerValue] == 0 ? NO : YES;
    self.shareView.isTodayWeiBo = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareWeiBo"] integerValue] == 0 ? NO : YES;
    self.shareView.isTodayQQ = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"isTodayShareQq"] integerValue] == 0 ? NO : YES;
    
    [self.shareView checkIsHasShare];
    
    
}

- (void)buildHideView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    self.shadowView.hidden = YES;
    
    UIView *darkView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    darkView.backgroundColor = [UIColor blackColor];
    darkView.alpha = 0.6;
    [self.shadowView addSubview:darkView];
    
    [self.view addSubview:self.shadowView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareViewDismiss:)];
    [self.shadowView addGestureRecognizer:tap];
    
}

- (void)getPointDidFailed:(NSString *)response
{
//    [TKUIUtil hiddenHUD];
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    [TKUIUtil alertInWindow:response withImage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [self.engine cancelAllRequest];
}

- (void)goToPersonInfo:(UITapGestureRecognizer *)tap
{
    if ([[ZSTF3Preferences shared].UserId isEqualToString:@""] || [[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""])
    {
        self.jumpTypeString = @"infoType";
        ZSTLoginController *loginVC = [[ZSTLoginController alloc] init];
        loginVC.delegate = self;
        loginVC.isFromSetting = YES;
        BaseNavgationController *navi = [[BaseNavgationController alloc] initWithRootViewController:loginVC];
        [self.navigationController presentViewController:navi animated:YES completion:^{
            
        }];
    }
    else
    {
        ZSTPersonInfoViewController *infoVC = [[ZSTPersonInfoViewController alloc] init];
        infoVC.infoDic = self.infoDic;
        self.isShareApp = YES;
        [self.navigationController pushViewController:infoVC animated:YES];
    }
}

- (void)showDefaultShare
{
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    NSString *shareString = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】地址：http://ci.pmit.cn/d/%@", nil),appDisplayName,[ZSTF3Preferences shared].ECECCID];
//    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"http://ci.pmit.cn/d/%@",[ZSTF3Preferences shared].ECECCID]];
    NSURL *URL = [NSURL URLWithString:@""];
    UIActivityViewController *activityViewController =
    [[UIActivityViewController alloc] initWithActivityItems:@[shareString, URL]
                                      applicationActivities:nil];
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToFacebook]; 
    [self.navigationController presentViewController:activityViewController
                                       animated:YES
                                     completion:^{
                                         // ... 
                                     }];
}

- (void)showSNSShare
{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    
    if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        NSString *sharebaseurl = [NSString stringWithFormat:@"%@/%@",NSLocalizedString(@"GP_shareUrl", nil),[ZSTF3Preferences shared].ECECCID];
        picker.body = [NSString stringWithFormat:NSLocalizedString(@"GP_SMSRecommendMessage", nil),appDisplayName,sharebaseurl];
        
        picker.messageComposeDelegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
    } else {
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
    }
    [ZSTLogUtil logUserAction:NSStringFromClass([MFMessageComposeViewController class])];
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        
        //        if (![ZSTF3Preferences shared].smsShare) {
        //            [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"推荐好友短信发送成功", nil)];
        //            [TKUIUtil showHUD:self.view];
        //            [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
        //                                        userId:[ZSTF3Preferences shared].UserId
        //                                      pointNum:10
        //                                     pointType:@"4"
        //                                    costAmount:@""
        //                                          desc:@""];
        //        }
        //        
        //        [ZSTF3Preferences shared].smsShare = YES;
    }
}

- (void)shareViewDismiss:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)dismissShareView
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.shareView.frame = CGRectMake(0, HEIGHT, WIDTH, self.shareView.bounds.size.height);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)wxShareSucceed:(NSNotification *)notification
{
    self.activityView.hidden = NO;
    [self.activityView startAnimating];

    if ([ZSTWXShareType shareInstance].isWXShare)
    {
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.wxSharePoint
                                 pointType:6
                                costAmount:@""
                                      desc:@""];
    }
    else
    {
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.wxFriendSharePoint
                                 pointType:7
                                costAmount:@""
                                      desc:@""];
    }
    
}

- (void)wxShareFaild:(NSNotification *)notification
{
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
}

- (void)sendWeiboSuccess:(NSNotification *)notification
{
    self.activityView.hidden = NO;
    [self.activityView startAnimating];
    [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                userId:[ZSTF3Preferences shared].UserId
                              pointNum:self.weiboSharePoint
                             pointType:5
                            costAmount:@""
                                  desc:@""];
}

- (void)uploadFileDidSucceed:(NSDictionary *)response
{
    
    NSString *iconurl = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Icon"];
    
    NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Icon":iconurl};
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    [self.engine changeUserInfoWithParam:param];
}

- (void)uploadFileDidFailed:(NSString *)respone
{
//    [TKUIUtil hiddenHUD];
    [self.activityView stopAnimating];
    self.activityView.hidden = YES;
    [TKUIUtil alertInWindow:respone withImage:nil];
}

- (void)sendQQShareSuccess:(NSNotification *)notification
{
    [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                userId:[ZSTF3Preferences shared].UserId
                              pointNum:self.weiboSharePoint
                             pointType:8
                            costAmount:@""
                                  desc:@""];
}

- (void)sendQQShareFail:(NSNotification *)notification
{
    
}

- (void)sendWeiBoInfoFailured
{
    
}

- (void)loginDidFinish
{
    self.hasNext = YES;
    [self.engine getUserInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
}


- (void)goToNextStep
{
    if ([self.jumpTypeString isEqualToString:@"loginType"])
    {
        ZSTAccountManagementVC *controller = [[ZSTAccountManagementVC alloc] init];
        controller.passwordStatus = passwordStatus;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if ([self.jumpTypeString isEqualToString:@"shareType"])
    {
        [UIView animateWithDuration:0.3f animations:^{
            
            self.shadowView.hidden = NO;
            self.shareView.frame = CGRectMake(0, HEIGHT - self.shareView.frame.size.height, WIDTH, self.shareView.frame.size.height);
            
        } completion:^(BOOL finished) {
            self.isShareApp = YES;
        }];
    }
    else if ([self.jumpTypeString isEqualToString:@"settingType"])
    {
        ZSTSettingViewController *settingVC = [[ZSTSettingViewController alloc] init];
        BOOL isPublic = NO;
        if (userInfo.phonePublic == 0)
        {
            isPublic = NO;
        }
        else
        {
            isPublic = YES;
        }
        
        settingVC.isPhonePublic = isPublic;
        [self.navigationController pushViewController:settingVC animated:YES];
    }
    else if ([self.jumpTypeString isEqualToString:@"infoType"])
    {
        ZSTPersonInfoViewController *infoVC = [[ZSTPersonInfoViewController alloc] init];
        infoVC.infoDic = self.infoDic;
        self.isShareApp = YES;
        [self.navigationController pushViewController:infoVC animated:YES];
    }
    
    self.hasNext = NO;
    
}

- (void)thirdLoginSuccess:(NSNotification *)notification
{
    self.hasNext = YES;
    [self.engine getUserInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
}

@end
