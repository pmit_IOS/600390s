//
//  ZSTPersonalVariableCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalVariableCell.h"

@implementation ZSTPersonalVariableCell

- (void)awakeFromNib
{
    _rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
    _rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:_rightImgView];
    [_rightImgView release];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
