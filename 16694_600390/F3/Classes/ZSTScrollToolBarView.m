//
//  ScrollToolBarView.m
//  F3
//
//  Created by xuhuijun on 11-11-14.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTScrollToolBarView.h"
#import <QuartzCore/QuartzCore.h>

@implementation ZSTScrollToolBarView

@synthesize isTakeBack = _isTakeBack;
@synthesize delegate = _delegate;
@synthesize items = _items;
@synthesize imageItems = _imageItems;


-(void)reclock
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollToolBarIsTakeBack) object:nil];
    [self performSelector:@selector(scrollToolBarIsTakeBack) withObject:nil afterDelay:5];
}

- (void)stopClock
{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollToolBarIsTakeBack) object:nil];
}

-(void)recoverAlpha
{
    
    for (PMRepairButton *btn in self.subviews) {
        if ([btn isKindOfClass:[PMRepairButton class]]) {
            [btn removeFromSuperview];
        }
    }    

//    _pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _pushButton = [[PMRepairButton alloc] init];
    _pushButton.frame = CGRectMake(0, 0, 64, intervalHeght);
    _pushButton.imageEdgeInsets = UIEdgeInsetsMake(5, -15, 5, 19);
    _pushButton.showsTouchWhenHighlighted = YES;
    [_pushButton addTarget:self action:@selector(pushOut) forControlEvents:UIControlEventTouchUpInside];
    [_pushButton setImage:[UIImage imageNamed:@"btn_webicon_toleft.png"] forState:UIControlStateNormal];
    [_pushButton setImage:[UIImage imageNamed:@"btn_webicon_toleft_light.png"] forState:UIControlStateHighlighted];
    [self addSubview:_pushButton];//第一位


    self.alpha = _alpha;
}

-(void)pushOut
{  
    
    intervalWidth = self.frame.size.width/([self.items count]+1);
    
    NSUInteger countOfItems = ([self.items count]+1);
    
    double widthCoefficient = 1.0;
    
    if (countOfItems > 5) {
        
        widthCoefficient = (countOfItems - 5)*0.6;
    }
    
    [_pushButton removeFromSuperview];  
//    _pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _pushButton = [[PMRepairButton alloc] init];
    _pushButton.frame = CGRectMake(intervalWidth*[self.items count], 0, intervalWidth, intervalHeght);
    _pushButton.imageEdgeInsets = UIEdgeInsetsMake(5, 12*widthCoefficient, 5, 12*widthCoefficient);
    [_pushButton addTarget:self action:@selector(pushIn) forControlEvents:UIControlEventTouchUpInside];
    [_pushButton setImage:[UIImage imageNamed:@"btn_webicon_toright.png"] forState:UIControlStateNormal];
    [_pushButton setImage:[UIImage imageNamed:@"btn_webicon_toright_light.png"] forState:UIControlStateHighlighted];
    [self addSubview:_pushButton];//最后一位
    
    for (int i = 0; i < [self.items count]; i ++) {
        UIImageView *iconIV = [[[UIImageView alloc] initWithFrame:CGRectMake(intervalWidth * i, 0, intervalWidth, intervalHeght)] autorelease];
        iconIV.contentMode = UIViewContentModeScaleAspectFit;
        iconIV.image = [UIImage imageNamed:self.imageItems[i]];
        [self addSubview:iconIV];
        
        PMRepairButton *btn = (PMRepairButton *)[self.items objectAtIndex:i];
        btn.frame = CGRectMake(intervalWidth * i, 0, intervalWidth, intervalHeght);
        [self addSubview:btn];
    }

    if ([_delegate respondsToSelector:@selector(clickedPushOut)])
	{
		[_delegate clickedPushOut];
	}
    
    _isTakeBack = NO;

    [self performSelector:@selector(scrollToolBarIsTakeBack) withObject:nil afterDelay:5];
    
}
- (void)automaticBack
{
    
    if (!self.isTakeBack) {
        
        if ([_delegate respondsToSelector:@selector(clickedPushIn)])
        {
            [_delegate clickedPushIn];
        }

        [UIView beginAnimations:@"scrollToolBarIn" context:nil];
        [UIView setAnimationDuration:0.6];
        self.alpha = 0.3;
        [UIView commitAnimations];
        [self performSelector:@selector(recoverAlpha) withObject:nil afterDelay:0.6];
        
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(scrollToolBarIsTakeBack) object:nil];
        
    }
    
    self.isTakeBack = YES;
}

-(void)scrollToolBarIsTakeBack
{
    if (!self.isTakeBack) {
        [self automaticBack];
    }
}


-(void)pushIn
{
    
    [self automaticBack];
}

- (void)dealloc
{
    self.items = nil;
    [super dealloc];
}

-(id)initWithFrame:(CGRect)frame  color:(UIColor *)customColor  alpha:(CGFloat)alpha 
{
    self = [super initWithFrame:frame];
    if (self) {
        
        _isTakeBack = NO;

        self.backgroundColor = customColor;
        self.alpha = alpha;
        _alpha = alpha; 
        
        CALayer * layer = [self layer];
        [layer setMasksToBounds:YES];
        [layer setCornerRadius:3.0];
        [layer setBorderWidth:1.0];
        [layer setBorderColor:[[UIColor clearColor] CGColor]];

         intervalHeght = frame.size.height;

//        _pushButton = [UIButton buttonWithType:UIButtonTypeCustom];
        _pushButton = [[PMRepairButton alloc] init];
        _pushButton.frame = CGRectMake(0, 0, 64, frame.size.height);
        _pushButton.imageEdgeInsets = UIEdgeInsetsMake(5, -15, 5, 19);
        _pushButton.showsTouchWhenHighlighted = YES;
        [_pushButton addTarget:self action:@selector(pushOut) forControlEvents:UIControlEventTouchUpInside];
        [_pushButton setImage:[UIImage imageNamed:@"btn_webicon_toleft.png"] forState:UIControlStateNormal];
        [_pushButton setImage:[UIImage imageNamed:@"btn_webicon_toleft_light.png"] forState:UIControlStateHighlighted];
        [self addSubview:_pushButton];//第一位

    }
    return self;
}


@end

