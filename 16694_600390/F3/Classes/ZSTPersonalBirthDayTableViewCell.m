//
//  ZSTPersonalBirthDayTableViewCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-31.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalBirthDayTableViewCell.h"

@implementation ZSTPersonalBirthDayTableViewCell

- (void)awakeFromNib
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didBeginEditing:)
                                                 name:UITextFieldTextDidBeginEditingNotification
                                               object:_contentField];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(textDidChange:)
                                                 name:UITextFieldTextDidChangeNotification
                                               object:_contentField];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didEndEditing:)
                                                 name:UITextFieldTextDidEndEditingNotification
                                               object:_contentField];
    
    UIImageView *rightImgView = [[UIImageView alloc] initWithFrame:CGRectMake(285, (self.frame.size.height - 12)/2.0, 7, 12)];
    rightImgView.image = ZSTModuleImage(@"module_setting_icon_right.png");
    [self addSubview:rightImgView];
    [rightImgView release];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) textDidChange:(NSNotification*)notification {
    if ([_delegate respondsToSelector:@selector(textFieldCellTextDidChange:)]) {
        [_delegate textFieldCellTextDidChange:self];
    }
}

- (void) didBeginEditing:(NSNotification*)notification {
    // The text field intercepts the touch event so we need to select the row manually
    // This does not trigger the -didSelectRow delegate method though
    UITableView* tableView = (UITableView*)self.superview;
    if (IS_IOS_7) {
        tableView = (UITableView*)self.superview.superview;
    }
    [tableView selectRowAtIndexPath:[tableView indexPathForCell:self]
                           animated:YES
                     scrollPosition:UITableViewScrollPositionNone];
    
    if ([_delegate respondsToSelector:@selector(textFieldCellDidBeginEditing:)]) {
        [_delegate textFieldCellDidBeginEditing:self];
    }
}

- (void) didEndEditing:(NSNotification*)notification {
    if ([_delegate respondsToSelector:@selector(textFieldCellDidEndEditing:)]) {
        [_delegate textFieldCellDidEndEditing:self];
    }
}

@end
