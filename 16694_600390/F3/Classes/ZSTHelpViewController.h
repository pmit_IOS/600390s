//
//  HelpViewController.h
//  F3
//
//  Created by xuhuijun on 11-12-22.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

extern NSString *const  HelpViewControllerDidDismissNotification;

@class ZSTHelpViewController;

@protocol ZSTHelpViewControllerDelegate <NSObject>

@optional

- (void)helpViewControllerDidDismiss:(ZSTHelpViewController *)helpViewController;

@end

@interface ZSTHelpViewController : NSObject<UIScrollViewDelegate>
{
    UIWindow *_window;
    id _delegate;
}

@property (nonatomic, assign) id<ZSTHelpViewControllerDelegate> delegate;

SINGLETON_INTERFACE(ZSTHelpViewController);

- (void)showWithImages:(NSArray *)images;

- (void)dismissAnimated:(BOOL)animated;

@end
