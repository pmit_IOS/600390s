//
//  ColumnBar.h
//  ColumnBarDemo
//
//  Created by chenfei on 4/12/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum _topBarType
{
    TopBarType_SelectedBlock = 0,
    TopBarType_Slider = 1
    
} TopBarType;


// 按钮之间的间距
#define kSpace 5
#define kColumnWidth 46.333333

@protocol ColumnBarDataSource;
@protocol ColumnBarDelegate;

@interface ColumnBar : UIImageView <UIScrollViewDelegate> {
    UIScrollView    *scrollView;
    UIImageView     *leftCap;
    UIImageView     *rightCap;
    UIImageView     *mover;
    
    NSInteger             selectedIndex;
    
    id              dataSource;
    id              delegate;
}

@property(nonatomic, retain) UIScrollView               *scrollView;
@property(nonatomic, retain) UIImage                    *leftCapImage;
@property(nonatomic, retain) UIImage                    *rightCapImage;
@property(nonatomic, retain) UIImage                    *moverImage;

@property(nonatomic, retain) UIColor                    *selectedColor;
@property(nonatomic, retain) UIColor                    *unSelectedColor;

@property(nonatomic, assign) NSInteger                        selectedIndex;

@property(nonatomic, assign) id<ColumnBarDataSource>    dataSource;
@property(nonatomic, assign) id<ColumnBarDelegate>      delegate;
@property(nonatomic, assign) TopBarType      topbarType;


- (void)reloadData;
- (void)selectTabAtIndex:(int)index;

@end

@protocol ColumnBarDataSource <NSObject>

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar;//tab的数量
- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(int)index;// tab的名称


@optional
- (UIImage *)columnBar:(ColumnBar *)columnBar selectImageForTabAtIndex:(int)index;// tab 的背景图片和选中图片(没有mover 的情况下)
- (int)columnWidthOfTabsInColumnBar:(ColumnBar *)columnBar;//tab的间距


@end

@protocol ColumnBarDelegate <NSObject>

@optional
- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index;

@end
