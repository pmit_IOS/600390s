//
//  ECMobileClientParams.m
//  F3
//
//  Created by xuhuijun on 12-4-17.
//  Copyright 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTECMobileClientParams.h"


@implementation ZSTECMobileClientParams

@synthesize MCRegistType;
@synthesize MCRegistDescription;
@synthesize MCRegistChangeDescription;
@synthesize IPPushFlag;
@synthesize HTTPPollTimeRule;
@synthesize SMSHookFlag;

-(NSString *) description
{
	NSMutableString *desc = [[NSMutableString alloc] initWithFormat:
                             @"MCRegistType = %d, MCRegistDescription = %@, MCRegistChangeDescription = %@, IPPushFlag = %d,  HTTPPollTimeRule = %@, SMSHookFlag = %d",
                             self.MCRegistType,MCRegistDescription,MCRegistChangeDescription,IPPushFlag,HTTPPollTimeRule,SMSHookFlag];
	return [desc autorelease];
}

- (void)dealloc
{
    self.MCRegistDescription = nil;
    self.MCRegistChangeDescription = nil;
    self.HTTPPollTimeRule = nil;
    [super dealloc];
}
@end
