//
//  ZSTModulController.h
//  F3
//
//  Created by luobin on 7/10/12.
//  Copyright (c) 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTModule.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@interface ZSTModuleBaseViewController : UIViewController<ZSTModuleDelegate>
{
    NSString *iconImageName;
    NSString *tabTitle;
    NSString *titleColor;
}

@property (nonatomic, retain)  NSString *iconImageName;
@property (nonatomic, retain)  NSString *tabTitle;
@property (nonatomic, retain)  NSString *titleColor;
@property (assign, readonly) UIView *rootView;

@end
