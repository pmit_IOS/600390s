//
//  PMMembersBalanceCell.m
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersBalanceCell.h"

@implementation PMMembersBalanceCell

- (void)createBalanceUI
{
    if (!self.timeLabel) {
        
        // 时间
        self.timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(260), 30)];
        self.timeLabel.backgroundColor= [UIColor clearColor];
        self.timeLabel.textColor = [UIColor blackColor];
        self.timeLabel.font = [UIFont systemFontOfSize:15.0f];
        self.timeLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.timeLabel];
        
        // 金额
        self.moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(470), 5, WidthRate(260), 30)];
        self.moneyLabel.backgroundColor= [UIColor clearColor];
        self.moneyLabel.font = [UIFont systemFontOfSize:15.0f];
        self.moneyLabel.textAlignment = NSTextAlignmentCenter;
        [self.contentView addSubview:self.moneyLabel];
        
        // 订单
        self.orderLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 40, WidthRate(560), 30)];
        self.orderLabel.backgroundColor= [UIColor clearColor];
        self.orderLabel.textColor = [UIColor blackColor];
        self.orderLabel.font = [UIFont systemFontOfSize:15.0f];
        self.orderLabel.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.orderLabel];
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(15, self.contentView.frame.size.height - 1.0f, WIDTH - 15, 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [self.contentView.layer addSublayer:layer];
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

- (void)setBalanceDataDict:(NSDictionary *)detailDict
{
    // 时间
    NSString *timeSting = [detailDict safeObjectForKey:@"createdOn"];
    self.timeLabel.text = [timeSting substringFromIndex:5];
    
    if ([[detailDict safeObjectForKey:@"type"] integerValue] == 1) {
        
        self.moneyLabel.textColor = RGBA(255, 106, 106, 1);
    }
    else
    {
        self.moneyLabel.textColor = [UIColor grayColor];
    }
    
    // 金额
    self.moneyLabel.text = [[detailDict safeObjectForKey:@"type"] integerValue] == 1 ? [NSString stringWithFormat:@"+ %.2lf", [[detailDict safeObjectForKey:@"chargeamount"] doubleValue]] : [NSString stringWithFormat:@"- %.2lf", [[detailDict safeObjectForKey:@"chargeamount"] doubleValue]];
    
    // 订单
    self.orderLabel.text = [NSString stringWithFormat:@"订单号: %@", [detailDict safeObjectForKey:@"serialNo"]];
}


@end
