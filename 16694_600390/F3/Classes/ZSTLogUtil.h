//
//  ZSTLogUtil.h
//  F3
//
//  Created by 9588 on 11/2/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


extern NSString * const LogType_UserAction;
extern NSString * const LogType_UserView;
extern NSString * const LogType_SysConnect;
extern NSString * const LogType_SysIPPush;
extern NSString * const LogType_SysError;
extern NSString * const LogType_SysInfo;

extern NSString * const IPPush_StartLogin;
extern NSString * const IPPush_LoginFail;
extern NSString * const IPPush_Success;
extern NSString * const IPPush_GetMsg;
extern NSString * const IPPush_Logout;

@interface ZSTLogUtil : NSObject

//记录日志
+(void)log:(NSString *)description type:(NSString *)logType;


/**
 *	@brief	记录用户行为日志
 *
 *	@param 	viewClassName 	查看的类
 */
+(void)logUserAction:(NSString *)viewClassName;


/**
 *	@brief	记录用户行为日志
 *
 *	@param 	viewName 	查看的视图名称
 *	@param 	pushId      pushId
 *	@param 	MSGID       MSGID
 */
+(void)logUserView:(NSString *)viewName pushId:(NSString *)pushId messageId:(NSString *)MSGID;


/**
 *	@brief	记录系统联网日志
 *
 *	@param 	url 	网络请求的url
 *	@param 	result 	联网结果
 *	@param 	response 	联网结果描述
 */
+(void)logSysConnect:(NSURL *)url result:(BOOL)result response:(NSString *)response;


/**
 *	@brief	记录ippush日志
 *
 *	@param 	type IPPush操作类型，可取值为IPPush_StartLogin, IPPush_LoginFail, IPPush_Success, IPPush_GetMsg, IPPush_Logout
 */
+(void)logSysIPPush:(NSString *)type;

/**
 *	@brief	记录系统错误日志
 *
 *	@param 	error 	错误描述
 */
+(void)logSysError:(NSString *)error;

/**
 *	@brief	记录系统信息
 *
 *	@param 	info 	信息描述
 */
+(void)logSysInfo:(NSString *)info;

//获取指定日期之前的用户日志
+(NSArray *)getUserLogsBeforeDate:(NSDate *)date;

//删除指定日期之前的用户日志
+(void)deleteUserLogsBeforeDate:(NSDate *)date;

//删除指定日期之前的系统日志
+(void)deleteSysLogsBeforeDate:(NSDate *)date;

@end
