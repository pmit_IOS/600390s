//
//  PMRechargeMoneyView.h
//  F3
//
//  Created by P&M on 15/9/15.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol PMRechargeMoneyViewDelegate <NSObject>

- (void)changeRechargeMoney:(NSString *)rechargeMoney changeRechargeId:(NSString *)rechargeId;
- (void)sureRechargeMoneySelected;
- (void)cancelRechargeMoneySelected;

@end

@interface PMRechargeMoneyView : UIView <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) id<PMRechargeMoneyViewDelegate> rechargeDelegate;

@property (strong, nonatomic) UIPickerView *rechargePickerView;

@property (strong, nonatomic) NSMutableArray *objectArray;
@property (strong, nonatomic) NSMutableArray *rechargeIdArray;

- (void)createRechargeMoneyPicker;

@end
