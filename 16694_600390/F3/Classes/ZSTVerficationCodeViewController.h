//
//  VerficationCodeView.h
//  F3
//
//  Created by xuhuijun on 11-10-20.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "ZSTRegisterViewController.h"

@interface ZSTVerficationCodeViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,ZSTF3EngineDelegate>
{
    UITableView *_tableView;
    UILabel *_phoneNumberLabel;
    UILabel *_intervarlLabel;

    BOOL _isOK;//判断是否绑定成功
//    BOOL _inObtainVerficationCode;
    
    UIButton *_re_sendButton;//重发验证码按钮
    
    id<RegisterDelegate> _delegate;
    
}
@property(nonatomic,assign)id<RegisterDelegate> delegate;
@property(nonatomic,retain)UILabel *phoneNumberLabel;
@property(nonatomic,retain)UILabel *intervarlLabel;
@property(nonatomic) BOOL inObtainVerficationCode;
@property(nonatomic, retain) ZSTF3Engine *engine;

@end
