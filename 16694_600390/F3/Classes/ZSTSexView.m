//
//  ZSTSexView.m
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTSexView.h"

@implementation ZSTSexView

- (void)createSexPicker
{
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(sexViewDismiss:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipe];
    
    UIView *optionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    optionView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
    line.frame = CGRectMake(0, 40, WIDTH, 0.5);
    [optionView.layer addSublayer:line];
    
    NSString *tipString = @"请选择...";
//    CGSize tipStringSize = [tipString sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 20)];
    CGSize tipStringSize = [tipString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, tipStringSize.width, 20)];
    tipLB.font = [UIFont systemFontOfSize:12.0f];
    tipLB.textColor = RGBA(136, 136, 136, 1);
    tipLB.textAlignment = NSTextAlignmentLeft;
    tipLB.text = tipString;
    [optionView addSubview:tipLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 10 - 40, 10, 40, 20);
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureSexPicker:) forControlEvents:UIControlEventTouchUpInside];
    [sureBtn setTitleColor:RGBA(244, 125, 55, 1) forState:UIControlStateNormal];
    [optionView addSubview:sureBtn];
    [self addSubview:optionView];
    
    self.sexPickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    self.sexPickerView.delegate = self;
    self.sexPickerView.dataSource = self;
    self.sexPickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.sexPickerView.frame = CGRectMake(0, 30, WIDTH, 90);
    [self addSubview:self.sexPickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return 2;
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    if (row == 0)
//    {
//        return @"男";
//    }
//    else
//    {
//        return @"女";
//    }
//}
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 45;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *myLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 30)];
    myLB.font = [UIFont systemFontOfSize:15.0f];
    myLB.textAlignment = NSTextAlignmentCenter;
    myLB.textColor = RGBA(43, 43, 43, 1);
    
    if (row == 0)
    {
        myLB.text = @"男";
    }
    else
    {
        myLB.text = @"女";
    }
    
    return myLB;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    UILabel *myLB = (UILabel *)[pickerView viewForRow:row forComponent:component];
    myLB.textColor = RGBA(244, 125, 55, 1);
    if ([myLB.text isEqualToString:@"男"])
    {
        self.selectedSexInteger = 1;
    }
    else
    {
        self.selectedSexInteger = 2;
    }
}

- (void)sureSexPicker:(UIButton *)sender
{
    if ([self.sexDelegate respondsToSelector:@selector(sureSexSelected:)])
    {
        [self.sexDelegate sureSexSelected:self.selectedSexInteger];
    }
}

- (void)showDefault
{
    if (self.defaultSexInteger == 1)
    {
        [self.sexPickerView selectRow:0 inComponent:0 animated:YES];
    }
    else if (self.defaultSexInteger == 2)
    {
        [self.sexPickerView selectRow:1 inComponent:0 animated:YES];
    }
    else
    {
        [self.sexPickerView selectRow:0 inComponent:0 animated:YES];
    }
}

- (void)sexViewDismiss:(UISwipeGestureRecognizer *)sender
{
    if ([self.sexDelegate respondsToSelector:@selector(cancelSexSelected)])
    {
        [self.sexDelegate cancelSexSelected];
    }
}

@end
