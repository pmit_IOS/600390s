//
//  ZSTAccountManagementVC.h
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTAccountManagementVC : UIViewController <UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *settingItems;
    NSMutableArray *accountItems;
    
    NSMutableArray *thirdAccount;
}

@property (nonatomic, retain) UITableView *tableView;

@property (nonatomic, assign) NSInteger passwordStatus;
@property (strong,nonatomic) ZSTF3Engine *engine;

@end
