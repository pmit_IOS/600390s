
//
//  ZSTLegicyCommunicator.h
//
//
//  Created by luobin on 12-3-8.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ASIHTTPRequest.h"

@class ASIHTTPRequest;
@class ASINetworkQueue;
@class ZSTLegacyResponse;

/*
 *
 *  支持早期xml接口网络请求封装类，新接口都已经采用json格式
 *
 */

@protocol ZSTLegicyCommunicatorDelegate <NSObject>
@optional
// Dictionary or array, default dictionary
- (void)requestDidSucceed:(ZSTLegacyResponse *)response;

- (void)requestDidFail:(ZSTLegacyResponse *)response;
@end

@interface ZSTLegicyCommunicator : NSObject {
@private
    ASINetworkQueue *networkQueue;
    NSMutableSet *uploadRequestsSet;
}

SINGLETON_INTERFACE(ZSTLegicyCommunicator);

@property (nonatomic, copy) NSArray *prefixParams;
@property (nonatomic, copy) NSString *baseURL;

// ############################ URL synchronous request methods ############################

-(ZSTLegacyResponse *)openSynAPIGetToMethod:(NSString *)method
                                     params:(NSDictionary *)params
                             timeOutSeconds:(NSTimeInterval)timeOutSeconds;

-(BOOL)downloadFileSynToMethod:(NSString *)method
                        params:(NSDictionary *)params
               destinationFile:(NSString *)destinationFile
                timeOutSeconds:(NSTimeInterval)timeOutSeconds;

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                                        body:(NSData *)body
                              timeOutSeconds:(NSTimeInterval)timeOutSeconds;

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                              replacedParams:(NSDictionary *)params;

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                              replacedParams:(NSDictionary *)params
                              timeOutSeconds:(NSTimeInterval)timeOutSeconds;

-(ZSTLegacyResponse *)openSynAPIPostToMethod:(NSString *)method
                                    Template:(NSString *)Template
                              replacedParams:(NSDictionary *)params
                              timeOutSeconds:(NSTimeInterval)timeOutSeconds;

// ############################ URL request methods ############################

/**
 *	@brief
 *
 *	@param 	method
 *	@param 	template            模板名称
 *	@param 	params              要替换的参数
 *	@param 	delegate            代理对象
 *	@param 	flag                是否异步，末认为yes
 *	@param 	failSelector        失败回调，默认为requestDidFail:(TKResponse *)response
 *	@param 	succeedSelector 	成功回调，默认为requestDidSucceed:(TKResponse *)response
 *	@param 	userInfo            额外信息，会在failSelector和succeedSelector中
 */
- (void)openAPIPostToMethod:(NSString *)method
                   Template:(NSString *)Template
             replacedParams:(NSDictionary *)params
               asynchronous:(BOOL)flag
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)userInfo;

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
               asynchronous:(BOOL)flag
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)userInfo;


/* Open API post信息请求接口 */
// delegate: TKCommunicatorDelegate
- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
               failSelector:(SEL)failSelector
            succeedSelector:(SEL)succeedSelector
                   userInfo:(id)userInfo;

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
               asynchronous:(BOOL)flag
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
                   userInfo:(id)userInfo;

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
                   userInfo:(id)userInfo;

- (void)openAPIPostToMethod:(NSString *)method
             replacedParams:(NSDictionary *)params
                   delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate;



- (ZSTLegacyResponse *)uploadFileSynToMethod:(NSString *)method
                                        path:(NSString *)path
                                      params:(NSDictionary *)params
                            progressDelegate:(id)progress;

- (ZSTLegacyResponse *)uploadFileSynToMethod:(NSString *)method
                                        data:(NSData *)data
                                      params:(NSDictionary *)params
                            progressDelegate:(id)progress;


/* 文件上传接口 */
// Path is whole path, contain the image name
// e.g. : "/image/to/path/name.jpg"
- (void)uploadFileToMethod:(NSString *)method
                      path:(NSString *)path
                    params:(NSDictionary *)params
                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
          progressDelegate:(id)progress; // progressDelegate is an UIProgressView, if needn't please set nil

// upload file data
- (void)uploadFileToMethod:(NSString *)method
                      data:(NSData *)data
                    params:(NSDictionary *)params
                  delegate:(id<ZSTLegicyCommunicatorDelegate>)delegate
          progressDelegate:(id)progress; // progressDelegate is an UIProgressView, if needn't please set nil

// ############################ URL request methods ############################

//
/**
 *	@brief	取消请求.只能取消异步请求
 *
 *	@param 	delegate
 */
- (void)cancelByDelegate:(id)delegate;

- (void)cancelUploadByDelegate:(id)delegate;

@end

#pragma mark -

@interface TKCommunicatorUserInfo : NSObject {
    id delegate;
    SEL failSelector;
    // TODO: uid should change name to requestid
    NSString *uid;
    SEL succeedSelector;
    id userInfo;
}

@property (nonatomic, assign) id delegate;
@property (nonatomic, assign) SEL failSelector;
@property (nonatomic, assign) SEL succeedSelector;
@property (nonatomic, retain) id userInfo;
@end

