//
//  ZSTLocationView.m
//  F3
//
//  Created by pmit on 15/8/29.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTLocationView.h"

@implementation ZSTLocationView

- (void)createUI
{
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"area.json" ofType:nil];
    NSData *data = [NSData dataWithContentsOfFile:path];
    self.jsonDic =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
    NSLog(@"json --> %@",self.jsonDic);
    
    NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
    NSArray *provinceKeyArr = [provinceDic allKeys];
    self.province = [provinceKeyArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        NSInteger num1 = [obj1 integerValue];
        NSInteger num2 = [obj2 integerValue];
        if (num1 > num2)
        {
            return 1;
        }
        return -1;
    }];
    NSString *firstKey = [self.province firstObject];
    self.city = [[self.jsonDic objectForKey:@"area1"] objectForKey:firstKey];
    
    self.backgroundColor = [UIColor whiteColor];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipe];
    
    UIView *optionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    optionView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
    line.frame = CGRectMake(0, 40, WIDTH, 0.5);
    [optionView.layer addSublayer:line];
    
    NSString *tipString = @"请选择...";
    //    CGSize tipStringSize = [tipString sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 20)];
    CGSize tipStringSize = [tipString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, tipStringSize.width, 20)];
    tipLB.font = [UIFont systemFontOfSize:12.0f];
    tipLB.textColor = RGBA(136, 136, 136, 1);
    tipLB.textAlignment = NSTextAlignmentLeft;
    tipLB.text = tipString;
    [optionView addSubview:tipLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 10 - 40, 10, 40, 20);
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:RGBA(244, 125, 55, 1) forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureBirthPicker:) forControlEvents:UIControlEventTouchUpInside];
    [optionView addSubview:sureBtn];
    [self addSubview:optionView];
    
    self.locationPickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    self.locationPickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.locationPickerView.frame = CGRectMake(0, 30, WIDTH, 162);
    self.locationPickerView.delegate = self;
    self.locationPickerView.dataSource = self;
    [self addSubview:self.locationPickerView];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return [self.province count];
    }
    else
    {
        return [self.city count];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *myLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 30)];
    myLB.font = [UIFont systemFontOfSize:15.0f];
    myLB.textAlignment = NSTextAlignmentCenter;
    NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
    
    if (component == 0)
    {
        NSString *pKey = [self.province objectAtIndex:row];
        myLB.text = [provinceDic objectForKey:pKey];
        
    }
    else if (component == 1)
    {
        myLB.text = [NSString stringWithFormat:@"%@",[self.city[row] lastObject]];
    }
    
    
    return myLB;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
    if (component == 0)
    {
        NSString *key = [self.province objectAtIndex:row];
        self.selectedProvice = [provinceDic safeObjectForKey:key];
        self.city = [[self.jsonDic objectForKey:@"area1"] objectForKey:key];
        NSArray *cityFirstArr = [self.city firstObject];
        self.selectedCity = [cityFirstArr lastObject];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView reloadAllComponents];
    }
    else
    {
        self.selectedCity = [[self.city objectAtIndex:row] lastObject];
    }
}

- (void)setDefaultData
{
    
    NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
    NSInteger defaultProvinceIndex = -1;
    NSString *defaultProvinceKey = @"";
    
    if ([self.nowCity isEqualToString:@"未设置"] || [self.nowCity rangeOfString:@"-"].length == 0)
    {
        self.selectedProvice = @"北京";
        self.selectedCity = @"北京市";
        [self.locationPickerView selectRow:0 inComponent:0 animated:YES];
        [self.locationPickerView selectRow:0 inComponent:1 animated:YES];
    }
    else
    {
        NSArray *cityArr = [self.nowCity componentsSeparatedByString:@"-"];
        NSString *provinceString = cityArr[0];
        NSString *cityString = cityArr[1];
        
        self.selectedProvice = provinceString;
        self.selectedCity = cityString;
        
        for (NSInteger i = 0; i < self.province.count; i++)
        {
            NSString *provinceKey = [self.province objectAtIndex:i];
            NSString *provinceName = [provinceDic objectForKey:provinceKey];
            if ([provinceString isEqualToString:provinceName])
            {
                defaultProvinceIndex = i;
                defaultProvinceKey = provinceKey;
                break;
            }
        }
        
        self.city = [[self.jsonDic objectForKey:@"area1"] objectForKey:defaultProvinceKey];
        NSInteger defaultCityIndex = -1;
        for (NSInteger i = 0 ; i < self.city.count; i++)
        {
            NSString *cityName = [[self.city objectAtIndex:i] lastObject];
            if ([cityString isEqualToString:cityName])
            {
                defaultCityIndex = i;
            }
        }
        
        [self.locationPickerView selectRow:defaultProvinceIndex inComponent:0 animated:YES];
        [self.locationPickerView reloadComponent:1];
        [self.locationPickerView selectRow:defaultCityIndex inComponent:1 animated:YES];
        
    }
    
    
}

- (void)pickerViewDismiss:(UISwipeGestureRecognizer *)swiper
{
    if ([self.locationDelegate respondsToSelector:@selector(cancelLocationPickerView)])
    {
        [self.locationDelegate cancelLocationPickerView];
    }
}

- (void)sureBirthPicker:(UIButton *)sender
{
    if ([self.locationDelegate respondsToSelector:@selector(sureLocationSelected:CellType:)])
    {
        NSString *locationString = [NSString stringWithFormat:@"%@-%@",self.selectedProvice,self.selectedCity];
        [self.locationDelegate sureLocationSelected:locationString CellType:self.isHometown];
    }
}

@end
