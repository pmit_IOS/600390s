//
//  ZSTPersonalCenterCell.m
//  F3
//
//  Created by P&M on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalCenterCell.h"

@implementation ZSTPersonalCenterCell

- (void)createPersonalCenterUI
{
    if (!self.cellIma) {
        
        // 图片
        self.cellIma = [[UIImageView alloc] initWithFrame:CGRectMake(WidthRate(30), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(50), WidthRate(50))];
        self.cellIma.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.cellIma];
        
        // 标题
        self.cellTitle = [[UILabel alloc] initWithFrame:CGRectMake(self.cellIma.frame.origin.x + self.cellIma.frame.size.width + WidthRate(30), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(160), WidthRate(50))];
        self.cellTitle.backgroundColor = [UIColor clearColor];
        self.cellTitle.textAlignment = NSTextAlignmentLeft;
        self.cellTitle.font = [UIFont systemFontOfSize:15.0f];
        [self.contentView addSubview:self.cellTitle];
        
        // 显示会员卡等级
        self.membersLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(305), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(220), WidthRate(50))];
        self.membersLab.backgroundColor = [UIColor clearColor];
        self.membersLab.textColor = [UIColor grayColor];
        self.membersLab.textAlignment = NSTextAlignmentRight;
        self.membersLab.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:self.membersLab];
        self.membersLab.hidden = YES;
        
        // 积分
        self.integralLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(280), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(150), WidthRate(50))];
        self.integralLab.backgroundColor = [UIColor clearColor];
        self.integralLab.textAlignment = NSTextAlignmentLeft;
        self.integralLab.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:self.integralLab];
        self.integralLab.hidden = YES;
        
        // 签到按钮
//        self.signInBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        self.signInBtn = [[PMRepairButton alloc] init];
        self.signInBtn.frame = CGRectMake(WIDTH - WidthRate(225), (self.contentView.frame.size.height - WidthRate(53)) / 2, WidthRate(130), WidthRate(53));
        [self.signInBtn setImage:[UIImage imageNamed:@"module_personal_signIn.png"] forState:UIControlStateNormal];
        [self.contentView addSubview:self.signInBtn];
        self.signInBtn.hidden = YES;
        
        // 今日已签到
        self.signedInLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(245), (self.contentView.frame.size.height - WidthRate(53)) / 2, WidthRate(160), WidthRate(53))];
        self.signedInLab.backgroundColor = [UIColor clearColor];
        self.signedInLab.text = @"今日已签到";
        self.signedInLab.textColor = [UIColor grayColor];
        self.signedInLab.textAlignment = NSTextAlignmentCenter;
        self.signedInLab.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:self.signedInLab];
        self.signedInLab.hidden = YES;
        
        // 显示账号
        self.accountLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(305), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(220), WidthRate(50))];
        self.accountLab.backgroundColor = [UIColor clearColor];
        self.accountLab.textColor = [UIColor grayColor];
        self.accountLab.textAlignment = NSTextAlignmentRight;
        self.accountLab.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:self.accountLab];
        self.accountLab.hidden = YES;
        
        // 赚积分
        self.integralTip = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(245), (self.contentView.frame.size.height - WidthRate(50)) / 2, WidthRate(160), WidthRate(50))];
        self.integralTip.backgroundColor = [UIColor clearColor];
        self.integralTip.text = @"赚积分";
        self.integralTip.textColor = [UIColor grayColor];
        self.integralTip.textAlignment = NSTextAlignmentRight;
        self.integralTip.font = [UIFont systemFontOfSize:13.0f];
        [self.contentView addSubview:self.integralTip];
        self.integralTip.hidden = YES;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

@end
