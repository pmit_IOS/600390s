//
//  VerficationCodeView.m
//  F3
//
//  Created by xuhuijun on 11-10-20.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTVerficationCodeViewController.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"
#import "TKUIUtil.h"
#import "ZSTRegisterTimer.h"
#import "ZSTLogUtil.h"

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

@interface Custom_Verfication_TableViewCell: UITableViewCell
{
    UITextField *_numberTextField;
}

@property(nonatomic,retain) UITextField *numberTextField;

@end

@implementation Custom_Verfication_TableViewCell

@synthesize numberTextField = _numberTextField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        
        _numberTextField = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 280, 40)];
        _numberTextField.borderStyle = UITextBorderStyleNone;
        _numberTextField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        _numberTextField.font = [UIFont systemFontOfSize:20];
        [_numberTextField becomeFirstResponder];
        _numberTextField.backgroundColor = [UIColor clearColor];
        _numberTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        [self.contentView addSubview:_numberTextField];
		
    }
    return self;
}

@end

@implementation ZSTVerficationCodeViewController

@synthesize delegate = _delegate;
@synthesize phoneNumberLabel = _phoneNumberLabel;
@synthesize intervarlLabel = _intervarlLabel;
@synthesize inObtainVerficationCode = _inObtainVerficationCode;
@synthesize engine;

- (void)dealloc
{
    [_tableView release];
    [_intervarlLabel release];
    self.engine = nil;
    [super dealloc];
}
-(void)dismissView
{
    [ZSTF3Preferences shared].verficationCode = @"";
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [self performSelector:@selector(callbackAction) withObject:nil afterDelay:2.0f];
}

- (void) callbackAction{
    
    if ([_delegate respondsToSelector:@selector(registerDidSucceed)]) {
        [_delegate registerDidSucceed];
    }
}

-(void)back
{
    Custom_Verfication_TableViewCell *cell = (Custom_Verfication_TableViewCell *)[_tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    dataManager.verficationCode = cell.numberTextField.text; 
    
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)verificationPoint
{
    Custom_Verfication_TableViewCell *cell = (Custom_Verfication_TableViewCell *)[_tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    
    [cell.numberTextField resignFirstResponder];
    
    if (!([cell.numberTextField.text length] == 0)) {
        
        ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
        preferences.verficationCode = cell.numberTextField.text; 
        [preferences synchronize];
        
        self.navigationItem.rightBarButtonItem.enabled = NO;
        CGRect rect = self.view.frame;
        [TKUIUtil showHUDInView:self.view withText:nil withImage:nil withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
        [self.engine registMobileClient:preferences.phoneNumber checksum:preferences.verficationCode];
        
    }else{
        
        CGRect rect = self.view.frame;
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入验证码", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
        
    }
}

-(void)updateIntervalAction:(NSNotification *)notification
{
    int remainingTime = [[notification object] intValue];
    
    [_re_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _re_sendButton.userInteractionEnabled = NO;
    
    if (remainingTime <= 0) {
        _intervarlLabel.hidden = YES;
        _intervarlLabel.text = @"";
        _re_sendButton.userInteractionEnabled = YES;
        [_re_sendButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    }
    _intervarlLabel.text = [NSString stringWithFormat:@"( %d )",remainingTime];
}

-(void)reSendVerificationCodeAction
{
    [[ZSTRegisterTimer sharedTimer] reset];
    
    self.navigationItem.rightBarButtonItem.enabled = YES;
    CGRect rect = self.view.frame;
    [TKUIUtil showHUDInView:self.view withText:nil withImage:nil withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
    [self.engine getRegChecksum:[ZSTF3Preferences shared].phoneNumber];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.title = NSLocalizedString(@"验证手机号", nil);
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
    CGRect rect;
    if (TKIsPad()) {
        
        rect = CGRectMake(0, 0, 460, 501);
    } else {
        
        rect = CGRectMake(0, 0, 320, 416+(iPhone5?88:0));
    }
    
    _tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStyleGrouped];
    _tableView.scrollEnabled = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    
    UIView *tableHeaderView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 50)] autorelease];
    tableHeaderView.alpha = 0.5;
    tableHeaderView.backgroundColor = [UIColor lightGrayColor];
    
    UIImageView *confirmImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    confirmImage.image = [UIImage imageNamed:@"badge-check.png"];
    [tableHeaderView addSubview:confirmImage];
    [confirmImage release];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(50, 10, 150, 30)];
    label.text = NSLocalizedString(@"短信验证信息已经发送到:", nil);
    label.font = [UIFont systemFontOfSize:13];
    label.backgroundColor = [UIColor clearColor];
    [tableHeaderView addSubview:label];
    [label release];
    
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    
    _phoneNumberLabel = [[UILabel alloc] initWithFrame:CGRectMake(205, 9, 105, 30)];
    _phoneNumberLabel.text =dataManager.phoneNumber;
    _phoneNumberLabel.backgroundColor = [UIColor clearColor];
    [tableHeaderView addSubview:_phoneNumberLabel];
    
    
    UIView *tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 60)] autorelease];
    tableFooterView.alpha = 0.5;
    tableFooterView.backgroundColor = [UIColor clearColor];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(TKIsPad()? 30: 10, 0, 180, 30)];
    textLabel.text = NSLocalizedString(@"未收到验证码？你可以:", nil);
    textLabel.font = [UIFont systemFontOfSize:15];
    textLabel.backgroundColor = [UIColor clearColor];
    [tableFooterView addSubview:textLabel];
    [textLabel release];
    
    _re_sendButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _re_sendButton.frame = CGRectMake(TKIsPad()? 25: 10, 30, 100, 30);
    _re_sendButton.userInteractionEnabled = NO;
    [_re_sendButton setTitle:NSLocalizedString(@"重发验证码", nil) forState:UIControlStateNormal];
    [_re_sendButton addTarget:self action:@selector(reSendVerificationCodeAction) forControlEvents:UIControlEventTouchUpInside];
    [tableFooterView addSubview:_re_sendButton];
    
    _intervarlLabel = [[UILabel alloc] initWithFrame:CGRectMake(115, 30, 55, 30)];
    _intervarlLabel.backgroundColor = [UIColor clearColor];
    _intervarlLabel.textAlignment = NSTextAlignmentCenter;
    
    _intervarlLabel.text = [NSString stringWithFormat:@"( %d )",[ZSTRegisterTimer sharedTimer].remainingTime];
    
    [tableFooterView addSubview:_intervarlLabel];
    
    _tableView.tableHeaderView = tableHeaderView;
    _tableView.tableFooterView = tableFooterView;
    [self.view addSubview:_tableView];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (back)];

    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"绑定", nil) target:self selector:@selector (verificationPoint)];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(updateIntervalAction:) 
                                                 name: NotificationName_RemaingTimeChange 
                                               object: nil];
    
    [[ZSTRegisterTimer sharedTimer] start];
    
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTVerficationCodeViewController class])];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}

#pragma mark ------------------UITableViewDataSource---------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"CustomTableViewCell";
    
    Custom_Verfication_TableViewCell *cell = (Custom_Verfication_TableViewCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    if (!cell)
    {
        cell = [[[Custom_Verfication_TableViewCell alloc] initWithStyle: UITableViewCellStyleDefault 
                                                     reuseIdentifier: cellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
    cell.numberTextField.text = dataManager.verficationCode;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return nil;
}

#pragma mark ------------------UITableViewDelegate---------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - ZSTF3EngineDelegate

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    CGRect rect = self.view.frame;
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"验证失败!", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
    [TKUIUtil hiddenHUDAfterDelay:2];
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)registMobileClientDidResponse:(NSString *)loginMsisdn loginPassword:(NSString *)loginPassword
{
    [TKUIUtil hiddenHUD];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self dismissView];
    
    if ([_delegate respondsToSelector:@selector(registerDidFinish)]) {
        [_delegate registerDidFinish];
    } else {
        
        CGRect rect = self.view.frame;
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"恭喜您绑定成功!", nil) withImage:[UIImage imageNamed:@"icon_smile_face.png"] withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
        [TKUIUtil hiddenHUDAfterDelay:2];
    }
}

- (void)getRegChecksumResponse
{
    [TKUIUtil hiddenHUD];
    [ZSTF3Preferences shared].verficationCode = @"";
    
    _re_sendButton.userInteractionEnabled = NO;
    _intervarlLabel.text = [NSString stringWithFormat:@"( %d )",[ZSTRegisterTimer sharedTimer].remainingTime];
    [_re_sendButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    _intervarlLabel.hidden = NO;
}
@end
