//
//  Setview.m
//  Net_Information
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <AudioToolbox/AudioToolbox.h>
#import <QuartzCore/QuartzCore.h>

#import "ZSTSettingsViewController.h"
#import "ZSTSuggestedQuestionsViewController.h"
#import "ZSTAboutViewController.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTHelpViewController.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTWebViewController.h"
#import "ZSTF3Engine.h"
#import "ZSTShell.h"
#import "ZSTModuleAddition.h"
#import "ZSTAccountManagementViewController.h"
#import "ZSTSettingCell.h"

#define  kZSTSettingItemType_Register       @"Register"
#define  kZSTSettingItemType_Audio          @"Sound"
#define  kZSTSettingItemType_Shake          @"Vibrate"
#define  kZSTSettingItemType_CleanMsg       @"MessageClear"
#define  kZSTSettingItemType_ReportState    @"Report"
#define  kZSTSettingItemType_FullScreen     @"FullScreen"
#define  kZSTSettingItemType_NMSPackageType @"NoPicture"
#define  kZSTSettingItemType_SinaWeiBo      @"sinaWeiBo"
#define  kZSTSettingItemType_Recommend      @"Recommend"
#define  kZSTSettingItemType_Advice         @"Suggest"
#define  kZSTSettingItemType_Version        @"Update"
#define  kZSTSettingItemType_Help           @"Help"
#define  kZSTSettingItemType_About          @"About"

#define  kZSTSettingItemType_Web            @"Web"

#define kZSTAudioSwitchTag          79
#define kZSTShakeSwitchTag          80
#define kZSTCleanMsgSwitchTag       81
#define kZSTReportStateSwitchTag    82
#define kZSTFullScreenSwitchTag     83
#define kZSTNMSPackageTypeSwitchTag 84
#define kZSTSynchronousSinaWeiBoSwitchTag 85

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)
#define IS_IOS_7 ([[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue] >= 7)


@implementation ZSTSettingItem

@synthesize title;
@synthesize type;

+(ZSTSettingItem *)itemWithTitle:(NSString *)aTitle type:(NSString *)aType
{
    ZSTSettingItem *item = [[ZSTSettingItem alloc] init];
    item.title = aTitle;
    item.type = aType;
    return [item autorelease];
}

- (void)dealloc
{
    [TKUIUtil hiddenHUD];
    self.type = nil;
    self.title = nil;
    [super dealloc];
}

@end

@implementation ZSTSettingsViewController
//@synthesize weiBoEngine = _weiBoEngine;

/////////////////////////////////////////////////////////////////////////////////////

#pragma mark - helper

- (void)initSettingItems
{
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    if ([GP_Setting_Items isEqualToString:@"GP_Setting_Items"]) {
        GP_Setting_Items = @"Register,Recommend, Suggest, Update, Help, About, Sound, Vibrate, Report, FullScreen, MessageClear";
    }
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    _settingItems = [[[GP_Setting_Items componentsSeparatedByString:@","] mutableCopy] retain];
        
    if (![ZSTShell isModuleAvailable:-1] && ![ZSTShell isModuleAvailable:22]) {
        [_settingItems removeObject:@"Sound"];
        [_settingItems removeObject:@"Vibrate"];
        [_settingItems removeObject:@"MessageClear"];
    }
    NSString *GP_Setting_UrlItems = NSLocalizedString(@"GP_Setting_UrlItems", nil);
    if ([GP_Setting_UrlItems isEqualToString:@"GP_Setting_UrlItems"]) {
        GP_Setting_UrlItems = @"";
    }
    
    _urlSettingItems = [[NSMutableArray alloc] init];
    _urlSettingItemDict = [[NSMutableDictionary alloc] init];
    
    NSArray *array = [GP_Setting_UrlItems componentsSeparatedByString:@","];
    for (NSString *str in array) {
        
        NSRange range = [str rangeOfString:@":"];
        if (range.location != NSNotFound) {
            
            NSString *name = [str substringToIndex:range.location];
            NSString *url = [str substringFromIndex:range.location + 1];
            
            name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [_urlSettingItems addObject:name];
            [_urlSettingItemDict setObject:url forKey:name];
        }
    }
}


-(void) openRecommendSMSView
{
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController")); 
    
    if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        NSString *sharebaseurl = [NSString stringWithFormat:@"%@/%@",NSLocalizedString(@"GP_shareUrl", nil),[ZSTF3Preferences shared].ECECCID];
        picker.body = [NSString stringWithFormat:NSLocalizedString(@"GP_SMSRecommendMessage", nil),appDisplayName,sharebaseurl];
        
        picker.messageComposeDelegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
        [picker release];
    } else { 
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
    }
    [ZSTLogUtil logUserAction:NSStringFromClass([MFMessageComposeViewController class])];
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"推荐好友短信发送成功", nil)];
    }
}

- (void)checkClientVersionResponse:(NSString *)versionURL updateNote:(NSString *)updateNote updateVersion:(int)updateVersion
{
    if (versionURL != nil && [versionURL length] != 0) {
        
        _clientVersionUrl = [[NSString alloc] initWithFormat:@"%@",versionURL];
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"检查到有最新版本", nil)
                                                         message:[updateNote isEmptyOrWhitespace]? NSLocalizedString(@"现在就去更新吗？", nil):updateNote
                                                        delegate:self 
                                               cancelButtonTitle:NSLocalizedString(@"我要更新", nil)
                                               otherButtonTitles:NSLocalizedString(@"稍候更新", nil),nil];
        alert.tag = 1024;
        [alert show];
        [alert release];
    }else{
        
        [_clientVersionUrl release];
        _clientVersionUrl = nil;
        [ZSTUtils showAlertTitle:NSLocalizedString(@"检查完毕", nil) message:NSLocalizedString(@"当前客户端为最新版本", nil)];
    }
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0)
    {
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:_clientVersionUrl]];
    }
}

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"网络连接失败", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
    [TKUIUtil hiddenHUDAfterDelay:2];
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	[tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    NSArray *array = [_tableViewDataArray objectAtIndex:indexPath.section];
    ZSTSettingItem *item = [array objectAtIndex:indexPath.row];
        
    if ([item.type isEqualToString:kZSTSettingItemType_Register])
    {
        ZSTAccountManagementViewController *accountManagement = [[ZSTAccountManagementViewController alloc] init];
        accountManagement.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:accountManagement animated:YES];
        [accountManagement release];
        
    } else if ([item.type isEqualToString:kZSTSettingItemType_Recommend])
    {
        [self openRecommendSMSView];
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_Advice])
    {
        ZSTSuggestedQuestionsViewController *suggested_questions = [[ZSTSuggestedQuestionsViewController alloc] init];
        [self.navigationController pushViewController:suggested_questions animated:YES];
        [suggested_questions release];
        
    }	
    else if ([item.type isEqualToString:kZSTSettingItemType_Version])
    {
        if ([[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""] || [ZSTF3Preferences shared].loginMsisdn == nil) {
            
            [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"    尚未绑定\n不能检查更新", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
            [TKUIUtil hiddenHUDAfterDelay:2];
            return;
        }
                
        [self.engine checkClientVersion];
        [self.engine enUpdateECMobileClientParams];

        [ZSTLogUtil logUserAction:@"CheckForUpdates"];
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_Help])
    {        
        NSMutableArray *images = [NSMutableArray array];
        for (int i = 0; i < 3; i++) {
            NSString *imagePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"framework_help%d", i + 1] ofType:@"jpg"];
            [images addObject:imagePath];
        }
        
        [[ZSTHelpViewController shared] showWithImages:images];
        [ZSTLogUtil logUserAction:@"HelpViewController"];
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_About])
    {        
        NSString *settingAbout = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"setting_about"];
        NSRange range = [settingAbout rangeOfString:@","];
        if ([settingAbout hasPrefix:@"http://"] && range.location == NSNotFound) {
            ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
            [webView setURL:settingAbout];
            webView.type = self.moduleType;
             webView.hidesBottomBarWhenPushed = YES;
             webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:webView animated:YES];
            [webView release];
        }else{
            ZSTAboutViewController *aboutview = [[ZSTAboutViewController alloc] init];
            aboutview.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:aboutview animated:YES];
            [aboutview release];
        }
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_Web])
    {
        ZSTWebViewController *webViewController = [[ZSTWebViewController alloc] init];
        webViewController.hidesBottomBarWhenPushed = YES;
        webViewController.type = 9999999999;
        [webViewController setURL:[_urlSettingItemDict objectForKey:item.title]];
         webViewController.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
        [self.navigationController pushViewController:webViewController animated:YES];
        [webViewController release];
    }
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [_tableViewDataArray objectAtIndex:section];
	return array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    return [_tableViewDataArray count];
}

- (UISwitch *)addSwitch:(UITableViewCell *)cell itemType:(ZSTSettingItem *)item
{
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UISwitch *aSwitch = nil;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 5.0) {
        aSwitch = [[[UISwitch alloc] initWithFrame:CGRectMake(210, 10, 60, 40)] autorelease] ;
    } else {
        aSwitch = [[[UISwitch alloc] initWithFrame:CGRectMake(190, 10, 60, 40)] autorelease] ;
    }

    [aSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
    [cell.contentView addSubview:aSwitch];//为每个cell创建一个switch开关。
    return aSwitch;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTSettingCell *cell = [[[ZSTSettingCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                              reuseIdentifier:nil] autorelease];
//    cell.customBackgroundColor = [UIColor colorWithWhite:0.95 alpha:1];
    cell.customBackgroundColor = [UIColor whiteColor];
    cell.customSeparatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.cornerRadius = 4.0;
    [cell prepareForTableView:tableView indexPath:indexPath];    
    NSArray *array = [_tableViewDataArray objectAtIndex:indexPath.section];
    ZSTSettingItem *item = [array objectAtIndex:indexPath.row];
    cell.textLabel.text =  item.title;
//    cell.textLabel.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    if ([item.type isEqualToString:kZSTSettingItemType_Audio])
    {
        UISwitch *aSwitch = [self addSwitch:cell itemType:item];
        aSwitch.tag = kZSTAudioSwitchTag;
        aSwitch.on = [ZSTF3Preferences shared].sound;
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_Shake])
    {
        UISwitch *aSwitch = [self addSwitch:cell itemType:item];
        aSwitch.tag = kZSTShakeSwitchTag;
        aSwitch.on = [ZSTF3Preferences shared].shake;
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_CleanMsg])
    {
        UISwitch *aSwitch = [self addSwitch:cell itemType:item];
        aSwitch.tag = kZSTCleanMsgSwitchTag;
        aSwitch.on = [ZSTF3Preferences shared].cleanUp;
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_ReportState])
    {
        UISwitch *aSwitch = [self addSwitch:cell itemType:item];
        aSwitch.tag = kZSTReportStateSwitchTag;
        aSwitch.on = [ZSTF3Preferences shared].reportState;
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_FullScreen])
    {
        UISwitch *aSwitch = [self addSwitch:cell itemType:item];
        aSwitch.tag = kZSTFullScreenSwitchTag;
        aSwitch.on = [ZSTF3Preferences shared].fullScreen;
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_NMSPackageType])
    {
        UISwitch *aSwitch = [self addSwitch:cell itemType:item];
        aSwitch.tag = kZSTNMSPackageTypeSwitchTag;
        
        if ([ZSTF3Preferences shared].packageType == NMSPackageType_noPicture) {
            aSwitch.on = YES;
        }else{
            aSwitch.on = NO;
        }
    }else {
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
//        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        [cell initWithData];
    }
    
    return cell;
}

-(void) switchChanged: (UISwitch*)sender
{
    switch (sender.tag) {
        case kZSTAudioSwitchTag:
            if (sender.on)
            {
                [ZSTLogUtil logSysInfo: NSLocalizedString(@"声音提示打开", nil)];
                [ZSTUtils playAlertSound:@"sms-received1" withExtension:@"caf"];
                
            }
            else
            {
                [ZSTLogUtil logSysInfo: NSLocalizedString(@"声音提示关闭", nil)];
            }
            [ZSTF3Preferences shared].sound = sender.on;
            break;
        case kZSTShakeSwitchTag:
            if (sender.on) 
            {
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                [ZSTLogUtil logSysInfo: NSLocalizedString(@"震动提示打开", nil)];
                
            }
            else
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"震动提示关闭", nil)];
            }
            [ZSTF3Preferences shared].shake = sender.on;
            break;
        case kZSTCleanMsgSwitchTag:
            if (sender.on) 
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"信息自动清理功能打开", nil)];
                
                postNotificationOnMainThreadNoWait(NotificationName_CleanupMessages);
                
                [ZSTF3Preferences shared].cleanupDate = [NSDate date];
            }
            else
            {
                [ZSTLogUtil logSysInfo:NSLocalizedString(@"信息自动清理功能关闭", nil)];
            }
            [ZSTF3Preferences shared].cleanUp = sender.on;
            break;
            
        case kZSTReportStateSwitchTag:
        {
            [ZSTF3Preferences shared].reportState = sender.on;
            break;

        }
        case kZSTFullScreenSwitchTag:
        {
            [ZSTF3Preferences shared].fullScreen = sender.on;
            break;

        }
        case kZSTNMSPackageTypeSwitchTag:
        {
            if (sender.on) {
                [ZSTF3Preferences shared].packageType = NMSPackageType_noPicture;
            }else{
                [ZSTF3Preferences shared].packageType = NMSPackageType_normal;
            }
            break;

        }
        case kZSTSynchronousSinaWeiBoSwitchTag:
        {
            if (sender.on) {
                [self.sinaWeiboEngine logIn];
            }else if ([self.sinaWeiboEngine isLoggedIn] && !(sender.on))
            {
                [self.sinaWeiboEngine logOut];
            }
            break;
        }
        default:
            break;
    }
    [[ZSTF3Preferences shared] synchronize];
}

- (void)refreshArray
{
    _tableViewDataArray = [[NSMutableArray alloc] init];
    
    if ([_settingItems containsObject:kZSTSettingItemType_Register]) {
        NSString *title = NSLocalizedString(@"帐号管理", nil);
        [_tableViewDataArray addObject:[NSArray arrayWithObjects:[ZSTSettingItem itemWithTitle:title type:kZSTSettingItemType_Register], nil]];
    }
    
    
    NSMutableArray *array = [NSMutableArray array];
    
    if ([_settingItems containsObject:kZSTSettingItemType_Audio]) {
         [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"声音提示", nil) type:kZSTSettingItemType_Audio]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_Shake] && [UIDevice isIPhone]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"震动提示", nil) type:kZSTSettingItemType_Shake]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_CleanMsg]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"信息清理提示(大于200条)", nil) type:kZSTSettingItemType_CleanMsg]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_ReportState]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"报告状态", nil) type:kZSTSettingItemType_ReportState]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_FullScreen]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"消息窗口全屏", nil) type:kZSTSettingItemType_FullScreen]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_NMSPackageType]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"无图模式", nil) type:kZSTSettingItemType_NMSPackageType]];
    }

    [_tableViewDataArray addObject:array];
    
    array = [NSMutableArray array];
    
    if ([_settingItems containsObject:kZSTSettingItemType_Advice]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"提交建议", nil) type:kZSTSettingItemType_Advice]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_Version]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"检查更新", nil) type:kZSTSettingItemType_Version]];
    }
    
    if ([ZSTF3Preferences shared].isShowHelp) {
        if ([_settingItems containsObject:kZSTSettingItemType_Help]) {
            [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"帮助", nil) type:kZSTSettingItemType_Help]];
        }
    }
    
    for (NSString *name in _urlSettingItems) {
        [array addObject:[ZSTSettingItem itemWithTitle:name type:kZSTSettingItemType_Web]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_About]) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"关于", nil) type:kZSTSettingItemType_About]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_Advice]) {
        Class messageClass = (NSClassFromString(@"MFMessageComposeViewController")); 
        if (messageClass != nil && [messageClass canSendText])
        {
            [array insertObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"短信推荐", nil) type:kZSTSettingItemType_Recommend] atIndex:0];
        }
    }
    
    [_tableViewDataArray addObject:array];
}

-(void)UpdateStingsView
{
    [self refreshArray];
    [_tableView reloadData];
}

-(UIImage *)imageWithImageSimple:(UIImage*)image scaledToSize:(CGSize)newSize
{
    
    UIGraphicsBeginImageContext(newSize);//根据当前大小创建一个基于位图图形的环境
    
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];//根据新的尺寸画出传过来的图片
    
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();//从当前环境当中得到重绘的图片
    
    UIGraphicsEndImageContext();//关闭当前环境
    
    return newImage;
}


- (void)viewDidLoad 
{
    [super viewDidLoad];
    self.navigationItem.titleView = [ZSTUtils logoView];
    [self initSettingItems];
    
    [self refreshArray];
    
	//create tableview
	_tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-(IS_IOS_7?0:20)+(iPhone5?88:0)) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.backgroundView = nil;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //ios6 bug  必须要这样写， 否则底部会有空隙
    _tableView.tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,1.0f,1.0f)] autorelease];
   	_tableView.dataSource = self;
    _tableView.delegate = self;
    
//    UIImageView *backgroundView = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]] autorelease];
//    backgroundView.frame = CGRectMake(0, 0, 320, 480-20-44-49);
//    [self.view addSubview:backgroundView];
	[self.view addSubview:_tableView];
    
}

-(void) remove
{
	[button removeFromSuperview];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}

- (void)dealloc {
    [_urlSettingItems release];
    [_urlSettingItemDict release];
    [_settingItems release];
    [_clientVersionUrl release];
	[_tableView release];
	[_tableViewDataArray release];
    [super dealloc];
}


@end
