
//
//  RegisterView.m
//  F3
//
//  Created by xuhuijun on 11-10-20.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTRegisterViewController.h"
#import "ZSTVerficationCodeViewController.h"
#import "TKUIUtil.h"
#import "ZSTRegisterTimer.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"
#import "ZSTF3Preferences.h"
#import "ElementParser.h"
#import "DocumentRoot.h"
#import <MessageUI/MessageUI.h>
#import "ZSTAgreementViewController.h"

#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(640, 1136), [[UIScreen mainScreen] currentMode].size) : NO)

@interface Custom_Register_UITextFiled: UITextField

@end

@implementation Custom_Register_UITextFiled

- (void) drawPlaceholderInRect:(CGRect)rect {
    [[UIColor colorWithWhite:0.7 alpha:1] setFill];
    CGRect frame = rect;
    if (IS_IOS_7) {
        
        frame.origin.y += 10;
    }
    [[self placeholder] drawInRect:frame withFont:[UIFont systemFontOfSize:16]];
}

@end

@class Custom_Register_TableViewCell;

@interface Custom_Register_TableViewCell: UITableViewCell
{
    UITextField *_numberTextField;
}

@property (nonatomic, retain) UITextField *numberTextField;

@end

@implementation Custom_Register_TableViewCell

@synthesize numberTextField = _numberTextField;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) 
    {
        _numberTextField = [[Custom_Register_UITextFiled alloc] initWithFrame:CGRectMake(10, 10, 280, 40)];
        if ([ZSTF3Preferences shared].loginMsisdn == nil || [[ZSTF3Preferences shared].loginMsisdn length] == 0 || [[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"9"])
        { 
            _numberTextField.placeholder = NSLocalizedString(@"请输入要绑定的手机号", nil);
            
        }else{
            _numberTextField.placeholder = NSLocalizedString(@"请输入要切换的手机号", nil);
        }
        _numberTextField.borderStyle = UITextBorderStyleNone;
        _numberTextField.keyboardType = UIKeyboardTypeNumberPad;
        _numberTextField.font = [UIFont systemFontOfSize:20];
        [_numberTextField becomeFirstResponder];
        _numberTextField.backgroundColor = [UIColor clearColor];
        _numberTextField.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        _numberTextField.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        _numberTextField.text = @" ";
        [self.contentView addSubview:_numberTextField];
    }
    return self;
}

- (void)dealloc
{
    TKRELEASE(_numberTextField);
    [super dealloc];
}

@end

@implementation ZSTRegisterViewController

@synthesize delegate = _delegate;
@synthesize engine;
@synthesize isFromSetting;

- (void)dealloc
{
    TKRELEASE(_tableView);
    TKRELEASE(_intervarlLabel);
    engine = nil;
    [super dealloc];
}

-(void)openVerificationViewController
{
    
    ZSTVerficationCodeViewController *verficationCodeViewController = [[ZSTVerficationCodeViewController alloc] init];
    verficationCodeViewController.delegate = _delegate;
    
    [self.navigationController pushViewController:verficationCodeViewController animated:YES];
    
    [verficationCodeViewController release];
    
}
-(void)dismissView
{
    
    if ([_delegate respondsToSelector:@selector(registerDidCancel)]) {
        [_delegate registerDidCancel];
        return;
    }
    if (TKIsPad()) {
        
        [self.navigationController popViewControllerAnimated:YES];
    } else 
    {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
 
}

-(void)statusOfGetVerfication
{
    
}

-(void)next
{
    Custom_Register_TableViewCell *cell = (Custom_Register_TableViewCell *)[_tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    [cell.numberTextField resignFirstResponder];
    
    if (!isallow) {
        
        [TKUIUtil alertInView:self.view withTitle:@"请先阅读并同意《用户协议》" withImage:nil];
        return;
    }

    if (!_isselected) {
        
        return;
    }
    
    if ([ZSTRegisterTimer sharedTimer].isTiming) {
        
        [self openVerificationViewController];
        return;
    }
    
    [cell.numberTextField resignFirstResponder];
    
    if ([cell.numberTextField.text length] == 0 || cell.numberTextField.text == nil || [cell.numberTextField.text length] != 11 ) {
        CGRect rect = self.view.frame;
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入正确的手机号码", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
        
    }else{

        
        ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
        preferences.phoneNumber = cell.numberTextField.text;
        
        if ([preferences.phoneNumber isEqualToString:@"13000000001"]) {
            preferences.verficationCode = @"1234";
            [self openVerificationViewController];
            return;
        }
        self.navigationItem.rightBarButtonItem.enabled = NO;
        CGRect rect = self.view.frame;
        [TKUIUtil showHUDInView:self.view withText:nil withImage:nil withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
        [self.engine getRegChecksum:preferences.phoneNumber];
        
    }
}

-(void)updateIntervalAction:(NSNotification *)notification
{
    
    Custom_Register_TableViewCell *cell = (Custom_Register_TableViewCell *)[_tableView cellForRowAtIndexPath: [NSIndexPath indexPathForRow:0 inSection:0]];
    
    int remainingTime = [[notification object] intValue];
    if ([cell.numberTextField isFirstResponder]) {
        [cell.numberTextField resignFirstResponder];
    }
    cell.numberTextField.userInteractionEnabled = NO;
    
    if (remainingTime <= 0) {
        _intervarlLabel.hidden = YES;
        _intervarlLabel.text = @"";
        cell.numberTextField.userInteractionEnabled = YES;
        [cell.numberTextField becomeFirstResponder];
    } else {
        _intervarlLabel.hidden = NO;
    }
    
    _intervarlLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d秒后您可以重新输入手机号", nil),remainingTime];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    
    [TKUIUtil hiddenHUD];
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    _isselected = YES;
    
    isallow = YES;
    
    self.tabBarItem.title = NSLocalizedString(@"输入手机号", nil);
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
    if (IS_IOS_7) {
        self.edgesForExtendedLayout = UIRectEdgeNone;
        self.extendedLayoutIncludesOpaqueBars = NO;
        self.modalPresentationCapturesStatusBarAppearance = NO;
        self.navigationController.navigationBar.translucent = NO;
    }
    
    
    CGRect rect;
    if (TKIsPad()) {
        
        rect = CGRectMake(0, 0, 460, 501);
    } else {
        
        rect = CGRectMake(0, 0, 320, 416+(iPhone5?88:0));
    }
    
    _tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStyleGrouped];
    _tableView.scrollEnabled = NO;
    _tableView.delegate = self;
    _tableView.dataSource = self;
    [self.view addSubview:_tableView];
    
    UIView *tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0, 0, 320,90)] autorelease];
    tableFooterView.alpha = 0.5;
    tableFooterView.backgroundColor = [UIColor clearColor];
    
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    bottomView.backgroundColor = [UIColor clearColor];
    [tableFooterView addSubview:bottomView];
    
    _markImgView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, 15, 15)];
    _markImgView.image = ZSTModuleImage(@"module_orderinfo_pay_on.png");
    [bottomView addSubview:_markImgView];
    
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    btn.backgroundColor = [UIColor clearColor];
    btn.frame = CGRectMake(0, 0, 60, 60);
    [btn addTarget:self action:@selector(agreeAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:btn];
    
    UILabel *textlal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(_markImgView.frame)+5, 0, 115, 20)];
    textlal.backgroundColor = [UIColor clearColor];
    textlal.textColor = [UIColor blackColor];
    textlal.text = @"我已阅读并同意";
    textlal.font = [UIFont systemFontOfSize:16];
    textlal.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:textlal];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(textlal.frame)-5, 0, 100, 20)];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = RGBCOLOR(7, 168, 19);
    label.text = @"《用户协议》";
    label.font = [UIFont systemFontOfSize:16];
    label.textAlignment = NSTextAlignmentCenter;
    [bottomView addSubview:label];
    
    UIButton *agreebtn = [UIButton buttonWithType:UIButtonTypeCustom];
    agreebtn.backgroundColor = [UIColor clearColor];
    agreebtn.frame = CGRectMake(label.frame.origin.x, 0, 80, 40);
    [agreebtn addTarget:self action:@selector(appointAction) forControlEvents:UIControlEventTouchUpInside];
    [bottomView addSubview:agreebtn];
    
    _intervarlLabel = [[UILabel alloc] initWithFrame:CGRectMake(TKIsPad()? 40: 20, 40, 300, 30)];
    _intervarlLabel.backgroundColor = [UIColor clearColor];
    
    [tableFooterView addSubview:_intervarlLabel];

    
    [label release];
    [textlal release];
    [bottomView release];
    
    if ([ZSTRegisterTimer sharedTimer].isTiming) {
        _intervarlLabel.hidden = NO;
        _intervarlLabel.text = [NSString stringWithFormat:NSLocalizedString(@"%d秒后您可以重新输入手机号", nil),[ZSTRegisterTimer sharedTimer].remainingTime];
    }
    _tableView.tableFooterView = tableFooterView;
    
    if ([ZSTF3Preferences shared].MCRegistType != MCRegistType_ForceRegister || isFromSetting) {

        self.navigationItem.leftBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (dismissView)];
    }
     

    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"下一步", nil) target:self selector:@selector (next)];
    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(updateIntervalAction:) 
                                                 name: NotificationName_RemaingTimeChange 
                                               object: nil];
    
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTRegisterViewController class])];
    
}

- (void) appointAction
{
    ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
    [navController release];
    [controller release];
}

- (void) agreeAction
{
    if (!_isselected) {
        _markImgView.image = ZSTModuleImage(@"module_orderinfo_pay_on.png");
        
        isallow = YES;
        
    } else {
        
        _markImgView.image = ZSTModuleImage(@"module_orderinfo_pay_off.png");
        
        isallow = NO;
    }
    
    _isselected = !_isselected;
}

#pragma mark ------------------UITableViewDataSource---------------------

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *cellIdentifier = @"CustomTableViewCell";
    
    Custom_Register_TableViewCell *cell = (Custom_Register_TableViewCell *)[tableView dequeueReusableCellWithIdentifier: cellIdentifier];
    if (!cell)
    {
        cell = [[[Custom_Register_TableViewCell alloc] initWithStyle: UITableViewCellStyleDefault 
                                   reuseIdentifier: cellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
     
    }
    
    ZSTF3Preferences *dataManager = [ZSTF3Preferences shared];
     cell.numberTextField.text = dataManager.loginMsisdn;
    
    return cell;
}


- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return NSLocalizedString(@"绑定手机号能获得更好的服务体验!\n\n", nil);
}

#pragma mark ------------------UITableViewDelegate---------------------

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}
#pragma mark - ZSTF3EngineDelegate
- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    [TKUIUtil hiddenHUD];
    CGRect rect = self.view.frame;
    NSString * result = response.stringResponse;
    ElementParser *parser = [[ElementParser alloc] init];
    DocumentRoot *root = [parser parseXML:result];
    NSArray * arr = [root selectElements:@"Response ResultDesc"];
    Element * element = [arr objectAtIndex:0];
    if (element) {
        NSString * resultShow = [element contentsText];
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(resultShow
                                                                    , nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
    }
    else
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"网络异常,请检查网络", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(rect.size.width/2, rect.size.height/3.6)];
    }
    self.navigationItem.rightBarButtonItem.enabled = YES;
}

- (void)getRegChecksumResponse
{
    [TKUIUtil hiddenHUD];
    self.navigationItem.rightBarButtonItem.enabled = YES;
    [self openVerificationViewController];
    [ZSTF3Preferences shared].verficationCode = @"";
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // 操作
    
    return NO;  // YES为允许横屏，否则不允许横屏
}
                                                                                            
@end
