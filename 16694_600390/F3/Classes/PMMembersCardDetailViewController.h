//
//  PMMembersCardDetailViewController.h
//  F3
//
//  Created by P&M on 15/9/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"

@interface PMMembersCardDetailViewController : UIViewController

@property (strong, nonatomic) ZSTF3Engine *engine;

@end
