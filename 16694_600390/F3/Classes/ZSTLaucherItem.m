//
//  ZSTLaucherItem.m
//  F3
//
//  Created by 9588 on 11/10/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTLaucherItem.h"

@implementation ZSTLaucherItem

@synthesize title = _title;

@synthesize introduce = _introduce;

@synthesize url = _url;

@synthesize icon = _icon;

@synthesize badgeNumber = _badgeNumber;

@synthesize enable = _enable;

@synthesize canDelete = _canDelete;

-(void)dealloc
{
    self.title = nil;
    self.icon = nil;
    [super dealloc];
}

+(id)itemWithTitle:(NSString *)title icon:(NSString *)icon url:(NSString *)url
{
    ZSTLaucherItem *item = [[ZSTLaucherItem alloc] init];
    item.title = title;
    item.icon = icon;
    item.url = url;
    item.canDelete = YES;
    item.enable = YES;
    return [item autorelease];
}

- (void)setItemIntroduce:(NSString *)introduce
{
    _introduce = introduce;
}

@end
