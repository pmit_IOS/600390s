//
//  ZSTAccountManagementViewController.m
//  F3
//
//  Created by xuhuijun on 13-7-9.
//  Copyright (c) 2013年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAccountManagementViewController.h"
#import "ZSTSettingsViewController.h"
#import "ZSTModuleAddition.h"
#import "ZSTUtils.h"

#define  kZSTSettingItemType_MainAccount            @"Register"
#define  kZSTSettingItemType_SinaWeiboAccount       @"SinaWeiboAccount"
#define  kZSTSettingItemType_QQAccount              @"QQAccount"
#define  kZSTSettingItemType_TweiboAccount          @"TweiboAccount"
#define  kZSTSettingItemType_WeixinAccount          @"WeixinAccoun"

@interface ZSTAccountManagementViewController ()

@end

@implementation ZSTAccountManagementViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)refreshArray
{
    accountItems = [[NSMutableArray alloc] init];
    
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    if ([GP_Setting_Items isEqualToString:@"GP_Setting_Items"]) {
        GP_Setting_Items = @"Register,Recommend, Suggest, Update, Help, About, Sound, Vibrate, Report, FullScreen, MessageClear";
    }
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    settingItems = (NSMutableArray *)[[GP_Setting_Items componentsSeparatedByString:@","] retain];
    
    if ([settingItems containsObject:kZSTSettingItemType_MainAccount]) {
        NSString *title = NSLocalizedString(@"帐号", nil);
        if ([ZSTF3Preferences shared].MCRegistType == MCRegistType_PromptRegister && ([ZSTF3Preferences shared].loginMsisdn == nil || [[ZSTF3Preferences shared].loginMsisdn length] == 0 || [[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"9"]))
        {
            title = NSLocalizedString(@"绑定", nil);
        }
        
        [accountItems addObject:[NSArray arrayWithObjects:[ZSTSettingItem itemWithTitle:title type:kZSTSettingItemType_MainAccount], nil]];
    }
    
    
    NSMutableArray *array = [NSMutableArray array];
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    if (pre.SinaWeiBo) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"新浪微博", nil) type:kZSTSettingItemType_SinaWeiboAccount]];
    }
    if (pre.TWeiBo) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"腾讯微博", nil) type:kZSTSettingItemType_TweiboAccount]];
    }
    if (pre.QQ) {
        [array addObject:[ZSTSettingItem itemWithTitle:NSLocalizedString(@"QQ空间", nil) type:kZSTSettingItemType_QQAccount]];
    }

    [accountItems addObject:array];    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"账号管理", nil)];
    [self refreshArray];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 320, 480-(IS_IOS_7?0:20)+(iPhone5?88:0)) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.backgroundView = nil;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //ios6 bug  必须要这样写， 否则底部会有空隙
    _tableView.tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,1.0f,1.0f)] autorelease];
   	_tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];

    
    [[NSNotificationCenter defaultCenter] addObserver: self
                                             selector: @selector(UpdateStingsView)
                                                 name: NotificationName_LoginMsisdnChange
                                               object: nil];
}
  
    
-(void)UpdateStingsView
{
    [self refreshArray];
    [_tableView reloadData];
}

#pragma mark - RegisterDelegate
- (void)registerDidFinish
{
    [_tableView reloadData];
}


#pragma -sinaWeiboEngineDelegate---

- (void)removeAuthData
{
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SinaWeiboAuthData"];
}

- (void)storeAuthData:(SinaWeibo *)sinaWeibo
{
    NSDictionary *authData = [NSDictionary dictionaryWithObjectsAndKeys:
                              sinaWeibo.accessToken, @"AccessTokenKey",
                              sinaWeibo.expirationDate, @"ExpirationDateKey",
                              sinaWeibo.userID, @"UserIDKey",
                              sinaWeibo.refreshToken, @"refresh_token", nil];
    [[NSUserDefaults standardUserDefaults] setObject:authData forKey:@"SinaWeiboAuthData"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)sinaweiboDidLogIn:(SinaWeibo *)sinaWeibo
{
    [self storeAuthData:sinaWeibo];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"新浪微博授权成功" , nil) withImage:nil];
    [_tableView reloadData];
}
- (void)sinaweibo:(SinaWeibo *)sinaweibo logInDidFailWithError:(NSError *)error
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"新浪微博授权失败" , nil) withImage:nil];
    [_tableView reloadData];
}

- (void)sinaweiboDidLogOut:(SinaWeibo *)sinaweibo
{
    [self removeAuthData];
    [_tableView reloadData];

}
- (void)sinaweibo:(SinaWeibo *)sinaweibo accessTokenInvalidOrExpired:(NSError *)error
{
    [self removeAuthData];
    [_tableView reloadData];
}


#pragma mark TencentSessionDelegate //QZone

- (void)tencentDidLogin {
    
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"QZone授权成功" , nil) withImage:nil];
    [_tableView reloadData];

}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
    if (!cancelled) {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"QZone授权失败" , nil) withImage:nil];
    }
    [_tableView reloadData];
}

- (void)tencentDidLogout
{
    [_tableView reloadData];
}


#pragma mark TencentWeibo


- (void)tencentLoginSuccessCallBack
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"腾讯微博授权成功", nil) withImage:nil];
    [_tableView reloadData];

}

- (void)tencentLoginFailureCallBack:(id)result
{
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"腾讯微博授权失败", nil) withImage:nil];
    [_tableView reloadData];
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDelegate－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失

    NSArray *array = [accountItems objectAtIndex:indexPath.section];
    ZSTSettingItem *item = [array objectAtIndex:indexPath.row];
    if ([item.type isEqualToString:kZSTSettingItemType_MainAccount])
    {
        if (([ZSTF3Preferences shared].MCRegistType == MCRegistType_PromptRegister)
            || ([ZSTF3Preferences shared].MCRegistType == MCRegistType_ForceRegister)) {
            
            ZSTRegisterViewController *registerView = [[ZSTRegisterViewController alloc] init];
            registerView.delegate = self;
            registerView.isFromSetting = YES;
            UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:registerView];
            [self presentViewController:navController animated:YES completion:nil];
            [navController release];
        }
    }else if ([item.type isEqualToString:kZSTSettingItemType_SinaWeiboAccount]){
        
        if ([self.sinaWeiboEngine isAuthValid]) {
            [self.sinaWeiboEngine logOut];
        }else{
            [self.sinaWeiboEngine logIn];
        }
    }else if ([item.type isEqualToString:kZSTSettingItemType_QQAccount]){
        
        if ([self.QQEngine isSessionValid]) {
            [self.QQEngine logout:self];
        }else{
            NSArray * permissions = [NSArray arrayWithObjects:
                                     kOPEN_PERMISSION_GET_USER_INFO,
                                     kOPEN_PERMISSION_GET_SIMPLE_USER_INFO,
                                     kOPEN_PERMISSION_ADD_ALBUM,
                                     kOPEN_PERMISSION_ADD_IDOL,
                                     kOPEN_PERMISSION_ADD_ONE_BLOG,
                                     kOPEN_PERMISSION_ADD_PIC_T,
                                     kOPEN_PERMISSION_ADD_SHARE,
                                     kOPEN_PERMISSION_ADD_TOPIC,
                                     kOPEN_PERMISSION_CHECK_PAGE_FANS,
                                     kOPEN_PERMISSION_DEL_IDOL,
                                     kOPEN_PERMISSION_DEL_T,
                                     kOPEN_PERMISSION_GET_FANSLIST,
                                     kOPEN_PERMISSION_GET_IDOLLIST,
                                     kOPEN_PERMISSION_GET_INFO,
                                     kOPEN_PERMISSION_GET_OTHER_INFO,
                                     kOPEN_PERMISSION_GET_REPOST_LIST,
                                     kOPEN_PERMISSION_LIST_ALBUM,
                                     kOPEN_PERMISSION_UPLOAD_PIC,
                                     kOPEN_PERMISSION_GET_VIP_INFO,
                                     kOPEN_PERMISSION_GET_VIP_RICH_INFO,
                                     kOPEN_PERMISSION_GET_INTIMATE_FRIENDS_WEIBO,
                                     kOPEN_PERMISSION_MATCH_NICK_TIPS_WEIBO,
                                     nil] ;
            [self.QQEngine authorize:permissions inSafari:NO];
            [permissions release];
        }
        
    }else if ([item.type isEqualToString:kZSTSettingItemType_TweiboAccount]){
        
        if ([self.tencentWeiboEngine isAuthorizeExpired]) {
            [self.tencentWeiboEngine logInWithDelegate:self
                                             onSuccess:@selector(tencentLoginSuccessCallBack)
                                             onFailure:@selector(tencentLoginFailureCallBack:)];
            [self.tencentWeiboEngine setRootViewController:self];
        }else{
            [self.tencentWeiboEngine logOut];
            [_tableView reloadData];
        }
    }

}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *array = [accountItems objectAtIndex:section];
    return array.count;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return [accountItems count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TKTableViewCell *cell = [[[TKTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                    reuseIdentifier:nil] autorelease];
    cell.customBackgroundColor = [UIColor whiteColor];
    cell.customSeparatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.cornerRadius = 4.0;
    [cell prepareForTableView:tableView indexPath:indexPath];
    NSArray *array = [accountItems objectAtIndex:indexPath.section];
    ZSTSettingItem *item = [array objectAtIndex:indexPath.row];
    cell.textLabel.text =  item.title;
    cell.textLabel.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    
    if ([item.type isEqualToString:kZSTSettingItemType_MainAccount])
    {
        UILabel *loginMsisdnLable = [[UILabel alloc] initWithFrame:CGRectMake(50, 15, 90, 20)];
        loginMsisdnLable.text = [NSString stringWithFormat:@"%@",[ZSTF3Preferences shared].loginMsisdn];
        loginMsisdnLable.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
        loginMsisdnLable.font = [UIFont systemFontOfSize:14];
        loginMsisdnLable.backgroundColor = [UIColor clearColor];
        [cell.contentView addSubview:loginMsisdnLable];
        [loginMsisdnLable release];
        
        if ([ZSTF3Preferences shared].MCRegistType == MCRegistType_VirtualRegister) {
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.accessoryType = UITableViewCellAccessoryNone;
        } else {
            UILabel *registerIdentifierLable = [[UILabel alloc] initWithFrame:CGRectMake(240, 15, 45, 20)];
            registerIdentifierLable.backgroundColor = [UIColor clearColor];
            registerIdentifierLable.textAlignment = NSTextAlignmentRight;
            registerIdentifierLable.font = [UIFont systemFontOfSize:14];
            
            if ([ZSTF3Preferences shared].loginMsisdn == nil || [[ZSTF3Preferences shared].loginMsisdn length] == 0 || [[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"9"])
            {
                if ([ZSTF3Preferences shared].MCRegistType == MCRegistType_PromptRegister) {
                    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                    
                } else {
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.accessoryType = UITableViewCellAccessoryNone;
                }
                
                registerIdentifierLable.text = NSLocalizedString(@"未绑定", nil);
                registerIdentifierLable.textColor = [UIColor redColor];
                
            } else {
                cell.selectionStyle = UITableViewCellSelectionStyleBlue;
                
                registerIdentifierLable.text = NSLocalizedString(@"已绑定", nil);
                registerIdentifierLable.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
            }
            
            [cell.contentView addSubview:registerIdentifierLable];
            [registerIdentifierLable release];
        }
        
    }else {
        UILabel *registerIdentifierLable = [[UILabel alloc] initWithFrame:CGRectMake(240, 15, 45, 20)];
        registerIdentifierLable.backgroundColor = [UIColor clearColor];
        registerIdentifierLable.textAlignment = NSTextAlignmentRight;
        registerIdentifierLable.font = [UIFont systemFontOfSize:14];
        cell.selectionStyle = UITableViewCellSelectionStyleBlue;
        if ([item.type isEqualToString:kZSTSettingItemType_SinaWeiboAccount]) {
            if (![self.sinaWeiboEngine isAuthValid])
            {
                registerIdentifierLable.text = NSLocalizedString(@"未绑定", nil);
                registerIdentifierLable.textColor = [UIColor redColor];
                
            } else {
                
                registerIdentifierLable.text = NSLocalizedString(@"已绑定", nil);
                registerIdentifierLable.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
            }
        }else if ([item.type isEqualToString:kZSTSettingItemType_QQAccount]) {
            if (![self.QQEngine isSessionValid])
            {
                registerIdentifierLable.text = NSLocalizedString(@"未绑定", nil);
                registerIdentifierLable.textColor = [UIColor redColor];
                
            } else {
                
                registerIdentifierLable.text = NSLocalizedString(@"已绑定", nil);
                registerIdentifierLable.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
            }
        }else if ([item.type isEqualToString:kZSTSettingItemType_TweiboAccount]) {
            if ([self.tencentWeiboEngine isAuthorizeExpired])
            {
                registerIdentifierLable.text = NSLocalizedString(@"未绑定", nil);
                registerIdentifierLable.textColor = [UIColor redColor];
                
            } else {
                
                registerIdentifierLable.text = NSLocalizedString(@"已绑定", nil);
                registerIdentifierLable.textColor = [UIColor colorWithRed:108/255.0 green:124/255.0 blue:154/255.0 alpha:1];
            }
        }

        
        [cell.contentView addSubview:registerIdentifierLable];
        [registerIdentifierLable release];
    }

    return cell;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
