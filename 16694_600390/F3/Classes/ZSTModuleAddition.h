//
//  UIViewController_Module.h
//  F3Engine
//
//  Created by luobin on 12-9-14.
//
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"
#import "ZSTDao.h"
#import "SinaWeibo.h"
#import <TencentOpenAPI/TencentOAuth.h>
#import "TCWBEngine.h"



@interface UIViewController (Module)<ZSTF3EngineDelegate,SinaWeiboDelegate,TencentSessionDelegate>

@property (nonatomic, assign) NSInteger moduleType;
@property (nonatomic, retain, readonly) ZSTF3Engine *engine;
@property (nonatomic, retain) ZSTDao *dao;
@property (nonatomic, retain) SinaWeibo *sinaWeiboEngine;
@property (nonatomic, retain) TencentOAuth *QQEngine;
@property (nonatomic, retain) TCWBEngine *tencentWeiboEngine;

@end

@interface UIView (Module)<ZSTF3EngineDelegate>

@property (nonatomic, retain, readonly) ZSTF3Engine *engine;
@property (nonatomic, retain) ZSTDao *dao;
@property (nonatomic, assign) NSInteger moduleType;
@end


