//
//  ZSTPhotoTypeView.h
//  F3
//
//  Created by pmit on 15/9/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTPhotoTypeViewDelegate <NSObject>

- (void)surePhotoType:(NSInteger)photoType;
- (void)cancelPhotoView;

@end

@interface ZSTPhotoTypeView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (strong,nonatomic) UIPickerView *photoTypePickerView;
@property (assign,nonatomic) NSInteger photoType;
@property (weak,nonatomic) id<ZSTPhotoTypeViewDelegate> photoTypeDelegate;

- (void)createPhotoPicker;

@end
