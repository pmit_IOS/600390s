//
//  ZSTPersonalExitTableViewCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-21.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalExitTableViewCell.h"

@implementation ZSTPersonalExitTableViewCell

- (void)awakeFromNib
{
    UIImage *imageN = ZSTModuleImage(@"module_personal_exit_grey_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_personal_exit_grey_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.exitbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.exitbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
