//
//  EditView.h
//  F3
//
//  Created by xuhuijun on 11-12-5.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ZSTEditView : UIImageView 
{
    UIWindow *_window;
}
@property (nonatomic, retain) UIWindow *window;

- (void)show;
- (void)dismiss;

@end
