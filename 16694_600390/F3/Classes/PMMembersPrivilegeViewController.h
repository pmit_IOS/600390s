//
//  PMMembersPrivilegeViewController.h
//  F3
//
//  Created by P&M on 15/9/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"

@interface PMMembersPrivilegeViewController : UIViewController

@property (strong, nonatomic) ZSTF3Engine *engine;

@property (strong, nonatomic) NSString *discount;

@end
