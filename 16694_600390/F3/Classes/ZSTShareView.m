//
//  ZSTShareView.m
//  F3
//
//  Created by pmit on 15/8/17.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTShareView.h"
#import "PMRepairButton.h"
#import <WXApi.h>
#import <TencentOpenAPI/TencentApiInterface.h>
#import <TencentOpenAPI/TencentMessageObject.h>
#import <TencentOpenAPI/QQApi.h>
#import <TencentOpenAPI/QQApiInterface.h>
#import "ZSTF3ClientAppDelegate.h"
#import <WeiboSDK.h>

@implementation ZSTShareView

- (UIView *)createUIWithFrame:(CGRect)frame
{
//    NSArray *shareArr = @[@"微信分享",@"朋友圈分享",@"QQ分享",@"新浪微博分享"];
    self.backgroundColor = [UIColor whiteColor];
    NSArray *shareImgArr = @[ZSTModuleImage(@"wechatshare.png"),ZSTModuleImage(@"friendcircleshare.png"),ZSTModuleImage(@"qqshare.png"),ZSTModuleImage(@"weiboshare.png")];
    for (NSInteger i = 0; i < shareImgArr.count; i++)
    {
        PMRepairButton *shareBtn = [[PMRepairButton alloc] init];
        shareBtn.frame = CGRectMake(i * (frame.size.width/ 4) + 10, 10, 60, 60);
        shareBtn.tag = i;
        [shareBtn setImage:shareImgArr[i] forState:UIControlStateNormal];
        [shareBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:shareBtn];
    }
    
    PMRepairButton *cancelBtn = [[PMRepairButton alloc] init];
    cancelBtn.frame = CGRectMake((WIDTH - 30) / 2, 100, 30, 30);
//    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setImage:ZSTModuleImage(@"cancel.png") forState:UIControlStateNormal];
    cancelBtn.contentMode = UIViewContentModeScaleAspectFit;
    cancelBtn.layer.borderColor = [UIColor whiteColor].CGColor;
    [cancelBtn addTarget:self action:@selector(dismissShare:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:cancelBtn];
    _scene = WXSceneSession;
    return self;
}

- (void)btnClick:(PMRepairButton *)sender
{
    switch (sender.tag)
    {
        case 0:
        {
            if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
            {
                SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
                req.text = self.shareString;
                req.bText = YES;
                req.scene = WXSceneSession;
                [WXApi sendReq:req];
            }
        }
            break;
        case 1:
        {
            if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
            {
                SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
                req.text = self.shareString;
                req.bText = YES;
                req.scene = WXSceneTimeline;
                [WXApi sendReq:req];
            }
        }
            break;
        case 2:
        {
            QQApiTextObject *txtObj = [QQApiTextObject objectWithText:self.shareString];
            SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:txtObj];
            QQApiSendResultCode sent = [QQApiInterface sendReq:req];
            [self handleSendResult:sent];
        }
            break;
        case 3:
        {
            ZSTF3ClientAppDelegate *myDelegate = (ZSTF3ClientAppDelegate*)[[UIApplication sharedApplication] delegate];
            WBAuthorizeRequest *authRequest = [WBAuthorizeRequest request];
            authRequest.redirectURI = [ZSTF3Preferences shared].weiboRedirect;
            authRequest.scope = @"all";
            WBMessageObject *message = [WBMessageObject message];
            message.text = self.shareString;
            WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:message authInfo:authRequest access_token:myDelegate.wbtoken];
            request.userInfo = @{@"ShareMessageFrom": @"ZSTNewsBContentViewController",
                                 @"Other_Info_1": [NSNumber numberWithInt:123],
                                 @"Other_Info_2": @[@"obj1", @"obj2"],
                                 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
            [WeiboSDK sendRequest:request];
        }
            break;
            
        default:
            break;
    }
}

- (void)dismissShare:(UIButton *)sender
{
    if ([_delegate respondsToSelector:@selector(dismissShareView)])
    {
        [_delegate dismissShareView];
    }
}

- (void)handleSendResult:(QQApiSendResultCode)sendResult
{
    switch (sendResult)
    {
        case EQQAPIAPPNOTREGISTED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"App未注册" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIMESSAGECONTENTINVALID:
        case EQQAPIMESSAGECONTENTNULL:
        case EQQAPIMESSAGETYPEINVALID:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"发送参数错误" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPIQQNOTINSTALLED:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"未安装手Q" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];

            
            break;
        }
        case EQQAPIQQNOTSUPPORTAPI:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"API接口不支持" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];
            
            break;
        }
        case EQQAPISENDFAILD:
        {
            UIAlertView *msgbox = [[UIAlertView alloc] initWithTitle:@"Error" message:@"发送失败" delegate:nil cancelButtonTitle:@"取消" otherButtonTitles:nil];
            [msgbox show];

            
            break;
        }
        default:
        {
            break;
        }
    }
}

- (void)tencentDidNotLogin:(BOOL)cancelled
{
    
}

- (void)tencentDidLogin
{
    
}

- (void)tencentDidNotNetWork
{
    
}


@end
