//
//  ZSTLoginController.m
//  F3
//
//  Created by pmit on 15/8/3.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTLoginController.h"
#import "PMRepairButton.h"
#import "ZSTF3RegisterViewController.h"
#import "ZSTF3ClientAppDelegate.h"
#import <SDKExport/WXApi.h>
#import <TencentOpenAPI/TencentOAuth.h>
//#import <libWeiboSDK/WeiboSDK.h>
#import "ZSTRegisterGetCapViewController.h"
#import "WeiboSDK.h"
#import "ZSTAMBindingMobileVC.h"
#import "ZSTNewRegisterViewController.h"
#import <EncryptUtil.h>


@interface ZSTLoginController () <TencentSessionDelegate,WeiboSDKDelegate>

@property (strong,nonatomic) UITextField *userNameTF;
@property (strong,nonatomic) UITextField *userPassTF;
@property (strong,nonatomic) TencentOAuth *tencentOAuth;
@property (strong,nonatomic) NSArray *permissions;
@property (strong,nonatomic) UILabel *tipsLB;

@end

@implementation ZSTLoginController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = YES;
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
//    [self getThirdLoginAuthorizeData];
    
    [self buildLoginView];
  
    PMRepairButton *wxBtn = (PMRepairButton *)[self.view viewWithTag:1000];
    PMRepairButton *qqBtn = (PMRepairButton *)[self.view viewWithTag:1001];
    PMRepairButton *wbBtn = (PMRepairButton *)[self.view viewWithTag:1002];
    
    if (![ZSTF3Preferences shared].isQQLogin || ![QQApi isQQInstalled] || ![QQApi isQQSupportApi])
    {
        qqBtn.hidden = YES;
    }
    else
    {
        qqBtn.hidden = NO;
    }
    
    if (![ZSTF3Preferences shared].isWXLogin || ![WXApi isWXAppInstalled] || ![WXApi isWXAppSupportApi])
    {
        wxBtn.hidden = YES;
    }
    else
    {
        wxBtn.hidden = NO;
    }
    
    if (![ZSTF3Preferences shared].isWBLogin)
    {
        wbBtn.hidden = YES;
    }
    else
    {
        wbBtn.hidden = NO;
    }
    
    if (![ZSTF3Preferences shared].isQQLogin && ![ZSTF3Preferences shared].isWXLogin && ![ZSTF3Preferences shared].isWBLogin)
    {
        self.tipsLB.hidden = YES;
    }
    else if ((![QQApi isQQSupportApi] || ![QQApi isQQInstalled]) && (![WXApi isWXAppSupportApi] || ![WXApi isWXAppInstalled])  && ![ZSTF3Preferences shared].isWBLogin)
    {
        self.tipsLB.hidden = YES;
    }
    else
    {
        self.tipsLB.hidden = NO;
    }
    

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(resignAllFirst:)];
    [self.view addGestureRecognizer:tap];

}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden =  YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildLoginView
{
    UIView *loginView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    loginView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview: loginView];
    
    UIImageView *loginIV = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    if (!iPhone4s) {
        loginIV.contentMode = UIViewContentModeScaleAspectFit;
    }
    loginIV.image = ZSTModuleImage(@"loginBackground.png");
    [loginView addSubview:loginIV];
    
    loginIV.userInteractionEnabled = YES;
    
    UIButton *skipBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [skipBtn setTitle:@"跳过 >>" forState:UIControlStateNormal];
    [skipBtn setTitleColor:RGBA(120, 120, 117, 1) forState:UIControlStateNormal];
    skipBtn.frame = CGRectMake(WIDTH - 90, 30, 68, 30);
    skipBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [skipBtn addTarget:self action:@selector(dismissView) forControlEvents:UIControlEventTouchUpInside];
    [loginIV addSubview:skipBtn];
    
    NSString *welcomeCNString = @"欢 迎 登 录";
//    CGSize welComeCNSize = [welcomeCNString sizeWithFont:[UIFont systemFontOfSize:20.0f] constrainedToSize:CGSizeMake(WidthRate(300), MAXFLOAT)];
    CGSize welComeCNSize = [welcomeCNString boundingRectWithSize:CGSizeMake(WidthRate(300), MAXFLOAT) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:20.0f]} context:nil].size;
    
    UILabel *welcomeLB = [[UILabel alloc] initWithFrame:CGRectMake(23, 30, WidthRate(300), welComeCNSize.height)];
    welcomeLB.text = welcomeCNString;
    welcomeLB.textColor = RGBA(202, 202, 202, 1);
    welcomeLB.font = [UIFont systemFontOfSize:20.0f];
    welcomeLB.textAlignment = NSTextAlignmentLeft;
    [self.view addSubview:welcomeLB];
    
    NSString *welcomeENString = @"welcome login";
    CGSize welcomeENSize = [welcomeENString sizeWithFont:[UIFont systemFontOfSize:14.0f] constrainedToSize:CGSizeMake(WidthRate(300), MAXFLOAT)];
    UILabel *welcomeEgLB = [[UILabel alloc] initWithFrame:CGRectMake(25, 30 + welComeCNSize.height, WidthRate(300), welcomeENSize.height)];
    welcomeEgLB.text = welcomeENString;
    welcomeEgLB.textColor = RGBA(120, 120, 117, 1);
    welcomeEgLB.textAlignment = NSTextAlignmentLeft;
    welcomeEgLB.font = [UIFont systemFontOfSize:14.0f];
    [self.view addSubview:welcomeEgLB];
    
    UIView *loginMessageView = [[UIView alloc] initWithFrame:CGRectMake(25, welcomeEgLB.frame.origin.y + welcomeENSize.height + 30, WIDTH - 50, 120)];
    loginMessageView.backgroundColor = [UIColor clearColor];
    [loginIV addSubview:loginMessageView];
    
    UIImageView *userIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 15, 20, 20)];
    userIV.contentMode = UIViewContentModeScaleAspectFit;
    userIV.image = ZSTModuleImage(@"user.png");
    [loginMessageView addSubview:userIV];
    
    self.userNameTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 13, loginMessageView.bounds.size.width - 40, 25)];
    self.userNameTF.placeholder = @"手机号";
    self.userNameTF.delegate = self;
    self.userNameTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.userNameTF setValue:RGBA(202, 202, 202, 1) forKeyPath:@"_placeholderLabel.textColor"];
    self.userNameTF.textColor = RGBA(202, 202, 202, 1);
    self.userNameTF.keyboardType = UIKeyboardTypeNumberPad;
    [loginMessageView addSubview:self.userNameTF];
    
    UILabel *lineOne = [[UILabel alloc] initWithFrame:CGRectMake(0, 45, loginMessageView.bounds.size.width, 1)];
    lineOne.backgroundColor = RGBA(202, 202, 202, 1);
    [loginMessageView addSubview:lineOne];
    
    UIImageView *passIV = [[UIImageView alloc] initWithFrame:CGRectMake(5, 28 + 46, 20, 20)];
    passIV.contentMode = UIViewContentModeScaleAspectFit;
    passIV.image = ZSTModuleImage(@"lock.png");
    [loginMessageView addSubview:passIV];
    
    self.userPassTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 46 + 28, loginMessageView.bounds.size.width - 40, 25)];
    self.userPassTF.placeholder = @"密码";
    self.userPassTF.delegate = self;
    self.userPassTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    self.userPassTF.secureTextEntry = YES;
    [self.userPassTF setValue:RGBA(202, 202, 202, 1) forKeyPath:@"_placeholderLabel.textColor"];
    self.userPassTF.textColor = RGBA(202, 202, 202, 1);
    [loginMessageView addSubview:self.userPassTF];
    
    UILabel *lineTwo = [[UILabel alloc] initWithFrame:CGRectMake(0, 46 + 28 + 32, loginMessageView.bounds.size.width, 1)];
    lineTwo.backgroundColor = RGBA(202, 202, 202, 1);
    [loginMessageView addSubview:lineTwo];
    
    UIButton *loginBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    loginBtn.frame = CGRectMake(25, loginMessageView.frame.origin.y + loginMessageView.bounds.size.height + 20, WIDTH - 50, 40);
    [loginBtn setBackgroundColor:RGBA(233, 119, 52, 1)];
    [loginBtn setTitle:@"登  录" forState:UIControlStateNormal];
    [loginBtn addTarget:self action:@selector(userLogin:) forControlEvents:UIControlEventTouchUpInside];
    [loginIV addSubview:loginBtn];
    
    UIView *otherView = [[UIView alloc] initWithFrame:CGRectMake(25, loginBtn.frame.origin.y + loginBtn.bounds.size.height + 20, WIDTH - 50, 20)];
    otherView.backgroundColor = [UIColor clearColor];
    [loginIV addSubview:otherView];
    
    UIButton *forgetBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [forgetBtn setTitle:@"忘记密码" forState:UIControlStateNormal];
    [forgetBtn setTitleColor:RGBA(120, 120, 117, 1) forState:UIControlStateNormal];
    forgetBtn.frame = CGRectMake(0, 2, (otherView.bounds.size.width - 2) / 2, 18);
    forgetBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [forgetBtn addTarget:self action:@selector(forgetPass:) forControlEvents:UIControlEventTouchUpInside];
    [otherView addSubview:forgetBtn];
    
    UILabel *lineThree = [[UILabel alloc] initWithFrame:CGRectMake((otherView.bounds.size.width - 2) / 2, 2, 1, 18)];
    lineThree.backgroundColor = RGBA(120, 120, 117, 1);
    [otherView addSubview:lineThree];
    
    UIButton *newsUserBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [newsUserBtn setTitle:@"新用户" forState:UIControlStateNormal];
    [newsUserBtn setTitleColor:RGBA(120, 120, 117, 1) forState:UIControlStateNormal];
    newsUserBtn.frame = CGRectMake((otherView.bounds.size.width - 2) / 2 + 1, 2, (otherView.bounds.size.width - 2) / 2, 18);
    newsUserBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    [newsUserBtn addTarget:self action:@selector(registerNewsUser:) forControlEvents:UIControlEventTouchUpInside];
    [otherView addSubview:newsUserBtn];
    
    UIView *thirdLoginView = [[UIView alloc] initWithFrame:CGRectMake(25, HEIGHT - 120, WIDTH - 50, 100)];
    thirdLoginView.backgroundColor = [UIColor clearColor];
    [loginIV addSubview:thirdLoginView];
    
    // 第三方登录提示
    UILabel *tipsLB = [[UILabel alloc] initWithFrame:CGRectMake(25, 0, thirdLoginView.frame.size.width - 50, 20)];
    tipsLB.text = @"—— 或使用第三方账号登录 ——";
    tipsLB.textColor = RGBA(120, 120, 117, 1);
    tipsLB.textAlignment = NSTextAlignmentCenter;
    tipsLB.font = [UIFont systemFontOfSize:14.0f];
    self.tipsLB = tipsLB;
    [thirdLoginView addSubview:tipsLB];
    
    
    NSArray *thirdImageArr = @[@"wechatlogin.png",@"qqlogin.png",@"weibologin.png"];
    for (NSInteger i = 0; i < thirdImageArr.count; i++)
    {
        //PMRepairButton *thirdBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(i * thirdLoginView.bounds.size.width / 3, 30, thirdLoginView.bounds.size.width / 3, 50)];
        PMRepairButton *thirdBtn = [[PMRepairButton alloc] init];
        thirdBtn.frame = CGRectMake(i * thirdLoginView.bounds.size.width / 3 + 20, 30, 50, 50);
        [thirdBtn setImage:ZSTModuleImage(thirdImageArr[i]) forState:UIControlStateNormal];
        thirdBtn.tag = i + 1000;
        [thirdBtn addTarget:self action:@selector(thirdLogin:) forControlEvents:UIControlEventTouchUpInside];
        [thirdLoginView addSubview:thirdBtn];
    }
}

- (void)loginViewDismiss:(UIButton *)sender
{
//    [self.navigationController popToRootViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];
}

- (void)forgetPass:(UIButton *)sender
{
    
    ZSTNewRegisterViewController *controller = [[ZSTNewRegisterViewController alloc] init];
    controller.registerType = ZSTRegisterTypeFindPass;
    [self.navigationController pushViewController:controller animated:YES];

}

- (void)registerNewsUser:(UIButton *)sender
{
    
    ZSTNewRegisterViewController *controller = [[ZSTNewRegisterViewController alloc] init];
    controller.registerType = ZSTRegisterTypeRegister;
    [self.navigationController pushViewController:controller animated:YES];
    
}

- (void)thirdLogin:(UIButton *)sender
{
    ZSTF3ClientAppDelegate *app = (ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate;
    switch (sender.tag - 1000)
    {
        case 0:
        {
            if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
            {
                [app sendAuthRequest];
            }
            else
            {
                [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有安装微信呢!", nil) withImage:nil];
            }
        }
            break;
            
        case 1:
        {
            if ([QQApi isQQInstalled] && [QQApi isQQSupportApi])
            {
                _tencentOAuth = [[TencentOAuth alloc] initWithAppId:[ZSTF3Preferences shared].qqKey andDelegate:self];
                _tencentOAuth.redirectURI = @"www.qq.com";
                _permissions =  [NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil];
                
                // 向服务器发出认证请求
                [_tencentOAuth authorize:_permissions inSafari:NO];
            }
            else
            {
                [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有安装QQ呢!", nil) withImage:nil];
            }
            
        }
            break;
        case 2:
        {
            NSLog(@"第三方新浪授权登录");
            
            WBAuthorizeRequest *request = [WBAuthorizeRequest request];
            request.redirectURI = [ZSTF3Preferences shared].weiboRedirect;//@"http://www.sina.com"
            request.scope = @"all";
            request.userInfo = @{@"SSO_From": @"ViewController",
                                 @"Other_Info_1": [NSNumber numberWithInt:123],
                                 @"Other_Info_2": @[@"obj1", @"obj2"],
                                 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
            [WeiboSDK sendRequest:request];
        }
            break;
        default:
            break;
    }
}

- (void)userLogin:(UIButton *)sender
{
    [self.userNameTF resignFirstResponder];
    [self.userPassTF resignFirstResponder];
    
    NSString *phoneNumberStr = [self.userNameTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSString *passWordStr = [self.userPassTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (phoneNumberStr.length == 0) {
        [TKUIUtil alertInWindow:@"手机号" withImage:nil];
        return;
    } else if(![self validateMobile:phoneNumberStr]) {
        
        UIAlertView* alertView = [[UIAlertView alloc]
                                  initWithTitle:nil
                                  message:@"您输入的手机号格式有误，请重试"
                                  delegate:self
                                  cancelButtonTitle:@"确定"
                                  otherButtonTitles:nil];
        [alertView show];
        return;
    }
    
    if (passWordStr.length == 0) {
        
        [TKUIUtil alertInWindow:@"密码" withImage:nil];
        return;
    }
    
    [TKUIUtil showHUD:self.view];
    [self.engine loginWithMsisdn:phoneNumberStr password:passWordStr];
}

#pragma mark - 第三方登录返回数据
#pragma mark QQ平台
- (void)getUserInfoResponse:(APIResponse*) response
{
    //NSLog(@"HHHHHHHHH ==== %@", response.jsonResponse);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *openId = [ud objectForKey:@"qqOpenId"];
    NSString *avatar = [response.jsonResponse safeObjectForKey:@"figureurl_qq_1"];
    NSString *nickName = [response.jsonResponse safeObjectForKey:@"nickname"];
    NSString *platformType = @"7";//第三方登录：QQ开放平台
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:openId forKey:@"OpenId"];
    [data setObject:avatar forKey:@"Avatar"];
    [data setObject:nickName forKey:@"NickName"];
    [data setObject:platformType forKey:@"PlatformType"];
    
    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"登录中,请稍后...", nil) withImage:nil];
    [self.engine thirdLoginWithDictionary:data];
}

#pragma mark - 获得第三方授权登录数据
- (void)getThirdLoginAuthorizeData
{
    NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    [self.engine getThirdLoginAuthorizeEcid:ecid];
}

- (BOOL)validateMobile:(NSString *)mobileNum
{
    /**
     * 手机号码
     * 移动：134[0-8],135,136,137,138,139,150,151,157,158,159,182,187,188
     * 联通：130,131,132,152,155,156,185,186
     * 电信：133,1349,153,180,189
     */
    NSString * MOBILE = @"^1(3[0-9]|5[0-35-9]|8[0125-9])\\d{8}$";
    /**
     10         * 中国移动：China Mobile
     11         * 134[0-8],135,136,137,138,139,150,151,157,158,159,182,183,187,188
     12         */
    NSString * CM = @"^1(34[0-8]|(3[5-9]|5[017-9]|8[2378]|7[0-9]|4[0-9])\\d)\\d{7}$";
    /**
     15         * 中国联通：China Unicom
     16         * 130,131,132,152,155,156,185,186
     17         */
    NSString * CU = @"^1(3[0-2]|5[256]|8[56])\\d{8}$";
    /**
     20         * 中国电信：China Telecom
     21         * 133,1349,153,180,189
     22         */
    NSString * CT = @"^1((33|53|8[09])[0-9]|349)\\d{7}$";
    /**
     25         * 大陆地区固话及小灵通
     26         * 区号：010,020,021,022,023,024,025,027,028,029
     27         * 号码：七位或八位
     28         */
    // NSString * PHS = @"^0(10|2[0-5789]|\\d{3})\\d{7,8}$";
    
    NSPredicate *regextestmobile = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", MOBILE];
    NSPredicate *regextestcm = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CM];
    NSPredicate *regextestcu = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CU];
    NSPredicate *regextestct = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", CT];
    
    if (([regextestmobile evaluateWithObject:mobileNum] == YES)
        || ([regextestcm evaluateWithObject:mobileNum] == YES)
        || ([regextestct evaluateWithObject:mobileNum] == YES)
        || ([regextestcu evaluateWithObject:mobileNum] == YES))
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)loginDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    [ZSTF3Preferences shared].loginMsisdn = self.userNameTF.text;
    [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
    
    if ([ZSTF3Engine syncPushNotificationParams])
    {
        NSLog(@"推送绑定成功");
    }
    else
    {
        NSLog(@"推送绑定失败");
    }
    
    [self loginFinish];
}

- (void)loginDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

// 第三方授权登录
- (void)loginAuthorizeDidSucceed:(NSDictionary *)response
{
    NSLog(@"第三方授权登录成功");
    self.tipsLB.hidden = NO;
    PMRepairButton *wxBtn = (PMRepairButton *)[self.view viewWithTag:1000];
    PMRepairButton *qqBtn = (PMRepairButton *)[self.view viewWithTag:1001];
    PMRepairButton *wbBtn = (PMRepairButton *)[self.view viewWithTag:1002];
    // 如果status的值等于1有第三方授权登录，否侧没有
    if ([[response objectForKey:@"status"] integerValue] == 1) {
        NSDictionary *dict = [response objectForKey:@"data"];
        
        NSString *qqKey = @"";
        //        NSString *qqKey = @"";
        NSString *qqSecret = @"";
        
        // 微信
        NSString *wxKey = @"";
        NSString *wxSecret = @"";
        
        // 新浪微博
        NSString *wbKey = @"";
        NSString *wbSecret = @"";
        NSString *wbRedirect = @"";
        
        NSString *wxCode = [dict safeObjectForKey:@"weixinDeveloperAccount"];
        NSString *qqCode = [dict safeObjectForKey:@"qqDeveloperAccount"];
        NSString *wbCode = [dict safeObjectForKey:@"sinaWeiBoDeveloperAccount"];
        NSString *ecid = [[ZSTF3Preferences shared].ECECCID length] == 8 ? [ZSTF3Preferences shared].ECECCID : [[ZSTF3Preferences shared].ECECCID stringByAppendingString:@"wg"];
        NSString *rWXCode = [EncryptUtil decryptUseDES:wxCode key:ecid];
        NSString *rQQCode = [EncryptUtil decryptUseDES:qqCode key:ecid];
        NSString *rWBCode = [EncryptUtil decryptUseDES:wbCode key:ecid];
        
        NSArray *wxArr = [rWXCode componentsSeparatedByString:@"|"];
        NSArray *qqArr = [rQQCode componentsSeparatedByString:@"|"];
        NSArray *wbArr = [rWBCode componentsSeparatedByString:@"|"];
        
        if (wxArr.count > 0 && wxArr[0] && [wxArr[0] isKindOfClass:[NSString class]])
        {
            wxKey = wxArr[0];
            wxSecret = wxArr[1];
        }
        
        if (qqArr.count > 0 && qqArr[0] && [qqArr[0] isKindOfClass:[NSString class]])
        {
            qqKey = qqArr[0];
            qqSecret = qqArr[1];
        }
        
        if (wbArr.count > 0 && wbArr[0] && [wbArr[0] isKindOfClass:[NSString class]])
        {
            wbKey = wbArr[0];
            wbSecret = wbArr[1];
            wbRedirect = wbArr[2];
        }
        
        [ZSTF3Preferences shared].qqKey = qqKey;
        [ZSTF3Preferences shared].qqSecret = qqSecret;
        [ZSTF3Preferences shared].wxKey = wxKey;
        [ZSTF3Preferences shared].wxSecret = wxSecret;
        [ZSTF3Preferences shared].weiboKey = wbKey;
        [ZSTF3Preferences shared].weiboSecret = wbSecret;
        [ZSTF3Preferences shared].weiboRedirect = wbRedirect;
        [ZSTF3Preferences shared].isQQLogin = [[dict safeObjectForKey:@"qq_accredit"] integerValue] == 1 ? YES : NO;
        [ZSTF3Preferences shared].isWBLogin = [[dict safeObjectForKey:@"wb_accredit"] integerValue] == 1 ? YES : NO;
        [ZSTF3Preferences shared].isWXLogin = [[dict safeObjectForKey:@"wx_accredit"] integerValue] == 1 ? YES : NO;
        
        if (![ZSTF3Preferences shared].isQQLogin)
        {
            qqBtn.hidden = YES;
        }
        else
        {
            qqBtn.hidden = NO;
        }
        
        if (![ZSTF3Preferences shared].isWXLogin)
        {
            wxBtn.hidden = YES;
        }
        else
        {
            wxBtn.hidden = NO;
        }
        
        if (![ZSTF3Preferences shared].isWBLogin)
        {
            wbBtn.hidden = YES;
        }
        else
        {
            wbBtn.hidden = NO;
        }
    }
    else
    {
        self.tipsLB.hidden = YES;
        qqBtn.hidden = YES;
        wxBtn.hidden = YES;
        wbBtn.hidden = YES;
    }
}

- (void)loginAuthorizeDidFailed:(NSString *)response
{
    NSLog(@"123");
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

// 第三方登录QQ
- (void)thirdLoginDidSucceed:(NSDictionary *)response
{
    NSLog(@"第三方登录成功");
    //NSLog(@"response === %@", response);
    NSString *userId = [[response objectForKey:@"data"] objectForKey:@"UserId"];
//    [[NSUserDefaults standardUserDefaults] setObject:userId forKey:@"UserId"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
    [ZSTF3Preferences shared].UserId = userId;
    [ZSTF3Preferences shared].loginMsisdn = [[response objectForKey:@"data"] objectForKey:@"Msisdn"];
    [self loginFinish];
}

- (void)thirdLoginDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)loginFinish
{
    if (_delegate && [_delegate respondsToSelector:@selector(loginDidFinish)]) {
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
        
        [_delegate loginDidFinish];
        return;
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.userNameTF) {
        
        if ([self.userNameTF.text isEqualToString:@""])
        {
            self.userNameTF.text = @"";
        }
    }
    
    if (textField == self.userPassTF) {
        
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField == self.userNameTF && textField.text.length - range.length + string.length > 11) || (textField == self.userNameTF && [textField.text rangeOfString:@"emoji"].length > 0)) {
        return NO;
    }
    
    if (textField == self.userPassTF && [textField.text rangeOfString:@"emoji"].length > 0) {
        return NO;
    }
    
    return YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.userNameTF resignFirstResponder];
    [self.userPassTF resignFirstResponder];
}

- (void)resignAllFirst:(UITapGestureRecognizer *)tap
{
    [self.userNameTF resignFirstResponder];
    [self.userPassTF resignFirstResponder];
}

-(IBAction)dismissView
{
    if (_delegate && [_delegate respondsToSelector:@selector(loginDidCancel)]) {
        
        [self.navigationController dismissViewControllerAnimated:YES completion:^(void) {
        }];
        
        [_delegate loginDidCancel];
        
        return;
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (void)tencentDidLogin
{
    if (_tencentOAuth.accessToken && 0 != [_tencentOAuth.accessToken length])
    {
        //  记录登录用户的OpenID、Token以及过期时间
        //_labelAccessToken.text = _tencentOAuth.accessToken;
        NSLog(@"_tencentOAuth.accessToken==>%@",_tencentOAuth.accessToken);
        NSLog(@"_tencentOAuth.openid==>%@",_tencentOAuth.openId);
        [ZSTF3Preferences shared].qqOpenId = _tencentOAuth.openId;
        [[NSUserDefaults standardUserDefaults] setValue:_tencentOAuth.openId forKey:@"qqOpenId"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"qqLoginSuccess" object:nil];
        
        [_tencentOAuth getUserInfo];
        
    }
    else
    {
        NSLog(@"登录不成功 没有获取accesstoken");
    }
}

@end
