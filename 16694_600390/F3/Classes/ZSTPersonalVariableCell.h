//
//  ZSTPersonalVariableCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ZSTPersonalVariableCell;

@interface ZSTPersonalVariableCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;

@property (nonatomic, strong) UIImageView *rightImgView;


@end
