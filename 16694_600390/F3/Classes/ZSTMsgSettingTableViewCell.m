//
//  ZSTMsgSettingTableViewCell.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTMsgSettingTableViewCell.h"

@implementation ZSTMsgSettingTableViewCell

- (void)awakeFromNib
{
//    _switchBtn = [[ZSTCustomSwitch alloc] initWithFrame:CGRectMake(235, 15, 49, 24)];
    _switchBtn = [[PMCustomSwitch alloc] initWithFrame:CGRectMake(235, 15, 49, 24)];
    _switchBtn.delegate = self;
//    _switchBtn.arrange = ZSTCustomSwitchArrangeOFFLeftONRight;
//    _switchBtn.onImage = ZSTModuleImage(@"module_personal_on.png");
//    _switchBtn.offImage = ZSTModuleImage(@"module_personal_off.png");
    _switchBtn.switchType = PMCustomSwitchStatusOn;
    _switchBtn.customSwitch.on = YES;
    [self.contentView addSubview:_switchBtn];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

# pragma mark - customSwitch delegate
-(void)zstcustomSwitchSetStatus:(PMCustomSwitchStatus)status
{
//    switch (status) {
//        case CustomSwitchStatusOn:
//            
//            TTLog(@"---------- %d",status);
//            break;
//        case CustomSwitchStatusOff:
//            
//            break;
//        default:
//            break;
//    }
    
    if ([_delegate respondsToSelector:@selector(cell:set:)]) {
        [_delegate cell:self set:status];
    }

}


@end
