//
//  ZSTMineViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-27.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTMineViewController.h"
#import "ZSTSettingCell.h"
#import "ZSTSuggestedQuestionsViewController.h"
#import "ZSTWebViewController.h"
#import "ZSTAboutViewController.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTWebViewController.h"
#import "ZSTF3Engine.h"
#import "ZSTShell.h"
#import "ZSTModuleAddition.h"
//#import "ZSTAccountViewController.h"
#import "ZSTAccountManagementVC.h"
#import "ZSTPersonalViewController.h"
#import "ZSTInformViewController.h"
#import "ZSTShareSettingViewController.h"
#import "ZSTPersonalExitTableViewCell.h"
#import "ZSTPersonalInfo.h"
#import "ZSTF3RegisterViewController.h"
#import "ZSTPersonalWebViewController.h"
#import "ZSTPasswordRemindViewController.h"
#import "BaseNavgationController.h"
#import "ZSTRegisterGetCapViewController.h"
#import "ZSTPersonalAboutViewController.h"
#import "ZSTPersonInfoViewController.h"
#import "ZSTSettingViewController.h"
#import <EncryptUtil.h>

#define  kZSTSettingItemType_Register       @"Register"
#define  kZSTSettingItemType_Share          @"Share"
#define  kZSTSettingItemType_Setting        @"Setting"
#define  kZSTSettingItemType_Advice         @"Suggest"
#define  kZSTSettingItemType_Version        @"Update"
#define  kZSTSettingItemType_About          @"About"

#define  kZSTPersonalItem_Exit                   8

@implementation ZSTSettingItems

@synthesize title;
@synthesize type;

+(ZSTSettingItems *)itemWithTitle:(NSString *)aTitle type:(NSString *)aType
{
    ZSTSettingItems *item = [[ZSTSettingItems alloc] init];
    item.title = aTitle;
    item.type = aType;
    return [item autorelease];
}

- (void)dealloc
{
//    [TKUIUtil hiddenHUD];
    self.type = nil;
    self.title = nil;
    [super dealloc];
}

@end


@interface ZSTMineViewController ()<TKAsynImageViewDelegate>
{
    BOOL islogin;
    BOOL isalert;
    
    NSString *sharePoint;
    
    int passwordStatus;
    
    UserInfo *userInfo;
    
    BOOL isHasPhone;
}

@property (strong,nonatomic) NSDictionary *infoDic;

@end

@implementation ZSTMineViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"我的", nil)];
    
    [self initSettingItems];
    [self refreshArray];
    
    UIImage *imageN = ZSTModuleImage(@"module_login_btn_green_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_login_btn_green_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.signbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.signbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    
    [self.loginbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.loginbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    
    _tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 140, 320, 480-20+(iPhone5?88:0)-140-44) style:UITableViewStyleGrouped];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.backgroundView = nil;
    _tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    _tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    //ios6 bug  必须要这样写， 否则底部会有空隙
//    _tableView.tableFooterView = [[[UIView alloc] initWithFrame:CGRectMake(0.0f,0.0f,1.0f,1.0f)] autorelease];
   	_tableView.dataSource = self;
    _tableView.delegate = self;
    [self.view addSubview:_tableView];
    
//    self.avaterImgView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(20, 15, 55, 55)];
    self.avaterImgView = [[UIImageView alloc] initWithFrame:CGRectMake(20, 15, 55, 55)];
//    self.avaterImgView.asynImageDelegate = self;
    [self.loginView addSubview:self.avaterImgView];
    self.avaterImgView.layer.cornerRadius = 6;
    self.avaterImgView.layer.masksToBounds = YES;
    [self.view bringSubviewToFront:self.showbtn];
    
    [self.view bringSubviewToFront:self.clickbtn];
    
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
    [self getThirdLoginAuthorizeData];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([ZSTF3Preferences shared].UserId && [ZSTF3Preferences shared].UserId.length > 0) {
        [TKUIUtil showHUD:self.view];
        NSString *loginMsisdn = @"";
        if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"4"])
        {
            loginMsisdn = @"";
        }
        else
        {
            loginMsisdn = [ZSTF3Preferences shared].loginMsisdn;
        }
        
        [self.engine getUserInfoWithMsisdn:loginMsisdn userId:[ZSTF3Preferences shared].UserId];
        
        if (![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_APP]) {
            
            [self.engine checkPasswordWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
        }
    }
    
    [_tableView reloadData];
    
     isalert = YES;
    
    self.navigationController.navigationBarHidden = NO;
    
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
        self.loginView.hidden = YES;
        self.unLoginView.hidden = NO;
        islogin = NO;
        self.registerbtn.hidden = NO;
        self.linkLabel.hidden = NO;
        if ([ZSTF3Preferences shared].CarrierType == 10) {
            
            self.registerbtn.hidden = YES;
            self.linkLabel.hidden = YES;
            
            CGRect frame = self.loginbtn.frame;
            frame.origin.x = (self.view.bounds.size.width - frame.size.width) / 2;
            self.loginbtn.frame = frame;
        }
        
    } else {
        
        self.loginView.hidden = NO;
        self.unLoginView.hidden = YES;
        islogin = YES;
    }
    
//     [self.engine checkClientVersion];
}

- (IBAction)loginAction:(id)sender
{
//    ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
//    controller.isFromSetting = YES;
//    controller.delegate = self;
    ZSTLoginController *controller = [[ZSTLoginController alloc] init];
    controller.isFromSetting = YES;
    controller.delegate = self;
    BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
    
    [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
    }];
    [navicontroller release];
    [controller release];
}

- (IBAction)registerAction:(id)sender
{
    ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] initWithNibName:@"ZSTF3RegisterViewController" bundle:nil];
    controller.isfromSetting = YES;
    controller.type = 1;
    BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
    
    [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
    }];
    [navicontroller release];
    [controller release];
}

- (IBAction)clickAction:(id)sender
{
//    ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
//    controller.isFromSetting = YES;
//    controller.delegate = self;
    ZSTLoginController *controller = [[ZSTLoginController alloc] init];
    controller.isFromSetting = YES;
    controller.delegate = self;
    BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
    
    [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
    }];
    [navicontroller release];
    [controller release];
}

- (IBAction)pointDetail:(id)sender
{
    if ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0)
    {
//        ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
//        controller.isFromSetting = YES;
//        controller.delegate = self;
        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
        controller.isFromSetting = YES;
        controller.delegate = self;
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        return;
    }
    
    ZSTPersonalWebViewController *controller = [[ZSTPersonalWebViewController alloc] init];
    [controller setURL:[NSString stringWithFormat:PointDetail,[ZSTF3Preferences shared].ECECCID,[ZSTF3Preferences shared].UserId,[ZSTF3Preferences shared].platform,[ZSTF3Preferences shared].loginMsisdn]];
    controller.hidesBottomBarWhenPushed = YES;
    controller.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];

}

- (IBAction)signAction:(id)sender
{
    [TKUIUtil showHUD:self.view];
    [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                userId:[ZSTF3Preferences shared].UserId
                              pointNum:[self.pointLabel.text intValue]
                             pointType:0
                            costAmount:@""
                                  desc:@""];
}

- (void)loginDidFinish
{
     [self.engine getUserInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
    [_tableView reloadData];
}

#pragma mark - helper

- (void)initSettingItems
{
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    if ([GP_Setting_Items isEqualToString:@"GP_Setting_Items"]) {
        GP_Setting_Items = @"Register,Share,Setting,Suggest,Update,About";
    }
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    _settingItems = [[[GP_Setting_Items componentsSeparatedByString:@","] mutableCopy] retain];
    
    NSString *GP_Setting_UrlItems = NSLocalizedString(@"GP_Setting_UrlItems", nil);
    if ([GP_Setting_UrlItems isEqualToString:@"GP_Setting_UrlItems"]) {
        GP_Setting_UrlItems = @"";
    }
    
    _urlSettingItems = [[NSMutableArray alloc] init];
    _urlSettingItemDict = [[NSMutableDictionary alloc] init];
    
    NSArray *array = [GP_Setting_UrlItems componentsSeparatedByString:@","];
    for (NSString *str in array) {
        
        NSRange range = [str rangeOfString:@":"];
        if (range.location != NSNotFound) {
            
            NSString *name = [str substringToIndex:range.location];
            NSString *url = [str substringFromIndex:range.location + 1];
            
            name = [name stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            url = [url stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            [_urlSettingItems addObject:name];
            [_urlSettingItemDict setObject:url forKey:name];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 3) {
        return 35;
    }
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (section == 3) {
        
        return 1;
    }
    
    NSArray *array = [_tableViewDataArray objectAtIndex:section];
    
    return array.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView              // Default is 1 if not implemented
{
    if (!islogin) {
        
        return [_tableViewDataArray count];
    }
    return ([_tableViewDataArray count] + 1);
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *exitidentifier = @"ZSTPersonalExitTableViewCell";
    
    if (indexPath.section == 3) {
        
        ZSTPersonalExitTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:exitidentifier];
        
        if (!cell) {
            
            NSArray* array = [[UINib nibWithNibName:exitidentifier bundle:nil] instantiateWithOwner:self options:nil];
            cell = [array objectAtIndex:0];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            [cell.exitbtn addTarget:self action:@selector(exitAction:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        cell.tag = indexPath.row;
        
        return cell;
    }
    
    ZSTSettingCell *cell = [[[ZSTSettingCell alloc] initWithStyle:UITableViewCellStyleDefault
                                                  reuseIdentifier:nil] autorelease];
    cell.customBackgroundColor = [UIColor whiteColor];
    cell.customSeparatorStyle = UITableViewCellSeparatorStyleSingleLine;
    cell.cornerRadius = 4.0;
    [cell prepareForTableView:tableView indexPath:indexPath];
    NSArray *array = [_tableViewDataArray objectAtIndex:indexPath.section];
    ZSTSettingItems *item = [array objectAtIndex:indexPath.row];
    cell.textLabel.text =  [NSString stringWithFormat:@"  %@",item.title];
    cell.textLabel.font = [UIFont systemFontOfSize:14];
    cell.textLabel.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    [cell initWithData];
    
    if (indexPath.section == 0) {
        
        if (passwordStatus == 1) {
            
            UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(190+(IS_IOS_7?10:0), 15, 80, 21)];
            rightLabel.backgroundColor = RGBCOLOR(206, 18, 24);
            rightLabel.textColor = [UIColor whiteColor];
            rightLabel.font = [UIFont systemFontOfSize:11];
            rightLabel.text = @"未设置密码";
            rightLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.clipsToBounds = YES;
            rightLabel.layer.cornerRadius = 10;//设置那个圆角的有多圆
            rightLabel.layer.masksToBounds = YES;//设为NO去试试
            [cell.contentView addSubview:rightLabel];
            [rightLabel release];

        }
    
    }else if (indexPath.section == 1) {
        
        if (![ZSTF3Preferences shared].smsShare || ![ZSTF3Preferences shared].SinaWeiBoShare || ![ZSTF3Preferences shared].WX_Friend || ![ZSTF3Preferences shared].WX_FriendsCircle) {
            UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(190+(IS_IOS_7?10:0), 15, 80, 21)];
            rightLabel.backgroundColor = RGBCOLOR(206, 18, 24);
            rightLabel.textColor = [UIColor whiteColor];
            rightLabel.font = [UIFont systemFontOfSize:11];
            rightLabel.text = @"分享赚积分";
            rightLabel.textAlignment = NSTextAlignmentCenter;
            rightLabel.clipsToBounds = YES;
            rightLabel.layer.cornerRadius = 10;//设置那个圆角的有多圆
            rightLabel.layer.masksToBounds = YES;//设为NO去试试
            [cell.contentView addSubview:rightLabel];
            [rightLabel release];
        }
    } else if (indexPath.section == 2) {
        
//        if (indexPath.row == 2) {
//            
//            UILabel *rightLabel = [[UILabel alloc] initWithFrame:CGRectMake(220+(IS_IOS_7?10:0), 15, 50, 21)];
//            rightLabel.backgroundColor = RGBCOLOR(206, 18, 24);
//            rightLabel.textColor = [UIColor whiteColor];
//            rightLabel.font = [UIFont systemFontOfSize:11];
//            NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//            rightLabel.text = version;
//            rightLabel.textAlignment = NSTextAlignmentCenter;
//            rightLabel.clipsToBounds = YES;
//            rightLabel.layer.cornerRadius = 10;//设置那个圆角的有多圆
//            rightLabel.layer.masksToBounds = YES;//设为NO去试试
//            [cell.contentView addSubview:rightLabel];
//            cell.selectionStyle = UITableViewCellSelectionStyleNone;
//            [rightLabel release];
//        }
    }
    
    return cell;
}

//section头部间距
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 2;//section头部高度
}
//section头部视图
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 2)];
    view.backgroundColor = [UIColor clearColor];
    return [view autorelease];
}
//section底部间距
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 2;
}
//section底部视图
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 2)];
    view.backgroundColor = [UIColor clearColor];
    return [view autorelease];
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    NSArray *array = [_tableViewDataArray objectAtIndex:indexPath.section];
    ZSTSettingItems *item = [array objectAtIndex:indexPath.row];
    
    if ([item.type isEqualToString:kZSTSettingItemType_Register])
    {
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
            
//            ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
//            controller.isFromSetting = YES;
//            controller.delegate = self;
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
            
            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            [navicontroller release];
            [controller release];
            
            return;
        }
        
        ZSTAccountManagementVC *controller = [[ZSTAccountManagementVC alloc] init];
        controller.passwordStatus = passwordStatus;
        controller.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
        
    } else if ([item.type isEqualToString:kZSTSettingItemType_Advice])
    {
        ZSTSuggestedQuestionsViewController *suggested_questions = [[ZSTSuggestedQuestionsViewController alloc] init];
        [self.navigationController pushViewController:suggested_questions animated:YES];
        [suggested_questions release];
    }
//    else if ([item.type isEqualToString:kZSTSettingItemType_Version])
//    {
//        if ([[ZSTF3Preferences shared].loginMsisdn isEqualToString:@""] || [ZSTF3Preferences shared].loginMsisdn == nil) {
//            
//            [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"    尚未绑定\n不能检查更新", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
//            [TKUIUtil hiddenHUDAfterDelay:2];
//            return;
//        }
        
//        [self.engine checkClientVersion];
//        [self.engine enUpdateECMobileClientParams];
//        
//        [ZSTLogUtil logUserAction:@"CheckForUpdates"];
//    }
    else if ([item.type isEqualToString:kZSTSettingItemType_About])
    {
        NSString *settingAbout = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"setting_about"];
        NSRange range = [settingAbout rangeOfString:@","];
        if ([settingAbout hasPrefix:@"http://"] && range.location == NSNotFound) {
            ZSTWebViewController *webView = [[ZSTWebViewController alloc] init];
            [webView setURL:settingAbout];
            webView.type = self.moduleType;
            webView.hidesBottomBarWhenPushed = YES;
            webView.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
            [self.navigationController pushViewController:webView animated:YES];
            [webView release];
        }else{
//            ZSTAboutViewController *aboutview = [[ZSTAboutViewController alloc] init];
//            aboutview.hidesBottomBarWhenPushed = YES;
//            [self.navigationController pushViewController:aboutview animated:YES];
//            [aboutview release];
            ZSTPersonalAboutViewController *personalAboutVC = [[ZSTPersonalAboutViewController alloc] init];
            [self.navigationController pushViewController:personalAboutVC animated:YES];
        }
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_Setting]) {
        
        ZSTInformViewController *controller = [[ZSTInformViewController alloc] initWithNibName:@"ZSTInformViewController" bundle:nil];
        controller.userInfo = userInfo;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
    }
    else if ([item.type isEqualToString:kZSTSettingItemType_Share]) {
        
        if (![ZSTF3Preferences shared].UserId || [ZSTF3Preferences shared].UserId.length == 0) {
            
            ZSTLoginController *controller = [[ZSTLoginController alloc] init];
            controller.isFromSetting = YES;
            controller.delegate = self;
            BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
            
            [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
            }];
            [navicontroller release];
            [controller release];
            
            return;
        }
//        else if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0)
//        {
//            [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没绑定账号,先去绑定账号吧", nil) withImage:nil];
//            return;
//        }
        
        [self.engine getPointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
    }
}

- (void)refreshArray
{
    _tableViewDataArray = [[NSMutableArray alloc] init];
    
    if ([_settingItems containsObject:kZSTSettingItemType_Register]) {
        NSString *title = NSLocalizedString(@"帐号管理", nil);
        [_tableViewDataArray addObject:[NSArray arrayWithObjects:[ZSTSettingItems itemWithTitle:title type:kZSTSettingItemType_Register], nil]];
    }
    
    NSMutableArray *array = [NSMutableArray array];
    
    if ([_settingItems containsObject:kZSTSettingItemType_Share]) {
        [array addObject:[ZSTSettingItems itemWithTitle:NSLocalizedString(@"分享", nil) type:kZSTSettingItemType_Share]];
    }
    
    [_tableViewDataArray addObject:array];
    
    array = [NSMutableArray array];
    
   
    if ([_settingItems containsObject:kZSTSettingItemType_Setting]) {
        [array addObject:[ZSTSettingItems itemWithTitle:NSLocalizedString(@"设置", nil) type:kZSTSettingItemType_Setting]];
    }
    
    if ([_settingItems containsObject:kZSTSettingItemType_Advice]) {
        [array addObject:[ZSTSettingItems itemWithTitle:NSLocalizedString(@"建议", nil) type:kZSTSettingItemType_Advice]];
    }
    
//    if ([_settingItems containsObject:kZSTSettingItemType_Version]) {
//        [array addObject:[ZSTSettingItems itemWithTitle:NSLocalizedString(@"当前版本号", nil) type:kZSTSettingItemType_Version]];
//    }
    

    if ([_settingItems containsObject:kZSTSettingItemType_About]) {
        [array addObject:[ZSTSettingItems itemWithTitle:NSLocalizedString(@"关于", nil) type:kZSTSettingItemType_About]];
    }
    
    [_tableViewDataArray addObject:array];
}

- (void) setUserInfo:(UserInfo *)info
{
    if (!info) {
        
        return;
    }
    
    if (info.userName && ![info.userName isEqualToString:@""]) {
        self.userNameLabe.text = info.userName;
    }else{
        self.userNameLabe.text = @"";
    }
    
    if (isHasPhone) {
        self.accountLabel.text = [NSString stringWithFormat:@"账号：%@",info.Msisdn];
    }else{
        self.accountLabel.text = @"还没设置账号";
    }
    self.pointLabel.text = info.pointNum;
    
//    [self.avaterImgView clear];
//    self.avaterImgView.url = [NSURL URLWithString:info.iconUrl];
//    [self.avaterImgView loadImage];
    [self.avaterImgView setImageWithURL:[NSURL URLWithString:info.iconUrl]];
    
    if  (info.iconUrl && info.iconUrl.length > 0) {
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:info.iconUrl]]];
        NSString *fileName = [info.iconUrl substringToIndex:(info.iconUrl.length-4)];
        NSArray *strArray = [fileName componentsSeparatedByString:@"/"];
        NSString *filename = [strArray lastObject];
        NSString *path = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",filename]];
        [UIImagePNGRepresentation(image) writeToFile:path atomically:YES];
        
        NSMutableDictionary *dic = [[NSMutableDictionary alloc] init];
        [dic setSafeObject:path forKey:[ZSTF3Preferences shared].loginMsisdn];
        
        [ZSTF3Preferences shared].avaterName = dic;
        
        [dic release];
    }
    
    if (info.signIn == 1) {
        
        self.signbtn.enabled = NO;
        UIImage *imageN = ZSTModuleImage(@"module_personal_btn_gray_n.png");
        UIImage *ImageP = ZSTModuleImage(@"module_personal_btn_gray_p.png");
        CGFloat capWidth = imageN.size.width / 2;
        CGFloat capHeight = imageN.size.height / 2;
        imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
        ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
        [self.signbtn setBackgroundImage:imageN forState:UIControlStateNormal];
        [self.signbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
        [self.signbtn setTitle:@"已签到" forState:UIControlStateNormal];
        [self.signbtn setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
    } else {
        
        self.signbtn.enabled = YES;
        UIImage *imageN = ZSTModuleImage(@"module_login_btn_green_n.png");
        UIImage *ImageP = ZSTModuleImage(@"module_login_btn_green_p.png");
        CGFloat capWidth = imageN.size.width / 2;
        CGFloat capHeight = imageN.size.height / 2;
        imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
        ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
        [self.signbtn setBackgroundImage:imageN forState:UIControlStateNormal];
        [self.signbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
        [self.signbtn setTitle:@"签到" forState:UIControlStateNormal];
        [self.signbtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
}

-(void)UpdateStingsView
{
    [self refreshArray];
    [_tableView reloadData];
}

- (void)asynImageViewDidClicked:(TKAsynImageView *)asynImageView
{
//    ZSTPersonalViewController *controller = [[ZSTPersonalViewController alloc] initWithNibName:@"ZSTPersonalViewController" bundle:nil];
//    [controller setBlock:^(NSString *nickname,int type) {
//        
//        if (type == 1) {
//            
////            [self.avaterImgView clear];
////            self.avaterImgView.url = [NSURL URLWithString:nickname];
////            [self.avaterImgView loadImage];
//            [self.avaterImgView setImageWithURL:[NSURL URLWithString:nickname]];
//            
//        } else if (type == 2) {
//            self.userNameLabe.text = nickname;
//        }
//        
//    }];
//    
//    [self.navigationController pushViewController:controller animated:YES];
//    [controller release];
    
    ZSTPersonInfoViewController *infoVC = [[ZSTPersonInfoViewController alloc] init];
    infoVC.infoDic = self.infoDic;
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (IBAction)personalAction:(id)sender
{
//    ZSTPersonalViewController *controller = [[ZSTPersonalViewController alloc] initWithNibName:@"ZSTPersonalViewController" bundle:nil];
//    [controller setBlock:^(NSString *nickname,int type) {
//        
//        if (type == 1) {
//            
////            [self.avaterImgView clear];
////            self.avaterImgView.url = [NSURL URLWithString:nickname];
////            [self.avaterImgView loadImage];
//            [self.avaterImgView setImageWithURL:[NSURL URLWithString:nickname]];
//            
//        } else if (type == 2) {
//            self.userNameLabe.text = nickname;
//        }
//        
//    }];
//    [self.navigationController pushViewController:controller animated:YES];
//    [controller release];
    ZSTPersonInfoViewController *infoVC = [[ZSTPersonInfoViewController alloc] init];
    infoVC.infoDic = self.infoDic;
    [self.navigationController pushViewController:infoVC animated:YES];
}

- (void)exitAction:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示" message:@"亲～，确定登出吗？" delegate:self cancelButtonTitle:@"取消" otherButtonTitles:@"确定", nil];
    [alert show];
    [alert release];
}

- (void)getUserInfoDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    userInfo = [UserInfo userInfoWithdic:[response safeObjectForKey:@"data"]];
    [ZSTF3Preferences shared].snsaStates = userInfo.phonePublic;
    if ([[[response safeObjectForKey:@"data"] safeObjectForKey:@"HasPhone"] integerValue] == 0)
    {
        isHasPhone = YES;
    }
    else
    {
        isHasPhone = NO;
    }
    [self setUserInfo:userInfo];
    self.infoDic = [response safeObjectForKey:@"data"];
    
    if ([[response safeObjectForKey:@"data"] safeObjectForKey:@"weixinopen_openid"] && [[[response safeObjectForKey:@"data"] safeObjectForKey:@"weixinopen_openid"] isKindOfClass:[NSString class]] && ![[[response safeObjectForKey:@"data"] objectForKey:@"weixinopen_openid"] isEqualToString:@""])
    {
        [ZSTF3Preferences shared].weixinOpenId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"weixinopen_openid"];
    }
    
    if ([[response safeObjectForKey:@"data"] safeObjectForKey:@"qq_openid"] && [[[response safeObjectForKey:@"data"] safeObjectForKey:@"qq_openid"] isKindOfClass:[NSString class]] && ![[[response safeObjectForKey:@"data"] objectForKey:@"qq_openid"] isEqualToString:@""])
    {
        [ZSTF3Preferences shared].qqOpenId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"qq_openid"];
        NSLog(@"qqOpenId --> %@",[ZSTF3Preferences shared].qqOpenId);
    }
    
    if ([[response safeObjectForKey:@"data"] safeObjectForKey:@"sina_openid"] && [[[response safeObjectForKey:@"data"] safeObjectForKey:@"sina_openid"] isKindOfClass:[NSString class]] && ![[[response safeObjectForKey:@"data"] objectForKey:@"sina_openid"] isEqualToString:@""])
    {
        [ZSTF3Preferences shared].sinaOpenId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"sina_openid"];
    }
}

- (void)getUserInfoDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1024) {
        
        if(buttonIndex == 0)
        {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:_clientVersionUrl]];
        }
    } else {
        if (buttonIndex != alertView.cancelButtonIndex) {
            
            [self.engine logoutWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
           
            [ZSTF3Preferences shared].UserId = @"";
            [ZSTF3Preferences shared].UID = @"";
            [ZSTF3Preferences shared].isChange = YES;
            
            if ([ZSTF3Preferences shared].MCRegistType != MCRegistType_VirtualRegister) {
//                ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
//                controller.delegate = self;
//                controller.isFromSetting = YES;
                ZSTLoginController *controller = [[ZSTLoginController alloc] init];
                controller.isFromSetting = YES;
                controller.delegate = self;
                BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
                [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
                }];
                [controller release];
                [navicontroller release];
            } else {
                
                [self.navigationController popToRootViewControllerAnimated:YES];
            }
        }
    }
}

- (void)updatePointDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    NSString *message = [[response safeObjectForKey:@"data"] safeObjectForKey:@"PointNum"];
    NSString *string = [NSString stringWithFormat:@"恭喜你获得%@积分，明天继续哦",message];
    [self showAlertWithMsg:string];
    
    int costNum = [message intValue];
    int originalNum = [self.pointLabel.text intValue];
    int currentNum = originalNum + costNum;
    self.pointLabel.text = [NSString stringWithFormat:@"%d",currentNum];
    
    self.signbtn.enabled = NO;
    UIImage *imageN = ZSTModuleImage(@"module_personal_btn_grey_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_personal_btn_grey_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.signbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.signbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    [self.signbtn setTitle:@"已签到" forState:UIControlStateNormal];

}

- (void)updatePointDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
//    [TKUIUtil alertInWindow:response withImage:nil];
    NSString *string = response;
    [self showAlertWithMsg:string];
}

- (void)getPointDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    sharePoint = [[response safeObjectForKey:@"data"] safeObjectForKey:@"SharePoint"];
    
    if ([sharePoint intValue] == 0) {
        
        sharePoint = @"0";
    }
    
    ZSTShareSettingViewController *controller = [[ZSTShareSettingViewController alloc] initWithNibName:@"ZSTShareSettingViewController" bundle:nil];
    [controller setBlocked:^(int count) {
        
        int pointcount = [self.pointLabel.text intValue];
        pointcount += count;
        
        self.pointLabel.text = [NSString stringWithFormat:@"%d",pointcount];
        
    }];
    controller.sharePoint = sharePoint;
    [self.navigationController pushViewController:controller animated:YES];
    [controller release];
}

- (void)getPointDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)logoutDidSucceed:(NSDictionary *)response
{
    
}

- (void)logoutDidFailed:(NSString *)response
{
    
}

- (void)checkPasswordDidSucceed:(NSDictionary *)response
{
    [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_APP];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    passwordStatus = [[response safeObjectForKey:@"status"] intValue];
    
    if (passwordStatus == 1 && ![[NSUserDefaults standardUserDefaults] valueForKey:FIRST_LAUNCH_SETTING]) {
        
        ZSTPasswordRemindViewController *controller = [[ZSTPasswordRemindViewController alloc] initWithNibName:@"ZSTPasswordRemindViewController" bundle:nil];
        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
        
        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
        }];
        [navicontroller release];
        [controller release];
        
        [_tableView reloadData];
        return;

    }
}

- (void)checkPasswordDidFailed:(NSString *)response
{
    
}

- (void) showAlertWithMsg:(NSString *)string
{
    UIView *promptView = [[UIView alloc] init];
    promptView.frame = CGRectMake(40, 180, 240, 75);
    promptView.backgroundColor = [UIColor whiteColor];
    promptView.layer.cornerRadius = 8;
    promptView.layer.masksToBounds = YES;
    promptView.layer.borderWidth = 1;
    promptView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    [self.view.window addSubview:promptView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 200, 18)];
    textLabel.numberOfLines = 0;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textAlignment = NSTextAlignmentCenter;
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(200, 50) lineBreakMode:NSLineBreakByCharWrapping];
    textLabel.frame = CGRectMake(20, 20, 200, size.height);
    textLabel.text = string;
    [promptView addSubview:textLabel];
    [textLabel release];
    
    [UIView animateWithDuration:0.2
                          delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         promptView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:0.8
                                               delay:0.8
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              promptView.alpha = 0;
                                          }
                                          completion:^(BOOL finished) {
                                          }
                          ];
                     }
     ];
    
    [promptView release];
}

- (void)checkClientVersionResponse:(NSString *)versionURL updateNote:(NSString *)updateNote updateVersion:(int)updateVersion
{
    if (!isalert) {
        if (versionURL != nil && [versionURL length] != 0) {
            
            _clientVersionUrl = [[NSString alloc] initWithFormat:@"%@",versionURL];
        
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"检查到有最新版本", nil)
                                                             message:[updateNote isEmptyOrWhitespace]? NSLocalizedString(@"现在就去更新吗？", nil):updateNote
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"我要更新", nil)
                                                   otherButtonTitles:NSLocalizedString(@"稍候更新", nil),nil];
            alert.tag = 1024;
            [alert show];
            [alert release];
            
        }else{
            
            [_clientVersionUrl release];
            _clientVersionUrl = nil;
            [ZSTUtils showAlertTitle:NSLocalizedString(@"检查完毕", nil) message:NSLocalizedString(@"当前客户端为最新版本", nil)];
        }
    }
    
    isalert = NO;
    
    [_tableView reloadData];
}

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
//    [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"网络连接失败", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]];
//    [TKUIUtil hiddenHUDAfterDelay:2];
//    
    isalert = NO;
    
    [_tableView reloadData];
}

- (void) viewDidUnload
{
    [super viewDidUnload];
    self.avaterImgView = nil;
    self.userNameLabe = nil;
    self.accountLabel = nil;
    self.pointLabel = nil;
    self.signbtn = nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    self.avaterImgView = nil;
    self.userNameLabe = nil;
    self.accountLabel = nil;
    self.pointLabel = nil;
    self.signbtn = nil;
    self.showbtn = nil;
    
    [_urlSettingItems release];
    [_urlSettingItemDict release];
    [_settingItems release];
    [_clientVersionUrl release];
    [_tableView release];
    [_tableViewDataArray release];
    [self.engine cancelAllRequest];
    [super dealloc];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}

- (void)getThirdLoginAuthorizeData
{
    NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    [self.engine getThirdLoginAuthorizeEcid:ecid];
}

- (void)loginAuthorizeDidSucceed:(NSDictionary *)response
{
    NSLog(@"第三方授权登录成功");
    
    // 如果status的值等于1有第三方授权登录，否侧没有
    if ([[response objectForKey:@"status"] integerValue] == 1) {
        NSDictionary *dict = [response objectForKey:@"data"];
        
        NSString *qqKey = @"";
        //        NSString *qqKey = @"";
        NSString *qqSecret = @"";
        
        // 微信
        NSString *wxKey = @"";
        NSString *wxSecret = @"";
        
        // 新浪微博
        NSString *wbKey = @"";
        NSString *wbSecret = @"";
        NSString *wbRedirect = @"";
        
        NSString *wxCode = [dict safeObjectForKey:@"weixinDeveloperAccount"];
        NSString *qqCode = [dict safeObjectForKey:@"qqDeveloperAccount"];
        NSString *wbCode = [dict safeObjectForKey:@"sinaWeiBoDeveloperAccount"];
        NSString *ecid = [[ZSTF3Preferences shared].ECECCID length] == 8 ? [ZSTF3Preferences shared].ECECCID : [[ZSTF3Preferences shared].ECECCID stringByAppendingString:@"wg"];
        NSString *rWXCode = [EncryptUtil decryptUseDES:wxCode key:ecid];
        NSString *rQQCode = [EncryptUtil decryptUseDES:qqCode key:ecid];
        NSString *rWBCode = [EncryptUtil decryptUseDES:wbCode key:ecid];
        
        NSArray *wxArr = [rWXCode componentsSeparatedByString:@"|"];
        NSArray *qqArr = [rQQCode componentsSeparatedByString:@"|"];
        NSArray *wbArr = [rWBCode componentsSeparatedByString:@"|"];
        
        if (wxArr.count > 0 && wxArr[0] && [wxArr[0] isKindOfClass:[NSString class]])
        {
            wxKey = wxArr[0];
            wxSecret = wxArr[1];
        }
        
        if (qqArr.count > 0 && qqArr[0] && [qqArr[0] isKindOfClass:[NSString class]])
        {
            qqKey = qqArr[0];
            qqSecret = qqArr[1];
        }
        
        if (wbArr.count > 0 && wbArr[0] && [wbArr[0] isKindOfClass:[NSString class]])
        {
            wbKey = wbArr[0];
            wbSecret = wbArr[1];
            wbRedirect = wbArr[2];
        }
        
        [ZSTF3Preferences shared].qqKey = qqKey;
        [ZSTF3Preferences shared].qqSecret = qqSecret;
        [ZSTF3Preferences shared].wxKey = wxKey;
        [ZSTF3Preferences shared].wxSecret = wxSecret;
        [ZSTF3Preferences shared].weiboKey = wbKey;
        [ZSTF3Preferences shared].weiboSecret = wbSecret;
        [ZSTF3Preferences shared].weiboRedirect = wbRedirect;
        [ZSTF3Preferences shared].isQQLogin = [[dict safeObjectForKey:@"qq_accredit"] integerValue] == 1 ? YES : NO;
        [ZSTF3Preferences shared].isWBLogin = [[dict safeObjectForKey:@"wb_accredit"] integerValue] == 1 ? YES : NO;
        [ZSTF3Preferences shared].isWXLogin = [[dict safeObjectForKey:@"wx_accredit"] integerValue] == 1 ? YES : NO;
    }
}

- (void)loginAuthorizeDidFailed:(NSString *)response
{
    NSLog(@"123");
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}



@end
