//
//  PMMembersBalanceCell.h
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMMembersBalanceCell : UITableViewCell

@property (strong, nonatomic) UILabel *timeLabel;
@property (strong, nonatomic) UILabel *moneyLabel;
@property (strong, nonatomic) UILabel *orderLabel;

- (void)createBalanceUI;

- (void)setBalanceDataDict:(NSDictionary *)detailDict;

@end
