//
//  PMMembersCardRechargeViewController.m
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersCardRechargeViewController.h"
#import "ZSTUtils.h"
#import "PMMembersPaymentViewController.h"
#import "PMRechargeMoneyView.h"

@interface PMMembersCardRechargeViewController () <PMRechargeMoneyViewDelegate>

@property (strong, nonatomic) UIView *rechargeView;
@property (strong, nonatomic) UIView *paymentView;
@property (strong, nonatomic) UIButton *rechargeBtn;

@property (strong, nonatomic) NSMutableArray *rechargeDataArr;
@property (strong, nonatomic) NSMutableArray *rechargeIdArr;

@property (strong, nonatomic) UIButton *selectButton;
@property (strong, nonatomic) NSMutableArray *titleArray;
@property (strong, nonatomic) UILabel *moneyLabel;
@property (assign, nonatomic) NSInteger paymentType;

@property (strong, nonatomic) UIView *showBgGrayView;
@property (strong, nonatomic) UIView *blackView;
@property (strong, nonatomic) PMRechargeMoneyView *rechargeMoneyView;
@property (strong, nonatomic) NSString *rechargeIdString;

@end

@implementation PMMembersCardRechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"会员卡充值"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    // 获取会员卡充值界面初始化信息
    [self getMemberCardRechargeInitData];
    
    self.titleArray = [NSMutableArray arrayWithObjects:@"100元（送50个积分）",@"200元（送120个积分）",@"300元（送250个积分）",@"500元（送450个积分）",@"800元（送600个积分）",@"1000元（送800个积分）", nil];
    self.paymentType = 0;
    
    [self createRechargeUI];
    [self createPaymentUI];
    [self createControllerUI];
}

- (void)createRechargeUI
{
    UIView *rechargeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(420))];
    rechargeView.backgroundColor = [UIColor whiteColor];
    self.rechargeView = rechargeView;
    [self.view addSubview:rechargeView];
    self.rechargeView.hidden = YES;
    
    UILabel *rechargeMoneyLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), WidthRate(40), WidthRate(260), HeightRate(60))];
    rechargeMoneyLab.backgroundColor = [UIColor clearColor];
    rechargeMoneyLab.text = @"充值金额";
    rechargeMoneyLab.textColor = [UIColor blackColor];
    rechargeMoneyLab.textAlignment = NSTextAlignmentLeft;
    rechargeMoneyLab.font = [UIFont systemFontOfSize:15.0f];
    [rechargeView addSubview:rechargeMoneyLab];
    
    // 显示充值选项 view
    UIView *optionView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(50), WidthRate(150), WIDTH - WidthRate(100), WidthRate(120))];
    optionView.backgroundColor = [UIColor clearColor];
    optionView.layer.cornerRadius = 0.0f;
    optionView.layer.borderWidth = 1.0f;
    optionView.layer.borderColor = [UIColor grayColor].CGColor;
    [rechargeView addSubview:optionView];
    
    // 实际充值金额
    UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(10), WidthRate(25), WidthRate(450), WidthRate(70))];
    moneyLabel.backgroundColor = [UIColor clearColor];
    moneyLabel.textColor = [UIColor blackColor];
    moneyLabel.textAlignment = NSTextAlignmentLeft;
    moneyLabel.font = [UIFont systemFontOfSize:16.0f];
    self.moneyLabel = moneyLabel;
    [optionView addSubview:moneyLabel];
    
    // 充值选项选择按钮
    UIButton *chooseBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    chooseBtn.frame = CGRectMake(optionView.frame.size.width - WidthRate(120), WidthRate(25), WidthRate(80), WidthRate(70));
    [chooseBtn setTitle:@"选择" forState:UIControlStateNormal];
    [chooseBtn setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    chooseBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [chooseBtn addTarget:self action:@selector(chooseButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [optionView addSubview:chooseBtn];
}

- (void)createRechargePickerViewUI
{
    // 创建充值选项背景 view
    self.showBgGrayView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.showBgGrayView.backgroundColor = [UIColor clearColor];
    self.showBgGrayView.hidden = YES;
    [self.view insertSubview:self.showBgGrayView atIndex:1000];
    
    self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.blackView.backgroundColor = [UIColor blackColor];
    self.blackView.alpha = 0.7;
    [self.showBgGrayView addSubview:self.blackView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss)];
    [self.blackView addGestureRecognizer:tap];
    
    // 创建充值选项 view
    self.rechargeMoneyView = [[PMRechargeMoneyView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 180)];
    self.rechargeMoneyView.objectArray = [NSMutableArray array];// 初始化数组
    self.rechargeMoneyView.rechargeIdArray = [NSMutableArray array];
    self.rechargeMoneyView.objectArray = self.rechargeDataArr;
    self.rechargeMoneyView.rechargeIdArray = self.rechargeIdArr;
    self.rechargeMoneyView.rechargeDelegate = self;
    [self.rechargeMoneyView createRechargeMoneyPicker];
    [self.showBgGrayView addSubview:self.rechargeMoneyView];
}

- (void)createPaymentUI
{
    UIView *paymentView = [[UIView alloc] initWithFrame:CGRectMake(0, WidthRate(420), WIDTH, WidthRate(320))];
    paymentView.backgroundColor = [UIColor whiteColor];
    self.paymentView = paymentView;
    [self.view insertSubview:paymentView atIndex:0];
    self.paymentView.hidden = YES;
    
    // 支付方式
    UILabel *paymentLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(40), WidthRate(30), WidthRate(260), WidthRate(60))];
    paymentLab.backgroundColor = [UIColor clearColor];
    paymentLab.text = @"支付方式";
    paymentLab.textColor = [UIColor blackColor];
    paymentLab.textAlignment = NSTextAlignmentLeft;
    paymentLab.font = [UIFont systemFontOfSize:15.0f];
    [paymentView addSubview:paymentLab];
    
    UILabel *zfbLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), WidthRate(120), WidthRate(200), WidthRate(60))];
    zfbLabel.backgroundColor = [UIColor clearColor];
    zfbLabel.text = @"支付宝";
    zfbLabel.textColor = [UIColor blackColor];
    zfbLabel.textAlignment = NSTextAlignmentLeft;
    zfbLabel.font = [UIFont systemFontOfSize:15.0f];
    [paymentView addSubview:zfbLabel];
    
    // 支付宝
    UIButton *zfbButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self setButtonSelected:zfbButton];
    zfbButton.frame = CGRectMake(WIDTH - 100 - WidthRate(50), 55, 100, 25);
    //[zfbButton setImage:[UIImage imageNamed:@"module_recharge_zhifubao"] forState:UIControlStateNormal];
    [zfbButton setBackgroundImage:[UIImage imageNamed:@"module_recharge_zhifubao"] forState:UIControlStateNormal];
    zfbButton.imageView.contentMode = UIViewContentModeScaleAspectFit;
    zfbButton.tag = 1111;
    zfbButton.layer.cornerRadius = 6.0;
    zfbButton.layer.borderColor = [UIColor redColor].CGColor;
    zfbButton.layer.borderWidth = 1.0;
    [zfbButton addTarget:self action:@selector(zfbButtonClick:) forControlEvents:UIControlEventTouchDown];
    [paymentView addSubview:zfbButton];
}

- (void)createControllerUI
{
    UIButton *rechargeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rechargeBtn.frame = CGRectMake(WidthRate(40), HEIGHT - 120, WIDTH - WidthRate(80), 45);
    [rechargeBtn setTitle:@"立即充值" forState:UIControlStateNormal];
    [rechargeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [rechargeBtn.titleLabel setFont:[UIFont boldSystemFontOfSize:16.0f]];
    rechargeBtn.backgroundColor = [UIColor blueColor];
    [rechargeBtn.layer setCornerRadius:6.0f];
    [rechargeBtn addTarget:self action:@selector(rechargeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.rechargeBtn = rechargeBtn;
    [self.view insertSubview:rechargeBtn atIndex:0];
    self.rechargeBtn.hidden = YES;
}

#pragma mark - 获取会员卡充值界面初始化信息
- (void)getMemberCardRechargeInitData
{
    // 请求参数
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid};
    
    [self.engine getMemberCardRechargeInitInfo:param];
}

// 获取会员卡充值界面初始化信息成功
- (void)getMemberCardRechargeInitInfoSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    // 数据截取，封装到数组里
    NSArray *array = [response safeObjectForKey:@"data"];
    
    // 初始化数组
    self.rechargeDataArr = [NSMutableArray array];
    self.rechargeIdArr = [NSMutableArray array];
    
    for (NSInteger i = 0; i < array.count; i++) {
        
        NSDictionary *dict = [array objectAtIndex:i];// 从数组中取出每一组数据放到字典里
        
        NSString *string = [NSString stringWithFormat:@"%@元（送%@个积分）", [dict safeObjectForKey:@"rechargeMoney"],[dict safeObjectForKey:@"pointNumber"]];// 获得充值钱数和积分数
        NSString *stringId = [NSString stringWithFormat:@"%@", [dict safeObjectForKey:@"id"]];// 获得充值选项id
        
        [self.rechargeDataArr addObject:string];// 把充值钱数和积分添加到数组
        [self.rechargeIdArr addObject:stringId];// 把充值id添加到数组
    }
    
    // 获取会员卡充值界面初始化信息成功显示界面
    self.rechargeView.hidden = NO;
    self.paymentView.hidden = NO;
    self.rechargeBtn.hidden = NO;
    
    // 获取会员卡充值界面初始化第一个数据
    self.moneyLabel.text = [self.rechargeDataArr objectAtIndex:0];
    self.rechargeIdString = [self.rechargeIdArr objectAtIndex:0];
    
    [self createRechargePickerViewUI];
    
    NSLog(@"获取会员卡充值界面初始化信息成功");
}

- (void)getMemberCardRechargeInitInfoFaild:(NSString *)response
{
//    [self createRechargePickerViewUI];
//    
//    self.rechargeView.hidden = NO;
//    self.paymentView.hidden = NO;
//    self.rechargeBtn.hidden = NO;
}


// 改变充值金额显示参数和充值id
- (void)changeRechargeMoney:(NSString *)rechargeMoney changeRechargeId:(NSString *)rechargeId
{
    self.moneyLabel.text = rechargeMoney;
    self.rechargeIdString = rechargeId;
}

// 完成按钮响应事件
- (void)sureRechargeMoneySelected
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.rechargeMoneyView.frame = CGRectMake(0, HEIGHT, WIDTH, 180);
    }completion:^(BOOL finished) {
        
        self.showBgGrayView.hidden = YES;
    }];
}

#pragma mark - 选择充值选项按钮响应事件
- (void)chooseButtonClick:(UIButton *)button
{
    self.showBgGrayView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.rechargeMoneyView.frame = CGRectMake(0, HEIGHT - 64 - 180, WIDTH, 180);
    }];
}

- (void)pickerViewDismiss
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.rechargeMoneyView.frame = CGRectMake(0, HEIGHT, WIDTH, 180);
        
    } completion:^(BOOL finished) {
        self.showBgGrayView.hidden = YES;
    }];
}

#pragma mark - 完成按钮响应事件
- (void)finishedButtonClick:(UIButton *)button
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.rechargeMoneyView.frame = CGRectMake(0, HEIGHT, WIDTH, 180);
        
    } completion:^(BOOL finished) {
        
        self.showBgGrayView.hidden = YES;
    }];
}

- (void)cancelRechargeMoneySelected
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.rechargeMoneyView.frame = CGRectMake(0, HEIGHT, WIDTH, 180);
    }completion:^(BOOL finished) {
        
        self.showBgGrayView.hidden = YES;
    }];
}

#pragma mark - 设置按钮选中
- (void)setButtonSelected:(UIButton *)button
{
    self.selectButton.selected = NO;  // 将原本选择的按钮的状态设置为普通
    self.selectButton.layer.borderColor = [UIColor whiteColor].CGColor;
    self.selectButton = button;       // 改变当前支付方式的按钮
    self.selectButton.selected = YES; // 将当前的支付方式按钮重新设置为被选择
    self.selectButton.layer.borderColor = [UIColor redColor].CGColor;
}

#pragma mark - 支付宝按钮响应事件
- (void)zfbButtonClick:(UIButton *)sender
{
    NSLog(@"zfbButtonClick");
}

#pragma mark - 立即充值按钮响应事件
- (void)rechargeButtonClick:(UIButton *)sender
{
    if (self.moneyLabel.text.length != 0 && self.paymentType == 0) {
        //[TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"充值成功", nil) withImage:nil];
        
        
        // 充值请求参数
        NSString *client = @"ios";
        NSString *ecid = @"600395";
        NSString *rechargeId = self.rechargeIdString;
        //NSString *memberId = [NSString stringWithFormat:@"%@", self.memberId];
        NSString *memberId = @"39C772A736BE4BC48A8586968EA7D789";
        NSString *payMethod = @"1";
        
        NSDictionary *param = @{@"client":client, @"ecid":ecid,@"rechargePointSetId":rechargeId,@"memberId":memberId,@"payMethod":payMethod};
        
        [self.engine requestMemberRechargeWithParam:param];
    }
}

// 请求充值订单成功
- (void)requestMemberRechargeSucceed:(NSDictionary *)response
{
    NSString *orderId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"orderId"];
    NSLog(@"orderId === %@", orderId);
    
    
    // 请求充值支付参数
    NSString *client = @"ios";
    //NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    NSString *ecid = @"600395";
    
    NSString *urlString = [NSString stringWithFormat:@"http://192.168.1.8:8081/app/mc_recharge!alipayOrder?orderId=%@&client=%@&ecid=%@", orderId,client,ecid];
    
    
    // 会员卡支付宝支付
    PMMembersPaymentViewController *paymentVC = [[PMMembersPaymentViewController alloc] init];
//    [paymentVC setURL:[NSString stringWithFormat:@"http://192.168.1.8:8081/app/mc_recharge!alipayOrder?orderId=%@&client=%@&ecid=%@", orderId,client,ecid]];
//    paymentVC.hidesBottomBarWhenPushed = YES;
//    paymentVC.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    paymentVC.urlString = urlString;
    [self.navigationController pushViewController:paymentVC animated:YES];
    
    
//    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"orderId":orderId};
//    
//    [self.engine requestMemberRechargePayment:param];
}

- (void)requestMemberRechargeFaild:(NSString *)response
{
    [TKUIUtil alertInWindow:response withImage:nil];
}


// 请求充值支付成功
- (void)requestMemberRechargePaymentSucceed:(NSDictionary *)response
{
    // 会员卡支付宝支付
//    PMMembersPaymentViewController *paymentVC = [[PMMembersPaymentViewController alloc] init];
//    //[paymentVC setURL:[NSString stringWithFormat:@"http://192.168.1.15:8030/app/AlipayTrade.ashx?orderid=%@", rechargeSetId]];
//    paymentVC.hidesBottomBarWhenPushed = YES;
//    paymentVC.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
//    [self.navigationController pushViewController:paymentVC animated:YES];
}

- (void)requestMemberRechargePaymentFaild:(NSString *)response
{
    
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
