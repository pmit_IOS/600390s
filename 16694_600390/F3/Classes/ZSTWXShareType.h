//
//  ZSTWXShareType.h
//  F3
//
//  Created by pmit on 15/8/28.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ZSTWXShareType : NSObject

+ (ZSTWXShareType *)shareInstance;
@property (assign,nonatomic) BOOL isWXShare;

@end
