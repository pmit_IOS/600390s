//
//  ZSTRegisterPassViewController.h
//  F3
//
//  Created by pmit on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTUtils.h"

typedef NS_ENUM(NSInteger, ZSTRegisterTypeSecond) {
    //以下是枚举成员
    ZSTRegisterTypeSecondRegister = 0,
    ZSTRegisterTypeSecondFindPass,
    ZSTRegisterTypeSecondChangePhoe,
    ZSTRegisterTypeSecondChangePass
    
};

@interface ZSTRegisterPassViewController : UIViewController

@property (copy,nonatomic) NSString *phoneString;
@property (copy,nonatomic) NSString *smsCodeString;
@property (assign,nonatomic) BOOL isRegister;
@property (assign,nonatomic) ZSTRegisterTypeSecond registerType;

@end
