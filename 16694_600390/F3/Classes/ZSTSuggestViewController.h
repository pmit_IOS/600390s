//
//  ZSTSuggestViewController.h
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"

@interface ZSTSuggestViewController : UIViewController

@property (strong,nonatomic) ZSTF3Engine *engine;

@end
