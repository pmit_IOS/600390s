//
//  UIFont+ZSTMyFont.m
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "UIFont+ZSTMyFont.h"

@implementation UIFont (ZSTMyFont)


+ (UIFont *)mySuitFont:(CGFloat)fontSize
{
    UIFont *myFont;
    if (!iPhone6Plus)
    {
        myFont = [self mySuitFont:fontSize];
    }
    else
    {
        myFont = [self mySuitFont:(fontSize + 2)];
    }
    
    return myFont;
}


@end
