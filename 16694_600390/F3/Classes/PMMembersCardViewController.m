//
//  PMMembersCardViewController.m
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersCardViewController.h"
#import "ZSTUtils.h"
#import "PMMembersCardCell.h"
#import "PMMembersCardRechargeViewController.h"
#import "PMMembersBalanceViewController.h"
#import "PMMembersInfoViewController.h"
#import "PMMembersCardDetailViewController.h"
#import "PMMembersCardShopInfosViewController.h"
#import "PMMembersPrivilegeViewController.h"
#import "UIImage+Compress.h"
#import "UIImageView+WebCache.h"

@interface PMMembersCardViewController ()

@property (strong, nonatomic) UITableView *membersTableView;
@property (strong, nonatomic) NSArray *titleArray;

@property (strong, nonatomic) UIButton *discountBtn;
@property (strong, nonatomic) UIButton *exclusiveBtn;

@property (strong, nonatomic) UIImageView *bgImage;
@property (strong, nonatomic) UIImageView *vipImage;
@property (strong, nonatomic) UILabel *tipsLabel;

@property (strong, nonatomic) UIView *footerView;

// 领取新会员卡
@property (strong, nonatomic) UIView *getCardBgView;
@property (strong, nonatomic) UIView *alphaView;
@property (strong, nonatomic) UIView *getCardPageView;
@property (strong, nonatomic) UILabel *remindLabel;
@property (strong, nonatomic) UIView *inputView;
@property (strong, nonatomic) UITextField *nameTextField;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) UITextField *authcodeTextField;
@property (strong, nonatomic) CALayer *layer2;
@property (strong, nonatomic) UIButton *authcodeBtn;
@property (strong, nonatomic) UILabel *retryLab;
@property (strong, nonatomic) NSTimer *retryTimer;
@property (assign, nonatomic) NSInteger retryCount;
@property (strong, nonatomic) UIButton *confirmBtn;
@property (assign, nonatomic) BOOL isShowKeyboard;

@property (strong, nonatomic) NSDictionary *membersData;
@property (assign, nonatomic) NSInteger isGetCard;
@property (strong, nonatomic) NSString *memberCardId;
@property (strong, nonatomic) NSString *cardsetId;

@end

@implementation PMMembersCardViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    // 导航控制器全屏滑动返回上一页效果实现代码
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    self.titleArray = @[@[@"我的余额",@"会员卡充值"],@[@"会员资料",@"会员卡说明",@"门店信息"]];
    
    [self requestMemberCard];
    
    // 初始化会员卡 tableView
    self.membersTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.membersTableView.delegate = self;
    self.membersTableView.dataSource = self;
    self.membersTableView.backgroundColor = RGBA(243, 243, 243, 1);
    //self.membersTableView.indicatorStyle = UIScrollViewIndicatorStyleBlack;// 修改tableView滚动条颜色
    self.membersTableView.separatorStyle = UITableViewCellSeparatorStyleNone;// 去掉tableView分行线条
    [self.membersTableView registerClass:[PMMembersCardCell class] forCellReuseIdentifier:@"membersCell"];
    [self.view addSubview:self.membersTableView];
    self.membersTableView.hidden = YES;
    
    [self createNewMembersCardUI];
    [self createHeaderViewUI];
    [self createFooterViewUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    
    // headerView 背景动画特效
    [UIView animateWithDuration:0.1f animations:^{
        
        // 改变背景图大小
        self.vipImage.frame = CGRectMake(1, 9, WIDTH - 2, WidthRate(340) - 18);
        
    } completion:^(BOOL finished) {
        
        [UIView animateWithDuration:0.1f animations:^{
            
            self.vipImage.frame = CGRectMake(2, 8, WIDTH - 4, WidthRate(340) - 16);
            
        } completion:^(BOOL finished) {
            
            [UIView animateWithDuration:0.1f animations:^{
                
                self.vipImage.frame = CGRectMake(3, 7, WIDTH - 6, WidthRate(340) - 14);
                
            } completion:^(BOOL finished) {
                
                [UIView animateWithDuration:0.1f animations:^{
                    
                    self.vipImage.frame = CGRectMake(4, 6, WIDTH - 8, WidthRate(340) - 12);
                    
                } completion:^(BOOL finished) {
                    
                    [UIView animateWithDuration:0.1f animations:^{
                        
                        self.vipImage.frame = CGRectMake(5, 5, WIDTH - 10, WidthRate(340) - 10);
                        
                    } completion:^(BOOL finished) {
                        
                        [UIView animateWithDuration:0.1f animations:^{
                            
                            self.vipImage.frame = CGRectMake(6, 4, WIDTH - 12, WidthRate(340) - 8);
                            
                        } completion:^(BOOL finished) {
                            
                            [UIView animateWithDuration:0.1f animations:^{
                                
                                self.vipImage.frame = CGRectMake(7, 3, WIDTH - 14, WidthRate(340) - 6);
                                
                            } completion:^(BOOL finished) {
                                
                                [UIView animateWithDuration:0.1f animations:^{
                                    
                                    self.vipImage.frame = CGRectMake(8, 2, WIDTH - 16, WidthRate(340) - 4);
                                    
                                } completion:^(BOOL finished) {
                                    
                                    [UIView animateWithDuration:0.1f animations:^{
                                        
                                        self.vipImage.frame = CGRectMake(9, 1, WIDTH - 18, WidthRate(340) - 2);
                                        
                                    } completion:^(BOOL finished) {
                                        
                                        [UIView animateWithDuration:0.1f animations:^{
                                            
                                            self.vipImage.frame = CGRectMake(10, 0, WIDTH - 20, WidthRate(340));
                                            
                                        } completion:^(BOOL finished) {
                                            
                                        }];
                                    }];
                                }];
                            }];
                        }];
                    }];
                }];
            }];
        }];
    }];
    
    
    // 判断用户是否已经成为会员
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"vipImage"]) {
        
        BOOL isVip = NO;
        isVip = [[NSUserDefaults standardUserDefaults] boolForKey:@"vipImage"];
        
        if (isVip == YES) {
            self.bgImage.hidden = YES;
            self.vipImage.hidden = NO;
            self.tipsLabel.userInteractionEnabled = NO;
            self.tipsLabel.text = @"使用时请出示会员卡";
            
            self.discountBtn.backgroundColor = RGBA(249, 237, 112, 1);
            self.exclusiveBtn.backgroundColor = RGBA(249, 237, 112, 1);
        }
        else {
            self.bgImage.hidden = NO;
            self.vipImage.hidden = YES;
        }
    }

    // 注册通知，监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    // 注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)createNewMembersCardUI
{
    self.alphaView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT)];
    self.alphaView.backgroundColor = [UIColor blackColor];
    self.alphaView.alpha = 0.8f;
    [self.view addSubview:self.alphaView];
    self.alphaView.hidden = YES;
    
    self.getCardBgView = [[UIView alloc] initWithFrame:self.alphaView.frame];
    self.getCardBgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.getCardBgView];
    self.getCardBgView.hidden = YES;
    
    self.getCardPageView = [[UIView alloc] initWithFrame:CGRectMake((WIDTH - WidthRate(690)) / 2, (HEIGHT - WidthRate(500)) / 2, WidthRate(690), WidthRate(500))];
    self.getCardPageView.backgroundColor = RGBA(243, 243, 243, 1);
    self.getCardPageView.layer.cornerRadius = 12.0f;
    self.getCardPageView.layer.borderWidth = 3.0f;
    self.getCardPageView.layer.borderColor = [UIColor whiteColor].CGColor;
    [self.getCardBgView addSubview:self.getCardPageView];
    self.getCardPageView.hidden = YES;
    
    // 取消按钮
    UIButton *cancelBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelBtn.frame = CGRectMake(self.getCardPageView.frame.size.width - WidthRate(80) - WidthRate(30), 5, WidthRate(80), WidthRate(60));
    [cancelBtn setTitle:@"取消" forState:UIControlStateNormal];
    [cancelBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    cancelBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [cancelBtn addTarget:self action:@selector(cancelButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.getCardPageView addSubview:cancelBtn];
    
    UILabel *titleLabel =[[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 5, WidthRate(200), WidthRate(60))];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.text = @"领卡信息";
    titleLabel.textColor = [UIColor darkGrayColor];
    titleLabel.textAlignment = NSTextAlignmentLeft;
    titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.getCardPageView addSubview:titleLabel];
    
    // 提示信息
    UILabel *remindLabel =[[UILabel alloc] initWithFrame:CGRectMake(WidthRate(30), 5 + WidthRate(60) + 5, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(100))];
    remindLabel.backgroundColor = [UIColor clearColor];
    remindLabel.text = @"填写真实的姓名以及电话号码，即可获得会员卡，享受会员优惠。";
    remindLabel.textColor = [UIColor darkGrayColor];
    remindLabel.textAlignment = NSTextAlignmentLeft;
    remindLabel.numberOfLines = 0;
    remindLabel.font = [UIFont systemFontOfSize:15.0f];
    self.remindLabel = remindLabel;
    [self.getCardPageView addSubview:remindLabel];
    
    // 输入框 view
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(WidthRate(30), self.remindLabel.frame.origin.y + self.remindLabel.frame.size.height, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(180))];
    inputView.backgroundColor = RGBA(243, 243, 243, 1);
    inputView.layer.cornerRadius = 8.0f;
    inputView.layer.borderWidth = 1.0f;
    inputView.layer.borderColor = [UIColor grayColor].CGColor;
    self.inputView = inputView;
    [self.getCardPageView addSubview:inputView];
    
    // 分隔线
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0, inputView.frame.size.height / 2, inputView.frame.size.width, WidthRate(1));
    layer1.backgroundColor = [UIColor grayColor].CGColor;
    [inputView.layer addSublayer:layer1];
    
    // 姓名输入框
    UITextField *nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, inputView.frame.size.width - 10, WidthRate(90))];
    nameTextField.borderStyle = UITextBorderStyleNone;
    nameTextField.delegate = self;
    nameTextField.tag = 1;
    nameTextField.textColor = RGBA(100, 100, 100, 1);
    nameTextField.textAlignment = NSTextAlignmentLeft;
    nameTextField.placeholder = @"请输入姓名";
    nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    nameTextField.returnKeyType = UIReturnKeyDone;
    nameTextField.keyboardType = UIReturnKeyDefault;
    self.nameTextField = nameTextField;
    [inputView addSubview:nameTextField];
    
    // 手机输入框
    UITextField *mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, inputView.frame.size.height / 2, inputView.frame.size.width - 10, WidthRate(90))];
    mobileTextField.borderStyle = UITextBorderStyleNone;
    mobileTextField.delegate = self;
    mobileTextField.tag = 2;
    mobileTextField.textColor = RGBA(100, 100, 100, 1);
    mobileTextField.textAlignment = NSTextAlignmentLeft;
    mobileTextField.placeholder = @"请输入手机号";
    mobileTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    mobileTextField.returnKeyType = UIReturnKeyDone;
    mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.mobileTextField = mobileTextField;
    [inputView addSubview:mobileTextField];
    
    // 分隔线
    self.layer2 = [CALayer layer];
    self.layer2.backgroundColor = [UIColor grayColor].CGColor;
    [inputView.layer addSublayer:self.layer2];
    self.layer2.hidden = YES;
    
    // 验证码输入框
    UITextField *authcodeTextField = [[UITextField alloc] initWithFrame:CGRectMake(5, WidthRate(180), inputView.frame.size.width - 10, WidthRate(90))];
    authcodeTextField.borderStyle = UITextBorderStyleNone;
    authcodeTextField.delegate = self;
    authcodeTextField.tag = 3;
    authcodeTextField.textColor = RGBA(100, 100, 100, 1);
    authcodeTextField.textAlignment = NSTextAlignmentLeft;
    authcodeTextField.placeholder = @"请输入验证码";
    authcodeTextField.returnKeyType = UIReturnKeyDone;
    authcodeTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.authcodeTextField = authcodeTextField;
    [inputView addSubview:authcodeTextField];
    self.authcodeTextField.hidden = YES;
    
    // 获取验证码按钮
    self.authcodeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.authcodeBtn.frame = CGRectMake(self.authcodeTextField.frame.size.width - WidthRate(150) - WidthRate(20), WidthRate(15), WidthRate(150), WidthRate(60));
    [self.authcodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.authcodeBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.authcodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.authcodeBtn addTarget:self action:@selector(getCardAuthcodeClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.authcodeBtn setBackgroundColor:[UIColor blueColor]];
    [self.authcodeBtn.layer setCornerRadius:4.0];
    [self.authcodeTextField addSubview:self.authcodeBtn];
    
    self.retryLab = [[UILabel alloc] initWithFrame:CGRectMake(self.authcodeTextField.frame.size.width - WidthRate(150) - WidthRate(20), WidthRate(15), WidthRate(150), WidthRate(60))];
    self.retryLab.textAlignment = NSTextAlignmentCenter;
    self.retryLab.hidden = YES;
    self.retryLab.text = @"重发(60)";
    self.retryLab.textColor = [UIColor darkGrayColor];
    self.retryLab.font = [UIFont systemFontOfSize:12.0f];
    self.retryLab.textAlignment = NSTextAlignmentCenter;
    [self.authcodeTextField addSubview:self.retryLab];
    
    // 确定按钮
    UIButton *confirmBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    confirmBtn.frame = CGRectMake(WidthRate(30), inputView.frame.origin.y + inputView.frame.size.height + 10, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(80));
    confirmBtn.backgroundColor = [UIColor clearColor];
    [confirmBtn setTitle:@"确定" forState:UIControlStateNormal];
    [confirmBtn setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    confirmBtn.titleLabel.font = [UIFont systemFontOfSize:16.0f];
    confirmBtn.layer.cornerRadius = 8.0f;
    confirmBtn.layer.borderWidth = 1.0f;
    confirmBtn.layer.borderColor = [UIColor grayColor].CGColor;
    [confirmBtn addTarget:self action:@selector(confirmButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.confirmBtn = confirmBtn;
    [self.getCardPageView addSubview:confirmBtn];
}

- (void)createHeaderViewUI
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(420))];
    headerView.backgroundColor = [UIColor whiteColor];
    
    // 背景图片
    UIImageView *bgImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, 0, WIDTH - 20, WidthRate(340))];
    UIImage *image = [UIImage imageNamed:@"vipImage.png"];
    bgImage.image = image;
    self.bgImage = bgImage;
    [headerView addSubview:bgImage];
    bgImage.userInteractionEnabled = YES;
    self.bgImage.hidden = NO;
    
    UIImageView *vipImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(340))];
    UIImage *vipIma = [UIImage imageNamed:@"vipImage.png"];
    vipImage.image = vipIma;
    self.vipImage = vipImage;
    [headerView addSubview:vipImage];
    self.vipImage.hidden = YES;
    
    // 获取新会员卡按钮
    self.tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(50, WidthRate(340), WIDTH - 100, WidthRate(80))];
    self.tipsLabel.backgroundColor = [UIColor clearColor];
    self.tipsLabel.text = @"领取您的新会员卡";
    self.tipsLabel.textColor = [UIColor darkGrayColor];
    self.tipsLabel.textAlignment = NSTextAlignmentCenter;
    self.tipsLabel.font = [UIFont systemFontOfSize:13.0f];
    [headerView addSubview:self.tipsLabel];
    
    UITapGestureRecognizer *tapAction = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(getNewCardAction:)];
    self.tipsLabel.userInteractionEnabled = YES;
    [self.tipsLabel addGestureRecognizer:tapAction];
    
    self.membersTableView.tableHeaderView = headerView;
}

- (void)createFooterViewUI
{
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = [UIColor whiteColor];
    self.footerView = footerView;
    
    // 分隔线
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(WidthRate(15), WidthRate(35), WidthRate(235), WidthRate(1));
    layer1.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
    [footerView.layer addSublayer:layer1];
    
    CALayer *layer2 = [CALayer layer];
    layer2.frame = CGRectMake(WidthRate(475), WidthRate(35), WidthRate(235), WidthRate(1));
    layer2.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
    [footerView.layer addSublayer:layer2];
    
    // 会员特权
    UILabel *privilegeLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(250), 0, WidthRate(220), WidthRate(70))];
    privilegeLab.backgroundColor = [UIColor clearColor];
    privilegeLab.text = @"会员特权";
    privilegeLab.textColor = [UIColor blackColor];
    privilegeLab.textAlignment = NSTextAlignmentCenter;
    privilegeLab.font = [UIFont systemFontOfSize:15.0f];
    [footerView addSubview:privilegeLab];
    
    // 会员折扣
    UIButton *discountBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    discountBtn.frame = CGRectMake(WidthRate(60), WidthRate(150), WidthRate(285), WidthRate(100));
    [discountBtn setTitle:@"会员折扣" forState:UIControlStateNormal];
    [discountBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [discountBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [discountBtn addTarget:self action:@selector(discountButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.discountBtn = discountBtn;
    [footerView addSubview:discountBtn];
    
    // 专享模块
    UIButton *exclusiveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    exclusiveBtn.frame = CGRectMake(WidthRate(405), WidthRate(150), WidthRate(285), WidthRate(100));
    [exclusiveBtn setTitle:@"专享模块" forState:UIControlStateNormal];
    [exclusiveBtn setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [exclusiveBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [exclusiveBtn addTarget:self action:@selector(exclusiveButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    self.exclusiveBtn = exclusiveBtn;
    [footerView addSubview:exclusiveBtn];
    
    self.membersTableView.tableFooterView.hidden = YES;
}

#pragma mark - tableView
#pragma mark
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.titleArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.titleArray[section] count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 15)];
    headerView.backgroundColor = RGBA(243, 243, 243, 1);
    
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 1)];
    footerView.backgroundColor = RGBA(243, 243, 243, 1);
    
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMMembersCardCell *cell = [tableView dequeueReusableCellWithIdentifier:@"membersCell"];
    
    [cell createTitleUI];
    
    cell.titleLabel.text = self.titleArray[indexPath.section][indexPath.row];
    
    if (indexPath.section == 0 && indexPath.row == 0) {
        
        //cell.balanceLab.text = @"0.00";
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(15, cell.frame.size.height - 1.0f, WIDTH - 15, 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [cell.layer addSublayer:layer];
    }
    else if (indexPath.section == 0 && indexPath.row == 1) {
        
        cell.integralLab.text = @"充值送积分";
    }
    else if (indexPath.section == 1 && indexPath.row == 0) {
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(15, cell.frame.size.height - 1.0f, WIDTH - 15, 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [cell.layer addSublayer:layer];
    }
    else if (indexPath.section == 1 && indexPath.row == 1) {
        
        // 分割线
        CALayer *layer = [[CALayer alloc] init];
        layer.frame = CGRectMake(15, cell.frame.size.height - 1.0f, WIDTH - 15, 1);
        layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
        [cell.layer addSublayer:layer];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0 && indexPath.row == 0) {// 我的余额
        
        // 判断用户是否成为会员
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"vipImage"]) {
            PMMembersBalanceViewController *balanceVC = [[PMMembersBalanceViewController alloc] init];
            [self.navigationController pushViewController:balanceVC animated:YES];
        }
        else {
            [TKUIUtil alertInWindow:@"亲，成为会员才能使用该功能" withImage:nil];
        }
    }
    
    else if (indexPath.section == 0 && indexPath.row == 1) {// 会员卡充值;
        
        PMMembersCardRechargeViewController *rechargeVC = [[PMMembersCardRechargeViewController alloc] init];
        rechargeVC.memberId = self.memberCardId;// 传会员卡号
        [self.navigationController pushViewController:rechargeVC animated:YES];
    }
    
    else if (indexPath.section == 1 && indexPath.row == 0) {// 会员资料
        
        // 判断用户是否成为会员
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"vipImage"]) {
            
            // 获得会员数据
//            NSDictionary *membersData = [NSDictionary dictionary];
//            membersData = [[NSUserDefaults standardUserDefaults] dictionaryForKey:@"memberData"];
            
            PMMembersInfoViewController *membersInfoVC = [[PMMembersInfoViewController alloc] init];
            membersInfoVC.memberId = self.memberCardId;// 传会员数据
            [self.navigationController pushViewController:membersInfoVC animated:YES];
        }
        else {
            [TKUIUtil alertInWindow:@"亲，成为会员才能使用该功能" withImage:nil];
        }
    }
    
    else if (indexPath.section == 1 && indexPath.row == 1) {// 会员卡说明
        
        PMMembersCardDetailViewController *membersCardDetailVC = [[PMMembersCardDetailViewController alloc] init];
        [self.navigationController pushViewController:membersCardDetailVC animated:YES];
    }
    
    else if (indexPath.section == 1 && indexPath.row == 2) {// 门店信息
        
        PMMembersCardShopInfosViewController *shopInfosVC = [[PMMembersCardShopInfosViewController alloc] init];
        [self.navigationController pushViewController:shopInfosVC animated:YES];
    }
}

// 回收键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 触摸背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *mobile = [ZSTF3Preferences shared].loginMsisdn;
    
    if (self.mobileTextField.tag == 2 && textField == self.mobileTextField && textField.text.length - range.length + string.length == 11 && [mobile hasPrefix:@"1"] && ![self.mobileTextField.text isEqualToString:mobile]) {
        
        // 领卡手机号和注册手机号不一样，需要验证码，改变UI
        self.getCardPageView.frame = CGRectMake((WIDTH - WidthRate(690)) / 2, (HEIGHT - WidthRate(500)) / 2, WidthRate(690), WidthRate(590));
        self.inputView.frame = CGRectMake(WidthRate(30), self.remindLabel.frame.origin.y + self.remindLabel.frame.size.height, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(270));
        self.layer2.hidden = NO;
        self.layer2.frame = CGRectMake(0, WidthRate(180), self.inputView.frame.size.width, WidthRate(1));
        self.authcodeTextField.hidden = NO;
        self.confirmBtn.frame = CGRectMake(WidthRate(30), self.inputView.frame.origin.y + self.inputView.frame.size.height + 10, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(80));
        
        
        //第一步,对组件增加监听器
        [textField addTarget:self action:@selector(getCardTextFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
        
    // 判断输入手机号码最大长度
    if (self.mobileTextField.tag == 2 && self.mobileTextField == textField) {
        NSInteger maxLength = 11;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    if (self.authcodeTextField.tag == 3 && self.authcodeTextField == textField) {
        NSInteger maxLength = 6;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

//第二步,实现回调函数
- (void)getCardTextFieldDidChange:(id)sender
{
    NSString *mobile = [ZSTF3Preferences shared].loginMsisdn;
    
    if ([self.mobileTextField.text isEqualToString:mobile]) {
        
        self.getCardPageView.frame = CGRectMake((WIDTH - WidthRate(690)) / 2, (HEIGHT - WidthRate(500)) / 2, WidthRate(690), WidthRate(500));
        self.inputView.frame = CGRectMake(WidthRate(30), self.remindLabel.frame.origin.y + self.remindLabel.frame.size.height, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(180));
        self.layer2.hidden = YES;
        self.authcodeTextField.hidden = YES;
        self.confirmBtn.frame = CGRectMake(WidthRate(30), self.inputView.frame.origin.y + self.inputView.frame.size.height + 10, self.getCardPageView.frame.size.width - WidthRate(60), WidthRate(80));
        
        [self.mobileTextField resignFirstResponder];
    }
}

#pragma mark - 解决textField输入时键盘遮挡问题
// 监听事件
- (void)handleKeyboardDidShow:(NSNotification *)paramNotification
{
    // 让出现键盘只执行一次，当isShowKeyboard ＝ NO 时，才向上移动60像素
    if (self.isShowKeyboard == NO) {
        // 屏幕向上移动70像素
        [self moveView:-70];
        
        self.isShowKeyboard = YES;
    }
}

- (void)handleKeyboardDidHidden
{
    [self moveView:70];
    
    // 当键盘消失时，向下移动60像素 ,把isShowKeyboard置NO，
    self.isShowKeyboard = NO;
}

- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}

#pragma mark - 领取新会员卡响应事件
- (void)getNewCardAction:(UITapGestureRecognizer *)tap
{
    self.getCardBgView.hidden = NO;
    self.alphaView.hidden = NO;
    self.getCardPageView.hidden = NO;
}

#pragma mark - 获取验证码按钮响应事件
- (void)getCardAuthcodeClick:(UIButton *)button
{
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || ![ZSTUtils checkMobile:self.mobileTextField.text])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入正确的手机号码", nil) withImage:nil];
        return;
    }
    
    self.retryCount = 60;
    self.retryTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(changeRetryTimer) userInfo:nil repeats:YES];
    [self.retryTimer fire];
    
    // 验证码请求参数
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *operType = @"4";
    NSString *mobile = [NSString stringWithFormat:@"%@", self.mobileTextField.text];
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"operType":operType,@"phone":mobile};
    
    [self.engine getChangeMemberCardMobileWithAuthcode:param];
}

- (void)changeRetryTimer
{
    if (self.retryCount > 0) {
        [self.authcodeBtn setTitle:[NSString stringWithFormat:@"%@",@(self.retryCount)] forState:UIControlStateDisabled];
        [self.authcodeBtn setEnabled:NO];
        self.retryCount--;
    }else {
        [self.authcodeBtn setTitle:@"重  发" forState: UIControlStateNormal];
        [self.authcodeBtn setEnabled:YES];
        [self.retryTimer invalidate];
    }
}

// 获取验证码
- (void)getChangeMemberCardMobileWithAuthcodeSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    NSLog(@"HHHHHHHHHHH === %@", response);
}

- (void)getChangeMemberCardMobileWithAuthcodeFaild:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}

#pragma mark - 取消按钮响应事件
- (void)cancelButtonClick:(UIButton *)button
{
    self.getCardBgView.hidden = YES;
    self.alphaView.hidden = YES;
    self.getCardPageView.hidden = YES;
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - (领取新会员卡)确定按钮响应事件
- (void)confirmButtonClick:(UIButton *)sender
{
    if (self.nameTextField.text.length == 0 || [ZSTUtils isEmpty:self.nameTextField.text]) {
        
        [TKUIUtil alertInWindow:@"请输入您的姓名" withImage:nil];
        return;
    }
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || ![ZSTUtils checkMobile:self.mobileTextField.text]) {
        
        [TKUIUtil alertInWindow:@"请输入正确的手机号码" withImage:nil];
        return;
    }
    
    // 领取新会员卡请求参数
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *cardSetId = [NSString stringWithFormat:@"%@", self.cardsetId];
    NSString *name = [NSString stringWithFormat:@"%@", self.nameTextField.text];
    NSString *mobile = [NSString stringWithFormat:@"%@", self.mobileTextField.text];
    NSString *captchaCode = [NSString stringWithFormat:@"%@", self.authcodeTextField.text];
    NSString *operType = @"4";
    NSString *memberId = [NSString stringWithFormat:@"%@", self.memberCardId];
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"cardSetId":cardSetId,@"membername":name,@"phone":mobile,@"captchaCode":captchaCode,@"operType":operType,@"memberId":memberId};
    
    [self.engine getMemberCard:param];
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

// 领取会员卡成功
- (void)getMembersCardSucceed:(NSDictionary *)response
{
    self.membersData = [response safeObjectForKey:@"data"];
    
    self.bgImage.hidden = YES;
    self.vipImage.hidden = NO;
    self.tipsLabel.text = @"使用时请出示会员卡";
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"vipImage"];
    
    // 暂时使用字典保存用户模拟数据
//    NSMutableDictionary *memberData = [NSMutableDictionary dictionary];
//    [memberData setObject:@"1000001" forKey:@"memberCardNo"];
//    [memberData setObject:self.nameTextField.text forKey:@"memberName"];
//    [memberData setObject:self.mobileTextField.text forKey:@"memberMobile"];
//    
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:memberData forKey:@"memberData"];
    
    
    self.discountBtn.backgroundColor = RGBA(249, 237, 112, 1);
    self.exclusiveBtn.backgroundColor = RGBA(249, 237, 112, 1);
    
    self.getCardBgView.hidden = YES;
    self.alphaView.hidden = YES;
    self.getCardPageView.hidden = YES;
}

- (void)getMembersCardFaild:(NSString *)response
{
    self.getCardBgView.hidden = YES;
    self.alphaView.hidden = YES;
    self.getCardPageView.hidden = YES;
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}


#pragma mark - 会员折扣按钮响应事件
- (void)discountButtonClick:(UIButton *)sender
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"vipImage"]) {
        
        [TKUIUtil alertInWindow:@"亲，成为会员才能使用该功能" withImage:nil];
    }
    else {
        PMMembersPrivilegeViewController *privilegeVC = [[PMMembersPrivilegeViewController alloc] init];
        privilegeVC.discount = @"discount";
        [self.navigationController pushViewController:privilegeVC animated:YES];
    }
}

#pragma mark - 专享模块按钮响应事件
- (void)exclusiveButtonClick:(UIButton *)sender
{
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"vipImage"]) {
        
        [TKUIUtil alertInWindow:@"亲，成为会员才能使用该功能" withImage:nil];
    }
    else {
        PMMembersPrivilegeViewController *privilegeVC = [[PMMembersPrivilegeViewController alloc] init];
        [self.navigationController pushViewController:privilegeVC animated:YES];
    }
}

#pragma mark - 请求会员卡初始化界面信息
- (void)requestMemberCard
{
    NSString *client = @"ios";
    //NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    NSString *ecid = @"600395";
    NSString *termuserId = [ZSTF3Preferences shared].UserId;
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"termuserId":termuserId};
    
    [self.engine requestMemberCard:param];
}

// 获取会员卡初始化界面信息
- (void)requestMembersCardSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    NSDictionary *dict = [response safeObjectForKey:@"data"];
    
    // 获取商户会员卡
    NSString *iconString = [NSString stringWithFormat:@"%@", [[dict safeObjectForKey:@"mcCardSet"] safeObjectForKey:@"cardbackgroundpic"]];
    [self.vipImage setImageWithURL:[NSURL URLWithString:iconString] placeholderImage:[UIImage imageNamed:@"vipImage.png"]];
    
    // 获取会员卡id
    self.memberCardId = [NSString stringWithFormat:@"%@", [[dict safeObjectForKey:@"mcMember"] safeObjectForKey:@"id"]];
    self.cardsetId = [NSString stringWithFormat:@"%@", [[dict safeObjectForKey:@"mcMember"] safeObjectForKey:@"cardsetid"]];
    
    // 我的余额
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    PMMembersCardCell *cell = (PMMembersCardCell *)[self.membersTableView cellForRowAtIndexPath:indexPath];
    cell.balanceLab.text = [NSString stringWithFormat:@"%.2f", [[[dict safeObjectForKey:@"mcMember"] safeObjectForKey:@"balance"] doubleValue]];
    
    // 判断用户是否已经领取会员卡，返回值为1即领取
    self.isGetCard = [[dict safeObjectForKey:@"isCard"] integerValue];
    
    if (self.isGetCard == 1) {
        
        self.membersTableView.tableFooterView.hidden = NO;
        self.footerView.frame = CGRectMake(0, 0, WIDTH, WidthRate(400));
        self.membersTableView.tableFooterView = self.footerView;
    }
    
    self.membersTableView.hidden = NO;
    [self.membersTableView reloadData];
}

- (void)requestMembersCardFaild:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
