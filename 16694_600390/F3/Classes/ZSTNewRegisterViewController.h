//
//  ZSTNewRegisterViewController.h
//  F3
//
//  Created by pmit on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTUtils.h"

typedef NS_ENUM(NSInteger, ZSTRegisterType) {
    //以下是枚举成员
    ZSTRegisterTypeRegister = 0,
    ZSTRegisterTypeFindPass,
    ZSTRegisterTypeChangePhoe,
    ZSTRegisterTypeChangePass
    
};

@interface ZSTNewRegisterViewController : UIViewController <ZSTF3EngineDelegate>

@property (strong,nonatomic) ZSTF3Engine *engine;
@property (assign,nonatomic) BOOL isRegister;
@property (assign,nonatomic) ZSTRegisterType registerType;
@property (copy, nonatomic) NSString *mobileString;
@property (assign,nonatomic) BOOL isFromLogin;

@end
