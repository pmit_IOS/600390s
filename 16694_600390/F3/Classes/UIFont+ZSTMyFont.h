//
//  UIFont+ZSTMyFont.h
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (ZSTMyFont)

+ (UIFont *)mySuitFont:(CGFloat)fontSize;

@end
