//
//  ZSTPersonInfoViewController.m
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonInfoViewController.h"
#import "ZSTUtils.h"
#import "ZSTInfoCell.h"
#import "ZSTSignViewController.h"
#import "ZSTSexView.h"
#import "ZSTBirthPickerView.h"
#import "ZSTLocationView.h"


@interface ZSTPersonInfoViewController () <UITableViewDataSource,UITableViewDelegate,ZSTSexViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIActionSheetDelegate,ZSTF3EngineDelegate,ZSTBirthPickerViewDelegate,ZSTSignViewControllerDelegate,ZSTLocationViewDelegate>

@property (strong,nonatomic) UITableView *personInfoTableView;
@property (strong,nonatomic) NSArray *titleArr;
@property (strong,nonatomic) NSArray *keyArr;
@property (strong,nonatomic) UIView *shadowView;
@property (strong,nonatomic) ZSTSexView *sexView;
@property (strong,nonatomic) ZSTBirthPickerView *birthPickerView;
@property (strong,nonatomic) ZSTLocationView *locationPickerView;
@property (copy,nonatomic) NSString *birString;
@property (strong,nonatomic) NSIndexPath *useIndexPath;
@property (strong,nonatomic) NSString *sexString;

@property (strong,nonatomic) CALayer *layer0;
@property (strong,nonatomic) CALayer *layer1;
@property (strong,nonatomic) CALayer *layer2;

@property (strong,nonatomic) CALayer *layer3;
@property (strong,nonatomic) CALayer *layer4;
@property (strong,nonatomic) CALayer *layer5;
@property (strong,nonatomic) CALayer *layer6;
@property (strong,nonatomic) CALayer *layer7;

@end

@implementation ZSTPersonInfoViewController

static NSString *const infoCell = @"infoCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"个人资料"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    self.useIndexPath = [NSIndexPath indexPathForRow:-1 inSection:0];
    self.titleArr = @[@[@"头像",@"昵称",@"性别",@"生日"],@[@"签名"],@[@"职业",@"公司",@"学校",@"所在地",@"家乡",@"电话"]];
    self.keyArr = @[@[@"Icon",@"Name",@"Sex",@"Birthday"],@[@"Signature"],@[@"Occupation",@"Company",@"School",@"Address",@"Hometown",@"Msisdn"]];
    [self buildInfoTableView];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    [self buildShadowView];
    [self buildSexPickView];
    [self buidlBirhPickerView];
    [self buildLocationPickerView];
    
    // 导航控制器全屏滑动返回上一页效果实现代码
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buidlBirhPickerView
{
    NSInteger timeCuo = [[self.infoDic safeObjectForKey:@"Birthday"] integerValue];
    NSInteger year = 0;
    NSInteger month = 0;
    NSInteger day = 0;
    
    NSMutableArray *yearArr = [NSMutableArray array];
    NSDate *nowDate = [NSDate new];
    NSInteger lastYear = nowDate.year;
    
    for (NSInteger i = nowDate.year - 100 < 1970 ? 1970 : nowDate.year; i <= lastYear; i++)
    {
        [yearArr addObject:@(i)];
    }
    
    if (!timeCuo || timeCuo == 0)
    {
        year = yearArr[0];
        month = 1;
        day = 1;
    }
    else
    {
        NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
        [formatter setDateStyle:NSDateFormatterMediumStyle];
        [formatter setTimeStyle:NSDateFormatterShortStyle];
        [formatter setDateFormat:@"yyyy-MM-dd HH:MM:ss"];
        NSDate *date = [NSDate dateWithTimeIntervalSince1970:timeCuo];
        NSString *dateString = [formatter stringFromDate:date];
        
        NSArray *dateArr = [dateString componentsSeparatedByString:@" "];
        NSArray *ymdArr = [dateArr[0] componentsSeparatedByString:@"-"];
        year = [ymdArr[0] integerValue];
        month = [ymdArr[1] integerValue];
        day = [ymdArr[2] integerValue];
    }
    
    self.birthPickerView = [[ZSTBirthPickerView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 192)];
    self.birthPickerView.nowYear = year;
    self.birthPickerView.nowMonth = month;
    self.birthPickerView.nowDay = day;
    self.birthPickerView.selectedYear = year;
    self.birthPickerView.selectedMonth = month;
    self.birthPickerView.selectedDay = day;
    self.birthPickerView.yearArr = yearArr;
    self.birthPickerView.birthDelegate = self;
    [self.birthPickerView createPickerView];
    [self.birthPickerView showDefaultShow];
    [self.view addSubview:self.birthPickerView];
}

- (void)buildInfoTableView
{
    self.personInfoTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.personInfoTableView.delegate = self;
    self.personInfoTableView.dataSource = self;
    self.personInfoTableView.backgroundColor = RGBA(243, 243, 243, 1);
    self.personInfoTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.personInfoTableView registerClass:[ZSTInfoCell class] forCellReuseIdentifier:infoCell];
    [self.view addSubview:self.personInfoTableView];
    
    UIView *footerView = [[UIView alloc] init];
    footerView.backgroundColor = RGBA(243, 243, 243, 1);
    self.personInfoTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [[self.titleArr objectAtIndex:section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:infoCell];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    // 分割线
    if (indexPath.section == 0) {
        
        if (indexPath.row == 0) {
            if (!self.layer0) {
                self.layer0 = [CALayer layer];
                self.layer0.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer0.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer0];
            }
        }
        else if (indexPath.row == 1) {
            if (!self.layer1) {
                self.layer1 = [CALayer layer];
                self.layer1.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer1.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer1];
            }
        }
        else if (indexPath.row == 2) {
            if (!self.layer2) {
                self.layer2 = [CALayer layer];
                self.layer2.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer2.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer2];
            }
        }
    }
    
    if (indexPath.section == 2) {
        if (indexPath.row == 0) {
            if (!self.layer3) {
                self.layer3 = [CALayer layer];
                self.layer3.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer3.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer3];
            }
        }
        else if (indexPath.row == 1) {
            if (!self.layer4) {
                self.layer4 = [CALayer layer];
                self.layer4.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer4.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer4];
            }
        }
        else if (indexPath.row == 2) {
            if (!self.layer5) {
                self.layer5 = [CALayer layer];
                self.layer5.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer5.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer5];
            }
        }
        else if (indexPath.row == 3) {
            if (!self.layer6) {
                self.layer6 = [CALayer layer];
                self.layer6.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer6.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer6];
            }
        }
        else if (indexPath.row == 4) {
            if (!self.layer7) {
                self.layer7 = [CALayer layer];
                self.layer7.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 1);
                self.layer7.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer7];
            }
        }
    }
    
    id contentString;
    if (indexPath.section == 0 && indexPath.row == 0)
    {
        cell.isHeaderCell = YES;
    }
    else
    {
        cell.isHeaderCell = NO;
        
    }
    NSString *keyString = self.keyArr[indexPath.section][indexPath.row];
    contentString = [self.infoDic safeObjectForKey:keyString];
    if (indexPath.section == 0 && indexPath.row == 2)
    {
        if ([contentString integerValue] == 1)
        {
            contentString = @"男";
        }
        else if ([contentString integerValue] == 2)
        {
            contentString = @"女";
        }
        else
        {
            contentString = @"未知";
        }
    }
    
    if (indexPath.section == 2 && indexPath.row == 5)
    {
        if ([contentString hasPrefix:@"4"] || [contentString hasPrefix:@"9"])
        {
            contentString = @"还没有绑定号码";
        }
    }
    
    if (indexPath.section == 0 && indexPath.row == 3)
    {
        if (contentString && ![contentString isKindOfClass:[NSNull class]]) {
            NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
            [formatter setDateStyle:NSDateFormatterMediumStyle];
            [formatter setTimeStyle:NSDateFormatterShortStyle];
            [formatter setDateFormat:@"MM-dd"];
            NSDate *date = [NSDate dateWithTimeIntervalSince1970:[contentString integerValue]];
            NSString *dateString = [formatter stringFromDate:date];
            contentString = dateString;
        }
    }
    
    [cell createUI];
    
    [cell setCellData:self.titleArr[indexPath.section][indexPath.row] AndContent:contentString];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sHeaderView = [[UIView alloc] init];
    sHeaderView.backgroundColor = RGBA(243, 243, 243, 1);
    return sHeaderView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.personInfoTableView)
    {
        CGFloat sectionHeaderHeight = 15;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.useIndexPath = indexPath;
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        ZSTInfoCell *cell = (ZSTInfoCell *)[tableView cellForRowAtIndexPath:indexPath];
        ZSTSignViewController *signVC = [[ZSTSignViewController alloc] init];
        signVC.signString = cell.rightLB.text;
        signVC.inputType = ZSTInputTypeSign;
        signVC.signDelegate = self;
        [self.navigationController pushViewController:signVC animated:YES];
    }
    
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            UIActionSheet *headerPhotoAS = [[UIActionSheet alloc] initWithTitle:@"头像上传" delegate:self cancelButtonTitle:@"取消" destructiveButtonTitle:@"拍照" otherButtonTitles:@"从相册中选择", nil];
            [headerPhotoAS showInView:self.view];
        }
        else if (indexPath.row == 3)
        {
            self.shadowView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
               
                self.birthPickerView.frame = CGRectMake(0, HEIGHT - 64 - 192, WIDTH, 192);
                
            }];
        }
        else if (indexPath.row == 2)
        {
            self.shadowView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.sexView.frame = CGRectMake(0, HEIGHT - 64 - 180, WIDTH, 180);
                
            }];
        }
        else if (indexPath.row == 1)
        {
            ZSTInfoCell *cell = (ZSTInfoCell *)[tableView cellForRowAtIndexPath:indexPath];
            ZSTSignViewController *signVC = [[ZSTSignViewController alloc] init];
            signVC.signString = cell.rightLB.text;
            signVC.signDelegate = self;
            signVC.inputType = ZSTInputTypeName;
            [self.navigationController pushViewController:signVC animated:YES];
        }
    }
    
    if (indexPath.section == 2)
    {
        if (indexPath.row != 5 && indexPath.row != 3 && indexPath.row != 4)
        {
            ZSTInfoCell *cell = (ZSTInfoCell *)[tableView cellForRowAtIndexPath:indexPath];
            ZSTSignViewController *signVC = [[ZSTSignViewController alloc] init];
            signVC.signString = cell.rightLB.text;
            signVC.signDelegate = self;
            if (indexPath.row == 0)
            {
                signVC.inputType = ZSTInputTypeJob;
            }
            else if (indexPath.row == 1)
            {
                signVC.inputType = ZSTInputTypeCompany;
            }
            else if (indexPath.row == 2)
            {
                signVC.inputType = ZSTInputTypeSchool;
            }
            [self.navigationController pushViewController:signVC animated:YES];
        }
        else if (indexPath.row == 3)
        {
            self.shadowView.hidden = NO;
            [UIView animateWithDuration:0.3f animations:^{
                
                ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
                self.locationPickerView.nowCity = cell.rightLB.text;
                [self.locationPickerView setDefaultData];
                self.locationPickerView.isHometown = NO;
                self.locationPickerView.frame = CGRectMake(0, HEIGHT - 64 - self.locationPickerView.frame.size.height, WIDTH, self.locationPickerView.frame.size.height);
                
            } completion:^(BOOL finished) {
                
            }];
        }
        else if (indexPath.row == 4)
        {
            self.shadowView.hidden = YES;
            [UIView animateWithDuration:0.3f animations:^{
                
                self.locationPickerView.isHometown = YES;
                 ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
                self.locationPickerView.nowCity = cell.rightLB.text;
                [self.locationPickerView setDefaultData];
                self.locationPickerView.frame = CGRectMake(0, HEIGHT - 64 - self.locationPickerView.frame.size.height, WIDTH, self.locationPickerView.frame.size.height);
                
            } completion:^(BOOL finished) {
                
            }];
        }
    }
}

- (void)buildShadowView
{
    self.shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.shadowView.backgroundColor = [UIColor clearColor];
    self.shadowView.hidden = YES;
    [self.view addSubview:self.shadowView];
    
    UIView *blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.6;
    [self.shadowView addSubview:blackView];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss:)];
    [blackView addGestureRecognizer:tap];
}

- (void)buildSexPickView
{
    self.sexView = [[ZSTSexView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 180)];
    self.sexView.backgroundColor = [UIColor whiteColor];
    self.sexView.sexDelegate = self;
    self.sexView.defaultSexInteger = [[self.infoDic safeObjectForKey:@"Sex"] integerValue];
    [self.sexView createSexPicker];
    [self.sexView showDefault];
    [self.view addSubview:self.sexView];
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
        {
            UIImagePickerControllerSourceType sourceType = UIImagePickerControllerSourceTypeCamera;
            if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypeCamera])
            {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                //设置拍照后的图片可被编辑
                picker.allowsEditing = YES;
                picker.sourceType = sourceType;
                [self presentViewController:picker animated:YES completion:nil];
            }
            else {
                NSLog(@"模拟其中无法打开照相机,请在真机中使用");
            }
        }
            break;
        case 1:
        {
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            picker.delegate = self;
            //设置选择后的图片可被编辑
            picker.allowsEditing = YES;
            [self presentViewController:picker animated:YES completion:nil];
        }
            break;
        default:
            break;
    }
}

-(void)imagePickerController:(UIImagePickerController*)picker didFinishPickingMediaWithInfo:(NSDictionary *)info

{
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    
    //当选择的类型是图片
    if ([type isEqualToString:@"public.image"])
    {
        //先把图片转成NSData
        UIImage *originImage = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
        
        
        //图片压缩，因为原图都是很大的，不必要传原图
        UIImage *scaleImage = [self scaleImage:originImage toScale:0.3];
        
        
        NSData *data;
        if (UIImagePNGRepresentation(scaleImage) == nil)
        {
            data = UIImageJPEGRepresentation(scaleImage, 1.0);
        }
        else
        {
            data = UIImagePNGRepresentation(scaleImage);
        }
        
        //将二进制数据生成UIImage
        UIImage *image = [UIImage imageWithData:data];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
        [cell.rightIV setImage:image];
        [self.engine uploadFileWith:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId data:data];
        [picker dismissViewControllerAnimated:YES completion:nil];
    }
}

-(UIImage *)scaleImage:(UIImage *)image toScale:(float)scaleSize
{
    UIGraphicsBeginImageContext(CGSizeMake(image.size.width*scaleSize,image.size.height*scaleSize));
    [image drawInRect:CGRectMake(0, 0, image.size.width * scaleSize, image.size.height *scaleSize)];
    UIImage *scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

- (void)uploadFileDidSucceed:(NSDictionary *)response
{
    NSString *iconurl = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Icon"];
    
    NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Icon":iconurl};
    [self.engine changeUserInfoWithParam:param];
}

- (void)uploadFileDidFailed:(NSString *)respone
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:respone withImage:nil];
}

- (void)sureBirth:(NSString *)birthString
{
    self.birString = birthString;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *date = [dateFormatter dateFromString:birthString];
    long timeSp = (long)[date timeIntervalSince1970];
    
    NSDate *nowdate = [NSDate date];
    long nowtimeSp = (long)[nowdate timeIntervalSince1970];
    
    if (nowtimeSp < timeSp) {
        
        [TKUIUtil alertInWindow:@"亲～，你穿越了吗" withImage:nil];
        return;
    }
    
    NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Birthday":@(nowtimeSp)};
    [self.engine changeUserInfoWithParam:param];
    
//     [self.engine updateUserInfoWith:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId iconUrl:@"" name:@"" birthday:timeSp sex:@"" address:@"" signature:@"" phonePublic:[ZSTF3Preferences shared].snsaStates];
    
    
    [UIView animateWithDuration:0.3f animations:^{
        
        self.birthPickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 192);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)cancelBirthPicker
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.birthPickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 192);
        
    } completion:^(BOOL finished) {
        self.shadowView.hidden = YES;
    }];
}

- (void)pickerViewDismiss:(UITapGestureRecognizer *)tap
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.birthPickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 192);
        self.sexView.frame = CGRectMake(0, HEIGHT, WIDTH, 180);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}


- (void)saveCallBack:(ZSTInputType)inputType ContentString:(NSString *)contentString
{
    if (inputType == ZSTInputTypeAddress)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:2];
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
        cell.rightLB.text = contentString;
    }
    else if (inputType == ZSTInputTypeSign)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
        cell.rightLB.text = contentString;
    }
    else if (inputType == ZSTInputTypeName)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
        cell.rightLB.text = contentString;
    }
    else
    {
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:self.useIndexPath];
        cell.rightLB.text = contentString;
    }
}

- (void)sureSexSelected:(NSInteger)sexStringInteger
{
    self.sexString = sexStringInteger == 1 ? @"男" : @"女";
    NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Sex":@(sexStringInteger)};
    [self.engine changeUserInfoWithParam:param];
}

- (void)changeUserInfoSucceed:(NSDictionary *)reponse
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:@"修改成功" withImage:nil];
    
    if (self.useIndexPath.section == 0 && self.useIndexPath.row == 3)
    {
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:self.useIndexPath];
        cell.rightLB.text = self.birString;
    }
    else if (self.useIndexPath.section == 0 && self.useIndexPath.row == 2)
    {
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:self.useIndexPath];
        cell.rightLB.text = self.sexString;
    }
    
    [UIView animateWithDuration:0.3 animations:^{
        
        self.sexView.frame = CGRectMake(0, HEIGHT , WIDTH , 180);
        self.birthPickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 192);
        self.locationPickerView.frame = CGRectMake(0, HEIGHT, WIDTH, 192);
        
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)changeUserInfoFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
}

- (void)cancelSexSelected
{
    [UIView animateWithDuration:0.3 animations:^{
        
        self.sexView.frame = CGRectMake(0, HEIGHT , WIDTH , 180);
    
    } completion:^(BOOL finished) {
        
        self.shadowView.hidden = YES;
        
    }];
}

- (void)buildLocationPickerView
{
    self.locationPickerView = [[ZSTLocationView alloc] initWithFrame:CGRectMake(0, HEIGHT, WIDTH, 192)];
    [self.locationPickerView createUI];
    self.locationPickerView.locationDelegate = self;
//    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:2];
//    ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
//    self.locationPickerView.nowCity = cell.rightLB.text;
//    [self.locationPickerView setDefaultData];
    [self.view addSubview:self.locationPickerView];
}

- (void)sureLocationSelected:(NSString *)locationString CellType:(BOOL)isHometown
{
    if (!isHometown)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:3 inSection:2];
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
        cell.rightLB.text = locationString;
        
        NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Address":locationString};
        [self.engine changeUserInfoWithParam:param];
    }
    else
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:4 inSection:2];
        ZSTInfoCell *cell = (ZSTInfoCell *)[self.personInfoTableView cellForRowAtIndexPath:indexPath];
        cell.rightLB.text = locationString;
        
        NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Hometown":locationString};
        [self.engine changeUserInfoWithParam:param];
    }
}

- (void)cancelLocationPickerView
{
    [UIView animateWithDuration:0.3f animations:^{
        
        self.locationPickerView.frame = CGRectMake(0, HEIGHT, WIDTH, self.locationPickerView.frame.size.height);
        
    } completion:^(BOOL finished) {
        self.shadowView.hidden = YES;
    }];
}

@end
