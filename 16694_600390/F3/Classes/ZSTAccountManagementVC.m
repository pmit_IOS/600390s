//
//  ZSTAccountManagementVC.m
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAccountManagementVC.h"
#import "ZSTUtils.h"
#import "ZSTMineViewController.h"
#import "ZSTSettingCell.h"
#import "ZSTF3RegisterViewController.h"
#import "ZSTThirdAccountCell.h"
#import "ZSTAMBindingMobileVC.h"
#import "ZSTAMSettingPassVC.h"
#import "ZSTF3ClientAppDelegate.h"
#import "ZSTLoginController.h"
#import "BaseNavgationController.h"
#import "ZSTNewRegisterViewController.h"

#define  kZSTSettingItemType_Password               @"Password"
#define  kZSTSettingItemType_MainAccount            @"Register"

@interface ZSTAccountManagementVC () <TencentSessionDelegate, ZSTLoginControllerDelegate>

@property (strong,nonatomic) TencentOAuth *tencentOAuth;
@property (strong,nonatomic) NSArray *permissions;

@property (strong,nonatomic) CALayer *layer0;
@property (strong,nonatomic) CALayer *layer1;
@property (strong,nonatomic) CALayer *layer2;
@property (assign,nonatomic) BOOL isBindWaiting;

@end

@implementation ZSTAccountManagementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([ZSTF3Preferences shared].wxKey)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxLoginSuccess:) name:@"wxLoginSuccess" object:nil];
    }

    if ([ZSTF3Preferences shared].qqKey)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(qqLoginSuccess:) name:@"qqLoginSuccess" object:nil];
    }
    
    if ([ZSTF3Preferences shared].weiboKey)
    {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sinaLoginSuccess:) name:@"sinaLoginSuccess" object:nil];
    }
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"登录帐号", nil)];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) style:UITableViewStyleGrouped];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerClass:[ZSTThirdAccountCell class] forCellReuseIdentifier:@"thirdCell"];
    [self.view addSubview:self.tableView];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    [self createFooterViewUI];
    
    self.isBindWaiting = NO;
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    
    [self refreshArray];
    thirdAccount = [NSMutableArray arrayWithObjects:@"绑定QQ",@"绑定微信", @"绑定新浪", nil];
    
    [self.tableView reloadData];
    
    if (self.isBindWaiting)
    {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在绑定,请稍后...", nil) withImage:nil];
    }
}

- (void)createFooterViewUI
{
    UIView *footerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    footerView.backgroundColor = [UIColor clearColor];

    // 创建退出登录按钮
    UIButton *logoutBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    logoutBtn.frame = CGRectMake(WidthRate(36), 10, WIDTH - WidthRate(72), 40);
    [logoutBtn setTitle:@"退出登录" forState:UIControlStateNormal];
    logoutBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [logoutBtn setTitleColor:RGBA(246, 148, 84, 1) forState:UIControlStateNormal];
    [logoutBtn addTarget:self action:@selector(logoutAction) forControlEvents:UIControlEventTouchUpInside];
    [logoutBtn.layer setCornerRadius:0.0f];
    [logoutBtn.layer setBorderWidth:0.5f];
    [logoutBtn.layer setBorderColor:RGBA(246, 148, 84, 1).CGColor];
    [footerView addSubview:logoutBtn];
    
    self.tableView.tableFooterView = footerView;
}

- (void)refreshArray
{
    // 如果使用手机号码注册了帐号，就显示修改密码和更换手机号，否则显示完善帐号资料
    if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"1"]) {
        accountItems = [NSMutableArray arrayWithObjects:@"修改密码",@"更换手机号", nil];
    }
    else {
        accountItems = [NSMutableArray arrayWithObjects:@"完善帐号资料", nil];
    }
}

#pragma mark - tableView dataSource delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([ZSTF3Preferences shared].wxKey.length != 0 || [ZSTF3Preferences shared].qqKey.length != 0 || [ZSTF3Preferences shared].weiboKey.length != 0) {
        return 2;
    }
    else {
        return 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return accountItems.count;
    }
    else if (section == 1) {
        return thirdAccount.count;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 1 && indexPath.row == 0)
    {
        if ([ZSTF3Preferences shared].qqKey.length == 0)
        {
            return 0;
        }
        else
        {
            return 45.0f;
        }
    }
    
    if (indexPath.section == 1 && indexPath.row == 1)
    {
        if ([ZSTF3Preferences shared].wxKey.length == 0)
        {
            return 0;
        }
        else
        {
            return 45.0f;
        }
    }
    
    if (indexPath.section == 1 && indexPath.row == 2)
    {
        if ([ZSTF3Preferences shared].weiboKey.length == 0)
        {
            return 0;
        }
        else
        {
            return 45.0f;
        }
    }
    return 45.f;
}

// 别忘了设置高度
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 35.0f;
    }
    else if (section == 1) {
        if ([ZSTF3Preferences shared].wxKey.length == 0 && [ZSTF3Preferences shared].qqKey.length == 0 && [ZSTF3Preferences shared].weiboKey.length == 0) {
            return 0;
        }
        else
        {
            return 17.0f;
        }
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        NSString *titleName = @"登录帐号";
        return titleName;
    }
    else if (section == 1) {
        NSString *titleName = @"绑定帐号";
        if ([ZSTF3Preferences shared].wxKey.length == 0 && [ZSTF3Preferences shared].qqKey.length == 0 && [ZSTF3Preferences shared].weiboKey.length == 0)
        {
            titleName = @"";
        }
        return titleName;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTThirdAccountCell *cell = [tableView dequeueReusableCellWithIdentifier:@"thirdCell"];
    
    if (indexPath.section == 0) {
        [cell createThirdAccountUI];
        
        if (indexPath.row == 0) {
            [cell setThirdAccountTitle:accountItems[indexPath.row] state:nil];
            
            // 分割线
            if (!self.layer0) {
                self.layer0 = [CALayer layer];
                self.layer0.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 0.5f);
                self.layer0.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer0];
            }
        }
        else if (indexPath.row == 1) {
            [cell setThirdAccountTitle:accountItems[indexPath.row] state:[ZSTF3Preferences shared].loginMsisdn];
        }
        
        return cell;
    }
    else if (indexPath.section == 1) {
        
        [cell createThirdAccountUI];
        
        
        if (indexPath.row == 0) {
            if (![ZSTF3Preferences shared].qqKey || [[ZSTF3Preferences shared].qqKey isEqualToString:@""]) {
                cell.titleLab.hidden = YES;
                cell.stateLab.hidden = YES;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.titleLab.hidden = NO;
                cell.stateLab.hidden = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setThirdAccountTitle:thirdAccount[indexPath.row] state:[ZSTF3Preferences shared].qqOpenId.length == 0 ? @"未绑定" : @"已绑定"];
            }
            
            // 分割线
            if (!self.layer1) {
                self.layer1 = [CALayer layer];
                self.layer1.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 0.5f);
                self.layer1.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer1];
            }
        }
        else if (indexPath.row == 1) {
            if (![ZSTF3Preferences shared].wxKey || [[ZSTF3Preferences shared].wxKey isEqualToString:@""]) {
                cell.titleLab.hidden = YES;
                cell.stateLab.hidden = YES;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.titleLab.hidden = NO;
                cell.stateLab.hidden = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setThirdAccountTitle:thirdAccount[indexPath.row] state:[ZSTF3Preferences shared].weixinOpenId.length == 0 ? @"未绑定" : @"已绑定"];
            }
            
            // 分割线
            if (!self.layer2) {
                self.layer2 = [CALayer layer];
                self.layer2.frame = CGRectMake(15, cell.contentView.frame.size.height, WIDTH - 15, 0.5f);
                self.layer2.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
                [cell.contentView.layer addSublayer:self.layer2];
            }
        }
        else if (indexPath.row == 2) {
            if (![ZSTF3Preferences shared].weiboKey || [[ZSTF3Preferences shared].weiboKey isEqualToString:@""]) {
                cell.titleLab.hidden = YES;
                cell.stateLab.hidden = YES;
                cell.accessoryType = UITableViewCellAccessoryNone;
            }
            else
            {
                cell.titleLab.hidden = NO;
                cell.stateLab.hidden = NO;
                cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                [cell setThirdAccountTitle:thirdAccount[indexPath.row] state:[ZSTF3Preferences shared].sinaOpenId.length == 0 ? @"未绑定" : @"已绑定"];
            }
        }
        
        return cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    if (indexPath.section == 0) {
        if (indexPath.row == 0) {
            
            if ([[ZSTF3Preferences shared].loginMsisdn hasPrefix:@"1"]) {

                ZSTNewRegisterViewController *controller = [[ZSTNewRegisterViewController alloc] init];
                controller.registerType = ZSTRegisterTypeChangePass;
                [self.navigationController pushViewController:controller animated:YES];
                
            }
            else {
                // 跳转到绑定帐号
                ZSTAMBindingMobileVC *bindingMobileVC = [[ZSTAMBindingMobileVC alloc] init];
                bindingMobileVC.registerType = RegisterTypeBinding;
                [self.navigationController pushViewController:bindingMobileVC animated:YES];
            }
            
        } else if (indexPath.row == 1) {
            
            ZSTNewRegisterViewController *controller = [[ZSTNewRegisterViewController alloc] init];
            controller.registerType = ZSTRegisterTypeChangePhoe;
            controller.mobileString = [ZSTF3Preferences shared].loginMsisdn;
            [self.navigationController pushViewController:controller animated:YES];
        }
    }
    else {
        ZSTF3ClientAppDelegate *app = (ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate;
        
        if (indexPath.row == 0)
        {
            _tencentOAuth = [[TencentOAuth alloc] initWithAppId:[ZSTF3Preferences shared].qqKey andDelegate:self];
            _tencentOAuth.redirectURI = @"www.qq.com";
            _permissions =  [NSArray arrayWithObjects:@"get_user_info", @"get_simple_userinfo", @"add_t", nil];
            
            // 向服务器发出认证请求
            [_tencentOAuth authorize:_permissions inSafari:NO];
        }
        else if (indexPath.row == 1)
        {
            if ([WXApi isWXAppInstalled] || [WXApi isWXAppSupportApi])
            {
                [app sendAuthRequest];
            }
            else
            {
                NSLog(@"还没有装微信");
            }
        }
        else
        {
            WBAuthorizeRequest *request = [WBAuthorizeRequest request];
            request.redirectURI = [ZSTF3Preferences shared].weiboRedirect;//@"http://www.sina.com"
            request.scope = @"all";
            request.userInfo = @{@"SSO_From": @"ZSTAccountManagementVC",
                                 @"Other_Info_1": [NSNumber numberWithInt:123],
                                 @"Other_Info_2": @[@"obj1", @"obj2"],
                                 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
            [WeiboSDK sendRequest:request];
        }
    }
}



#pragma mark - 退出登录按钮响应事件
- (void)logoutAction
{
    [self.engine logoutWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
    
    [ZSTF3Preferences shared].UserId = @"";
    [ZSTF3Preferences shared].UID = @"";
    [ZSTF3Preferences shared].isChange = YES;
    [ZSTF3Preferences shared].personIcon = @"";
    [ZSTF3Preferences shared].personName = @"";
    [ZSTF3Preferences shared].loginMsisdn = @"";
    
    ZSTF3ClientAppDelegate *app = (ZSTF3ClientAppDelegate *)[UIApplication sharedApplication].delegate;
    app.hasChange = YES;
    
    if ([ZSTF3Preferences shared].MCRegistType != MCRegistType_VirtualRegister) {
        
//        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
//        controller.isFromSetting = YES;
//        controller.delegate = self;
//        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
//        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
//        }];
        [self.navigationController popViewControllerAnimated:YES];
        
        
    }
    else {
//        ZSTLoginController *controller = [[ZSTLoginController alloc] init];
//        controller.isFromSetting = YES;
//        controller.delegate = self;
//        BaseNavgationController *navicontroller = [[BaseNavgationController alloc] initWithRootViewController:controller];
//        [self.navigationController presentViewController:navicontroller animated:YES completion:^(void) {
//        }];
        [self.navigationController popToRootViewControllerAnimated:YES];
        
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)wxLoginSuccess:(NSNotification *)notification
{
    [self.tableView reloadData];
}

- (void)sinaLoginSuccess:(NSNotification *)notification
{
    [self.tableView reloadData];
}

- (void)qqLoginSuccess:(NSNotification *)notification
{
    [self.tableView reloadData];
}

- (void)tencentDidLogin
{
    if (_tencentOAuth.accessToken && 0 != [_tencentOAuth.accessToken length])
    {
        //  记录登录用户的OpenID、Token以及过期时间
        //_labelAccessToken.text = _tencentOAuth.accessToken;
        NSLog(@"_tencentOAuth.accessToken==>%@",_tencentOAuth.accessToken);
        NSLog(@"_tencentOAuth.openid==>%@",_tencentOAuth.openId);
        [ZSTF3Preferences shared].qqOpenId = _tencentOAuth.openId;
        [[NSUserDefaults standardUserDefaults] setValue:_tencentOAuth.openId forKey:@"qqOpenId"];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"qqLoginSuccess" object:nil];
        
        [_tencentOAuth getUserInfo];
        
    }
    else
    {
        NSLog(@"登录不成功 没有获取accesstoken");
    }
}

- (void)getUserInfoResponse:(APIResponse*) response
{
    //NSLog(@"HHHHHHHHH ==== %@", response.jsonResponse);
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    NSString *openId = [ud objectForKey:@"qqOpenId"];
    NSString *avatar = [response.jsonResponse safeObjectForKey:@"figureurl_qq_1"];
    NSString *nickName = [response.jsonResponse safeObjectForKey:@"nickname"];
    NSString *platformType = @"7";//第三方登录：QQ开放平台
    
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:openId forKey:@"OpenId"];
    [data setObject:avatar forKey:@"Avatar"];
    [data setObject:nickName forKey:@"NickName"];
    [data setObject:platformType forKey:@"PlatformType"];

    [self.engine thirdLoginWithDictionary:data];
}

- (void)thirdLoginDidSucceed:(NSDictionary *)response
{
    
}

@end
