//
//  ZSTAMSettingPassVC.m
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTAMSettingPassVC.h"
#import "ZSTUtils.h"
#import "PMRepairButton.h"
#import "ZSTMineCenterViewController.h"

@interface ZSTAMSettingPassVC ()

@end

@implementation ZSTAMSettingPassVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    //self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector(popViewController)];
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"完善帐号资料", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    [self createFirstViewUI];
    [self createButtonControllerUI];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
}

- (void)createFirstViewUI
{
    UIView *firstView = [[UIView alloc] initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 80)];
    firstView.backgroundColor = [UIColor clearColor];
    self.firstView = firstView;
    [self.view addSubview:firstView];
    
    UILabel *settingLab = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 100, 30)];
    settingLab.backgroundColor = [UIColor clearColor];
    settingLab.text = @"设置密码:";
    settingLab.textColor = RGBACOLOR(175, 175, 175, 1);
    settingLab.textAlignment = NSTextAlignmentLeft;
    settingLab.font = [UIFont boldSystemFontOfSize:16.0f];
    [firstView addSubview:settingLab];
    
    // 密码图片
    UIImageView *lockIma = [[UIImageView alloc] initWithFrame:CGRectMake(50, 40, 20, 30)];
    lockIma.image = ZSTModuleImage(@"lock.png");
    lockIma.contentMode = UIViewContentModeScaleAspectFit;
    [firstView addSubview:lockIma];
    
    // 手机号输入框
    UITextField *passTF = [[UITextField alloc] initWithFrame:CGRectMake(80, 45, firstView.frame.size.width - 120, 30)];
    passTF.delegate = self;
    passTF.borderStyle = UITextBorderStyleNone;
    passTF.secureTextEntry = YES;
    passTF.tag = 1;
    passTF.textColor = RGBACOLOR(100, 100, 100, 1);
    passTF.textAlignment = NSTextAlignmentLeft;
    passTF.font = [UIFont systemFontOfSize:14.0f];
    passTF.placeholder = @"6~16个字符，区分大小写";
    passTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    passTF.returnKeyType = UIReturnKeyDone;
    passTF.keyboardType = UIKeyboardTypeDefault;
    self.passTF = passTF;
    [firstView addSubview:passTF];
    
    // 分隔线
    CALayer *layer = [[CALayer alloc] init];
    layer.frame = CGRectMake(40, 80 - 0.5f, firstView.frame.size.width - 80, 0.5f);
    layer.backgroundColor = RGBACOLOR(220, 220, 220, 1).CGColor;
    [firstView.layer addSublayer:layer];
}

- (void)createButtonControllerUI
{
    // 初始化显示密码按钮
    PMRepairButton *showButton = [[PMRepairButton alloc] init];
    showButton.frame = CGRectMake(self.view.frame.size.width - 120, self.firstView.origin.y + self.firstView.frame.size.height + 5, 80, 18);
    [showButton setImage:ZSTModuleImage(@"showpass_no.png") forState:UIControlStateNormal];
    [showButton setImage:ZSTModuleImage(@"showpass_yes.png") forState:UIControlStateSelected];
    [showButton.imageView setContentMode:UIViewContentModeScaleAspectFit];
    [showButton.titleLabel setFont:[UIFont systemFontOfSize:13.0f]];
    [showButton setTitle:@"显示密码" forState:UIControlStateNormal];
    [showButton setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [showButton addTarget:self action:@selector(showPassButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:showButton];
    
    // 创建立即绑定手机号按钮
    UIButton *finishBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    finishBtn.frame = CGRectMake(40, self.firstView.origin.y + self.firstView.frame.size.height + 40, self.view.frame.size.width - 80, 50);
    [finishBtn setTitle:@"完成注册" forState:UIControlStateNormal];
    [finishBtn.titleLabel setFont:[UIFont systemFontOfSize:15.0f]];
    [finishBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [finishBtn addTarget:self action:@selector(finishLoginClick:) forControlEvents:UIControlEventTouchUpInside];
    [finishBtn setBackgroundColor:[UIColor orangeColor]];
    [self.view addSubview:finishBtn];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.passTF.tag == 1 && self.passTF == textField) {
        NSInteger maxLength = 16;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 点击背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

- (void)showPassButtonClick:(UIButton *)button
{
    // 改变按钮的选中状态
    button.selected = !button.selected;
    if (button.selected) {
        self.passTF.secureTextEntry = NO;
    }
    if (!button.selected) {
        self.passTF.secureTextEntry = YES;
    }
}

- (void)finishLoginClick:(UIButton *)sender
{
    [self.passTF resignFirstResponder];
    
    NSString *passWordStr = [self.passTF.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if(passWordStr.length == 0 || passWordStr.length < 6 || [ZSTUtils isEmpty:passWordStr]) {
        
        [TKUIUtil alertInWindow:@"亲，您输入的密码格式错误" withImage:nil];
        return;
    }
    
    [TKUIUtil showHUD:self.view];
    [self.engine finishedLoginWithPassWord:passWordStr AndUserId:[ZSTF3Preferences shared].UserId MobilePhone:self.mobilePhone Code:self.verifyCode];
}

#pragma mark ------------------ZSTF3EngineDelegate---------------------

- (void)finishedLoginDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    NSLog(@"response --> %@",response);
    if ([[response safeObjectForKey:@"code"] integerValue] == 1) {
        
        [ZSTF3Preferences shared].loginMsisdn = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Msisdn"];
        [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
        
        if ([[self.navigationController.viewControllers objectAtIndex:1] isKindOfClass:[ZSTMineCenterViewController class]])
        {
            ZSTMineCenterViewController *viewController = (ZSTMineCenterViewController *)[self.navigationController.viewControllers objectAtIndex:1];
            viewController.isShareApp = YES;
        }
        
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:1] animated:YES];
    }
}

- (void)finishedLoginDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



@end
