//
//  SqlViewRender.m
//  CCBClient
//
//  Created by 谢伟 on 10-9-25.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "ZSTSqlManager.h"
#import "PlausibleDatabase.h"
#import "ZSTLogUtil.h"
#import "ZSTUtils.h"

#define createDBsql @" \n"\
"     \n"\
"    CREATE TABLE IF NOT EXISTS [BusinessAppInfo] \n"\
"    ( \n"\
"       [ID] INTEGER PRIMARY KEY,\n"\
"       [AppID] TEXT NOT NULL UNIQUE DEFAULT '',\n"\
"       [Name] TEXT NOT NULL DEFAULT '',\n"\
"       [ECECCID] TEXT NULL DEFAULT '',\n"\
"       [IconKey] TEXT NULL DEFAULT '',\n"\
"       [Introduce] TEXT NOT NULL DEFAULT '',\n"\
"       [Version] TEXT NOT NULL DEFAULT '',\n"\
"       [Status] TEXT NOT NULL DEFAULT '',\n"\
"       [appOrder] INTEGER DEFAULT 0,\n"\
"       [Url] TEXT NULL DEFAULT '',\n"\
"       [Local] TEXT NULL DEFAULT '',\n"\
"       [ModuleID] INTEGER NULL DEFAULT 0,\n"\
"       [ModuleType] INTEGER NULL DEFAULT 0,\n"\
"       [InterfaceUrl] TEXT NULL DEFAULT ''\n"\
"     ); \n"\
"     \n"\
"    CREATE TABLE IF NOT EXISTS [UserLog] \n"\
"    ( \n"\
"       [ID] INTEGER PRIMARY KEY,  \n"\
"       [Time] Double NOT NULL DEFAULT 0,  \n"\
"       [LogType] Text NOT NULL DEFAULT '',  \n"\
"       [Description] Text NOT NULL DEFAULT ''\n"\
"     ); \n"\
"  \n"\

#define CurrentVersion 3

#define RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(sel)             \
NSError *outError = nil;\
id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&outError];\
if (!outError) {\
    NSMutableArray *argsArray = [NSMutableArray array];\
    va_list args;\
    va_start(args, sql);\
    int parameterCount = [ps parameterCount];\
    for (int i = 0; i < parameterCount; i++) {\
        id paramter = va_arg(args, id);\
        if (paramter) {\
            [argsArray addObject:paramter];\
        } else {\
            NSAssert(NO ,@"Prepared statement provided invalid parameter count (expected %d, but %d were provided)", parameterCount, i);\
        }\
    }\
    va_end(args);\
    [ps bindParameters:argsArray];\
    id<PLResultSet> rs = [ps executeQueryAndReturnError:&outError];\
    if (rs.next) {\
        return [rs sel:0];\
    }\
}\
return 0x00;

@implementation ZSTSqlManager

// g_plDatabase类型:类静态变量
PLSqliteDatabase *g_plDatabase;
NSCondition *g_condition;

+(NSString *) dbPath
{
	static NSString *dbPath = nil;
	if (!dbPath)
	{
		dbPath = [[[ZSTUtils pathForECECC] stringByAppendingPathComponent: @"ad.sqlite"] retain];
	}
    return dbPath;
}

+(BOOL) dbExists
{
    return [[NSFileManager defaultManager] fileExistsAtPath: [self dbPath]];
}

+(void) openDatabase
{
    if(nil == g_plDatabase)
    {
        //NSString *dbPath = @":memory:";
        NSString *dbPath = [self dbPath];
        
        //数据库升级管理
        PLSqliteConnectionProvider *provider = [[PLSqliteConnectionProvider alloc] initWithPath:dbPath];
        PLSqliteMigrationVersionManager *migrationVersionManager = [[PLSqliteMigrationVersionManager alloc] init];
        
        PLDatabaseMigrationManager *migrationManager = [[PLDatabaseMigrationManager alloc] initWithConnectionProvider:provider transactionManager:migrationVersionManager versionManager:migrationVersionManager delegate:(id<PLDatabaseMigrationDelegate>)self];
        [migrationManager migrateAndReturnError:nil];
        [migrationManager release];
        
        [migrationVersionManager release];
        [provider release];
        
        g_plDatabase = [[PLSqliteDatabase alloc] initWithPath: dbPath];
        BOOL bOpen = [g_plDatabase open];
        
        [ZSTLogUtil logSysInfo:[NSString stringWithFormat:@"dbPath = %@, bOpen = %d", [self dbPath], bOpen]];
        
        if (bOpen)
        {
            [ZSTLogUtil logSysInfo:@"Database opened OK!"];
        }
        
        g_condition = [[NSCondition alloc] init];
    }
}

//数据库升级管理
#pragma mark - PLDatabaseMigrationDelegate
+ (BOOL) migrateDatabase: (id<PLDatabase>) database currentVersion: (int) currentVersion newVersion: (int *) newVersion error: (NSError **) outError
{
    *newVersion = CurrentVersion;
    
    if (currentVersion == 0) {
        
        //创建表结构
        NSArray *sqlArray = [createDBsql componentsSeparatedByString:@";"];
        for (NSString *sqlString in sqlArray) 
        {
            sqlString = [sqlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if ([sqlString length] > 0)
            {
                [database executeUpdate:sqlString];
            }
        }
        
        //添加fileId字段
        id<PLResultSet> rs = [database executeQuery:@"select sql from sqlite_master where tbl_name='MessageInfo' and type='table'"];
        
        if ([rs next]) {
            NSString *sql = [rs stringForColumn:@"sql"];
            
            if ([sql rangeOfString:@"fileId"].location == NSNotFound) {
                
                if (![database executeUpdateAndReturnError:outError statement:@"ALTER TABLE 'MessageInfo' ADD 'fileId' VARCHAR( 10 ) NULL"]) {
                    NSLog(@"%@", *outError);
                };
            }
        }
        [rs close];
    }
    
    if (currentVersion < 2) {
        
        //添加NativeType,InterfaceUrl字段
        id<PLResultSet> rs2 = [database executeQuery:@"select sql from sqlite_master where tbl_name='BusinessAppInfo' and type='table'"];
        if ([rs2 next]) {
            NSString *sql = [rs2 stringForColumn:@"sql"];
            if ([sql rangeOfString:@"InterfaceUrl"].location == NSNotFound) {
                if (![database executeUpdateAndReturnError:outError statement:@"ALTER TABLE 'BusinessAppInfo' ADD 'InterfaceUrl' TEXT NULL DEFAULT ''"]) {
                    NSLog(@"%@", *outError);
                };
            }
        }
        [rs2 close];
    }
    
    if (currentVersion < 3) {
        
        //添加ModuleType,ModuleId字段
        id<PLResultSet> rs2 = [database executeQuery:@"select sql from sqlite_master where tbl_name='BusinessAppInfo' and type='table'"];
        if ([rs2 next]) {
            NSString *sql = [rs2 stringForColumn:@"sql"];
            
            if ([sql rangeOfString:@"ModuleType"].location == NSNotFound) {
                
                if (![database executeUpdateAndReturnError:outError statement:@"ALTER TABLE 'BusinessAppInfo' ADD 'ModuleType' INTEGER NULL DEFAULT 0"]) {
                    NSLog(@"%@", *outError);
                };
            }
            
            if ([sql rangeOfString:@"ModuleID"].location == NSNotFound) {
                if (![database executeUpdateAndReturnError:outError statement:@"ALTER TABLE 'BusinessAppInfo' ADD 'ModuleID' INTEGER NULL DEFAULT 0"]) {
                    NSLog(@"%@", *outError);
                };
            }
        }
        [rs2 close];
    }
    return YES;
}

+(void) closeDatabase
{
    [g_condition release];
    [g_plDatabase release];
}

+(void) lock
{
    [g_condition lock];
}

+(void) unlock
{
    [g_condition unlock];
}

+(void) executeSqlWithSqlStrings: (NSString *) sqlStrings
{
    NSArray *sqlArray = [sqlStrings componentsSeparatedByString: @";"];
    
    for (NSString *sqlString in sqlArray) 
    {
        sqlString = [sqlString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if ([sqlString length] > 0)
        {
            [self executeUpdate:sqlString];
        }
    }
}

+(NSInteger)executeInsert:(NSString *)sql, ...
{
    NSError *outError = nil;
    id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&outError];
    if (!outError) {
        
        NSMutableArray *argsArray = [NSMutableArray array];
        va_list args;
        va_start(args, sql);
        int parameterCount = [ps parameterCount];
        for (int i = 0; i < parameterCount; i++) {
            id paramter = va_arg(args, id);
            if (paramter) {
                [argsArray addObject:paramter];
            } else {
                NSAssert(NO ,@"Prepared statement provided invalid parameter count (expected %d, but %d were provided)", parameterCount, i);
            }
        }
        va_end(args);
        
        [ps bindParameters:argsArray];
        
        [self lock];
        BOOL success = [ps executeUpdateAndReturnError:&outError];
        [self unlock];
        return success?[g_plDatabase lastInsertRowId]:-1;
    }
    return -1;
}

+(NSInteger)executeInsert:(NSString *)sql arguments:(NSArray *)args
{
    NSError *outError = nil;
    id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&outError];
    if (!outError) {
        [ps bindParameters:args];
        [self lock];
        BOOL success = [ps executeUpdateAndReturnError:&outError];
        [self unlock];
        return success?[g_plDatabase lastInsertRowId]:-1;
    }
    return -1;
}

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

+ (NSArray *)executeQuery:(NSString *)sql, ... {
    
    NSMutableArray *arrayList = [NSMutableArray array];
    NSError *outError = nil;
    id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&outError];
    if (!outError) {
        
        NSMutableArray *argsArray = [NSMutableArray array];
        va_list args;
        va_start(args, sql);
        int parameterCount = [ps parameterCount];
        for (int i = 0; i < parameterCount; i++) {
            id paramter = va_arg(args, id);
            if (paramter) {
                [argsArray addObject:paramter];
            } else {
                NSAssert(NO ,@"Prepared statement provided invalid parameter count (expected %d, but %d were provided)", parameterCount, i);
            }
        }
        va_end(args);
        
        [ps bindParameters:argsArray];
        id<PLResultSet> rs = [ps executeQueryAndReturnError:&outError];
        int columnCount = [rs columnCount];
        while (rs.next) {
            NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
            for (int i = 0; i < columnCount; ++i) {
                [dictionary setObject:[rs objectForColumnIndex:i] forKey:[rs nameForColumnIndex:i]];
            }
            [arrayList addObject:[dictionary autorelease]];
        }
    }
    return arrayList;
}

+ (NSArray *)executeQuery:(NSString *)sql arguments:(NSArray *)args {
    
    NSMutableArray *arrayList = [NSMutableArray array];
    NSError *outError = nil;
    id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&outError];
    if (!outError) {
        [ps bindParameters:args];
        id<PLResultSet> rs = [ps executeQueryAndReturnError:&outError];
        int columnCount = [rs columnCount];
        while (rs.next) {
            NSMutableDictionary *dictionary = [[NSMutableDictionary alloc] init];
            for (int i = 0; i < columnCount; ++i) {
                [dictionary setObject:[rs objectForColumnIndex:i] forKey:[rs nameForColumnIndex:i]];
            }
            [arrayList addObject:[dictionary autorelease]];
        }
    }
    return arrayList;
}

+ (BOOL)executeUpdate:(NSString *)sql, ... {
    
    NSError *error = nil;
    id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&error];
    if (!error) {
        
        NSMutableArray *argsArray = [NSMutableArray array];
        va_list args;
        va_start(args, sql);
        int parameterCount = [ps parameterCount];
        for (int i = 0; i < parameterCount; i++) {
            id paramter = va_arg(args, id);
            if (paramter) {
                [argsArray addObject:paramter];
            } else {
                NSAssert(NO ,@"Prepared statement provided invalid parameter count (expected %d, but %d were provided)", parameterCount, i);
            }
        }
        va_end(args);
        
        [ps bindParameters:argsArray];
        
        [self lock];
        BOOL success = [ps executeUpdateAndReturnError:&error];
        [self unlock];
        return success;
    }
    return NO;
}

+ (BOOL)executeUpdate:(NSString *)sql arguments:(NSArray *)args {    
    NSError *error = nil;
    id<PLPreparedStatement> ps = [g_plDatabase prepareStatement:(NSString *)sql error:&error];
    if (!error) {
        [ps bindParameters:args];
        [self lock];
        BOOL success = [ps executeUpdateAndReturnError:&error];
        [self unlock];
        return success;
    }
    return NO;
}

+ (BOOL)commit {
    return [self executeUpdate:@"COMMIT TRANSACTION;"];
}

+ (BOOL)rollback {
    return [self executeUpdate:@"ROLLBACK TRANSACTION;"];
}

+ (BOOL)beginTransaction {
    return [self executeUpdate:@"BEGIN EXCLUSIVE TRANSACTION;"];
}

+ (BOOL)beginDeferredTransaction {
    return [self executeUpdate:@"BEGIN DEFERRED TRANSACTION;"];
}

+ (BOOL)tableExists:(NSString*)tableName {
    tableName = [tableName lowercaseString];
    NSArray *result = [self executeQuery:@"select [sql] from sqlite_master where [type] = 'table' and lower(name) = ?", tableName];
    return result.count > 0;
}

+ (BOOL)indexExists:(NSString*)indexName {
    indexName = [indexName lowercaseString];
    NSArray *result = [self executeQuery:@"select [sql] from sqlite_master where [type] = 'index' and lower(name) = ?", indexName];
    return result.count > 0;
}

+ (NSString*)stringForQuery:(NSString*)sql, ... {
    RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(stringForColumnIndex);
}

+ (int)intForQuery:(NSString*)sql, ... {
    RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(intForColumnIndex);
}

+ (long)longForQuery:(NSString*)sql, ... {
    RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(bigIntForColumnIndex);
}

+ (BOOL)boolForQuery:(NSString*)sql, ... {
    RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(boolForColumnIndex);
}

+ (double)doubleForQuery:(NSString*)sql, ... {
    RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(doubleForColumnIndex);
}

+ (NSData*)dataForQuery:(NSString*)sql, ... {
    RETURN_RESULT_FOR_QUERY_WITH_SELECTOR(dataForColumnIndex);
}

@end
