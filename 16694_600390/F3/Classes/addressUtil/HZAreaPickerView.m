//
//  HZAreaPickerView.m
//  areapicker
//
//  Created by Cloud Dai on 12-9-9.
//  Copyright (c) 2012年 clouddai.com. All rights reserved.
//

#import "HZAreaPickerView.h"
#import <QuartzCore/QuartzCore.h>

#define kDuration 0.3

@interface HZAreaPickerView ()
{
    NSArray *provinces, *cities, *areas;
}

@end

@implementation HZAreaPickerView

@synthesize delegate=_delegate;
@synthesize pickerStyle=_pickerStyle;
@synthesize locate=_locate;
@synthesize locatePicker = _locatePicker;

-(HZLocation *)locate
{
    if (_locate == nil) {
        _locate = [[HZLocation alloc] init];
    }
    
    return _locate;
}

- (id)initWithStyle:(HZAreaPickerStyle)pickerStyle delegate:(id<HZAreaPickerDelegate>)delegate
{
    
    self = [[[NSBundle mainBundle] loadNibNamed:@"HZAreaPickerView" owner:self options:nil] objectAtIndex:0];
    if (self) {
        self.delegate = delegate;
        self.pickerStyle = pickerStyle;
        self.locatePicker.dataSource = self;
        self.locatePicker.delegate = self;
        self.locatePicker.backgroundColor = RGBACOLOR(247, 247, 247, 1);
        
        //加载数据
        if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
//            provinces = [[NSArray alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"area.plist" ofType:nil]];
//            cities = [[provinces objectAtIndex:0] objectNullForKey:@"cities"];
//            
//            self.locate.state = [[provinces objectAtIndex:0] objectNullForKey:@"state"];
//            self.locate.city = [[cities objectAtIndex:0] objectNullForKey:@"city"];
//            
//            areas = [[cities objectAtIndex:0] objectNullForKey:@"areas"];
//            if (areas.count > 0) {
//                self.locate.district = [areas objectAtIndex:0];
//            } else{
//                self.locate.district = @"";
//            }
            NSString *path = [[NSBundle mainBundle] pathForResource:@"area.json" ofType:nil];
            NSData *data = [NSData dataWithContentsOfFile:path];
            self.jsonDic =  [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
            NSLog(@"provinceDic --> %@",self.jsonDic);
            NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
            NSArray *provinceKeyArr = [provinceDic allKeys];
            provinces = [provinceKeyArr sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                
                NSInteger num1 = [obj1 integerValue];
                NSInteger num2 = [obj2 integerValue];
                if (num1 > num2)
                {
                    return 1;
                }
                return -1;
            }];
            NSString *firstKey = [provinces firstObject];
            cities = [[self.jsonDic objectForKey:@"area1"] objectForKey:firstKey];
            NSArray *cityFirstArr = [cities firstObject];
            NSString *cityKey = [cityFirstArr lastObject];
            NSDictionary *areaDic = [self.jsonDic objectForKey:@"area2"];
            areas = [areaDic objectForKey:cityKey];
        }
    }
        
    return self;
}

#pragma mark - PickerView lifecycle
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 0:
            return [provinces count];
            break;
        case 1:
            return [cities count];
            break;
        case 2:
            if (self.pickerStyle == HZAreaPickerWithStateAndCityAndDistrict) {
                return [areas count];
                break;
            }
        default:
            return 0;
            break;
    }
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
    if (true) {
        switch (component) {
            case 0:
            {
                NSString *pKey = [provinces objectAtIndex:row];
                return [provinceDic objectForKey:pKey];
            }
                break;
            case 1:
                return [[cities objectAtIndex:row] lastObject];
                break;
            default:
                return  @" ";
                break;
        }
    }
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (true) {
        switch (component) {
            case 0:
            {
                NSString *key = [provinces objectAtIndex:row];
                self.locate.provinceId = key;
                cities = [[self.jsonDic objectForKey:@"area1"] objectForKey:key];
                [self.locatePicker selectRow:0 inComponent:1 animated:YES];
                [self.locatePicker reloadComponent:1];
                
                NSArray *cityFirstArr = [cities firstObject];
                NSString *cityKey = [cityFirstArr firstObject];
                self.locate.cityId = cityKey;
                
                NSString *provinceKey = [provinces objectAtIndex:row];
                NSString *provinceValue = [[self.jsonDic objectForKey:@"area0"] objectForKey:provinceKey];
                
                self.locate.state = provinceValue;
                self.locate.city = [[cities objectAtIndex:0] lastObject];
                self.locate.cityId = [[cities objectAtIndex:0] firstObject];
            }
                break;
            case 1:
            {
                NSString *cityKey = [[cities objectAtIndex:row] firstObject];
                self.locate.cityId = cityKey;
                self.locate.city = [[cities objectAtIndex:row] lastObject];
            }
                break;
            default:
                break;
        }
    }
    
    if ([self.delegate respondsToSelector:@selector(pickerDidChaneStatus:)]) {
        [self.delegate pickerDidChaneStatus:self];
    }
}

#pragma mark - animation

- (void)showInView:(UIView *) view
{
    self.frame = CGRectMake(0, view.frame.size.height, view.frame.size.width, self.frame.size.height);
    [view addSubview:self];
    
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0, view.frame.size.height - self.frame.size.height, view.frame.size.width, self.frame.size.height);
    }];
}

- (void)cancelPicker
{
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.frame = CGRectMake(0, self.frame.origin.y+self.frame.size.height, self.frame.size.width, self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         [self removeFromSuperview];
                     }];
}

- (void)selectDefaultCity:(NSString *)pCityName
{
    NSDictionary *provinceDic = [self.jsonDic objectForKey:@"area0"];
    NSInteger defaultProvinceIndex = -1;
    NSString *defaultProvinceKey = @"";
    for (NSInteger i = 0; i < provinces.count; i++)
    {
        NSString *provinceKey = [provinces objectAtIndex:i];
        NSString *provinceName = [provinceDic objectForKey:provinceKey];
        if ([pCityName rangeOfString:provinceName].length > 0)
        {
            defaultProvinceIndex = i;
            defaultProvinceKey = provinceKey;
            break;
        }
    }
    
    cities = [[self.jsonDic objectForKey:@"area1"] objectForKey:defaultProvinceKey];
    NSInteger defaultCityIndex = -1;
    for (NSInteger i = 0 ; i < cities.count; i++)
    {
        NSString *cityName = [[cities objectAtIndex:i] lastObject];
        if ([pCityName rangeOfString:cityName].length > 0)
        {
            defaultCityIndex = i;
        }
    }
    
    [self.locatePicker selectRow:defaultProvinceIndex inComponent:0 animated:YES];
    [self.locatePicker reloadComponent:1];
    [self.locatePicker selectRow:defaultCityIndex inComponent:1 animated:YES];
    
}

@end
