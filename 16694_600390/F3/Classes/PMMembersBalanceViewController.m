//
//  PMMembersBalanceViewController.m
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersBalanceViewController.h"
#import "ZSTUtils.h"
#import "PMMembersBalanceCell.h"
#import "YBDatePickerView.h"

@interface PMMembersBalanceViewController () <YBDatePickerViewDelegate>

@property (strong, nonatomic) UITableView *balanceTableView;

@property (strong, nonatomic) UIView *pickView;
@property (strong, nonatomic) UIView *shadowView;
@property (assign, nonatomic) NSInteger iYear;
@property (assign, nonatomic) NSInteger iMonth;
@property (strong, nonatomic) UILabel *yearLab;
@property (strong, nonatomic) UILabel *monthLab;
@property (strong, nonatomic) UILabel *rechargeMoneyLab;
@property (strong, nonatomic) UILabel *consumptionMoneyLab;
@property (strong, nonatomic) UILabel *allMoneyLab;

// 充值记录数据数组
@property (strong, nonatomic) NSMutableArray *balanceArray;


@end

@implementation PMMembersBalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"我的余额"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    self.balanceArray = [NSMutableArray array];
    
    
    [self getMemberCardBalanceInfo];
    [self requestMemberRechargeRecordsData];
    
    // 初始化我的余额 tableview
    self.balanceTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.balanceTableView.delegate = self;
    self.balanceTableView.dataSource = self;
//    self.balanceTableView.layer.borderWidth = 1;// tableView外框线宽度
//    self.balanceTableView.layer.borderColor = [[UIColor grayColor] CGColor];// tableView外框线颜色
    [self.balanceTableView registerClass:[PMMembersBalanceCell  class] forCellReuseIdentifier:@"balanceCell"];
    self.balanceTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.balanceTableView];
    
    [self createBalanceUI];
    [self createYMSelected];
    //[self initData];//获取假数据
}

- (void)createBalanceUI
{
    //通过月份查看收支明细
    UIView *headView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 60)];
    headView.backgroundColor = RGBA(243, 243, 243, 1);
    self.balanceTableView.tableHeaderView = headView;
    
    
    //获取当前时间
    NSDate *nowDate = [NSDate new];
    NSCalendar *calendar =  [NSCalendar currentCalendar];
    NSUInteger unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit;
    NSDateComponents *dateComponent = [calendar components:unitFlags fromDate:nowDate];
    NSInteger year = [dateComponent year];
    NSInteger month = [dateComponent month];
    self.iYear = year;
    self.iMonth = month;
    
    //年Label
    UILabel *yearLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 5, WIDTH / 6, 15)];
    yearLB.text = [NSString stringWithFormat:@"%ld年",(long)year];
    yearLB.font = [UIFont systemFontOfSize:14.0f];
    yearLB.textColor = [UIColor grayColor];
    self.yearLab = yearLB;
    [headView addSubview:yearLB];
    
    UILabel *monthLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), 20, WidthRate(80), 35)];
    NSString *monthString = @"";
    if (month < 10)
    {
        monthString = [NSString stringWithFormat:@"0%ld",(long)month];
    }
    else
    {
        monthString = [NSString stringWithFormat:@"%ld",(long)month];
    }
    monthLB.text = monthString;
    monthLB.font = [UIFont systemFontOfSize:23.0f];
    monthLB.textColor = [UIColor grayColor];
    self.monthLab = monthLB;
    [headView addSubview:monthLB];
    
    UILabel *monthTitleLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(170), 25, WidthRate(30), 35)];
    monthTitleLB.text = @"月";
    monthTitleLB.font = [UIFont systemFontOfSize:13.0f];
    monthTitleLB.textColor = [UIColor grayColor];
    [headView addSubview:monthTitleLB];
    
    UIImageView *arrowIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(140), 38, 10, 10)];
    arrowIV.image = [UIImage imageNamed:@"module_recharge_drop-down"];
    arrowIV.contentMode = UIViewContentModeScaleAspectFit;
    [headView addSubview:arrowIV];
    
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(WIDTH / 3 - WidthRate(80), 10, 1, 40);
    line.backgroundColor = [UIColor whiteColor].CGColor;
    [headView.layer addSublayer:line];
    
    //选择月份
    UIButton *monthButton = [UIButton buttonWithType:UIButtonTypeCustom];
    monthButton.frame = CGRectMake(0, 0, WIDTH / 3 - WidthRate(80), headView.bounds.size.height);
    monthButton.tag = 1;
    [monthButton addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    [headView addSubview:monthButton];
    
    UILabel *rechargeLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(75), 5, WIDTH / 4, 15)];
    rechargeLB.text = @"总充值";
    rechargeLB.font = [UIFont systemFontOfSize:14.0f];
    rechargeLB.textColor = [UIColor grayColor];
    rechargeLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:rechargeLB];
    
    UILabel *rechargeMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH / 3 - WidthRate(75), 20, WIDTH / 4, 40)];
    rechargeMoneyLB.text = @"0.00";
    rechargeMoneyLB.font = [UIFont systemFontOfSize:15.0f];
    rechargeMoneyLB.textColor = [UIColor grayColor];
    rechargeMoneyLB.textAlignment = NSTextAlignmentCenter;
    self.rechargeMoneyLab= rechargeMoneyLB;
    [headView addSubview:rechargeMoneyLB];
    
    UILabel *consumptionLB = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(5) + rechargeLB.frame.origin.x + rechargeLB.bounds.size.width, 5, WIDTH / 4, 15)];
    consumptionLB.text = @"总消费";
    consumptionLB.font = [UIFont systemFontOfSize:14.0f];
    consumptionLB.textColor = [UIColor grayColor];
    consumptionLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:consumptionLB];
    
    UILabel *consumptionMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(rechargeLB.frame.origin.x + rechargeLB.bounds.size.width + WidthRate(5), 20, WIDTH / 4, 40)];
    consumptionMoneyLB.text = @"0.00";
    consumptionMoneyLB.font = [UIFont systemFontOfSize:15.0f];
    consumptionMoneyLB.textColor = [UIColor grayColor];
    consumptionMoneyLB.textAlignment = NSTextAlignmentCenter;
    self.consumptionMoneyLab = consumptionMoneyLB;
    [headView addSubview:consumptionMoneyLB];
    
    UILabel *allLB = [[UILabel alloc] initWithFrame:CGRectMake(consumptionLB.frame.origin.x + consumptionLB.bounds.size.width + WidthRate(5), 5, WIDTH / 4, 15)];
    allLB.text = @"总余额";
    allLB.font = [UIFont systemFontOfSize:14.0f];
    allLB.textColor = [UIColor grayColor];
    allLB.textAlignment = NSTextAlignmentCenter;
    [headView addSubview:allLB];
    
    UILabel *allMoneyLB = [[UILabel alloc] initWithFrame:CGRectMake(consumptionLB.frame.origin.x + consumptionLB.bounds.size.width + WidthRate(5), 20, WIDTH / 4, 40)];
    allMoneyLB.text = @"0.00";
    allMoneyLB.font = [UIFont systemFontOfSize:15.0f];
    allMoneyLB.textColor = [UIColor grayColor];
    allMoneyLB.textAlignment = NSTextAlignmentCenter;
    self.allMoneyLab = allMoneyLB;
    [headView addSubview:allMoneyLB];
    
    
    UIView *shadowView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 300)];
    shadowView.backgroundColor = [UIColor lightGrayColor];
    shadowView.alpha = 0.5;
    shadowView.hidden = YES;
    self.shadowView = shadowView;
    [self.view addSubview:shadowView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.balanceArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 70;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return HeightRate(100);
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *typeView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HeightRate(100))];
    typeView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.frame = CGRectMake(0, HeightRate(100) - 1, WIDTH, 1);
    line.backgroundColor = RGBA(236, 236, 236, 1).CGColor;
    [typeView.layer addSublayer:line];
    
    // 创建时间
    UILabel *timeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), HeightRate(25), WidthRate(230), HeightRate(50))];
    timeLabel.backgroundColor = [UIColor clearColor];
    timeLabel.text = @"时间";
    timeLabel.textColor = RGBA(65, 65, 65, 1);
    timeLabel.textAlignment = NSTextAlignmentCenter;
    timeLabel.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:timeLabel];
    
    // 金额
    UILabel *moneyLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(490), HeightRate(25), WidthRate(230), HeightRate(50))];
    moneyLabel.backgroundColor = [UIColor clearColor];
    moneyLabel.text = @"金额(元)";
    moneyLabel.textColor = RGBA(65, 65, 65, 1);
    moneyLabel.textAlignment = NSTextAlignmentCenter;
    moneyLabel.font = [UIFont systemFontOfSize:13.0f];
    [typeView addSubview:moneyLabel];
    
    return typeView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PMMembersBalanceCell *cell = [tableView dequeueReusableCellWithIdentifier:@"balanceCell"];
    
    [cell createBalanceUI];
    
    [cell setBalanceDataDict:self.balanceArray[indexPath.row]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - 年月选择器
- (void)createYMSelected
{
    YBDatePickerView *datePicker = [[YBDatePickerView alloc] init];
    datePicker.myDelegate = self;
    datePicker.nowYear = [NSString stringWithFormat:@"%ld年", (long)self.iYear];
    datePicker.nowMonth = [NSString stringWithFormat:@"%ld", (long)self.iMonth];
    [datePicker createUIWithFrame:CGRectMake(0, HEIGHT, WIDTH, 300)];
    self.pickView = datePicker;
    [self.view addSubview:datePicker];
}


#pragma mark - 点击事件
- (void)clickBtn:(UIButton *)btn
{
    self.shadowView.hidden = NO;
    [UIView animateWithDuration:0.3f animations:^{
        
        self.pickView.frame = CGRectMake(0, HEIGHT - 300, WIDTH, 300);
    }];
}

- (void)cancelPickerView
{
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

- (void)surePickerView:(NSString *)selectedYear And:(NSString *)selectedMonth
{
    self.yearLab.text = selectedYear;
    self.monthLab.text = selectedMonth;
    
    [self getRedData:selectedYear AndMon:selectedMonth];
    
    self.shadowView.hidden = YES;
    
    [UIView animateWithDuration:0.3f animations:^{
        self.pickView.frame = CGRectMake(0, HEIGHT, WIDTH, 300);
    }];
}

#pragma mark - 请求账户详情数据
- (void)getMemberCardBalanceInfo
{
    // 请求总充值、总消费、总余额参数
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *memberId = @"39C772A736BE4BC48A8586968EA7D789";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"memberId":memberId};
    
    [self.engine getMemberCardBalanceInfo:param];
}

// 获取会员余额信息成功
- (void)getMemberCardBalanceInfoSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    NSLog(@"HHHHHHHHHHH === %@", response);
    
    NSDictionary *dict = [response safeObjectForKey:@"data"];
    
    // 获取数据
    self.rechargeMoneyLab.text = [NSString stringWithFormat:@"%@", [dict safeObjectForKey:@"totalRecharge"]];
    self.consumptionMoneyLab.text = [NSString stringWithFormat:@"%@", [dict safeObjectForKey:@"totalConsume"]];
    self.allMoneyLab.text = [NSString stringWithFormat:@"%@", [dict safeObjectForKey:@"balance"]];
}

- (void)getMemberCardBalanceInfoFaild:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}


#pragma mark - 请求会员充值记录详细情况
- (void)getRedData:(NSString *)year AndMon:(NSString *)month
{
    // 请求会员充值记录参数
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *memberId = @"39C772A736BE4BC48A8586968EA7D789";
    NSString *curPage = @"1";
    NSString *pageSize = @"10";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"memberId":memberId,@"curPage":curPage,@"pageSize":pageSize,@"year":year,@"month":month};
    
    [self.engine requestMemberRechargeRecords:param];
    
    
//    [self.balanceArray removeAllObjects];
//    if ([year isEqualToString:@"2015年"] && [month isEqualToString:@"08"]) {
//        
//        [self initData2];
//    }
//    else if ([year isEqualToString:@"2015年"] && [month isEqualToString:@"09"]) {
//        [self initData];
//    }
//    else {
//        self.balanceArray = nil;
//        
//        // 空数据提示
//        UILabel *tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), (HEIGHT - HeightRate(80)) / 2, WidthRate(710), HeightRate(80))];
//        tipsLabel.backgroundColor = [UIColor clearColor];
//        tipsLabel.text = @"亲，您本月没有明细数据";
//        tipsLabel.textColor = RGBA(65, 65, 65, 1);
//        tipsLabel.textAlignment = NSTextAlignmentCenter;
//        tipsLabel.font = [UIFont boldSystemFontOfSize:20.0f];
//        [self.view addSubview:tipsLabel];
//    }
//    [self.balanceTableView reloadData];
}


#pragma mark - 请求会员充值记录数据
- (void)requestMemberRechargeRecordsData
{
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *memberId = @"39C772A736BE4BC48A8586968EA7D789";
    NSString *curPage = @"1";
    NSString *pageSize = @"10";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"memberId":memberId,@"curPage":curPage,@"pageSize":pageSize};
    
    [self.engine requestMemberRechargeRecords:param];
}

// 会员充值记录请求成功
- (void)requestMemberRechargeRecordsSucceed:(NSDictionary *)response
{
    self.balanceArray = [[response safeObjectForKey:@"data"] safeObjectForKey:@"items"];
    
    [self.balanceTableView reloadData];
}

- (void)requestMemberRechargeRecordsFaild:(NSString *)response
{
    
}


// 初始化假数据
- (void)initData2
{
    NSDictionary *dic4 = @{@"time":@"08-12 16:37",@"order":@"2015081216370524",@"money":@"98.00",@"tradeType":@"1"};
    NSDictionary *dic3 = @{@"time":@"08-13 08:54",@"order":@"2015081308542206",@"money":@"627.35",@"tradeType":@"0"};
    NSDictionary *dic2 = @{@"time":@"08-18 17:20",@"order":@"2015081817203301",@"money":@"510.15",@"tradeType":@"0"};
    NSDictionary *dic1 = @{@"time":@"08-22 16:37",@"order":@"2015082216370524",@"money":@"348.50",@"tradeType":@"1"};
    
    [self.balanceArray addObject:dic1];
    [self.balanceArray addObject:dic2];
    [self.balanceArray addObject:dic3];
    [self.balanceArray addObject:dic4];
}

// 初始化假数据
- (void)initData
{
    NSDictionary *dic7 = @{@"time":@"01-23 14:23",@"order":@"2015012314231301",@"money":@"510.70",@"tradeType":@"1"};
    NSDictionary *dic6 = @{@"time":@"06-03 08:54",@"order":@"2015060308542206",@"money":@"640.04",@"tradeType":@"1"};
    NSDictionary *dic5 = @{@"time":@"07-08 17:20",@"order":@"2015070817203301",@"money":@"520.13",@"tradeType":@"0"};
    NSDictionary *dic4 = @{@"time":@"08-12 16:37",@"order":@"2015081216370524",@"money":@"98.00",@"tradeType":@"1"};
    NSDictionary *dic3 = @{@"time":@"09-03 08:54",@"order":@"2015090308542206",@"money":@"627.35",@"tradeType":@"0"};
    NSDictionary *dic2 = @{@"time":@"09-08 17:20",@"order":@"2015090817203301",@"money":@"510.15",@"tradeType":@"0"};
    NSDictionary *dic1 = @{@"time":@"09-12 16:37",@"order":@"2015091216370524",@"money":@"348.50",@"tradeType":@"1"};
    
    [self.balanceArray addObject:dic1];
    [self.balanceArray addObject:dic2];
    [self.balanceArray addObject:dic3];
    [self.balanceArray addObject:dic4];
    [self.balanceArray addObject:dic5];
    [self.balanceArray addObject:dic6];
    [self.balanceArray addObject:dic7];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
