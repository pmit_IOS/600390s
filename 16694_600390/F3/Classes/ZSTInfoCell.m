//
//  ZSTInfoCell.m
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTInfoCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@implementation ZSTInfoCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.titleLB)
    {
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, (WIDTH - 30) / 2, 25)];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textColor = RGBA(43, 43, 43, 1);
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLB];
        
        self.arrowView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"cellRight.png"]];
        self.arrowView.frame = CGRectMake(WIDTH - 15 - 10, 17.5, 10, 10);
        self.arrowView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.arrowView];
        
        self.rightLB = [[UILabel alloc] initWithFrame:CGRectMake((WIDTH - 30) / 2 + 15, 10, (WIDTH - 30) / 2 - 15 - 10, 25)];
        self.rightLB.textColor = RGBA(136, 136, 136, 1);
        self.rightLB.textAlignment = NSTextAlignmentRight;
        self.rightLB.font = [UIFont systemFontOfSize:12.0f];
        [self.contentView addSubview:self.rightLB];
        
        self.rightIV = [[UIImageView alloc] initWithFrame:CGRectMake(WIDTH - 15 - 10 - 15 - 35, 5, 35, 35)];
        self.rightIV.layer.cornerRadius = self.rightIV.frame.size.width / 2;
        self.rightIV.layer.masksToBounds = YES;
        //self.rightIV.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:self.rightIV];
    }
    
    if (self.isHeaderCell)
    {
        self.rightIV.hidden = NO;
        self.rightLB.hidden = YES;
    }
    else
    {
        self.rightIV.hidden = YES;
        self.rightLB.hidden = NO;
    }
}

- (void)setCellData:(NSString *)title AndContent:(id)contentString;
{
    if ([title isEqualToString:@"电话"])
    {
        self.rightLB.frame = CGRectMake((WIDTH - 30) / 2 + 15, 10, (WIDTH - 30) / 2, 25);
        self.arrowView.hidden = YES;
    }
    else
    {
        self.rightLB.frame = CGRectMake((WIDTH - 30) / 2 + 15, 10, (WIDTH - 30) / 2 - 15 - 10, 25);
        self.arrowView.hidden = NO;
    }
    
    self.titleLB.text = title;
    if (self.isHeaderCell)
    {
        [self.rightIV setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@",contentString]] placeholderImage:[UIImage imageNamed:@"module_personal_my_avater_defaut.png"]];
    }
    else
    {
        if (contentString && [contentString isKindOfClass:[NSString class]] && ![contentString isEqualToString:@""])
        {
            self.rightLB.text = [NSString stringWithFormat:@"%@",contentString];
        }
        else
        {
            self.rightLB.text = @"未设置";
        }
        
    }
}

@end
