//
//  ZSTBirthPickerView.m
//  F3
//
//  Created by pmit on 15/8/27.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTBirthPickerView.h"

@implementation ZSTBirthPickerView

- (void)createPickerView
{
    self.backgroundColor = [UIColor whiteColor];
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(pickerViewDismiss:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipe];
    
    UIView *optionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    optionView.backgroundColor = [UIColor whiteColor];
    
    CALayer *line = [CALayer layer];
    line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
    line.frame = CGRectMake(0, 40, WIDTH, 0.5);
    [optionView.layer addSublayer:line];
    
    NSString *tipString = @"请选择...";
    //    CGSize tipStringSize = [tipString sizeWithFont:[UIFont systemFontOfSize:12.0f] constrainedToSize:CGSizeMake(MAXFLOAT, 20)];
    CGSize tipStringSize = [tipString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    UILabel *tipLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, tipStringSize.width, 20)];
    tipLB.font = [UIFont systemFontOfSize:12.0f];
    tipLB.textColor = RGBA(136, 136, 136, 1);
    tipLB.textAlignment = NSTextAlignmentLeft;
    tipLB.text = tipString;
    [optionView addSubview:tipLB];
    
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.frame = CGRectMake(WIDTH - 10 - 40, 10, 40, 20);
    sureBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [sureBtn setTitle:@"完成" forState:UIControlStateNormal];
    [sureBtn setTitleColor:RGBA(244, 125, 55, 1) forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureBirthPicker:) forControlEvents:UIControlEventTouchUpInside];
    [optionView addSubview:sureBtn];
    [self addSubview:optionView];
    
    self.dateTimePicker = [[UIPickerView alloc] initWithFrame:CGRectZero];
    self.dateTimePicker.autoresizingMask = UIViewAutoresizingFlexibleHeight|UIViewAutoresizingFlexibleWidth;
    self.dateTimePicker.frame = CGRectMake(0, 30, WIDTH, 162);
    self.dateTimePicker.delegate = self;
    self.dateTimePicker.dataSource = self;
    [self addSubview:self.dateTimePicker];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
    {
        return self.yearArr.count;
    }
    else if (component == 1)
    {
        return 12;
    }
    else
    {
//        return 31;
        return [self getLastDay:self.selectedMonth];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *myLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 30)];
    myLB.font = [UIFont systemFontOfSize:15.0f];
    myLB.textAlignment = NSTextAlignmentCenter;
    
    if (component == 0)
    {
        myLB.text = [NSString stringWithFormat:@"%@",self.yearArr[row]];
    }
    else if (component == 1)
    {
        myLB.text = row + 1 >= 10 ? [NSString stringWithFormat:@"%@",@(row + 1)] : [NSString stringWithFormat:@"0%@",@(row + 1)];
    }
    else if (component == 2)
    {
        myLB.text = row + 1 >= 10 ? [NSString stringWithFormat:@"%@",@(row + 1)] : [NSString stringWithFormat:@"0%@",@(row + 1)];
    }
    
    return myLB;
}

//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    if (component == 0)
//    {
//        return [NSString stringWithFormat:@"%@",self.yearArr[row]];
//    }
//    else if (component == 1)
//    {
//        return [NSString stringWithFormat:@"%@",@(row + 1)];
//    }
//    else
//    {
//        return [NSString stringWithFormat:@"%@",@(row + 1)];
//    }
//}

- (void)showDefaultShow
{
    if (self.nowYear == 0 || self.nowMonth == 0 || self.nowDay == 0)
    {
        self.nowYear = [self.yearArr[0] integerValue];
        self.nowMonth = 1;
        self.nowDay = 1;
    }

    NSInteger yearIndex = -1;
    for (NSInteger i = 0; i < self.yearArr.count; i++)
    {
        NSInteger oneYear = [self.yearArr[i] integerValue];
        if (oneYear == self.nowYear)
        {
            yearIndex = i;
            break;
        }
    }
        
    NSInteger monthIndex = -1;
    for (NSInteger i = 0; i < 12; i++)
    {
        NSInteger oneMonth = i + 1;
        if (oneMonth == self.nowMonth)
        {
            monthIndex = i;
            break;
        }
    }
        
    NSInteger dayIndex = -1;
    NSInteger lastDay = [self getLastDay:self.nowMonth];
    for (NSInteger i = 0; i < lastDay; i++)
    {
        NSInteger oneDay = i + 1;
        if (oneDay == self.nowDay)
        {
            dayIndex = i;
            break;
        }
    }
        
    [self.dateTimePicker selectRow:yearIndex inComponent:0 animated:YES];
    [self.dateTimePicker selectRow:monthIndex inComponent:1 animated:YES];
    [self.dateTimePicker selectRow:dayIndex inComponent:2 animated:YES];
    
}

- (NSInteger)getLastDay:(NSInteger)oneDay
{
    NSInteger lastDay = 0;
    if (oneDay == 1 || oneDay == 3 || oneDay == 5 || oneDay == 7 || oneDay == 8 || oneDay == 10 || oneDay == 12)
    {
        lastDay = 31;
    }
    else if (oneDay != 2)
    {
        lastDay = 30;
    }
    else
    {
        if ((oneDay % 100 == 0 && oneDay % 400 == 0 ) || (oneDay % 100 != 0 && oneDay % 4 == 0))
        {
            lastDay = 29;
        }
        else
        {
            lastDay = 28;
        }
    }
    
    return lastDay;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (component == 0)
    {
        self.selectedYear = [self.yearArr[row] integerValue];
        [pickerView selectRow:0 inComponent:1 animated:YES];
        [pickerView selectRow:0 inComponent:2 animated:YES];
        [pickerView reloadAllComponents];
    }
    else if (component == 1)
    {
        self.selectedMonth = row + 1;
        [pickerView selectRow:0 inComponent:2 animated:YES];
        [pickerView reloadAllComponents];
    }
    else
    {
        self.selectedDay = row + 1;
    }
}

- (void)sureBirthPicker:(UIButton *)sender
{
    if ([self.birthDelegate respondsToSelector:@selector(sureBirth:)])
    {
        NSString *dateString = [NSString stringWithFormat:@"%@-%@",@(self.selectedMonth),@(self.selectedDay)];
        [self.birthDelegate sureBirth:dateString];
    }
}

- (void)pickerViewDismiss:(UISwipeGestureRecognizer *)swipe
{
    if ([self.birthDelegate respondsToSelector:@selector(cancelBirthPicker)])
    {
        [self.birthDelegate cancelBirthPicker];
    }
}


@end
