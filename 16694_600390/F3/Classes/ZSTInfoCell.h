//
//  ZSTInfoCell.h
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTInfoCell : UITableViewCell

@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) UILabel *rightLB;
@property (strong,nonatomic) UIImageView *rightIV;
@property (strong,nonatomic) UIImageView *arrowView;
@property (assign,nonatomic) BOOL isHeaderCell;

- (void)createUI;
//- (void)setCellData:(NSDictionary *)cellData;
- (void)setCellData:(NSString *)title AndContent:(NSString *)contentString;

@end
