//
//  LongPressButton.m
//  F3_UI
//
//  Created by luobin on 8/1/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "ZSTLongPressButton.h"

@implementation ZSTLongPressButton

-(void) onLongPress {
    [self sendActionsForControlEvents:UIControlEventLongPress];//为给定的控制事件发送指定的动作消息
}

-(void) longPressBegin {
    //如果大于1秒 执行onTimer 方法 否则 不执行
    _timer = [[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(onLongPress) userInfo:nil repeats:NO] retain];
//    [_timer invalidate];
}

-(void) longPressEnd {   //touch Up Inside 
    [_timer invalidate];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
        [self addTarget:self action:@selector(longPressBegin) forControlEvents:UIControlEventTouchDown];
        [self addTarget:self action:@selector(longPressEnd) forControlEvents:UIControlEventTouchUpInside];
        [self addTarget:self action:@selector(longPressEnd) forControlEvents:UIControlEventTouchUpOutside];
        //[self addTarget:self action:@selector(longPressEnd) forControlEvents:UIControlEventTouchDragInside];
    }
    return self;
}

- (void)dealloc
{
    [_timer invalidate];
    [_timer release];
    [super dealloc];
}

@end
