//
//  Suggested_questions.m
//  Net_Information
//
//  Created by huqinghe on 11-6-8.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

#import "ZSTSuggestedQuestionsViewController.h"
#import "TKUIUtil.h"
#import "ZSTUtils.h"
#import "ZSTLogUtil.h"

@implementation ZSTSuggestedQuestionsViewController

- (void)dealloc
{
    [_engine release]; _engine = nil;
    [super dealloc];
    
}

- (void)requestDidFail:(ZSTLegacyResponse *)response
{
    [TKUIUtil alertInWindow:NSLocalizedString(@"提交失败", nil) withImage:[UIImage imageNamed:@"icon_warning.png"]  withCenter:CGPointMake(160, 100)];
}

- (void)submitAdviceResponse
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void) click
{
    if (textview.text.length == 0) {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"请输入建议内容", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(160, 100)];
        [TKUIUtil hiddenHUDAfterDelay:2];
        return;
    }
    
	_engine = [[ZSTF3Engine alloc] init];
    _engine.delegate = self;
	NSString *suggested = textview.text;
	[_engine submitAdvice:suggested];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", nil) target:self selector:@selector (popViewController)];
	self.navigationItem.titleView = [ZSTUtils logoView];
    UIImageView *backgroundImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg.png"]];
	backgroundImage.frame = CGRectMake(0, 0, 320, 480+(iPhone5?88:0));
	[self.view addSubview:backgroundImage];
	[backgroundImage release];
    
	textview = [[TKPlaceHolderTextView alloc] initWithFrame:CGRectMake(4, 4, 312, 152)];
    textview.font = [UIFont systemFontOfSize:18];
	[self.view addSubview:textview];
    [textview becomeFirstResponder];
	[textview release];
	
    button = [ZSTUtils customButtonTitle:NSLocalizedString(@"提 交" , @"")
                          nImage:[UIImage imageNamed:@"btn_commonbg_n.png"] 
                          pImage:[UIImage imageNamed:@"btn_commonbg_p.png"] 
                          titleFontSize:13 
                          conerRsdius:5.0];
    button.frame = CGRectMake(130, 163, 60, 30);
	[button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];

	[self.view addSubview:button];
    
    NSString *minimumSystemVersion = @"5.0";
    NSString *systemVersion = [[UIDevice currentDevice] systemVersion];
    if ([systemVersion compare:minimumSystemVersion options:NSNumericSearch] != NSOrderedAscending)
    {
        CGRect textViewFrame = textview.frame;
        textViewFrame.size = CGSizeMake(312, 152-36);
        textview.frame = textViewFrame;
        
        button.frame = CGRectMake(130, 163-36, 60, 30);
    }
    
    [ZSTLogUtil logUserAction:NSStringFromClass([ZSTSuggestedQuestionsViewController class])];
	
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [TKUIUtil hiddenHUD];
}


@end
