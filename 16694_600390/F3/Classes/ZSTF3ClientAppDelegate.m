//
//  F3ClientAppDelegate.m
//
//
//  Created by luobin on 2012-04-28.
//  Copyright 2012 ZhangShangTong Stock Co. All rights reserved.
//

#import "ZSTF3ClientAppDelegate.h"

#import <AVFoundation/AVFoundation.h>

#import "ZSTSqlManager.h"
#import "ZSTUtils.h"
#import "TKUIUtil.h"
#import "ZSTShell.h"
#import <EncryptUtil.h>
#import "ZSTLogUtil.h"
#import "NSStringAdditions.h"
#import "ZSTCoverAView.h"
#import "BaseNavgationController.h"
#import "CatchObject.h"
#import <NSObject+SBJSON.h>
#import <objc/runtime.h>
#import "UIFont+ZSTMyFont.h"
#import <TencentOpenAPI/QQApiInterfaceObject.h>
#import <ZSTECBProductTableViewController.h>

#define  kZSTSettingItemType_Help  @"Help"

@implementation ZSTF3ClientAppDelegate

@synthesize window;
@synthesize rootController;
@synthesize coverView;
@synthesize f3Engine;
@synthesize sinaWeiboEngine;
@synthesize tWeiboEngine;
@synthesize QQEngine;



#pragma mark -
#pragma mark Application lifecycle

- (void) initWithShell
{
    NSDictionary *shellOptions= [ZSTShell initWithShell];
    
    if (self.rootController == nil) {
        [[UIApplication sharedApplication] setStatusBarHidden:NO];
        self.rootController = [shellOptions objectForKey:ShellRootViewController];
        self.window.rootViewController = self.rootController;
    }
    
    //封面信息
//    self.coverView = (ZSTCoverAView *)[shellOptions objectForKey:CoverView];
//    if (self.coverView && ![self.coverView isKindOfClass:[NSNull class]]) {
//        self.coverView.frame = self.window.frame;
//        self.coverView.delegate = self;
//    }
}



#pragma mark - ZSTCoverAViewDelegate

- (void)coverAViewDidClicked:(ZSTCoverAView *)coverAView
{
    
}

- (void)coverAViewDidSlided:(ZSTCoverAView *)coverAView;{
    
}

- (void)coverAViewDidDismiss:(ZSTCoverAView *)coverAView
{
    
}

- (void)showCover
{
    if (self.coverView) {
        [self.coverView coverAShow];
    }
}


#pragma mark - ZSTHelpViewControllerDelegate

- (void)helpViewControllerDidDismiss:(ZSTHelpViewController *)helpViewController
{
    if (isShowHelp) {
        isShowHelp = NO;
        [self showCover];
    }
}

- (void)showHelp
{
    isShowHelp = YES;
    NSMutableArray *images = [NSMutableArray array];
    for (int i = 0; i < 3; i++) {
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"framework_help%d", i + 1] ofType:@"jpg"];
        [images addObject:imagePath];
    }
    
    [ZSTHelpViewController shared].delegate = self;
    [[ZSTHelpViewController shared] showWithImages:images];
    [ZSTLogUtil logUserAction:@"HelpViewController"];
}

#pragma mark---

-(void)registerByManualAnimated:(BOOL)animated
{
//    ZSTRegisterViewController *registerView = [[ZSTRegisterViewController alloc] init];
//    registerView.delegate = self;
//    registerView.isFromSetting = NO;
    
//    ZSTLoginViewController *controller = [[ZSTLoginViewController alloc] initWithNibName:@"ZSTLoginViewController" bundle:nil];
//    controller.isFromSetting = NO;
//    controller.delegate = self;
    ZSTLoginController *controller = [[ZSTLoginController alloc] init];
    controller.isFromSetting = NO;
    controller.delegate = self;
    BaseNavgationController *navController = [[BaseNavgationController alloc] initWithRootViewController:controller];
    
    [self.rootController presentViewController:navController animated:YES completion:^(void) {
    }];
    
//    if ([ZSTF3Preferences shared].MCRegistType != MCRegistType_ForceRegister) {
//        controller.navigationItem.leftBarButtonItem.title = NSLocalizedString(@"取消",@"取消");
//    }
    [navController release];
    [controller release];
}

-(void) initErrorViewController
{
    
}

- (BOOL) checkNetwork
{
    BOOL isOnline = TRUE;
    if (![UIDevice networkAvailable]) {
        [TKUIUtil showHUDInView:self.window
                       withText:NSLocalizedString(@"网络异常，请检查网络", nil)
                      withImage:[UIImage imageNamed:@"icon_warning.png"]];
        [TKUIUtil hiddenHUDAfterDelay:2];
        isOnline = FALSE;
    }
    return isOnline;
}

- (void)loginDidCancel
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    //首次绑定成功，等帮助结束以后在检查更新
    if (preferences.isFirstLogin) {
        
        if (preferences.isShowHelp) {
            [self showHelp];
        }else{
            [self showCover];
        }
        preferences.isFirstLogin = NO;
    } else {
        if (![preferences.ParamValue isEqualToString:@"0"] || !preferences.ParamValue || [preferences.ParamValue isEqualToString:@""]) {
            [self showCover];
            //检查更新
            [self.f3Engine checkClientVersion];
        }
    }
    
    [preferences synchronize];
    
    [rootController dismissViewControllerAnimated:YES completion:nil];
    [self startAllService];
}

- (void)loginDidCancel:(NSNotification*)notify
{
    int shellID = [[notify object] intValue];
    
    if (shellID == 20) {
    
        [((UITabBarController *)self.window.rootViewController) setSelectedIndex:2];
    } else if (shellID == 43) {
        
        [((UITabBarController *)self.window.rootViewController) setSelectedIndex:1];
    } else if (shellID != 23 && shellID != 40){
        
        if ([[((UITabBarController *)self.window.rootViewController) viewControllers] count] > 0) {
            
            [((UITabBarController *)self.window.rootViewController) setSelectedIndex:0];
        }
    }
}

- (void) loginDidFinish
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
//    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    //首次绑定成功，等帮助结束以后在检查更新
    if (preferences.isFirstLogin) {
        
        if (preferences.isShowHelp) {
            [self showHelp];
        }
        preferences.isFirstLogin = NO;
    } else {
        if (![preferences.ParamValue isEqualToString:@"0"] || !preferences.ParamValue || [preferences.ParamValue isEqualToString:@""]) {
            [self showCover];
            //检查更新
            [self.f3Engine checkClientVersion];
        }
    }
    
    [preferences synchronize];
    [self startAllService];
}


- (void)registerDidFinish
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    //首次绑定成功，等帮助结束以后在检查更新
    if (preferences.isFirstLogin) {
        
        if (preferences.isShowHelp && [settingItems containsObject:kZSTSettingItemType_Help]) {
            [self showHelp];
        }
        preferences.isFirstLogin = NO;
    } else {
        if (![preferences.ParamValue isEqualToString:@"0"] || !preferences.ParamValue || [preferences.ParamValue isEqualToString:@""]) {
            [self showCover];
            //检查更新
            [self.f3Engine checkClientVersion];
        }
    }
    
    [preferences synchronize];
    [self startAllService];
}

-(void)registerDidCancel
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    //首次绑定成功，等帮助结束以后在检查更新
    if (preferences.isFirstLogin) {
        
        if (preferences.isShowHelp && [settingItems containsObject:kZSTSettingItemType_Help]) {
            [self showHelp];
        }else{
            [self showCover];
        }
        preferences.isFirstLogin = NO;
    } else {
        if (![preferences.ParamValue isEqualToString:@"0"] || !preferences.ParamValue || [preferences.ParamValue isEqualToString:@""]) {
            [self showCover];
            //检查更新
            [self.f3Engine checkClientVersion];
        }
    }
    
    [preferences synchronize];
    
    [rootController dismissViewControllerAnimated:YES completion:nil];
    [self startAllService];
}

- (void)checkClientVersionResponse:(NSString *)versionURL updateNote:(NSString *)updateNote updateVersion:(int)updateVersion
{
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    
    if (versionURL != nil && [versionURL length] != 0 && preferences.UpdateVersion != updateVersion) {
        preferences.newVersion = updateVersion;
        _clientVersionUrl = [[NSString alloc] initWithFormat:@"%@",versionURL];
        
        if ([preferences.ParamValue isEqualToString:@"2"])
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"检查到有最新版本", nil)
                                                             message:[updateNote isEmptyOrWhitespace]? NSLocalizedString(@"现在就去更新吗？", nil):updateNote
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:NSLocalizedString(@"我要更新", nil),nil];
            alert.tag = 1025;
            [alert show];
        }
        else if ([preferences.ParamValue isEqualToString:@"1"])
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"检查到有最新版本", nil)
                                                             message:[updateNote isEmptyOrWhitespace]? NSLocalizedString(@"现在就去更新吗？", nil):updateNote
                                                            delegate:self
                                                   cancelButtonTitle:NSLocalizedString(@"稍候更新", nil)
                                                   otherButtonTitles:NSLocalizedString(@"我要更新", nil),NSLocalizedString(@"不再提醒", nil),nil];
            alert.tag = 1024;
            [alert show];
        }
    }
}

#pragma mark －－－－－－－－－－－－－－－－－UIAlertViewDelegate－－－－－－－－－－－－－－－－－－－－－－－－－－－

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1024) {
        if(buttonIndex == 1)
        {
            [[UIApplication sharedApplication] openURL: [NSURL URLWithString:_clientVersionUrl]];

        }else if(buttonIndex == 2)
        {
            ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
            preferences.UpdateVersion = preferences.newVersion;
        }
    }
    else if (alertView.tag == 1025)
    {
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:_clientVersionUrl]];
    }
}

- (void)applicationSetUp
{
    isShowHelp = NO;
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    preferences.isInPush = NO;
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    if (!TKIsPad()) {
//        IRSplashWindow *splashWindow = [[IRSplashWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
//        splashWindow.transitionType = IRSplashWindowTransitionTypeFade;
//        [splashWindow makeKeyAndVisible];
//        
//        //更新商户参数
//        if (![ZSTF3Engine updateECMobileClientParams] && !preferences.hasUpdateClientParams) {
//            [ZSTUtils showAlertTitle:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] message:NSLocalizedString(@"初始化客户端失败!", nil)];
//            [self initErrorViewController];
//            [splashWindow release];
//            return;
//        } else {
//            preferences.hasUpdateClientParams = YES;
//            [splashWindow retreatSplash];
//        }
//        
//        [splashWindow release];
    } else {
        //更新商户参数
        if (![ZSTF3Engine updateECMobileClientParams] && !preferences.hasUpdateClientParams) {
            [ZSTUtils showAlertTitle:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"] message:NSLocalizedString(@"初始化客户端失败!", nil)];
            [self initErrorViewController];
            return;
        } else {
            preferences.hasUpdateClientParams = YES;
        }
    }
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationNone];
    
    //如果loginMsisdn为空，绑定客户端
//    if ([preferences.loginMsisdn isEmptyOrWhitespace]) {
//        [ZSTF3Engine registMobileClientFor:nil checksum:nil];
//    }
    
    //绑定失败，提示用户
//    if ([preferences.loginMsisdn isEmptyOrWhitespace]) {
//        [ZSTUtils showAlertTitle:[[NSBundle mainBundle]
//                                  objectForInfoDictionaryKey:@"CFBundleDisplayName"]
//                         message:NSLocalizedString(@"网络连接失败!",nil)];
//        [self initErrorViewController];
//        return;
//    }
    
    [self initWithShell];
    [self.window makeKeyAndVisible];
    [WeiboSDK enableDebugMode:YES];
    [WeiboSDK registerApp:@"2971820215"];

    [self showCover];


    [self startAllService];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([[ud objectForKey:@"firstLaunch"] isEqualToString:@"1"])
    {
        
    }
    else
    {
        [self phoneGetFakeUserId];
    }
    
    [self getThirdLoginAuthorizeData];
}

- (void)startAllService
{
//    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
//    if ([[ud objectForKey:@"firstLaunch"] isEqualToString:@"1"])
//    {
//        
//    }
//    else
//    {
//        
//        [ud setObject:@"1" forKey:@"firstLaunch"];
//        [self.f3Engine thirdLoginWithDictionary:@{@"openId":@"firstLaunch"}];
//        [ud synchronize];
//    }
    
   
}

- (void)phoneGetFakeUserId
{
    NSMutableDictionary *data = [NSMutableDictionary dictionary];
    [data setObject:@"firstLaunch" forKey:@"OpenId"];
    [data setObject:@"firstLaunch" forKey:@"Avatar"];
    [data setObject:@"firstLaunch" forKey:@"NickName"];
    [data setObject:@"firstLaunch" forKey:@"PlatformType"];
    
    [self.f3Engine thirdLoginWithDictionary:data];
}

- (void) applicationCheckRegistType
{
     ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    NSString *GP_Setting_Items = NSLocalizedString(@"GP_Setting_Items", nil);
    GP_Setting_Items = [GP_Setting_Items stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSArray *settingItems = [GP_Setting_Items componentsSeparatedByString:@","];
    
    if (preferences.MCRegistType == MCRegistType_ForceRegister && ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) && status != 0) {
        //手动绑定次数清零，如果服务再次把绑定类型修改为提示绑定，重新提示三次
        preferences.regManualTime = 0;
        [preferences synchronize];
        
        //手动绑定
        [self registerByManualAnimated:NO];
        return;
        
        //如果绑定类型是提示绑定，并且loginMsisdn是虚拟手机号，则提示用户手动绑定3次
    } else if(preferences.MCRegistType == MCRegistType_PromptRegister && ([ZSTF3Preferences shared].UserId == nil || [[ZSTF3Preferences shared].UserId length] == 0) && status != 0) {
        
        //提示绑定三次
        //        if (preferences.regManualTime  < 3) {
        //            preferences.regManualTime += 1;
        //            [preferences synchronize];
        //提示绑定
        [self registerByManualAnimated:NO];
        return;
        //        }
        
        //如果绑定类型是虚拟绑定，而loginMsisdn不为空
    } else if(preferences.MCRegistType == MCRegistType_VirtualRegister){
        
        //手动绑定次数清零，如果服务再次把绑定类型修改为提示绑定，重新提示三次
        preferences.regManualTime = 0;
    }
    
    //首次绑定成功，等帮助结束以后在检查更新
    if (preferences.isFirstLogin) {
        
        if (preferences.isShowHelp && [settingItems containsObject:kZSTSettingItemType_Help]) {
            [self showHelp];
        }
        preferences.isFirstLogin = NO;
    } else {
        if (![preferences.ParamValue isEqualToString:@"0"] || !preferences.ParamValue || [preferences.ParamValue isEqualToString:@""]) {
            [self showCover];
            //检查更新
            [self.f3Engine checkClientVersion];
        }
    }
    
    [preferences synchronize];

}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////

- (void)remoteAction
{
    ZSTF3Preferences *per = [ZSTF3Preferences shared];
    
    if (per.pushOpentype == PushOpentype_TabBar) {
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:@(per.pushSelectIndex).stringValue,@"selectIndex", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_PushBMessage object:dic];
    }else if(per.pushOpentype == PushOpentype_ImageButton && !per.isInPush) {
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_PushViewController object:nil];
    }
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[[UIWindow alloc] initWithFrame: [UIScreen mainScreen].bounds] autorelease];
    UIViewController *vc = [[UIViewController alloc] initWithNibName:nil bundle:nil];
    self.window.rootViewController = vc;
    [self.window makeKeyAndVisible];
    
    AVAudioSession *audioSession = [AVAudioSession sharedInstance];
    
    [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
    
    //默认情况下扬声器播放
    [audioSession setCategory:AVAudioSessionCategoryPlayback error:nil];
    [audioSession setActive:YES error:nil];
    
    [ZSTLogUtil logSysInfo:@"application launch..."];

//
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >=7) {
//        [application setStatusBarStyle:UIStatusBarStyleLightContent];
//        self.window.clipsToBounds = YES;
//        self.window.frame = CGRectMake(0, 20, self.window.frame.size.width, self.window.frame.size.height-20);
//        self.window.bounds = CGRectMake(0, 20, self.window.frame.size.width, self.window.frame.size.height);
//    }
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginDidCancel:) name:kNotificationLoginCancel object:nil];
    
    //初始化数据库
    [ZSTSqlManager openDatabase];
    
    if ([ZSTShell isModuleAvailable:-1] || [ZSTShell isModuleAvailable:22]) {
        if (IS_IOS_8)
        {
            UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeAlert) categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
        else
        {
            [[UIApplication sharedApplication] registerForRemoteNotificationTypes:(UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound|UIRemoteNotificationTypeBadge)];
        }
    }
    if (self.f3Engine == nil) {
        ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
        engine.delegate  = self;
        self.f3Engine = engine;
        [engine release];
    }
    
    [self.f3Engine automaticLoginWithMsisdn:[ZSTF3Preferences shared].loginMsisdn userId:[ZSTF3Preferences shared].UserId];
    
    //初始化
    [self applicationSetUp];
    ZSTF3Preferences *preferences = [ZSTF3Preferences shared];
    if (preferences.WeiXin) {
        NSArray *infoArr = [preferences.WeiXin componentsSeparatedByString:@"|"];
        [WXApi registerApp:[infoArr objectAtIndex:0]];
    }
    if ([launchOptions objectForKey:@"UIApplicationLaunchOptionsRemoteNotificationKey"]) {
        [self remoteAction];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"LaunchRemoteNotificationKey"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_ForceHttpPoll object:nil];
    }
    
    [self.f3Engine updateECClientVisitInfoWithMsisdn:[ZSTF3Preferences shared].loginMsisdn];
    
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    //程序已经注册远程通知
    NSString *tokenStr = [NSString stringByTrimmingWhitespaceCharactersAndAngleBracket:[deviceToken description]];
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([[ud objectForKey:@"firstLaunch"] isEqualToString:@"1"])
    {
        
    }
    else
    {
        self.deviceTokenString = tokenStr;
    }
    
    NSLog(@"tokenStr -->%@",tokenStr);
    [ZSTF3Preferences shared].pushToken = tokenStr;
    if ([ZSTF3Engine syncPushNotificationParams]) {
        NSLog(@"syncPushNotificationParams success！");
    }else{
        NSLog(@"syncPushNotificationParams failed！");
    }
    
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    //程序没有注册远程通知
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"tipSound"]) {
        AudioServicesPlaySystemSound(1007);
    }
    
    NSString *moduleString = [userInfo safeObjectForKey:@"customitem"];
//    for (UIViewController *subVC in window.rootViewController.navigationController.childViewControllers)
//    {
//        if (subVC == [ZSTECBProductTableViewController class])
//        {
//            NSLog(@"有");
//            ZSTECBProductTableViewController *ebVC = (ZSTECBProductTableViewController *)subVC;
//            [window.rootViewController.navigationController pushViewController:ebVC animated:YES];
//        }
//        else
//        {
//            ZSTECBProductTableViewController *ebVC = [[ZSTECBProductTableViewController alloc] init];
//            [window.rootViewController.navigationController pushViewController:ebVC animated:YES];
//         }
//    }
    
    [((UITabBarController *)self.window.rootViewController) setSelectedIndex:4];
    
    if ([ZSTShell isModuleAvailable:-1] || [ZSTShell isModuleAvailable:22]) {

        if ( application.applicationState == UIApplicationStateBackground
            || application.applicationState == UIApplicationStateInactive){

            [self remoteAction];
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_ForceHttpPoll object:nil];
    }

}


-(void) onResp:(BaseResp*)resp //weixin
{
    if ([resp isKindOfClass:[QQBaseResp class]])
    {
        QQBaseResp *qqResp = (QQBaseResp *)resp;
        switch (qqResp.type)
        {
            case ESENDMESSAGETOQQRESPTYPE:
            {
                SendMessageToQQResp* sendResp = (SendMessageToQQResp*)resp;
                if (sendResp.result == 0)
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"QQShareSuccess" object:nil];
                }
                else
                {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"QQShareFail" object:nil];
                }
                
            }
                break;
                
            default:
                break;
        }
        
       
    }
    else
    {
        if ([resp isKindOfClass:[SendAuthResp class]])
        {
            SendAuthResp *aresp = (SendAuthResp *)resp;
            if (aresp.errCode == 0) {
                NSString *code = aresp.code;
                [self getAccess_token:code];
            }
        }
        else
        {
            if (resp.errCode == 0) {
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_WXShareSucceed object:resp];
            } else {
                
                [[NSNotificationCenter defaultCenter] postNotificationName:NotificationName_WXShareFaild object:resp];
            }
        }
    }
    
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ([sourceApplication isEqualToString:@"com.tencent.xin"]) {
        return [WXApi handleOpenURL:url delegate:self];//weixin
    }else if ([sourceApplication isEqualToString:@"com.sina.weibo"]){
        return [WeiboSDK handleOpenURL:url delegate:self];//sinaWeibo
    }else if (url){
        return [TencentOAuth HandleOpenURL:url] || [QQApiInterface handleOpenURL:url delegate:self];//qq
        
    }else if (url){
        return [TencentOAuth HandleOpenURL:url];//tweibo
    }else{}
    return YES;//return NO if the application can't open for some reason(暂时还没有考虑到)

}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self performSelectorOnMainThread:@selector(checkNetwork) withObject:nil waitUntilDone:NO];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SNSA_SOUND_CLOSE object:nil];
    

}

-(void)applicationDidEnterBackground:(UIApplication *)application
{
    [[ZSTF3Preferences shared] synchronize];
}

- (void)updateECClientDidSucceed:(NSDictionary *)response
{
    
}

- (void)updateECClientDidFailed:(NSString *)response
{
    
}

- (void)getECClientParamsDidSucceed:(NSDictionary *)response
{
    int msisdntype = [[[response safeObjectForKey:@"info"] safeObjectForKey:@"msisdntype"] intValue];
    int registtype = [[[response safeObjectForKey:@"info"] safeObjectForKey:@"registtype"] intValue];
    
    [ZSTF3Preferences shared].CarrierType = msisdntype;
    [ZSTF3Preferences shared].MCRegistType = registtype;
    
     [self applicationCheckRegistType];
}

- (void)getECClientParamsDidFailed:(NSString *)response
{
     [self applicationCheckRegistType];
}

- (void)automaticLoginDidSucceed:(NSDictionary *)response
{
    status = [[[response safeObjectForKey:@"data"] safeObjectForKey:@"status"] intValue];
    
    if (status == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = [[response safeObjectForKey:@"data"] safeObjectForKey:@"Msisdn"];
        [ZSTF3Preferences shared].UserId = [[response safeObjectForKey:@"data"] safeObjectForKey:@"UserId"];
        
        if ([ZSTF3Engine syncPushNotificationParams])
        {
            NSLog(@"推送绑定成功");
        }
        else
        {
            NSLog(@"推送绑定失败");
        }
    }
    
      [self.f3Engine getECClientParams];
}

- (void)automaticLoginDidFailed:(NSString *)response
{
    [self.f3Engine getECClientParams];

}

//- (NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
//{
//    return UIInterfaceOrientationMaskPortrait;
//}

- (void)dealloc {

    [TKUIUtil hiddenHUD];
    TKRELEASE(_clientVersionUrl);
    self.rootController = nil;
    self.f3Engine = nil;
    self.window = nil;
    [super dealloc];
}



-(void)sendAuthRequest
{
    SendAuthReq* req =[[SendAuthReq alloc ] init];
    req.scope = @"snsapi_userinfo,snsapi_base";
    req.state = [ZSTF3Preferences shared].ECECCID;
    [WXApi sendReq:req];
}

-(void)getAccess_token:(NSString *)code
{
    //https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
    
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code",[ZSTF3Preferences shared].wxKey,[ZSTF3Preferences shared].wxSecret,code];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                NSString *token = [NSString stringWithFormat:@"%@", [dic objectForKey:@"access_token"]];
                NSString *openId = [NSString stringWithFormat:@"%@", [dic objectForKey:@"openid"]];
                [self getUserInfoWithToken:token penId:openId];
                
            }
        });
    });
}

-(void)getUserInfoWithToken:(NSString *)token penId:(NSString *)openId
{
    // https://api.weixin.qq.com/sns/userinfo?access_token=ACCESS_TOKEN&openid=OPENID
    
    NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/userinfo?access_token=%@&openid=%@",token,openId];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSURL *zoneUrl = [NSURL URLWithString:url];
        NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
        NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];
        dispatch_async(dispatch_get_main_queue(), ^{
            if (data) {
                NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
                /*
                 {
                 city = Haidian;
                 country = CN;
                 headimgurl = "http://wx.qlogo.cn/mmopen/FrdAUicrPIibcpGzxuD0kjfnvc2klwzQ62a1brlWq1sjNfWREia6W8Cf8kNCbErowsSUcGSIltXTqrhQgPEibYakpl5EokGMibMPU/0";
                 language = "zh_CN";
                 nickname = "xxx";
                 openid = oyAaTjsDx7pl4xxxxxxx;
                 privilege =     (
                 );
                 province = Beijing;
                 sex = 1;
                 unionid = oyAaTjsxxxxxxQ42O3xxxxxxs;
                 }
                 */
                
                NSString *openId = [dic objectForKey:@"openid"];
                [ZSTF3Preferences shared].weixinOpenId = openId;
                NSString *nickName = [dic objectForKey:@"nickname"];
                NSString *headIma = [NSString stringWithFormat:@"%@", [dic objectForKey:@"headimgurl"]];
                NSString *platformType = @"6";//第三方登录：微信开放平台
                
                [[NSUserDefaults standardUserDefaults] setValue:openId forKey:@"weixinOpenId"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                NSMutableDictionary *data = [NSMutableDictionary dictionary];
                [data setObject:openId forKey:@"OpenId"];
                [data setObject:headIma forKey:@"Avatar"];
                [data setObject:nickName forKey:@"NickName"];
                [data setObject:platformType forKey:@"PlatformType"];
                
                [TKUIUtil showHUDInView:self.window withText:NSLocalizedString(@"登录中,请稍后...", nil) withImage:nil];
                [self.f3Engine thirdLoginWithDictionary:data];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"wxLoginSuccess" object:nil];
                
            }
        });
        
    });
}

- (void)thirdLoginDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    NSLog(@"第三方登录成功");
    NSLog(@"response === %@", response);
    NSString *userId = [[response objectForKey:@"data"] objectForKey:@"UserId"];
    [ZSTF3Preferences shared].UserId = userId;
    [ZSTF3Preferences shared].loginMsisdn = [[response objectForKey:@"data"] objectForKey:@"Msisdn"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"thirdLoginSuccess" object:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"wxLoginSuccess" object:nil];
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if ([[ud objectForKey:@"firstLaunch"] isEqualToString:@"1"])
    {
        
    }
    else
    {
        [ZSTF3Preferences shared].pushToken = self.deviceTokenString;
        
        if ([ZSTF3Engine syncPushNotificationParams])
        {
            [ud setObject:@"1" forKey:@"firstLaunch"];
            NSLog(@"推送绑定成功第一次");
        }
        else
        {
            NSLog(@"推送绑定失败");
        }
    }
    
    [self loginDidCancel];
}

- (void)thirdLoginDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:response withImage:nil];
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
    if ([response isKindOfClass:WBSendMessageToWeiboResponse.class])
    {
//        WBSendMessageToWeiboResponse *wbResp = (WBSendMessageToWeiboResponse *)response;
        if (response.statusCode == WeiboSDKResponseStatusCodeSuccess)
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendWeiboSuccess" object:nil];
        }
        else
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendWeiboFailure" object:nil];
        }
        
    }
    else if ([response isKindOfClass:WBAuthorizeResponse.class])
    {
        
        if ([[(WBAuthorizeResponse *)response userID] isKindOfClass:[NSString class]] && ![[(WBAuthorizeResponse *)response userID] isEqualToString:@""])
        {
            self.wbtoken = [(WBAuthorizeResponse *)response accessToken];
            self.wbCurrentUserID = [(WBAuthorizeResponse *)response userID];
            self.wbRefreshToken = [(WBAuthorizeResponse *)response refreshToken];
            [self getWBUsers];
        }
       
    }
    else if ([response isKindOfClass:WBPaymentResponse.class])
    {
        NSString *title = NSLocalizedString(@"支付结果", nil);
        NSString *message = [NSString stringWithFormat:@"%@: %d\nresponse.payStatusCode: %@\nresponse.payStatusMessage: %@\n%@: %@\n%@: %@", NSLocalizedString(@"响应状态", nil), (int)response.statusCode,[(WBPaymentResponse *)response payStatusCode], [(WBPaymentResponse *)response payStatusMessage], NSLocalizedString(@"响应UserInfo数据", nil),response.userInfo, NSLocalizedString(@"原请求UserInfo数据", nil), response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if([response isKindOfClass:WBSDKAppRecommendResponse.class])
    {
        NSString *title = NSLocalizedString(@"邀请结果", nil);
        NSString *message = [NSString stringWithFormat:@"accesstoken:\n%@\nresponse.StatusCode: %d\n响应UserInfo数据:%@\n原请求UserInfo数据:%@",[(WBSDKAppRecommendResponse *)response accessToken],(int)response.statusCode,response.userInfo,response.requestUserInfo];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                        message:message
                                                       delegate:nil
                                              cancelButtonTitle:NSLocalizedString(@"确定", nil)
                                              otherButtonTitles:nil];
        [alert show];
    }
}

- (void)getWBUsers
{
    NSURL *wbUrl = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.weibo.com/2/users/show.json?access_token=%@&uid=%@",self.wbtoken,self.wbCurrentUserID]];
    NSURLRequest *request=[NSURLRequest requestWithURL:wbUrl];
    NSOperationQueue *queue = [[NSOperationQueue alloc]init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        
        NSDictionary *responseDic = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSString *avatar = [responseDic objectForKey:@"avatar_hd"];
        NSString *nickName = [responseDic objectForKey:@"name"];
        NSString *platFormType = @"8";
        
        [[NSUserDefaults standardUserDefaults] setValue:self.wbCurrentUserID forKey:@"sinaOpenId"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSMutableDictionary *dataDic = [NSMutableDictionary dictionary];
        [dataDic setObject:self.wbCurrentUserID forKey:@"OpenId"];
        [dataDic setObject:avatar forKey:@"Avatar"];
        [dataDic setObject:nickName forKey:@"NickName"];
        [dataDic setObject:platFormType forKey:@"PlatformType"];
        
        [TKUIUtil showHUDInView:self.window withText:NSLocalizedString(@"登录中,请稍后...", nil) withImage:nil];
        [self.f3Engine thirdLoginWithDictionary:dataDic];
        [ZSTF3Preferences shared].sinaOpenId = self.wbCurrentUserID;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sinaLoginSuccess" object:nil];
    }];
}




- (void)submitBugInfo:(NSString *)bugInfo
{
//    NSUserDefaults *userDefaultsPush = [NSUserDefaults standardUserDefaults];
//    
//    NSString *exceptionLog=[userDefaultsPush stringForKey:@"exceptionLog"];
//    ZSTF3Engine *engine = [[ZSTF3Engine alloc] init];
//    engine.delegate  = self;
//    self.f3Engine = engine;
//    
////    if (![exceptionLog isKindOfClass:[NSNull class]] && ![exceptionLog isEqualToString:@""]) {
////        [self.f3Engine postSystemExceptionLog:[ZSTF3Preferences shared].ECECCID logInfo:exceptionLog];
////    }
////    
//    NSMutableDictionary *param = [NSMutableDictionary dictionary];
//    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"Ecid"];
//    [param setValue:bugInfo forKey:@"LogInfo"];
//    
//    NSURL *url = [NSURL URLWithString:BUGINFOURL];
//    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
//    NSData *body = [[(NSDictionary *)param JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
//    NSString *contentType = [NSString stringWithFormat:@"text/json"];
//    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
//    [request setHTTPMethod:@"POST"];
//    [request setHTTPBody:body];
//    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
//    NSLog(@"received --> %@",[[NSString alloc] initWithData:received encoding:NSUTF8StringEncoding]);
    
}



-(void)postSystemExceptionLogSucceed:(NSDictionary *)reponse{
    
    NSLog(@"------Exception---上传成功------------");
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:@"" forKey:@"exceptionLog"];
    [userDefaults synchronize];
    
}

-(void)postSystemExceptionLogFailed:(NSString *)response{
    
}

- (void)getThirdLoginAuthorizeData
{
    NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    [self.f3Engine getThirdLoginAuthorizeEcid:ecid];
}

- (void)loginAuthorizeDidSucceed:(NSDictionary *)response
{
    // 如果status的值等于1有第三方授权登录，否侧没有
    if ([[response objectForKey:@"status"] integerValue] == 1) {
        NSDictionary *dict = [response objectForKey:@"data"];
        
        // QQ
        NSString *qqKey = @"";
        //        NSString *qqKey = @"";
        NSString *qqSecret = @"";
        
        // 微信
        NSString *wxKey = @"";
        NSString *wxSecret = @"";
        
        // 新浪微博
        NSString *wbKey = @"";
        NSString *wbSecret = @"";
        NSString *wbRedirect = @"";
        
        NSString *wxCode = [dict safeObjectForKey:@"weixinDeveloperAccount"];
        NSString *qqCode = [dict safeObjectForKey:@"qqDeveloperAccount"];
        NSString *wbCode = [dict safeObjectForKey:@"sinaWeiBoDeveloperAccount"];
        NSString *ecid = [[ZSTF3Preferences shared].ECECCID length] == 8 ? [ZSTF3Preferences shared].ECECCID : [[ZSTF3Preferences shared].ECECCID stringByAppendingString:@"wg"];
        NSString *rWXCode = [EncryptUtil decryptUseDES:wxCode key:ecid];
        NSString *rQQCode = [EncryptUtil decryptUseDES:qqCode key:ecid];
        NSString *rWBCode = [EncryptUtil decryptUseDES:wbCode key:ecid];
        
        NSArray *wxArr = [rWXCode componentsSeparatedByString:@"|"];
        NSArray *qqArr = [rQQCode componentsSeparatedByString:@"|"];
        NSArray *wbArr = [rWBCode componentsSeparatedByString:@"|"];
        
        if (wxArr.count > 0 && wxArr[0] && [wxArr[0] isKindOfClass:[NSString class]])
        {
            wxKey = wxArr[0];
            wxSecret = wxArr[1];
        }
        
        if (qqArr.count > 0 && qqArr[0] && [qqArr[0] isKindOfClass:[NSString class]])
        {
            qqKey = qqArr[0];
            qqSecret = qqArr[1];
        }
        
        if (wbArr.count > 0 && wbArr[0] && [wbArr[0] isKindOfClass:[NSString class]])
        {
            wbKey = wbArr[0];
            wbSecret = wbArr[1];
            wbRedirect = wbArr[2];
        }
        
        [ZSTF3Preferences shared].qqKey = qqKey;
        [ZSTF3Preferences shared].qqSecret = qqSecret;
        [ZSTF3Preferences shared].wxKey = wxKey;
        [ZSTF3Preferences shared].wxSecret = wxSecret;
        [ZSTF3Preferences shared].weiboKey = wbKey;
        [ZSTF3Preferences shared].weiboSecret = wbSecret;
        [ZSTF3Preferences shared].weiboRedirect = wbRedirect;
        [ZSTF3Preferences shared].isQQLogin = [[dict safeObjectForKey:@"qq_accredit"] integerValue] == 1 ? YES : NO;
        [ZSTF3Preferences shared].isWBLogin = [[dict safeObjectForKey:@"wb_accredit"] integerValue] == 1 ? YES : NO;
        [ZSTF3Preferences shared].isWXLogin = [[dict safeObjectForKey:@"wx_accredit"] integerValue] == 1 ? YES : NO;
        
        if (qqKey && ![qqKey isEqualToString:@""])
        {
             TencentOAuth *tencent = [[TencentOAuth alloc] initWithAppId:qqKey andDelegate:self];
        }
        
        if (wxKey && ![wxKey isEqualToString:@""])
        {
            [WXApi registerApp:wxKey];
        }
       
        
    }
}

+ (void)load
{
    Class class = object_getClass((id)[UIFont class]);
//    Method originalMethod = class_getInstanceMethod(class, @selector(systemFontOfSize:));
//    Method swapMethod = class_getInstanceMethod(class, @selector(mySuitFont:));
//    method_exchangeImplementations(originalMethod, swapMethod);
    
    
    
    SEL originalSelector = @selector(systemFontOfSize:);
    SEL swizzledSelector = @selector(mySuitFont:);
    
    Method originalMethod = class_getInstanceMethod(class, originalSelector);
    Method swizzledMethod = class_getInstanceMethod(class, swizzledSelector);
    
    BOOL didAddMethod =
    class_addMethod(class,
                    originalSelector,
                    method_getImplementation(swizzledMethod),
                    method_getTypeEncoding(swizzledMethod));
    
    if (didAddMethod) {
        class_replaceMethod(class,
                            swizzledSelector,
                            method_getImplementation(originalMethod),
                            method_getTypeEncoding(originalMethod));
    } else {
        method_exchangeImplementations(originalMethod, swizzledMethod);
    }
    
}

- (void)onReq:(QQBaseReq *)req
{
    NSLog(@"1231");
}



@end
