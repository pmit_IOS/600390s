//
//  ZSTSettingViewController.m
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTSettingViewController.h"
#import "ZSTUtils.h"
#import "ZSTNewSettingCell.h"

@interface ZSTSettingViewController () <UITableViewDataSource,UITableViewDelegate,ZSTF3EngineDelegate>

@property (strong,nonatomic) UITableView *settingTableView;
@property (strong,nonatomic) NSArray *titleArr;

@end

@implementation ZSTSettingViewController

static NSString *const settingCell = @"settingCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"系统设置", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.titleArr = @[@[@"声音",@"震动",@"消息大于100条提醒"],@[@"公开手机号码"]];
    [self buildSettingTableView];
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildSettingTableView
{
    self.settingTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64) style:UITableViewStylePlain];
    self.settingTableView.delegate = self;
    self.settingTableView.dataSource = self;
    self.settingTableView.backgroundColor = RGBA(243, 243, 243, 1);
    self.settingTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.settingTableView registerClass:[ZSTNewSettingCell class] forCellReuseIdentifier:settingCell];
    [self.view addSubview:self.settingTableView];
    
    UIView *footerView = [[UIView alloc] init];
    self.settingTableView.tableFooterView = footerView;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.titleArr[section] count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTNewSettingCell *cell = [tableView dequeueReusableCellWithIdentifier:settingCell];
    BOOL isCheck;
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0 || indexPath.row == 1) {
            CALayer *line = [CALayer layer];
            line.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
            line.frame = CGRectMake(15, 45 - 0.5f, WIDTH - 15, 0.5f);
            [cell.contentView.layer addSublayer:line];
        }
        
        switch (indexPath.row)
        {
            case 0:
                if (![ud objectForKey:@"soundsOpen"] || [[ud objectForKey:@"soundsOpen"] isKindOfClass:[NSNull class]])
                {
                    isCheck = YES;
                }
                else
                {
                    if ([[ud objectForKey:@"soundsOpen"] integerValue] == 0)
                    {
                        isCheck = YES;
                    }
                    else
                    {
                        isCheck = NO;
                    }
                }
                break;
            case 1:
                if (![ud objectForKey:@"shakeOpen"] || [[ud objectForKey:@"shakeOpen"] isKindOfClass:[NSNull class]])
                {
                    isCheck = YES;
                }
                else
                {
                    if ([[ud objectForKey:@"shakeOpen"] integerValue] == 0)
                    {
                        isCheck = YES;
                    }
                    else
                    {
                        isCheck = NO;
                    }
                }
                break;
            case 2:
                if (![ud objectForKey:@"cleanOpen"] || [[ud objectForKey:@"cleanOpen"] isKindOfClass:[NSNull class]])
                {
                    isCheck = YES;
                }
                else
                {
                    if ([[ud objectForKey:@"cleanOpen"] integerValue] == 0)
                    {
                        isCheck = YES;
                    }
                    else
                    {
                        isCheck = NO;
                    }
                }
                break;
                
            default:
                break;
        }
    }
    else
    {
        isCheck = self.isPhonePublic;
    }
    
    [cell createUI];
    [cell setCellData:self.titleArr[indexPath.section][indexPath.row] IsCheck:isCheck];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 45;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 15;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *sHeaderView = [[UIView alloc] init];
    sHeaderView.backgroundColor = RGBA(243, 243, 243, 1);
    return sHeaderView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ZSTNewSettingCell *cell = (ZSTNewSettingCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.checkBtn.selected = !cell.checkBtn.isSelected;
    if (indexPath.section == 1)
    {
        NSInteger isPublic = 0;
        if (cell.checkBtn.isSelected)
        {
            isPublic = 1;
        }
        else
        {
            isPublic = 0;
        }
        
        NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"PhonePublic":@(isPublic)};
        [self.engine changeUserInfoWithParam:param];
    }
    else if (indexPath.section == 0)
    {
        NSInteger states = 0;
        if (indexPath.row == 0)
        {
            if (cell.checkBtn.isSelected)
            {
                states = 0;
                
                NSString *message = @"有新消息开启声音提醒";
                [TKUIUtil alertInWindow:message withImage:nil];
                
                AudioServicesPlaySystemSound(1007);//其中1007是系统声音的编号，其他的可用编号： iphone系统声效
                
                NSUserDefaults *soundRemind = [NSUserDefaults standardUserDefaults];
                [soundRemind setBool:YES forKey:@"tipSound"];
            }
            else
            {
                states = 1;
                
                NSString *message = @"有新消息不开启声音提醒";
                [TKUIUtil alertInWindow:message withImage:nil];
                
                NSUserDefaults *soundRemind = [NSUserDefaults standardUserDefaults];
                [soundRemind removeObjectForKey:@"tipSound"];
            }
            
            [[NSUserDefaults standardUserDefaults] setValue:@(states) forKey:@"soundsOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else if (indexPath.row == 1)
        {
            if (cell.checkBtn.isSelected)
            {
                states = 0;
                
                NSString *message = @"有新消息开启震动提醒";
                [TKUIUtil alertInWindow:message withImage:nil];
                
                AudioServicesPlaySystemSound (kSystemSoundID_Vibrate);
            }
            else
            {
                states = 1;
                
                NSString *message = @"有新消息不开启震动提醒";
                [TKUIUtil alertInWindow:message withImage:nil];
            }
            
            [[NSUserDefaults standardUserDefaults] setValue:@(states) forKey:@"shakeOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        else
        {
            if (cell.checkBtn.isSelected)
            {
                states = 0;
                
                NSString *message = @"消息大于100条开启提醒";
                [TKUIUtil alertInWindow:message withImage:nil];
            }
            else
            {
                states = 1;
                
                NSString *message = @"消息大于100条不开启提醒";
                [TKUIUtil alertInWindow:message withImage:nil];
            }
            
            [[NSUserDefaults standardUserDefaults] setValue:@(states) forKey:@"cleanOpen"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.settingTableView)
    {
        CGFloat sectionHeaderHeight = 15;
        if (scrollView.contentOffset.y<=sectionHeaderHeight&&scrollView.contentOffset.y>=0) {
            scrollView.contentInset = UIEdgeInsetsMake(-scrollView.contentOffset.y, 0, 0, 0);
        }
        else if (scrollView.contentOffset.y>=sectionHeaderHeight) {
            scrollView.contentInset = UIEdgeInsetsMake(-sectionHeaderHeight, 0, 0, 0);
        }
    }
}

- (void)changeUserInfoSucceed:(NSDictionary *)reponse
{
    NSString *message = @"";
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
    ZSTNewSettingCell *cell = (ZSTNewSettingCell *)[self.settingTableView cellForRowAtIndexPath:indexPath];
    if (cell.checkBtn.isSelected)
    {
        message = @"您已开启朋友圈公开您的手机号码";
    }
    else
    {
        message = @"您已取消朋友圈公开您的手机号码";
    }
    
    [TKUIUtil alertInWindow:message withImage:nil];
}

- (void)changeUserInfoFailed:(NSString *)response
{
    [TKUIUtil alertInWindow:response withImage:nil];
}

@end
