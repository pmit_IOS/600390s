//
//  ZSTShareSettingViewController.m
//  F3
//
//  Created by LiZhenQu on 14-10-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTShareSettingViewController.h"
#import "ZSTShareTableViewCell.h"
#import "ZSTUtils.h"
#import <SDKExport/WXApi.h>
#import "ZSTGlobal+F3.h"

#define  kZSTSettingItemType_Message                    0
#define  kZSTSettingItemType_SinaWeiBo                  3
#define  kZSTSettingItemType_WeiXinHaoYou               1
#define  kZSTSettingItemType_WeiXinPengYouQuan          2

@interface ZSTShareSettingViewController ()
{
    BOOL isallow;
    NSInteger sinaRow;
    NSInteger weixinHaoYouRow;
    NSInteger weixinPengYouQuanRow;
}

@end

@implementation ZSTShareSettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
     self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
     self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"分享", nil)];
    
    self.tableView.layer.borderWidth = 1;
    self.tableView.layer.cornerRadius = 4;
    self.tableView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    self.tableView.layer.masksToBounds = YES;
    
    [self initSettingItems];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareSucceed:) name:NotificationName_WXShareSucceed object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(wxShareFaild:) name:NotificationName_WXShareFaild object:nil];
    
    self.engine = [[[ZSTF3Engine alloc] init] autorelease];
    self.engine.delegate = self;
    
    isallow = YES;
}

- (void) initSettingItems
{
    self.dataArray = [[NSMutableArray alloc] initWithObjects:@"短信分享",nil];
    ZSTF3Preferences *pre = [ZSTF3Preferences shared];
    if (pre.SinaWeiBo) {
        sinaRow = 1;
        [_dataArray addObject:NSLocalizedString(@"新浪微博",nil)];
        
    } else {
        sinaRow = 4;
        [ZSTF3Preferences shared].SinaWeiBoShare = YES;
    }
    
    if (pre.WeiXin) {
        
        if (pre.SinaWeiBo)
        {
            weixinHaoYouRow = 2;
            weixinPengYouQuanRow = 3;
        }
        else
        {
            weixinHaoYouRow = 1;
            weixinPengYouQuanRow = 2;
        }
        
        
        [_dataArray addObject:NSLocalizedString(@"微信好友",nil)];
        [_dataArray addObject:NSLocalizedString(@"微信朋友圈",nil)];
    } else {
        
        weixinHaoYouRow = 5;
        weixinPengYouQuanRow = 6;
        
        [ZSTF3Preferences shared].WX_Friend = YES;
        [ZSTF3Preferences shared].WX_FriendsCircle = YES;
    }
    
    CGRect frame = self.tableView.frame;
    frame.size.height = self.dataArray.count * 60;
    self.tableView.frame = frame;
}

- (void) setBlocked:(shareBlocked)block
{
    _shareBlocked = [block copy];
}

- (void) showAlertWithMsg:(NSString *)string
{
    UIView *promptView = [[UIView alloc] init];
    promptView.frame = CGRectMake(40, 180, 240, 75);
    promptView.backgroundColor = [UIColor whiteColor];
    promptView.layer.cornerRadius = 8;
    promptView.layer.masksToBounds = YES;
    promptView.layer.borderWidth = 1;
    promptView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    [self.view.window addSubview:promptView];
    
    UILabel *textLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 20, 200, 18)];
    textLabel.numberOfLines = 0;
    textLabel.backgroundColor = [UIColor clearColor];
    textLabel.font = [UIFont systemFontOfSize:14];
    textLabel.textAlignment = NSTextAlignmentCenter;
    CGSize size = [string sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(200, 50) lineBreakMode:NSLineBreakByCharWrapping];
    
    textLabel.frame = CGRectMake(20, 20, 200, size.height);
    textLabel.text = string;
    [promptView addSubview:textLabel];
    [textLabel release];
    
    [UIView animateWithDuration:0.2
                          delay:0.2
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         promptView.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                         
                         [UIView animateWithDuration:2
                                               delay:2
                                             options:UIViewAnimationOptionCurveEaseOut
                                          animations:^{
                                              promptView.alpha = 0;
                                          }
                                          completion:^(BOOL finished) {
                                          }
                          ];
                     }
     ];
    
    [promptView release];
}

#pragma mark- ZSTHHSinaShareControllerDelegate

- (void)shareDidFinish:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
    
    if (![ZSTF3Preferences shared].SinaWeiBoShare) {
        [TKUIUtil showHUD:self.view];
        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                    userId:[ZSTF3Preferences shared].UserId
                                  pointNum:self.sharePoint
                                 pointType:5
                                costAmount:@""
                                      desc:@""];
        _shareBlocked([self.sharePoint intValue]);
    }
    
    [ZSTF3Preferences shared].SinaWeiBoShare = YES;
}

- (void)shareDidFail:(ZSTHHSinaShareController *)sinaShareController options:(NSString *)options
{
    [TKUIUtil alertInView:self.view withTitle:options withImage:nil];
}


#pragma mark wxShare

- (void)wxShareSucceed:(NSNotification*) notification
{
    if ([ZSTF3Preferences shared].WXshareType == 0) {
        
        if (![ZSTF3Preferences shared].WX_Friend) {
            
            [TKUIUtil showHUD:self.view];
            [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                        userId:[ZSTF3Preferences shared].UserId
                                      pointNum:self.sharePoint
                                     pointType:6
                                    costAmount:@""
                                          desc:@""];
            
             _shareBlocked([self.sharePoint intValue]);
        }
        
        [ZSTF3Preferences shared].WX_Friend = YES;
        
    } else if ([ZSTF3Preferences shared].WXshareType == 1) {
        
        if (![ZSTF3Preferences shared].WX_FriendsCircle) {
            
            [TKUIUtil showHUD:self.view];
            [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
                                        userId:[ZSTF3Preferences shared].UserId
                                      pointNum:self.sharePoint
                                     pointType:7
                                    costAmount:@""
                                          desc:@""];
            _shareBlocked([self.sharePoint intValue]);
        }
        
        [ZSTF3Preferences shared].WX_FriendsCircle = YES;
        
    }
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"微信分享成功", nil) withImage:nil];
    
    [self.tableView reloadData];
    
    isallow = YES;
}

- (void)wxShareFaild:(NSNotification*) notification
{
    
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您已取消了分享", nil) withImage:nil];
    [self.tableView reloadData];
    
    isallow = YES;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark - UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ZSTShareTableViewCell";
    ZSTShareTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell) {
        
        NSArray* array = [[UINib nibWithNibName:identifier bundle:nil] instantiateWithOwner:self options:nil];
        cell = [array objectAtIndex:0];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.titleLabel.text = [self.dataArray objectAtIndex:indexPath.row];
    
    cell.rightLabel.text = [NSString stringWithFormat:@"分享赚%@积分",self.sharePoint];
    
    //这里是根据本地保存的数据判断是否已经分享过。
    //正确应该是从服务器取得/
    if (indexPath.row == 0) {
        cell.rightLabel.hidden = YES;
    } else if (indexPath.row == sinaRow) {
        
        cell.rightLabel.hidden = [ZSTF3Preferences shared].SinaWeiBoShare;
    } else if (indexPath.row == weixinHaoYouRow) {
        
        cell.rightLabel.hidden = [ZSTF3Preferences shared].WX_Friend;
    } else if (indexPath.row == weixinPengYouQuanRow) {
        
        cell.rightLabel.hidden = [ZSTF3Preferences shared].WX_FriendsCircle;
    }
    
    return cell;
}

#pragma mark －－－－－－－－－－－－－－－－－－－－－UITableViewDataSource－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];//选中后的反显颜色即刻消失
    
    if (indexPath.row == kZSTSettingItemType_Message) {
        
        [self openRecommendSMSView];
        
    } else if (indexPath.row == sinaRow) {
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        ZSTHHSinaShareController *sinaShare = [[ZSTHHSinaShareController alloc] init];
        sinaShare.delegate = self;
        sinaShare.shareString = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】地址：%@", nil),appDisplayName,[ZSTF3Preferences shared].ECECCID];
        sinaShare.shareType = sinaWeibo_ShareType;
        sinaShare.navigationItem.title = NSLocalizedString(@"新浪微博", nil);
        UINavigationController *n = [[UINavigationController alloc] initWithRootViewController:sinaShare];
        [self presentViewController:n animated:YES completion:nil];
        [n release];
        [sinaShare release];
        
    } else if (indexPath.row == weixinHaoYouRow) {
        
        if (!isallow) {
            
            [TKUIUtil alertInWindow:@"亲～，点击太快哦" withImage:nil];
            
            return;
        }
        
        if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"提示" , nil)
                                  message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                  otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
            alert.tag = 108;
            [alert show];
            return;
        }
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        // 发送内容给微信
        WXMediaMessage *message = [WXMediaMessage message];
        message.title = appDisplayName;
        [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
        message.description = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】", nil),appDisplayName];
        
        WXImageObject *ext = [WXImageObject object];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
        ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
        message.mediaObject = ext;
        
        WXWebpageObject *webpage = [WXWebpageObject object];
        message.mediaObject = webpage;
        webpage.webpageUrl = [NSString stringWithFormat:@"%@/%@",NSLocalizedString(@"GP_shareUrl", nil),[ZSTF3Preferences shared].ECECCID];
        
        SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.message = message;
        req.scene = WXSceneSession;
        [ZSTF3Preferences shared].WXshareType = 0;
        
        [WXApi sendReq:req];
        
        isallow = NO;
        
    } else if (indexPath.row == weixinPengYouQuanRow) {
        
        if (!isallow) {
            
            [TKUIUtil alertInWindow:@"亲～，点击太快哦" withImage:nil];
            
            return;
        }
        if (![WXApi isWXAppInstalled] ||! [WXApi isWXAppSupportApi]) {
            
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"提示" , nil)
                                  message:NSLocalizedString(@"您未安装微信，现在安装？" , nil)
                                  delegate:self
                                  cancelButtonTitle:NSLocalizedString(@"取消" , nil)
                                  otherButtonTitles:NSLocalizedString(@"安装" , nil), nil];
            alert.tag = 108;
            [alert show];
            return;
        }
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        // 发送内容给微信
        WXMediaMessage *message = [WXMediaMessage message];
        message.title = [NSString stringWithFormat:NSLocalizedString(@"亲们，给大家分享一个不错的APP【%@】", nil),appDisplayName];
        [message setThumbImage:[UIImage imageNamed:@"icon.png"]];
        
        WXImageObject *ext = [WXImageObject object];
        NSString *filePath = [[NSBundle mainBundle] pathForResource:@"icon" ofType:@"png"];
        ext.imageData = [NSData dataWithContentsOfFile:filePath] ;
        message.mediaObject = ext;
        
        WXWebpageObject *webpage = [WXWebpageObject object];
        message.mediaObject = webpage;
        webpage.webpageUrl = [NSString stringWithFormat:@"%@/%@",NSLocalizedString(@"GP_shareUrl", nil),[ZSTF3Preferences shared].ECECCID];
        
        SendMessageToWXReq* req = [[SendMessageToWXReq alloc] init];
        req.bText = NO;
        req.message = message;
        req.scene = WXSceneTimeline;
         [ZSTF3Preferences shared].WXshareType = 1;
        
        [WXApi sendReq:req];
        
        isallow = NO;
    }
}

-(void) openRecommendSMSView
{
    if (![ZSTF3Preferences shared].smsShare) {
//        [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"推荐好友短信发送成功", nil)];
        [TKUIUtil showHUD:self.view];
//        [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
//                                    userId:[ZSTF3Preferences shared].UserId
//                                  pointNum:self.sharePoint
//                                 pointType:4
//                                costAmount:@""
//                                      desc:@""];
//        
//        _shareBlocked([self.sharePoint intValue]);
    }
    
    [ZSTF3Preferences shared].smsShare = YES;
    
    Class messageClass = (NSClassFromString(@"MFMessageComposeViewController"));
    
    if (messageClass != nil && [MFMessageComposeViewController canSendText]) {
        
        NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
        
        MFMessageComposeViewController *picker = [[MFMessageComposeViewController alloc] init];
        NSString *sharebaseurl = [NSString stringWithFormat:@"%@/%@",NSLocalizedString(@"GP_shareUrl", nil),[ZSTF3Preferences shared].ECECCID];
        picker.body = [NSString stringWithFormat:NSLocalizedString(@"GP_SMSRecommendMessage", nil),appDisplayName,sharebaseurl];
        
        picker.messageComposeDelegate = self;
        
        [self presentViewController:picker animated:YES completion:nil];
        
        [picker release];
    } else {
        
        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:[NSString stringWithFormat:@"sms:"]]];
    }
    [ZSTLogUtil logUserAction:NSStringFromClass([MFMessageComposeViewController class])];
    
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller
                 didFinishWithResult:(MessageComposeResult)result
{
    [self dismissViewControllerAnimated:YES completion:nil];
    
    // 直接检测服务器是否绑定成功
    if (result==MessageComposeResultSent) {
        
//        if (![ZSTF3Preferences shared].smsShare) {
//            [ZSTUtils showAlertTitle:nil message:NSLocalizedString(@"推荐好友短信发送成功", nil)];
//            [TKUIUtil showHUD:self.view];
//            [self.engine updatePointWithMsisdn:[ZSTF3Preferences shared].loginMsisdn
//                                        userId:[ZSTF3Preferences shared].UserId
//                                      pointNum:10
//                                     pointType:@"4"
//                                    costAmount:@""
//                                          desc:@""];
//        }
//        
//        [ZSTF3Preferences shared].smsShare = YES;
    }
}

- (void)updatePointDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    [self showAlertWithMsg:[NSString stringWithFormat:@"恭喜你获得%@积分",self.sharePoint]];
}

- (void)updatePointDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
}

#pragma mark - UIAlertViewDelegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex != 0) {
        NSString* installUrl =  [WXApi getWXAppInstallUrl];
        [[UIApplication sharedApplication]openURL:[NSURL URLWithString:installUrl]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    TKRELEASE(_tableView);
    [_dataArray release];
    [super dealloc];
}

@end
