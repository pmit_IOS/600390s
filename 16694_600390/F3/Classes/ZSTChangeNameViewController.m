//
//  ZSTChangeNameViewController.m
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-30.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTChangeNameViewController.h"

#define kZSTPersonalItem_Name                   1
#define kZSTPersonalItem_PhoneNum               2
#define kZSTPersonalItem_Birthday               3
#define kZSTPersonalItem_Sex                    4
#define kZSTPersonalItem_Address                5
#define kZSTPersonalItem_Signature              6

@interface ZSTChangeNameViewController ()


@end

@implementation ZSTChangeNameViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.rightBarButtonItem = [TKUIUtil itemForNavigationWithTitle:NSLocalizedString(@"保存",@"") target:self selector:@selector (saveAction:)];
    
    _bgView = [[UIView alloc] initWithFrame:CGRectMake(20, 25, 280, 40)];
    _bgView.backgroundColor = [UIColor whiteColor];
    _bgView.layer.borderWidth = 1;
    _bgView.layer.cornerRadius = 4;
    _bgView.layer.borderColor = [UIColor colorWithRed:228/255.0f green:228/255.0f blue:228/255.0f alpha:1].CGColor;
    _bgView.layer.masksToBounds = YES;
    [self.scrollView addSubview:_bgView];
    
    _textView = [[ZSTTextView alloc] initWithFrame:CGRectMake(5, 5, 270, 30)];
    _textView.delegate = self;
    _textView.backgroundColor = [UIColor clearColor];
    _textView.font = [UIFont systemFontOfSize:16.0f];
    _textView.scrollEnabled = NO;
    _textView.showsVerticalScrollIndicator = NO;
    _textView.showsHorizontalScrollIndicator = NO;
    [_bgView addSubview:_textView];
    
    [self initWithData];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
}

-(void)setBlock:(changeBlocked) btnClickedBlock
{
    _changeInfoBlocked = [btnClickedBlock copy];
}

- (void) initWithData
{
    switch (self.type) {
        case kZSTPersonalItem_Name:
            _textView.text = self.userInfo.userName;
            break;
        case kZSTPersonalItem_Address:
            _textView.text = self.userInfo.address;
            break;
        case kZSTPersonalItem_Signature:
            _textView.text = self.userInfo.signature;
            break;
        default:
            break;
    }
    
    CGFloat newHeight = [self.textView sizeThatFits:CGSizeMake(self.textView.frame.size.width, FLT_MAX)].height;
    CGRect frame = self.textView.frame;
    frame.size.height = newHeight;
    self.textView.frame = frame;
    
    frame = _bgView.frame;
    frame.size.height = newHeight + 10;
    self.bgView.frame = frame;
}

- (void)saveAction:(id)sender
{
    [_textView resignFirstResponder];
    NSString *nameStr = [_textView.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    
    if (nameStr.length == 0 && self.type == kZSTPersonalItem_Name) {
        [TKUIUtil alertInWindow:@"亲，用户名不能为空哦～" withImage:nil];
        return;
    }
    [TKUIUtil showHUD:self.view];
    switch (self.type) {
        case kZSTPersonalItem_Name:
           
            [self.engine updateUserInfoWith:self.userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:self.userInfo.iconUrl  name:nameStr birthday:self.userInfo.birthday sex:self.userInfo.sex address:self.userInfo.address signature:self.userInfo.signature phonePublic:[ZSTF3Preferences shared].snsaStates];
            break;
        case kZSTPersonalItem_Address:
            
            if (nameStr.length == 0) {
                
                nameStr = @"未填写";
            }
           
            [self.engine updateUserInfoWith:self.userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:self.userInfo.iconUrl  name:self.userInfo.userName birthday:self.userInfo.birthday sex:self.userInfo.sex address:nameStr signature:self.userInfo.signature  phonePublic:[ZSTF3Preferences shared].snsaStates];
            break;
        case kZSTPersonalItem_Signature:
            if (nameStr.length == 0) {
                
                nameStr = @"未填写";
            }
            [self.engine updateUserInfoWith:self.userInfo.Msisdn userId:[ZSTF3Preferences shared].UserId iconUrl:self.userInfo.iconUrl  name:self.userInfo.userName birthday:self.userInfo.birthday sex:self.userInfo.sex address:self.userInfo.address signature:nameStr  phonePublic:[ZSTF3Preferences shared].snsaStates];
            break;
        default:
            break;
    }
}

- (void)updateUserInfoDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    NSString *message = [response safeObjectForKey:@"notice"];
    [TKUIUtil alertInWindow:message withImage:nil];
    
    if (self.type == kZSTPersonalItem_Name) {
        
        _changeInfoBlocked(_textView.text);
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)updateUserInfoDidFailed:(NSString *)respone
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInWindow:respone withImage:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([_textView.text isEqualToString:@"未填写"]) {
        
        _textView.text = @"";
    }
    
    [_textView becomeFirstResponder];
}

#pragma mark - Text View Delegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    // make sure the cell is at the top
    return YES;
}

- (void)textViewDidChange:(UITextView *)theTextView
{
    CGFloat newHeight = [self.textView sizeThatFits:CGSizeMake(self.textView.frame.size.width, FLT_MAX)].height;
    CGRect frame = self.textView.frame;
    frame.size.height = newHeight;
    self.textView.frame = frame;
    
    frame = _bgView.frame;
    frame.size.height = newHeight + 10;
    self.bgView.frame = frame;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    [_bgView release];
    [_textView release];
    self.scrollView = nil;
    [super dealloc];
}

@end
