//
//  ZSTMineCenterViewController.h
//  F3
//
//  Created by P&M on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRepairButton.h"

@interface ZSTMineCenterViewController : UIViewController <UITableViewDelegate, UITableViewDataSource, UIActionSheetDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIAlertViewDelegate, ZSTF3EngineDelegate>
{
    NSMutableArray *_tableViewDataArray;
}

@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) NSArray *imageArray;
@property (strong, nonatomic) NSArray *titleArray;

@property (strong, nonatomic) UIButton *loginBtn;

@property (strong, nonatomic) UIImageView *headerViewBgIma;
@property (strong, nonatomic) UIImageView *avatarBg;
@property (strong, nonatomic) UIImageView *avatarImage;

@property (strong, nonatomic) UIImageView *sexIma;
@property (strong, nonatomic) UILabel *nameLabel;

@property (strong, nonatomic) UIImagePickerController *iconPickerController;

@property (assign,nonatomic) BOOL isShareApp;

@property (strong, nonatomic) ZSTF3Engine *engine;

@end
