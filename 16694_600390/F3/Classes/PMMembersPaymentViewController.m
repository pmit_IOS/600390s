//
//  PMMembersPaymentViewController.m
//  F3
//
//  Created by P&M on 15/9/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersPaymentViewController.h"
#import "ZSTUtils.h"
#import "PMMembersCardRechargeViewController.h"

@interface PMMembersPaymentViewController () <UIWebViewDelegate>

@property (strong, nonatomic) UIWebView *webView;
@property (strong, nonatomic) NSString *url;

@end

@implementation PMMembersPaymentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"支付宝支付", nil)] ;
    self.navigationItem.leftBarButtonItem = [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"暂不支付", @"") target:self selector:@selector (popViewController)];
    
    _webView = [[UIWebView alloc] initWithFrame:CGRectMake(0 , 0, WIDTH, HEIGHT)];
    _webView.backgroundColor = [UIColor clearColor];
    _webView.delegate = self;
    _webView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [_webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
    [self.view addSubview:_webView];
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    [TKUIUtil showHUD:self.view];
}


- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *title = [_webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    self.navigationItem.titleView = [self titleViewWithTitle:title];
    
    [TKUIUtil hiddenHUD];
}

- (UIView *)titleViewWithTitle:(NSString *)title
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(62, 0, 200, 44)];
    label.backgroundColor = [UIColor clearColor];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = title;
    label.font = [UIFont boldSystemFontOfSize:20];
    //label.lineBreakMode = UILineBreakModeMiddleTruncation;
    label.lineBreakMode = NSLineBreakByWordWrapping;
    label.textColor = [ZSTUtils getNavigationTextColor];
    return[label autorelease];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType
{
    NSString *urlString = [[[request URL] absoluteString] lowercaseString];
    
    if ([urlString rangeOfString:@"ios://payfinish"].location != NSNotFound) {
        
        // popToViewController用法
        for (UIViewController *controller in self.navigationController.viewControllers) {
            
            // PMMembersCardRechargeViewController 为指定跳转到的页面
            if ([controller isKindOfClass:[PMMembersCardRechargeViewController class]]) {
                
                [self.navigationController popToViewController:controller animated:YES];
            }
        }
    }
    
    return YES;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
