
#import "TKUtil.h"
#import "UIImage+Resize.h"
#import <CommonCrypto/CommonHMAC.h>
extern unsigned char *RequestImagePixelData(UIImage *inImage);
@implementation TKUtil
+ (NSString *)postUID {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyyMMddHHmmssSSS"];
    NSDate *date = [NSDate date];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter release];
    return dateString;
}

+(UIImage *)imageNamed:(NSString *)name
{
    NSString *path = [[NSBundle mainBundle] pathForResource:[name stringByDeletingPathExtension] ofType:[name pathExtension]];
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:path];
    return [image autorelease];
}

+ (NSString *)trim:(NSString *)str {
    if (str && [str length]>0) {
        while ([str hasPrefix:@" "]) {
            str = [str substringFromIndex:1];
        }
        while ([str hasSuffix:@" "]) {
            str = [str substringToIndex:str.length-1];
        }
        return str;
    }
    return @"";
}

+ (NSString *)buildURL:(NSString *)baseURL params:(NSDictionary *)params
{
    if (params != nil && [[params allKeys] count] != 0) {
        
        NSString *prefix;
        NSRange range = [baseURL rangeOfString:@"?"];
        if (range.location == NSNotFound) {
            prefix = @"?";
        } else {
            prefix = @"&";
        }
        
        NSMutableArray *paramsArray = [NSMutableArray array];
        for (id key in [params allKeys]) {
            [paramsArray addObject:[NSString stringWithFormat:@"%@=%@", key, [params safeObjectForKey:key]]];
        }
        NSString *queryString = [paramsArray componentsJoinedByString:@"&"];
        
        return [NSString stringWithFormat:@"%@%@%@", baseURL, prefix, queryString];
    }
    return baseURL;
}

+ (id)wrapNilObject:(id)obj
{
    return obj?obj:[NSNull null];
}

// Borrowed from: http://stackoverflow.com/questions/652300/using-md5-hash-on-a-string-in-cocoa
+ (NSString *)md5:(NSString *)str {
    if (!str) {
        return @"00000000000000000000000000000000";
    }
    const char *cStr = [str UTF8String];
    unsigned char result[16];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3], 
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

+ (NSString *)md5Data:(NSData *)data {
    if (!data) {
        return @"00000000000000000000000000000000";
    }
    // Create byte array of unsigned chars
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(data.bytes, (CC_LONG)data.length, result);
    return [NSString stringWithFormat:
            @"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X",
            result[0], result[1], result[2], result[3], 
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];
}

//获取汉字与字符混合的字符串长度
+(NSInteger)strLength:(NSString*)strtemp
{
    if ((id)strtemp==[NSNull null])
    {
        return 0;
    }
    NSInteger strlength = 0;
    char* p = (char*)[strtemp cStringUsingEncoding:NSUnicodeStringEncoding];
    for (NSInteger i=0 ; i<[strtemp lengthOfBytesUsingEncoding:NSUnicodeStringEncoding] ;i++) {
        if (*p) {
            p++;
            strlength++;
        }
        else {
            p++;
        }
    }
    return strlength;
}

#define BEGIN_FLAG @"[/"
#define END_FLAG @"]"

//图文混排
+ (void)getImageRange:(NSString*)message : (NSMutableArray*)array {
    NSRange range=[message rangeOfString: BEGIN_FLAG];
    NSRange range1=[message rangeOfString: END_FLAG];
    //判断当前字符串是否还有表情的标志。
    if (range.length>0 && range1.length>0) {
        if (range.location > 0) {
            [array addObject:[message substringToIndex:range.location]];
            [array addObject:[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)]];
            NSString *str=[message substringFromIndex:range1.location+1];
            [self getImageRange:str :array];
        }else {
            NSString *nextstr=[message substringWithRange:NSMakeRange(range.location, range1.location+1-range.location)];
            //排除文字是“”的
            if (![nextstr isEqualToString:@""]) {
                [array addObject:nextstr];
                NSString *str=[message substringFromIndex:range1.location+1];
                [self getImageRange:str :array];
            }else {
                return;
            }
        }
        
    } else if (message != nil) {
        [array addObject:message];
    }
}

#define KFacialSizeWidth  18
#define KFacialSizeHeight 18
#define MAX_WIDTH 165


+ (UIView *)assembleMessageAtIndex : (NSString *) message from:(BOOL)fromself
{
    NSMutableArray *array = [[NSMutableArray alloc] init];
    [self getImageRange:message :array];
    UIView *returnView = [[UIView alloc] initWithFrame:CGRectZero];
    NSArray *data = array;
    UIFont *fon = [UIFont systemFontOfSize:14.0f];
    CGFloat upX = 0;
    CGFloat upY = 0;
    CGFloat X = 0;
    CGFloat Y = 0;
    if (data) {
        
        UILabel *fromlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 2, 50, 20)];
        fromlabel.font = [UIFont systemFontOfSize:14.0f];
        fromlabel.backgroundColor = [UIColor clearColor];
        fromlabel.tag = 1200;
        
        UIImageView *timeImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/bubble-time.png"]];
        timeImageView.frame = CGRectMake(CGRectGetMaxX(fromlabel.frame)+5, 4+2, 12, 12);
        
        UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timeImageView.frame)+5, 2, 115, 20)];
        dateLabel.font = [UIFont systemFontOfSize:11];
        dateLabel.textColor = [UIColor colorWithRed:145/255.0 green:145/255.0 blue:145/255.0 alpha:1];
        dateLabel.backgroundColor = [UIColor clearColor];
        NSDateFormatter  *formatter = [[NSDateFormatter alloc] init];
		[formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
		NSString *timeString = [formatter stringFromDate:[NSDate date]];
		[formatter release];
        dateLabel.tag = 1201;
		[dateLabel setText:timeString];
        
        if (fromself) {
            fromlabel.textColor = [UIColor colorWithRed:199/255.0 green:156/255.0 blue:54/255.0 alpha:1];
            fromlabel.text = NSLocalizedString(@"用户：", nil);
        }else {
            
            fromlabel.textColor = [UIColor colorWithRed:47/255.0 green:137/255.0 blue:198/255.0 alpha:1];
            fromlabel.text = NSLocalizedString(@"客服：", nil);;
        }
        
        [returnView addSubview:fromlabel];
        [returnView addSubview:timeImageView];
        [returnView addSubview:dateLabel];
        [fromlabel release];
        [timeImageView release];
        [dateLabel release];
        
        for (int i=0;i < [data count];i++) {
            NSString *str=[data objectAtIndex:i];
            if ([str hasPrefix: BEGIN_FLAG] && [str hasSuffix: END_FLAG])
            {
                if (upX >= MAX_WIDTH)
                {
                    upY = upY + KFacialSizeHeight;
                    upX = 0;
                    X = 150;
                    Y = upY;
                }
                NSString *imageName=[str substringWithRange:NSMakeRange(2, str.length - 3)];
                UIImageView *img=[[UIImageView alloc]initWithImage:[UIImage imageNamed:imageName]];
                img.frame = CGRectMake(upX, upY+30, KFacialSizeWidth, KFacialSizeHeight);
                [returnView addSubview:img];
                [img release];
                upX=KFacialSizeWidth+upX;
                if (X<150) X = upX;
                
                
            } else {
                for (int j = 0; j < [str length]; j++) {
                    NSString *temp = [str substringWithRange:NSMakeRange(j, 1)];
                    if (upX >= MAX_WIDTH)
                    {
                        upY = upY + KFacialSizeHeight;
                        upX = 0;
                        X = 150;
                        Y =upY;
                    }
                    CGSize size=[temp sizeWithFont:fon constrainedToSize:CGSizeMake(150, 40)];
                    UILabel *la = [[UILabel alloc] initWithFrame:CGRectMake(upX,upY+30,size.width,size.height)];
                    la.font = fon;
                    la.text = temp;
                    la.backgroundColor = [UIColor clearColor];
                    [returnView addSubview:la];
                    [la release];
                    upX=upX+size.width;
                    if (X<150) {
                        X = upX;
                    }
                }
            }
        }
    }
    returnView.frame = CGRectMake(15.0f,1.0f, MAX_WIDTH+10, Y+35); //@ 需要将该view的尺寸记下，方便以后使用
//    NSLog(@"%.1f %.1f", X, Y);
    return returnView;
}

+ (UIView *)bubbleView:(NSString *)text from:(BOOL)fromSelf {

    // build single chat bubble cell with given text
    UIView *returnView =  [self assembleMessageAtIndex:text from:fromSelf];
    returnView.backgroundColor = [UIColor clearColor];
    UIView *cellView = [[UIView alloc] initWithFrame:CGRectZero];
    cellView.backgroundColor = [UIColor redColor];
    
    NSString *imgPath=[[NSBundle mainBundle] pathForResource:fromSelf?@"TKCommonLib.bundle/BubbleView/bubble-myself-bg":@"TKCommonLib.bundle/BubbleView/bubble-other-bg" ofType:@"png"];
    UIImage *bubble = [UIImage imageWithContentsOfFile:imgPath];
	UIImageView *bubbleImageView = [[UIImageView alloc] initWithImage:[bubble stretchableImageWithLeftCapWidth:20 topCapHeight:26]];
    
    TKAsynImageView *headImageView = [[TKAsynImageView alloc] initWithFrame:CGRectMake(24, 12, 23, 23)];
    headImageView.tag = 1202;
    headImageView.adjustsImageWhenHighlighted = NO;
    
    UIActivityIndicatorView *indicatorView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    indicatorView.backgroundColor = [UIColor yellowColor];
    
    if(fromSelf){
        headImageView.defaultImage = [UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/face-myself.png"];
        returnView.frame= CGRectMake(29.0f, 12.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(20.0f, 10.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f );
        indicatorView.frame = CGRectMake(5.0f, CGRectGetMidY(bubbleImageView.frame)-4.0f, 8, 8);
        cellView.frame = CGRectMake(260.0f-bubbleImageView.frame.size.width-20.0f, 0.0f,bubbleImageView.frame.size.width+60.0f+20.0f, bubbleImageView.frame.size.height+30.0f);
        headImageView.frame = CGRectMake(bubbleImageView.frame.size.width + 30.0f, 10.0f, 40.0f, 40.0f);
        [cellView addSubview:indicatorView];
        [indicatorView startAnimating];
        [indicatorView release];
    }
	else{
        headImageView.defaultImage = [UIImage imageNamed:@"TKCommonLib.bundle/BubbleView/face-other.png"];
        returnView.frame= CGRectMake(75.0f, 12.0f, returnView.frame.size.width, returnView.frame.size.height);
        bubbleImageView.frame = CGRectMake(60.0f, 10.0f, returnView.frame.size.width+24.0f, returnView.frame.size.height+24.0f);
		cellView.frame = CGRectMake(0.0f, 0.0f, CGRectGetMaxX(bubbleImageView.frame),bubbleImageView.frame.size.height+30.0f);
        headImageView.frame = CGRectMake(10.0f, 10.0f, 40.0f, 40.0f);
    }
    

    [cellView addSubview:bubbleImageView];
    [cellView addSubview:headImageView];
    [cellView addSubview:returnView];
    [bubbleImageView release];
    [returnView release];
    [headImageView release];
	return [cellView autorelease];
    
}


//将角度转换为弧度
static double deg2rad(double degree) {
    return degree / 180 * 3.1415926;
}
//将弧度转换为角度
static double rad2deg(double radian) {
    return radian * 180 / 3.1415926;
}

+ (double)distanceBetweenPoint:(CGPoint)fromPoint withPoint:(CGPoint)toPoint {
    double lat1 = fromPoint.x;
    double lon1 = fromPoint.y;
    double lat2 = toPoint.x;
    double lon2 = toPoint.y;
    double theta = lon1 - lon2;
    double dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1))*cos(deg2rad(lat2))*cos(deg2rad(theta));
    dist = acos(dist);
    dist = rad2deg(dist);
    double meters = dist * 60 * 1.1515*1609.344;
    return meters;
}

+ (double)distanceBetweenFirstX:(double)lat1
                         firstY:(double)lon1
                        secondX:(double)lat2
                        secondY:(double)lon2 {
    double theta = lon1 - lon2;
    double dist = sin(deg2rad(lat1)) * sin(deg2rad(lat2)) + cos(deg2rad(lat1))*cos(deg2rad(lat2))*cos(deg2rad(theta));
    dist = acos(dist);
    dist = rad2deg(dist);
    double meters = dist * 60 * 1.1515*1609.344;
    return meters;
}

+ (NSString *)timeFrom1970ToString:(NSString *)string
                            formot:(NSString *)format {
    return [TKUtil timeIntervalSince1970:[string doubleValue] formot:format];
}

+ (NSString *)timeIntervalSince1970:(double)interval
                             formot:(NSString *)format {
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:format];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:interval];
    NSString *dateString = [dateFormatter stringFromDate:date];
    [dateFormatter release];
    return dateString;
}

+ (double)nowIntervalSince1970 {
    double d = [[NSDate date] timeIntervalSince1970];
    NSString *str = [NSString stringWithFormat:@"%.4lf",d];
    return [str doubleValue];
}

+ (NSString*)timeIntervalSince1970:(double)timeInterval
{
    NSString* time = nil;
	NSCalendar* calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
	NSCalendarUnit unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
	NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    
    NSDate* createdAt = [NSDate dateWithTimeIntervalSince1970:timeInterval];
    NSDateComponents *nowComponents = [calendar components:unitFlags fromDate:[NSDate date]];
    NSDateComponents *createdAtComponents = [calendar components:unitFlags fromDate:createdAt];
	if([nowComponents year] == [createdAtComponents year] &&
       [nowComponents month] == [createdAtComponents month] &&
       [nowComponents day] == [createdAtComponents day])
    {//今天
		
		int time_long = [createdAt timeIntervalSinceNow];
		
		if (time_long < 0 && time_long >-60*60) {//一小时之内
			int min = -time_long/60;
			if (min == 0) {
				min = 1;
			}
			time = [[[NSString alloc]initWithFormat:NSLocalizedString(@"%d分钟前",@"%d分钟前"),min] autorelease];
			
		}else {
			[dateFormatter setDateFormat:NSLocalizedString(@"'今天'HH:mm",@"'今天'HH:mm")];
			time = [dateFormatter stringFromDate:createdAt];
		}
    }
    else
    {//前天及以前
		NSLocale *cnLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"zh_CN"];
		[dateFormatter setLocale:cnLocale];
		[cnLocale release];
		[dateFormatter setDateFormat:NSLocalizedString(@"MMMMd'日 'HH:mm",@"MMMMd'日 'HH:mm")];
		time = [dateFormatter stringFromDate:createdAt];
    }
    [calendar release];
    [dateFormatter release];
	
    return time;
}

+(UIImage *)imageZoom:(UIImage *)image andLength:(CGFloat)length
{
    CGSize  imageSize=image.size;
    if (imageSize.width<length&&imageSize.height<length) 
    {
        return image;
    }
    CGFloat widthRate = length/imageSize.width;
    CGFloat heightRate = length/imageSize.height;
    CGFloat rate=widthRate<heightRate ? widthRate:heightRate;//按照长的一边适应
    CGFloat width=imageSize.width*rate;
    CGFloat height=imageSize.height*rate;
    UIImage *newImage=[image resizedImage:CGSizeMake(width, height) interpolationQuality:kCGInterpolationDefault];
    return newImage;
}

////add eric 2011 11 3 图片锐化 number在［－1,1］之间 radius:为锐化半径
//+(UIImage *)setSharpen:(UIImage *)inImage withNumber:(double)number withRadius:(int)radius
//{
//    unsigned char *imgPixel = RequestImagePixelData(inImage);
//	CGImageRef inImageRef = [inImage CGImage];
//	int w = CGImageGetWidth(inImageRef);
//	int h = CGImageGetHeight(inImageRef);
//	
//	int wOff = 0;
//	int pixOff = 0;
//    
//    int linew = 1 + 2*radius;
//    int lineNum = linew*linew;
//    
//	for(int y = 0;y< h;y++)
//	{
//		pixOff = wOff;
//		float vR = 0, vG = 0, vB = 0;
//        
//        float vRTemp = 0,vGTemp = 0,vBTemp = 0;
//		for (int x = 0; x<w; x++) 
//		{
//            vR = 0, vG = 0, vB = 0;
//            if (x <= radius-1 || x >= w - radius || y <= radius-1 || y >= h - radius)
//            {
//                //不做
//                continue;
//            }
//            else
//            {
//                int r[lineNum];
//                int g[lineNum];
//                int b[lineNum];
//                
//                int r0,g0,b0;
//                if (x==radius)
//                {
//                    for (int i = 0;i<linew;i++)
//                    {
//                        for (int j=0;j<linew;j++) 
//                        {
//                            int inde =  pixOff + ((i-radius)*w)*4 + (j - radius)*4;
//                            r[i*linew +j] = (unsigned char)imgPixel[inde];
//                            g[i*linew +j] = (unsigned char)imgPixel[inde+1];
//                            b[i*linew +j] = (unsigned char)imgPixel[inde+2];
//                            vR += r[i*linew +j];
//                            vG += g[i*linew +j];
//                            vB += b[i*linew +j];
//                            
//                        }
//                    }
//                }
//                else if(x>radius&&x < w - radius&&y>radius-1&&y<h - radius)
//                {
//                    for (int j = -radius;j<=radius;j++) 
//                    {
//                        vRTemp = vRTemp + (unsigned char)imgPixel[pixOff + (radius)*4 + j*4*w] - (unsigned char)imgPixel[pixOff - (radius+1)*4+ j*4*w];
//                        
//                        vGTemp = vGTemp + (unsigned char)imgPixel[pixOff + (radius)*4 +1+ j*4*w] - (unsigned char)imgPixel[pixOff - (radius+1)*4 +1+ j*4*w];
//                        vBTemp = vBTemp + (unsigned char)imgPixel[pixOff + (radius)*4 + 2 + j*4*w] - (unsigned char)imgPixel[pixOff - (radius+1)*4 + 2+ j*4*w];
//                    }
//                    vR = vRTemp;
//                    vG = vGTemp;
//                    vB = vBTemp;
//                    
//                }
//                
//                //本身
//                r0 = (unsigned char)imgPixel[pixOff];
//                g0 = (unsigned char)imgPixel[pixOff + 1];
//                b0 = (unsigned char)imgPixel[pixOff + 2];
//                
//                
//                vRTemp = vR;
//                vGTemp = vG;
//                vBTemp = vB;
//                
//                vR = (CGFloat)r0 - vR/(CGFloat)lineNum;
//                vG = (CGFloat)g0 - vG/(CGFloat)lineNum;
//                vB = (CGFloat)b0 - vB/(CGFloat)lineNum;
//                
//                int red = r0 + vR * number ;
//                int green = g0 + vG * number;
//                int blue = b0 + vB * number;
//                
//                
//                
//                if (red>255)
//                {
//                    red = 255;
//                }
//                else if (red<0)
//                {
//                    red = 0;
//                }
//                
//                if (green>255) 
//                {
//                    green = 255;
//                }
//                else if(green<0)
//                {
//                    green = 0;
//                }
//                
//                if (blue>255) 
//                {
//                    blue = 255;
//                }
//                else if(blue<0)
//                {
//                    blue = 0;
//                }
//                
//                vRTemp = vRTemp - r0 + red;
//                vGTemp = vGTemp - g0 + green;
//                vBTemp = vBTemp - b0 + blue;
//                
//                imgPixel[pixOff] = red;
//                imgPixel[pixOff+1] = green;
//                imgPixel[pixOff+2] = blue;
//                
//            }
//			pixOff += 4;
//		}
//		wOff += w * 4;
//	}
//	
//	NSInteger dataLength = w*h* 4;
//	CGDataProviderRef provider = CGDataProviderCreateWithData(NULL, imgPixel, dataLength, NULL);
//	int bitsPerComponent = 8;
//	int bitsPerPixel = 32;
//	int bytesPerRow = 4 * w;
//	CGColorSpaceRef colorSpaceRef = CGColorSpaceCreateDeviceRGB();
//	CGBitmapInfo bitmapInfo = kCGBitmapByteOrderDefault;
//	CGColorRenderingIntent renderingIntent = kCGRenderingIntentDefault;
//	
//	CGImageRef imageRef = CGImageCreate(w, h, 
//                                        bitsPerComponent, 
//                                        bitsPerPixel, 
//                                        bytesPerRow, 
//                                        colorSpaceRef, 
//                                        bitmapInfo, 
//                                        provider, NULL, NO, renderingIntent);
//	
//	UIImage *my_Image = [UIImage imageWithCGImage:imageRef];
//	
//	CFRelease(imageRef);
//	CGColorSpaceRelease(colorSpaceRef);
//	CGDataProviderRelease(provider);
//	return my_Image;
//}



+ (UIColor *)string2Color:(NSString *)colorStr {
    return [TKUtil string2Color:colorStr defaultColor:nil];
}

+ (UIColor *)string2Color:(NSString *)colorStr
             defaultColor:(UIColor *)color {
    NSString *subStr = colorStr;
    if (!colorStr) {
        if ([colorStr hasPrefix:@"#"]) {
            subStr = [colorStr substringFromIndex:1];
        }
        if ([subStr length]!=8) {
            if (!color) {
                TKDPRINT(@"Your hex color string style is error. And default color is NIL.. please check.");
            }
            return color;// TODO: return exception
        }
    }
    unsigned int r, g, b, al;
    [[NSScanner scannerWithString:[subStr substringWithRange:NSMakeRange(0, 2)]] scanHexInt:&r];
    [[NSScanner scannerWithString:[subStr substringWithRange:NSMakeRange(2, 2)]] scanHexInt:&g];
    [[NSScanner scannerWithString:[subStr substringWithRange:NSMakeRange(4, 2)]] scanHexInt:&b];
    [[NSScanner scannerWithString:[subStr substringWithRange:NSMakeRange(6, 2)]] scanHexInt:&al];
    return TKColor(r, g, b, (float)al/10);
}
@end

#pragma mark -
@implementation TKUtil (SSToolkit)
void SSDrawRoundedRect(CGContextRef context, CGRect rect, CGFloat cornerRadius) {
    CGPoint min = CGPointMake(CGRectGetMinX(rect), CGRectGetMinY(rect));
    CGPoint mid = CGPointMake(CGRectGetMidX(rect), CGRectGetMidY(rect));
    CGPoint max = CGPointMake(CGRectGetMaxX(rect), CGRectGetMaxY(rect));
    
    CGContextMoveToPoint(context, min.x, mid.y);
    CGContextAddArcToPoint(context, min.x, min.y, mid.x, min.y, cornerRadius);
    CGContextAddArcToPoint(context, max.x, min.y, max.x, mid.y, cornerRadius);
    CGContextAddArcToPoint(context, max.x, max.y, mid.x, max.y, cornerRadius);
    CGContextAddArcToPoint(context, min.x, max.y, min.x, mid.y, cornerRadius);
    
    CGContextClosePath(context);
    CGContextFillPath(context);
}
@end


