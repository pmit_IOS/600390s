//
//  PMMembersCardShopInfosViewController.m
//  F3
//
//  Created by P&M on 15/9/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersCardShopInfosViewController.h"
#import "ZSTUtils.h"

@interface PMMembersCardShopInfosViewController ()

@property (strong, nonatomic) UILabel *label;

@end

@implementation PMMembersCardShopInfosViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"门店信息"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:nil selector:@selector(popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self getMembersCardShopInfos];
    
    [self createShopInfosUI];
}

- (void)createShopInfosUI
{
    self.label = [[UILabel alloc] initWithFrame:CGRectMake(20, 200, 280, 100)];
    self.label.textColor = [UIColor blackColor];
    self.label.numberOfLines = 0;
    [self.view addSubview:self.label];
}


#pragma mark - 获取会员卡门店信息数据
- (void)getMembersCardShopInfos
{
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *cardSetId = @"F70B35CDCD4B7476E040810A90115241";
    NSInteger curPage = 1;
    NSInteger pageSize = 10;
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"cardSetId":cardSetId,@"curPage":@(curPage),@"pageSize":@(pageSize)};
    
    [self.engine getMembersCardShopInfos:param];
}

// 获取会员卡门店信息成功
- (void)getMembersCardDetailsSucceed:(NSDictionary *)response
{
    NSLog(@"门店信息:%@", response);
    
    NSArray *array = [[response safeObjectForKey:@"data"] safeObjectForKey:@"items"];
    self.label.text = [NSString stringWithFormat:@"%@", [[array objectAtIndex:0] safeObjectForKey:@"description"]];
}

- (void)getMembersCardDetailsFaild:(NSString *)response
{
    [TKUIUtil alertInWindow:response withImage:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
