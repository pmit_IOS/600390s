//
//  RegisterView.h
//  F3
//
//  Created by xuhuijun on 11-10-20.
//  Copyright 2011年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol RegisterDelegate <NSObject>

@optional

- (void)registerDidFinish;

- (void)registerDidCancel;

- (void)registerDidSucceed;

@end

@interface ZSTRegisterViewController : UIViewController <UITableViewDelegate,UITableViewDataSource,ZSTF3EngineDelegate>
{
    UITableView *_tableView;
    
    UILabel *_intervarlLabel;
    
    id<RegisterDelegate> _delegate;
    
    BOOL isallow;
}

@property(nonatomic,assign)id<RegisterDelegate> delegate;
@property(nonatomic, retain) ZSTF3Engine *engine;
@property(nonatomic, assign) BOOL isFromSetting;
@property (nonatomic, retain) UIImageView *markImgView;
@property (nonatomic, assign) BOOL isselected;

@end
