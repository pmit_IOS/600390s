//
//  ZSTPersonalViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-28.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTPersonalVariableCell.h"
#import "ZSTPersonalSexCell.h"
#import "ZSTPersonalBirthDayTableViewCell.h"

typedef  void (^saveInfoBlocked)(NSString *nickname, int type);
@interface ZSTPersonalViewController : UIViewController<UITableViewDelegate,UITableViewDataSource,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,ZSTTextFieldCellDelegate,PersonalSexCellDelegate,ZSTF3EngineDelegate>
{
     saveInfoBlocked _btnSaveBlock;
}

@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) IBOutlet UIPickerView *datePicker;
@property (weak, nonatomic) IBOutlet UIToolbar *toolbarCancelDone;

@property(nonatomic, retain) ZSTF3Engine *engine;

@property (nonatomic, strong) UIImagePickerController *iconPickerController;

-(void)setBlock:(saveInfoBlocked) btnClickedBlock;

@end
