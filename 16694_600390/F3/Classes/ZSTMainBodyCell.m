//
//  ZSTMainBodyCell.m
//  F3
//
//  Created by  xuhuijun on 12-6-14.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTMainBodyCell.h"
#import "ZSTUtils.h"
#import "NSAttributedString+NimbusAttributedLabel.h"
#import "util.h"
#import "F3ClientAppDelegate.h"
#import "ImageViewController.h"

#define kZSTMessageMaxHeight 1000
#define kZSTParagraphSpace 5

@implementation ZSTMainBodyUserCell

@synthesize avtarImageView;
@synthesize name;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 12.5, 45, 45)] autorelease];
        CALayer * avtarImageViewLayer = [avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        [self addSubview:avtarImageView];
        
        self.name = [[[UILabel alloc] init] autorelease];
        self.name.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 20, 25, 150, 20);
        self.name.backgroundColor = [UIColor clearColor];
        self.name.font = [UIFont systemFontOfSize:15];
        self.name.textAlignment = UITextAlignmentLeft;
        [self addSubview:self.name];
        
    }
    return self;
}

- (void)configCell:(ZSTWeiBoInfo *)weiBoInfo
{
    self.name.text = weiBoInfo.name;
    
    [self.avtarImageView clear];
    self.avtarImageView.url = [NSURL URLWithString:weiBoInfo.avatarString];
    [self.avtarImageView loadImage];
}


- (void)dealloc
{
    self.avtarImageView = nil;
    self.name = nil;

    [super dealloc];
}
@end

@implementation ZSTMainBodyUserTextCell 

@synthesize contentLabel;
@synthesize webHeight;
@synthesize delegate;
@synthesize WBInfo;

- (NSString*)makeHTMLMessage:(ZSTWeiBoInfo *)weiBoInfo 
{
    NSString *text = nil;

    if ([weiBoInfo.bisforward isEqualToString:@"True"] && [weiBoInfo.bisforward length] != 0) {
        text = weiBoInfo.boriginalcontent;
    }else{
        text = weiBoInfo.content;
    }
    
    //test parsed content 
	
	NSString *html = @"";
	
    NSArray *lines = [text componentsSeparatedByString:@"\\n"];
    
	NSString *line;
	_newLineCounter = [lines count];
	NSMutableArray *filteredLines = [[NSMutableArray alloc] initWithCapacity:_newLineCounter];
	NSEnumerator *en = [lines objectEnumerator];
	while(line = [en nextObject])
	{
		NSArray *words = [line componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
		NSEnumerator *en = [words objectEnumerator];
		NSString *word;
		NSMutableArray *filteredWords = [[NSMutableArray alloc] initWithCapacity:[words count]];
		while(word = [en nextObject])
		{
			if([word hasPrefix:@"http://"] || [word hasPrefix:@"https://"] || [word hasPrefix:@"www"])
			{
				if([word hasPrefix:@"www"])
					word = [@"http://" stringByAppendingString:word];
                
				NSString *yFrogURL = ValidateYFrogLink(word);
                
				if(yFrogURL == nil)
				{
					if([word hasSuffix:@".jpg"] ||
                       [word hasSuffix:@".bmp"] ||
                       [word hasSuffix:@".jpeg"] ||
                       [word hasSuffix:@".tif"] ||
                       [word hasSuffix:@".tiff"] ||
                       [word hasSuffix:@".png"] ||
                       [word hasSuffix:@".gif"]
                       )
					{
						[imagesLinks setObject:[NSNull null] forKey:word];
					}
					word = [NSString  stringWithFormat:@" <a href=%@>%@</a> ", word, word];
				}
				else
				{
					[imagesLinks setObject:word forKey:word];
					word = [NSString  stringWithFormat:@"<br><a href=%@><img src=%@.th.jpg></a><br>", yFrogURL, yFrogURL];
					_newLineCounter += 6;
				}
			}
			[filteredWords addObject:word];
		}
		[filteredLines addObject:[filteredWords componentsJoinedByString:@" "]];
		[filteredWords release];
	}
	
	//NSString *htmlTemplate = @"";
    
	//html = [NSString stringWithFormat:htmlTemplate, (int)textField.frame.size.width - 10, [filteredLines componentsJoinedByString:@"<br>"]];
	[filteredLines release];
    
    //----------------------------------------------------------------------------
	NSString *path = [[NSBundle mainBundle] pathForResource: @"detail"
													 ofType:@"html"]; 
	NSString *fileTemplate = [NSString stringWithContentsOfFile:path encoding: 
                              NSUTF8StringEncoding error:nil];
    NSString* time = [ZSTUtils convertToTime:[NSString stringWithFormat:@"%f",[[weiBoInfo.time dateByAddingTimeInterval:-8*60*60] timeIntervalSince1970]]];
    NSString*rootNick = weiBoInfo.bisforward;	
    NSString* picPath = nil;
    picPath = weiBoInfo.thumbnail_pic;
	NSString* pic320 = [weiBoInfo.original_pic stringByReplacingOccurrencesOfString:@"wap240" withString:@"woriginal"];	//wap128 //wap320
	NSString* pic_html = @"";
	if ([picPath length] != 0 && picPath != nil) {
		pic_html = [NSString stringWithFormat:@"<div id=\"content_pic\"> \
                    <img id=\"img_loading\" src=\"cushionMessage.png\" /> \
                    <a href=\"pic320://%@\"><img id=\"img_weibo\" style=\"display:none\" onload='javascript:document.getElementById(\"img_loading\").style.display=\"none\";document.getElementById(\"img_weibo\").style.display=\"block\";' \
                    onerror='javascript:document.getElementById(\"img_loading\").style.display=\"none\";' \
                    src=\"%@\" onload=\"resizeimage();\" /></a> \
                    </div>",pic320,picPath];
	}
    NSString* content = nil;
    if ([weiBoInfo.bisforward isEqualToString:@"True"] && [weiBoInfo.bisforward length] != 0) {
       content = weiBoInfo.boriginalcontent;
    }else{
        content = weiBoInfo.content;
    }

	NSString* content_html;
	//对@的用户名进行解析
    content = handleSpecialCharsForBrowser(content);
	
	NSString* origin_content = @"";
	NSString* origin_content_html = @"";
    NSString* rtReason = nil;
    if ([weiBoInfo.bisforward isEqualToString:@"True"] && [weiBoInfo.bisforward length] != 0) {
        rtReason = weiBoInfo.content;
    }
    rtReason = handleSpecialCharsForBrowser(rtReason);
    if ([rootNick isEqualToString:@"True"] && [rootNick length] != 0)
    {
        NSString* template = @"<a href=http://t.sina.cn/n/gsid=%@></a>%@";
        origin_content = [NSString stringWithFormat:template, [WBEngine gsid], content];
		content = rtReason;
    }
	
	if ([rootNick isEqualToString:@"False"] || [rootNick length] == 0) {
		content_html = [NSString stringWithFormat:@"<div id=\"content_text\">%@ %@</div>",content,pic_html];		
	} else {
		content_html = [NSString stringWithFormat:@"<div id=\"content_text\">%@</div>",content];
		origin_content_html = [NSString stringWithFormat:@"<div id=\"content_origin\"><div class=\"header\"></div><div class=\"mid\">%@ %@</div><div class=\"footer\"></div> </div>",origin_content,pic_html];
	}
	
    NSString* fileText = [NSString stringWithFormat:fileTemplate,content_html,origin_content_html,time];
	html = fileText;
	return html;
}


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {

        
        self.contentLabel = [[UIWebView alloc] init];
        self.contentLabel.dataDetectorTypes = UIDataDetectorTypeAll;
        self.contentLabel.delegate = self;
        self.contentLabel.scalesPageToFit = YES;
        for (id subview in self.contentLabel.subviews){ 
            if ([[subview class] isSubclassOfClass: [UIScrollView class]])
                ((UIScrollView *)subview).scrollEnabled = NO;
        }
        [self addSubview:self.contentLabel];
        
    }
    return self;
}



- (void)configCell:(ZSTWeiBoInfo *)weiBoInfo
{
    [self.contentLabel loadHTMLString:[self makeHTMLMessage:weiBoInfo] baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    self.WBInfo = weiBoInfo;
    
}

#pragma mark - UIWebViewDelegate


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{    
   [webView stringByEvaluatingJavaScriptFromString:@"adjustPageSize();"];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSString *url = [[request URL] absoluteString ];
    if ([url hasPrefix:@"resize://"]) {
        if([[url substringFromIndex:9] doubleValue] != 0)
        {
            self.webHeight = [[url substringFromIndex:9] doubleValue];
            if([self.delegate respondsToSelector:@selector(ZSTMainBodyUserTextCell:shouldAssignHeight:)]) {
                [self.delegate ZSTMainBodyUserTextCell:self shouldAssignHeight:self.webHeight];
            }
        }
    }else if ([url hasPrefix:@"http://"]) {

        NSString *newUrl = [NSString stringWithFormat:@"http://3g.sina.com.cn/dpool/ttt/sinaurlc.php?vt=3&gsid=%@&u=%@",[WBEngine gsid],urlEncodeValue(url)];
        
        if([self.delegate respondsToSelector:@selector(ZSTMainBodyUserTextCell:didSelectLink:)]) {
            [self.delegate ZSTMainBodyUserTextCell:self didSelectLink:newUrl];
        }
        
    }else if([url hasPrefix:@"pic320://"]){
		NSString *finalStr = [NSString stringWithFormat:@"%@",[url substringFromIndex:9]];
        
        if([self.delegate respondsToSelector:@selector(ZSTMainBodyUserTextCell:didSelectImageLink:)]) {
            [self.delegate ZSTMainBodyUserTextCell:self didSelectImageLink:finalStr];
        }
		
		
	}else{
        return YES;
    }
	return NO;
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    if (self.webHeight == 0) {
        CGSize size = [self.WBInfo.content sizeWithFont:[UIFont systemFontOfSize:15] constrainedToSize:CGSizeMake(245, 600.0f) lineBreakMode:UILineBreakModeCharacterWrap];
        CGSize attachmentSize = [self.WBInfo.boriginalcontent sizeWithFont:[UIFont systemFontOfSize:13] constrainedToSize:CGSizeMake(225, 600.0f) lineBreakMode:UILineBreakModeCharacterWrap];
        float height = 10 + 20 + 10 + (size.height ? size.height + 20 : 0) + (self.WBInfo.thumbnail_pic ? 210 : 0) + (attachmentSize.height ? attachmentSize.height + 20 : 0) +  (self.WBInfo.bisforward ? 20 : 0);
        self.webHeight = height;
    }
    self.contentLabel.frame = CGRectMake(0, 0, 320, self.webHeight);
}
- (void)dealloc
{ 
    [contentLabel release];
    self.contentLabel = nil;
    [super dealloc];
}
@end


@implementation ZSTMainBodyForwardAndCommentCell 

@synthesize avtarImageView;
@synthesize name;
@synthesize dateLabel;
@synthesize contentLabel;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        
        self.avtarImageView = [[[TKAsynImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)] autorelease];
        CALayer * avtarImageViewLayer = [avtarImageView layer];
        [avtarImageViewLayer setMasksToBounds:YES];
        [avtarImageViewLayer setCornerRadius:5.0];
        self.avtarImageView.defaultImage = [UIImage imageNamed:@"avatar_default.png"];
        [self addSubview:avtarImageView];
        
        self.name = [[[UILabel alloc] init] autorelease];
        self.name.backgroundColor = [UIColor clearColor];
        self.name.font = [UIFont systemFontOfSize:12];
        self.name.textAlignment = UITextAlignmentLeft;
        [self addSubview:self.name];
        
        self.dateLabel = [[[UILabel alloc] init] autorelease];
        self.dateLabel.backgroundColor = [UIColor clearColor];
        self.dateLabel.textAlignment = UITextAlignmentRight;
        self.dateLabel.font = [UIFont systemFontOfSize:10];
        self.dateLabel.textColor = [UIColor colorWithWhite:0.7 alpha:1];
        self.dateLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.dateLabel.shadowOffset = CGSizeMake(0, 1.0f);
        self.detailTextLabel.textAlignment = UITextAlignmentRight;
        [self addSubview:self.dateLabel];
 
        self.contentLabel = [[[UILabel alloc] init] autorelease];
        self.contentLabel.numberOfLines = 0;
        self.contentLabel.backgroundColor = [UIColor clearColor];
        self.contentLabel.font = [UIFont systemFontOfSize:13];
        self.contentLabel.textColor = [UIColor colorWithWhite:0.06 alpha:1];
        self.contentLabel.shadowColor = [UIColor colorWithWhite:1.0f alpha:1];
        self.contentLabel.shadowOffset = CGSizeMake(0, 1.0f);
        [self addSubview:self.contentLabel];
    }
    return self;
}
- (void)configCell:(ZSTWeiBoInfo *)weiBoInfo
{
    self.name.text = weiBoInfo.name;
    
    [self.avtarImageView clear];
    self.avtarImageView.url = [NSURL URLWithString:weiBoInfo.avatarString];
    [self.avtarImageView loadImage];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEE MMM dd HH:mm:ss Z yyyy"];
    [formatter setDateFormat:@"M月dd日 HH:mm"];
    NSString *dateString = [formatter stringFromDate:weiBoInfo.time];
    
    self.dateLabel.text = dateString;
    self.contentLabel.text = weiBoInfo.content;
    
}

- (void)layoutSubviews
{
    [super layoutSubviews];

    float height = 0;
    self.name.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, 8, 150, 20);
    self.dateLabel.frame =  CGRectMake(CGRectGetMaxX(self.name.frame) + 5, 8, 95, 20);
    
    height = CGRectGetMaxY(self.name.frame);
    
    CGSize size = [self.contentLabel.text sizeWithFont:self.contentLabel.font constrainedToSize:CGSizeMake(245, kZSTMessageMaxHeight) lineBreakMode:UILineBreakModeCharacterWrap];
    if (![self.contentLabel.text isEqualToString:@""] && self.contentLabel.text != nil && [self.contentLabel.text length] != 0) {
        self.contentLabel.frame = CGRectMake(CGRectGetMaxX(self.avtarImageView.frame) + 10, height + 2, 245, size.height);
        height = CGRectGetMaxY(self.contentLabel.frame);
    }else{
        self.contentLabel.frame = CGRectZero;
    }
}

- (void)dealloc
{
    self.avtarImageView = nil;
    self.name = nil;
    self.dateLabel = nil;
    self.contentLabel = nil;
    
    [super dealloc];
}
@end


