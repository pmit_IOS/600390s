//
//  PMMembersInfoViewController.m
//  F3
//
//  Created by P&M on 15/9/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersInfoViewController.h"
#import "ZSTUtils.h"

@interface PMMembersInfoViewController ()

// 会员资料
@property (strong, nonatomic) UIView *infosView;
@property (strong, nonatomic) UILabel *cardNumLab;
@property (strong, nonatomic) UITextField *nameTextField;
@property (strong, nonatomic) UITextField *mobileTextField;
@property (strong, nonatomic) CALayer *layer4;

// 验证码
@property (strong, nonatomic) UIView *authcodeView;
@property (strong, nonatomic) UITextField *authcodeTF;
@property (strong, nonatomic) UIButton *authcodeBtn;
@property (strong, nonatomic) UILabel *retryLab;
@property (strong, nonatomic) NSTimer *retryTimer;
@property (assign, nonatomic) NSInteger retryCount;
@property (assign, nonatomic) BOOL isAuthcode;

// 备注
@property (strong, nonatomic) UIView *remarkView;
@property (strong, nonatomic) UITextView *remarkTV;
@property (strong, nonatomic) UILabel *tipsLabel;
@property (assign, nonatomic) BOOL isShowKeyboard;

@property (strong, nonatomic) NSDictionary *memberData;

@end

@implementation PMMembersInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"会员资料"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    self.isAuthcode = NO;
    self.isShowKeyboard = NO;
    
    [self getMemberInfo];
    
    [self createMembersInfoUI];
    [self createRemarkViewUI];
    [self createButtonControllerUI];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // 注册通知，监听键盘出现
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidShow:)
                                                name:UIKeyboardWillShowNotification
                                              object:nil];
    // 注册通知，监听键盘消失事件
    [[NSNotificationCenter defaultCenter]addObserver:self
                                            selector:@selector(handleKeyboardDidHidden)
                                                name:UIKeyboardWillHideNotification
                                              object:nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)createMembersInfoUI
{
    // 创建手机号码 view
    self.infosView = [[UIView alloc] initWithFrame:CGRectMake(0, HeightRate(120), WIDTH, 132)];
    self.infosView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.infosView];
    
    // 分隔线
    CALayer *layer1 = [[CALayer alloc] init];
    layer1.frame = CGRectMake(0, 0, WIDTH, HeightRate(1));
    layer1.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.infosView.layer addSublayer:layer1];
    
    CALayer *layer2 = [[CALayer alloc] init];
    layer2.frame = CGRectMake(WidthRate(36), 44, WIDTH - WidthRate(36), HeightRate(1));
    layer2.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.infosView.layer addSublayer:layer2];
    
    CALayer *layer3 = [[CALayer alloc] init];
    layer3.frame = CGRectMake(WidthRate(36), 88, WIDTH - WidthRate(36), HeightRate(1));
    layer3.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.infosView.layer addSublayer:layer3];
    
    CALayer *layer4 = [[CALayer alloc] init];
    layer4.frame = CGRectMake(0, 132 - 0.5f, WIDTH, HeightRate(1));
    layer4.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    self.layer4 = layer4;
    [self.infosView.layer addSublayer:layer4];
    
    // 会员卡号
    UILabel *cardLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WidthRate(180), 30)];
    cardLabel.backgroundColor = [UIColor clearColor];
    cardLabel.text = @"会员卡号:";
    cardLabel.textColor = [UIColor blackColor];
    cardLabel.textAlignment = NSTextAlignmentLeft;
    cardLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.infosView addSubview:cardLabel];
    
    // 会员卡号码
    UILabel *cardNumLab = [[UILabel alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(350) - WidthRate(36), 7, WidthRate(350), 30)];
    cardNumLab.backgroundColor = [UIColor clearColor];
    cardNumLab.textColor = [UIColor grayColor];
    cardNumLab.textAlignment = NSTextAlignmentRight;
    cardNumLab.font = [UIFont systemFontOfSize:15.0f];
    self.cardNumLab = cardNumLab;
    [self.infosView addSubview:cardNumLab];
    
    // 真实姓名
    UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 51, WidthRate(180), 30)];
    nameLabel.backgroundColor = [UIColor clearColor];
    nameLabel.text = @"真实姓名:";
    nameLabel.textColor = [UIColor blackColor];
    nameLabel.textAlignment = NSTextAlignmentLeft;
    nameLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.infosView addSubview:nameLabel];
    
    // 真实姓名输入框
    UITextField *nameTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(350) - WidthRate(36), 51, WidthRate(350), 30)];
    nameTextField.borderStyle = UITextBorderStyleNone;
    nameTextField.delegate = self;
    nameTextField.tag = 1;
    nameTextField.textColor = RGBA(100, 100, 100, 1);
    nameTextField.textAlignment = NSTextAlignmentRight;
    nameTextField.font = [UIFont systemFontOfSize:15.0f];
    nameTextField.placeholder = @"请输入真实姓名";
    //nameTextField.tintColor = [UIColor redColor];// 改变UITextField中光标颜色
    nameTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    nameTextField.returnKeyType = UIReturnKeyDone;
    nameTextField.keyboardType = UIReturnKeyDefault;
    self.nameTextField = nameTextField;
    [self.infosView addSubview:nameTextField];
    
    // 联系电话
    UILabel *mobileLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 95, WidthRate(180), 30)];
    mobileLabel.backgroundColor = [UIColor clearColor];
    mobileLabel.text = @"联系电话:";
    mobileLabel.textColor = [UIColor blackColor];
    mobileLabel.textAlignment = NSTextAlignmentLeft;
    mobileLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.infosView addSubview:mobileLabel];
    
    // 手机号码输入框
    UITextField *mobileTextField = [[UITextField alloc] initWithFrame:CGRectMake(WIDTH - WidthRate(350) - WidthRate(36), 95, WidthRate(350), 30)];
    mobileTextField.borderStyle = UITextBorderStyleNone;
    mobileTextField.delegate = self;
    mobileTextField.tag = 2;
    mobileTextField.textColor = RGBA(100, 100, 100, 1);
    mobileTextField.textAlignment = NSTextAlignmentRight;
    mobileTextField.font = [UIFont systemFontOfSize:15.0f];
    mobileTextField.placeholder = @"请输入手机号码";
    mobileTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
    mobileTextField.returnKeyType = UIReturnKeyDone;
    mobileTextField.keyboardType = UIKeyboardTypeNumberPad;
    self.mobileTextField = mobileTextField;
    [self.infosView addSubview:mobileTextField];
    
    
    // 创建验证码 view
    self.authcodeView = [[UIView alloc] initWithFrame:CGRectMake(0, self.infosView.frame.origin.y + self.infosView.frame.size.height, WIDTH, 44)];
    self.authcodeView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.authcodeView];
    self.authcodeView.hidden = YES;
    
    // 短信验证码
    UILabel *authcodeLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 7, WidthRate(180), 30)];
    authcodeLabel.backgroundColor = [UIColor clearColor];
    authcodeLabel.text = @"验  证  码:";
    authcodeLabel.textColor = [UIColor blackColor];
    authcodeLabel.textAlignment = NSTextAlignmentLeft;
    authcodeLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.authcodeView addSubview:authcodeLabel];
    
    // 验证码输入框
    UITextField *authcodeTF = [[UITextField alloc] initWithFrame:CGRectMake(WidthRate(250), 7, WIDTH - WidthRate(270) - HeightRate(210), 30)];
    authcodeTF.borderStyle = UITextBorderStyleNone;
    authcodeTF.delegate = self;
    authcodeTF.tag = 3;
    authcodeTF.textColor = RGBA(100, 100, 100, 1);
    authcodeTF.textAlignment = NSTextAlignmentLeft;
    authcodeTF.font = [UIFont systemFontOfSize:14.0f];
    authcodeTF.placeholder = @"请输入验证码";
    authcodeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    authcodeTF.returnKeyType = UIReturnKeyDone;
    authcodeTF.keyboardType = UIKeyboardTypeNumberPad;
    self.authcodeTF = authcodeTF;
    [self.authcodeView addSubview:authcodeTF];
    
    // 获取验证码按钮
    CGFloat X = WIDTH - (WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30));
    CGFloat widtd = WIDTH - WidthRate(260) - WIDTH / 3 - WidthRate(30) - WidthRate(46);
    self.authcodeBtn = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    self.authcodeBtn.frame = CGRectMake(X, 7, widtd, 30);
    [self.authcodeBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.authcodeBtn.titleLabel setFont:[UIFont systemFontOfSize:12.0f]];
    [self.authcodeBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.authcodeBtn addTarget:self action:@selector(authcodeButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.authcodeBtn setBackgroundColor:[UIColor blueColor]];
    [self.authcodeBtn.layer setCornerRadius:4.0];
    [self.authcodeView addSubview:self.authcodeBtn];
    
    self.retryLab = [[UILabel alloc] initWithFrame:CGRectMake(X, 7, widtd, 30)];
    self.retryLab.textAlignment = NSTextAlignmentCenter;
    self.retryLab.hidden = YES;
    self.retryLab.text = @"重发(60)";
    self.retryLab.textColor = [UIColor darkGrayColor];
    self.retryLab.font = [UIFont systemFontOfSize:12.0f];
    self.retryLab.textAlignment = NSTextAlignmentCenter;
    [self.authcodeView addSubview:self.retryLab];
    
    CALayer *layer5 = [[CALayer alloc] init];
    layer5.frame = CGRectMake(0, 44 - 0.5, WIDTH, 0.5);
    layer5.backgroundColor = RGBA(200, 200, 200, 1).CGColor;
    [self.authcodeView.layer addSublayer:layer5];
}

- (void)createRemarkViewUI
{
    // 创建备注 view
    self.remarkView = [[UIView alloc] initWithFrame:CGRectMake(0, self.infosView.frame.origin.y + self.infosView.frame.size.height, WIDTH, 60)];
    self.remarkView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.remarkView];
    
    // 备注
    UILabel *remarkLabel = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(36), 5, WidthRate(90), 25)];
    remarkLabel.backgroundColor = [UIColor clearColor];
    remarkLabel.text = @"备注:";
    remarkLabel.textColor = [UIColor blackColor];
    remarkLabel.textAlignment = NSTextAlignmentLeft;
    remarkLabel.font = [UIFont systemFontOfSize:15.0f];
    [self.remarkView addSubview:remarkLabel];
    
    // 创建输入备注 textView
    self.remarkTV = [[UITextView alloc] initWithFrame:CGRectMake(WidthRate(125), 0, WIDTH - WidthRate(125) - WidthRate(36), 60)];
    self.remarkTV.backgroundColor = [UIColor clearColor];
    self.remarkTV.delegate = self;
    self.remarkTV.textColor = RGBA(79, 79, 79, 1);
    self.remarkTV.font = [UIFont systemFontOfSize:15.0f];
    self.remarkTV.layer.cornerRadius = 0.0f;  // 设置圆角值
    self.remarkTV.layer.masksToBounds = YES;
    self.remarkTV.layer.borderWidth = 0.5f;   // 设置边框大小
    [self.remarkTV.layer setBorderColor:[[UIColor clearColor] CGColor]];
    self.remarkTV.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.remarkTV.returnKeyType = UIReturnKeyDone;
    self.remarkTV.keyboardType = UIKeyboardTypeDefault;
    self.remarkTV.scrollEnabled = YES;
    [self.remarkView addSubview:self.remarkTV];
    
    self.tipsLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, WidthRate(350), 25)];
    self.tipsLabel.backgroundColor = [UIColor clearColor];
    self.tipsLabel.text = @"请输入备注(可选)";
    self.tipsLabel.textColor = RGBA(231, 231, 231, 1);
    self.tipsLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.remarkTV addSubview:self.tipsLabel];
    self.tipsLabel.hidden = NO;
}

- (void)createButtonControllerUI
{
    UIBarButtonItem *submitButton = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStylePlain target:self action:@selector(saveButtonClick:)];
    
    [submitButton setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:[UIColor whiteColor], NSForegroundColorAttributeName, [UIFont boldSystemFontOfSize:18], NSFontAttributeName, nil] forState:UIControlStateNormal];
    
    self.navigationItem.rightBarButtonItem = submitButton;
    
//    // 初始化完成设置按钮
//    UIButton *submitButton = [UIButton buttonWithType:UIButtonTypeCustom];
//    submitButton.frame = CGRectMake(WidthRate(46), HEIGHT - 64 - 60, WIDTH - WidthRate(46) * 2, 44);
//    [submitButton setTitle:@"保存" forState:UIControlStateNormal];
//    [submitButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [submitButton addTarget:self action:@selector(saveButtonClick:) forControlEvents:UIControlEventTouchUpInside];
//    [submitButton.titleLabel setFont:[UIFont boldSystemFontOfSize:ButtonFont]];
//    [submitButton setBackgroundColor:ButtonBgColor];
//    [submitButton.layer setCornerRadius:6.0f];
//    [self.view addSubview:submitButton];
}

// 回收软键盘
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

// 触摸背景回收键盘
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    
    [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *mobile = [self.memberData safeObjectForKey:@"mobileNo"];
    
    if (self.mobileTextField.tag == 2 && textField == self.mobileTextField && textField.text.length - range.length + string.length == 11 && ![self.mobileTextField.text isEqualToString:mobile]) {
        
        self.isAuthcode = YES;
        self.authcodeView.hidden = NO;
        self.layer4.frame = CGRectMake(WidthRate(36), 132 - 0.5, WIDTH - WidthRate(36), HeightRate(1));
        self.authcodeBtn.enabled = YES;
        [self.authcodeBtn setBackgroundColor:[UIColor blueColor]];
        self.authcodeTF.enabled = YES;
        self.remarkView.frame = CGRectMake(0, self.authcodeView.frame.origin.y + self.authcodeView.frame.size.height, WIDTH, 60);
        
        //第一步,对组件增加监听器
        [textField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    
    // 判断输入手机号码最大长度
    if (self.mobileTextField.tag == 2 && self.mobileTextField == textField) {
        NSInteger maxLength = 11;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    // 判断输入验证码最大长度
    if (self.authcodeTF.tag == 3 && self.authcodeTF == textField) {
        NSInteger maxLength = 6;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

//- (void)addTarget:(id)target action:(SEL)action forControlEvents:(UIControlEvents)controlEvents;

//第二步,实现回调函数
- (void)textFieldDidChange:(id)sender {
//    UITextField *_field = (UITextField *)sender;
//    NSLog(@"%@",[_field text]);
    
    NSString *mobile = [self.memberData safeObjectForKey:@"mobileNo"];
    
    if ([self.mobileTextField.text isEqualToString:mobile]) {
        
        self.isAuthcode = NO;
        self.authcodeView.hidden = YES;
        self.layer4.frame = CGRectMake(0, 132 - 0.5f, WIDTH, HeightRate(1));
        self.authcodeBtn.enabled = NO;
        self.remarkView.frame = CGRectMake(0, self.infosView.frame.origin.y + self.infosView.frame.size.height, WIDTH, 60);
    }
}

#pragma mark - 解决textView输入时键盘遮挡问题
// 监听事件
- (void)handleKeyboardDidShow:(NSNotification *)paramNotification
{
    // 让出现键盘只执行一次，当isShowKeyboard ＝ NO 时，才向上移动90像素
    if (self.isShowKeyboard == NO) {
        // 只针对3.5屏幕向上移动90像素
        [self moveView:(iPhone4s ? -90 : 0)];
        
        self.isShowKeyboard = YES;
    }
}

- (void)handleKeyboardDidHidden
{
    [self moveView:(iPhone4s ? 90 : 0)];
    
    // 当键盘消失时，向下移动90像素 ,把isShowKeyboard置NO，
    self.isShowKeyboard = NO;
}

- (void)moveView:(CGFloat)move
{
    NSTimeInterval animationDuration = 0.30f;
    CGRect frame = self.view.frame;
    frame.origin.y += move;//view的y轴上移
    self.view.frame = frame;
    [UIView beginAnimations:@"ResizeView" context:nil];
    [UIView setAnimationDuration:animationDuration];
    self.view.frame = frame;
    [UIView commitAnimations];//设置调整界面的动画效果
}

#pragma mark 隐藏键盘，实现UITextViewDelegate
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (![text isEqualToString:@""]) {
        
        self.tipsLabel.hidden = YES;
    }
    
    if ([text isEqualToString:@""] && range.location == 0 && range.length == 1) {
        
        self.tipsLabel.hidden = NO;
    }
    
    // 判断输入是否已经超出最大可输入长度
    if ([text isEqualToString:@""] && range.length > 0) {
        
        //删除字符肯定是安全的
        return YES;
    }
    else {
        
        if (self.remarkTV.text.length - range.length + text.length > 80) {
            
            [TKUIUtil alertInWindow:@"超出最大可输入长度" withImage:nil];
            
            return NO;
        }
        else {
            return YES;
        }
    }
    return YES;
}


#pragma mark - 获取会员资料
- (void)getMemberInfo
{
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *memberId = self.memberId;
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"memberId":memberId};
    
    [self.engine getMemberInfo:param];
}

// 获取会员资料成功
- (void)getMemberInfoSucceed:(NSDictionary *)response
{
    self.memberData = [response safeObjectForKey:@"data"];
    
    // 获取会员资料数据
    self.cardNumLab.text = [NSString stringWithFormat:@"%@", [self.memberData safeObjectForKey:@"cardNo"]];
    self.nameTextField.text = [NSString stringWithFormat:@"%@", [self.memberData safeObjectForKey:@"memberName"]];
    self.mobileTextField.text = [NSString stringWithFormat:@"%@", [self.memberData safeObjectForKey:@"mobileNo"]];
}

- (void)getMemberInfoFaild:(NSString *)response
{
    [TKUIUtil alertInWindow:response withImage:nil];
}


#pragma mark - 获取验证码按钮响应事件
- (void)authcodeButtonClick:(UIButton *)button
{
    NSLog(@"authcodeButtonClick");
    
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || ![ZSTUtils checkMobile:self.mobileTextField.text])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入正确的手机号码", nil) withImage:nil];
        return;
    }
    
    self.retryCount = 60;
    self.retryTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(changeRetryTimer) userInfo:nil repeats:YES];
    [self.retryTimer fire];
    
    // 验证码请求参数
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *operType = @"4";
    NSString *mobile = [NSString stringWithFormat:@"%@", self.mobileTextField.text];
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"operType":operType,@"phone":mobile};
    
    [self.engine getChangeMemberCardMobileWithAuthcode:param];
}

- (void)changeRetryTimer
{
    if (self.retryCount > 0) {
        [self.authcodeBtn setTitle:[NSString stringWithFormat:@"%@",@(self.retryCount)] forState:UIControlStateDisabled];
        [self.authcodeBtn setEnabled:NO];
        self.retryCount--;
    }else {
        [self.authcodeBtn setTitle:@"重  发" forState: UIControlStateNormal];
        [self.authcodeBtn setEnabled:YES];
        [self.retryTimer invalidate];
    }
}

// 获取验证码
- (void)getChangeMemberCardMobileWithAuthcodeSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    //NSLog(@"HHHHHHHHHHH === %@", response);
}

- (void)getChangeMemberCardMobileWithAuthcodeFaild:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}

#pragma mark - 保存按钮响应事件
- (void)saveButtonClick:(id)sender
{
    NSLog(@"saveButtonClick");
    
    // 判断是否按要求输入手机号或者验证码
    if (self.mobileTextField.text.length != 11 || ![self.mobileTextField.text hasPrefix:@"1"] || ![ZSTUtils checkMobile:self.mobileTextField.text])
    {
        [TKUIUtil alertInWindow:@"请输入正确的手机号码" withImage:nil];
        return;
    }
    if (self.authcodeTF.text.length == 0 && self.isAuthcode == YES) {
        
        [TKUIUtil alertInWindow:@"请输入获取到手机验证码" withImage:nil];
        return;
    }
    if (self.nameTextField.text.length == 0 || [ZSTUtils isEmpty:self.nameTextField.text]) {
        
        [TKUIUtil alertInWindow:@"请输入真实姓名" withImage:nil];
        return;
    }
    else {
        
        // 保存会员资料参数请求
        NSString *client = @"ios";
        NSString *ecid = @"600395";
        NSString *memberId = self.memberId;
        NSString *memberName = [NSString stringWithFormat:@"%@", self.nameTextField.text];
        NSString *memberMobile = [NSString stringWithFormat:@"%@", self.mobileTextField.text];
        NSString *memberRemark = [NSString stringWithFormat:@"%@", self.remarkTV.text];
        NSString *captchaCode = [NSString stringWithFormat:@"%@", self.authcodeTF.text];
        
        NSDictionary *param = @{@"client":client,@"ecid":ecid,@"memberId":memberId,@"memberName":memberName,@"memberRemark":memberRemark,@"memberMobile":memberMobile,@"captchaCode":captchaCode};
        
        [self.engine changeMemberCardInfo:param];
    }
}

// 修改会员卡信息成功
- (void)changeMemberCardInfoSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:@"保存会员资料成功" withImage:nil];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)changeMemberCardInfoFaild:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    
    [TKUIUtil alertInWindow:response withImage:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
