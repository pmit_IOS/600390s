//
//  FTPluginManager.h
//  F3
//
//  Created by luobin on 7/12/12.
//  Copyright (c) 2012 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZSTModule.h"
#import "ZSTModuleApplication.h"

@interface ZSTModuleManager : NSObject

SINGLETON_INTERFACE(ZSTModuleManager)

- (ZSTModule *)findModule:(NSInteger)moduleID;

- (void)destoryModules;

- (UIViewController *)launchModuleApplication:(NSInteger)moduleID withOptions:(NSDictionary *)launchOptions;
- (UIView *)launchModuleView:(NSInteger)moduleID withOptions:(NSDictionary *)launchOptions;
- (UIViewController *)launchModuleSetting:(NSInteger)moduleID withOptions:(NSDictionary *)launchOptions;

@end
