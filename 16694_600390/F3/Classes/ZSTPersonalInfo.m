//
//  ZSTPersonalInfo.m
//  F3
//
//  Created by LiZhenQu on 14-10-30.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalInfo.h"

@implementation UserInfo

+ (UserInfo *)userInfoWithdic:(NSDictionary *)dic
{
    UserInfo *info = [[UserInfo alloc] init];
    
    info.iconUrl = [dic safeObjectForKey:@"Icon"];
    info.userName = [dic safeObjectForKey:@"Name"];
    info.Msisdn = [dic safeObjectForKey:@"Msisdn"];
    info.pointNum = [dic safeObjectForKey:@"PointNum"];
    info.birthday = [[dic safeObjectForKey:@"Birthday"] intValue];
    info.sex = [[dic safeObjectForKey:@"Sex"] intValue];
    info.address = [dic safeObjectForKey:@"Address"];
    info.signature = [dic safeObjectForKey:@"Signature"];
    info.signIn = [[dic safeObjectForKey:@"SignIn"] intValue];
    info.phonePublic = [[dic safeObjectForKey:@"PhonePublic"] intValue];
    
    [ZSTF3Preferences shared].loginMsisdn = info.Msisdn;
    
    return info;
}

@end
