//
//  PMCustomSwitch.h
//  F3
//
//  Created by pmit on 15/6/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, PMCustomSwitchStatus)
{
    PMCustomSwitchStatusOn = 0,//开启
    PMCustomSwitchStatusOff = 1//关闭
};

typedef NS_ENUM(NSUInteger, PMCustomSwitchType)
{
    PMCustomSwitchTypeRight = 0,//开启
    PMCustomSwitchTypeLeft = 1//关闭
};

@protocol PMCustomSwitchDelegate <NSObject>

-(void)zstcustomSwitchSetStatus:(PMCustomSwitchStatus)status;
@end

@interface PMCustomSwitch : UIView

@property(nonatomic) PMCustomSwitchStatus status;
@property (nonatomic) PMCustomSwitchType switchType;
@property (retain,nonatomic) id<PMCustomSwitchDelegate> delegate;
@property (nonatomic,retain) UISwitch *customSwitch;


@end
