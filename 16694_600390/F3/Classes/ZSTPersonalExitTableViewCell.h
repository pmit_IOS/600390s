//
//  ZSTPersonalExitTableViewCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-21.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTPersonalExitTableViewCell : UITableViewCell

@property (nonatomic, strong) IBOutlet UIButton *exitbtn;

@end
