//
//  CatchObject.m
//  catchException
//
//  Created by pmit on 15/7/29.
//  Copyright (c) 2015年 pmit. All rights reserved.
//

#import "CatchObject.h"
#import "ZSTF3Preferences.h"
#import "ZSTF3Engine.h"
#import "ASIHttpRequest.h"
#import "ASIFormDataRequest.h"
#import "JSON.h"

@interface CatchObject()

@end


@implementation CatchObject{

    ASIFormDataRequest *request;

}

void uncaughtExceptionHandler(NSException *exception)
{
    
    //异常的堆栈信息
    NSArray *stackArray = [exception callStackSymbols];
    //出现异常的原因
    NSString *reason = [exception reason];
    //异常名称
    NSString *name = [exception name];
    NSString *exceptionInfo = [NSString stringWithFormat:@"Exceptionreason：%@nExceptionname：%@nExceptionstack：%@",name,reason,stackArray];
    NSLog(@"exception --> %@",exceptionInfo);
    if ([[ZSTF3Preferences shared].ECECCID isEqualToString:@"602781"])
    {
        NSString *syserror = [NSString stringWithFormat:@"<br><br><br>"
                              "Error Detail:<br>%@<br>--------------------------<br>%@<br>---------------------<br>%@",
                              name,reason,[stackArray componentsJoinedByString:@"<br>"]];
        NSURL *url = [NSURL URLWithString:[syserror stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
        //[[UIApplication sharedApplication] openURL:url];
        //mailto://824243782@qq.com?subject=bug报告&body=感谢您的配合!
    }
    
    NSString * phoneVersion = [[UIDevice currentDevice] systemVersion];
    NSLog(@"手机系统版本: %@", phoneVersion);
    
    NSString *ecid = [ZSTF3Preferences shared].ECECCID;
    NSString *iphoneName = @"";
    if (IS_IPHONE_4_OR_LESS)
    {
        iphoneName = @"其他";
    }
    else if (IS_IPHONE_5)
    {
        iphoneName = @"iPhone5";
    }
    else if (IS_IPHONE_6)
    {
        iphoneName = @"iPhone6";
    }
    else if (IS_IPHONE_6P)
    {
        iphoneName = @"iPhone6 Plus";
    }
    else
    {
        iphoneName = @"iPhone5s";
    }
        
    
    NSMutableArray *tmpArr=[NSMutableArray arrayWithArray:stackArray];
    [tmpArr insertObject:reason atIndex:0];
    
    
    
    NSString *encodedValue = (NSString*)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(nil,(CFStringRef)exceptionInfo, nil,(CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
    
    NSMutableDictionary *param = [NSMutableDictionary dictionary];
    [param setValue:[ZSTF3Preferences shared].ECECCID forKey:@"Ecid"];
    [param setValue:encodedValue forKey:@"LogInfo"];
    [param setValue:@"ios" forKey:@"Platform"];
    
    NSURL *url = [NSURL URLWithString:BUGINFOURL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    NSData *body = [[(NSDictionary *)param JSONRepresentation] dataUsingEncoding:NSUTF8StringEncoding];
    NSString *contentType = [NSString stringWithFormat:@"text/json"];
    [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
    [request setHTTPMethod:@"POST"];
    [request setHTTPBody:body];
    NSData *received = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
    
    NSString *status = [[NSString alloc] initWithData:received encoding:NSUTF8StringEncoding];
    
    NSLog(@"=====status====>%@",status);
    
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:exceptionInfo forKey:@"exceptionLog"];
    [userDefaults synchronize];
    
    
    
    
    
    
}

@end
