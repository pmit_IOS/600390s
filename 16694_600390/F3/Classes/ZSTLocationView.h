//
//  ZSTLocationView.h
//  F3
//
//  Created by pmit on 15/8/29.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTLocationViewDelegate <NSObject>

- (void)sureLocationSelected:(NSString *)locationString CellType:(BOOL)isHometown;
- (void)cancelLocationPickerView;

@end

@interface ZSTLocationView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (copy,nonatomic) NSString *nowCity;
@property (strong,nonatomic) UIPickerView *locationPickerView;
@property (strong,nonatomic) NSDictionary *jsonDic;
@property (strong,nonatomic) NSArray *province;
@property (strong,nonatomic) NSArray *city;
@property (copy,nonatomic) NSString *selectedProvice;
@property (copy,nonatomic) NSString *selectedCity;
@property (weak,nonatomic) id<ZSTLocationViewDelegate> locationDelegate;
@property (assign,nonatomic) BOOL isHometown;

- (void)createUI;
- (void)setDefaultData;

@end
