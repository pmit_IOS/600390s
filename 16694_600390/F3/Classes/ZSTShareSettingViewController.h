//
//  ZSTShareSettingViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MessageUI.h>
#import "ZSTHHSinaShareController.h"
#import "ZSTF3Engine.h"

typedef  void (^shareBlocked)(int count);

@interface ZSTShareSettingViewController : UIViewController< MFMessageComposeViewControllerDelegate,ZSTHHSinaShareControllerDelegate,ZSTF3EngineDelegate>
{
    shareBlocked _shareBlocked;
}

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *dataArray;

@property(nonatomic, retain) ZSTF3Engine *engine;

@property (nonatomic, retain) NSString *sharePoint;

- (void) setBlocked:(shareBlocked)block;

@end
