//
//  ZSTNewSettingCell.h
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PMRepairButton.h"

@interface ZSTNewSettingCell : UITableViewCell

@property (strong,nonatomic) UILabel *titleLB;
@property (strong,nonatomic) PMRepairButton *checkBtn;

- (void)createUI;
- (void)setCellData:(NSString *)titleString IsCheck:(BOOL)isCheck;

@end
