//
//  PMRechargeMoneyView.m
//  F3
//
//  Created by P&M on 15/9/15.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMRechargeMoneyView.h"

@implementation PMRechargeMoneyView

- (void)createRechargeMoneyPicker
{
    self.backgroundColor = [UIColor whiteColor];
    
    // 导航控制器全屏滑动返回上一页效果实现代码
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(rechargePickerViewDismiss:)];
    swipe.direction = UISwipeGestureRecognizerDirectionDown;
    [self addGestureRecognizer:swipe];
    
    UIView *optionView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, 40)];
    optionView.backgroundColor = [UIColor whiteColor];
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0, 40 - 0.5f, WIDTH, HeightRate(1));
    layer.backgroundColor = RGBA(243, 243, 243, 1).CGColor;
    [optionView.layer addSublayer:layer];
    
    
    NSString *tipsString = @"请选择...";
    CGSize tipsSize = [tipsString boundingRectWithSize:CGSizeMake(MAXFLOAT, 20) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:15.0f]} context:nil].size;
    
    UILabel *tipsLab = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, tipsSize.width, 20)];
    tipsLab.backgroundColor = [UIColor clearColor];
    tipsLab.text = tipsString;
    tipsLab.textColor = RGBA(136, 136, 136, 1);
    tipsLab.textAlignment = NSTextAlignmentLeft;
    tipsLab.font = [UIFont systemFontOfSize:15.0f];
    [optionView addSubview:tipsLab];
    
    // 创建完成按钮
    UIButton *finishedBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    finishedBtn.frame = CGRectMake(WIDTH - 10 - 40, 10, 40, 20);
    [finishedBtn setTitle:@"完成" forState:UIControlStateNormal];
    [finishedBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    finishedBtn.titleLabel.font = [UIFont systemFontOfSize:15.0f];
    [finishedBtn addTarget:self action:@selector(finishedButtonClick:) forControlEvents:UIControlEventTouchUpInside];
    [optionView addSubview:finishedBtn];
    [self addSubview:optionView];
    
    // 选择器
    self.rechargePickerView = [[UIPickerView alloc] initWithFrame:CGRectZero];
    self.rechargePickerView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.rechargePickerView.frame = CGRectMake(0, 30, WIDTH, 150);
    self.rechargePickerView.delegate = self;
    self.rechargePickerView.dataSource = self;
    [self addSubview:self.rechargePickerView];
    
    // 刷新 UIPickerView
    [self.rechargePickerView reloadAllComponents];
}

// 返回有几列
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// 返回指定列的行数
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return self.objectArray.count;
}

// 返回指定列，行的高度，就是自定义行的高度
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    return 30;
}

// 返回指定列的宽度
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    return WIDTH - WidthRate(100);
}

// 自定义指定列的每行的视图，即指定列的每行的视图行一致
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *textLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(50), 0, WIDTH - WidthRate(100), 30)];
    textLab.backgroundColor = [UIColor clearColor];
    textLab.text = [self.objectArray objectAtIndex:row];
    textLab.textAlignment = NSTextAlignmentCenter;
    [view addSubview:textLab];
    
    return textLab;
}

//// 显示的标题
//- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
//{
//    NSString *titleString = [self.objectArray objectAtIndex:row];
//    
//    return titleString;
//}

// 显示的标题字体，颜色等属性
- (NSAttributedString *)pickerView:(UIPickerView *)pickerView attributedTitleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *titleString = [self.objectArray objectAtIndex:row];
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:titleString];
    [attributedString setAttributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:18.0f]} range:NSMakeRange(0, titleString.length)];
    
    return attributedString;
}

// 被选择的行
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    //NSLog(@"HHHHHHHHHH === %@", [self.objectArray objectAtIndex:row]);
    NSString *objectString = [NSString stringWithFormat:@"%@", [self.objectArray objectAtIndex:row]];
    NSString *rechargeIdStrig = [NSString stringWithFormat:@"%@", [self.rechargeIdArray objectAtIndex:row]];
    [self.rechargeDelegate changeRechargeMoney:objectString changeRechargeId:rechargeIdStrig];
}

- (void)finishedButtonClick:(UIButton *)sender
{
    if ([self.rechargeDelegate respondsToSelector:@selector(sureRechargeMoneySelected)])
    {
        [self.rechargeDelegate sureRechargeMoneySelected];
    }
}

- (void)rechargePickerViewDismiss:(UISwipeGestureRecognizer *)sender
{
    if ([self.rechargeDelegate respondsToSelector:@selector(cancelRechargeMoneySelected)])
    {
        [self.rechargeDelegate cancelRechargeMoneySelected];
    }
}


@end
