//
//  ZSTPersonalAboutViewController.m
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPersonalAboutViewController.h"
#import "PMRepairButton.h"
#import "ZSTAgreementViewController.h"
#import "ZSTTechnicalSupportVC.h"

@interface ZSTPersonalAboutViewController ()

@end

@implementation ZSTPersonalAboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.view.backgroundColor = [UIColor whiteColor];
    [self buildAboutView];
    [self buildBottomView];
    
    // 导航控制器全屏滑动返回上一页效果实现代码
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
}

- (void)buildAboutView
{
    UIImageView *aboutIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"mineAbout.jpg"]];
    aboutIV.userInteractionEnabled = YES;
    aboutIV.frame = CGRectMake(0, 0, WIDTH, WIDTH * 880 / 750 - (iPhone4s ? 15 : 0));
    //aboutIV.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:aboutIV];
    
    PMRepairButton *backBtn = [[PMRepairButton alloc] init];
    backBtn.frame = CGRectMake(WidthRate(20), 20 + WidthRate(20), WidthRate(60), WidthRate(60));
    [backBtn setImage:[UIImage imageNamed:@"aboutBack.png"] forState:UIControlStateNormal];
    [backBtn addTarget:self action:@selector(goToPrevious:) forControlEvents:UIControlEventTouchUpInside];
    [aboutIV addSubview:backBtn];
    
    UIImageView *logoIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon.png"]];
    logoIV.center = CGPointMake(WIDTH / 2, aboutIV.bounds.size.height / 2 - 40);
    logoIV.bounds = CGRectMake(0, 0, 90, 90);
    logoIV.layer.cornerRadius = 4.0f;
    [aboutIV addSubview:logoIV];
    
    logoIV.layer.shadowColor = [UIColor blackColor].CGColor;
    logoIV.layer.shadowOffset = CGSizeMake(0,0);
    logoIV.layer.shadowOpacity = 0.5;
    logoIV.layer.shadowRadius = 4.0;//给imageview添加阴影和边框
    
    NSString *appDisplayName = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
    UILabel *appNameLB = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(logoIV.frame) + 17, WIDTH, 20)];
    appNameLB.textAlignment = NSTextAlignmentCenter;
    appNameLB.font = [UIFont systemFontOfSize:16.0f];
    appNameLB.textColor = RGBA(43, 43, 43, 1);
    appNameLB.text = appDisplayName;
    [aboutIV addSubview:appNameLB];
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    UILabel *versionlabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(appNameLB.frame), WIDTH, 20)];
    versionlabel.textAlignment = NSTextAlignmentCenter;
    versionlabel.font = [UIFont systemFontOfSize:12.0f];
    versionlabel.textColor = RGBA(136, 136, 136, 1);
    versionlabel.text = [NSString stringWithFormat:@"v %@",version];
    [aboutIV addSubview:versionlabel];
}

- (void)buildBottomView
{
    NSString *settingAbout = [[[NSBundle mainBundle] infoDictionary] safeObjectForKey:@"setting_about"];
    NSArray *settingArr = [settingAbout componentsSeparatedByString:@","];
    UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake(15, HEIGHT - 115, WIDTH - 30, 105)];
    
    if (iPhone4s) {
        UIButton *serverBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        serverBtn.frame = CGRectMake(0, 0, 75, 25);
        [serverBtn setImage:[UIImage imageNamed:@"serviceImg.png"] forState:UIControlStateNormal];
        [bottomView addSubview:serverBtn];
        [serverBtn addTarget:self action:@selector(goToServer:) forControlEvents:UIControlEventTouchUpInside];
    }
    else {
        PMRepairButton *serverBtn = [[PMRepairButton alloc] init];
        serverBtn.frame = CGRectMake(0, 0, 75, 25);
        [serverBtn setImage:[UIImage imageNamed:@"serviceImg.png"] forState:UIControlStateNormal];
        [bottomView addSubview:serverBtn];
        [serverBtn addTarget:self action:@selector(goToServer:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    UILabel *powerLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 30, bottomView.bounds.size.width, 25)];
    powerLB.textColor = RGBA(136, 136, 136, 1);
    powerLB.font = [UIFont systemFontOfSize:12.0f];
    powerLB.textAlignment = NSTextAlignmentLeft;
    powerLB.text = [NSString stringWithFormat:@"版权所属 : %@",[settingArr objectAtIndex:1]];
    [bottomView addSubview:powerLB];
    
    
    NSString *technicalStr = @"技术支持 : ";
    CGSize stringSize = [technicalStr boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    UILabel *techLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 55, stringSize.width, 25)];
    techLB.textColor = RGBA(136, 136, 136, 1);
    techLB.font = [UIFont systemFontOfSize:12.0f];
    techLB.textAlignment = NSTextAlignmentLeft;
    techLB.text = technicalStr;
    [bottomView addSubview:techLB];
    
    NSString *companyStr = @"广州市加减信息技术有限公司";
    CGSize companySize = [companyStr boundingRectWithSize:CGSizeMake(MAXFLOAT, 30) options:NSStringDrawingTruncatesLastVisibleLine attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:12.0f]} context:nil].size;
    
    UILabel *companyLB = [[UILabel alloc] initWithFrame:CGRectMake(techLB.frame.origin.x + stringSize.width, 55, companySize.width, 25)];
    companyLB.textColor = RGBA(136, 136, 136, 1);
    companyLB.font = [UIFont systemFontOfSize:12.0f];
    companyLB.textAlignment = NSTextAlignmentLeft;
    companyLB.text = companyStr;
    [bottomView addSubview:companyLB];
    
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(companyLB.frame.origin.x, 75, companySize.width, 0.5f);
    layer.backgroundColor = RGBA(136, 136, 136, 1).CGColor;
    [bottomView.layer addSublayer:layer];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(technicalAction)];
    [companyLB addGestureRecognizer:tap];
    companyLB.userInteractionEnabled = YES;
    
    UIImageView *theEarthIma = [[UIImageView alloc] initWithFrame:CGRectMake(companyLB.frame.origin.x + companySize.width + 10, 57, 20, 20)];
    theEarthIma.image = [UIImage imageNamed:@"theEarth_image.png"];
    [bottomView addSubview:theEarthIma];
    
    UILabel *copyLB = [[UILabel alloc] initWithFrame:CGRectMake(0, 80, bottomView.bounds.size.width, 25)];
    copyLB.textColor = RGBA(136, 136, 136, 1);
    copyLB.font = [UIFont systemFontOfSize:12.0f];
    copyLB.textAlignment = NSTextAlignmentLeft;
    copyLB.text = @"Copyright ©2009-2015 All Rights Reserved";
    [bottomView addSubview:copyLB];
    
    [self.view addSubview:bottomView];
}

- (void)goToServer:(PMRepairButton *)sender
{
    ZSTAgreementViewController *controller = [[ZSTAgreementViewController alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
}

- (void)goToPrevious:(PMRepairButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)technicalAction
{
    ZSTTechnicalSupportVC *controller = [[ZSTTechnicalSupportVC alloc] init];
    controller.source = NO;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navController animated:YES completion:nil];
}

@end
