//
//  ZSTPersonInfoViewController.h
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"

@interface ZSTPersonInfoViewController : UIViewController

@property (strong,nonatomic) NSDictionary *infoDic;
@property (strong,nonatomic) ZSTF3Engine *engine;

@end
