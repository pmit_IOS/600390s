//
//  StringUtil.m
//  TuanGo
//
//  Created by simon on 11-1-21.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "StringUtil.h"


@implementation NSString(extend) 


+(NSString*)trim:(NSString*)string{
	
	NSString* returnStr = string;
	returnStr = [returnStr stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
	returnStr = [returnStr stringByReplacingOccurrencesOfString: @"\r" withString: @""];
	returnStr = [returnStr stringByReplacingOccurrencesOfString: @"\n" withString: @""];
	returnStr = [returnStr stringByReplacingOccurrencesOfString: @"\t" withString: @""];
	
	return returnStr;
}

-(BOOL)containString:(NSString*)string
{
	if([self rangeOfString:string].location != 2147483647){
		return YES;
	}
	return NO;
}

@end
