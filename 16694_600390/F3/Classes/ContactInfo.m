//
//  ContactInfo.m
//  HelloWorld
//
//  Created by huqinghe on 11-6-5.
//  Copyright 2011 Shinetechchina.com. All rights reserved.
//

#import "ContactInfo.h"


@implementation ContactInfo

@synthesize recordID = _recordID;
@synthesize name = _name;
@synthesize phoneNum = _phoneNum;

-(void) dealloc
{
	self.name = nil;
	self.phoneNum = nil;
    [super dealloc];
}

- (BOOL)isEqual:(id)object
{
    return [object isKindOfClass:[ContactInfo class]]
                                           && self.recordID ==((ContactInfo *)object).recordID 
                                           && self.name ==((ContactInfo *)object).name 
                                           && self.phoneNum == ((ContactInfo *)object).phoneNum;
}

@end
