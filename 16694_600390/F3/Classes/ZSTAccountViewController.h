//
//  ZSTAccountViewController.h
//  F3
//
//  Created by LiZhenQu on 14-10-27.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTAccountViewController : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
      NSMutableArray *settingItems;
      NSMutableArray *accountItems;
}

@property (nonatomic, retain) IBOutlet UITableView *tableView;

@property (nonatomic, assign) int passwordStatus;

@end
