//
//  PMMembersCardDetailViewController.m
//  F3
//
//  Created by P&M on 15/9/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMMembersCardDetailViewController.h"
#import "ZSTUtils.h"

@interface PMMembersCardDetailViewController () <UITextViewDelegate>

@property (strong, nonatomic) UITextView *contextTV;

@end

@implementation PMMembersCardDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:@"会员卡说明"];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:nil selector:@selector(popViewController)];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self getMembersCardDetails];
    
    [self createWebView];
}

- (void)createWebView
{
    UIView *oneView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, WidthRate(120))];
    oneView.backgroundColor = RGBA(170, 101, 62, 1);
    [self.view addSubview:oneView];
    
    UILabel *titleLab = [[UILabel alloc] initWithFrame:CGRectMake(WidthRate(20), WidthRate(10), WidthRate(350), WidthRate(100))];
    titleLab.backgroundColor = [UIColor clearColor];
    titleLab.text = @"会员卡说明";
    titleLab.textColor = [UIColor whiteColor];
    titleLab.textAlignment = NSTextAlignmentLeft;
    titleLab.font = [UIFont systemFontOfSize:24.0f];
    [oneView addSubview:titleLab];
    
    
    self.contextTV = [[UITextView alloc] initWithFrame:CGRectMake(0, oneView.frame.origin.y + oneView.frame.size.height, WIDTH, HEIGHT - 64 - oneView.frame.size.height)];
    self.contextTV.editable = NO;
    self.contextTV.backgroundColor = [UIColor clearColor];
    self.contextTV.textColor = [UIColor blackColor];
    self.contextTV.textAlignment = NSTextAlignmentLeft;
    self.contextTV.font = [UIFont systemFontOfSize:14.0f];
    [self.view addSubview:self.contextTV];
}

#pragma mark - 获取会员卡说明
- (void)getMembersCardDetails
{
    NSString *client = @"ios";
    NSString *ecid = @"600395";
    NSString *cardSetId = @"F70B35CDCD4B7476E040810A90115241";
    
    NSDictionary *param = @{@"client":client,@"ecid":ecid,@"cardSetId":cardSetId};
    
    [self.engine getMembersCardDetails:param];
}

// 获取会员卡说明成功
- (void)getMembersCardDetailsSucceed:(NSDictionary *)response
{
    self.contextTV.text = [response safeObjectForKey:@"data"];
}

- (void)getMembersCardDetailsFaild:(NSString *)response
{
    [TKUIUtil alertInWindow:response withImage:nil];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
