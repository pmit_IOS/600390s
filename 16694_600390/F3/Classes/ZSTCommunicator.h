//
//  ZSTCommunicator.h
//  
//
//  Created by luobin on 7/16/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//


#import <Foundation/Foundation.h>
#import "ASINetworkQueue.h"
#import "ZSTLegicyCommunicator.h"
#import "ZSTResponse.h"

//请求返回状态码
#define ZSTResultCode_Failure 0                //服务器处理请求失败
#define ZSTResultCode_OK 1                     //成功
#define ZSTResultCode_RequestTimedOut 2        //请求超时
#define ZSTResultCode_ConnectionFailure 3      //网络连接失败
#define ZSTResultCode_ParseJsonFailure 4       //json解析失败
#define ZSTResultCode_DataIllegal 5            //返回数据格式错误
#define ZSTResultCode_Other 6                  //其他错误

extern NSString * const ZSTHttpRequestErrorDomain;

/*
 *  json网络请求的封装，该类主要是达到以下两个目的：
 *  1.封装网络请求的细节，包括错误处理，日志纪录，json解析，通用参数设置，使用更加方便简单
 *  2.解藕业务代码和ASI网络请求框架，方便以后的升级和维护，随时可以替换掉ASI网络请求框架。
 */
@interface ZSTCommunicator : NSObject {
@private
    ASINetworkQueue *networkQueue;
    NSMutableSet *uploadRequestsSet;
}

SINGLETON_INTERFACE(ZSTCommunicator);

/**
 *	@brief	post信息请求接口
 *
 *	@param 	path         请求的URL地址
 *	@param 	params       请求参数
 *	@param 	target       请求回调消息的发送对象
 *	@param 	selector     请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 */
- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector;

/**
 *	@brief	get信息请求接口
 *
 *	@param 	path         请求的URL地址
 *	@param 	params       请求参数
 *	@param 	target       请求回调消息的发送对象
 *	@param 	selector     请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 *	@param 	userInfo
 *	@param 	matchCase    请求数据是否区分大小写（加密验证）no:不区分大小写，使用小写，yes：区分，使用大写
 */
- (void)openAPIGetToPath:(NSString *)path
                  params:(NSDictionary *)params
                  target:(id)target
                selector:(SEL)selector
                userInfo:(id)userInfo
               matchCase:(BOOL)matchCase;

/**
 *	@brief	post信息请求接口
 *
 *	@param 	path         请求的URL地址
 *	@param 	params       请求参数
 *	@param 	target       请求回调消息的发送对象
 *	@param 	selector     请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 *	@param 	userInfo  
 *	@param 	matchCase    请求数据是否区分大小写（加密验证）no:不区分大小写，使用小写，yes：区分，使用大写
 */
- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                   selector:(SEL)selector
                   userInfo:(id)userInfo
                   matchCase:(BOOL)matchCase;

/**
 *	@brief	post信息请求接口
 *
 *	@param 	path         请求的URL地址
 *	@param 	params       请求参数
 *	@param 	target       请求回调消息的发送对象
 *	@param 	selector     请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 *	@param 	userInfo
 */
- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector
                 userInfo:(id)userInfo;

/**
 *	@brief	post信息请求接口
 *
 *	@param 	path         请求的URL地址
 *	@param 	params       请求参数
 *	@param 	target       请求回调消息的发送对象
 *	@param 	selector     请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 *	@param 	userInfo
 *	@param 	longConnection 是否是长连接
 */
- (void)openAPIPostToPath:(NSString *)path
                   params:(NSDictionary *)params
                   target:(id)target
                 selector:(SEL)selector
                 userInfo:(id)userInfo
           longConnection:(BOOL)isLongCon;


/**
 *	@brief	文件上传接口
 *
 *	@param 	path                请求的URL地址
 *	@param 	filePath            要上传的文件在文件系统的位置
 *	@param 	target              请求回调消息的发送对象
 *	@param 	selector            请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 *	@param 	progressSelector    上传进度回调      暂未实现
 *	@param 	userInfo            
 */
- (void)uploadFileToPath:(NSString *)path
                filePath:(NSString *)filePath
                  target:(id)target
                selector:(SEL)selector
        progressSelector:(SEL)progressSelector
                userInfo:(id)userInfo; // progressDelegate is an UIProgressView, if needn't please set nil

/**
 *	@brief	文件上传接口
 *
 *	@param 	path                请求的URL地址
 *	@param 	data                要上传的二进制数据
 *	@param 	suffix              文件后缀
 *	@param 	target              请求回调消息的发送对象
 *	@param 	selector            请求回调       - (void)requestDidResponse:(ZSTResponse *)response userInfo:(id)userInfo
 *	@param 	progressSelector    上传进度回调      暂未实现
 *	@param 	userInfo
 */
- (void)uploadFileToPath:(NSString *)path
                    data:(NSData *)data
                  suffix:(NSString *)suffix
                  target:(id)target
                selector:(SEL)selector
        progressSelector:(SEL)progressSelector
                userInfo:(id)userInfo;

- (void)uploadFileTopath:(NSString *)path
                  params:(NSDictionary *)params
                    data:(NSData *)data
                  suffix:(NSString *)suffix
                  target:(id)target
                selector:(SEL)selector
                userInfo:(id)userInfo;

- (void)openAPIDownLoadToPath:(NSString *)path
                     filepath:(NSString *)filepath
                       target:(id)target
                     selector:(SEL)selector
                     userInfo:(id)userInfo;

- (void)openAPIPostToPaths:(NSString *)path
                    params:(NSDictionary *)params
                    target:(id)target
                  selector:(SEL)selector;

- (void)openAPIPostDCToPaths:(NSString *)path
                      params:(NSDictionary *)params
                      target:(id)target
                    selector:(SEL)selector;

- (void)openAPISNSAPostToPath:(NSString *)path
                       params:(NSDictionary *)params
                       target:(id)target
                     selector:(SEL)selector
                     userInfo:(id)userInfo;

- (void)openAPIPostMemberCardToPaths:(NSString *)path
                              params:(NSDictionary *)params
                              target:(id)target
                            selector:(SEL)selector;

// ############################ URL request methods ############################

// 放弃请求.
- (void)cancelByTarget:(id)target;
- (void)cancelUploadByTarget:(id)target;
- (void)cancelRequest:(NSURL *)url;//放弃单个请求

@end

#pragma mark -
@interface ZSTCommunicatorUserInfo : NSObject
@property (nonatomic, assign) id target;
@property (nonatomic, assign) SEL selector;
@property (nonatomic, retain) id userInfo;
@end

