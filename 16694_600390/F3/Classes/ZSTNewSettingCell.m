//
//  ZSTNewSettingCell.m
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewSettingCell.h"

@implementation ZSTNewSettingCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)createUI
{
    if (!self.titleLB)
    {
        self.titleLB = [[UILabel alloc] initWithFrame:CGRectMake(15, 10, WIDTH - 15 - 20, 25)];
        self.titleLB.font = [UIFont systemFontOfSize:14.0f];
        self.titleLB.textColor = RGBA(43, 43, 43, 1);
        self.titleLB.textAlignment = NSTextAlignmentLeft;
        [self.contentView addSubview:self.titleLB];

        self.checkBtn = [[PMRepairButton alloc] init];
        self.checkBtn.frame = CGRectMake(WIDTH - 15 - 25, 10, 25, 25);
        [self.checkBtn setImage:[UIImage imageNamed:@"unSelected.png"] forState:UIControlStateNormal];
        [self.checkBtn setImage:[UIImage imageNamed:@"selected"] forState:UIControlStateSelected];
        [self.contentView addSubview:self.checkBtn];
    }
}

- (void)setCellData:(NSString *)titleString IsCheck:(BOOL)isCheck
{
    self.titleLB.text = titleString;
    self.checkBtn.selected = isCheck;
}

@end
