//
//  ZSTPersonalInfo.h
//  F3
//
//  Created by LiZhenQu on 14-10-30.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

@property (nonatomic, retain) NSString *iconUrl;
@property (nonatomic, retain) NSString *userName;
@property (nonatomic, retain) NSString *Msisdn;
@property (nonatomic, retain) NSString *pointNum;
@property (nonatomic, assign) int birthday;
@property (nonatomic, assign) int sex;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *signature;
@property (nonatomic, assign) int signIn;
@property (nonatomic, assign) int phonePublic;

+ (UserInfo *)userInfoWithdic:(NSDictionary *)dic;

@end
