//
//  ZSTMsgSettingTableViewCell.h
//  InfantCloud
//
//  Created by LiZhenQu on 14-7-18.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTCustomSwitch.h"
#import "PMCustomSwitch.h"

@class ZSTMsgSettingTableViewCell;

@protocol MsgSettingCellDelegate <NSObject>
@optional
- (void) cell:(ZSTMsgSettingTableViewCell *)cell set:(int)states;
@end


@interface ZSTMsgSettingTableViewCell : UITableViewCell <PMCustomSwitchDelegate>

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, strong) IBOutlet UILabel *descriptionLabel;
//@property (nonatomic, strong) ZSTCustomSwitch *switchBtn;
@property (nonatomic,strong) PMCustomSwitch *switchBtn;

@property (nonatomic, strong) id<MsgSettingCellDelegate>delegate;

@end
