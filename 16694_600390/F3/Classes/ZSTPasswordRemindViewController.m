//
//  ZSTPasswordRemindViewController.m
//  F3
//
//  Created by LiZhenQu on 14/11/29.
//  Copyright (c) 2014年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTPasswordRemindViewController.h"
#import "ZSTF3RegisterViewController.h"

@interface ZSTPasswordRemindViewController ()

@end

@implementation ZSTPasswordRemindViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.bgImeView.image = ZSTModuleImage(@"module_login_bg.jpg");
    self.bgImeView.alpha = 0;
    
    UIImage *imageN = ZSTModuleImage(@"module_login_btn_green_n.png");
    UIImage *ImageP = ZSTModuleImage(@"module_login_btn_green_p.png");
    CGFloat capWidth = imageN.size.width / 2;
    CGFloat capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.setbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.setbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];
    
    imageN = ZSTModuleImage(@"module_personal_btn_grey_n.png");
    ImageP = ZSTModuleImage(@"module_personal_btn_grey_p.png");
    capWidth = imageN.size.width / 2;
    capHeight = imageN.size.height / 2;
    imageN = [imageN stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    ImageP = [ImageP stretchableImageWithLeftCapWidth:capWidth topCapHeight:capHeight];
    [self.laterbtn setBackgroundImage:imageN forState:UIControlStateNormal];
    [self.laterbtn setBackgroundImage:ImageP forState:UIControlStateHighlighted];

    _avaterImgView.layer.borderWidth = 1;
    _avaterImgView.layer.cornerRadius = _avaterImgView.frame.size.width / 2;
    _avaterImgView.layer.borderColor = [UIColor clearColor].CGColor;
    _avaterImgView.layer.masksToBounds = YES;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.navigationController.navigationBarHidden = YES;
    
    NSDictionary *dic = [ZSTF3Preferences shared].avaterName;
    NSString *path = nil;
    
    if (![ZSTF3Preferences shared].loginMsisdn || [ZSTF3Preferences shared].loginMsisdn.length == 0) {
        
        [ZSTF3Preferences shared].loginMsisdn = @"";
    }
   
    if (dic && [dic isKindOfClass:[NSDictionary class]]) {
        path = [dic safeObjectForKey:[ZSTF3Preferences shared].loginMsisdn];
    }
    
    if (path && path.length > 0) {
        
        NSData *reader = [NSData dataWithContentsOfFile:path];
        UIImage *image = [UIImage imageWithData:reader];
        _avaterImgView.image = image;
    }
    
    if (!_avaterImgView.image || path == nil) {
        
        _avaterImgView.image = [UIImage imageNamed:@"module_personal_avater_deafaut.png"];
    }
    
    self.phoneNumLabel.text = [ZSTF3Preferences shared].loginMsisdn;
}

- (IBAction)clickAction:(id)sender
{
    UIButton *button = (UIButton *)sender;
    
    if (button.tag == 0) {
        
        ZSTF3RegisterViewController *controller = [[ZSTF3RegisterViewController alloc] init];
        controller.type = 4;
        controller.phoneNum = [ZSTF3Preferences shared].loginMsisdn;
        [self.navigationController pushViewController:controller animated:YES];
        [controller release];
        
    } else if (button.tag == 1) {
        
        [self.navigationController dismissViewControllerAnimated:YES completion:nil];
    }
    
     [[NSUserDefaults standardUserDefaults] setValue:[NSNumber numberWithBool:YES] forKey:FIRST_LAUNCH_SETTING];
     [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) dealloc
{
    self.avaterImgView = nil;
    self.setbtn = nil;
    self.laterbtn = nil;
    self.bgImeView = nil;
    [super dealloc];
}

@end
