//
//  ZSTBirthPickerView.h
//  F3
//
//  Created by pmit on 15/8/27.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  ZSTBirthPickerViewDelegate <NSObject>

- (void)sureBirth:(NSString *)birthString;
- (void)cancelBirthPicker;

@end

@interface ZSTBirthPickerView : UIView <UIPickerViewDataSource,UIPickerViewDelegate>

@property (strong,nonatomic) UIPickerView *dateTimePicker;
@property (assign,nonatomic) NSInteger nowYear;
@property (assign,nonatomic) NSInteger nowMonth;
@property (assign,nonatomic) NSInteger nowDay;

@property (assign,nonatomic) NSInteger selectedYear;
@property (assign,nonatomic) NSInteger selectedMonth;
@property (assign,nonatomic) NSInteger selectedDay;
@property (strong,nonatomic) NSArray *yearArr;
@property (weak,nonatomic) id<ZSTBirthPickerViewDelegate> birthDelegate;


- (void)createPickerView;
- (void)showDefaultShow;

@end
