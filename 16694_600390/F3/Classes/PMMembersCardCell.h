//
//  PMMembersCardCell.h
//  F3
//
//  Created by P&M on 15/9/10.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMMembersCardCell : UITableViewCell

// 会员卡列表标题
@property (strong, nonatomic) UILabel *titleLabel;

// 会员余额
@property (strong, nonatomic) UILabel *balanceLab;

// 充值送积分
@property (strong, nonatomic) UILabel *integralLab;


// 创建会员卡列表 UI
- (void)createTitleUI;

@end
