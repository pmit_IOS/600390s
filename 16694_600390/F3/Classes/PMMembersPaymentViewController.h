//
//  PMMembersPaymentViewController.h
//  F3
//
//  Created by P&M on 15/9/14.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PMMembersPaymentViewController : UIViewController

@property (copy, nonatomic) NSString *urlString;

@end
