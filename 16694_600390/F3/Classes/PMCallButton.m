//
//  PMCallButton.m
//  F3
//
//  Created by pmit on 15/5/29.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "PMCallButton.h"

@implementation PMCallButton
{
    UIImageView *norIv;
    UIImageView *hiIv;
}

- (void)setImage:(UIImage *)image forState:(UIControlState)state
{
    //首先执行super方法，保证imageView有创建
    [super setImage:image forState:state];
    
    if(state == UIControlStateNormal)
    {
        if(norIv == nil)
        {
            norIv = [[[UIImageView alloc] initWithFrame:self.imageView.frame] autorelease];
            norIv.contentMode = UIViewContentModeScaleAspectFit;
            [self insertSubview:norIv aboveSubview:self.imageView];
        }
        norIv.image = image;
    }
    else if(state == UIControlStateHighlighted)
    {
        if(hiIv == nil)
        {
            hiIv = [[[UIImageView alloc] initWithFrame:self.imageView.frame] autorelease];
            [self insertSubview:hiIv aboveSubview:self.imageView];
        }
        hiIv.image = image;
        hiIv.hidden = YES;
    }
    
}

- (CGRect)titleRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(contentRect.size.width * 0.2, 0, contentRect.size.width * 0.8, contentRect.size.height);
}

- (CGRect)imageRectForContentRect:(CGRect)contentRect
{
    return CGRectMake(0, contentRect.size.height * 0.2, contentRect.size.width * 0.2, contentRect.size.height * 0.6);
}

@end
