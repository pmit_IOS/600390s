//
//  ZSTNewRegisterViewController.m
//  F3
//
//  Created by pmit on 15/8/24.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTNewRegisterViewController.h"
#import "PMRepairButton.h"
#import "ZSTRegisterPassViewController.h"

#define trueNum(num) (num + 64)

@interface ZSTNewRegisterViewController () <UITextFieldDelegate>

@property (strong,nonatomic) UIScrollView *rScrollView;
@property (strong,nonatomic) UITextField *phoneTF;
@property (strong,nonatomic) UITextField *verfiCodeTF;
@property (strong,nonatomic) UITextField *smsCodeTF;
@property (strong,nonatomic) NSTimer *btnTimer;
@property (assign,nonatomic) NSInteger downCount;
@property (strong,nonatomic) PMRepairButton *smsBtn;

@end

@implementation ZSTNewRegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.registerType == ZSTRegisterTypeRegister)
    {
        self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"注册", nil)];
    }
    else if (self.registerType == ZSTRegisterTypeFindPass)
    {
        self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"找回密码", nil)];
    }
    else if (self.registerType == ZSTRegisterTypeChangePhoe)
    {
        self.navigationItem .titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"更改手机号", nil)];
    }
    else if (self.registerType == ZSTRegisterTypeChangePass)
    {
        self.navigationItem .titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"更改密码", nil)];
    }
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = NO;
    self.navigationController.navigationBar.backgroundImage = [UIImage imageNamed:@"framework_top_bg.png"];
    //self.navigationItem.leftBarButtonItem =  [TKUIUtil backItemForNavigationWithTitle:NSLocalizedString(@"返回", @"") target:self selector:@selector (popViewController)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    self.rScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, WIDTH, HEIGHT - 64)];
    self.rScrollView.showsHorizontalScrollIndicator = NO;
    self.rScrollView.showsVerticalScrollIndicator = NO;
    self.rScrollView.contentSize = CGSizeMake(0, 240);
    self.rScrollView.userInteractionEnabled = YES;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapResignFirst:)];
    [self.rScrollView addGestureRecognizer:tap];
    
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
    
    [self.view addSubview:self.rScrollView];
    [self buildRegisterView];
    [self buildInputView];
    [self buildBtnView];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if (self.registerType == ZSTRegisterTypeChangePhoe)
    {
        self.phoneTF.text = self.mobileString;
    }
}

- (void)buildRegisterView
{
    UIImageView *statusIV = [[UIImageView alloc] initWithFrame:CGRectMake(25, 35, WIDTH - 50, 10)];
    statusIV.contentMode = UIViewContentModeScaleAspectFit;
    statusIV.image = [UIImage imageNamed:@"statusOne.png"];
    [self.rScrollView addSubview:statusIV];
    
    UIImageView *numIV = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"numBorder"]];
    numIV.center = CGPointMake(WIDTH / 2, 22);
    numIV.bounds = CGRectMake(0, 0, 35, 25);
    [self.rScrollView addSubview:numIV];
    
    PMRepairButton *numBtn = [[PMRepairButton alloc] initWithFrame:CGRectMake(0, 0, 35, 20)];
    numBtn.titleLabel.font = [UIFont systemFontOfSize:12.0f];
    [numBtn setTitle:@"50%" forState:UIControlStateNormal];
    [numBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [numIV addSubview:numBtn];
}

- (void)buildInputView
{
    UIView *inputView = [[UIView alloc] initWithFrame:CGRectMake(15, 80, WIDTH - 50, 91)];
    
    UIImageView *phoneIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    phoneIcon.image = [UIImage imageNamed:@"rPhone.png"];
    phoneIcon.contentMode = UIViewContentModeScaleAspectFit;
    [inputView addSubview:phoneIcon];
    
    self.phoneTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 5, inputView.bounds.size.width - 40, 30)];
    self.phoneTF.placeholder = @"请点击此输入手机号码";
    self.phoneTF.delegate = self;
    self.phoneTF.tag = 1;
    self.phoneTF.keyboardType = UIKeyboardTypeNumberPad;
    self.phoneTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.phoneTF setValue:[UIFont boldSystemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];
    [self.phoneTF setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, self.phoneTF.bounds.size.width, self.phoneTF.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    [inputView addSubview:self.phoneTF];
    
    
    CALayer *line1 = [CALayer layer];
    line1.backgroundColor = RGBA(234, 234, 234, 1).CGColor;
    line1.frame = CGRectMake(10, 40, inputView.size.width, 0.5);
    [inputView.layer addSublayer:line1];
    
//    UIImageView *eyeIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 65.5, 20, 20)];
//    eyeIcon.image = [UIImage imageNamed:@"rEye.png"];
//    eyeIcon.contentMode = UIViewContentModeScaleAspectFit;
//    [inputView addSubview:eyeIcon];
//    
//    self.verfiCodeTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 62.5, inputView.size.width - 40, 30)];
//    self.verfiCodeTF.placeholder = @"输入验证码";
//    self.verfiCodeTF.delegate = self;
//    [self.verfiCodeTF setValue:[UIFont boldSystemFontOfSize:15] forKeyPath:@"_placeholderLabel.font"];
//    [self.verfiCodeTF setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, self.verfiCodeTF.bounds.size.width, self.verfiCodeTF.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
//    [inputView addSubview:self.verfiCodeTF];
//    
//    
//    CALayer *line2 = [CALayer layer];
//    line2.backgroundColor = RGBA(234, 234, 234, 1).CGColor;
//    line2.frame = CGRectMake(0, 100.5, inputView.size.width, 0.5);
//    [inputView.layer addSublayer:line2];
    
    UIImageView *smsIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 55.5, 20, 20)];
    smsIcon.image = [UIImage imageNamed:@"rMail.png"];
    smsIcon.contentMode = UIViewContentModeScaleAspectFit;
    [inputView addSubview:smsIcon];
    
    self.smsCodeTF = [[UITextField alloc] initWithFrame:CGRectMake(40, 51.5, inputView.bounds.size.width - 40 - 80, 30)];
    self.smsCodeTF.placeholder = @"请点击此输入验证码";
    self.smsCodeTF.delegate = self;
    self.smsCodeTF.tag = 2;
    self.smsCodeTF.clearButtonMode = UITextFieldViewModeWhileEditing;
    [self.smsCodeTF setValue:[UIFont boldSystemFontOfSize:14] forKeyPath:@"_placeholderLabel.font"];
    [self.smsCodeTF setValue:[NSValue valueWithCGRect:CGRectMake(0, 0, self.smsCodeTF.bounds.size.width, self.smsCodeTF.bounds.size.height)] forKeyPath:@"_placeholderLabel.frame"];
    self.smsCodeTF.keyboardType = UIKeyboardTypeNumberPad;
    [inputView addSubview:self.smsCodeTF];
    
    self.smsBtn = [[PMRepairButton alloc] init];
    self.smsBtn.frame = CGRectMake(inputView.bounds.size.width - 80, 50.5, 80, 30);
    [self.smsBtn setBackgroundImage:[UIImage imageNamed:@"colorBorder.png"] forState:UIControlStateNormal];
    [self.smsBtn setTitle:@"获取验证码" forState:UIControlStateNormal];
    self.smsBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [self.smsBtn setTitleColor:RGBA(248, 145, 70, 1) forState:UIControlStateNormal];
    [self.smsBtn addTarget:self action:@selector(getSMSCode:) forControlEvents:UIControlEventTouchUpInside];
    [inputView addSubview:self.smsBtn];
    
    
    CALayer *line3 = [CALayer layer];
    line3.backgroundColor = RGBA(234, 234, 234, 1).CGColor;
    line3.frame = CGRectMake(10, 90.5, inputView.size.width, 0.5);
    [inputView.layer addSublayer:line3];
    
    
    [self.rScrollView addSubview:inputView];
}

- (void)buildBtnView
{
    UIButton *sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    sureBtn.center = CGPointMake(WIDTH / 2, 220);
    sureBtn.bounds = CGRectMake(0, 0, WIDTH - 50, 40);
    [sureBtn setBackgroundColor:RGBA(244, 125, 54, 1)];
    [sureBtn setTitle:@"确  定" forState:UIControlStateNormal];
    [sureBtn addTarget:self action:@selector(sureRegister:) forControlEvents:UIControlEventTouchUpInside];
    [self.rScrollView addSubview:sureBtn];
}


- (void)tapResignFirst:(UITapGestureRecognizer *)tap
{
    [self.phoneTF resignFirstResponder];
    [self.verfiCodeTF resignFirstResponder];
    [self.smsCodeTF resignFirstResponder];
}


- (void)sureRegister:(UIButton *)sender
{
    if ([self.phoneTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有输入验证码", nil) withImage:nil];
    }
    else if ([self.smsCodeTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有输入验证码", nil) withImage:nil];
        
    }
    else
    {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在验证验证码...", nil) withImage:nil];
        NSInteger operType = 0;
        switch (self.registerType) {
            case ZSTRegisterTypeRegister:
                operType = 1;
                break;
            case ZSTRegisterTypeFindPass:
                operType = 2;
                break;
            case ZSTRegisterTypeChangePhoe:
                operType = 3;
                break;
            case ZSTRegisterTypeChangePass:
                operType = 2;
                break;
                
            default:
                break;
        }
        
        [self.engine checkVerificationCodeWithMsisdn:self.phoneTF.text verificationCode:self.smsCodeTF.text OperType:operType];
    }
}

#pragma mark - textField delegate
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.phoneTF.tag == 1 && self.phoneTF == textField) {
        NSInteger maxLength = 11;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    if (self.smsCodeTF.tag == 2 && self.smsCodeTF == textField) {
        NSInteger maxLength = 6;
        NSInteger strLength = textField.text.length - range.length + string.length;
        
        return (strLength <= maxLength);
    }
    
    return YES;
}

- (void)getSMSCode:(PMRepairButton *)sender
{
    if ([self.phoneTF.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入手机号码", nil) withImage:nil];
    }
    else if (![ZSTUtils checkMobile:self.phoneTF.text])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"请输入正确的手机号码", nil) withImage:nil];
    }
    else
    {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在获取验证码,请稍后", nil) withImage:nil];
        
        NSInteger operType = 0;
        switch (self.registerType) {
            case ZSTRegisterTypeRegister:
                operType = 1;
                break;
            case ZSTRegisterTypeFindPass:
                operType = 2;
                break;
            case ZSTRegisterTypeChangePhoe:
                operType = 3;
                break;
            case ZSTRegisterTypeChangePass:
                operType = 2;
                break;
                
            default:
                break;
        }
        
        [self.engine getVerificationCodeWithMsisdn:self.phoneTF.text OperType:operType];
        self.downCount = 60;
        self.btnTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(changeTimerNum) userInfo:nil repeats:YES];
        [self.btnTimer fire];
    }
}

- (void)getVerificationCodeDidSucceed:(NSDictionary *)response
{
    [TKUIUtil hiddenHUD];
}

- (void)getVerificationCodeDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(response, nil) withImage:nil];
}

- (void)changeTimerNum
{
    if (self.downCount > 0) {
        [self.smsBtn setTitle:[NSString stringWithFormat:@"%@",@(self.downCount)] forState:UIControlStateDisabled];
        [self.smsBtn setEnabled:NO];
        self.downCount--;
    }else {
        [self.smsBtn setTitle:@"重  发" forState: UIControlStateNormal];
        [self.smsBtn setEnabled:YES];
        [self.btnTimer invalidate];
    }
}

- (void)checkVerificationCodeDidSucceed:(NSDictionary *)response
{
    
    [TKUIUtil hiddenHUD];
    
    if (self.registerType == ZSTRegisterTypeChangePhoe)
    {
        [self.engine updateMsisdnWithMsisdn:self.phoneTF.text userId:[ZSTF3Preferences shared].UserId verificationCode:self.smsCodeTF.text OperType:3];
    }
    else
    {
        ZSTRegisterPassViewController *registerPassVC = [[ZSTRegisterPassViewController alloc] init];
        registerPassVC.smsCodeString = self.smsCodeTF.text;
        registerPassVC.phoneString = self.phoneTF.text;
        if (self.registerType == ZSTRegisterTypeFindPass)
        {
            registerPassVC.registerType = ZSTRegisterTypeSecondFindPass;
        }
        else if (self.registerType == ZSTRegisterTypeChangePass)
        {
            registerPassVC.registerType = ZSTRegisterTypeSecondChangePass;
        }
        else if (self.registerType == ZSTRegisterTypeRegister)
        {
            registerPassVC.registerType = ZSTRegisterTypeRegister;
        }
        
        
        //    registerPassVC.isRegister = self.isRegister;
        [self.navigationController pushViewController:registerPassVC animated:YES];
    }
    
}

- (void)checkVerificationCodeDidFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(response, nil) withImage:nil];
}

@end
