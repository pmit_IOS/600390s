//
//  ZSTSignViewController.m
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTSignViewController.h"
#import "ZSTUtils.h"

@interface ZSTSignViewController () <UITextViewDelegate,ZSTF3EngineDelegate>

@property (strong,nonatomic) UITextView *inputTextView;
@property (strong,nonatomic) UILabel *inputPlaceLB;

@end

@implementation ZSTSignViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *message = @"";
    switch (self.inputType)
    {
        case ZSTInputTypeAddress:
            message = @"所在地";
            break;
        case ZSTInputTypeCompany:
            message = @"公司";
            break;
        case ZSTInputTypeHometown:
            message = @"家乡";
            break;
        case ZSTInputTypeJob:
            message = @"职业";
            break;
        case ZSTInputTypeName:
            message = @"用户名";
            break;
        case ZSTInputTypeSchool:
            message = @"学校";
            break;
        case ZSTInputTypeSign:
            message = @"前面";
            break;
        default:
            break;
    }
    
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(message, nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
    
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"保存" style:UIBarButtonItemStyleDone target:self action:@selector(saveSign:)];
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(0, 0, 30, 20);
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [saveBtn addTarget:self action:@selector(saveSign:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tvResign:)];
    [self.view addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    [self buildTVView];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.inputTextView becomeFirstResponder];
}

- (void)buildTVView
{
    self.inputTextView = [[UITextView alloc] initWithFrame:CGRectMake(15, 15, WIDTH - 30, 60)];
    self.inputTextView.font = [UIFont systemFontOfSize:12.0f];
    self.inputTextView.textColor = RGBA(136, 136, 136, 1);
    self.inputTextView.delegate = self;
    
    self.inputPlaceLB = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, WIDTH - 30, 20)];
    self.inputPlaceLB.text = @"请在此输入...";
    self.inputPlaceLB.font = [UIFont systemFontOfSize:12.0f];
    self.inputPlaceLB.textColor = RGBA(204, 204, 204, 1);
    [self.inputTextView addSubview:self.inputPlaceLB];
    
    
    if (!self.signString || [self.signString isEqualToString:@"未设置"])
    {
        self.inputPlaceLB.hidden = NO;
    }
    else
    {
        self.inputPlaceLB.hidden = YES;
        self.inputTextView.text = self.signString;
    }
    
    [self.view addSubview:self.inputTextView];
}

- (void)saveSign:(UIBarButtonItem *)sender
{
    [self.inputTextView resignFirstResponder];
    
    if ([self.inputTextView.text isEqualToString:@""])
    {
        [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"您还没有填写信息呀！", nil) withImage:nil];
    }
    else
    {
        [self.inputTextView resignFirstResponder];
        
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"正在修改,请稍后...", nil) withImage:nil];
        if (self.inputType == ZSTInputTypeAddress)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Address":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        else if (self.inputType == ZSTInputTypeSign)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Signature":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        else if (self.inputType == ZSTInputTypeName)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Name":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        else if (self.inputType == ZSTInputTypeJob)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Occupation":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        else if (self.inputType == ZSTInputTypeCompany)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Company":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        else if (self.inputType == ZSTInputTypeSchool)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"School":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        else if (self.inputType == ZSTInputTypeHometown)
        {
            NSDictionary *param = @{@"Msisdn":[ZSTF3Preferences shared].loginMsisdn,@"UserId":[ZSTF3Preferences shared].UserId,@"Hometown":self.inputTextView.text};
            [self.engine changeUserInfoWithParam:param];
        }
        

    }
}

-(void)textViewDidChange:(UITextView *)textView
{
//    self.examineText =  textView.text;
    if (textView.text.length == 0) {
        
        self.inputPlaceLB.hidden = NO;
        
    }else{
        
        self.inputPlaceLB.hidden = YES;
    }
}

- (void)tvResign:(UITapGestureRecognizer *)tap
{
    [self.inputTextView resignFirstResponder];
}


- (void)changeUserInfoSucceed:(NSDictionary *)reponse
{
    [TKUIUtil hiddenHUD];
    if ([self.signDelegate respondsToSelector:@selector(saveCallBack:ContentString:)])
    {
        [self.signDelegate saveCallBack:self.inputType ContentString:self.inputTextView.text];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

- (void)changeUserInfoFailed:(NSString *)response
{
    [TKUIUtil hiddenHUD];
    [TKUIUtil alertInView:self.view withTitle:NSLocalizedString(@"很抱歉,更新失败了", nil) withImage:nil];
}

@end
