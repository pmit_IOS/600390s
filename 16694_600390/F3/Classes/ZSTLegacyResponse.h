//
//  ZSTLegacyResponse.h
//  
//
//  Created by luobin on 4/19/12.
//  Copyright 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASIHTTPRequest;
@class DocumentRoot;

/**
 *	@brief	响应封装，处理响应错误、记录联网日志
 */
@interface ZSTLegacyResponse : NSObject
{
    
}

@property (nonatomic, retain, readonly) ASIHTTPRequest *request;
@property (nonatomic, retain, readonly) id userInfo;
@property (nonatomic, retain, readonly) NSError *error;
@property (nonatomic, retain, readonly) NSString *type;
@property (nonatomic, retain, readonly) NSString *stringResponse;
@property (nonatomic, retain, readonly) DocumentRoot *xmlResponse;
@property (nonatomic, retain, readonly) id jsonResponse;

+ (id)responseWithXmlResponse:(DocumentRoot *)xmlResponse 
               stringResponse:(NSString *)stringResponse 
                        error:(NSError *)error 
                     userInfo:(id)userInfo;

+ (id)responseWithJSONResponse:(id)jsonResponse 
               stringResponse:(NSString *)stringResponse 
                        error:(NSError *)error 
                     userInfo:(id)userInfo;

- (id)initWithRequest:(ASIHTTPRequest *)request;

+ (id)responseWithRequest:(ASIHTTPRequest *)request;

- (void)loadInfoFromRequest:(ASIHTTPRequest *)request;

@end
