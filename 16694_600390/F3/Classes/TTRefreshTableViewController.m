//
//  TTRefreshTableViewController.m
//  TravelGuide
//
//  Created by Ma Jianglin on 12/27/12.
//  Copyright (c) 2012 Ma Jianglin. All rights reserved.
//

#import "TTRefreshTableViewController.h"

@interface TTRefreshTableViewController ()


@end

@implementation TTRefreshTableViewController

@synthesize tableView,dataArray,moreCell,topbar,categories;


- (void)loadDataForPage:(int)pageIndex
{
    TTLog(@"");
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _hasMore = NO;

    if (self.topbar == nil) {
        ColumnBar * bar = [[ColumnBar alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 38)];
        bar.topbarType = TopBarType_Slider;
        bar.dataSource = self;
        bar.delegate = self;
        bar.leftCapImage = ZSTModuleImage(@"module_newsb_leftCapImage.png");
        bar.rightCapImage = ZSTModuleImage(@"module_newsb_rightCapImage.png");
        bar.moverImage = ZSTModuleImage(@"module_newsb_category_selected.png");
        bar.backgroundColor = [UIColor colorWithWhite:0.88 alpha:1];
        self.topbar = bar;
        [self.view addSubview:self.topbar];
        self.topbar.hidden = NO;
    }
    
    if (self.tableView == nil)
    {
        UITableView *list = [[UITableView alloc] initWithFrame:CGRectMake(0, self.topbar.hidden ? 0 : self.topbar.size.height, self.view.size.width, self.view.frame.size.height-(self.topbar.hidden ? 0 : self.topbar.size.height)) style:UITableViewStylePlain];
        list.delegate = self;
        list.dataSource = self;
        self.tableView = list;
        self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
        [self.view addSubview:self.tableView];
	}
    
    if (_refreshHeaderView == nil)
    {
		EGORefreshTableHeaderView *view = [[EGORefreshTableHeaderView alloc] initWithFrame:CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height)];
		view.delegate = self;
		[self.tableView addSubview:view];
		_refreshHeaderView = view;
	}
	
	[_refreshHeaderView refreshLastUpdatedDate];
}

- (void)reloadView
{
    self.tableView.frame = CGRectMake(0, self.topbar.hidden ? 0 : self.topbar.size.height, self.view.size.width, self.view.frame.size.height-(self.topbar.hidden ? 0 : self.topbar.size.height));
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    _refreshHeaderView.frame = CGRectMake(0.0f, 0.0f - self.tableView.bounds.size.height, self.view.frame.size.width, self.tableView.bounds.size.height);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - ColumnBarDataSource

- (NSInteger)numberOfTabsInColumnBar:(ColumnBar *)columnBar //tab的数量
{
    return [self.categories count];
}

- (NSString *)columnBar:(ColumnBar *)columnBar titleForTabAtIndex:(int)index // tab的名称
{
    return nil;
}

#pragma mark - ColumnBarDelegate

- (void)columnBar:(ColumnBar *)columnBar didSelectedTabAtIndex:(NSInteger)index
{

}

#pragma mark - UITableView Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (_hasMore)
    {
        return self.dataArray.count + 1;  //有更多按钮
    }
    else
    {
        return self.dataArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

- (UITableViewCell*)moreCell
{
    if (moreCell == nil)
    {
        self.moreCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MoreCell"];
        _moreButton = [MoreButton button];
        [_moreButton addTarget:self action:@selector(loadMore) forControlEvents:UIControlEventTouchUpInside];
        [self.moreCell.contentView addSubview:_moreButton];
    }
    
    return moreCell;
}

#pragma mark -
#pragma mark Data Source Loading / Reloading Methods

//顶上下拽刷新，开始加载第1页数据
- (void)reloadTableViewDataSource
{
    //加载第1页
	_loading = YES;
    _pageIndex = 1;
	[self loadDataForPage:_pageIndex];
}

//更多按钮点击，或者底部上拽加载更多
- (void)loadMore
{
    if (_hasMore)
    {
        _loading = YES;
        [_moreButton displayIndicator];
        
        [self loadDataForPage:_pageIndex+1];
    }
}

- (void)aotuLoadData
{
    [_refreshHeaderView autoRefreshOnScroll:self.tableView animated:NO];

}

//完成加载数据
- (void)doneLoadingData
{
    _loading = NO;
    [_moreButton hideIndicator];
    [_refreshHeaderView egoRefreshScrollViewDataSourceDidFinishedLoading:self.tableView];
    [self.tableView reloadData];
}

//这个是处理数据条数不够一屏的情况
- (float)tableViewHeight
{
    if (self.tableView.contentSize.height < self.tableView.frame.size.height)
    {
        return self.tableView.frame.size.height;
    }
    else
    {
        return self.tableView.contentSize.height;
    }
}

//这个是处理数据条数不够一屏的情况
- (float)endOfTableView:(UIScrollView *)scrollView
{
    return [self tableViewHeight] - scrollView.bounds.size.height - scrollView.bounds.origin.y;
}


#pragma mark -
#pragma mark UIScrollViewDelegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [_refreshHeaderView egoRefreshScrollViewDidScroll:scrollView];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self endOfTableView:scrollView] <= -65.0f && !_loading)
    {
        [self loadMore];
    }
    else
    {
        [_refreshHeaderView egoRefreshScrollViewDidEndDragging:scrollView];
    }
}


#pragma mark -
#pragma mark EGORefreshTableHeaderDelegate Methods


- (void)egoRefreshTableHeaderDidTriggerRefresh:(EGORefreshTableHeaderView*)view
{
	[self reloadTableViewDataSource];
}

- (BOOL)egoRefreshTableHeaderDataSourceIsLoading:(EGORefreshTableHeaderView*)view
{
	return _loading;
}

- (NSDate*)egoRefreshTableHeaderDataSourceLastUpdated:(EGORefreshTableHeaderView*)view
{
	return [NSDate date];
}

@end
