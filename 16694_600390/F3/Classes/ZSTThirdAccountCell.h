//
//  ZSTThirdAccountCell.h
//  F3
//
//  Created by P&M on 15/8/12.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZSTThirdAccountCell : UITableViewCell

@property (strong, nonatomic) UILabel *titleLab;
@property (strong, nonatomic) UILabel *stateLab;

- (void)createThirdAccountUI;

- (void)setThirdAccountTitle:(NSString *)title state:(NSString *)state;

@end
