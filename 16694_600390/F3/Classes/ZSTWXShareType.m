//
//  ZSTWXShareType.m
//  F3
//
//  Created by pmit on 15/8/28.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTWXShareType.h"

static ZSTWXShareType *instance;

@implementation ZSTWXShareType

+ (ZSTWXShareType *)shareInstance
{
    if (!instance)
    {
        instance = [[ZSTWXShareType alloc] init];
    }
    
    return instance;
}



@end
