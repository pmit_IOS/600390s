//
//  ZSTModuleApplication.m
//  F3
//
//  Created by luobin on 12-10-30.
//  Copyright (c) 2012年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTModuleApplication.h"
#import "ZSTModuleDelegate.h"
#import "ZSTDao.h"

@implementation ZSTModuleApplication

@synthesize delegate;
@synthesize module;
@synthesize launchOptions;
@synthesize moduleType;
@synthesize dao;

- (id)initWithModule:(ZSTModule *)theModule
{
    self = [super init];
    if (self) {
        self.module = theModule;

        self.delegate = [[[self.module.entryPoint alloc] init] autorelease];
    }
    return self;
}

- (void)dealloc
{
    self.launchOptions = nil;
    self.dao = nil;
    self.module = nil;
    [super dealloc];
}

- (UIViewController *)launchWithOptions:(NSDictionary *)theLaunchOptions;
{
    self.delegate.application = self;
    if ([self.delegate respondsToSelector:@selector(moduleApplication:willFinishLaunchingWithOptions:)]) {
        [self.delegate moduleApplication:self willFinishLaunchingWithOptions:launchOptions];
    }
    self.launchOptions = theLaunchOptions;
    self.moduleType = [[launchOptions safeObjectForKey:@"type"] integerValue];
    if (self.moduleType == 0) {
        self.moduleType = self.module.defaultModuleType;
    }
    self.dao = [ZSTDao daoWithModuleType:self.moduleType];
    if ([self.delegate respondsToSelector:@selector(moduleApplication:didFinishLaunchingWithOptions:)]) {
        [self.delegate moduleApplication:self didFinishLaunchingWithOptions:launchOptions];
    }
    return self.delegate.rootViewController;
}

- (UIViewController *)moduleSettingViewController
{
    return [[[self.module.settingEntryPoint alloc] init] autorelease];;
}


- (UIView *)launchViewWithOptions:(NSDictionary *)theLaunchOptions
{
    self.delegate.application = self;
    
    if ([self.delegate respondsToSelector:@selector(moduleApplication:willFinishLaunchingWithOptions:)]) {
        [self.delegate moduleApplication:self willFinishLaunchingWithOptions:launchOptions];
    }
    self.launchOptions = theLaunchOptions;
    self.moduleType = [[launchOptions safeObjectForKey:@"type"] integerValue];
    if (self.moduleType == 0) {
        self.moduleType = self.module.defaultModuleType;
    }
    self.dao = [ZSTDao daoWithModuleType:self.moduleType];
    if ([self.delegate respondsToSelector:@selector(moduleApplication:didFinishLaunchingWithOptions:)]) {
        [self.delegate moduleApplication:self didFinishLaunchingWithOptions:launchOptions];
    }
    return self.delegate.rootView;
}

@end
