//
//  ZSTLogUtil.m
//  F3
//
//  Created by 9588 on 11/2/11.
//  Copyright 2011 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTLogUtil.h"
#import "ZSTSqlManager.h"
#import "ZSTF3Preferences.h"
#import "ZSTUtils.h"
#import "ZSTUtils.h"

#define kBoolTrue @"true"
#define kBoolFalse @"false"

NSString * const LogType_UserAction = @"ACTION";
NSString * const LogType_UserView = @"VIEW";
NSString * const LogType_SysConnect = @"CONNECT";
NSString * const LogType_SysIPPush = @"IPPUSH";
NSString * const LogType_SysError = @"ERROR";
NSString * const LogType_SysInfo = @"INFO";

NSString * const IPPush_StartLogin = @"START_LOGIN";
NSString * const IPPush_LoginFail = @"LOGIN_FAIL";
NSString * const IPPush_Success = @"LOGIN_SUCCESS";
NSString * const IPPush_GetMsg = @"GET_MSG";
NSString * const IPPush_Logout = @"LOGOUT";

static NSDate *lastUpdateDate = nil;

@implementation ZSTLogUtil

+ (void)redirectNSLogToDocumentFolder{
#if !TARGET_IPHONE_SIMULATOR
    #ifdef NDEBUG    
    NSFileManager *fm = [NSFileManager defaultManager];
    
    NSString *logssDir = [NSString stringWithFormat:@"%@", [[ZSTUtils pathForECECC] stringByAppendingPathComponent:@"/logs/"]];
    
    if (![fm fileExistsAtPath:logssDir]) {
        NSDictionary *attributes = [NSDictionary dictionaryWithObject: [NSNumber numberWithUnsignedLong: 0777] forKey: NSFilePosixPermissions];
        NSError *theError = NULL;
        
        [fm createDirectoryAtPath:logssDir withIntermediateDirectories: YES attributes: attributes error: &theError];
    }
    
    NSString *fileName =[NSString stringWithFormat:@"sys_%@.log", [ZSTUtils formatDate:[NSDate date] format:@"yyyy-MM-dd"]];
    NSString *logFilePath = [logssDir stringByAppendingPathComponent:fileName];
    freopen([logFilePath cStringUsingEncoding:NSASCIIStringEncoding],"a+",stderr);
    #endif
#endif
}

+ (void)log:(NSString *)description type:(NSString *)logType
{
    if ([logType isEqualToString:LogType_UserAction] || [logType isEqualToString:LogType_UserView]) {
        NSString *sql = @"Insert into UserLog (time, logType, description) values (?, ?, ?) ";
        [ZSTSqlManager executeUpdate:sql,
                                                  [NSDate date],
                                                  logType,
                                                  description
         ];
    }
    else
    {
        @synchronized(self)
        {
            if (lastUpdateDate == nil || ![ZSTUtils isInOneDate:lastUpdateDate date2:[NSDate date]]) {
                [self redirectNSLogToDocumentFolder];
            }
            [lastUpdateDate release];
            lastUpdateDate = [[NSDate date] retain];
            
            NSLog(@"[%@]%@", logType, description);
        }
    }
}

+ (void)logUserAction:(NSString *)viewClassName
{
    [self log:[NSString stringWithFormat:@"[Iphone][%@][%@]", [ZSTF3Preferences shared].loginMsisdn, viewClassName] type:LogType_UserAction];
}

+ (void)logUserView:(NSString *)viewName pushId:(NSString *)pushId messageId:(NSString *)MSGID
{
    [self log:[NSString stringWithFormat:@"[Iphone][%@][%@][%@][%@]", [ZSTF3Preferences shared].loginMsisdn, MSGID, pushId, viewName] type:LogType_UserView];
}

+ (void)logSysConnect:(NSURL *)url result:(BOOL)result response:(NSString *)response
{
    [self log:[NSString stringWithFormat:@"[Iphone][%@][%@][%@][%@]", [ZSTF3Preferences shared].loginMsisdn, url, result? kBoolTrue : kBoolFalse, response] type:LogType_SysConnect];
}

+ (void)logSysIPPush:(NSString *)type
{
    [self log:[NSString stringWithFormat:@"[Iphone][%@][%@]", [ZSTF3Preferences shared].loginMsisdn, type] type:LogType_SysIPPush];
}

+ (void)logSysError:(NSString *)error
{
    [self log:[NSString stringWithFormat:@"[Iphone][%@][%@]", [ZSTF3Preferences shared].loginMsisdn, error] type:LogType_SysError];
}

+ (void)logSysInfo:(NSString *)info
{
    [self log:[NSString stringWithFormat:@"[Iphone][%@][%@]", [ZSTF3Preferences shared].loginMsisdn, info] type:LogType_SysInfo];
}

+ (NSArray *)getUserLogsBeforeDate:(NSDate *)date
{   
    NSString *sql = @"Select time, logType, description from UserLog where time < ?";
    
    NSArray *resultSet = [ZSTSqlManager executeQuery:sql, date];
    NSMutableArray *infos = [NSMutableArray array];
    for (NSDictionary *rs in resultSet)
    {
        NSDate *time = [NSDate dateWithTimeIntervalSince1970:[[rs safeObjectForKey:@"time"] floatValue]];
        NSString *logType = [rs safeObjectForKey:@"logType"];
        NSString *description = [rs safeObjectForKey:@"description"];
        [infos addObject:[NSString stringWithFormat:@"[%@][%@]%@", [ZSTUtils formatDate:time format:@"yyyy-MM-dd HH:mm:ss"], logType, description]];
    }
    return infos;
}

+ (void)deleteUserLogsBeforeDate:(NSDate *)date
{   
    NSString *sql = @"delete from UserLog where time < ?";
    [ZSTSqlManager executeUpdate:sql, [NSDate date]];
}

+ (void)deleteSysLogsBeforeDate:(NSDate *)date
{
    NSDirectoryEnumerator *enums = [[NSFileManager defaultManager] enumeratorAtPath: [ZSTUtils pathForLogs]];
    NSString *fileName = nil;
    while ((fileName = [enums nextObject]) != nil) {
        NSDate *createDate = [[enums fileAttributes] fileCreationDate];
        
        if ([createDate compare:date] == NSOrderedAscending) {//时间有序递增
            NSString *logPath = [[ZSTUtils pathForLogs] stringByAppendingPathComponent:fileName];
            NSError *error = nil;
            // 从文件系统中删除
            if (![[NSFileManager defaultManager] removeItemAtPath:logPath error:&error]) {
                
            }
        }
    }
}

@end
