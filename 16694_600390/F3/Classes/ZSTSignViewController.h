//
//  ZSTSignViewController.h
//  F3
//
//  Created by pmit on 15/8/25.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"

typedef NS_ENUM(NSInteger, ZSTInputType) {
    //以下是枚举成员
    ZSTInputTypeSign = 0,
    ZSTInputTypeAddress,
    ZSTInputTypeName,
    ZSTInputTypeJob,
    ZSTInputTypeCompany,
    ZSTInputTypeSchool,
    ZSTInputTypeHometown
};

@protocol ZSTSignViewControllerDelegate  <NSObject>

- (void)saveCallBack:(ZSTInputType)inputType ContentString:(NSString *)contentString;

@end

@interface ZSTSignViewController : UIViewController

@property (copy,nonatomic) NSString *signString;
@property (assign,nonatomic) ZSTInputType inputType;
@property (weak,nonatomic) id<ZSTSignViewControllerDelegate> signDelegate;
@property (strong,nonatomic) ZSTF3Engine *engine;

@end
