//
//  ZSTSettingViewController.h
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZSTF3Engine.h"
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>

@interface ZSTSettingViewController : UIViewController

@property (strong,nonatomic) ZSTF3Engine *engine;
@property (assign,nonatomic) BOOL isPhonePublic;

@end
