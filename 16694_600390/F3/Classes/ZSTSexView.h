//
//  ZSTSexView.h
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZSTSexViewDelegate <NSObject>

- (void)sureSexSelected:(NSInteger)sexStringInteger;
- (void)cancelSexSelected;

@end

@interface ZSTSexView : UIView <UIPickerViewDelegate,UIPickerViewDataSource>

@property (weak,nonatomic) id<ZSTSexViewDelegate> sexDelegate;
@property (assign,nonatomic) NSInteger selectedSexInteger;
@property (assign,nonatomic) NSInteger defaultSexInteger;
@property (strong,nonatomic) UIPickerView *sexPickerView;

- (void)createSexPicker;
- (void)showDefault;

@end
