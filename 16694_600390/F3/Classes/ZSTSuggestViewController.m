//
//  ZSTSuggestViewController.m
//  F3
//
//  Created by pmit on 15/8/26.
//  Copyright (c) 2015年 ZhangShangTong Stock Co., Ltd. All rights reserved.
//

#import "ZSTSuggestViewController.h"
#import "ZSTUtils.h"

@interface ZSTSuggestViewController () <UITextViewDelegate,ZSTF3EngineDelegate>

@property (strong,nonatomic) UITextView *inputTextView;
@property (strong,nonatomic) UILabel *inputPlaceLB;

@end

@implementation ZSTSuggestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBarHidden = NO;
    self.navigationItem.titleView = [ZSTUtils titleViewWithTitle:NSLocalizedString(@"提提建议", nil)];
    self.navigationItem.leftBarButtonItem = [TKUIUtil backNewItemForNavigationWithTitle:@"" target:self selector:@selector(popViewController)];
//    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"提交" style:UIBarButtonItemStyleDone target:self action:@selector(submitSuggest:)];
    
    UIButton *saveBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    saveBtn.frame = CGRectMake(0, 0, 30, 20);
    [saveBtn setTitle:@"保存" forState:UIControlStateNormal];
    saveBtn.titleLabel.font = [UIFont systemFontOfSize:14.0f];
    [saveBtn addTarget:self action:@selector(submitSuggest:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:saveBtn];
    
    self.view.backgroundColor = RGBA(243, 243, 243, 1);
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tvResign:)];
    [self.view addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer *swipe = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(popViewController)];
    swipe.direction = UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swipe];
    
    [self buildSuggestView];
    
    self.engine = [[ZSTF3Engine alloc] init];
    self.engine.delegate = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)buildSuggestView
{
    self.inputTextView = [[UITextView alloc] initWithFrame:CGRectMake(15, 15, WIDTH - 30, 120)];
    self.inputTextView.font = [UIFont systemFontOfSize:12.0f];
    self.inputTextView.textColor = RGBA(136, 136, 136, 1);
    self.inputTextView.delegate = self;
    
    self.inputPlaceLB = [[UILabel alloc] initWithFrame:CGRectMake(4, 4, WIDTH - 30, 20)];
    self.inputPlaceLB.text = @"请在此输入您的建议...";
    self.inputPlaceLB.font = [UIFont systemFontOfSize:12.0f];
    self.inputPlaceLB.textColor = RGBA(204, 204, 204, 1);
    [self.inputTextView addSubview:self.inputPlaceLB];
    
    [self.view addSubview:self.inputTextView];
}

-(void)textViewDidChange:(UITextView *)textView
{
    //    self.examineText =  textView.text;
    if (textView.text.length == 0) {
        
        self.inputPlaceLB.hidden = NO;
        
    }else{
        
        self.inputPlaceLB.hidden = YES;
    }
}

- (void)tvResign:(UITapGestureRecognizer *)tap
{
    [self.inputTextView resignFirstResponder];
}

- (void)submitSuggest:(UIBarButtonItem *)sender
{
    if (self.inputTextView.text.length == 0 || [ZSTUtils isEmpty:self.inputTextView.text]) {
        [TKUIUtil showHUDInView:self.view withText:NSLocalizedString(@"请输入建议内容", nil) withImage:[UIImage imageNamed:@"icon_warning.png"] withCenter:CGPointMake(160, 100)];
        [TKUIUtil hiddenHUDAfterDelay:2];
        return;
    }
    else
    {
        [self.engine submitAdvice:self.inputTextView.text];
    }
}

- (void)submitAdviceResponse
{
    [self.navigationController popViewControllerAnimated:YES];
}

@end
