//
//  Adapter.h
//  Adapt
//
//  Created by anan on 10/15/15.
//  Copyright © 2015 anan. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

@interface Adapter : NSObject

+(void)adapt:(nullable id)target;

@end
